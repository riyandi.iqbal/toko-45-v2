<?php

return [
    'Corp' => [
        'Corp_nama'     => 'Primaax',
        'Corp_singkatan'    => 'Primaax',
        'Corp_icon'     => 'favicon.ico',
        'Corp_logo'     => 'logo.png',
        'Corp_alamat'   => 'Jl. Abcdef',
        'Corp_kota'     => 'Surabaya',
        'Corp_provinsi' => 'Jawa Timur',
        'Corp_negara'   => 'Indonesia',
        'Corp_telp'     => '123132301012',
        'Corp_email'    => 'primaax@email.com'
    ]
];