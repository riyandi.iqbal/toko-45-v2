/*
 Navicat Premium Data Transfer

 Source Server         : Local_PgSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 110005
 Source Host           : localhost:5432
 Source Catalog        : toko45_Opol
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110005
 File Encoding         : 65001

 Date: 01/07/2020 10:41:56
*/


-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_agen_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_agen_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_agen_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_bank_IDBank_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDBank_seq";
CREATE SEQUENCE "public"."tbl_bank_IDBank_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_bank_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_bank_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_bank_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_barcode_print_IDBarcodePrint_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_barcode_print_IDBarcodePrint_seq";
CREATE SEQUENCE "public"."tbl_barcode_print_IDBarcodePrint_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDBO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDBO_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDBO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_booking_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_booking_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_booking_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCOASaldoAwal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDCoa_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDCoa_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_coa_saldo_awal_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_coa_saldo_awal_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_giro_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDGiro_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDGiro_seq";
CREATE SEQUENCE "public"."tbl_giro_IDGiro_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_giro_IDPerusahaan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_giro_IDPerusahaan_seq";
CREATE SEQUENCE "public"."tbl_giro_IDPerusahaan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_asset_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_asset_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_barang_IDGroupBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_barang_IDGroupBarang_seq";
CREATE SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_coa_IDGroupCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_coa_IDGroupCOA_seq";
CREATE SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_customer_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_customer_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_supplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_supplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_group_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_group_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_group_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_gudang_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_gudang_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_gudang_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDGroupCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDGroupCustomer_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDHargaJual_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDHargaJual_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_harga_jual_barang_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_harga_jual_barang_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDHutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDHutang_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDHutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_hutang_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_hutang_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_hutang_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_in_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_in_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDIn_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDIn_seq";
CREATE SEQUENCE "public"."tbl_in_IDIn_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_in_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_in_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_in_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_in_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_in_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDPO_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDIP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDIP_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_instruksi_pengiriman_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDKartuStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDKartuStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kartu_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kartu_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_kartu_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDKonversi_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDKonversi_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanBerat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanBerat_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_konversi_satuan_IDSatuanKecil_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_konversi_satuan_IDSatuanKecil_seq";
CREATE SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKPDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_IDKPScan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDKP_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_koreksi_persediaan_scan_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_kota_IDKota_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_kota_IDKota_seq";
CREATE SEQUENCE "public"."tbl_kota_IDKota_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDLapPers_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDLapPers_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_lap_umur_persediaan_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_lap_umur_persediaan_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_loguser_id_log_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_loguser_id_log_seq";
CREATE SEQUENCE "public"."tbl_loguser_id_log_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_mata_uang_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_mata_uang_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_detail_IDMenu_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_detail_IDMenu_seq";
CREATE SEQUENCE "public"."tbl_menu_detail_IDMenu_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuDetail_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_menu_role_IDMenuRole_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_menu_role_IDMenuRole_seq";
CREATE SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_out_IDOut_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_out_IDOut_seq";
CREATE SEQUENCE "public"."tbl_out_IDOut_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDCOA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDCOA_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDCOA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFBPembayaran_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFBPembayaran_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembayaran_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembayaran_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_pembelian_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBADetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBADetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDFBA_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDFBA_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_asset_detail_IDGroupAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq";
CREATE SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFBDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_pembelian_grand_total_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_pembelian_grand_total_IDFB_seq";
CREATE SEQUENCE "public"."tbl_pembelian_grand_total_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_piutang_IDPiutang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_piutang_IDPiutang_seq";
CREATE SEQUENCE "public"."tbl_piutang_IDPiutang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_posting_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_posting_id_seq";
CREATE SEQUENCE "public"."tbl_posting_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDAgen_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDAgen_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDAgen_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPODetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPODetail_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPODetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDPO_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_purchase_order_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_purchase_order_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_purchase_order_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDFB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDFB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDFB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRBDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRBDetail_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRBDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_retur_pembelian_grand_total_IDRB_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_retur_pembelian_grand_total_IDRB_seq";
CREATE SEQUENCE "public"."tbl_retur_pembelian_grand_total_IDRB_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_saldo_awal_asset_IDSaldoAwalAsset_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq";
CREATE SEQUENCE "public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_satuan_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_satuan_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_satuan_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_AsalIDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_AsalIDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_AsalIDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFakturDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFakturDetail_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFakturDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDHistoryStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDHistoryStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDHistoryStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDMataUang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDMataUang_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDMataUang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDStok_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDStok_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDStok_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_history_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_history_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_history_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDGudang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDGudang_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDGudang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDStokOpname_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDStokOpname_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDStokOpname_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_stok_opname_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_stok_opname_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_stok_opname_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_suplier_IDGroupSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDGroupSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDGroupSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_suplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_suplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_suplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDPO_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSJM_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_celup_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_tampungan_IDT_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_tampungan_IDT_seq";
CREATE SEQUENCE "public"."tbl_tampungan_IDT_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDPO_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDPO_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDPO_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDCorak_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDCorak_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDCorak_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBSDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDTBS_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDTBS_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDTBS_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_detail_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_detail_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_detail_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSJH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSJH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDSupplier_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDBarang_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDMerk_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq";
CREATE SEQUENCE "public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDFaktur_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDFaktur_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDFaktur_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_um_customer_IDUMCustomer_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_um_customer_IDUMCustomer_seq";
CREATE SEQUENCE "public"."tbl_um_customer_IDUMCustomer_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_user_IDGroupUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDGroupUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDGroupUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_user_IDUser_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_IDUser_seq";
CREATE SEQUENCE "public"."tbl_user_IDUser_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_warna_IDWarna_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_warna_IDWarna_seq";
CREATE SEQUENCE "public"."tbl_warna_IDWarna_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "public"."migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS "public"."password_resets";
CREATE TABLE "public"."password_resets" (
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0)
)
;

-- ----------------------------
-- Table structure for tbl_agen
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_agen";
CREATE TABLE "public"."tbl_agen" (
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_agen_IDAgen_seq"'::regclass),
  "Kode_Perusahaan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Perusahaan" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Initial" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_Tlp" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Image" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(70) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_asset";
CREATE TABLE "public"."tbl_asset" (
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Asset" varchar(200) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Akumulasi_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Coa_Beban_Asset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_bank";
CREATE TABLE "public"."tbl_bank" (
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDBank_seq"'::regclass),
  "Nomor_Rekening" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_bank_IDCoa_seq"'::regclass),
  "Atas_Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_barang";
CREATE TABLE "public"."tbl_barang" (
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default",
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default",
  "Kode_Barang" varchar(20) COLLATE "pg_catalog"."default",
  "Nama_Barang" varchar(255) COLLATE "pg_catalog"."default",
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default",
  "Aktif" varchar(20) COLLATE "pg_catalog"."default",
  "IDKategori" varchar(64) COLLATE "pg_catalog"."default",
  "IDUkuran" varchar(64) COLLATE "pg_catalog"."default",
  "IDTipe" varchar(64) COLLATE "pg_catalog"."default",
  "Berat" int8,
  "StokMinimal" int8
)
;

-- ----------------------------
-- Records of tbl_barang
-- ----------------------------
INSERT INTO "public"."tbl_barang" VALUES ('1', '3', 'PS001', 'EM 4 Tambak 1lt', '2', 'aktif', '1', '38', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('2', '3', 'PS002', 'Linex 500gr', '1', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('3', '3', 'PS003', 'Lodan 1kg', '1', 'aktif', '1', '81', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('4', '3', 'PS004', 'Raja Bandeng 2,5kg', '1', 'aktif', '1', '17', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('5', '3', 'PS005', 'Supermes E 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('6', '3', 'PS006', 'Togatsu 100ml', '2', 'aktif', '1', '34', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('7', '3', 'PS007', 'Togatsu 300ml', '2', 'aktif', '1', '111', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('8', '3', 'PS008', 'Tosim 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('9', '3', 'PS009', 'Ursal 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('10', '4', 'PS010', 'Starmyl 25WP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('11', '4', 'PS011', 'Alberto 375 EC 250ml', '2', 'aktif', '1', '85', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('12', '4', 'PS012', 'Amistartop 325SC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('13', '4', 'PS013', 'Amistartop 325SC 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('14', '4', 'PS014', 'Antracol 70WP 250gr', '1', 'aktif', '1', '131', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('15', '4', 'PS015', 'Antracol 70WP 500gr', '1', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('16', '4', 'PS016', 'Antracol 70WP 1kg', '1', 'aktif', '1', '31', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('17', '4', 'PS017', 'Aurora 800gr', '1', 'aktif', '1', '61', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('18', '4', 'PS018', 'Azoxa Plus 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('19', '4', 'PS019', 'Azoxa Plus 200ml', '2', 'aktif', '1', '53', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('20', '4', 'PS020', 'Azoxa Plus 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('21', '4', 'PS021', 'Blast 200SC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('22', '4', 'PS022', 'Blast Gone 25gr', '1', 'aktif', '1', '78', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('23', '4', 'PS023', 'Brilliant 72WP 100gr', '1', 'aktif', '1', '33', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('24', '4', 'PS024', 'Delsene MX 80WP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('25', '4', 'PS025', 'Dithane M-45 80WP 200gr', '1', 'aktif', '1', '122', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('26', '4', 'PS026', 'Dithane M-45 80WP 500gr', '1', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('27', '4', 'PS027', 'Dithane M-45 80WP 1kg', '1', 'aktif', '1', '48', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('28', '4', 'PS028', 'Filia 525SE 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('29', '4', 'PS029', 'Folicur 430SC 240ml', '2', 'aktif', '1', '73', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('30', '4', 'PS030', 'Fujiwan 400EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('31', '4', 'PS031', 'Heksa 50SC 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('32', '4', 'PS032', 'Inari 72,5WP 200gr', '1', 'aktif', '1', '128', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('33', '4', 'PS033', 'Kocide Opti  100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('34', '4', 'PS034', 'Kontaf Plus 250EC 400ml', '2', 'aktif', '1', '84', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('35', '4', 'PS035', 'Megathane 72WP 400gr', '1', 'aktif', '1', '101', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('36', '4', 'PS036', 'Recor Plus 250ml', '2', 'aktif', '1', '124', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('37', '4', 'PS037', 'Rovral 50WP 100gr', '1', 'aktif', '1', '33', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('38', '4', 'PS038', 'Sinergy 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('39', '4', 'PS039', 'Throne 250EC 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('40', '4', 'PS040', 'Throne 250EC 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('41', '4', 'PS041', 'Throne 250EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('42', '4', 'PS042', 'Topsin  500SC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('43', '4', 'PS043', 'Vilan 50EC 250ml', '2', 'aktif', '1', '85', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('44', '4', 'PS044', 'Vondozeb 500gr', '1', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('45', '4', 'PS045', 'Vondozeb 1kg', '1', 'aktif', '1', '13', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('46', '5', 'PS046', 'Aladin 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('47', '5', 'PS047', 'Ally 20WG 5gr', '1', 'aktif', '1', '66', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('48', '5', 'PS048', 'Ally plus 40gr', '1', 'aktif', '1', '65', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('49', '5', 'PS049', 'Amandy 865SL 400ml', '2', 'aktif', '1', '98', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('50', '5', 'PS050', 'Basmilang 480 AS 240ml', '2', 'aktif', '1', '54', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('51', '5', 'PS051', 'Benfuron 12/18WP 5gr', '1', 'aktif', '1', '112', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('52', '5', 'PS052', 'Benfuron 12/18WP 25gr', '1', 'aktif', '1', '43', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('53', '5', 'PS053', 'Bigquat 276SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('54', '5', 'PS054', 'Billy 20 WP 5gr', '1', 'aktif', '1', '104', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('55', '5', 'PS055', 'Bispac 100SC', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('56', '5', 'PS056', 'Crash 480SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('57', '5', 'PS057', 'Crash 480SL 1lt', '2', 'aktif', '1', '32', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('58', '5', 'PS058', 'Crash 480SL 5lt', '2', 'aktif', '1', '89', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('59', '5', 'PS059', 'Crash 480SL 20lt', '2', 'aktif', '1', '10', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('60', '5', 'PS060', 'Gempur 480SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('61', '5', 'PS061', 'Goal 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('62', '5', 'PS062', 'Goal 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('63', '5', 'PS063', 'Indamin 200ml', '2', 'aktif', '1', '53', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('64', '5', 'PS064', 'Indamin 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('65', '5', 'PS065', 'Indamin plus 40gr', '1', 'aktif', '1', '90', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('66', '5', 'PS066', 'Indomet 20WG 5gr', '1', 'aktif', '1', '133', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('67', '5', 'PS067', 'Kimiru 45WP 200gr', '1', 'aktif', '1', '52', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('68', '5', 'PS068', 'Lindomin 860AS 200ml', '2', 'aktif', '1', '62', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('69', '5', 'PS069', 'Logran 75WG 1,5gr', '1', 'aktif', '1', '28', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('70', '5', 'PS070', 'Promin 865SL 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('71', '5', 'PS071', 'Roger 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('72', '5', 'PS072', 'Roger 5lt', '2', 'aktif', '1', '89', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('73', '5', 'PS073', 'Roger 20lt', '2', 'aktif', '1', '10', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('74', '5', 'PS074', 'Round Up 200ml', '2', 'aktif', '1', '123', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('75', '5', 'PS075', 'Round Up 1lt', '2', 'aktif', '1', '32', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('76', '5', 'PS076', 'Round Up 4lt', '2', 'aktif', '1', '88', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('77', '5', 'PS077', 'Round Up 20lt', '2', 'aktif', '1', '10', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('78', '5', 'PS078', 'Round Up (1/2literan)', '2', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('79', '5', 'PS079', 'Rumpas 120EW 100ml', '2', 'aktif', '1', '24', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('80', '5', 'PS080', 'Rumpas 120EW 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('81', '5', 'PS081', 'Sando Up 480SL 200ml', '2', 'aktif', '1', '123', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('82', '5', 'PS082', 'Servoxon 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('83', '5', 'PS083', 'Servoxon 5lt', '2', 'aktif', '1', '89', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('84', '5', 'PS084', 'Servoxon 20lt', '2', 'aktif', '1', '10', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('85', '5', 'PS085', 'Sidamin 865AS 400ml', '2', 'aktif', '1', '102', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('86', '5', 'PS086', 'Supremo 480SL 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('87', '5', 'PS087', 'Supretox 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('88', '5', 'PS088', 'Tabas 400EC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('89', '5', 'PS089', 'Tabas 400EC 200ml', '2', 'aktif', '1', '62', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('90', '5', 'PS090', 'Tigold 10WP 25gr', '1', 'aktif', '1', '64', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('91', '5', 'PS091', 'Win WP 5gr', '1', 'aktif', '1', '126', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('92', '5', 'PS092', 'Win WP 5gr (1 pak)', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('93', '5', 'PS093', 'Wrap Up 1lt', '2', 'aktif', '1', '32', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('94', '5', 'PS094', 'Wrap Up 5lt', '2', 'aktif', '1', '89', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('95', '5', 'PS095', 'Wrap Up 20lt', '2', 'aktif', '1', '10', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('96', '6', 'PS096', 'Abacel 18EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('97', '6', 'PS097', 'Abacel 18EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('98', '6', 'PS098', 'Abacel 18EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('99', '6', 'PS099', 'Abacel 18EC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('100', '6', 'PS100', 'Abuki 50SL 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('101', '6', 'PS101', 'Abuki 50SL 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('102', '6', 'PS102', 'Abuki 50SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('103', '6', 'PS103', 'Agrimec 18 EC 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('104', '6', 'PS104', 'Agrimec 18 EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('105', '6', 'PS105', 'Amethys 40EC 200ml', '2', 'aktif', '1', '62', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('106', '6', 'PS106', 'Amida 200SL (1 paket)', '2', 'aktif', '1', '8', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('107', '6', 'PS107', 'Ammate 150EC 50ml', '2', 'aktif', '1', '58', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('108', '6', 'PS108', 'Ammate 150EC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('109', '6', 'PS109', 'Applaud 10WP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('110', '6', 'PS110', 'Applaud 10WP 400gr', '1', 'aktif', '1', '83', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('111', '6', 'PS111', 'Applaud 440 SC 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('112', '6', 'PS112', 'Applaud 440 SC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('113', '6', 'PS113', 'Ares 100SL 300ml', '2', 'aktif', '1', '56', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('114', '6', 'PS114', 'Ares 100SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('115', '6', 'PS115', 'Arrivo 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('116', '6', 'PS116', 'Barrier 1kg', '1', 'aktif', '1', '30', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('117', '6', 'PS117', 'Baycarb 500EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('118', '6', 'PS118', 'Benhur 500EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('119', '6', 'PS119', 'Benhur 500EC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('120', '6', 'PS120', 'Bestok 50EC 80ml', '2', 'aktif', '1', '27', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('121', '6', 'PS121', 'Bestok 50EC 250ml', '2', 'aktif', '1', '110', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('122', '6', 'PS122', 'Bestok 50EC 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('123', '6', 'PS123', 'Besvidan 160/10EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('124', '6', 'PS124', 'Buldok 25EC 100ml', '2', 'aktif', '1', '24', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('125', '6', 'PS125', 'Buldok 25EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('126', '6', 'PS126', 'Closer 50WG 7,5gr', '1', 'aktif', '1', '129', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('127', '6', 'PS127', 'Confidor 5WP 15gr', '1', 'aktif', '1', '37', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('128', '6', 'PS128', 'Confidor 5WP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('129', '6', 'PS129', 'Cronus 18EC 200ml', '2', 'aktif', '1', '123', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('130', '6', 'PS130', 'Curacron 500EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('131', '6', 'PS131', 'Curacron 500EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('132', '6', 'PS132', 'Cypermax 100 EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('133', '6', 'PS133', 'Darmasan 600EC 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('134', '6', 'PS134', 'Darmasan 600EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('135', '6', 'PS135', 'Decis 25EC 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('136', '6', 'PS136', 'Decis 25EC 100ml', '2', 'aktif', '1', '24', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('137', '6', 'PS137', 'Decis 25EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('138', '6', 'PS138', 'Decis 25EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('139', '6', 'PS139', 'Deltacron 500EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('140', '6', 'PS140', 'Destello 480SC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('141', '6', 'PS141', 'Diazinon 600EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('142', '6', 'PS142', 'Dimilin 25WP 100gr', '2', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('143', '6', 'PS143', 'Dimocel 400SL 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('144', '6', 'PS144', 'Dimocel 400SL 2lt', '2', 'aktif', '1', '16', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('145', '6', 'PS145', 'Dimocel 400SL 5lt', '2', 'aktif', '1', '89', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('146', '6', 'PS146', 'Dobelman 45EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('147', '6', 'PS147', 'Dursban 200EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('148', '6', 'PS148', 'Dursban 200EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('149', '6', 'PS149', 'Dursban 200EC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('150', '6', 'PS150', 'Dipho 290 SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('151', '6', 'PS151', 'Dipho 290 SL 400ml', '2', 'aktif', '1', '102', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('152', '6', 'PS152', 'Dipho 290 SL 200ml', '2', 'aktif', '1', '62', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('153', '6', 'PS153', 'Emacel 30EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('154', '6', 'PS154', 'Emacel 30EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('155', '6', 'PS155', 'Emacel 30EC 1lt', '2', 'aktif', '1', '119', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('156', '6', 'PS156', 'Endure 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('157', '6', 'PS157', 'Endure 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('158', '6', 'PS158', 'Enduro 120SC 50ml', '2', 'aktif', '1', '116', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('159', '6', 'PS159', 'Enduro 120SC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('160', '6', 'PS160', 'Furadan 3G 2kg', '1', 'aktif', '1', '15', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('161', '6', 'PS161', 'Gavin 35WP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('162', '6', 'PS162', 'Greta 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('163', '6', 'PS163', 'Greta 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('164', '6', 'PS164', 'Indovin 85 SP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('165', '6', 'PS165', 'Joker 100gr', '1', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('166', '6', 'PS166', 'Joker 400gr', '1', 'aktif', '1', '83', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('167', '6', 'PS167', 'Kejora 15EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('168', '6', 'PS168', 'Kejora 15EC 1lt', '2', 'aktif', '1', '41', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('169', '6', 'PS169', 'Ketave 100SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('170', '6', 'PS170', 'Ketave 100SL 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('171', '6', 'PS171', 'Kick Off 36EC 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('172', '6', 'PS172', 'X Siller 550EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('173', '6', 'PS173', 'Larvin 75WP 15gr', '1', 'aktif', '1', '127', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('174', '6', 'PS174', 'Losmine 50EC 250ml', '2', 'aktif', '1', '85', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('175', '6', 'PS175', 'Maestro 200EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('176', '6', 'PS176', 'Marshal 200EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('177', '6', 'PS177', 'Marshal 200EC 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('178', '6', 'PS178', 'Marshal 200SC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('179', '6', 'PS179', 'Marshal 200SC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('180', '6', 'PS180', 'Marshal 25DS 25gr', '1', 'aktif', '1', '64', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('181', '6', 'PS181', 'Marshal 25DS 100gr', '1', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('182', '6', 'PS182', 'Maxima 68WP 250gr', '1', 'aktif', '1', '95', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('183', '6', 'PS183', 'Meteor 25EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('184', '6', 'PS184', 'Mipcinta 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('185', '6', 'PS185', 'Mipcinta 500gr', '1', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('186', '6', 'PS186', 'Mospilan 30EC 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('187', '6', 'PS187', 'Mospilan 30EC 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('188', '6', 'PS188', 'Montaf 400SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('189', '6', 'PS189', 'Nurelle D 500/50EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('190', '6', 'PS190', 'Obsesi 400SL 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('191', '6', 'PS191', 'Ohio 10WP 100gr', '1', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('192', '6', 'PS192', 'Osada 75SP 400gr', '1', 'aktif', '1', '97', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('193', '6', 'PS193', 'Ovistop 100EC 400ml', '2', 'aktif', '1', '102', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('194', '6', 'PS194', 'Paket Keriting Kecil', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('195', '6', 'PS195', 'Padan 50SP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('196', '6', 'PS196', 'Pexalon 106SC 100ml', '2', 'aktif', '1', '29', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('197', '6', 'PS197', 'Plenum 25gr', '1', 'aktif', '1', '35', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('198', '6', 'PS198', 'Plenum 100gr', '1', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('199', '6', 'PS199', 'Pounce 20EC 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('200', '6', 'PS200', 'Prevathon 50SC (Dupont) 100ml', '2', 'aktif', '1', '93', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('201', '6', 'PS201', 'Prevathon 50SC (FMC) 100ml', '2', 'aktif', '1', '93', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('202', '6', 'PS202', 'Prevathon 50SC 250ml', '2', 'aktif', '1', '70', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('203', '6', 'PS203', 'Promectin 18 EC 50ml', '2', 'aktif', '1', '116', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('204', '6', 'PS204', 'Promectin 18 EC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('205', '6', 'PS205', 'Promectin 18 EC 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('206', '6', 'PS206', 'Promectin 60 EC 200ml', '2', 'aktif', '1', '123', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('207', '6', 'PS207', 'Prosid 25WP 100gr', '2', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('208', '6', 'PS208', 'Prosid 150EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('209', '6', 'PS209', 'Raydock 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('210', '6', 'PS210', 'Raydock 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('211', '6', 'PS211', 'Regent 50SC 100ml', '2', 'aktif', '1', '109', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('212', '6', 'PS212', 'Regent 50SC 250ml', '2', 'aktif', '1', '70', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('213', '6', 'PS213', 'Regent 50SC 500ml', '2', 'aktif', '1', '18', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('214', '6', 'PS214', 'Regent 0,3 gr 1kg', '1', 'aktif', '1', '13', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('215', '6', 'PS215', 'Regent WG 1,6gr (1 pak isi 5)', '1', 'aktif', '1', '7', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('216', '6', 'PS216', 'Ripcord 100ml', '2', 'aktif', '1', '109', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('217', '6', 'PS217', 'Ripcord 250ml', '2', 'aktif', '1', '70', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('218', '6', 'PS218', 'Ripcord 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('219', '6', 'PS219', 'Samite 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('220', '6', 'PS220', 'Samite 250ml', '2', 'aktif', '1', '124', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('221', '6', 'PS221', 'Sevin 85 SP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('222', '6', 'PS222', 'Sidacin 50WP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('223', '6', 'PS223', 'Sidazinon 600EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('224', '6', 'PS224', 'Sigomektin 36EC 200ml', '2', 'aktif', '1', '94', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('225', '6', 'PS225', 'Sinopest 250EC 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('226', '6', 'PS226', 'Sniper 50EC 250ml', '2', 'aktif', '1', '55', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('227', '6', 'PS227', 'Solfac 20gr', '1', 'aktif', '1', '47', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('228', '6', 'PS228', 'Spontan 400SL 200ml', '2', 'aktif', '1', '53', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('229', '6', 'PS229', 'Spontan 400SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('230', '6', 'PS230', 'Spontan 400SL 1lt', '2', 'aktif', '1', '32', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('231', '6', 'PS231', 'Spontan 400SL 4lt', '2', 'aktif', '1', '88', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('232', '6', 'PS232', 'Stadium 18EC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('233', '6', 'PS233', 'Starban 585EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('234', '6', 'PS234', 'Starfidor 5WP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('235', '6', 'PS235', 'Stuntman 500SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('236', '6', 'PS236', 'Stuntman 500SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('237', '6', 'PS237', 'Talstar 25EC 250ml', '2', 'aktif', '1', '85', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('238', '6', 'PS238', 'Tandem 325SC 250ml', '2', 'aktif', '1', '85', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('239', '6', 'PS239', 'Taurus 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('240', '6', 'PS240', 'Taurus 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('241', '6', 'PS241', 'Tenchu 20SG 25gr', '1', 'aktif', '1', '35', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('242', '6', 'PS242', 'Tenchu 20SG 100gr', '1', 'aktif', '1', '120', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('243', '6', 'PS243', 'Tetrin 30EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('244', '6', 'PS244', 'Thompas 25EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('245', '6', 'PS245', 'Thompas 25EC 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('246', '6', 'PS246', 'Top Up 525SL 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('247', '6', 'PS247', 'Total 50EC 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('248', '6', 'PS248', 'Total 10/40EC 100ml', '2', 'aktif', '1', '20', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('249', '6', 'PS249', 'Trebon 500ml', '2', 'aktif', '1', '72', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('250', '6', 'PS250', 'Trigon 25EC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('251', '6', 'PS251', 'Trisula 450SL 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('252', '6', 'PS252', 'Trisula 450SL 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('253', '6', 'PS253', 'Trisula 450SL 2lt', '2', 'aktif', '1', '16', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('254', '6', 'PS254', 'Virtako 300SC 10ml', '1', 'aktif', '1', '12', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('255', '6', 'PS255', 'Virtako 300SC 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('256', '6', 'PS256', 'Virtako 300SC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('257', '6', 'PS257', 'Vista 400WSC 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('258', '6', 'PS258', 'Winder 25 WP 100gr', '1', 'aktif', '1', '130', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('259', '6', 'PS259', 'Winder 100EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('260', '6', 'PS260', 'Winder 100EC 250ml', '2', 'aktif', '1', '124', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('261', '7', 'PS261', 'Hamador 25EC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('262', '9', 'PS262', 'Bentan 45 WP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('263', '9', 'PS263', 'Bentan 60 WP 100gr', '1', 'aktif', '1', '23', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('264', '9', 'PS264', 'Instant 60WP 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('265', '10', 'PS265', 'EM 4 Peternakan 1lt', '2', 'aktif', '1', '38', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('266', '11', 'PS266', 'Besmore 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('267', '11', 'PS267', 'Besmore 500ml', '2', 'aktif', '1', '100', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('268', '11', 'PS268', 'Besmore Ultra 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('269', '11', 'PS269', 'Tripel XXX 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('270', '13', 'PS270', 'Bambu Ijo 12ml', '2', 'aktif', '1', '103', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('271', '13', 'PS271', 'Boom Flower 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('272', '13', 'PS272', 'Boom Flower 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('273', '13', 'PS273', 'Dosdet 500gr', '1', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('274', '13', 'PS274', 'EM 4 Pertanian 1lt', '2', 'aktif', '1', '38', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('275', '13', 'PS275', 'Fello 500gr', '1', 'aktif', '1', '87', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('276', '13', 'PS276', 'Ferterra 2kg', '1', 'aktif', '1', '15', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('277', '13', 'PS277', 'Gandasil B 500gr', '1', 'aktif', '1', '71', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('278', '13', 'PS278', 'Gardena D 500gr', '1', 'aktif', '1', '71', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('279', '13', 'PS279', 'Gardena B 500gr', '1', 'aktif', '1', '71', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('280', '13', 'PS280', 'Grand K 1kg', '1', 'aktif', '1', '48', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('281', '13', 'PS281', 'Grand K Merah 2kg', '1', 'aktif', '1', '45', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('282', '13', 'PS282', 'Green Asri (GA) 1lt', '2', 'aktif', '1', '82', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('283', '13', 'PS283', 'Kemira 100gr', '1', 'aktif', '1', '19', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('284', '13', 'PS284', 'KCL Cair 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('285', '13', 'PS285', 'KCL Cair 1lt', '2', 'aktif', '1', '6', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('286', '13', 'PS286', 'Multi KP 1kg', '1', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('287', '13', 'PS287', 'Multi Padi 1kg', '1', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('288', '13', 'PS288', 'Power Grow 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('289', '13', 'PS289', 'Power Grow 2lt', '2', 'aktif', '1', '51', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('290', '13', 'PS290', 'Rosasol E 1lb', '2', 'aktif', '1', '92', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('291', '13', 'PS291', 'Rosasol K (red) 500gr', '2', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('292', '13', 'PS292', 'Rosasol K (red) (bungkusan) 1kg', '1', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('293', '13', 'PS293', 'Rosasol N (green) 500gr', '2', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('294', '13', 'PS294', 'Rosasol P (orange) 500gr', '2', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('295', '13', 'PS295', 'Supermes 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('296', '13', 'PS296', 'Tanivit B 500gr', '1', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('297', '13', 'PS297', 'Tanivit D 500gr', '1', 'aktif', '1', '99', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('298', '13', 'PS298', 'Trobos 500ml', '2', 'aktif', '1', '125', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('299', '14', 'PS299', 'Ratcell 80P 10gr', '1', 'aktif', '1', '11', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('300', '14', 'PS300', 'Ratgone 0,005BB 1kg', '1', 'aktif', '1', '13', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('301', '14', 'PS301', 'Ratgone 0,005BB (bungkusan) ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('302', '16', 'PS302', 'Agrogib  35ml', '2', 'aktif', '1', '108', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('303', '16', 'PS303', 'Bigest 10ml', '2', 'aktif', '1', '63', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('304', '16', 'PS304', 'Bigest 50ml', '2', 'aktif', '1', '25', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('305', '16', 'PS305', 'Bigest Tablet', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('306', '16', 'PS306', 'Protephon 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('307', '16', 'PS307', 'Speed 6%SP 500gr', '2', 'aktif', '1', '59', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('308', '6', 'PS308', 'Arjuna 300ml', '2', 'aktif', '1', '56', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('309', '1', 'PS309', 'Bactocyn 250ml', '2', 'aktif', '1', '124', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('310', '6', 'PS310', 'Confidor SL 60ml', '2', 'aktif', '1', '26', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('311', '6', 'PS311', 'Crowen 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('312', '6', 'PS312', 'Cyrotex 75 SP 25gr', '1', 'aktif', '1', '64', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('313', '6', 'PS313', 'Darmabas 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('314', '6', 'PS314', 'Demollish  250ml', '2', 'aktif', '1', '42', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('315', '6', 'PS315', 'Destan 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('316', '6', 'PS316', 'Detacron 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('317', '6', 'PS317', 'Detazeb 1kg', '1', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('318', '5', 'PS318', 'Gromoxone 1lt', '2', 'aktif', '1', '49', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('319', '6', 'PS319', 'Hamacid 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('320', '13', 'PS320', 'KNO3 Merah 2kg', '1', 'aktif', '1', '39', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('321', '4', 'PS321', 'Kontaf 50SC 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('322', '4', 'PS322', 'Kontaf 50SC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('323', '6', 'PS323', 'Larvin 75WP 100gr', '1', 'aktif', '1', '130', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('324', '6', 'PS324', 'Naga 400ml', '2', 'aktif', '1', '84', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('325', '6', 'PS325', 'Noxone 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('326', '13', 'PS326', 'Paket Boom Padi', '7', 'aktif', '1', '9', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('327', '13', 'PS327', 'Paket Boom Padi kecil', '7', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('328', '6', 'PS328', 'Paket Keriting B 500ml', '7', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('329', '13', 'PS329', 'Power Call 1kg', '2', 'aktif', '1', '48', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('330', '16', 'PS330', 'Power Gib 30ml', '2', 'aktif', '1', '86', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('331', '6', 'PS331', 'Proclaim 25gr', '1', 'aktif', '1', '78', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('332', '6', 'PS332', 'Rajatrin 400ml', '2', 'aktif', '1', '84', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('333', '6', 'PS333', 'Rampage EC 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('334', '6', 'PS334', 'Rampage EC 250ml', '2', 'aktif', '1', '70', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('335', '6', 'PS335', 'Raydent 100ml', '2', 'aktif', '1', '121', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('336', '6', 'PS336', 'Raydent 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('337', '4', 'PS337', 'Score 250ml', '2', 'aktif', '1', '96', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('338', '6', 'PS338', 'Sidametrin 400ml', '2', 'aktif', '1', '84', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('339', '9', 'PS339', 'Snaildown 100ml', '2', 'aktif', '1', '50', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('340', '9', 'PS340', 'Snaildown 200ml', '2', 'aktif', '1', '53', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('341', '9', 'PS341', 'Snaildown 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('342', '4', 'PS342', 'Sorento 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('343', '6', 'PS343', 'Starban 585EC 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('344', '6', 'PS344', 'Supemec 1lt', '2', 'aktif', '1', '14', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('345', '6', 'PS345', 'Ultimax 500ml', '2', 'aktif', '1', '60', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('346', '6', 'PS346', 'Ventura 2kg', '1', 'aktif', '1', '15', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('347', '4', 'PS347', 'Victar 1kg', '1', 'aktif', '1', '48', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('348', '6', 'PS348', 'Viper 400ml', '2', 'aktif', '1', '57', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('349', '6', 'PS349', 'Winder 25 WP 25gr', '1', 'aktif', '1', '43', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('350', '6', 'PS350', 'Wingran 0,5 G', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('351', '6', 'PS351', 'Wingran WS 25gr', '1', 'aktif', '1', '43', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('352', '6', 'PS352', 'Zeus', '2', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('353', '13', 'PK01', 'Booster DGW 25kg', '6', 'aktif', '1', '79', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('354', '13', 'PK02', 'Booster DGW 50kg', '6', 'aktif', '1', '114', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('355', '13', 'PK03', 'DGW Compaction 50kg/sak', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('356', '13', 'PK04', 'DGW Compaction 5kg', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('357', '13', 'PK05', 'DGW Gold 25kg/sak', '6', 'aktif', '1', '80', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('358', '13', 'PK06', 'KCL Mahkota 1kg', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('359', '13', 'PK07', 'KCL Mahkota 5kg', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('360', '13', 'PK08', 'KCL Mahkota 50kg/sak', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('361', '13', 'PK09', 'KNO Merah 2kg ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('362', '13', 'PK11', 'MPS 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('363', '13', 'PK13', 'MPS 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('364', '13', 'PK15', 'MPS 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('365', '13', 'PK17', 'NPK Tawon 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('366', '13', 'PK19', 'NPK Tawon 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('367', '13', 'PK21', 'NPK Tawon 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('368', '13', 'PK23', 'Mutiara 16-16 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('369', '13', 'PK25', 'Mutiara 16-16 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('370', '13', 'PK27', 'Mutiara 16-16 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('371', '13', 'PK29', 'NPK Grower 25kg/sak ', '6', 'aktif', '1', '80', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('372', '13', 'PK31', 'NPK Grower 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('373', '13', 'PK33', 'NPK Grower  1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('374', '13', 'PK35', 'Ponska Plus 25kg/sak ', '6', 'aktif', '1', '80', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('375', '13', 'PK37', 'Patenkali 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('376', '13', 'PK39', 'Patenkali 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('377', '13', 'PK41', 'Patenkali 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('378', '13', 'PK43', 'Saprodap 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('379', '13', 'PK45', 'Saprodap 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('380', '13', 'PK47', 'Saprodap 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('381', '13', 'PK49', 'Dap Burung 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('382', '13', 'PK51', 'Dap Burung 5kg ', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('383', '13', 'PK53', 'Dap Burung 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('384', '13', 'PK55', 'Sendawa Putih 25kg ', '6', 'aktif', '1', '79', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('385', '13', 'PK57', 'Sendawa Putih 1kg ', '3', 'aktif', '1', '5', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('386', '13', 'PK59', 'TSP 46 50kg/sak ', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('387', '13', 'PK61', 'ZA Kuda 50kg/sak', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('388', '13', 'PK63', 'ZA Tawon 50kg/sak', '6', 'aktif', '1', '115', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('389', '9', 'PK65', 'Samponin 25kg', '6', 'aktif', '1', '79', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('390', '9', 'PK67', 'Samponin 5kg', '3', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('391', '9', 'PK69', 'Samponin 2kg', '3', 'aktif', '1', '45', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('392', '13', 'PK71', 'Kapur Tani 40kg', '6', 'aktif', '1', '91', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('393', '13', 'PK73', 'Nitrea 5kg', '1', 'aktif', '1', '113', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('394', '2', 'BT01', 'Cabe Tombak 10gr', '1', 'aktif', '1', '11', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('395', '2', 'BT02', 'Gambas (Oyong) Estillo 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('396', '2', 'BT03', 'Green Pak Choy 25gr', '1', 'aktif', '1', '138', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('397', '2', 'BT04', 'Green Pak Choy 50gr', '1', 'aktif', '1', '139', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('398', '2', 'BT05', 'Green Pak Choy (kaleng)', '4', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('399', '2', 'BT06', 'Jagung Golden Boy 500gr', '1', 'aktif', '1', '69', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('400', '2', 'BT07', 'Jagung Golden Boy 250gr', '1', 'aktif', '1', '105', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('401', '2', 'BT08', 'Kacang Panjang Marathon 50gr', '1', 'aktif', '1', '106', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('402', '2', 'BT09', 'Kacang Panjang Persada 500gr', '1', 'aktif', '1', '107', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('403', '2', 'BT10', 'Kangkung Amanda 1kg', '1', 'aktif', '1', '68', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('404', '2', 'BT11', 'Kangkung Bisi 1kg', '1', 'aktif', '1', '68', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('405', '2', 'BT12', 'Kembang Kol Orient ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('406', '2', 'BT13', 'Padi Ciherang SS 5kg', '1', 'aktif', '1', '135', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('407', '2', 'BT14', 'Paria Comodor 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('408', '2', 'BT15', 'Paria Crown 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('409', '2', 'BT16', 'Sawi Christina 25gr', '1', 'aktif', '1', '67', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('410', '2', 'BT17', 'Sawi Christina (kaleng) 100gr', '4', 'aktif', '1', '134', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('411', '2', 'BT18', 'Semangka Bali Flower 20gr', '1', 'aktif', '1', '36', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('412', '2', 'BT19', 'Semangka Bangkok Flower 10gr', '1', 'aktif', '1', '136', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('413', '2', 'BT20', 'Semangka Redin 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('414', '2', 'BT21', 'Semangka Redin 20gr', '1', 'aktif', '1', '76', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('415', '2', 'BT22', 'Semangka Redin 10gr', '1', 'aktif', '1', '136', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('416', '2', 'BT23', 'Terong Ratih Hijau 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('417', '2', 'BT24', 'Terong Ratih Ungu 10gr', '1', 'aktif', '1', '74', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('418', '2', 'BT25', 'Timun Model 21 20gr', '1', 'aktif', '1', '137', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('419', '2', 'BT26', 'Waluh Orbit 100gr', '1', 'aktif', '1', '75', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('420', '2', 'BT27', 'Caisim Tosakan bungkus 100gr', '1', 'aktif', '1', '77', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('421', '2', 'BT28', 'Caisim Tosakan kaleng ', '4', 'aktif', '1', '1', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('422', '2', 'BT29', 'Kacang Panjang Parade ', '1', 'aktif', '1', '3', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('423', '2', 'BT30', 'Terong Mustang ', '1', 'aktif', '1', '132', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('424', '2', 'BT31', 'Timun Sabana F1 ', '1', 'aktif', '1', '46', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('425', '2', 'BT32', 'Tomat Servo ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('426', '2', 'BT33', 'Tomat Timoti ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('427', '2', 'BT34', 'Kacang Panjang Katrina ', '1', 'aktif', '1', '2', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('428', '2', 'BT35', 'Pare Hokian 10gr', '1', 'aktif', '1', '44', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('429', '2', 'BT36', 'Timun Labana 20gr', '1', 'aktif', '1', '36', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('430', '2', 'BT37', 'Timun Sabana ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('431', '15', 'SP01', 'Sprayer ABC putih biru ', '5', 'aktif', '1', '40', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('432', '15', 'SP02', 'Sprayer Top Agri ', '5', 'aktif', '1', '40', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('433', '15', 'SP03', 'Sprayer Sukaku ', '5', 'aktif', '1', '40', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('434', '15', 'SP04', 'Sprayer Sukatani ', '5', 'aktif', '1', '40', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('435', '15', 'SP05', 'Sprayer Robotech ', '5', 'aktif', '1', '40', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('436', '15', 'SP06', 'Kemarangan Tanika Lubang 5 ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('437', '15', 'SP07', 'Kemarangan Tanika Lubang 4 ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('438', '15', 'SP08', 'Kemarangan Korea Lubang 4 ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('439', '15', 'SP09', 'Kemarangan Lubang apollo ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('440', '15', 'SP10', 'Kemarangan Lubang cabang ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('441', '15', 'SP11', 'Stik Elektrik Kuningan ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('442', '15', 'SP12', 'Stik Elektrik Plastik ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('443', '15', 'SP13', 'Stik Manual ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('444', '15', 'SP14', 'Selang Sprayer Elektrik ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('445', '15', 'SP15', 'Selang Sprayer Manual ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('446', '15', 'SP16', 'Dinamo Sprayer Otomatis', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('447', '15', 'SP17', 'Stop Kran Manual ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('448', '15', 'SP18', 'Stop Kran Elektrik ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('449', '15', 'SP19', 'Charger ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('450', '15', 'SP20', 'Dinamo Sprayer Biasa', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('451', '15', 'SP21', 'Potensio Sprayer', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('452', '15', 'SP22', 'Aki Sprayer ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('453', '15', 'SP23', 'Elbow ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('454', '15', 'SP24', 'Lumping Kulit B ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('455', '15', 'SP25', 'Lumping Kulit K ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('456', '15', 'SP26', 'Lumping Kulit T ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('457', '15', 'SP27', 'Mimis Besar ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('458', '15', 'SP28', 'Mimis Kecil ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('459', '15', 'SP29', 'Packing Atas ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('460', '15', 'SP30', 'Ring Bebek B ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('461', '15', 'SP31', 'Ring Bebek K ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('462', '15', 'SP32', 'Ring Piston ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('463', '15', 'SP33', 'Ring Piston ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('464', '15', 'SP34', 'Sabuk Sprayer Elektrik ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('465', '15', 'SP35', 'Sabuk Sprayer Manual ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('466', '15', 'SP36', 'Saringan atas  B ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('467', '15', 'SP37', 'Saringan atas  K ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('468', '15', 'SP38', 'Tabung piston Komplit ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('469', '15', 'SP39', 'Tutup atas B ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('470', '15', 'SP40', 'Tutup atas K ', '5', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('471', '12', 'LN01', 'Plastik Tebaran 0,6mm', '1', 'aktif', '1', '21', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('472', '12', 'LN02', 'Plastik Tebaran 0,3mm', '1', 'aktif', '1', '117', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('473', '12', 'LN03', 'Plastik Tebaran 0,5mm', '1', 'aktif', '1', '118', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('474', '12', 'LN04', 'Plastik Tebaran 0,8mm', '1', 'aktif', '1', '22', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('475', '12', 'LN05', 'Tali Rafia Lonceng ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('476', '12', 'LN06', 'Tali Rafia Gudang Beras ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('477', '12', 'LN07', 'Tali Rafia Panah ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('478', '12', 'LN08', 'Tali Palawija CR ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('479', '12', 'LN09', 'Tali Silver ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('480', '12', 'LN10', 'Polybag bawang ', '1', 'aktif', '1', '4', '1', 0, 0);
INSERT INTO "public"."tbl_barang" VALUES ('481', '8', 'LN11', 'Sepatu Sawah', '5', 'aktif', '1', '4', '1', 0, 0);

-- ----------------------------
-- Table structure for tbl_barcode_print
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_barcode_print";
CREATE TABLE "public"."tbl_barcode_print" (
  "Barcode" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CustDes" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "WarnaCust" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Yard" float8,
  "Panjang_Meter" float8,
  "Grade" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarcodePrint" int8 NOT NULL DEFAULT nextval('"tbl_barcode_print_IDBarcodePrint_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_booking_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_booking_order";
CREATE TABLE "public"."tbl_booking_order" (
  "IDBO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDBO_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_booking_order_IDCorak_seq"'::regclass),
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_booking_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_buku_bank
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_bank";
CREATE TABLE "public"."tbl_buku_bank" (
  "IDBukuBank" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_buku_bank_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_bank_detail";
CREATE TABLE "public"."tbl_buku_bank_detail" (
  "IDBukuBankDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBukuBank" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_buku_kas
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_kas";
CREATE TABLE "public"."tbl_buku_kas" (
  "IDBukuKas" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_uang" float8
)
;

-- ----------------------------
-- Table structure for tbl_buku_kas_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_buku_kas_detail";
CREATE TABLE "public"."tbl_buku_kas_detail" (
  "IDBukuKasDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBukuKas" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Kredit" float8
)
;

-- ----------------------------
-- Table structure for tbl_bulan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_bulan";
CREATE TABLE "public"."tbl_bulan" (
  "IDBulan" varchar(2) COLLATE "pg_catalog"."default" NOT NULL,
  "NmBulan" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_cara_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_cara_bayar";
CREATE TABLE "public"."tbl_cara_bayar" (
  "IDCaraBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama" varchar(255) COLLATE "pg_catalog"."default",
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "PO" varchar(10) COLLATE "pg_catalog"."default",
  "PH" varchar(10) COLLATE "pg_catalog"."default",
  "FB" varchar(10) COLLATE "pg_catalog"."default",
  "SO" varchar(10) COLLATE "pg_catalog"."default",
  "FJ" varchar(10) COLLATE "pg_catalog"."default",
  "PP" varchar(10) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_cara_bayar
-- ----------------------------
INSERT INTO "public"."tbl_cara_bayar" VALUES ('5e37ce95aa147', 'TRANSFER', 'P000003', '1', '2020-02-03 07:41:09', '1', '2020-02-04 06:19:21', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes');
INSERT INTO "public"."tbl_cara_bayar" VALUES ('5e37cf97b6737', 'CASH', 'P000093', '1', '2020-02-03 07:45:27', '1', '2020-02-27 04:12:43', 'yes', 'yes', 'yes', 'yes', 'yes', 'yes');
INSERT INTO "public"."tbl_cara_bayar" VALUES ('5e37cfa3999a2', 'GIRO', 'P000003', '1', '2020-02-03 07:45:39', NULL, NULL, 'no', 'yes', 'yes', 'no', 'yes', 'yes');
INSERT INTO "public"."tbl_cara_bayar" VALUES ('5e3a2442b3951', 'DEBET', 'P000003', '1', '2020-02-05 02:11:14', NULL, NULL, 'no', 'no', 'no', 'no', 'no', 'no');

-- ----------------------------
-- Table structure for tbl_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa";
CREATE TABLE "public"."tbl_coa" (
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_IDCoa_seq"'::regclass),
  "Kode_COA" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_COA" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Modal" float8,
  "Saldo_Awal" varchar(10) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_coa
-- ----------------------------
INSERT INTO "public"."tbl_coa" VALUES ('P000004', '10201', 'Piutang Dagang', 'anak', 'aktif', 'P000001', 0, 'f');
INSERT INTO "public"."tbl_coa" VALUES ('P000005', '10202', 'Piutang Giro Mundur', 'anak', 'aktif', 'P000001', 0, 'f');
INSERT INTO "public"."tbl_coa" VALUES ('P000006', '10203', 'Piutang Usaha', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000007', '10204', 'Piutang Lain-lain', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000008', '10301', 'Persediaan Dalam Proses', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000009', '10302', 'Persedian Barang', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000012', '10402', 'Pajak Dibayar Dimuka - PPH 22', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000013', '10401', 'PPN Masukan', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000014', '10403', 'Pajak Dibayar Dimuka - PPH 23', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000015', '10404', 'Pajak Dibayar Dimuka - PPH 25', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000016', '10405', 'Pajak Dibayar Dimuka - PPH 29', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000017', '10406', 'Pajak Dibayar Dimuka - PPH 4(2)', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000018', '10501', 'Biaya Dibayar Dimuka - Sewa', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000019', '10502', 'Biaya Dibayar Dimuka - Asuransi', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000020', '10601', 'Asset Tetap - Tanah', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000021', '10602', 'Asset Tetap - Bangunan', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000022', '10603', 'Asset Tetap - Kendaraan', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000023', '10604', 'Asset Tetap - Lainnya', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000024', '10701', 'Akumulasi Depresiasi - Bangunan', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000025', '10702', 'Akumulasi Depresiasi - Kendaraan', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000026', '10703', 'Akumulasi Depresiasi - Lainnya', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000027', '10801', 'Asset Tidak Lancar Lainnya', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000028', '20101', 'Utang Jangka Pendek', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000029', '20102', 'Utang Giro Mundur', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000030', '20103', 'Utang Usaha', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000031', '20104', 'Utang Lain-lain Pihak Ketiga', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000032', '20105', 'Utang Jangka Panjang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000033', '20201', 'PPN Keluaran', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000034', '20202', 'PPH 4(2) - Terutang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000035', '20203', 'PPH 21 - Terutang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000037', '20205', 'PPH 25 - Terutang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000038', '20206', 'PPH 26 - Terutang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000039', '20207', 'PPH 29 - Terutang', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000041', '20302', 'Utang Deviden', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000042', '20401', 'Modal Disetor', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000043', '20402', 'Saldo Laba Ditahan', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000044', '20403', 'Laba Rugi Tahun Berjalan', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000045', '30101', 'Modal', 'anak', 'aktif', 'P000003', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000046', '30102', 'Prive', 'anak', 'aktif', 'P000003', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000047', '30103', 'Laba Tahun Berjalan', 'anak', 'aktif', 'P000003', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000048', '40101', 'Penjualan', 'anak', 'aktif', 'P000004', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000049', '40102', 'Retur Penjualan', 'anak', 'aktif', 'P000004', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000050', '40103', 'Harga Pokok Penjualan', 'anak', 'aktif', 'P000004', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000051', '40104', 'Potongan Penjualan', 'anak', 'aktif', 'P000004', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000052', '60101', 'Biaya Penjualan - Promosi dan Iklan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000053', '60102', 'Biaya Penjualan - Komisi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000054', '60103', 'Biaya Penjualan - Perjalanan dan Akomodasi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000055', '60104', 'Biaya Penjualan - Transportasi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000057', '60202', 'Biaya Administrasi Umum - Lembur', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000058', '60203', 'Biaya Administrasi Umum - Tunjangan dan Bonus', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000059', '60204', 'Biaya Administrasi Umum - Asuransi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000060', '60205', 'Biaya Administrasi Umum - Pemeliharaan Gedung', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000061', '60206', 'Biaya Administrasi Umum - Pemeliharaan Kendaraan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000062', '60207', 'Biaya Administrasi Umum - Sumbangan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000063', '60208', 'Biaya Administrasi Umum - Perizinan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000064', '60209', 'Biaya Administrasi Umum - Alat Tulis Kantor', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000065', '60210', 'Biaya Administrasi Umum - Biaya Bank', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000066', '60211', 'Biaya Administrasi Umum - Perjalanan dan Akomodasi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000067', '60212', 'Biaya Administrasi Umum - Transportasi', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000068', '60213', 'Biaya Administrasi Umum - Konsultan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000069', '60214', 'Biaya Administrasi Umum - Depresiasi Bangunan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000070', '60215', 'Biaya Administrasi Umum - Depresiasi Kendaraan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000071', '60216', 'Biaya Administrasi Umum - Depresiasi Lainnya', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000072', '60217', 'Biaya Administrasi Umum - Telepon', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000073', '60218', 'Biaya Administrasi Umum - Internet', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000074', '60219', 'Biaya Administrasi Umum - Air dan Listrik', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000075', '60220', 'Biaya Administrasi Umum - Pajak', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000076', '60221', 'Biaya Administrasi Umum - Asuransi Kendaraan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000077', '60222', 'Biaya Administrasi Umum - Asuransi Bangunan', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000003', '10103', 'Bank', 'kepala', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000002', '0100.02', 'Kas Kecil', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000040', '20301', 'Uang Muka Penjualan', 'anak', 'aktif', 'P000002', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000010', '10303', 'Penerimaan Barang', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000011', '10304', 'Uang Muka Pembelian', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000036', '20204', 'PPH 23 - Terutang', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000056', '60201', 'Biaya Administrasi Umum - Gaji dan Upah', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000078', '60223', 'Biaya Administrasi Umum - Lainnya', 'anak', 'aktif', 'P000005', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000079', '70101', 'Pendapatan Lain - Pendapatan Jasa Giro', 'anak', 'aktif', 'P000006', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000080', '70102', 'Pendapatan Lain - Pendapatan Bunga', 'anak', 'aktif', 'P000006', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000081', '70201', 'Laba Selisih Kurs', 'anak', 'aktif', 'P000006', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000082', '80000', 'Beban Lain - Bunga Pinjaman Pihak Ketiga', 'anak', 'aktif', 'P000007', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000083', '80102', 'Biaya Bunga', 'anak', 'aktif', 'P000007', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000084', '80103', 'Beban Lain - Beban Lainnya', 'anak', 'aktif', 'P000007', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000085', '80201', 'Untung/Rugi Atas Penjualan Asset', 'anak', 'aktif', 'P000007', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000089', '11111', 'testings', 'kepala', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000090', '111', 'Cek COA', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000093', '0100', 'KAS', 'kepala', 'aktif', 'P000001', 0, 'f');
INSERT INTO "public"."tbl_coa" VALUES ('P000001', '0100.01', 'Kas Besar', 'anak', 'aktif', 'P000001', 0, 't');
INSERT INTO "public"."tbl_coa" VALUES ('P000086', '10103.01', 'Bank Central Asia', 'anak', 'aktif', 'P000001', 0, 'f');
INSERT INTO "public"."tbl_coa" VALUES ('P000087', '10103.02', 'Bank NISP', 'anak', 'aktif', 'P000001', 0, 'f');

-- ----------------------------
-- Table structure for tbl_coa_saldo_awal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_coa_saldo_awal";
CREATE TABLE "public"."tbl_coa_saldo_awal" (
  "IDCOASaldoAwal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCOASaldoAwal_seq"'::regclass),
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDCoa_seq"'::regclass),
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_coa_saldo_awal_IDGroupCOA_seq"'::regclass),
  "Nilai_Saldo_Awal" float8,
  "Aktif" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" timestamp(0)
)
;

-- ----------------------------
-- Table structure for tbl_controlpanel
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_controlpanel";
CREATE TABLE "public"."tbl_controlpanel" (
  "IDCP" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama" varchar(255) COLLATE "pg_catalog"."default",
  "Link" varchar(255) COLLATE "pg_catalog"."default",
  "Urutan" float8,
  "Group" int8
)
;

-- ----------------------------
-- Records of tbl_controlpanel
-- ----------------------------
INSERT INTO "public"."tbl_controlpanel" VALUES ('2', 'Setting Data Perusahaan', 'settingperusahaan', 1, 1);
INSERT INTO "public"."tbl_controlpanel" VALUES ('3', 'Upload Logo Head', 'uploadlogohead', 2, 1);
INSERT INTO "public"."tbl_controlpanel" VALUES ('4', 'Upload Logo Text', 'uploadlogotext', 3, 1);
INSERT INTO "public"."tbl_controlpanel" VALUES ('1', 'Setting COA', 'settingcoa', 1, 2);
INSERT INTO "public"."tbl_controlpanel" VALUES ('5', 'Setting Cara Bayar', 'settingcarabayar', 2, 2);
INSERT INTO "public"."tbl_controlpanel" VALUES ('6', 'Cara Bayar -> Menu', 'settingcarabayarmenu', 3, 2);
INSERT INTO "public"."tbl_controlpanel" VALUES ('7', 'User', 'settinguser', 1, 3);
INSERT INTO "public"."tbl_controlpanel" VALUES ('8', 'Group User', 'settinggroupuser', 2, 3);
INSERT INTO "public"."tbl_controlpanel" VALUES ('9', 'Setting Font', 'settingfont', 4, 1);

-- ----------------------------
-- Table structure for tbl_corak
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_corak";
CREATE TABLE "public"."tbl_corak" (
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default",
  "Kode_Corak" varchar(20) COLLATE "pg_catalog"."default",
  "Corak" varchar(100) COLLATE "pg_catalog"."default",
  "Aktif" varchar(20) COLLATE "pg_catalog"."default",
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_customer";
CREATE TABLE "public"."tbl_customer" (
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDCustomer_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Deskripsi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_customer
-- ----------------------------
INSERT INTO "public"."tbl_customer" VALUES ('P000001', 'P000002', 'EK001', 'SASA', 'SASA', NULL, 'P000036', NULL, NULL, NULL, NULL, 'aktif', NULL);
INSERT INTO "public"."tbl_customer" VALUES ('P000002', 'P000003', NULL, 'JHJHH', 'JKJKJKJJ', '1111', 'P000001', NULL, NULL, NULL, NULL, 'aktif', 'CUSTOMER DYESTUFF');

-- ----------------------------
-- Table structure for tbl_down_payment
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_down_payment";
CREATE TABLE "public"."tbl_down_payment" (
  "IDDownPayment" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "NoDP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalDP" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CTime" date,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "Pembayaran" int8,
  "Jenis_pembayaran" varchar(64) COLLATE "pg_catalog"."default",
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default",
  "Saldo_Akhir" float8
)
;

-- ----------------------------
-- Table structure for tbl_font
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_font";
CREATE TABLE "public"."tbl_font" (
  "IDFont" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama" varchar(255) COLLATE "pg_catalog"."default",
  "Style" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_font
-- ----------------------------
INSERT INTO "public"."tbl_font" VALUES ('1', 'Times New Roman', NULL);
INSERT INTO "public"."tbl_font" VALUES ('2', 'Comic Sans MS', '');
INSERT INTO "public"."tbl_font" VALUES ('3', 'Arial', NULL);
INSERT INTO "public"."tbl_font" VALUES ('4', 'Apple Braille', NULL);
INSERT INTO "public"."tbl_font" VALUES ('5', 'Batang', NULL);
INSERT INTO "public"."tbl_font" VALUES ('6', 'Bradley Hand', NULL);
INSERT INTO "public"."tbl_font" VALUES ('7', 'Dotum', NULL);
INSERT INTO "public"."tbl_font" VALUES ('8', 'Libian SC', NULL);
INSERT INTO "public"."tbl_font" VALUES ('9', 'Zapfino', NULL);

-- ----------------------------
-- Table structure for tbl_fontsize
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_fontsize";
CREATE TABLE "public"."tbl_fontsize" (
  "IDFontSize" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Angka" int8
)
;

-- ----------------------------
-- Records of tbl_fontsize
-- ----------------------------
INSERT INTO "public"."tbl_fontsize" VALUES ('1', 8);
INSERT INTO "public"."tbl_fontsize" VALUES ('2', 10);
INSERT INTO "public"."tbl_fontsize" VALUES ('3', 12);
INSERT INTO "public"."tbl_fontsize" VALUES ('4', 14);
INSERT INTO "public"."tbl_fontsize" VALUES ('5', 16);
INSERT INTO "public"."tbl_fontsize" VALUES ('6', 18);
INSERT INTO "public"."tbl_fontsize" VALUES ('7', 20);

-- ----------------------------
-- Table structure for tbl_fontstyle
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_fontstyle";
CREATE TABLE "public"."tbl_fontstyle" (
  "IDStyle" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Style" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_fontstyle
-- ----------------------------
INSERT INTO "public"."tbl_fontstyle" VALUES ('1', 'Reguler');
INSERT INTO "public"."tbl_fontstyle" VALUES ('2', 'Italic');
INSERT INTO "public"."tbl_fontstyle" VALUES ('3', 'Underline');
INSERT INTO "public"."tbl_fontstyle" VALUES ('4', 'Bold');

-- ----------------------------
-- Table structure for tbl_formula
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_formula";
CREATE TABLE "public"."tbl_formula" (
  "IDFormula" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama_Formula" varchar(255) COLLATE "pg_catalog"."default",
  "Total" int8,
  "Tanggal" date,
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" date,
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" date
)
;

-- ----------------------------
-- Records of tbl_formula
-- ----------------------------
INSERT INTO "public"."tbl_formula" VALUES ('P000001', 'TESTING TOTAL', 35, '2020-02-11', '1', '2020-02-11', '1', '2020-02-26');

-- ----------------------------
-- Table structure for tbl_formula_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_formula_detail";
CREATE TABLE "public"."tbl_formula_detail" (
  "IDFormulaDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDFormula" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDBarang" varchar(255) COLLATE "pg_catalog"."default",
  "Berat" int8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_formula_detail
-- ----------------------------
INSERT INTO "public"."tbl_formula_detail" VALUES ('P000001', 'P000001', 'P000328', 15, 'P000006');
INSERT INTO "public"."tbl_formula_detail" VALUES ('P000002', 'P000001', 'P000015', 20, 'P000001');

-- ----------------------------
-- Table structure for tbl_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_giro";
CREATE TABLE "public"."tbl_giro" (
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDGiro_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDFaktur_seq"'::regclass),
  "IDPerusahaan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_giro_IDPerusahaan_seq"'::regclass),
  "Jenis_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBank" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_faktur" date,
  "Tanggal_cair" date,
  "Nilai" int8,
  "Status_giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_giro" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date,
  "Nomor_rekening" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_giro
-- ----------------------------
INSERT INTO "public"."tbl_giro" VALUES ('5eaa3173caf3c', 'P000004', '29', 'PH', '0-00004/PH/IV/20', '-', NULL, NULL, '2020-04-30', 500000, 'belum cair', 'giro keluar', NULL, NULL);
INSERT INTO "public"."tbl_giro" VALUES ('5eaa3173cb643', 'P000004', '29', 'PH', '0-00004/PH/IV/20', 'P000086', '32843472234', '2020-05-28', '2020-04-30', 500000, 'Cair', 'Keluar', NULL, '2123456');
INSERT INTO "public"."tbl_giro" VALUES ('5ecf2b149fc10', 'P000012', '28', 'PH', '0-00012/PH/V/20', 'P000086', '12345', '2020-05-28', '2020-05-28', 100000, 'Cair', 'Keluar', NULL, '2123456');
INSERT INTO "public"."tbl_giro" VALUES ('5ecf2fd039883', 'P000014', '28', 'PH', '0-00014/PH/V/20', 'P000086', '123', '2020-05-30', '2020-05-28', 10000, 'Cair', 'Keluar', NULL, '2123456');
INSERT INTO "public"."tbl_giro" VALUES ('5ecf2f8c3141f', 'P000013', '28', 'PH', '0-00013/PH/V/20', 'P000086', '567', '2020-05-30', '2020-05-28', 20000, 'Nomor_baru', 'Keluar', NULL, '2123456');
INSERT INTO "public"."tbl_giro" VALUES ('5efc01dc54954', '5efc01dc3de07', 'P000001', 'FJ', '0-0004/FJ/VII/20', NULL, 'GR001982', '2020-07-01', NULL, 55000, NULL, NULL, '2020-07-31', NULL);

-- ----------------------------
-- Table structure for tbl_group_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_asset";
CREATE TABLE "public"."tbl_group_asset" (
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_asset_IDGroupAsset_seq"'::regclass),
  "Kode_Group_Asset" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Asset" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tarif_Penyusutan" float8,
  "Umur" int2,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default",
  "Metode_Penyusutan" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_group_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_barang";
CREATE TABLE "public"."tbl_group_barang" (
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_barang_IDGroupBarang_seq"'::regclass),
  "Kode_Group_Barang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Barang" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Kode" varchar(53) COLLATE "pg_catalog"."default",
  "Cetak_Kode" varchar(255) COLLATE "pg_catalog"."default",
  "Pemisah_Kode" varchar(53) COLLATE "pg_catalog"."default",
  "Panjang_Nama" varchar(53) COLLATE "pg_catalog"."default",
  "Cetak_Nama" varchar(255) COLLATE "pg_catalog"."default",
  "Pemisah_Nama" varchar(53) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_group_barang
-- ----------------------------
INSERT INTO "public"."tbl_group_barang" VALUES ('1', 'G01', 'Bakterisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('2', 'G02', 'Benih', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('3', 'G03', 'Empang', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('4', 'G04', 'Fungisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('5', 'G05', 'Herbisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('6', 'G06', 'Insektisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('7', 'G07', 'Insektisida/Akarisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('8', 'G08', 'lainnya', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('9', 'G09', 'Moluskisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('10', 'G10', 'Pakan', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('11', 'G11', 'Pelekat', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('12', 'G12', 'Plastik', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('13', 'G13', 'Pupuk', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('14', 'G14', 'Rodentisida', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('15', 'G15', 'Sprayer', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_group_barang" VALUES ('16', 'G16', 'ZPT', 'aktif', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_group_coa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_coa";
CREATE TABLE "public"."tbl_group_coa" (
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_coa_IDGroupCOA_seq"'::regclass),
  "Nama_Group" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Normal_Balance" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kode_Group_COA" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_coa
-- ----------------------------
INSERT INTO "public"."tbl_group_coa" VALUES ('P000001', 'AKTIVA', 'debet', 'aktif', '10000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000002', 'HUTANG', 'kredit', 'aktif', '20000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000003', 'MODAL', 'kredit', 'aktif', '30000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000004', 'PENJUALAN', 'kredit', 'aktif', '40000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000005', 'BEBAN', 'debet', 'aktif', '60000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000006', 'PENDAPATAN', 'kredit', 'aktif', '70000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000007', 'BEBAN LAINNYA', 'debet', 'aktif', '80000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000008', 'KEWAJIBAN', 'kredit', 'aktif', '90000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000009', 'BIAYA', 'debet', 'aktif', '100000');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000010', 'JUDUL', 'debet', 'aktif', '10');
INSERT INTO "public"."tbl_group_coa" VALUES ('P000011', 'TEST', 'debet', 'aktif', '19');

-- ----------------------------
-- Table structure for tbl_group_coa_copy1
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_coa_copy1";
CREATE TABLE "public"."tbl_group_coa_copy1" (
  "IDGroupCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_coa_IDGroupCOA_seq"'::regclass),
  "Nama_Group" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Normal_Balance" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kode_Group_COA" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_group_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_customer";
CREATE TABLE "public"."tbl_group_customer" (
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_customer_IDGroupCustomer_seq"'::regclass),
  "Kode_Group_Customer" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Group_Customer" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_customer
-- ----------------------------
INSERT INTO "public"."tbl_group_customer" VALUES ('P000001', 'CS', 'UMUM', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_supplier";
CREATE TABLE "public"."tbl_group_supplier" (
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_group_supplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Group_Supplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_Supplier" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_group_supplier
-- ----------------------------
INSERT INTO "public"."tbl_group_supplier" VALUES ('P000001', 'UM', 'UMUM', 'aktif');

-- ----------------------------
-- Table structure for tbl_group_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_group_user";
CREATE TABLE "public"."tbl_group_user" (
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_group_user_IDGroupUser_seq"'::regclass),
  "Kode_Group_User" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group_User" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tambah" varchar(1) COLLATE "pg_catalog"."default",
  "Edit" varchar(1) COLLATE "pg_catalog"."default",
  "Batal" varchar(1) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_group_user
-- ----------------------------
INSERT INTO "public"."tbl_group_user" VALUES (5, '123', 'DEVELOPER', 'Y', 'Y', 'Y');
INSERT INTO "public"."tbl_group_user" VALUES (8, NULL, 'ADMIN INDOWARNA', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_gudang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_gudang";
CREATE TABLE "public"."tbl_gudang" (
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_gudang_IDGudang_seq"'::regclass),
  "Kode_Gudang" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_Gudang" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_gudang
-- ----------------------------
INSERT INTO "public"."tbl_gudang" VALUES ('P000001', 'BJ', 'GUDANG BARANG JADI', 'Aktif');
INSERT INTO "public"."tbl_gudang" VALUES ('P000002', 'GR', 'GUDANG RETUR', 'Aktif');

-- ----------------------------
-- Table structure for tbl_harga_jual_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_harga_jual_barang";
CREATE TABLE "public"."tbl_harga_jual_barang" (
  "IDHargaJual" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDHargaJual_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDBarang_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDSatuan_seq"'::regclass),
  "IDGroupCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_harga_jual_barang_IDGroupCustomer_seq"'::regclass),
  "Modal" numeric(100) DEFAULT NULL::numeric,
  "Harga_Jual" numeric(100) DEFAULT NULL::numeric,
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Status" varchar(64) COLLATE "pg_catalog"."default",
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_harga_jual_barang
-- ----------------------------
INSERT INTO "public"."tbl_harga_jual_barang" VALUES ('5efa92663df21', '7', '2', 'P000001', 100000, 0, '4', '2020-06-30 01:16:22', NULL, NULL, 'aktif', 'P000002');
INSERT INTO "public"."tbl_harga_jual_barang" VALUES ('5ef9bdc2aea0e', '3', '1', 'P000001', 3000, 5000, '4', '2020-06-29 10:09:06', '4', '2020-06-30 02:27:01', 'aktif', 'P000002');

-- ----------------------------
-- Table structure for tbl_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_hutang";
CREATE TABLE "public"."tbl_hutang" (
  "Tanggal_Hutang" date,
  "Jatuh_Tempo" date,
  "No_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDSupplier_seq"'::regclass),
  "Jenis_Faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Hutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDHutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_hutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0,
  "Batal" varchar(53) COLLATE "pg_catalog"."default",
  "is_paid" varchar(1) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(6),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(6),
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "no_inv" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_hutang
-- ----------------------------
INSERT INTO "public"."tbl_hutang" VALUES ('2020-06-30', NULL, '1-0013/FB/VI/20', '30', 'INV', 300000, 300000, 300000, 0, '5efaa2f5cb2db', '5efaa2f5c5cea', 0, 0, 0, 0, 0, 0, 'aktif', 't', NULL, NULL, NULL, NULL, 'P000002', 'INV-001');

-- ----------------------------
-- Table structure for tbl_idr
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_idr";
CREATE TABLE "public"."tbl_idr" (
  "IDMU" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama" varchar(64) COLLATE "pg_catalog"."default",
  "Negara" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_idr
-- ----------------------------
INSERT INTO "public"."tbl_idr" VALUES ('P000001', 'IDR', NULL);
INSERT INTO "public"."tbl_idr" VALUES ('P000002', 'USD', NULL);
INSERT INTO "public"."tbl_idr" VALUES ('P000003', 'IDR', NULL);
INSERT INTO "public"."tbl_idr" VALUES ('P000004', 'IDR', NULL);

-- ----------------------------
-- Table structure for tbl_in
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_in";
CREATE TABLE "public"."tbl_in" (
  "IDIn" int8 NOT NULL DEFAULT nextval('"tbl_in_IDIn_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDMerk_seq"'::regclass),
  "Panjang_yard" float8,
  "Panjang_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_in_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman";
CREATE TABLE "public"."tbl_instruksi_pengiriman" (
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDIP_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_kirim" date,
  "Total_yard" float8,
  "Total_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_instruksi_pengiriman_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_instruksi_pengiriman_detail";
CREATE TABLE "public"."tbl_instruksi_pengiriman_detail" (
  "IDIPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIPDetail_seq"'::regclass),
  "IDIP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDIP_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_instruksi_pengiriman_detail_IDSatuan_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_jurnal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal";
CREATE TABLE "public"."tbl_jurnal" (
  "IDJurnal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Total_debet" float8,
  "Total_kredit" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Saldo" float8,
  "Batal" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 0,
  "IDCOAAnak" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_jurnal
-- ----------------------------
INSERT INTO "public"."tbl_jurnal" VALUES ('P000001', '2020-06-23', '1-0001/FB/VI/20', '5ef1c07ddecfd', '-', 'FB', 'P000009', 40000, 0, 'P000002', 1, 40000, 0, 'Invoice Pembelian Nomor 1-0001/FB/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000002', '2020-06-23', '1-0001/FB/VI/20', '5ef1c07ddecfd', '-', 'FB', 'P000030', 0, 40000, 'P000002', 1, 0, 40000, 'Invoice Pembelian Nomor 1-0001/FB/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000003', '2020-06-23', '1-0002/FB/VI/20', '5ef1cd9cbc2c1', '-', 'FB', 'P000009', 50000, 0, 'P000002', 1, 50000, 0, 'Invoice Pembelian Nomor 1-0002/FB/VI/20', 50000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000004', '2020-06-23', '1-0002/FB/VI/20', '5ef1cd9cbc2c1', '-', 'FB', 'P000030', 0, 50000, 'P000002', 1, 0, 50000, 'Invoice Pembelian Nomor 1-0002/FB/VI/20', 50000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000005', '2020-06-23', '0001/PH/VI/20', '5ef1d0720e765', '-', 'PH', 'P000030', 40000, 0, '1', 1, 40000, 0, 'Pembayaran Hutang Nomor 0001/PH/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000006', '2020-06-23', '0001/PH/VI/20', '5ef1d0720e765', '5ef1d07210e76', 'PH', 'P000002', 0, 40000, 'P000002', 1, 0, 40000, 'Pembayaran Hutang Nomor 0001/PH/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000007', '2020-06-24', '1-0003/FB/VI/20', '5ef2cb34517d9', '-', 'FB', 'P000009', 20000, 0, 'P000002', 1, 20000, 0, 'Invoice Pembelian Nomor 1-0003/FB/VI/20', 20000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000008', '2020-06-24', '1-0003/FB/VI/20', '5ef2cb34517d9', '-', 'FB', 'P000030', 0, 20000, 'P000002', 1, 0, 20000, 'Invoice Pembelian Nomor 1-0003/FB/VI/20', 20000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000009', '2020-06-24', '0-0004/FB/VI/20', '5ef2cb34517d9', '-', 'FB', 'P000009', 19800, 0, 'P000002', 1, 19800, 0, 'Invoice Pembelian Nomor 0-0004/FB/VI/20', 19800, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000010', '2020-06-24', '0-0004/FB/VI/20', '5ef2cb34517d9', '-', 'FB', 'P000030', 0, 19800, 'P000002', 1, 0, 19800, 'Invoice Pembelian Nomor 0-0004/FB/VI/20', 19800, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000011', '2020-06-24', '0-0004/FB/VI/20', '5ef2cb34517d9', '-', 'FB', 'P000013', 0, 0, 'P000002', 1, 0, 0, 'PPN Invoice Pembelian Nomor 0-0004/FB/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000012', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000004', 20000, 0, 'P000002', 1, 20000, 0, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 20000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000013', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000048', 0, 20000, 'P000002', 1, 0, 20000, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 20000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000014', '2020-06-25', '1-0004/FB/VI/20', '5ef4016f25ad5', '-', 'FB', 'P000009', 300000, 0, 'P000002', 1, 300000, 0, 'Invoice Pembelian Nomor 1-0004/FB/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000015', '2020-06-25', '1-0004/FB/VI/20', '5ef4016f25ad5', '-', 'FB', 'P000030', 0, 300000, 'P000002', 1, 0, 300000, 'Invoice Pembelian Nomor 1-0004/FB/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000016', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 100000, 0, 'P000002', 1, 100000, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 100000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000017', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 100000, 'P000002', 1, 0, 100000, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 100000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000018', '2020-06-25', '0-0003/FJ/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 11000, 0, 'P000002', 1, 11000, 0, 'Invoice Penjualan No. 0-0003/FJ/VI/20', 11000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000019', '2020-06-25', '0-0003/FJ/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 11000, 'P000002', 1, 0, 11000, 'Invoice Penjualan No. 0-0003/FJ/VI/20', 11000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000020', '2020-06-25', '0-0003/FJ/VI/20', '5ef4255198491', '-', 'FJ', 'P000033', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 0-0003/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000021', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 8500000, 0, 'P000002', 1, 8500000, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 8500000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000022', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 8500000, 'P000002', 1, 0, 8500000, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 8500000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000023', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000004', 30020000, 0, 'P000002', 1, 30020000, 0, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 30020000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000024', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000048', 0, 30020000, 'P000002', 1, 0, 30020000, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 30020000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000025', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 40000, 0, 'P000002', 1, 40000, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000026', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 40000, 'P000002', 1, 0, 40000, 'Invoice Penjualan No. 1-0005/FB/VI/20', 40000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000027', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000028', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000029', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000004', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000030', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000048', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000031', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000004', 30060000, 0, 'P000002', 1, 30060000, 0, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 30060000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000032', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '-', 'FJ', 'P000048', 0, 30060000, 'P000002', 1, 0, 30060000, 'Invoice Penjualan No. 1-0001/FJ/VI/20', 30060000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000033', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 9600000, 0, 'P000002', 1, 9600000, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 9600000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000034', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 9600000, 'P000002', 1, 0, 9600000, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 9600000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000035', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 900000, 0, 'P000002', 1, 900000, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 900000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000036', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 900000, 'P000002', 1, 0, 900000, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 900000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000037', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000003', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000038', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000039', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000040', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000041', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000004', 85600000, 0, 'P000002', 1, 85600000, 0, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 85600000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000042', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '-', 'FJ', 'P000048', 0, 85600000, 'P000002', 1, 0, 85600000, 'Invoice Penjualan No. 1-0002/FJ/VI/20', 85600000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000043', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 310000, 0, 'P000002', 1, 310000, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 310000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000044', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 310000, 'P000002', 1, 0, 310000, 'Invoice Penjualan No. 1-0005/FB/VI/20', 310000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000045', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 2200000, 0, 'P000002', 1, 2200000, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 2200000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000046', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 2200000, 'P000002', 1, 0, 2200000, 'Invoice Penjualan No. 1-0005/FB/VI/20', 2200000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000047', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000004', 4900000, 0, 'P000002', 1, 4900000, 0, 'Invoice Penjualan No. 1-0005/FB/VI/20', 4900000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000048', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '-', 'FJ', 'P000048', 0, 4900000, 'P000002', 1, 0, 4900000, 'Invoice Penjualan No. 1-0005/FB/VI/20', 4900000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000049', '2020-06-29', '1-0005/FB/VI/20', '5ef9b00e98079', '-', 'FB', 'P000009', 150000000, 0, 'P000002', 1, 150000000, 0, 'Invoice Pembelian Nomor 1-0005/FB/VI/20', 150000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000050', '2020-06-29', '1-0005/FB/VI/20', '5ef9b00e98079', '-', 'FB', 'P000030', 0, 150000000, 'P000002', 1, 0, 150000000, 'Invoice Pembelian Nomor 1-0005/FB/VI/20', 150000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000051', '2020-06-29', '1-0006/FB/VI/20', '5ef9b5c092cce', '-', 'FB', 'P000009', 150000000, 0, 'P000002', 1, 150000000, 0, 'Invoice Pembelian Nomor 1-0006/FB/VI/20', 150000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000052', '2020-06-29', '1-0006/FB/VI/20', '5ef9b5c092cce', '-', 'FB', 'P000030', 0, 150000000, 'P000002', 1, 0, 150000000, 'Invoice Pembelian Nomor 1-0006/FB/VI/20', 150000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000053', '2020-06-29', '1-0007/FB/VI/20', '5ef9b9066f87b', '-', 'FB', 'P000009', 160000000, 0, 'P000002', 1, 160000000, 0, 'Invoice Pembelian Nomor 1-0007/FB/VI/20', 160000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000054', '2020-06-29', '1-0007/FB/VI/20', '5ef9b9066f87b', '-', 'FB', 'P000030', 0, 160000000, 'P000002', 1, 0, 160000000, 'Invoice Pembelian Nomor 1-0007/FB/VI/20', 160000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000055', '2020-06-29', '1-0008/FB/VI/20', '5ef9b9b29aa67', '-', 'FB', 'P000009', 780000000, 0, 'P000002', 1, 780000000, 0, 'Invoice Pembelian Nomor 1-0008/FB/VI/20', 780000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000056', '2020-06-29', '1-0008/FB/VI/20', '5ef9b9b29aa67', '-', 'FB', 'P000030', 0, 780000000, 'P000002', 1, 0, 780000000, 'Invoice Pembelian Nomor 1-0008/FB/VI/20', 780000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000057', '2020-06-29', '1-0009/FB/VI/20', '5ef9bbce4e0a9', '-', 'FB', 'P000009', 3000000, 0, 'P000002', 1, 3000000, 0, 'Invoice Pembelian Nomor 1-0009/FB/VI/20', 3000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000058', '2020-06-29', '1-0009/FB/VI/20', '5ef9bbce4e0a9', '-', 'FB', 'P000030', 0, 3000000, 'P000002', 1, 0, 3000000, 'Invoice Pembelian Nomor 1-0009/FB/VI/20', 3000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000059', '2020-06-29', '1-0010/FB/VI/20', '5ef9bd48eca62', '-', 'FB', 'P000009', 348000, 0, 'P000002', 1, 348000, 0, 'Invoice Pembelian Nomor 1-0010/FB/VI/20', 348000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000060', '2020-06-29', '1-0010/FB/VI/20', '5ef9bd48eca62', '-', 'FB', 'P000030', 0, 348000, 'P000002', 1, 0, 348000, 'Invoice Pembelian Nomor 1-0010/FB/VI/20', 348000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000061', '2020-06-29', '1-0011/FB/VI/20', '5ef9bdc2aceb5', '-', 'FB', 'P000009', 1530000, 0, 'P000002', 1, 1530000, 0, 'Invoice Pembelian Nomor 1-0011/FB/VI/20', 1530000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000062', '2020-06-29', '1-0011/FB/VI/20', '5ef9bdc2aceb5', '-', 'FB', 'P000030', 0, 1530000, 'P000002', 1, 0, 1530000, 'Invoice Pembelian Nomor 1-0011/FB/VI/20', 1530000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000063', '2020-06-30', '0-0012/FB/VI/20', '5efa92663c3c9', '-', 'FB', 'P000009', 11000000, 0, 'P000002', 1, 11000000, 0, 'Invoice Pembelian Nomor 0-0012/FB/VI/20', 11000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000064', '2020-06-30', '0-0012/FB/VI/20', '5efa92663c3c9', '-', 'FB', 'P000030', 0, 11000000, 'P000002', 1, 0, 11000000, 'Invoice Pembelian Nomor 0-0012/FB/VI/20', 11000000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000065', '2020-06-30', '0-0012/FB/VI/20', '5efa92663c3c9', '-', 'FB', 'P000013', 0, 0, 'P000002', 1, 0, 0, 'PPN Invoice Pembelian Nomor 0-0012/FB/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000066', '2020-06-29', '0-0013/FB/VI/20', '5ef9bbce4e0a9', '-', 'FB', 'P000009', 3300000, 0, 'P000002', 1, 3300000, 0, 'Invoice Pembelian Nomor 0-0013/FB/VI/20', 3300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000067', '2020-06-29', '0-0013/FB/VI/20', '5ef9bbce4e0a9', '-', 'FB', 'P000030', 0, 3300000, 'P000002', 1, 0, 3300000, 'Invoice Pembelian Nomor 0-0013/FB/VI/20', 3300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000068', '2020-06-29', '0-0013/FB/VI/20', '5ef9bbce4e0a9', '-', 'FB', 'P000013', 0, 0, 'P000002', 1, 0, 0, 'PPN Invoice Pembelian Nomor 0-0013/FB/VI/20', 0, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000069', '2020-06-30', '1-0013/FB/VI/20', '5efaa2f5c5cea', '-', 'FB', 'P000009', 300000, 0, 'P000002', 1, 300000, 0, 'Invoice Pembelian Nomor 1-0013/FB/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000070', '2020-06-30', '1-0013/FB/VI/20', '5efaa2f5c5cea', '-', 'FB', 'P000030', 0, 300000, 'P000002', 1, 0, 300000, 'Invoice Pembelian Nomor 1-0013/FB/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000071', '2020-06-30', '0002/PH/VI/20', '5efaa57306828', '-', 'PH', 'P000030', 300000, 0, 'P000002', 1, 300000, 0, 'Pembayaran Hutang Nomor 0002/PH/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000072', '2020-06-30', '0002/PH/VI/20', '5efaa57306828', '5efaa5730a2c1', 'PH', 'P000001', 0, 300000, 'P000002', 1, 0, 300000, 'Pembayaran Hutang Nomor 0002/PH/VI/20', 300000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000073', '2020-07-01', '0-0004/FJ/VII/20', '5efc01dc3de07', '-', 'FJ', 'P000003', 55000, 0, 'P000002', 1, 55000, 0, 'Invoice Penjualan No. 0-0004/FJ/VII/20', 55000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000074', '2020-07-01', '0-0004/FJ/VII/20', '5efc01dc3de07', '-', 'FJ', 'P000048', 0, 55000, 'P000002', 1, 0, 55000, 'Invoice Penjualan No. 0-0004/FJ/VII/20', 55000, '0', NULL);
INSERT INTO "public"."tbl_jurnal" VALUES ('P000075', '2020-07-01', '0-0004/FJ/VII/20', '5efc01dc3de07', '-', 'FJ', 'P000033', 0, 0, 'P000002', 1, 0, 0, 'Invoice Penjualan No. 0-0004/FJ/VII/20', 0, '0', NULL);

-- ----------------------------
-- Table structure for tbl_jurnal_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum";
CREATE TABLE "public"."tbl_jurnal_umum" (
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT 0,
  "Total_debit" float8,
  "Total_kredit" float8,
  "dibuat_pada" timestamp(0),
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" timestamp(0),
  "diubah_oleh" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_jurnal_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_jurnal_umum_detail";
CREATE TABLE "public"."tbl_jurnal_umum_detail" (
  "IDJUDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDJU" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Posisi" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Debet" float8,
  "Kredit" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Urutan" int2
)
;

-- ----------------------------
-- Table structure for tbl_kartu_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kartu_stok";
CREATE TABLE "public"."tbl_kartu_stok" (
  "IDKartuStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDKartuStok_seq"'::regclass),
  "Tanggal" date,
  "Nomor_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDFakturDetail_seq"'::regclass),
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDStok_seq"'::regclass),
  "Jenis_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDBarang_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDGudang_seq"'::regclass),
  "Masuk_yard" float8,
  "Masuk_meter" float8,
  "Keluar_yard" float8,
  "Keluar_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kartu_stok_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT nextval('"tbl_kartu_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "Masuk_pcs" float8,
  "Keluar_pcs" float8,
  "Nama_Barang" varchar(255) COLLATE "pg_catalog"."default",
  "Berat" float8
)
;

-- ----------------------------
-- Records of tbl_kartu_stok
-- ----------------------------
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef1c07e57cef', '2020-06-23', '1-0001/FB/VI/20', '5ef1c07ddecfd', '5ef1c07def2b9', '5ef1c07e51b45', 'FB', NULL, '1', '711', NULL, NULL, NULL, NULL, NULL, '2', 40000, '710', NULL, NULL, 1, NULL, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef1cd9cc10e2', '2020-06-23', '1-0002/FB/VI/20', '5ef1cd9cbc2c1', '5ef1cd9cbda31', '5ef1cd9cc0cfa', 'FB', NULL, '5', '715', NULL, NULL, NULL, NULL, NULL, '2', 50000, '714', NULL, NULL, 1, NULL, 'Supermes E 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef2cb3492ae0', '2020-06-24', '1-0003/FB/VI/20', '5ef2cb34517d9', '5ef2cb3452b61', '5ef1c07e51b45', 'FB', NULL, '1', '716', NULL, NULL, NULL, NULL, NULL, '2', 20000, '715', NULL, NULL, 1, NULL, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef2cde74b83b', '2020-06-24', '0-0004/FB/VI/20', '5ef2cb34517d9', '5ef2cde748d42', '5ef1c07e51b45', 'FB', NULL, '1', '718', NULL, NULL, NULL, NULL, NULL, '2', 20000, '717', NULL, NULL, 1, NULL, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef3ff8249468', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '5ef3ff823f43d', '5ef3ff824907f', 'SJ', NULL, '1', '719', NULL, NULL, NULL, NULL, NULL, '2', 20000, '718', NULL, NULL, NULL, 1, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef4016f521d0', '2020-06-25', '1-0004/FB/VI/20', '5ef4016f25ad5', '5ef4016f26a75', '5ef1c07e51b45', 'FB', NULL, '1', '720', NULL, NULL, NULL, NULL, NULL, '2', 300000, '719', NULL, NULL, 1, NULL, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef403f3265e8', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef403f324a90', '5ef403f325e18', 'SJ', NULL, '3', '721', NULL, NULL, NULL, NULL, NULL, '1', 100000, '720', NULL, NULL, NULL, 1, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef425519aba2', '2020-06-25', '0-0003/FJ/VI/20', '5ef4255198491', '5ef4255199431', '5ef425519a7ba', 'SJ', NULL, '6', '723', NULL, NULL, NULL, NULL, NULL, '2', 10000, '722', NULL, NULL, NULL, 1, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef4348783770', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef4348781448', '5ef3ff824907f', 'SJ', NULL, '1', '724', NULL, NULL, NULL, NULL, NULL, '2', 400000, '723', NULL, NULL, NULL, 21, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef434b542eb0', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '5ef434b540f70', '5ef425519a7ba', 'SJ', NULL, '6', '725', NULL, NULL, NULL, NULL, NULL, '2', 30000, '724', NULL, NULL, NULL, 1000, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef435f075f35', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '5ef435f073825', '5ef435f075b4d', 'SJ', NULL, '7', '726', NULL, NULL, NULL, NULL, NULL, '2', 30000, '725', NULL, NULL, NULL, 1, 'Togatsu 300ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef437345906a', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '5ef4373457512', '5ef425519a7ba', 'SJ', NULL, '6', '727', NULL, NULL, NULL, NULL, NULL, '2', 10000, '726', NULL, NULL, NULL, 1, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef43bd00cf84', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '5ef43bd00b42c', '5ef3ff824907f', 'SJ', NULL, '1', '728', NULL, NULL, NULL, NULL, NULL, '2', 20000, '727', NULL, NULL, NULL, 1, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef43f400ccea', '2020-06-25', '1-0001/FJ/VI/20', '5ef3ff823e49d', '5ef43f400a9c1', '5ef3ff824907f', 'SJ', NULL, '1', '729', NULL, NULL, NULL, NULL, NULL, '2', 20000, '728', NULL, NULL, NULL, 3, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef43f5ba4cdd', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef43f5ba3185', '5ef3ff824907f', 'SJ', NULL, '1', '730', NULL, NULL, NULL, NULL, NULL, '2', 400000, '729', NULL, NULL, NULL, 21, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef44066cce96', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef44066cb33e', '5ef3ff824907f', 'SJ', NULL, '1', '731', NULL, NULL, NULL, NULL, NULL, '2', 400000, '730', NULL, NULL, NULL, 2, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef4409d13a25', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef4409d11ae4', '5ef3ff824907f', 'SJ', NULL, '1', '732', NULL, NULL, NULL, NULL, NULL, '2', 400000, '731', NULL, NULL, NULL, 21, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef4444a91930', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef4444a8f9f0', '5ef3ff824907f', 'SJ', NULL, '1', '733', NULL, NULL, NULL, NULL, NULL, '2', 400000, '732', NULL, NULL, NULL, 21, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef444d579641', '2020-06-25', '1-0002/FJ/VI/20', '5ef403f323707', '5ef444d576760', '5ef3ff824907f', 'SJ', NULL, '1', '734', NULL, NULL, NULL, NULL, NULL, '2', 400000, '733', NULL, NULL, NULL, 211, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef44552aa422', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '5ef44552a80fa', '5ef425519a7ba', 'SJ', NULL, '6', '735', NULL, NULL, NULL, NULL, NULL, '2', 10000, '734', NULL, NULL, NULL, 1, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef4494759c9b', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '5ef4494757d5a', '5ef425519a7ba', 'SJ', NULL, '6', '736', NULL, NULL, NULL, NULL, NULL, '2', 10000, '735', NULL, NULL, NULL, 190, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef449762b140', '2020-06-25', '1-0005/FB/VI/20', '5ef4255198491', '5ef4497629200', '5ef435f075b4d', 'SJ', NULL, '7', '737', NULL, NULL, NULL, NULL, NULL, '2', 30000, '736', NULL, NULL, NULL, 100, 'Togatsu 300ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9b00f22d81', '2020-06-29', '1-0005/FB/VI/20', '5ef9b00e98079', '5ef9b00eb9997', '5ef9b00f1b850', 'FB', NULL, '3', '738', NULL, NULL, NULL, NULL, NULL, '1', 50000, '737', NULL, NULL, 3000, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9b5c097aee', '2020-06-29', '1-0006/FB/VI/20', '5ef9b5c092cce', '5ef9b5c094056', '5ef9b5c09712a', 'FB', NULL, '2', '739', NULL, NULL, NULL, NULL, NULL, '1', 50000, '738', NULL, NULL, 3000, NULL, 'Linex 500gr', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9b9068e0c5', '2020-06-29', '1-0007/FB/VI/20', '5ef9b9066f87b', '5ef9b90670c03', '5ef9b00f1b850', 'FB', NULL, '3', '740', NULL, NULL, NULL, NULL, NULL, '1', 40000, '739', NULL, NULL, 4000, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9b9b29eec3', '2020-06-29', '1-0008/FB/VI/20', '5ef9b9b29aa67', '5ef9b9b29c7b3', '5ef9b9b29e4ff', 'FB', NULL, '6', '741', NULL, NULL, NULL, NULL, NULL, '2', 130000, '740', NULL, NULL, 6000, NULL, 'Togatsu 100ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9bbce51f2a', '2020-06-29', '1-0009/FB/VI/20', '5ef9bbce4e0a9', '5ef9bbce4f049', '5ef9b00f1b850', 'FB', NULL, '3', '742', NULL, NULL, NULL, NULL, NULL, '1', 3000, '741', NULL, NULL, 1000, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9bd48f04fb', '2020-06-29', '1-0010/FB/VI/20', '5ef9bd48eca62', '5ef9bd48eda03', '5ef1c07e51b45', 'FB', NULL, '1', '743', NULL, NULL, NULL, NULL, NULL, '2', 29000, '742', NULL, NULL, 12, NULL, 'EM 4 Tambak 1lt', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5ef9bdc2b0d36', '2020-06-29', '1-0011/FB/VI/20', '5ef9bdc2aceb5', '5ef9bdc2ade56', '5ef9b00f1b850', 'FB', NULL, '3', '744', NULL, NULL, NULL, NULL, NULL, '1', 45000, '743', NULL, NULL, 34, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5efa92663fa79', '2020-06-30', '0-0012/FB/VI/20', '5efa92663c3c9', '5efa92663d369', '5efa92663f2a9', 'FB', NULL, '7', '746', NULL, NULL, NULL, NULL, NULL, '2', 100000, '745', NULL, NULL, 100, NULL, 'Togatsu 300ml', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5efa9933a983e', '2020-06-29', '0-0013/FB/VI/20', '5ef9bbce4e0a9', '5efa9933a695d', '5ef9b00f1b850', 'FB', NULL, '3', '747', NULL, NULL, NULL, NULL, NULL, '1', 3000, '746', NULL, NULL, 1000, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5efaa2f5cab0b', '2020-06-30', '1-0013/FB/VI/20', '5efaa2f5c5cea', '5efaa2f5c7072', '5ef9b00f1b850', 'FB', NULL, '3', '748', NULL, NULL, NULL, NULL, NULL, '1', 3000, '747', NULL, NULL, 100, NULL, 'Lodan 1kg', NULL);
INSERT INTO "public"."tbl_kartu_stok" VALUES ('5efc01dc414b8', '2020-07-01', '0-0004/FJ/VII/20', '5efc01dc3de07', '5efc01dc3f18f', '5ef403f325e18', 'SJ', NULL, '3', '751', NULL, NULL, NULL, NULL, NULL, '1', 50000, '750', NULL, NULL, NULL, 1, 'Lodan 1kg', NULL);

-- ----------------------------
-- Table structure for tbl_kategori
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kategori";
CREATE TABLE "public"."tbl_kategori" (
  "IDKategori" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama_Kategori" varchar(255) COLLATE "pg_catalog"."default",
  "Status" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Kode_Kategori" varchar(64) COLLATE "pg_catalog"."default",
  "IDGroupBarang" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_kategori
-- ----------------------------
INSERT INTO "public"."tbl_kategori" VALUES ('1', 'Empang', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT001', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('2', 'Fungisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT002', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('3', 'Herbisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT003', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('4', 'Insektisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT004', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('5', 'Akarisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT005', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('6', 'Moluskisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT006', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('7', 'Pakan', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT007', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('8', 'Pelekat', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT008', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('9', 'Pupuk', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT009', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('10', 'Rodentisida', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT010', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('11', 'ZPT', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT011', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('12', '-', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT012', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('13', '1 bungkus', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT013', '26');
INSERT INTO "public"."tbl_kategori" VALUES ('14', '1 bal', 'aktif', '', '2020-03-18 12:24:55', '', '2020-03-18 12:24:55', 'KT014', '26');

-- ----------------------------
-- Table structure for tbl_kode_transaksi
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kode_transaksi";
CREATE TABLE "public"."tbl_kode_transaksi" (
  "IDKode" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Kode" varchar(255) COLLATE "pg_catalog"."default",
  "IDMenuDetail" int8,
  "Jadi1" varchar(255) COLLATE "pg_catalog"."default",
  "Jadi2" varchar(255) COLLATE "pg_catalog"."default",
  "Jadi3" varchar(255) COLLATE "pg_catalog"."default",
  "Jadi4" varchar(255) COLLATE "pg_catalog"."default",
  "Pisah1" varchar(255) COLLATE "pg_catalog"."default",
  "Pisah2" varchar(255) COLLATE "pg_catalog"."default",
  "Pisah3" varchar(255) COLLATE "pg_catalog"."default",
  "Pisah4" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_kode_transaksi
-- ----------------------------
INSERT INTO "public"."tbl_kode_transaksi" VALUES ('5ea7b14f23e3a', '0001/FB/I/20', 44, '0001', 'FB', 'I', '20', NULL, '/', '/', '/');
INSERT INTO "public"."tbl_kode_transaksi" VALUES ('5ea6324ae17c9', '0001\PO\I\20', 35, '0001', 'PO', 'I', '20', NULL, '/', '/', '/');
INSERT INTO "public"."tbl_kode_transaksi" VALUES ('5ea694bd5f0e7', '0001\PB\I\20', 39, '0001', 'PB', 'I', '20', NULL, '/', '/', '/');
INSERT INTO "public"."tbl_kode_transaksi" VALUES ('5ef3fe70baa4a', '0001\FJ/I/20', 80, '0001', 'FJ', 'I', '20', NULL, '/', '/', '/');

-- ----------------------------
-- Table structure for tbl_kode_transaksi_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kode_transaksi_detail";
CREATE TABLE "public"."tbl_kode_transaksi_detail" (
  "IDKode" varchar(64) COLLATE "pg_catalog"."default",
  "IDKodeDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Asal1" varchar(64) COLLATE "pg_catalog"."default",
  "Tampil1" varchar(64) COLLATE "pg_catalog"."default",
  "Manual1" varchar(64) COLLATE "pg_catalog"."default",
  "Pemisah1" varchar(64) COLLATE "pg_catalog"."default",
  "Format1" varchar(64) COLLATE "pg_catalog"."default",
  "Jenis1" varchar(64) COLLATE "pg_catalog"."default",
  "Asal2" varchar(64) COLLATE "pg_catalog"."default",
  "Tampil2" varchar(64) COLLATE "pg_catalog"."default",
  "Manual2" varchar(64) COLLATE "pg_catalog"."default",
  "Pemisah2" varchar(64) COLLATE "pg_catalog"."default",
  "Format2" varchar(64) COLLATE "pg_catalog"."default",
  "Jenis2" varchar(64) COLLATE "pg_catalog"."default",
  "Asal3" varchar(64) COLLATE "pg_catalog"."default",
  "Tampil3" varchar(64) COLLATE "pg_catalog"."default",
  "Manual3" varchar(64) COLLATE "pg_catalog"."default",
  "Pemisah3" varchar(64) COLLATE "pg_catalog"."default",
  "Format3" varchar(64) COLLATE "pg_catalog"."default",
  "Jenis3" varchar(64) COLLATE "pg_catalog"."default",
  "Asal4" varchar(64) COLLATE "pg_catalog"."default",
  "Tampil4" varchar(64) COLLATE "pg_catalog"."default",
  "Manual4" varchar(64) COLLATE "pg_catalog"."default",
  "Pemisah4" varchar(64) COLLATE "pg_catalog"."default",
  "Format4" varchar(64) COLLATE "pg_catalog"."default",
  "Jenis4" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_kode_transaksi_detail
-- ----------------------------
INSERT INTO "public"."tbl_kode_transaksi_detail" VALUES ('5ea7b14f23e3a', '5ea7b15064ae0', '1', '1', NULL, NULL, '1', '1', '2', '1', 'FB', '/', '1', '1', '3', '1', NULL, '/', '2', '2', '4', '1', NULL, '/', '3', '1');
INSERT INTO "public"."tbl_kode_transaksi_detail" VALUES ('5ea6324ae17c9', '5ea6324b03a02', '1', '1', NULL, NULL, '1', '0', '2', '1', 'PO', '/', '0', '0', '3', '1', NULL, '/', '2', '2', '4', '1', NULL, '/', '3', '1');
INSERT INTO "public"."tbl_kode_transaksi_detail" VALUES ('5ea694bd5f0e7', '5ea694bd751b7', '1', '1', NULL, NULL, '1', '0', '2', '1', 'PB', '/', '0', '0', '3', '1', NULL, '/', '2', '2', '4', '1', NULL, '/', '3', '1');
INSERT INTO "public"."tbl_kode_transaksi_detail" VALUES ('5ef3fe70baa4a', '5ef3fe70bb21a', '1', '1', NULL, NULL, '1', '0', '2', '1', 'FJ', '/', '1', '1', '3', '1', NULL, '/', '2', '2', '4', '1', NULL, '/', '3', '1');

-- ----------------------------
-- Table structure for tbl_konversi_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_konversi_satuan";
CREATE TABLE "public"."tbl_konversi_satuan" (
  "IDKonversi" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDKonversi_seq"'::regclass),
  "IDSatuanBerat" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanBerat_seq"'::regclass),
  "Qty" float8,
  "IDSatuanKecil" int8 NOT NULL DEFAULT nextval('"tbl_konversi_satuan_IDSatuanKecil_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan";
CREATE TABLE "public"."tbl_koreksi_persediaan" (
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDKP_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_IDMataUang_seq"'::regclass),
  "Kurs" float8,
  "Jenis" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(35) COLLATE "pg_catalog"."default" DEFAULT 0,
  "dibuat_pada" timestamp(0),
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" timestamp(0),
  "diubah_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "IDGudang" varchar(30) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_koreksi_persediaan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_koreksi_persediaan_detail";
CREATE TABLE "public"."tbl_koreksi_persediaan_detail" (
  "IDKPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKPDetail_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDSatuan_seq"'::regclass),
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDBarang_seq"'::regclass),
  "IDKP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDKP_seq"'::regclass),
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_detail_IDMataUang_seq"'::regclass),
  "Qty" float8,
  "Saldo_qty" float8,
  "Harga" float8,
  "Kurs" float8
)
;

-- ----------------------------
-- Table structure for tbl_kota
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_kota";
CREATE TABLE "public"."tbl_kota" (
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_kota_IDKota_seq"'::regclass),
  "Kode_Kota" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Provinsi" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kota" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_kota
-- ----------------------------
INSERT INTO "public"."tbl_kota" VALUES ('P000010', 'JKT', 'DKI Jakarta', 'JAKARTA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000012', 'LPG', 'Lampung', 'LAMPUNG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000013', 'KRW', 'Jawa Barat', 'KARAWANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000014', 'MND', 'Sulawesi Utara', 'MANADO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000015', 'MDN', 'Sumatera Utara', 'MEDAN', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000016', 'MKSR', 'Sulawesi Selatan', 'MAKASAR', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000017', 'MLG', 'Jawa Timur', 'MALANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000018', 'PDG', 'Sumatera Barat', 'PADANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000019', 'PLB', 'Sumatera Selata', 'PALEMBANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000020', 'PKBR', 'Riau', 'PEKANBARU', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000023', 'SLO', 'Jawa Tengah', 'SOLO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000025', 'SRG', 'Banten', 'SERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000026', 'SMRD', 'Kalimantan Timur', 'SAMARINDA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000027', 'TSK', 'Jawa Barat', 'TASIKMALAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000028', 'TGR', 'Banten', 'TANGERANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000029', 'TRNT', 'Maluku Utara', 'TERNATE', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000011', 'JMB', 'Jambi', 'JAMBI', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000003', 'MJKT', 'Jawa Timur', 'MOJOKERTO', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000021', 'JAI', 'MALUKU UTARA', 'JAILOLO', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000030', 'BJG', 'JAWA TIMUR', 'BOJONEGORO', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000031', 'PTNK', 'Kalimantan Barat', 'PONTIANAK', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000032', 'SBY', 'Jawa TImur', 'SURABAYA', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000033', 'BGR', 'JAWA BARAT', 'BOGOR', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000022', 'LWG', 'Jawa Barat', 'LEUWIGAJAH', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000034', 'SDA', 'JAWA TIMUR', 'SIDOARJO', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000035', NULL, 'JAWA TENGAH', 'SEMARANG', 'aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000036', 'BJM', 'KALIMANTAN SELATAN', 'BANJARMASIN', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000037', 'YYK', 'JAWA TENGAH', 'YOGYAKARTA', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000038', '00000', 'JAWA TENGAH', 'SEMARANG', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000039', 'TSM', 'JAWA BARAT', 'TASIKMALAYA KAB', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000040', '57136', 'JAWA TENGAH', 'SURAKARTA', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000041', 'TAR', 'KALIMANTAN UTARA', 'TARAKAN', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000042', 'TAR', 'KALIMANTAN UTARA', 'TARAKAN', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000043', '67156', 'JAWA TIMUR', 'PASURUAN', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000044', '00000', 'SUMATERA UTARA', 'DELI', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000045', '0', 'DKI JAKARTA', 'JAKARTA BARAT', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000046', '00000', 'NUSA TENGGARA BARAT', 'MATARAM', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000047', 'BDO', 'JAWAB BARAT', 'BANDUNG DENG', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000001', 'BDG', 'JAWA BARAT', 'BANDUNG', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000007', 'CRB', 'JAWA BARAT', 'CIREBON', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000008', 'DPK', 'JAWA BARAT', 'DEPOK', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000009', 'GRNT', 'GORONTALO', 'GORONTALO', 'Aktif');
INSERT INTO "public"."tbl_kota" VALUES ('P000048', 'IND', 'JAWA BARAT', 'INDRAMAYU', 'Aktif');

-- ----------------------------
-- Table structure for tbl_lap_umur_persediaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_lap_umur_persediaan";
CREATE TABLE "public"."tbl_lap_umur_persediaan" (
  "IDLapPers" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDLapPers_seq"'::regclass),
  "IDBarang" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDBarang_seq"'::regclass),
  "IDCorak" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDCorak_seq"'::regclass),
  "IDMerk" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDMerk_seq"'::regclass),
  "IDWarna" int8 NOT NULL DEFAULT nextval('"tbl_lap_umur_persediaan_IDWarna_seq"'::regclass),
  "Qty_yard1" float8,
  "Qty_meter1" float8,
  "Nilai1" float8,
  "Qty_yard2" float8,
  "Qty_meter2" float8,
  "Nilai2" float8,
  "Qty_yard3" float8,
  "Qty_meter3" float8,
  "Nilai3" float8,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Total_nilai" float8
)
;

-- ----------------------------
-- Table structure for tbl_loguser
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_loguser";
CREATE TABLE "public"."tbl_loguser" (
  "id_log" int4 NOT NULL DEFAULT nextval('tbl_loguser_id_log_seq'::regclass),
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Aksi" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "Tanggal" timestamptz(0) NOT NULL
)
;

-- ----------------------------
-- Records of tbl_loguser
-- ----------------------------
INSERT INTO "public"."tbl_loguser" VALUES (2, 'P000001', 'Login', '2019-06-23 07:33:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (3, 'P000001', 'Logout', '2019-06-23 07:33:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (4, 'P000001', 'Login', '2019-06-23 07:33:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (6, 'P000001', 'Login', '2019-06-24 14:37:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (7, 'P000001', 'Login', '2019-06-27 19:32:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (9, 'P000001', 'Login', '2019-06-28 07:21:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (11, 'P000001', 'Login', '2019-06-28 20:14:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (13, 'P000001', 'Login', '2019-06-29 06:16:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (15, 'P000001', 'Login', '2019-06-29 20:52:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (17, 'P000001', 'Login', '2019-06-30 06:49:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (19, 'P000001', 'Login', '2019-06-30 10:24:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (5, 'P000001', 'Logout', '2019-06-24 14:36:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (8, 'P000001', 'Logout', '2019-06-28 07:21:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (10, 'P000001', 'Logout', '2019-06-28 20:13:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (12, 'P000001', 'Logout', '2019-06-29 06:16:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (14, 'P000001', 'Logout', '2019-06-29 20:52:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (16, 'P000001', 'Logout', '2019-06-30 06:49:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (18, 'P000001', 'Logout', '2019-06-30 10:24:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (21, '', 'Logout', '2019-07-01 13:48:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (22, 'P000001', 'Login', '2019-07-01 13:48:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (23, 'P000001', 'Login', '2019-07-02 09:31:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (24, 'P000001', 'Login', '2019-07-02 10:19:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (25, 'P000001', 'Login', '2019-07-03 08:56:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (26, 'P000001', 'Login', '2019-07-03 09:22:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (27, 'P000001', 'Login', '2019-07-03 14:05:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (28, 'P000001', 'Login', '2019-07-03 14:08:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (29, 'P000001', 'Logout', '2019-07-03 16:15:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (30, 'P000001', 'Login', '2019-07-03 16:15:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (31, 'P000001', 'Login', '2019-07-03 16:18:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (32, 'P000001', 'Login', '2019-07-03 16:20:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (33, 'P000001', 'Login', '2019-07-03 17:20:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (34, 'P000001', 'Logout', '2019-07-03 17:35:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (35, 'P000001', 'Login', '2019-07-04 08:08:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (36, 'P000001', 'Login', '2019-07-04 09:33:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (37, 'P000001', 'Login', '2019-07-04 10:09:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (38, 'P000001', 'Login', '2019-07-04 10:10:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (40, 'P000001', 'Login', '2019-07-04 16:59:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (41, 'P000001', 'Login', '2019-07-04 17:59:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (42, 'P000001', 'Login', '2019-07-05 08:32:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (43, 'P000001', 'Login', '2019-07-05 08:48:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (44, 'P000001', 'Login', '2019-07-05 09:01:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (45, 'P000001', 'Login', '2019-07-05 09:01:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (46, 'P000001', 'Login', '2019-07-05 09:01:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (47, 'P000001', 'Login', '2019-07-05 09:09:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (48, 'P000001', 'Login', '2019-07-05 09:09:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (49, 'P000001', 'Login', '2019-07-05 09:27:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (50, 'P000001', 'Login', '2019-07-05 09:28:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (51, 'P000001', 'Login', '2019-07-05 09:31:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (52, 'P000001', 'Login', '2019-07-05 09:31:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (53, 'P000001', 'Login', '2019-07-05 09:32:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (54, 'P000001', 'Login', '2019-07-05 09:33:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (55, 'P000001', 'Login', '2019-07-05 09:33:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (56, 'P000001', 'Login', '2019-07-05 09:54:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (57, 'P000001', 'Login', '2019-07-05 09:58:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (58, 'P000001', 'Login', '2019-07-05 09:59:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (59, 'P000001', 'Login', '2019-07-05 10:00:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (60, 'P000001', 'Login', '2019-07-05 10:23:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (61, 'P000001', 'Login', '2019-07-05 10:26:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (62, 'P000001', 'Login', '2019-07-05 10:26:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (63, 'P000001', 'Login', '2019-07-05 10:29:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (64, 'P000001', 'Login', '2019-07-05 10:47:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (65, 'P000001', 'Login', '2019-07-05 10:55:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (66, 'P000001', 'Login', '2019-07-05 10:57:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (67, 'P000001', 'Login', '2019-07-05 11:01:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (68, 'P000001', 'Login', '2019-07-05 11:02:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (69, 'P000001', 'Login', '2019-07-05 11:03:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (70, 'P000001', 'Login', '2019-07-05 11:05:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (71, 'P000001', 'Login', '2019-07-05 11:07:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (72, 'P000001', 'Login', '2019-07-05 11:07:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (73, 'P000001', 'Login', '2019-07-05 11:09:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (74, 'P000001', 'Login', '2019-07-05 11:11:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (75, 'P000001', 'Login', '2019-07-05 11:13:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (76, 'P000001', 'Login', '2019-07-05 13:16:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (77, 'P000001', 'Login', '2019-07-05 13:23:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (78, 'P000001', 'Login', '2019-07-05 14:57:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (79, 'P000001', 'Login', '2019-07-05 15:43:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (80, 'P000001', 'Login', '2019-07-05 15:43:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (81, 'P000001', 'Login', '2019-07-05 15:44:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (82, 'P000001', 'Login', '2019-07-05 15:44:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (83, 'P000001', 'Login', '2019-07-05 15:44:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (84, 'P000001', 'Login', '2019-07-05 16:26:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (85, 'P000001', 'Login', '2019-07-05 16:35:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (86, 'P000001', 'Login', '2019-07-05 16:40:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (87, 'P000001', 'Login', '2019-07-05 16:44:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (88, 'P000001', 'Login', '2019-07-05 16:44:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (89, 'P000001', 'Login', '2019-07-05 16:46:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (90, 'P000001', 'Login', '2019-07-05 16:47:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (91, 'P000001', 'Login', '2019-07-05 22:03:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (92, 'P000001', 'Login', '2019-07-05 22:39:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (93, 'P000001', 'Login', '2019-07-06 09:57:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (94, 'P000001', 'Login', '2019-07-08 08:20:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (95, 'P000001', 'Login', '2019-07-08 08:22:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (96, 'P000001', 'Login', '2019-07-08 08:26:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (97, 'P000001', 'Login', '2019-07-08 08:32:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (98, 'P000001', 'Login', '2019-07-08 08:40:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (99, 'P000001', 'Login', '2019-07-08 08:42:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (100, 'P000001', 'Login', '2019-07-08 08:47:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (101, 'P000001', 'Login', '2019-07-08 08:48:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (102, 'P000001', 'Login', '2019-07-08 08:50:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (103, 'P000001', 'Login', '2019-07-08 08:54:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (104, 'P000001', 'Login', '2019-07-08 08:58:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (105, 'P000001', 'Login', '2019-07-08 09:20:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (106, 'P000001', 'Login', '2019-07-08 09:20:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (107, 'P000001', 'Login', '2019-07-08 09:33:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (108, 'P000001', 'Login', '2019-07-08 09:34:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (109, 'P000001', 'Login', '2019-07-08 09:43:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (110, 'P000001', 'Login', '2019-07-08 09:48:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (111, 'P000001', 'Login', '2019-07-08 09:56:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (112, 'P000001', 'Login', '2019-07-08 10:00:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (113, 'P000001', 'Login', '2019-07-08 10:04:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (114, 'P000001', 'Login', '2019-07-08 10:05:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (115, 'P000001', 'Login', '2019-07-08 10:08:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (116, 'P000001', 'Login', '2019-07-08 10:09:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (117, 'P000001', 'Login', '2019-07-08 10:11:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (118, 'P000001', 'Login', '2019-07-08 10:18:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (119, 'P000001', 'Logout', '2019-07-08 10:25:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (120, 'P000001', 'Login', '2019-07-08 10:33:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (121, 'P000001', 'Login', '2019-07-08 10:34:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (122, 'P000001', 'Login', '2019-07-08 10:45:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (123, 'P000001', 'Login', '2019-07-08 10:46:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (124, 'P000001', 'Login', '2019-07-08 10:55:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (125, 'P000001', 'Login', '2019-07-08 10:57:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (126, 'P000001', 'Login', '2019-07-08 11:05:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (127, 'P000001', 'Login', '2019-07-08 11:11:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (128, 'P000001', 'Login', '2019-07-08 11:14:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (129, 'P000001', 'Login', '2019-07-08 11:14:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (130, 'P000001', 'Login', '2019-07-08 11:15:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (131, 'P000001', 'Login', '2019-07-08 11:15:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (132, 'P000001', 'Login', '2019-07-08 11:18:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (133, 'P000001', 'Login', '2019-07-08 11:20:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (134, 'P000001', 'Login', '2019-07-08 11:21:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (135, 'P000001', 'Login', '2019-07-08 12:59:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (136, 'P000001', 'Login', '2019-07-08 13:03:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (137, 'P000001', 'Login', '2019-07-08 13:04:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (138, 'P000001', 'Login', '2019-07-08 13:12:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (139, 'P000001', 'Login', '2019-07-08 13:12:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (140, 'P000001', 'Logout', '2019-07-08 13:13:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (141, 'P000001', 'Login', '2019-07-08 13:13:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (142, 'P000001', 'Login', '2019-07-08 13:13:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (143, 'P000001', 'Login', '2019-07-08 13:14:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (144, 'P000001', 'Login', '2019-07-08 13:14:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (145, '', 'Logout', '2019-07-08 13:17:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (146, 'P000001', 'Login', '2019-07-08 13:20:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (147, 'P000001', 'Login', '2019-07-08 13:22:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (148, 'P000001', 'Login', '2019-07-08 13:22:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (149, 'P000001', 'Login', '2019-07-08 13:23:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (150, 'P000001', 'Login', '2019-07-08 13:24:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (151, 'P000001', 'Login', '2019-07-08 13:25:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (152, '', 'Logout', '2019-07-08 13:28:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (153, 'P000001', 'Login', '2019-07-08 13:28:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (154, 'P000001', 'Login', '2019-07-08 13:30:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (155, 'P000001', 'Login', '2019-07-08 13:31:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (156, 'P000001', 'Login', '2019-07-08 13:31:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (157, 'P000001', 'Login', '2019-07-08 13:42:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (158, 'P000001', 'Login', '2019-07-08 13:46:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (159, 'P000001', 'Login', '2019-07-08 13:48:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (160, 'P000001', 'Login', '2019-07-08 13:48:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (161, 'P000001', 'Login', '2019-07-08 13:52:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (162, 'P000001', 'Login', '2019-07-08 13:55:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (163, 'P000001', 'Login', '2019-07-08 13:55:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (164, 'P000001', 'Login', '2019-07-08 13:57:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (165, 'P000001', 'Login', '2019-07-08 13:58:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (166, 'P000001', 'Login', '2019-07-08 13:59:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (167, 'P000001', 'Login', '2019-07-08 14:03:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (168, 'P000001', 'Login', '2019-07-08 14:11:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (169, 'P000001', 'Login', '2019-07-08 14:12:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (170, 'P000001', 'Login', '2019-07-08 14:26:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (171, 'P000001', 'Login', '2019-07-08 14:28:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (172, 'P000001', 'Login', '2019-07-08 14:28:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (173, 'P000001', 'Login', '2019-07-08 14:29:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (174, 'P000001', 'Login', '2019-07-08 14:30:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (175, 'P000001', 'Login', '2019-07-08 14:54:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (176, 'P000001', 'Login', '2019-07-08 14:59:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (177, 'P000001', 'Login', '2019-07-08 15:28:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (178, '', 'Logout', '2019-07-08 22:25:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (179, 'P000001', 'Login', '2019-07-08 22:25:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (180, 'P000001', 'Login', '2019-07-09 08:13:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (181, 'P000001', 'Login', '2019-07-09 08:20:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (182, 'P000001', 'Login', '2019-07-09 08:33:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (183, 'P000001', 'Login', '2019-07-09 08:54:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (184, 'P000001', 'Login', '2019-07-09 09:09:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (185, 'P000001', 'Login', '2019-07-09 09:10:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (186, 'P000001', 'Login', '2019-07-09 09:12:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (187, 'P000001', 'Login', '2019-07-09 09:36:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (188, 'P000001', 'Login', '2019-07-09 09:44:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (189, 'P000001', 'Login', '2019-07-09 09:51:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (190, 'P000001', 'Login', '2019-07-09 09:53:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (191, 'P000001', 'Login', '2019-07-09 09:54:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (192, 'P000001', 'Login', '2019-07-09 09:58:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (193, 'P000001', 'Login', '2019-07-09 10:28:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (194, 'P000001', 'Login', '2019-07-09 10:29:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (195, 'P000001', 'Login', '2019-07-09 10:29:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (196, 'P000001', 'Login', '2019-07-09 10:35:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (197, 'P000001', 'Login', '2019-07-09 10:39:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (198, 'P000001', 'Login', '2019-07-09 10:42:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (199, 'P000001', 'Login', '2019-07-09 10:51:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (200, 'P000001', 'Login', '2019-07-09 11:11:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (201, 'P000001', 'Login', '2019-07-09 11:23:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (202, 'P000001', 'Logout', '2019-07-09 11:24:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (203, 'P000001', 'Login', '2019-07-09 11:34:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (204, 'P000001', 'Login', '2019-07-09 11:35:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (205, 'P000001', 'Login', '2019-07-09 11:39:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (206, 'P000001', 'Login', '2019-07-09 11:40:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (207, 'P000001', 'Login', '2019-07-09 11:42:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (208, 'P000001', 'Login', '2019-07-09 11:42:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (209, '', 'Logout', '2019-07-09 11:43:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (210, 'P000001', 'Login', '2019-07-09 12:40:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (211, 'P000001', 'Login', '2019-07-09 12:42:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (212, 'P000001', 'Login', '2019-07-09 12:51:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (213, 'P000001', 'Login', '2019-07-09 12:55:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (214, 'P000001', 'Login', '2019-07-09 13:13:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (215, 'P000001', 'Login', '2019-07-09 13:15:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (216, 'P000001', 'Login', '2019-07-09 13:19:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (217, 'P000001', 'Login', '2019-07-09 13:23:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (218, 'P000001', 'Login', '2019-07-09 13:29:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (219, 'P000001', 'Login', '2019-07-09 13:46:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (220, 'P000001', 'Login', '2019-07-09 14:00:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (221, 'P000001', 'Login', '2019-07-09 14:02:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (222, 'P000001', 'Login', '2019-07-09 14:53:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (223, 'P000001', 'Login', '2019-07-09 14:57:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (224, 'P000001', 'Login', '2019-07-09 15:09:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (225, 'P000001', 'Login', '2019-07-09 15:16:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (226, 'P000001', 'Login', '2019-07-09 15:34:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (227, 'P000001', 'Login', '2019-07-09 15:38:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (228, 'P000001', 'Login', '2019-07-09 15:58:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (229, 'P000001', 'Login', '2019-07-09 16:09:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (230, 'P000001', 'Login', '2019-07-09 16:35:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (231, 'P000001', 'Login', '2019-07-09 16:36:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (232, 'P000001', 'Login', '2019-07-09 16:36:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (233, 'P000001', 'Login', '2019-07-09 16:37:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (234, 'P000001', 'Login', '2019-07-09 16:38:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (235, 'P000001', 'Login', '2019-07-09 16:38:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (236, 'P000001', 'Login', '2019-07-09 16:39:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (237, 'P000001', 'Login', '2019-07-09 16:39:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (238, 'P000001', 'Login', '2019-07-09 18:33:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (239, 'P000001', 'Login', '2019-07-10 08:27:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (240, 'P000001', 'Login', '2019-07-10 08:27:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (241, 'P000001', 'Login', '2019-07-10 08:37:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (242, 'P000001', 'Login', '2019-07-10 08:37:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (243, 'P000001', 'Login', '2019-07-10 08:52:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (244, 'P000001', 'Login', '2019-07-10 08:56:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (245, '', 'Logout', '2019-07-10 09:00:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (246, 'P000001', 'Login', '2019-07-10 09:00:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (247, 'P000001', 'Login', '2019-07-10 09:07:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (248, 'P000001', 'Login', '2019-07-10 09:09:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (249, 'P000001', 'Login', '2019-07-10 09:12:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (250, 'P000001', 'Login', '2019-07-10 09:18:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (251, 'P000001', 'Login', '2019-07-10 09:24:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (252, 'P000001', 'Login', '2019-07-10 09:24:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (253, 'P000001', 'Login', '2019-07-10 09:24:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (254, 'P000001', 'Login', '2019-07-10 09:25:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (255, 'P000001', 'Login', '2019-07-10 09:26:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (256, 'P000001', 'Login', '2019-07-10 09:26:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (257, 'P000001', 'Login', '2019-07-10 09:32:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (258, 'P000001', 'Login', '2019-07-10 09:34:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (259, 'P000001', 'Login', '2019-07-10 09:38:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (260, 'P000001', 'Login', '2019-07-10 09:39:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (261, 'P000001', 'Login', '2019-07-10 09:46:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (262, 'P000001', 'Login', '2019-07-10 09:47:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (263, 'P000001', 'Login', '2019-07-10 09:48:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (264, 'P000001', 'Login', '2019-07-10 10:11:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (265, 'P000001', 'Login', '2019-07-10 10:11:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (266, 'P000001', 'Login', '2019-07-10 10:24:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (267, 'P000001', 'Login', '2019-07-10 10:39:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (268, 'P000001', 'Login', '2019-07-10 12:23:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (269, 'P000001', 'Login', '2019-07-10 12:50:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (270, 'P000001', 'Login', '2019-07-10 13:12:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (271, 'P000001', 'Login', '2019-07-10 13:12:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (272, 'P000001', 'Login', '2019-07-10 13:27:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (273, 'P000001', 'Login', '2019-07-10 13:28:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (274, 'P000001', 'Login', '2019-07-10 13:31:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (275, 'P000001', 'Logout', '2019-07-10 13:35:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (276, 'P000001', 'Login', '2019-07-10 13:35:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (277, 'P000001', 'Login', '2019-07-10 13:39:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (278, 'P000001', 'Login', '2019-07-10 13:44:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (279, '', 'Logout', '2019-07-10 14:35:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (280, 'P000001', 'Login', '2019-07-10 14:35:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (281, 'P000001', 'Login', '2019-07-10 14:43:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (282, 'P000001', 'Login', '2019-07-10 14:50:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (283, 'P000001', 'Login', '2019-07-10 14:55:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (284, 'P000001', 'Login', '2019-07-10 15:14:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (285, 'P000001', 'Login', '2019-07-10 15:54:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (286, 'P000001', 'Login', '2019-07-10 16:17:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (287, 'P000001', 'Login', '2019-07-10 16:47:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (288, 'P000001', 'Login', '2019-07-10 16:48:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (289, 'P000001', 'Login', '2019-07-10 16:53:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (290, 'P000001', 'Login', '2019-07-10 16:54:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (291, 'P000001', 'Login', '2019-07-10 16:55:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (292, 'P000001', 'Login', '2019-07-10 18:40:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (293, 'P000001', 'Login', '2019-07-10 19:51:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (294, 'P000001', 'Login', '2019-07-10 20:09:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (295, 'P000001', 'Login', '2019-07-10 20:10:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (296, 'P000001', 'Login', '2019-07-10 20:31:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (297, 'P000001', 'Login', '2019-07-11 08:14:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (298, 'P000001', 'Login', '2019-07-11 08:17:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (299, 'P000001', 'Login', '2019-07-11 08:25:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (300, 'P000001', 'Login', '2019-07-11 08:30:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (301, 'P000001', 'Login', '2019-07-11 08:50:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (302, 'P000001', 'Login', '2019-07-11 09:14:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (303, 'P000001', 'Login', '2019-07-11 09:18:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (304, 'P000001', 'Logout', '2019-07-11 09:21:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (305, 'P000001', 'Login', '2019-07-11 09:21:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (306, 'P000001', 'Login', '2019-07-11 09:24:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (307, 'P000001', 'Login', '2019-07-11 09:24:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (308, 'P000001', 'Login', '2019-07-11 09:25:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (309, 'P000001', 'Logout', '2019-07-11 09:27:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (310, 'P000001', 'Login', '2019-07-11 09:27:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (311, 'P000001', 'Login', '2019-07-11 09:41:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (312, 'P000001', 'Login', '2019-07-11 09:41:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (313, 'P000001', 'Login', '2019-07-11 09:42:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (314, 'P000001', 'Login', '2019-07-11 09:47:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (315, 'P000001', 'Login', '2019-07-11 09:50:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (316, 'P000001', 'Login', '2019-07-11 09:51:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (317, 'P000001', 'Login', '2019-07-11 09:52:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (318, '', 'Logout', '2019-07-11 09:52:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (319, 'P000001', 'Login', '2019-07-11 09:52:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (320, 'P000001', 'Login', '2019-07-11 09:52:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (321, 'P000001', 'Login', '2019-07-11 09:53:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (322, 'P000001', 'Login', '2019-07-11 09:53:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (323, 'P000001', 'Login', '2019-07-11 09:53:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (324, 'P000001', 'Login', '2019-07-11 09:54:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (325, 'P000001', 'Login', '2019-07-11 09:56:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (326, 'P000001', 'Login', '2019-07-11 10:21:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (327, 'P000001', 'Login', '2019-07-11 10:24:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (328, 'P000001', 'Login', '2019-07-11 10:25:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (329, 'P000001', 'Login', '2019-07-11 10:25:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (330, 'P000001', 'Login', '2019-07-11 10:33:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (331, 'P000001', 'Login', '2019-07-11 10:33:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (332, 'P000001', 'Login', '2019-07-11 10:34:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (333, '', 'Logout', '2019-07-11 10:34:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (334, 'P000001', 'Login', '2019-07-11 10:34:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (335, 'P000001', 'Login', '2019-07-11 10:34:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (336, 'P000001', 'Login', '2019-07-11 10:45:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (337, 'P000001', 'Login', '2019-07-11 10:52:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (338, 'P000001', 'Login', '2019-07-11 10:55:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (339, 'P000001', 'Login', '2019-07-11 11:06:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (340, 'P000001', 'Login', '2019-07-11 11:07:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (341, 'P000001', 'Login', '2019-07-11 11:11:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (342, 'P000001', 'Login', '2019-07-11 11:11:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (343, 'P000001', 'Login', '2019-07-11 11:12:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (344, 'P000001', 'Login', '2019-07-11 11:16:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (345, 'P000001', 'Login', '2019-07-11 12:34:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (346, 'P000001', 'Login', '2019-07-11 13:13:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (347, 'P000001', 'Login', '2019-07-11 13:21:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (348, 'P000001', 'Login', '2019-07-11 13:25:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (349, 'P000001', 'Login', '2019-07-11 13:34:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (350, 'P000001', 'Login', '2019-07-11 13:41:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (351, 'P000001', 'Login', '2019-07-11 13:50:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (352, 'P000001', 'Login', '2019-07-11 13:58:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (353, 'P000001', 'Login', '2019-07-11 14:04:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (354, 'P000001', 'Login', '2019-07-11 14:06:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (355, 'P000001', 'Login', '2019-07-11 14:12:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (356, 'P000001', 'Login', '2019-07-11 14:59:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (357, 'P000001', 'Login', '2019-07-11 15:37:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (358, 'P000001', 'Login', '2019-07-11 15:39:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (359, 'P000001', 'Login', '2019-07-11 15:40:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (360, 'P000001', 'Login', '2019-07-11 15:44:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (361, 'P000001', 'Login', '2019-07-11 15:51:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (362, 'P000001', 'Login', '2019-07-11 15:52:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (363, 'P000001', 'Logout', '2019-07-11 17:12:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (364, 'P000001', 'Login', '2019-07-11 17:50:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (365, '', 'Logout', '2019-07-12 08:25:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (366, 'P000001', 'Login', '2019-07-12 08:25:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (367, 'P000001', 'Login', '2019-07-12 08:34:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (368, 'P000001', 'Login', '2019-07-12 08:39:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (369, 'P000001', 'Login', '2019-07-12 08:56:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (370, 'P000001', 'Login', '2019-07-12 08:58:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (371, 'P000001', 'Login', '2019-07-12 08:59:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (372, 'P000001', 'Login', '2019-07-12 09:25:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (373, 'P000001', 'Login', '2019-07-12 09:28:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (374, 'P000001', 'Login', '2019-07-12 09:39:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (375, 'P000001', 'Login', '2019-07-12 10:17:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (376, 'P000001', 'Login', '2019-07-12 10:19:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (377, 'P000001', 'Login', '2019-07-12 10:51:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (378, 'P000001', 'Login', '2019-07-12 10:58:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (379, 'P000001', 'Login', '2019-07-12 11:12:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (380, 'P000001', 'Login', '2019-07-12 11:22:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (381, 'P000001', 'Login', '2019-07-12 11:25:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (382, 'P000001', 'Login', '2019-07-12 11:28:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (383, 'P000001', 'Logout', '2019-07-12 11:35:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (384, 'P000001', 'Login', '2019-07-12 11:35:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (385, 'P000001', 'Login', '2019-07-12 13:13:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (386, 'P000001', 'Login', '2019-07-12 13:32:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (387, 'P000001', 'Login', '2019-07-12 13:53:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (388, 'P000001', 'Logout', '2019-07-12 14:01:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (389, 'P000001', 'Login', '2019-07-12 14:01:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (390, 'P000001', 'Login', '2019-07-12 14:02:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (391, 'P000001', 'Logout', '2019-07-12 14:13:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (392, 'P000001', 'Login', '2019-07-12 14:18:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (393, 'P000001', 'Login', '2019-07-12 14:25:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (394, 'P000001', 'Login', '2019-07-12 14:29:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (395, 'P000001', 'Login', '2019-07-12 14:33:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (396, 'P000001', 'Login', '2019-07-12 14:33:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (397, 'P000001', 'Login', '2019-07-12 14:33:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (398, 'P000001', 'Login', '2019-07-12 14:56:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (399, 'P000001', 'Login', '2019-07-12 14:59:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (400, 'P000001', 'Login', '2019-07-12 15:35:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (401, 'P000001', 'Login', '2019-07-12 15:44:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (402, 'P000001', 'Login', '2019-07-12 15:54:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (403, 'P000001', 'Login', '2019-07-12 15:54:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (404, 'P000001', 'Login', '2019-07-12 15:55:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (405, 'P000001', 'Login', '2019-07-12 15:55:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (406, 'P000001', 'Login', '2019-07-12 15:57:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (407, 'P000001', 'Login', '2019-07-12 15:57:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (408, 'P000001', 'Login', '2019-07-12 16:08:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (409, 'P000001', 'Login', '2019-07-12 16:09:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (410, 'P000001', 'Login', '2019-07-12 16:14:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (411, 'P000001', 'Login', '2019-07-12 16:16:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (412, 'P000001', 'Logout', '2019-07-12 16:29:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (413, 'P000001', 'Login', '2019-07-12 16:34:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (414, 'P000001', 'Login', '2019-07-12 16:39:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (415, 'P000001', 'Login', '2019-07-12 20:30:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (416, 'P000001', 'Login', '2019-07-12 20:31:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (417, 'P000001', 'Login', '2019-07-14 16:57:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (418, 'P000001', 'Login', '2019-07-14 17:33:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (419, 'P000001', 'Login', '2019-07-14 18:23:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (420, 'P000001', 'Login', '2019-07-14 21:51:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (421, 'P000001', 'Logout', '2019-07-14 22:49:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (422, 'P000001', 'Login', '2019-07-15 08:09:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (423, 'P000001', 'Login', '2019-07-15 08:15:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (424, 'P000001', 'Login', '2019-07-15 08:19:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (425, 'P000001', 'Login', '2019-07-15 08:47:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (426, 'P000001', 'Login', '2019-07-15 09:21:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (427, 'P000001', 'Login', '2019-07-15 09:21:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (428, 'P000001', 'Login', '2019-07-15 09:26:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (429, 'P000001', 'Login', '2019-07-15 09:26:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (430, 'P000001', 'Login', '2019-07-15 09:33:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (431, 'P000001', 'Login', '2019-07-15 09:34:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (432, 'P000001', 'Login', '2019-07-15 09:36:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (433, 'P000001', 'Login', '2019-07-15 09:37:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (434, 'P000001', 'Login', '2019-07-15 09:40:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (435, 'P000001', 'Login', '2019-07-15 09:42:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (436, 'P000001', 'Login', '2019-07-15 09:46:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (437, 'P000001', 'Login', '2019-07-15 09:49:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (438, 'P000001', 'Login', '2019-07-15 10:32:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (439, 'P000001', 'Login', '2019-07-15 10:37:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (440, 'P000001', 'Login', '2019-07-15 10:43:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (441, 'P000001', 'Login', '2019-07-15 10:45:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (442, 'P000001', 'Login', '2019-07-15 10:58:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (443, '', 'Logout', '2019-07-15 13:31:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (444, 'P000001', 'Login', '2019-07-15 13:31:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (445, 'P000001', 'Login', '2019-07-15 13:42:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (446, 'P000001', 'Login', '2019-07-15 13:43:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (447, 'P000001', 'Login', '2019-07-15 13:43:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (448, 'P000001', 'Login', '2019-07-15 13:50:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (449, 'P000001', 'Login', '2019-07-15 14:21:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (450, 'P000001', 'Login', '2019-07-15 16:18:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (451, 'P000001', 'Login', '2019-07-15 16:24:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (452, 'P000001', 'Login', '2019-07-15 16:28:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (453, 'P000001', 'Logout', '2019-07-15 16:58:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (454, 'P000001', 'Login', '2019-07-15 17:32:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (455, 'P000001', 'Login', '2019-07-15 17:51:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (456, 'P000001', 'Login', '2019-07-15 18:31:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (457, 'P000001', 'Login', '2019-07-15 18:59:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (458, 'P000001', 'Login', '2019-07-15 19:51:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (459, '', 'Logout', '2019-07-16 08:51:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (460, 'P000001', 'Login', '2019-07-16 08:52:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (461, 'P000001', 'Login', '2019-07-16 08:59:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (462, 'P000001', 'Login', '2019-07-16 09:14:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (463, 'P000001', 'Login', '2019-07-16 09:17:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (464, 'P000001', 'Login', '2019-07-16 09:24:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (465, 'P000001', 'Login', '2019-07-16 09:25:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (466, 'P000001', 'Login', '2019-07-16 09:36:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (467, 'P000001', 'Login', '2019-07-16 09:45:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (468, 'P000001', 'Login', '2019-07-16 09:50:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (469, 'P000001', 'Login', '2019-07-16 09:54:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (470, 'P000001', 'Login', '2019-07-16 09:59:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (471, 'P000001', 'Login', '2019-07-16 10:10:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (472, 'P000001', 'Login', '2019-07-16 10:13:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (473, 'P000001', 'Login', '2019-07-16 10:24:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (474, 'P000001', 'Login', '2019-07-16 10:30:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (475, 'P000001', 'Login', '2019-07-16 10:39:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (476, 'P000001', 'Login', '2019-07-16 10:45:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (477, 'P000001', 'Login', '2019-07-16 10:56:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (478, 'P000001', 'Login', '2019-07-16 11:16:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (479, 'P000001', 'Login', '2019-07-16 11:23:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (480, 'P000001', 'Login', '2019-07-16 11:35:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (481, 'P000001', 'Login', '2019-07-16 13:01:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (482, 'P000001', 'Login', '2019-07-16 13:24:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (483, '', 'Logout', '2019-07-16 13:24:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (484, 'P000001', 'Login', '2019-07-16 13:24:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (485, 'P000001', 'Login', '2019-07-16 13:40:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (486, 'P000001', 'Login', '2019-07-16 13:41:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (487, 'P000001', 'Login', '2019-07-16 13:49:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (488, 'P000001', 'Login', '2019-07-16 13:49:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (489, 'P000001', 'Login', '2019-07-16 13:49:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (490, 'P000001', 'Login', '2019-07-16 14:09:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (491, 'P000001', 'Login', '2019-07-16 15:23:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (492, 'P000001', 'Login', '2019-07-16 15:23:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (493, 'P000001', 'Login', '2019-07-16 15:25:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (494, 'P000001', 'Login', '2019-07-16 16:46:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (495, 'P000001', 'Login', '2019-07-16 17:19:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (496, 'P000001', 'Login', '2019-07-16 17:22:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (497, 'P000001', 'Login', '2019-07-16 17:58:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (498, 'P000001', 'Login', '2019-07-16 18:16:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (499, 'P000001', 'Login', '2019-07-16 18:23:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (500, '', 'Logout', '2019-07-16 18:51:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (501, 'P000001', 'Login', '2019-07-16 18:51:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (502, 'P000001', 'Login', '2019-07-17 08:35:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (503, 'P000001', 'Login', '2019-07-17 09:08:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (504, '', 'Logout', '2019-07-17 09:08:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (505, 'P000001', 'Login', '2019-07-17 09:08:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (506, 'P000001', 'Login', '2019-07-17 09:30:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (507, 'P000001', 'Login', '2019-07-17 09:35:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (508, 'P000001', 'Login', '2019-07-17 09:52:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (509, 'P000001', 'Login', '2019-07-17 10:00:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (510, 'P000001', 'Login', '2019-07-17 10:20:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (511, 'P000001', 'Login', '2019-07-17 10:34:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (512, 'P000001', 'Login', '2019-07-17 12:49:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (513, 'P000001', 'Login', '2019-07-17 12:55:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (514, 'P000001', 'Login', '2019-07-17 12:57:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (515, 'P000001', 'Login', '2019-07-17 12:57:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (516, 'P000001', 'Login', '2019-07-17 13:23:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (517, 'P000001', 'Login', '2019-07-17 13:31:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (518, 'P000001', 'Login', '2019-07-17 13:32:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (519, 'P000001', 'Login', '2019-07-17 13:37:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (520, 'P000001', 'Login', '2019-07-17 13:45:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (521, 'P000001', 'Login', '2019-07-17 13:53:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (522, 'P000001', 'Login', '2019-07-17 13:56:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (523, 'P000001', 'Login', '2019-07-17 15:51:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (524, 'P000001', 'Login', '2019-07-17 15:54:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (525, '', 'Logout', '2019-07-17 16:05:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (526, 'P000001', 'Login', '2019-07-17 16:05:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (527, 'P000001', 'Login', '2019-07-17 16:10:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (528, '', 'Logout', '2019-07-17 20:28:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (529, 'P000001', 'Login', '2019-07-17 20:28:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (530, '', 'Logout', '2019-07-18 07:05:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (531, 'P000001', 'Login', '2019-07-18 07:05:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (532, 'P000001', 'Login', '2019-07-18 08:29:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (533, 'P000001', 'Login', '2019-07-18 08:40:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (534, 'P000001', 'Login', '2019-07-18 09:09:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (535, 'P000001', 'Login', '2019-07-18 09:47:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (536, 'P000001', 'Login', '2019-07-18 10:05:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (537, 'P000001', 'Login', '2019-07-18 10:06:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (538, 'P000001', 'Login', '2019-07-18 10:12:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (539, 'P000001', 'Login', '2019-07-18 10:23:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (540, '', 'Logout', '2019-07-18 10:31:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (541, 'P000001', 'Login', '2019-07-18 10:31:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (542, 'P000001', 'Login', '2019-07-18 10:33:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (543, 'P000001', 'Login', '2019-07-18 10:54:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (544, 'P000001', 'Login', '2019-07-18 10:57:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (545, 'P000001', 'Login', '2019-07-18 11:01:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (546, 'P000001', 'Login', '2019-07-18 11:10:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (547, 'P000001', 'Login', '2019-07-18 13:24:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (548, 'P000001', 'Login', '2019-07-18 13:32:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (549, 'P000001', 'Login', '2019-07-18 13:55:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (550, 'P000001', 'Login', '2019-07-18 14:44:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (551, 'P000001', 'Login', '2019-07-18 14:49:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (552, 'P000001', 'Login', '2019-07-18 15:41:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (553, '', 'Logout', '2019-07-18 16:07:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (554, 'P000001', 'Login', '2019-07-18 16:07:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (555, 'P000001', 'Login', '2019-07-18 16:22:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (556, '', 'Logout', '2019-07-18 21:19:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (557, 'P000001', 'Login', '2019-07-18 21:19:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (558, 'P000001', 'Login', '2019-07-19 09:05:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (559, 'P000001', 'Login', '2019-07-19 09:12:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (560, 'P000001', 'Login', '2019-07-19 09:20:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (561, '', 'Logout', '2019-07-19 09:30:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (562, 'P000001', 'Login', '2019-07-19 09:30:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (563, 'P000001', 'Login', '2019-07-19 09:30:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (564, 'P000001', 'Login', '2019-07-19 09:39:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (565, 'P000001', 'Login', '2019-07-19 10:01:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (566, 'P000001', 'Login', '2019-07-19 10:08:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (567, 'P000001', 'Login', '2019-07-19 10:09:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (568, 'P000001', 'Login', '2019-07-19 10:41:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (569, 'P000001', 'Login', '2019-07-19 10:59:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (570, 'P000001', 'Login', '2019-07-19 11:00:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (571, 'P000001', 'Login', '2019-07-19 11:04:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (572, 'P000001', 'Login', '2019-07-19 11:13:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (573, 'P000001', 'Login', '2019-07-19 11:16:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (574, 'P000001', 'Login', '2019-07-19 13:09:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (575, 'P000001', 'Logout', '2019-07-19 13:09:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (576, 'P000001', 'Login', '2019-07-19 13:23:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (577, 'P000001', 'Login', '2019-07-19 13:33:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (578, 'P000001', 'Login', '2019-07-19 13:40:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (579, 'P000001', 'Login', '2019-07-19 14:20:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (580, 'P000001', 'Login', '2019-07-19 14:21:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (581, 'P000001', 'Login', '2019-07-19 14:52:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (582, 'P000001', 'Login', '2019-07-19 14:54:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (583, 'P000001', 'Login', '2019-07-19 16:32:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (584, 'P000001', 'Login', '2019-07-19 16:32:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (585, 'P000001', 'Login', '2019-07-19 16:57:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (586, 'P000001', 'Login', '2019-07-19 17:01:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (587, 'P000001', 'Login', '2019-07-19 17:35:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (588, 'P000001', 'Login', '2019-07-22 08:29:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (589, '', 'Logout', '2019-07-22 08:39:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (590, 'P000001', 'Login', '2019-07-22 08:39:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (591, 'P000001', 'Login', '2019-07-22 08:40:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (592, 'P000001', 'Login', '2019-07-22 09:29:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (593, 'P000001', 'Login', '2019-07-22 09:36:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (594, 'P000001', 'Login', '2019-07-22 11:20:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (595, 'P000001', 'Login', '2019-07-22 11:32:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (596, 'P000001', 'Login', '2019-07-22 13:27:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (597, 'P000001', 'Login', '2019-07-22 13:31:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (598, 'P000001', 'Login', '2019-07-22 14:54:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (599, 'P000001', 'Login', '2019-07-22 15:48:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (600, 'P000001', 'Login', '2019-07-22 16:06:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (601, 'P000001', 'Login', '2019-07-22 16:30:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (602, 'P000001', 'Login', '2019-07-22 16:59:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (603, 'P000001', 'Login', '2019-07-23 08:47:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (604, 'P000001', 'Login', '2019-07-23 09:01:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (605, 'P000001', 'Login', '2019-07-23 09:07:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (606, 'P000001', 'Login', '2019-07-23 09:38:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (607, '', 'Logout', '2019-07-23 09:39:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (608, 'P000001', 'Login', '2019-07-23 09:39:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (609, 'P000001', 'Logout', '2019-07-23 09:40:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (610, 'P000001', 'Login', '2019-07-23 09:40:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (611, 'P000001', 'Login', '2019-07-23 12:51:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (612, '', 'Logout', '2019-07-23 13:12:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (613, 'P000001', 'Login', '2019-07-23 13:12:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (614, 'P000001', 'Login', '2019-07-23 14:16:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (615, 'P000001', 'Login', '2019-07-23 15:05:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (616, 'P000001', 'Login', '2019-07-23 16:01:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (617, 'P000001', 'Login', '2019-07-23 16:16:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (618, 'P000001', 'Login', '2019-07-23 16:18:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (619, 'P000001', 'Login', '2019-07-23 16:29:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (620, 'P000001', 'Login', '2019-07-23 16:30:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (621, '', 'Logout', '2019-07-23 22:40:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (622, 'P000001', 'Login', '2019-07-23 22:40:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (623, 'P000001', 'Login', '2019-07-24 09:33:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (624, '', 'Logout', '2019-07-24 09:57:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (625, '', 'Logout', '2019-07-24 09:58:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (626, 'P000001', 'Login', '2019-07-24 09:58:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (627, 'P000001', 'Login', '2019-07-24 10:03:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (628, 'P000001', 'Login', '2019-07-24 12:45:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (629, '', 'Logout', '2019-07-24 22:26:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (630, 'P000001', 'Login', '2019-07-24 22:27:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (631, 'P000001', 'Login', '2019-07-25 08:58:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (632, 'P000001', 'Login', '2019-07-25 09:08:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (633, 'P000001', 'Login', '2019-07-25 09:27:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (634, '', 'Logout', '2019-07-25 09:46:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (635, 'P000001', 'Login', '2019-07-25 09:46:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (636, 'P000001', 'Login', '2019-07-25 10:48:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (637, 'P000001', 'Login', '2019-07-25 10:54:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (638, 'P000001', 'Login', '2019-07-25 11:24:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (639, 'P000001', 'Login', '2019-07-25 13:20:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (640, 'P000001', 'Login', '2019-07-25 13:36:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (641, 'P000001', 'Login', '2019-07-25 13:40:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (642, 'P000001', 'Login', '2019-07-25 14:41:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (643, 'P000001', 'Login', '2019-07-25 15:05:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (644, 'P000001', 'Login', '2019-07-25 15:13:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (645, 'P000001', 'Login', '2019-07-25 15:13:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (646, 'P000001', 'Login', '2019-07-25 15:25:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (647, 'P000001', 'Login', '2019-07-25 16:14:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (648, 'P000001', 'Login', '2019-07-25 17:02:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (649, 'P000001', 'Logout', '2019-07-25 17:03:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (650, 'P000001', 'Login', '2019-07-25 17:16:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (651, 'P000001', 'Login', '2019-07-25 17:16:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (652, 'P000001', 'Login', '2019-07-25 18:55:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (653, 'P000001', 'Login', '2019-07-25 21:30:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (654, '', 'Logout', '2019-07-25 22:02:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (655, 'P000001', 'Login', '2019-07-25 22:02:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (656, 'P000001', 'Login', '2019-07-26 08:57:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (657, 'P000001', 'Login', '2019-07-26 09:08:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (658, 'P000001', 'Login', '2019-07-26 09:33:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (659, 'P000001', 'Login', '2019-07-26 09:42:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (660, 'P000001', 'Login', '2019-07-26 10:05:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (661, 'P000001', 'Login', '2019-07-26 13:15:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (662, '', 'Logout', '2019-07-26 16:05:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (663, 'P000001', 'Login', '2019-07-26 16:05:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (664, 'P000001', 'Login', '2019-07-29 10:20:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (665, 'P000001', 'Login', '2019-07-29 11:17:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (666, 'P000001', 'Login', '2019-07-29 14:46:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (667, 'P000001', 'Login', '2019-07-30 09:32:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (668, '', 'Logout', '2019-07-30 09:49:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (669, 'P000001', 'Login', '2019-07-30 09:49:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (670, 'P000001', 'Login', '2019-07-30 10:28:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (671, '', 'Logout', '2019-07-30 15:01:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (672, 'P000001', 'Login', '2019-07-30 15:01:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (673, '', 'Logout', '2019-07-31 08:59:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (674, '', 'Logout', '2019-07-31 08:59:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (675, 'P000001', 'Login', '2019-07-31 09:00:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (676, 'P000001', 'Login', '2019-07-31 09:09:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (677, 'P000001', 'Logout', '2019-07-31 10:29:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (678, 'P000001', 'Login', '2019-07-31 10:29:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (679, 'P000001', 'Login', '2019-07-31 10:31:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (680, 'P000001', 'Login', '2019-07-31 10:37:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (681, 'P000001', 'Login', '2019-07-31 10:40:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (682, 'P000001', 'Login', '2019-07-31 12:00:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (683, 'P000001', 'Login', '2019-07-31 12:01:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (684, 'P000001', 'Login', '2019-07-31 12:58:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (685, 'P000001', 'Login', '2019-07-31 13:19:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (686, 'P000001', 'Login', '2019-07-31 14:01:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (687, 'P000001', 'Login', '2019-08-01 10:10:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (688, 'P000001', 'Login', '2019-08-01 10:29:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (689, 'P000001', 'Login', '2019-08-01 13:54:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (690, 'P000001', 'Login', '2019-08-02 09:13:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (691, '', 'Logout', '2019-08-02 09:54:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (692, '', 'Logout', '2019-08-02 09:54:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (693, 'P000001', 'Login', '2019-08-02 09:54:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (694, 'P000001', 'Login', '2019-08-02 11:43:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (695, 'P000001', 'Login', '2019-08-02 13:46:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (696, 'P000001', 'Login', '2019-08-02 14:00:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (697, 'P000001', 'Login', '2019-08-02 14:02:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (698, 'P000001', 'Login', '2019-08-02 15:12:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (699, 'P000001', 'Login', '2019-08-02 15:30:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (700, 'P000001', 'Login', '2019-08-02 15:36:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (701, 'P000001', 'Login', '2019-08-02 15:53:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (702, 'P000001', 'Login', '2019-08-02 16:25:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (703, 'P000001', 'Login', '2019-08-05 09:21:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (704, '', 'Logout', '2019-08-05 09:44:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (705, 'P000001', 'Login', '2019-08-05 09:44:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (706, 'P000001', 'Login', '2019-08-05 09:48:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (707, 'P000001', 'Login', '2019-08-05 13:58:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (708, 'P000001', 'Login', '2019-08-06 08:55:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (709, '', 'Logout', '2019-08-06 10:36:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (710, 'P000001', 'Login', '2019-08-06 10:36:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (711, 'P000001', 'Login', '2019-08-06 14:33:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (712, '', 'Logout', '2019-08-06 22:15:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (713, '', 'Logout', '2019-08-06 22:15:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (714, 'P000001', 'Login', '2019-08-06 22:16:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (715, '', 'Logout', '2019-08-07 09:08:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (716, 'P000001', 'Login', '2019-08-07 09:13:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (717, 'P000001', 'Login', '2019-08-07 10:35:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (718, '', 'Logout', '2019-08-07 13:49:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (719, 'P000001', 'Login', '2019-08-07 13:50:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (720, 'P000001', 'Login', '2019-08-07 14:22:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (721, 'P000001', 'Login', '2019-08-07 14:30:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (722, 'P000001', 'Login', '2019-08-07 16:42:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (723, '', 'Logout', '2019-08-07 19:50:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (724, 'P000001', 'Login', '2019-08-07 19:50:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (725, 'P000001', 'Login', '2019-08-08 10:13:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (726, '', 'Logout', '2019-08-08 10:16:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (727, 'P000001', 'Login', '2019-08-08 10:17:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (728, 'P000001', 'Login', '2019-08-08 13:02:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (729, 'P000001', 'Login', '2019-08-08 13:24:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (730, 'P000001', 'Login', '2019-08-08 13:24:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (731, 'P000001', 'Login', '2019-08-08 14:47:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (732, 'P000001', 'Login', '2019-08-09 09:04:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (733, '', 'Logout', '2019-08-09 09:39:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (734, '', 'Logout', '2019-08-09 09:40:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (735, 'P000001', 'Login', '2019-08-09 09:40:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (736, 'P000001', 'Login', '2019-08-09 10:30:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (737, 'P000001', 'Logout', '2019-08-09 10:34:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (738, '', 'Logout', '2019-08-09 10:34:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (739, '', 'Logout', '2019-08-09 10:34:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (740, '', 'Logout', '2019-08-09 10:35:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (741, 'P000001', 'Login', '2019-08-09 10:42:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (742, '', 'Logout', '2019-08-09 14:24:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (743, 'P000001', 'Login', '2019-08-09 14:24:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (744, 'P000001', 'Login', '2019-08-12 09:11:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (745, '', 'Logout', '2019-08-12 10:11:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (746, 'P000001', 'Login', '2019-08-12 10:11:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (747, '', 'Logout', '2019-08-12 11:23:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (748, 'P000001', 'Login', '2019-08-12 14:15:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (749, 'P000001', 'Login', '2019-08-12 15:11:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (750, 'P000001', 'Login', '2019-08-12 18:26:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (751, 'P000001', 'Login', '2019-08-12 20:03:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (752, 'P000001', 'Login', '2019-08-13 08:37:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (753, 'P000001', 'Login', '2019-08-13 08:52:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (754, 'P000001', 'Login', '2019-08-13 09:02:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (755, 'P000001', 'Login', '2019-08-13 14:11:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (756, 'P000001', 'Login', '2019-08-13 15:21:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (757, 'P000001', 'Login', '2019-08-14 08:55:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (758, 'P000001', 'Login', '2019-08-14 09:09:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (759, 'P000001', 'Login', '2019-08-14 09:24:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (760, 'P000001', 'Login', '2019-08-14 18:29:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (761, '', 'Logout', '2019-08-15 08:40:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (762, 'P000001', 'Login', '2019-08-15 08:40:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (763, 'P000001', 'Login', '2019-08-15 09:04:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (764, 'P000001', 'Login', '2019-08-15 09:12:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (765, 'P000001', 'Login', '2019-08-15 09:17:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (766, 'P000001', 'Login', '2019-08-15 09:36:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (767, 'P000001', 'Login', '2019-08-15 16:42:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (768, 'P000001', 'Login', '2019-08-16 08:57:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (769, 'P000001', 'Login', '2019-08-16 09:53:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (770, 'P000001', 'Login', '2019-08-16 09:56:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (771, 'P000001', 'Login', '2019-08-16 10:09:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (772, 'P000001', 'Login', '2019-08-16 10:30:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (773, 'P000001', 'Login', '2019-08-16 10:39:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (774, 'P000001', 'Login', '2019-08-16 10:52:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (775, 'P000001', 'Login', '2019-08-16 14:36:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (776, 'P000001', 'Login', '2019-08-16 14:40:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (777, 'P000001', 'Login', '2019-08-16 14:46:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (778, 'P000001', 'Login', '2019-08-16 14:51:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (779, 'P000001', 'Logout', '2019-08-16 14:51:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (780, 'P000001', 'Login', '2019-08-16 14:51:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (781, 'P000001', 'Login', '2019-08-16 15:03:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (782, 'P000001', 'Login', '2019-08-16 15:37:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (783, 'P000001', 'Login', '2019-08-19 08:54:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (784, 'P000001', 'Login', '2019-08-19 09:10:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (785, 'P000001', 'Login', '2019-08-19 09:36:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (786, 'P000001', 'Login', '2019-08-19 10:50:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (787, 'P000001', 'Login', '2019-08-19 12:35:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (788, 'P000001', 'Login', '2019-08-19 12:38:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (789, 'P000001', 'Login', '2019-08-19 13:05:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (790, 'P000001', 'Login', '2019-08-19 14:06:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (791, 'P000001', 'Login', '2019-08-19 15:44:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (792, 'P000001', 'Login', '2019-08-19 16:46:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (793, 'P000001', 'Login', '2019-08-20 08:50:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (794, 'P000001', 'Login', '2019-08-20 08:52:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (795, 'P000001', 'Login', '2019-08-20 08:52:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (796, 'P000001', 'Logout', '2019-08-20 08:52:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (797, 'P000001', 'Login', '2019-08-20 08:55:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (798, 'P000001', 'Login', '2019-08-20 08:55:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (799, 'P000001', 'Login', '2019-08-20 08:56:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (800, 'P000001', 'Logout', '2019-08-20 08:58:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (801, 'P000001', 'Login', '2019-08-20 09:51:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (802, 'P000001', 'Login', '2019-08-20 11:08:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (803, 'P000001', 'Login', '2019-08-20 14:06:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (804, 'P000001', 'Login', '2019-08-21 09:09:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (805, 'P000001', 'Login', '2019-08-21 09:17:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (806, 'P000001', 'Login', '2019-08-22 08:40:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (807, 'P000001', 'Login', '2019-08-22 08:57:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (808, 'P000001', 'Login', '2019-08-22 09:25:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (809, 'P000001', 'Logout', '2019-08-22 11:32:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (810, 'P000001', 'Login', '2019-08-22 11:32:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (811, 'P000001', 'Login', '2019-08-22 13:45:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (812, 'P000001', 'Login', '2019-08-22 15:48:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (813, 'P000001', 'Login', '2019-08-22 15:48:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (814, 'P000001', 'Login', '2019-08-22 21:31:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (815, 'P000001', 'Login', '2019-08-22 22:20:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (816, 'P000001', 'Login', '2019-08-23 11:34:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (817, 'P000001', 'Login', '2019-08-23 12:39:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (818, 'P000001', 'Login', '2019-08-23 14:57:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (819, '', 'Logout', '2019-08-23 15:07:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (820, 'P000001', 'Login', '2019-08-23 15:07:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (821, 'P000001', 'Login', '2019-08-23 16:38:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (822, 'P000001', 'Login', '2019-08-26 00:52:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (823, 'P000001', 'Login', '2019-08-26 08:40:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (824, 'P000001', 'Login', '2019-08-26 09:10:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (825, 'P000001', 'Login', '2019-08-26 09:26:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (826, 'P000001', 'Login', '2019-08-26 09:27:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (827, 'P000001', 'Login', '2019-08-26 13:34:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (828, 'P000001', 'Login', '2019-08-26 13:35:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (829, 'P000001', 'Login', '2019-08-26 14:05:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (830, 'P000001', 'Login', '2019-08-26 14:26:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (831, 'P000001', 'Login', '2019-08-26 18:39:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (832, 'P000001', 'Login', '2019-08-27 08:51:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (833, 'P000001', 'Login', '2019-08-27 09:04:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (834, 'P000001', 'Login', '2019-08-27 12:53:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (835, 'P000001', 'Login', '2019-08-27 13:46:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (836, 'P000001', 'Logout', '2019-08-27 13:46:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (837, 'P000001', 'Login', '2019-08-27 13:46:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (838, 'P000001', 'Login', '2019-08-28 08:49:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (839, 'P000001', 'Login', '2019-08-28 09:02:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (840, 'P000001', 'Login', '2019-08-28 10:03:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (841, 'P000001', 'Login', '2019-08-28 10:09:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (842, 'P000001', 'Login', '2019-08-28 13:58:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (843, 'P000001', 'Login', '2019-08-28 16:38:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (844, 'P000001', 'Login', '2019-08-28 16:52:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (845, 'P000001', 'Login', '2019-08-28 18:41:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (846, 'P000001', 'Login', '2019-08-29 09:06:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (847, 'P000001', 'Login', '2019-08-29 09:22:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (848, 'P000001', 'Login', '2019-08-29 09:39:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (849, 'P000001', 'Login', '2019-08-29 09:45:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (850, 'P000001', 'Login', '2019-08-29 09:48:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (851, 'P000001', 'Login', '2019-08-29 09:55:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (852, 'P000001', 'Login', '2019-08-29 10:11:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (853, 'P000001', 'Login', '2019-08-29 10:20:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (854, 'P000001', 'Login', '2019-08-29 10:30:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (855, 'P000001', 'Login', '2019-08-29 10:33:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (856, 'P000001', 'Login', '2019-08-29 11:24:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (857, 'P000001', 'Login', '2019-08-29 15:38:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (858, 'P000001', 'Login', '2019-08-29 17:12:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (859, 'P000001', 'Login', '2019-08-29 17:23:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (860, 'P000001', 'Login', '2019-08-30 10:21:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (861, 'P000001', 'Login', '2019-08-30 10:30:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (862, 'P000001', 'Login', '2019-08-30 10:45:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (863, 'P000001', 'Login', '2019-08-30 10:58:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (864, 'P000001', 'Login', '2019-08-30 13:48:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (865, 'P000001', 'Login', '2019-08-30 15:26:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (866, 'P000001', 'Login', '2019-09-01 15:15:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (867, 'P000001', 'Login', '2019-09-01 15:18:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (868, 'P000001', 'Login', '2019-09-01 15:28:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (869, 'P000001', 'Login', '2019-09-01 17:50:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (870, 'P000001', 'Login', '2019-09-01 22:58:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (871, 'P000001', 'Login', '2019-09-02 09:10:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (872, 'P000001', 'Login', '2019-09-02 09:26:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (873, 'P000001', 'Login', '2019-09-02 09:32:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (874, 'P000001', 'Login', '2019-09-02 09:39:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (875, 'P000001', 'Login', '2019-09-02 10:47:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (876, 'P000001', 'Login', '2019-09-02 11:01:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (877, 'P000001', 'Login', '2019-09-02 11:35:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (878, 'P000001', 'Login', '2019-09-02 12:48:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (879, 'P000001', 'Login', '2019-09-02 13:41:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (880, 'P000001', 'Login', '2019-09-02 13:42:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (881, 'P000001', 'Login', '2019-09-02 13:54:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (882, 'P000001', 'Login', '2019-09-02 13:55:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (883, 'P000001', 'Login', '2019-09-02 14:04:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (884, 'P000001', 'Login', '2019-09-02 14:04:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (885, 'P000001', 'Login', '2019-09-02 15:45:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (886, 'P000001', 'Login', '2019-09-02 19:02:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (887, 'P000001', 'Login', '2019-09-02 20:53:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (888, 'P000001', 'Login', '2019-09-02 21:55:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (889, 'P000001', 'Login', '2019-09-03 02:29:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (890, 'P000001', 'Login', '2019-09-03 08:41:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (891, 'P000001', 'Login', '2019-09-03 08:44:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (892, 'P000001', 'Login', '2019-09-03 08:50:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (893, 'P000001', 'Login', '2019-09-03 08:55:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (894, 'P000001', 'Login', '2019-09-03 09:08:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (895, 'P000001', 'Login', '2019-09-03 10:32:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (896, 'P000001', 'Login', '2019-09-03 10:37:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (897, 'P000001', 'Login', '2019-09-03 11:03:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (898, 'P000001', 'Login', '2019-09-03 11:04:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (899, 'P000001', 'Login', '2019-09-03 11:04:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (900, 'P000001', 'Login', '2019-09-03 11:09:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (901, 'P000001', 'Login', '2019-09-03 12:22:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (902, 'P000001', 'Login', '2019-09-03 14:02:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (903, 'P000001', 'Login', '2019-09-03 14:03:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (904, 'P000001', 'Login', '2019-09-03 14:29:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (905, 'P000001', 'Login', '2019-09-03 14:42:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (906, 'P000001', 'Login', '2019-09-03 15:54:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (907, 'P000001', 'Login', '2019-09-04 08:45:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (908, 'P000001', 'Login', '2019-09-04 08:58:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (909, 'P000001', 'Login', '2019-09-04 09:16:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (910, 'P000001', 'Login', '2019-09-04 10:24:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (911, 'P000001', 'Login', '2019-09-04 13:12:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (912, 'P000001', 'Login', '2019-09-04 13:22:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (913, 'P000001', 'Login', '2019-09-05 08:38:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (914, 'P000001', 'Login', '2019-09-05 09:56:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (915, 'P000001', 'Login', '2019-09-05 10:11:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (916, 'P000001', 'Login', '2019-09-05 21:43:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (917, 'P000001', 'Login', '2019-09-06 08:24:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (918, 'P000001', 'Login', '2019-09-06 09:48:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (919, 'P000001', 'Login', '2019-09-06 09:55:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (920, 'P000001', 'Login', '2019-09-06 10:19:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (921, 'P000001', 'Login', '2019-09-06 14:21:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (922, 'P000001', 'Login', '2019-09-06 14:24:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (923, 'P000001', 'Login', '2019-09-06 14:32:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (924, 'P000001', 'Login', '2019-09-06 22:50:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (925, 'P000001', 'Login', '2019-09-07 09:32:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (926, 'P000001', 'Login', '2019-09-07 17:14:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (927, 'P000001', 'Login', '2019-09-08 09:24:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (928, 'P000001', 'Login', '2019-09-08 19:19:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (929, 'P000001', 'Login', '2019-09-09 08:17:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (930, 'P000001', 'Login', '2019-09-09 08:18:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (931, 'P000001', 'Login', '2019-09-09 08:29:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (932, 'P000001', 'Login', '2019-09-09 08:42:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (933, 'P000001', 'Login', '2019-09-09 08:46:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (934, 'P000001', 'Login', '2019-09-09 08:47:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (935, 'P000001', 'Login', '2019-09-09 08:53:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (936, 'P000001', 'Login', '2019-09-09 09:03:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (937, 'P000001', 'Login', '2019-09-09 09:36:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (938, 'P000001', 'Login', '2019-09-09 09:58:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (939, 'P000001', 'Login', '2019-09-09 10:48:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (940, 'P000001', 'Login', '2019-09-09 10:51:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (941, 'P000001', 'Login', '2019-09-09 12:51:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (942, 'P000001', 'Login', '2019-09-09 12:59:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (943, 'P000001', 'Login', '2019-09-09 13:03:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (944, 'P000001', 'Login', '2019-09-09 13:30:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (945, 'P000001', 'Login', '2019-09-09 13:41:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (946, 'P000001', 'Login', '2019-09-09 13:42:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (947, 'P000001', 'Login', '2019-09-09 13:43:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (948, 'P000001', 'Login', '2019-09-09 14:52:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (949, 'P000001', 'Login', '2019-09-09 14:53:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (950, 'P000001', 'Login', '2019-09-09 16:22:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (951, 'P000001', 'Login', '2019-09-09 19:15:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (952, 'P000001', 'Login', '2019-09-09 19:38:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (953, 'P000001', 'Login', '2019-09-10 07:56:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (954, 'P000001', 'Login', '2019-09-10 08:08:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (955, 'P000001', 'Login', '2019-09-10 08:10:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (956, 'P000001', 'Login', '2019-09-10 08:29:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (957, 'P000001', 'Login', '2019-09-10 08:36:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (958, 'P000001', 'Login', '2019-09-10 08:41:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (959, 'P000001', 'Login', '2019-09-10 08:41:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (960, 'P000001', 'Login', '2019-09-10 09:18:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (961, 'P000001', 'Login', '2019-09-10 10:14:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (962, 'P000001', 'Login', '2019-09-10 10:57:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (963, 'P000001', 'Login', '2019-09-10 11:12:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (964, 'P000001', 'Logout', '2019-09-10 11:28:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (965, 'P000001', 'Login', '2019-09-10 12:50:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (966, 'P000001', 'Login', '2019-09-10 13:21:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (967, 'P000001', 'Login', '2019-09-10 14:03:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (968, 'P000001', 'Login', '2019-09-10 15:27:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (969, 'P000001', 'Login', '2019-09-10 15:32:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (970, 'P000001', 'Login', '2019-09-10 15:39:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (971, 'P000001', 'Login', '2019-09-10 15:49:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (972, 'P000001', 'Login', '2019-09-11 07:36:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (973, '', 'Logout', '2019-09-11 08:03:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (974, 'P000001', 'Login', '2019-09-11 08:05:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (975, 'P000001', 'Login', '2019-09-11 08:05:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (976, 'P000001', 'Login', '2019-09-11 08:16:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (977, 'P000001', 'Logout', '2019-09-11 08:17:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (978, 'P000001', 'Login', '2019-09-11 08:18:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (979, 'P000001', 'Logout', '2019-09-11 08:18:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (980, '', 'Logout', '2019-09-11 08:19:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (981, '', 'Logout', '2019-09-11 08:21:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (982, '', 'Logout', '2019-09-11 08:21:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (983, '', 'Logout', '2019-09-11 08:21:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (984, '', 'Logout', '2019-09-11 08:21:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (985, '', 'Logout', '2019-09-11 08:21:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (986, '', 'Logout', '2019-09-11 08:22:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (987, '', 'Logout', '2019-09-11 08:22:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (988, '', 'Logout', '2019-09-11 08:23:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (989, 'P000001', 'Login', '2019-09-11 08:25:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (990, '', 'Logout', '2019-09-11 08:25:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (991, '', 'Logout', '2019-09-11 08:25:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (992, '', 'Logout', '2019-09-11 08:25:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (993, '', 'Logout', '2019-09-11 08:25:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (994, '', 'Logout', '2019-09-11 08:25:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (995, '', 'Logout', '2019-09-11 08:25:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (996, '', 'Logout', '2019-09-11 08:25:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (997, 'P000001', 'Login', '2019-09-11 08:26:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (998, 'P000001', 'Login', '2019-09-11 08:27:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (999, 'P000001', 'Logout', '2019-09-11 08:28:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1000, 'P000001', 'Login', '2019-09-11 08:29:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1001, 'P000001', 'Login', '2019-09-11 08:29:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1002, 'P000001', 'Login', '2019-09-11 08:34:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1003, 'P000001', 'Login', '2019-09-11 09:01:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1004, 'P000001', 'Login', '2019-09-11 09:06:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1005, 'P000001', 'Login', '2019-09-11 09:37:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1006, 'P000001', 'Login', '2019-09-11 09:41:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1007, '', 'Logout', '2019-09-11 09:44:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1008, 'P000001', 'Login', '2019-09-11 09:44:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1009, 'P000001', 'Login', '2019-09-11 09:46:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1010, 'P000001', 'Login', '2019-09-11 09:46:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1011, 'P000001', 'Login', '2019-09-11 09:46:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1012, 'P000001', 'Login', '2019-09-11 09:46:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1013, 'P000001', 'Login', '2019-09-11 09:46:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1014, 'P000001', 'Login', '2019-09-11 10:19:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1015, 'P000001', 'Login', '2019-09-11 12:31:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1016, 'P000001', 'Login', '2019-09-11 12:38:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1017, 'P000001', 'Logout', '2019-09-11 12:52:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1018, '', 'Logout', '2019-09-11 13:54:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1019, 'P000001', 'Login', '2019-09-11 13:54:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1020, 'P000001', 'Login', '2019-09-11 13:57:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1021, 'P000001', 'Login', '2019-09-11 14:32:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1022, 'P000001', 'Login', '2019-09-11 14:46:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1023, 'P000001', 'Logout', '2019-09-11 14:46:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1024, '', 'Logout', '2019-09-11 14:46:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1025, 'P000001', 'Login', '2019-09-11 14:46:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1026, 'P000001', 'Logout', '2019-09-11 15:23:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1027, '', 'Logout', '2019-09-11 15:25:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1028, '', 'Logout', '2019-09-11 15:38:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1029, '', 'Logout', '2019-09-11 15:38:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1030, '', 'Logout', '2019-09-11 15:39:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1031, '', 'Logout', '2019-09-11 15:39:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1032, '', 'Logout', '2019-09-11 15:41:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1033, 'P000001', 'Login', '2019-09-11 15:53:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1034, 'P000001', 'Logout', '2019-09-11 15:55:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1035, 'P000001', 'Login', '2019-09-11 16:05:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1036, 'P000001', 'Login', '2019-09-11 16:54:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1037, 'P000001', 'Login', '2019-09-11 17:05:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1038, 'P000001', 'Login', '2019-09-11 19:07:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1039, 'P000001', 'Login', '2019-09-12 08:05:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1040, 'P000001', 'Login', '2019-09-12 08:08:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1041, 'P000001', 'Login', '2019-09-12 08:19:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1042, 'P000001', 'Login', '2019-09-12 08:22:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1043, 'P000001', 'Login', '2019-09-12 08:38:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1044, 'P000001', 'Login', '2019-09-12 08:40:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1045, 'P000001', 'Login', '2019-09-12 08:41:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1046, 'P000001', 'Login', '2019-09-12 08:51:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1047, 'P000001', 'Login', '2019-09-12 08:55:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1048, 'P000001', 'Login', '2019-09-12 08:56:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1049, 'P000001', 'Login', '2019-09-12 09:43:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1050, 'P000001', 'Login', '2019-09-12 10:02:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1051, 'P000001', 'Login', '2019-09-12 10:08:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1052, 'P000001', 'Login', '2019-09-12 11:01:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1053, 'P000001', 'Login', '2019-09-12 11:03:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1054, 'P000001', 'Login', '2019-09-12 11:10:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1055, 'P000001', 'Logout', '2019-09-12 13:00:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1056, 'P000001', 'Login', '2019-09-12 13:52:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1057, 'P000001', 'Login', '2019-09-12 14:00:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1058, 'P000001', 'Login', '2019-09-12 14:02:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1059, 'P000001', 'Login', '2019-09-12 14:36:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1060, 'P000001', 'Login', '2019-09-12 15:25:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1061, '', 'Logout', '2019-09-12 16:09:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1062, 'P000001', 'Login', '2019-09-12 16:09:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1063, 'P000001', 'Login', '2019-09-13 08:06:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1064, 'P000001', 'Login', '2019-09-13 08:13:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1065, 'P000001', 'Login', '2019-09-13 08:27:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1066, 'P000001', 'Login', '2019-09-13 08:29:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1067, 'P000001', 'Login', '2019-09-13 08:37:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1068, '', 'Logout', '2019-09-13 08:38:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1069, 'P000001', 'Login', '2019-09-13 08:51:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1070, 'P000001', 'Login', '2019-09-13 09:29:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1071, 'P000001', 'Login', '2019-09-13 09:35:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1072, 'P000001', 'Login', '2019-09-13 10:07:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1073, 'P000001', 'Logout', '2019-09-13 11:13:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1074, 'P000001', 'Login', '2019-09-13 13:23:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1075, 'P000001', 'Login', '2019-09-13 13:26:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1076, 'P000001', 'Login', '2019-09-13 13:59:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1077, 'P000001', 'Login', '2019-09-13 14:11:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1078, 'P000001', 'Login', '2019-09-13 14:11:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1079, '', 'Logout', '2019-09-13 14:26:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1080, 'P000001', 'Login', '2019-09-13 14:26:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1081, '', 'Logout', '2019-09-13 14:26:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1082, 'P000001', 'Login', '2019-09-13 15:18:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1083, 'P000001', 'Logout', '2019-09-13 17:42:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1084, 'P000001', 'Login', '2019-09-14 07:54:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1085, 'P000001', 'Login', '2019-09-16 08:19:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1086, 'P000001', 'Login', '2019-09-16 08:26:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1087, 'P000001', 'Login', '2019-09-16 08:35:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1088, 'P000001', 'Logout', '2019-09-16 08:35:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1089, 'P000001', 'Login', '2019-09-16 08:35:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1090, 'P000001', 'Login', '2019-09-16 08:41:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1091, 'P000001', 'Login', '2019-09-16 09:14:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1092, 'P000001', 'Login', '2019-09-16 09:21:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1093, 'P000001', 'Login', '2019-09-16 09:33:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1094, 'P000001', 'Login', '2019-09-16 09:43:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1095, 'P000001', 'Login', '2019-09-16 09:47:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1096, 'P000001', 'Login', '2019-09-16 10:18:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1097, 'P000001', 'Login', '2019-09-16 10:50:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1098, 'P000001', 'Login', '2019-09-16 11:29:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1099, 'P000001', 'Login', '2019-09-16 11:33:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1100, 'P000001', 'Login', '2019-09-16 13:41:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1101, 'P000001', 'Login', '2019-09-16 13:59:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1102, '', 'Logout', '2019-09-16 14:00:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1103, 'P000001', 'Login', '2019-09-16 14:00:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1104, 'P000001', 'Login', '2019-09-16 14:24:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1105, 'P000001', 'Logout', '2019-09-16 14:29:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1106, 'P000001', 'Login', '2019-09-16 14:37:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1107, 'P000001', 'Login', '2019-09-16 15:53:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1108, 'P000001', 'Login', '2019-09-16 16:15:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1109, 'P000001', 'Logout', '2019-09-16 16:16:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1110, 'P000001', 'Login', '2019-09-16 16:40:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1111, 'P000001', 'Login', '2019-09-16 16:43:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1112, 'P000001', 'Login', '2019-09-16 17:10:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1113, 'P000001', 'Login', '2019-09-16 17:20:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1114, 'P000001', 'Login', '2019-09-16 17:22:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1115, 'P000001', 'Login', '2019-09-16 17:32:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1116, 'P000001', 'Login', '2019-09-16 17:36:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1117, 'P000001', 'Logout', '2019-09-16 18:09:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1118, 'P000001', 'Login', '2019-09-17 08:19:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1119, 'P000001', 'Login', '2019-09-17 08:29:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1120, 'P000001', 'Login', '2019-09-17 08:29:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1121, '', 'Logout', '2019-09-17 08:36:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1122, 'P000001', 'Login', '2019-09-17 08:37:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1123, 'P000001', 'Login', '2019-09-17 08:40:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1124, 'P000001', 'Login', '2019-09-17 08:43:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1125, 'P000001', 'Login', '2019-09-17 09:03:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1126, 'P000001', 'Login', '2019-09-17 09:21:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1127, 'P000001', 'Login', '2019-09-17 09:28:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1128, 'P000001', 'Login', '2019-09-17 09:37:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1129, 'P000001', 'Logout', '2019-09-17 10:05:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1130, 'P000001', 'Login', '2019-09-17 11:02:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1131, 'P000001', 'Login', '2019-09-17 13:12:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1132, 'P000001', 'Login', '2019-09-17 13:17:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1133, '', 'Logout', '2019-09-17 13:18:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1134, 'P000001', 'Login', '2019-09-17 13:19:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1135, 'P000001', 'Login', '2019-09-17 13:22:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1136, 'P000001', 'Login', '2019-09-17 13:23:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1137, 'P000001', 'Logout', '2019-09-17 13:23:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1138, 'P000001', 'Login', '2019-09-17 13:55:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1139, 'P000001', 'Login', '2019-09-17 14:04:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1140, '', 'Logout', '2019-09-17 14:24:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1141, 'P000001', 'Login', '2019-09-17 14:24:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1142, 'P000001', 'Login', '2019-09-17 14:54:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1143, 'P000001', 'Logout', '2019-09-17 15:15:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1144, 'P000001', 'Login', '2019-09-17 15:17:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1145, '', 'Logout', '2019-09-17 15:18:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1146, 'P000001', 'Login', '2019-09-17 15:18:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1147, 'P000001', 'Login', '2019-09-17 15:37:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1148, '', 'Logout', '2019-09-17 15:52:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1149, 'P000001', 'Login', '2019-09-17 15:52:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1150, 'P000001', 'Login', '2019-09-17 15:54:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1151, 'P000001', 'Logout', '2019-09-17 16:46:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1152, '', 'Logout', '2019-09-17 16:46:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1153, 'P000001', 'Login', '2019-09-17 17:25:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1154, 'P000001', 'Login', '2019-09-17 18:19:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1155, 'P000001', 'Logout', '2019-09-17 18:55:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1156, 'P000001', 'Login', '2019-09-17 18:58:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1157, 'P000001', 'Login', '2019-09-17 19:32:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1158, 'P000001', 'Login', '2019-09-17 19:47:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1159, 'P000001', 'Login', '2019-09-17 20:01:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1160, 'P000001', 'Login', '2019-09-17 20:12:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1161, 'P000001', 'Login', '2019-09-17 20:22:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1162, 'P000001', 'Logout', '2019-09-17 20:58:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1163, 'P000001', 'Login', '2019-09-17 21:09:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1164, 'P000001', 'Login', '2019-09-17 21:13:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1165, 'P000001', 'Login', '2019-09-17 21:17:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1166, '', 'Logout', '2019-09-17 21:19:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1167, 'P000001', 'Login', '2019-09-17 21:20:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1168, '', 'Logout', '2019-09-18 08:18:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1169, 'P000001', 'Login', '2019-09-18 08:18:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1170, '', 'Logout', '2019-09-18 08:28:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1171, 'P000001', 'Login', '2019-09-18 08:29:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1172, 'P000001', 'Login', '2019-09-18 08:33:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1173, 'P000001', 'Login', '2019-09-18 08:38:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1174, 'P000001', 'Login', '2019-09-18 08:40:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1175, 'P000001', 'Login', '2019-09-18 08:41:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1176, 'P000001', 'Login', '2019-09-18 09:27:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1177, '', 'Logout', '2019-09-18 09:33:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1178, 'P000001', 'Login', '2019-09-18 09:33:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1179, 'P000001', 'Logout', '2019-09-18 10:15:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1180, 'P000001', 'Login', '2019-09-18 10:23:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1181, 'P000001', 'Login', '2019-09-18 10:44:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1182, 'P000001', 'Login', '2019-09-18 10:48:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1183, 'P000001', 'Login', '2019-09-18 10:59:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1184, 'P000001', 'Login', '2019-09-18 11:05:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1185, 'P000001', 'Login', '2019-09-18 11:06:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1186, 'P000001', 'Login', '2019-09-18 13:06:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1187, 'P000001', 'Login', '2019-09-18 13:15:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1188, '', 'Logout', '2019-09-18 13:27:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1189, 'P000001', 'Login', '2019-09-18 13:27:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1190, 'P000001', 'Login', '2019-09-18 14:13:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1191, 'P000001', 'Logout', '2019-09-18 14:37:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1192, 'P000001', 'Login', '2019-09-18 14:37:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1193, 'P000001', 'Login', '2019-09-18 15:11:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1194, 'P000001', 'Login', '2019-09-18 15:23:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1195, 'P000001', 'Logout', '2019-09-18 16:45:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1196, 'P000001', 'Login', '2019-09-18 18:17:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1197, 'P000001', 'Logout', '2019-09-18 19:04:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1198, '', 'Logout', '2019-09-18 19:58:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1199, 'P000001', 'Login', '2019-09-18 19:58:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1200, 'P000001', 'Login', '2019-09-19 08:25:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1201, 'P000001', 'Login', '2019-09-19 08:52:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1202, 'P000001', 'Login', '2019-09-19 08:54:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1203, 'P000001', 'Login', '2019-09-19 08:54:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1204, 'P000001', 'Login', '2019-09-19 09:04:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1205, 'P000001', 'Login', '2019-09-19 09:26:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1206, 'P000001', 'Login', '2019-09-19 09:31:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1207, 'P000001', 'Login', '2019-09-19 09:42:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1208, 'P000001', 'Login', '2019-09-19 10:13:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1209, 'P000001', 'Login', '2019-09-19 11:21:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1210, 'P000001', 'Login', '2019-09-19 13:07:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1211, 'P000001', 'Login', '2019-09-19 13:09:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1212, 'P000001', 'Login', '2019-09-19 13:36:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1213, 'P000001', 'Login', '2019-09-19 13:54:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1214, 'P000001', 'Login', '2019-09-19 13:57:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1215, 'P000001', 'Login', '2019-09-19 14:00:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1216, 'P000001', 'Login', '2019-09-19 14:12:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1217, 'P000001', 'Login', '2019-09-19 14:36:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1218, '', 'Logout', '2019-09-19 15:31:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1219, 'P000001', 'Login', '2019-09-19 15:31:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1220, '', 'Logout', '2019-09-19 15:48:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1221, 'P000001', 'Login', '2019-09-19 15:48:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1222, 'P000001', 'Logout', '2019-09-19 15:51:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1223, '', 'Logout', '2019-09-19 15:51:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1224, 'P000001', 'Login', '2019-09-19 15:51:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1225, 'P000001', 'Login', '2019-09-19 16:59:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1226, 'P000001', 'Login', '2019-09-19 17:28:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1227, 'P000001', 'Logout', '2019-09-19 17:36:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1228, 'P000001', 'Login', '2019-09-20 08:11:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1229, 'P000001', 'Login', '2019-09-20 08:18:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1230, 'P000001', 'Login', '2019-09-20 08:20:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1231, '', 'Logout', '2019-09-20 08:26:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1232, 'P000001', 'Login', '2019-09-20 08:26:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1233, 'P000001', 'Login', '2019-09-20 08:26:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1234, 'P000001', 'Login', '2019-09-20 08:28:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1235, 'P000001', 'Login', '2019-09-20 08:32:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1236, 'P000001', 'Login', '2019-09-20 08:40:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1237, 'P000001', 'Login', '2019-09-20 08:46:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1238, 'P000001', 'Login', '2019-09-20 08:53:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1239, 'P000001', 'Login', '2019-09-20 09:00:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1240, 'P000001', 'Logout', '2019-09-20 09:00:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1241, 'P000001', 'Login', '2019-09-20 09:37:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1242, 'P000001', 'Login', '2019-09-20 09:37:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1243, 'P000001', 'Login', '2019-09-20 10:53:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1244, '', 'Logout', '2019-09-20 13:08:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1245, '', 'Logout', '2019-09-20 13:08:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1246, 'P000001', 'Login', '2019-09-20 13:08:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1247, 'P000001', 'Login', '2019-09-20 13:39:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1248, 'P000001', 'Login', '2019-09-20 13:57:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1249, 'P000001', 'Login', '2019-09-20 13:58:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1250, 'P000001', 'Login', '2019-09-20 14:05:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1251, '', 'Logout', '2019-09-20 15:17:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1252, 'P000001', 'Login', '2019-09-20 15:17:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1253, 'P000001', 'Login', '2019-09-20 15:19:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1254, 'P000001', 'Login', '2019-09-20 16:01:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1255, 'P000001', 'Login', '2019-09-20 16:02:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1256, 'P000001', 'Logout', '2019-09-20 16:53:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1257, 'P000001', 'Login', '2019-09-20 17:32:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1258, 'P000001', 'Login', '2019-09-20 18:19:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1259, 'P000001', 'Login', '2019-09-20 19:07:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1260, 'P000001', 'Logout', '2019-09-20 19:41:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1261, 'P000001', 'Login', '2019-09-21 10:11:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1262, 'P000001', 'Login', '2019-09-22 14:59:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1263, 'P000001', 'Login', '2019-09-23 08:16:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1264, 'P000001', 'Login', '2019-09-23 08:37:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1265, 'P000001', 'Login', '2019-09-23 08:38:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1266, 'P000001', 'Login', '2019-09-23 09:25:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1267, '', 'Logout', '2019-09-23 09:32:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1268, 'P000001', 'Login', '2019-09-23 09:33:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1269, 'P000001', 'Login', '2019-09-23 09:40:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1270, 'P000001', 'Login', '2019-09-23 09:42:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1271, 'P000001', 'Login', '2019-09-23 10:07:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1272, 'P000001', 'Logout', '2019-09-23 10:12:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1273, 'P000001', 'Login', '2019-09-23 11:18:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1274, 'P000001', 'Login', '2019-09-23 11:53:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1275, 'P000001', 'Login', '2019-09-23 13:40:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1276, 'P000001', 'Login', '2019-09-23 14:11:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1277, '', 'Logout', '2019-09-23 14:40:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1278, 'P000001', 'Login', '2019-09-23 14:40:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1279, 'P000001', 'Login', '2019-09-23 15:18:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1280, 'P000001', 'Login', '2019-09-23 16:07:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1281, 'P000001', 'Login', '2019-09-23 16:39:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1282, 'P000001', 'Logout', '2019-09-23 16:50:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1283, 'P000001', 'Login', '2019-09-23 17:54:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1284, 'P000001', 'Login', '2019-09-23 19:18:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1285, '', 'Logout', '2019-09-23 19:44:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1286, 'P000001', 'Login', '2019-09-23 19:48:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1287, 'P000001', 'Logout', '2019-09-23 20:12:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1288, 'P000001', 'Login', '2019-09-24 08:49:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1289, 'P000001', 'Login', '2019-09-24 08:57:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1290, 'P000001', 'Login', '2019-09-24 09:16:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1291, 'P000001', 'Login', '2019-09-24 09:25:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1292, 'P000001', 'Login', '2019-09-24 09:28:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1293, 'P000001', 'Login', '2019-09-24 09:37:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1294, '', 'Logout', '2019-09-24 10:05:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1295, 'P000001', 'Login', '2019-09-24 10:06:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1296, 'P000001', 'Login', '2019-09-24 10:38:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1297, 'P000001', 'Login', '2019-09-24 11:28:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1298, '', 'Logout', '2019-09-24 13:32:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1299, 'P000001', 'Login', '2019-09-24 13:32:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1300, 'P000001', 'Logout', '2019-09-24 13:36:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1301, 'P000001', 'Login', '2019-09-24 13:36:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1302, 'P000001', 'Login', '2019-09-24 13:38:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1303, 'P000001', 'Login', '2019-09-24 14:09:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1304, '', 'Logout', '2019-09-24 14:10:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1305, 'P000001', 'Login', '2019-09-24 14:10:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1306, 'P000001', 'Login', '2019-09-24 15:12:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1307, 'P000001', 'Login', '2019-09-24 17:22:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1308, 'P000001', 'Login', '2019-09-24 18:22:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1309, '', 'Logout', '2019-09-24 19:24:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1310, 'P000001', 'Login', '2019-09-24 19:24:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1311, 'P000001', 'Login', '2019-09-25 08:00:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1312, '', 'Logout', '2019-09-25 08:26:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1313, 'P000001', 'Login', '2019-09-25 08:38:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1314, 'P000001', 'Login', '2019-09-25 09:08:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1315, 'P000001', 'Login', '2019-09-25 09:17:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1316, 'P000001', 'Login', '2019-09-25 09:20:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1317, 'P000001', 'Login', '2019-09-25 09:47:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1318, 'P000001', 'Logout', '2019-09-25 09:53:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1319, 'P000001', 'Login', '2019-09-25 09:53:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1320, 'P000001', 'Login', '2019-09-25 10:05:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1321, 'P000001', 'Logout', '2019-09-25 10:06:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1322, 'P000001', 'Login', '2019-09-25 10:06:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1323, 'P000001', 'Login', '2019-09-25 11:07:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1324, 'P000001', 'Login', '2019-09-25 11:55:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1325, '', 'Logout', '2019-09-25 12:30:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1326, 'P000001', 'Login', '2019-09-25 12:40:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1327, '', 'Logout', '2019-09-25 12:47:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1328, '', 'Logout', '2019-09-25 12:51:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1329, '', 'Logout', '2019-09-25 12:51:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1330, 'P000001', 'Login', '2019-09-25 12:51:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1331, 'P000001', 'Login', '2019-09-25 13:53:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1332, 'P000001', 'Login', '2019-09-25 15:05:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1333, '', 'Logout', '2019-09-25 15:20:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1334, 'P000001', 'Login', '2019-09-25 15:20:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1335, 'P000001', 'Login', '2019-09-25 15:23:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1336, 'P000001', 'Login', '2019-09-25 16:13:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1337, 'P000001', 'Logout', '2019-09-25 16:30:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1338, '', 'Logout', '2019-09-25 17:40:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1339, '', 'Logout', '2019-09-25 17:40:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1340, 'P000001', 'Login', '2019-09-25 17:40:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1341, 'P000001', 'Login', '2019-09-25 18:14:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1342, '', 'Logout', '2019-09-25 18:47:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1343, 'P000001', 'Login', '2019-09-25 18:47:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1344, 'P000001', 'Logout', '2019-09-25 19:32:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1345, 'P000001', 'Login', '2019-09-25 19:40:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1346, '', 'Logout', '2019-09-25 21:16:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1347, 'P000001', 'Login', '2019-09-25 21:16:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1348, 'P000001', 'Login', '2019-09-26 09:36:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1349, 'P000001', 'Login', '2019-09-26 10:32:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1350, 'P000001', 'Login', '2019-09-26 10:57:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1351, 'P000001', 'Login', '2019-09-27 11:30:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1352, 'P000001', 'Login', '2019-09-28 18:29:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1353, 'P000001', 'Login', '2019-09-28 19:49:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1354, 'P000001', 'Login', '2019-09-28 19:52:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1355, 'P000001', 'Logout', '2019-09-28 19:52:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1356, 'P000001', 'Login', '2019-09-28 19:52:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1357, 'P000001', 'Logout', '2019-09-28 19:52:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1358, '', 'Logout', '2019-09-28 19:52:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1359, '', 'Logout', '2019-09-28 19:52:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1360, 'P000001', 'Login', '2019-09-28 19:52:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1361, 'P000001', 'Login', '2019-09-28 19:53:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1362, 'P000001', 'Login', '2019-09-28 19:57:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1363, 'P000001', 'Login', '2019-09-28 19:57:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1364, 'P000001', 'Login', '2019-09-28 19:57:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1365, 'P000001', 'Login', '2019-09-28 19:57:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1366, 'P000001', 'Login', '2019-09-28 19:58:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1367, 'P000001', 'Logout', '2019-09-28 20:00:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1368, '', 'Logout', '2019-09-28 20:02:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1369, 'P000001', 'Login', '2019-09-28 20:02:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1370, 'P000001', 'Login', '2019-09-28 20:03:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1371, 'P000001', 'Login', '2019-09-28 20:05:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1372, 'P000001', 'Logout', '2019-09-28 20:05:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1373, 'P000001', 'Login', '2019-09-28 20:05:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1374, 'P000001', 'Login', '2019-09-28 20:06:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1375, 'P000001', 'Login', '2019-09-28 20:07:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1376, 'P000001', 'Logout', '2019-09-28 20:07:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1377, 'P000001', 'Login', '2019-09-28 20:07:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1378, 'P000001', 'Login', '2019-09-28 21:27:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1379, 'P000001', 'Logout', '2019-09-28 21:27:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1380, '', 'Logout', '2019-09-28 23:06:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1381, '', 'Logout', '2019-09-28 23:07:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1382, '', 'Logout', '2019-09-29 10:38:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1383, 'P000001', 'Login', '2019-09-30 14:57:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1384, 'P000001', 'Login', '2019-09-30 15:38:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1385, 'P000001', 'Login', '2019-10-01 09:46:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1386, 'P000001', 'Login', '2019-10-01 15:02:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1387, 'P000001', 'Login', '2019-10-03 13:38:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1388, 'P000001', 'Login', '2019-10-03 13:39:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1389, 'P000001', 'Login', '2019-10-03 13:54:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1390, 'P000001', 'Login', '2019-10-03 14:11:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1391, 'P000001', 'Login', '2019-10-03 14:11:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1392, 'P000001', 'Login', '2019-10-03 14:11:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1393, 'P000001', 'Login', '2019-10-03 14:11:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1394, 'P000001', 'Login', '2019-10-03 14:12:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1395, 'P000001', 'Login', '2019-10-03 14:12:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1396, 'P000001', 'Login', '2019-10-03 14:15:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1397, 'P000001', 'Login', '2019-10-03 14:18:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1398, 'P000001', 'Login', '2019-10-03 14:18:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1399, 'P000001', 'Login', '2019-10-03 14:24:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1400, 'P000001', 'Login', '2019-10-03 14:24:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1401, 'P000001', 'Login', '2019-10-03 14:24:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1402, 'P000001', 'Login', '2019-10-03 14:24:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1403, 'P000001', 'Login', '2019-10-03 14:35:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1404, 'P000001', 'Login', '2019-10-03 14:40:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1405, 'P000001', 'Login', '2019-10-03 15:31:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1406, 'P000001', 'Login', '2019-10-03 16:26:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1407, 'P000001', 'Login', '2019-10-03 16:27:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1408, 'P000001', 'Login', '2019-10-03 16:27:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1409, 'P000001', 'Login', '2019-10-03 16:27:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1410, 'P000001', 'Login', '2019-10-03 17:13:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1411, 'P000001', 'Login', '2019-10-03 17:32:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1412, 'P000001', 'Login', '2019-10-03 19:00:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1413, 'P000001', 'Login', '2019-10-04 08:44:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1414, 'P000001', 'Login', '2019-10-04 08:48:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1415, 'P000001', 'Login', '2019-10-04 08:49:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1416, 'P000001', 'Login', '2019-10-04 08:49:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1417, 'P000001', 'Login', '2019-10-04 08:49:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1418, 'P000001', 'Login', '2019-10-04 09:30:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1419, 'P000001', 'Login', '2019-10-04 09:31:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1420, 'P000001', 'Login', '2019-10-04 09:31:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1421, 'P000001', 'Login', '2019-10-04 09:31:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1422, 'P000001', 'Login', '2019-10-04 09:38:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1423, 'P000001', 'Login', '2019-10-04 09:38:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1424, 'P000001', 'Login', '2019-10-04 09:42:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1425, 'P000001', 'Login', '2019-10-04 09:43:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1426, 'P000001', 'Login', '2019-10-04 09:46:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1427, 'P000001', 'Login', '2019-10-04 09:47:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1428, 'P000001', 'Login', '2019-10-04 09:58:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1429, 'P000001', 'Login', '2019-10-04 09:59:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1430, 'P000001', 'Login', '2019-10-04 09:59:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1431, 'P000001', 'Login', '2019-10-04 10:01:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1432, 'P000001', 'Login', '2019-10-04 10:06:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1433, 'P000001', 'Login', '2019-10-04 10:06:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1434, 'P000001', 'Logout', '2019-10-04 10:15:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1435, '', 'Logout', '2019-10-04 10:15:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1436, '', 'Logout', '2019-10-04 10:15:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1437, 'P000001', 'Login', '2019-10-04 10:15:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1438, 'P000001', 'Login', '2019-10-04 10:18:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1439, 'P000001', 'Login', '2019-10-04 10:47:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1440, 'P000001', 'Login', '2019-10-04 10:48:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1441, 'P000001', 'Login', '2019-10-04 10:53:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1442, 'P000001', 'Login', '2019-10-04 10:53:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1443, 'P000001', 'Login', '2019-10-04 10:54:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1444, 'P000001', 'Login', '2019-10-04 10:54:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1445, 'P000001', 'Login', '2019-10-04 10:56:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1446, 'P000001', 'Login', '2019-10-04 10:56:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1447, 'P000001', 'Login', '2019-10-04 10:56:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1448, 'P000001', 'Login', '2019-10-04 10:56:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1449, 'P000001', 'Login', '2019-10-04 10:56:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1450, 'P000001', 'Login', '2019-10-04 11:01:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1451, 'P000001', 'Login', '2019-10-04 11:01:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1452, 'P000001', 'Login', '2019-10-04 11:02:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1453, 'P000001', 'Login', '2019-10-04 11:03:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1454, 'P000001', 'Login', '2019-10-04 11:04:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1455, 'P000001', 'Login', '2019-10-04 11:06:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1456, 'P000001', 'Login', '2019-10-04 11:06:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1457, 'P000001', 'Login', '2019-10-04 11:07:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1458, 'P000001', 'Login', '2019-10-04 11:07:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1459, 'P000001', 'Login', '2019-10-04 11:08:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1460, 'P000001', 'Login', '2019-10-04 11:09:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1461, 'P000001', 'Login', '2019-10-04 11:10:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1462, 'P000001', 'Login', '2019-10-04 11:11:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1463, 'P000001', 'Login', '2019-10-04 11:11:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1464, 'P000001', 'Login', '2019-10-04 11:11:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1465, 'P000001', 'Login', '2019-10-04 11:12:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1466, 'P000001', 'Login', '2019-10-04 11:13:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1467, 'P000001', 'Login', '2019-10-04 11:14:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1468, 'P000001', 'Login', '2019-10-04 11:15:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1469, 'P000001', 'Login', '2019-10-04 11:15:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1470, 'P000001', 'Login', '2019-10-04 14:27:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1471, 'P000001', 'Login', '2019-10-04 14:29:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1472, 'P000001', 'Login', '2019-10-07 08:51:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1473, 'P000001', 'Login', '2019-10-07 10:43:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1474, 'P000001', 'Login', '2019-10-07 10:44:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1475, 'P000001', 'Login', '2019-10-07 10:44:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1476, 'P000001', 'Logout', '2019-10-07 10:45:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1477, '', 'Logout', '2019-10-07 10:45:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1478, 'P000001', 'Login', '2019-10-07 11:31:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1479, 'P000001', 'Login', '2019-10-07 11:31:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1480, 'P000001', 'Login', '2019-10-07 11:35:13+07');
INSERT INTO "public"."tbl_loguser" VALUES (1481, 'P000001', 'Login', '2019-10-07 11:39:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1482, 'P000001', 'Login', '2019-10-07 11:46:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1483, '', 'Logout', '2019-10-07 12:35:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1484, 'P000001', 'Login', '2019-10-07 12:36:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1485, 'P000001', 'Login', '2019-10-07 12:38:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1486, 'P000001', 'Login', '2019-10-07 12:41:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1487, 'P000001', 'Login', '2019-10-07 12:42:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1488, 'P000001', 'Login', '2019-10-07 12:42:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1489, 'P000001', 'Login', '2019-10-07 12:45:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1490, 'P000001', 'Login', '2019-10-07 12:46:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1491, 'P000001', 'Login', '2019-10-07 12:49:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1492, 'P000001', 'Login', '2019-10-07 12:49:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1493, 'P000001', 'Login', '2019-10-07 12:49:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1494, 'P000001', 'Login', '2019-10-07 12:49:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1495, 'P000001', 'Login', '2019-10-07 12:50:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1496, 'P000001', 'Login', '2019-10-07 12:53:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1497, 'P000001', 'Login', '2019-10-07 12:59:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1498, 'P000001', 'Logout', '2019-10-07 13:05:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1499, 'P000001', 'Login', '2019-10-07 13:05:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1500, 'P000001', 'Logout', '2019-10-07 13:05:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1501, 'P000001', 'Login', '2019-10-07 13:06:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1502, 'P000001', 'Login', '2019-10-07 13:06:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1503, 'P000001', 'Login', '2019-10-07 13:06:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1504, 'P000001', 'Login', '2019-10-07 13:09:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1505, 'P000001', 'Login', '2019-10-07 13:10:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1506, 'P000001', 'Login', '2019-10-07 13:10:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1507, 'P000001', 'Login', '2019-10-07 13:10:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1508, 'P000001', 'Logout', '2019-10-07 13:11:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1509, 'P000001', 'Login', '2019-10-07 13:12:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1510, 'P000001', 'Login', '2019-10-07 13:14:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1511, 'P000001', 'Login', '2019-10-07 14:48:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1512, 'P000001', 'Logout', '2019-10-07 17:16:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1513, 'P000001', 'Login', '2019-10-07 17:16:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1514, '', 'Logout', '2019-10-07 19:59:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1515, '', 'Logout', '2019-10-08 08:17:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1516, 'P000001', 'Login', '2019-10-08 08:17:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1517, 'P000001', 'Login', '2019-10-08 10:58:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1518, 'P000001', 'Login', '2019-10-09 06:27:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1519, 'P000001', 'Login', '2019-10-09 08:39:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1520, 'P000001', 'Login', '2019-10-09 08:44:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1521, 'P000001', 'Login', '2019-10-09 13:44:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1522, 'P000001', 'Login', '2019-10-09 14:40:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1523, 'P000001', 'Login', '2019-10-09 14:42:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1524, 'P000001', 'Login', '2019-10-10 08:15:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1525, 'P000001', 'Login', '2019-10-10 08:16:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1526, 'P000001', 'Login', '2019-10-10 09:39:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1527, 'P000001', 'Login', '2019-10-10 13:38:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1528, 'P000001', 'Login', '2019-10-10 21:23:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1529, 'P000001', 'Login', '2019-10-11 08:17:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1530, 'P000001', 'Login', '2019-10-11 08:23:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1531, 'P000001', 'Login', '2019-10-11 11:09:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1532, 'P000001', 'Login', '2019-10-11 12:56:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1533, 'P000001', 'Login', '2019-10-11 14:00:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1534, 'P000001', 'Login', '2019-10-12 13:52:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1535, 'P000001', 'Logout', '2019-10-12 13:52:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1536, 'P000001', 'Login', '2019-10-12 16:36:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1537, 'P000001', 'Login', '2019-10-14 08:49:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1538, 'P000001', 'Login', '2019-10-14 15:13:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1539, 'P000001', 'Login', '2019-10-15 08:20:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1540, 'P000001', 'Login', '2019-10-15 11:14:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1541, 'P000001', 'Login', '2019-10-15 14:45:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1542, 'P000001', 'Login', '2019-10-15 14:47:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1543, 'P000001', 'Login', '2019-10-15 14:57:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1544, 'P000001', 'Login', '2019-10-15 14:58:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1545, 'P000001', 'Login', '2019-10-15 19:10:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1546, 'P000001', 'Login', '2019-10-16 08:44:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1547, 'P000001', 'Login', '2019-10-16 09:25:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1548, 'P000001', 'Login', '2019-10-16 15:30:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1549, 'P000001', 'Login', '2019-10-17 08:25:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1550, 'P000001', 'Login', '2019-10-17 09:13:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1551, 'P000001', 'Login', '2019-10-18 08:18:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1552, 'P000001', 'Login', '2019-10-18 08:19:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1553, 'P000001', 'Login', '2019-10-18 09:03:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1554, 'P000001', 'Login', '2019-10-18 15:14:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1555, 'P000001', 'Login', '2019-10-18 22:59:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1556, 'P000001', 'Logout', '2019-10-18 23:22:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1557, 'P000001', 'Login', '2019-10-21 08:19:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1558, 'P000001', 'Login', '2019-10-21 10:44:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1559, 'P000001', 'Login', '2019-10-21 13:04:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1560, 'P000001', 'Login', '2019-10-21 14:52:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1561, 'P000001', 'Login', '2019-10-21 16:46:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1562, 'P000001', 'Login', '2019-10-21 16:49:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1563, 'P000001', 'Login', '2019-10-21 16:49:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1564, 'P000001', 'Login', '2019-10-21 16:49:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1565, 'P000001', 'Login', '2019-10-21 17:47:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1566, '', 'Logout', '2019-10-21 19:03:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1567, 'P000001', 'Login', '2019-10-21 19:03:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1568, 'P000001', 'Login', '2019-10-21 19:04:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1569, '', 'Logout', '2019-10-21 22:55:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1570, 'P000001', 'Login', '2019-10-21 22:55:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1571, 'P000001', 'Logout', '2019-10-21 23:38:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1572, 'P000001', 'Login', '2019-10-21 23:38:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1573, 'P000001', 'Login', '2019-10-22 09:13:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1574, 'P000001', 'Login', '2019-10-22 09:23:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1575, 'P000001', 'Login', '2019-10-22 10:56:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1578, 'P000001', 'Login', '2019-10-30 16:48:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1579, 'P000001', 'Login', '2019-10-30 16:50:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1580, 'P000001', 'Login', '2019-10-30 16:51:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1581, 'P000001', 'Login', '2019-10-30 16:57:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1582, 'P000001', 'Login', '2019-10-30 16:57:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1583, 'P000001', 'Login', '2019-10-30 16:57:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1584, 'P000001', 'Login', '2019-10-30 16:58:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1585, 'P000001', 'Login', '2019-10-30 16:58:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1586, 'P000001', 'Login', '2019-10-31 08:02:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1587, 'P000001', 'Login', '2019-10-31 08:29:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1588, 'P000001', 'Login', '2019-10-31 08:33:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1589, '', 'Logout', '2019-10-31 08:52:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1590, 'P000001', 'Login', '2019-10-31 08:52:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1591, 'P000001', 'Login', '2019-10-31 09:16:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1592, '', 'Logout', '2019-10-31 13:43:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1593, 'P000001', 'Login', '2019-10-31 13:44:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1594, 'P000001', 'Login', '2019-10-31 15:04:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1595, '', 'Logout', '2019-10-31 16:29:30+07');
INSERT INTO "public"."tbl_loguser" VALUES (1596, 'P000001', 'Login', '2019-10-31 16:29:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1597, 'P000001', 'Logout', '2019-10-31 16:30:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1598, 'P000001', 'Login', '2019-10-31 22:02:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1599, 'P000001', 'Logout', '2019-10-31 22:20:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1600, 'P000001', 'Login', '2019-11-01 08:23:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1601, 'P000001', 'Login', '2019-11-01 09:06:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1602, '', 'Logout', '2019-11-01 13:23:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1603, 'P000001', 'Login', '2019-11-01 13:24:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1604, 'P000001', 'Login', '2019-11-01 13:25:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1605, 'P000001', 'Login', '2019-11-01 14:49:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1606, '', 'Logout', '2019-11-01 15:40:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1607, 'P000001', 'Login', '2019-11-01 15:40:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1608, 'P000001', 'Login', '2019-11-02 23:10:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1609, 'P000001', 'Login', '2019-11-04 08:22:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1610, 'P000001', 'Login', '2019-11-04 08:45:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1611, 'P000001', 'Login', '2019-11-04 08:51:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1612, 'P000001', 'Login', '2019-11-04 10:19:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1613, '', 'Logout', '2019-11-04 12:59:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1614, 'P000001', 'Login', '2019-11-04 13:00:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1615, 'P000001', 'Login', '2019-11-04 13:55:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1616, 'P000001', 'Login', '2019-11-04 16:29:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1617, 'P000001', 'Login', '2019-11-04 20:14:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1618, 'P000001', 'Login', '2019-11-05 08:03:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1619, '', 'Logout', '2019-11-05 10:41:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1620, 'P000001', 'Login', '2019-11-05 10:42:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1621, 'P000001', 'Logout', '2019-11-05 11:00:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1622, 'P000001', 'Login', '2019-11-05 11:00:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1623, 'P000001', 'Login', '2019-11-05 15:19:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1624, 'P000001', 'Login', '2019-11-05 16:54:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1625, 'P000001', 'Login', '2019-11-06 08:17:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1626, 'P000001', 'Login', '2019-11-06 08:21:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1627, 'P000001', 'Login', '2019-11-06 10:15:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1628, '', 'Logout', '2019-11-06 12:12:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1629, 'P000001', 'Login', '2019-11-06 12:12:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1630, 'P000001', 'Login', '2019-11-06 13:46:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1631, '', 'Logout', '2019-11-07 08:15:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1632, 'P000001', 'Login', '2019-11-07 08:24:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1633, 'P000001', 'Login', '2019-11-07 09:08:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1634, '', 'Logout', '2019-11-07 13:12:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1635, 'P000001', 'Login', '2019-11-07 13:12:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1636, 'P000001', 'Login', '2019-11-08 09:50:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1637, 'P000001', 'Login', '2019-11-09 13:55:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1638, 'P000001', 'Login', '2019-11-11 08:19:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1639, 'P000001', 'Login', '2019-11-11 10:14:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1640, 'P000001', 'Login', '2019-11-11 10:45:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1641, 'P000001', 'Logout', '2019-11-11 13:04:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1642, 'P000001', 'Login', '2019-11-11 13:04:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1643, 'P000001', 'Login', '2019-11-11 15:16:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1644, 'P000001', 'Login', '2019-11-11 15:36:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1645, 'P000001', 'Login', '2019-11-12 08:09:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1646, 'P000001', 'Login', '2019-11-12 08:11:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1647, 'P000001', 'Login', '2019-11-12 10:18:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1648, 'P000001', 'Login', '2019-11-12 10:47:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1649, '', 'Logout', '2019-11-12 12:49:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1650, 'P000001', 'Login', '2019-11-12 12:49:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1651, 'P000001', 'Login', '2019-11-12 13:24:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1652, 'P000001', 'Login', '2019-11-12 16:28:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1653, '', 'Logout', '2019-11-12 16:37:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1654, 'P000001', 'Login', '2019-11-12 16:37:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1655, 'P000001', 'Login', '2019-11-13 08:39:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1656, 'P000001', 'Login', '2019-11-13 09:15:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1657, 'P000001', 'Login', '2019-11-13 11:46:49+07');
INSERT INTO "public"."tbl_loguser" VALUES (1658, 'P000001', 'Login', '2019-11-13 12:13:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1659, 'P000001', 'Login', '2019-11-13 13:25:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1660, 'P000001', 'Login', '2019-11-13 14:07:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1661, 'P000001', 'Login', '2019-11-13 16:54:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1662, 'P000001', 'Login', '2019-11-13 22:29:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1663, 'P000001', 'Login', '2019-11-13 22:35:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1664, 'P000001', 'Logout', '2019-11-13 23:32:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1665, 'P000001', 'Login', '2019-11-13 23:32:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1666, 'P000001', 'Login', '2019-11-14 07:57:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1667, 'P000001', 'Login', '2019-11-14 09:45:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1668, 'P000001', 'Login', '2019-11-14 09:47:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1669, 'P000001', 'Login', '2019-11-14 09:54:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1670, 'P000001', 'Login', '2019-11-14 09:55:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1671, 'P000001', 'Login', '2019-11-14 09:57:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1672, 'P000001', 'Login', '2019-11-14 10:41:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1673, 'P000001', 'Login', '2019-11-14 10:47:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1674, 'P000001', 'Login', '2019-11-14 10:48:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1675, 'P000001', 'Login', '2019-11-14 11:57:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1676, 'P000001', 'Login', '2019-11-14 11:57:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1677, 'P000001', 'Login', '2019-11-14 12:01:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1678, 'P000001', 'Login', '2019-11-14 13:00:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1679, 'P000001', 'Login', '2019-11-14 13:02:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1680, 'P000001', 'Login', '2019-11-14 15:26:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1681, 'P000001', 'Login', '2019-11-15 08:04:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1682, 'P000001', 'Login', '2019-11-15 09:16:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1683, 'P000001', 'Login', '2019-11-15 09:36:01+07');
INSERT INTO "public"."tbl_loguser" VALUES (1684, 'P000001', 'Logout', '2019-11-15 09:50:52+07');
INSERT INTO "public"."tbl_loguser" VALUES (1685, 'P000001', 'Login', '2019-11-15 10:51:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1686, 'P000001', 'Login', '2019-11-15 13:38:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1687, 'P000001', 'Login', '2019-11-15 13:47:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1688, 'P000001', 'Logout', '2019-11-15 13:48:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1689, 'P000001', 'Login', '2019-11-15 14:05:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1690, 'P000001', 'Logout', '2019-11-15 14:25:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1691, 'P000001', 'Login', '2019-11-18 08:30:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1692, 'P000001', 'Login', '2019-11-18 08:32:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1693, 'P000001', 'Login', '2019-11-18 09:17:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1694, 'P000001', 'Login', '2019-11-18 09:30:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1695, 'P000001', 'Logout', '2019-11-18 10:17:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1696, '', 'Logout', '2019-11-18 10:18:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1697, 'P000001', 'Login', '2019-11-18 10:36:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1698, 'P000001', 'Login', '2019-11-18 10:37:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1699, 'P000001', 'Login', '2019-11-18 11:12:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1700, '', 'Logout', '2019-11-18 11:25:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1701, 'P000001', 'Login', '2019-11-18 11:25:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1702, 'P000001', 'Login', '2019-11-18 12:35:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1703, 'P000001', 'Login', '2019-11-18 13:13:29+07');
INSERT INTO "public"."tbl_loguser" VALUES (1704, 'P000001', 'Login', '2019-11-18 13:35:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1705, 'P000001', 'Login', '2019-11-19 08:11:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1706, 'P000001', 'Login', '2019-11-19 08:13:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1707, 'P000001', 'Login', '2019-11-19 08:38:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1708, 'P000001', 'Logout', '2019-11-19 08:48:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1709, 'P000001', 'Login', '2019-11-19 09:23:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1710, 'P000001', 'Logout', '2019-11-19 09:27:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1711, 'P000001', 'Login', '2019-11-19 10:04:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1712, 'P000001', 'Login', '2019-11-19 10:22:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1713, 'P000001', 'Login', '2019-11-19 10:34:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1714, 'P000001', 'Login', '2019-11-19 12:38:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1715, 'P000001', 'Login', '2019-11-19 15:14:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1716, 'P000001', 'Login', '2019-11-20 08:11:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1717, 'P000001', 'Login', '2019-11-20 08:13:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1718, 'P000001', 'Login', '2019-11-20 08:21:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1719, 'P000001', 'Login', '2019-11-20 08:37:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1720, 'P000001', 'Login', '2019-11-20 09:36:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1721, 'P000001', 'Login', '2019-11-20 10:03:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1722, 'P000001', 'Login', '2019-11-20 10:06:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1723, 'P000001', 'Login', '2019-11-20 13:25:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1724, 'P000001', 'Login', '2019-11-20 13:27:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1725, 'P000001', 'Login', '2019-11-20 13:29:26+07');
INSERT INTO "public"."tbl_loguser" VALUES (1726, 'P000001', 'Login', '2019-11-20 13:40:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1727, 'P000001', 'Login', '2019-11-20 13:53:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1728, 'P000001', 'Logout', '2019-11-20 13:59:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1729, 'P000001', 'Login', '2019-11-20 14:08:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1730, 'P000001', 'Login', '2019-11-20 14:44:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1731, 'P000001', 'Logout', '2019-11-20 15:12:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1732, 'P000001', 'Login', '2019-11-20 15:21:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1733, 'P000001', 'Login', '2019-11-20 15:50:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1734, 'P000001', 'Login', '2019-11-20 15:51:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1735, 'P000001', 'Login', '2019-11-20 16:18:39+07');
INSERT INTO "public"."tbl_loguser" VALUES (1736, 'P000001', 'Logout', '2019-11-20 16:19:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1737, 'P000001', 'Login', '2019-11-20 16:34:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1738, 'P000001', 'Login', '2019-11-21 08:15:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1739, 'P000001', 'Login', '2019-11-21 08:30:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1740, 'P000001', 'Logout', '2019-11-21 08:34:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1741, 'P000001', 'Login', '2019-11-21 08:35:15+07');
INSERT INTO "public"."tbl_loguser" VALUES (1742, 'P000001', 'Login', '2019-11-21 09:58:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1743, 'P000001', 'Login', '2019-11-21 09:59:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1744, 'P000001', 'Login', '2019-11-21 12:23:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1745, '', 'Logout', '2019-11-21 16:03:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1746, 'P000001', 'Login', '2019-11-21 16:03:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1747, 'P000001', 'Login', '2019-11-22 08:11:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1748, 'P000001', 'Login', '2019-11-22 09:10:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1749, 'P000001', 'Login', '2019-11-22 10:27:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1750, '', 'Logout', '2019-11-22 10:39:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1751, 'P000001', 'Login', '2019-11-22 10:39:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1752, 'P000001', 'Login', '2019-11-22 12:59:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1753, 'P000001', 'Login', '2019-11-22 13:17:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1754, 'P000001', 'Logout', '2019-11-22 13:32:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1755, '', 'Logout', '2019-11-22 13:38:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1756, 'P000001', 'Login', '2019-11-22 13:38:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1757, 'P000001', 'Login', '2019-11-22 14:41:33+07');
INSERT INTO "public"."tbl_loguser" VALUES (1758, 'P000001', 'Login', '2019-11-22 15:49:16+07');
INSERT INTO "public"."tbl_loguser" VALUES (1759, 'P000001', 'Login', '2019-11-25 08:18:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1760, 'P000001', 'Login', '2019-11-25 08:31:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1761, 'P000001', 'Login', '2019-11-25 08:35:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1762, 'P000001', 'Login', '2019-11-25 09:38:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1763, '', 'Logout', '2019-11-25 12:47:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1764, 'P000001', 'Login', '2019-11-25 12:49:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1765, 'P000001', 'Login', '2019-11-25 12:49:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1766, 'P000001', 'Login', '2019-11-25 13:47:18+07');
INSERT INTO "public"."tbl_loguser" VALUES (1767, 'P000001', 'Login', '2019-11-25 14:46:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1768, 'P000001', 'Login', '2019-11-25 14:48:44+07');
INSERT INTO "public"."tbl_loguser" VALUES (1769, 'P000001', 'Login', '2019-11-25 15:35:41+07');
INSERT INTO "public"."tbl_loguser" VALUES (1770, 'P000001', 'Login', '2019-11-26 09:00:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1771, 'P000001', 'Login', '2019-11-26 11:49:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1772, 'P000001', 'Login', '2019-11-26 12:24:51+07');
INSERT INTO "public"."tbl_loguser" VALUES (1773, 'P000001', 'Login', '2019-11-26 12:39:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1774, 'P000001', 'Login', '2019-11-26 14:24:32+07');
INSERT INTO "public"."tbl_loguser" VALUES (1775, 'P000001', 'Login', '2019-11-27 08:11:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1776, 'P000001', 'Login', '2019-11-27 08:20:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1777, 'P000001', 'Login', '2019-11-27 08:20:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1778, 'P000001', 'Logout', '2019-11-27 08:44:55+07');
INSERT INTO "public"."tbl_loguser" VALUES (1779, 'P000001', 'Login', '2019-11-27 08:44:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1780, 'P000001', 'Logout', '2019-11-27 08:45:25+07');
INSERT INTO "public"."tbl_loguser" VALUES (1781, 'P000001', 'Login', '2019-11-27 08:45:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1782, 'P000001', 'Login', '2019-11-27 10:46:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1783, 'P000001', 'Logout', '2019-11-27 10:52:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1784, 'P000001', 'Login', '2019-11-27 13:12:34+07');
INSERT INTO "public"."tbl_loguser" VALUES (1785, 'P000001', 'Logout', '2019-11-27 13:23:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1786, 'P000001', 'Login', '2019-11-27 13:25:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1787, 'P000001', 'Login', '2019-11-27 14:18:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1788, 'P000001', 'Login', '2019-11-28 09:08:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1789, 'P000001', 'Login', '2019-11-28 09:16:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1790, 'P000001', 'Login', '2019-11-28 10:03:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1791, 'P000001', 'Login', '2019-11-28 10:15:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1792, 'P000001', 'Login', '2019-11-28 10:34:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1793, 'P000001', 'Login', '2019-11-28 10:42:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1794, 'P000001', 'Login', '2019-11-28 12:45:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1795, 'P000001', 'Login', '2019-11-28 13:35:05+07');
INSERT INTO "public"."tbl_loguser" VALUES (1796, 'P000001', 'Login', '2019-11-28 14:40:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1797, 'P000001', 'Logout', '2019-11-28 14:58:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1798, 'P000001', 'Login', '2019-11-28 15:16:43+07');
INSERT INTO "public"."tbl_loguser" VALUES (1799, '', 'Logout', '2019-11-28 15:21:57+07');
INSERT INTO "public"."tbl_loguser" VALUES (1800, 'P000001', 'Login', '2019-11-28 15:21:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1801, '', 'Logout', '2019-11-29 08:21:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1802, 'P000001', 'Login', '2019-11-29 08:21:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1803, 'P000001', 'Login', '2019-11-29 08:49:59+07');
INSERT INTO "public"."tbl_loguser" VALUES (1804, 'P000001', 'Login', '2019-11-29 08:53:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1805, 'P000001', 'Login', '2019-11-29 08:56:54+07');
INSERT INTO "public"."tbl_loguser" VALUES (1806, 'P000001', 'Logout', '2019-11-29 09:23:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1807, 'P000001', 'Login', '2019-11-29 09:26:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1808, 'P000001', 'Login', '2019-11-29 10:53:08+07');
INSERT INTO "public"."tbl_loguser" VALUES (1809, '', 'Logout', '2019-11-29 13:03:07+07');
INSERT INTO "public"."tbl_loguser" VALUES (1810, 'P000001', 'Login', '2019-11-29 13:03:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1811, 'P000001', 'Login', '2019-11-29 13:53:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1812, '', 'Logout', '2019-11-29 16:35:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1813, 'P000001', 'Login', '2019-11-29 16:35:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1814, 'P000001', 'Login', '2019-12-02 08:21:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1815, 'P000001', 'Login', '2019-12-02 09:07:04+07');
INSERT INTO "public"."tbl_loguser" VALUES (1816, 'P000001', 'Login', '2019-12-02 12:54:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1817, 'P000001', 'Login', '2019-12-02 14:20:48+07');
INSERT INTO "public"."tbl_loguser" VALUES (1818, 'P000001', 'Login', '2019-12-02 14:29:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1819, 'P000001', 'Login', '2019-12-02 14:40:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1820, 'P000001', 'Login', '2019-12-03 08:36:56+07');
INSERT INTO "public"."tbl_loguser" VALUES (1821, 'P000001', 'Login', '2019-12-03 08:55:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1822, 'P000001', 'Login', '2019-12-03 09:08:27+07');
INSERT INTO "public"."tbl_loguser" VALUES (1823, 'P000001', 'Login', '2019-12-03 09:53:21+07');
INSERT INTO "public"."tbl_loguser" VALUES (1824, 'P000001', 'Login', '2019-12-03 12:41:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1825, 'P000001', 'Login', '2019-12-03 13:02:28+07');
INSERT INTO "public"."tbl_loguser" VALUES (1826, 'P000001', 'Login', '2019-12-04 08:24:37+07');
INSERT INTO "public"."tbl_loguser" VALUES (1827, 'P000001', 'Login', '2019-12-04 08:26:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1828, 'P000001', 'Login', '2019-12-04 08:27:50+07');
INSERT INTO "public"."tbl_loguser" VALUES (1829, 'P000001', 'Login', '2019-12-04 08:28:22+07');
INSERT INTO "public"."tbl_loguser" VALUES (1830, 'P000001', 'Login', '2019-12-04 09:02:00+07');
INSERT INTO "public"."tbl_loguser" VALUES (1831, 'P000001', 'Logout', '2019-12-04 10:46:36+07');
INSERT INTO "public"."tbl_loguser" VALUES (1832, 'P000001', 'Login', '2019-12-04 10:46:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1833, 'P000001', 'Login', '2019-12-04 10:46:46+07');
INSERT INTO "public"."tbl_loguser" VALUES (1834, 'P000001', 'Login', '2019-12-04 12:47:03+07');
INSERT INTO "public"."tbl_loguser" VALUES (1835, 'P000001', 'Login', '2019-12-04 12:49:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1836, 'P000001', 'Login', '2019-12-04 14:13:19+07');
INSERT INTO "public"."tbl_loguser" VALUES (1837, 'P000001', 'Login', '2019-12-04 14:25:10+07');
INSERT INTO "public"."tbl_loguser" VALUES (1838, 'P000001', 'Logout', '2019-12-04 14:26:06+07');
INSERT INTO "public"."tbl_loguser" VALUES (1839, '', 'Logout', '2019-12-04 15:23:14+07');
INSERT INTO "public"."tbl_loguser" VALUES (1840, 'P000001', 'Login', '2019-12-04 15:23:17+07');
INSERT INTO "public"."tbl_loguser" VALUES (1841, 'P000001', 'Login', '2019-12-04 15:29:38+07');
INSERT INTO "public"."tbl_loguser" VALUES (1842, 'P000001', 'Login', '2019-12-04 16:22:40+07');
INSERT INTO "public"."tbl_loguser" VALUES (1843, 'P000001', 'Login', '2019-12-04 18:53:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1844, 'P000001', 'Login', '2019-12-05 08:08:58+07');
INSERT INTO "public"."tbl_loguser" VALUES (1845, 'P000001', 'Login', '2019-12-05 08:50:23+07');
INSERT INTO "public"."tbl_loguser" VALUES (1846, 'P000001', 'Login', '2019-12-05 09:14:02+07');
INSERT INTO "public"."tbl_loguser" VALUES (1847, 'P000001', 'Login', '2019-12-05 09:54:47+07');
INSERT INTO "public"."tbl_loguser" VALUES (1848, 'P000001', 'Login', '2019-12-05 10:22:42+07');
INSERT INTO "public"."tbl_loguser" VALUES (1849, 'P000001', 'Login', '2019-12-05 10:23:12+07');
INSERT INTO "public"."tbl_loguser" VALUES (1850, 'P000001', 'Login', '2019-12-05 11:54:53+07');
INSERT INTO "public"."tbl_loguser" VALUES (1851, 'P000001', 'Login', '2019-12-05 13:27:11+07');
INSERT INTO "public"."tbl_loguser" VALUES (1852, 'P000001', 'Login', '2019-12-06 08:28:35+07');
INSERT INTO "public"."tbl_loguser" VALUES (1853, 'P000001', 'Logout', '2019-12-06 08:59:24+07');
INSERT INTO "public"."tbl_loguser" VALUES (1854, 'P000001', 'Login', '2019-12-06 09:52:09+07');
INSERT INTO "public"."tbl_loguser" VALUES (1855, 'P000001', 'Login', '2019-12-06 14:38:20+07');
INSERT INTO "public"."tbl_loguser" VALUES (1856, 'P000001', 'Login', '2019-12-08 18:07:31+07');
INSERT INTO "public"."tbl_loguser" VALUES (1857, 'P000001', 'Login', '2019-12-09 09:28:45+07');
INSERT INTO "public"."tbl_loguser" VALUES (1858, 'P000001', 'Login', '2019-12-09 09:44:06+07');

-- ----------------------------
-- Table structure for tbl_mata_uang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mata_uang";
CREATE TABLE "public"."tbl_mata_uang" (
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_mata_uang_IDMataUang_seq"'::regclass),
  "Kode" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Mata_uang" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Negara" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Default" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" timestamp(6)
)
;

-- ----------------------------
-- Records of tbl_mata_uang
-- ----------------------------
INSERT INTO "public"."tbl_mata_uang" VALUES ('P000002', 'Rp. ', 'Rp. ', 'Rp. ', NULL, '1', 'aktif', '2019-12-10 00:00:00');

-- ----------------------------
-- Table structure for tbl_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu";
CREATE TABLE "public"."tbl_menu" (
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_IDMenu_seq"'::regclass),
  "Menu" varchar(80) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Urutan_menu" int2,
  "icon" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO "public"."tbl_menu" VALUES (1, 'Data Master', 1, 'data_master.png');
INSERT INTO "public"."tbl_menu" VALUES (12, 'Pembelian', 3, 'pembelian.png');
INSERT INTO "public"."tbl_menu" VALUES (19, 'Penjualan', 4, 'penjualan.png');
INSERT INTO "public"."tbl_menu" VALUES (20, 'Keuangan', 7, 'keuangan.png');
INSERT INTO "public"."tbl_menu" VALUES (29, 'Asset', 8, 'aset.png');
INSERT INTO "public"."tbl_menu" VALUES (31, 'Control Panel', 10, 'gears.png');
INSERT INTO "public"."tbl_menu" VALUES (30, 'Bahan Baku', 9, 'bahan_baku.png');

-- ----------------------------
-- Table structure for tbl_menu_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_detail";
CREATE TABLE "public"."tbl_menu_detail" (
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenuDetail_seq"'::regclass),
  "IDMenu" int8 NOT NULL DEFAULT nextval('"tbl_menu_detail_IDMenu_seq"'::regclass),
  "Menu_Detail" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Url" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Urutan" int2,
  "Keterangan" varchar(500) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_menu_detail
-- ----------------------------
INSERT INTO "public"."tbl_menu_detail" VALUES (117, 20, 'Mutasi Giro', 'MutasiGiro', 2, 'Mutasi Giro');
INSERT INTO "public"."tbl_menu_detail" VALUES (85, 19, 'Retur Penjualan', 'ReturPenjualan', 5, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (80, 19, 'Faktur Penjualan', 'InvoicePenjualan', 4, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (156, 31, 'Setting Laporan Keuangan', 'ControlPanelKeuangan', 5, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (16, 16, 'Hutang', 'Hutang', 19, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (22, 16, 'Piutang', 'Piutang', 20, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (157, 20, 'Laporan Neraca', 'LaporanNeraca', 4, 'suatu bagian dari laporan keuangan suatu perusahaan atau entitas bisnis yang dihasilkan dalam suatu periode akuntansi dimana menunjukkan posisi atas keuangan perusahaan tersebut pada akhir periode akuntansi tersebut yang bisa menjadi dasar dalam menghasilkan keputusan bisnis.');
INSERT INTO "public"."tbl_menu_detail" VALUES (158, 20, 'Laporan Laba/Rugi', 'LaporanLabaRugi', 5, 'bagian dari laporan keuangan suatu perusahaan yang dihasilkan pada suatu periode akuntansi yang menjabarkan unsur-unsur pendapatan dan beban perusahaan sehingga menghasilkan suatu laba (atau rugi) bersih.');
INSERT INTO "public"."tbl_menu_detail" VALUES (93, 20, 'Pembayaran Hutang', 'PembayaranHutang', 3, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (94, 20, 'Penerimaan Piutang', 'PenerimaanPiutang', 4, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (47, 12, 'Retur Pembelian', 'ReturPembelian', 40, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (72, 28, 'Stok Opname', 'StokOpName', 84, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (123, 18, 'Laporan Pindah Gudang', 'PindahGudang/laporan', 5, 'Dimana laporan ini untuk mengetahui secara detail barang – barang yang melakukan perpindahan antar gudang satu sama lainnya.');
INSERT INTO "public"."tbl_menu_detail" VALUES (122, 1, 'Satuan Konversi', 'SatuanKonversi', 17, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (124, 20, 'Laporan Mutasi Giro', 'MutasiGiro/laporan', 5, 'menampilkan semua transaksi yang terjadi pada rekening bank pada periode tertentu. Pada laporan ini, Anda dapat melihat jumlah saldo mana yang sudah dicairkan, dibatalkan maupun pergantian giro');
INSERT INTO "public"."tbl_menu_detail" VALUES (125, 18, 'Laporan Koreksi Persediaan', 'KoreksiPersediaan/laporan', 1, 'untuk mengetahui data detail mengenai persediaan yang ada di dalam catatan pembukuan dan yang ada di gudang apakah benar-benar sama nilainya atau malah ada selisih kelebihan/kekurangan persediaan barang dagang setelah melakukan koreksi persediaan. ');
INSERT INTO "public"."tbl_menu_detail" VALUES (15, 1, 'Harga Jual Barang', 'HargaJualBarang', 18, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (159, 31, 'Setting Kode Transaksi', 'KodeTransaksi', 6, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (13, 17, 'Group User', 'GroupUser', NULL, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (19, 17, 'Menu', 'Menu', NULL, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (20, 17, 'Menu Detail', 'MenuDetail', NULL, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (27, 17, 'User', 'User', NULL, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (3, 1, 'Bank', 'Bank', 3, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (18, 1, 'Kota', 'Kota', 5, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (25, 1, 'Supplier', 'Supplier', 8, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (11, 1, 'Group Customer', 'GroupCustomer', 9, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (7, 1, 'Customer', 'Customer', 10, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (10, 1, 'Group Barang', 'GroupBarang', 11, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (24, 1, 'Satuan', 'Satuan', 15, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (4, 1, 'Barang', 'Barang', 16, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (102, 19, 'Laporan Sales Order', 'SalesOrder/laporan', 68, 'Menampilkan Informasi yang memperlihatkan Order Penjualan dari Customer yang memesan barang kepada perusahaan untuk diproses dengan Rincian penting Nama Customer, Nama Barang, Jumlah Barang yang di pesan.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (44, 12, 'Faktur Pembelian', 'InvoicePembelian', 37, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (73, 28, 'Laporan Stok Opname', 'StokOpName/laporan_stokopname', 85, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.');
INSERT INTO "public"."tbl_menu_detail" VALUES (53, 12, 'Laporan Pesanan Pembelian', 'Po/laporan_po', 44, 'Menampilkan Informasi yang memperlihatkan detail perusahaan yang terlibat dan tanggal pesanan, Laporan Pesanan Pembelian juga berisi rincian penting tentang barang yang akan dibeli dengan Nama Barang, Jumlah Barang yang dibeli, Harga, dan juga ketentuan tambahan untuk penjualan seperti diskon.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (82, 20, 'Laporan Pembayaran Hutang', 'PembayaranHutang/pencarian_lap_pembayaran_hutang', 4, 'Laporan Pembayaran Utang adalah laporan yang berisikan informasi mengenai kewajiban perusahaan tentang pembayaran utang yang dibayarkan kepada pemasok/supplier.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (121, 12, 'Laporan Retur Pembelian', 'ReturPembelian/laporan_retur_pembelian', 55, 'Berisi Informasi pengembalian barang yang rusak atau tidak sesuai dengan keinginan pembeli kepada penjual yang dilakukan oleh pembeli. Transaksi retur pembelian menyebabkan nominal utang pembeli.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (56, 12, 'Laporan Penerimaan Barang', 'Laporan_penerimaan_barang/index', 48, 'Berisi dari data hasil pemeriksaan atas barang yang diterima dan dokumen-dokumen yang terkait, antara lain : Tanggal penerimaan barang, Nomor Pesanan Pembelian yang bersangkutan, Nama Barang dan Jumlah Barang yang diterima.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (110, 19, 'Laporan Retur Penjualan', 'ReturPenjualan/laporan', 75, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.');
INSERT INTO "public"."tbl_menu_detail" VALUES (143, 18, 'Laporan Stok', 'Laporan_stok', 109, 'Laporan dimana untuk menampilkan stok akhir dan pergerakan stok secara detail dan sesuai dengan transaksi yang dibuat sehingga user gudang maupun user yang diberi hak akses ini dapat melihat stok secara mudah dan cepat.');
INSERT INTO "public"."tbl_menu_detail" VALUES (153, 31, 'Setting User', 'ControlPanelUser', 4, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (154, 31, 'Setting Group User', 'ControlPanelGroupUser', 3, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (12, 1, 'Group Supplier', 'GroupSupplier', 7, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (142, 1, 'Mata Uang', 'Matauang', 4, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (119, 12, 'Laporan Faktur Pembelian', 'InvoicePembelian/laporan_invoice', 53, 'Berisi informasi tentang pemesanan dari perusahaan kepada pemasok/supplier. Informasi tersebut adalah nomor order, tanggal transaksi, nama pemasok/supplier, rincian nama barang dan jumlah barang, harga dan total barang yang dipesan.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (141, 0, 'Ukuran', 'Ukuran', 14, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (145, 0, 'Tipe', 'Tipe', 13, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (140, 0, 'Kategori', 'Kategori', 12, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (146, 0, 'Formula', 'Formula', 19, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (148, 0, 'Persetujuan Pesanan Pembelian', 'PersetujuanPesanan', 28, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (155, 0, 'Persetujuan Sales Order', 'PersetujuanSalesOrder', 2, 'Persetujuan Sales Order untuk Grup Customer Khusus');
INSERT INTO "public"."tbl_menu_detail" VALUES (112, 19, 'Laporan Surat Jalan', 'SuratJalan/laporan', 71, 'Berisi dari data hasil Sales Order dan dokumen – dokumen yang terkait antara lain : Nomor Surat Jalan, Tanggal Surat Jalan, Nama Customer, Alamat Customer untuk informasi pengiriman, Nama Barang, Jumlah Barang yang di pesan.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (147, 30, 'Penerimaan Barang Jadi', 'BarangJadi', 2, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (144, 30, 'Produksi Bahan Baku', 'Bahan_baku/produksi', 1, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (108, 19, 'Laporan Faktur Penjualan', 'InvoicePenjualan/laporan', 73, 'Berisi bukti tagihan atau bukti transaksi kepada pelanggan atas pembelian suatu barang/ jasa. Faktur Penjualan biasanya dikirim oleh Perusahaan bersamaan dengan atau setelah pengiriman barang/ jasa. Dan menunjukkan daftar kronologis dari semua faktur, pemesanan, penawaran, dan pembayaran Anda untuk rentang tanggal yang dipilih.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (84, 20, 'Laporan Penerimaan Piutang', 'PenerimaanPiutang/laporan', 5, 'Laporan Penerimaan Piutang berisi tentang informasi mana saja Customer yang sudang melakukan pembayaran hutangnya kepada perusahaan. Dan di catat sebagai Pelunasan Piutang dari Customer.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (152, 20, 'Laporan Jurnal Transaksi', 'Jurnal', 6, 'Laporan Jurnal Transaksi menampilkan semua jenis transaksi yang berhubungan dengan Keuangan/Jurnal yang akan di catat sebagai acuan keluar masuknya transaksi atau debet kreditnya pencatatan keuangan dari semua jenis transksi.
');
INSERT INTO "public"."tbl_menu_detail" VALUES (150, 30, 'Pemakaian Barang', 'PemakaianBarang', 3, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (149, 31, 'Setting Dasar', 'ControlPanel', 1, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (151, 31, 'Setting COA', 'ControlPanelCoa', 2, NULL);
INSERT INTO "public"."tbl_menu_detail" VALUES (160, 12, 'Update No Pajak', 'UpdatePajakPembelian', 38, NULL);

-- ----------------------------
-- Table structure for tbl_menu_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_menu_role";
CREATE TABLE "public"."tbl_menu_role" (
  "IDMenuRole" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuRole_seq"'::regclass),
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDGroupUser_seq"'::regclass),
  "IDMenuDetail" int8 NOT NULL DEFAULT nextval('"tbl_menu_role_IDMenuDetail_seq"'::regclass)
)
;

-- ----------------------------
-- Records of tbl_menu_role
-- ----------------------------
INSERT INTO "public"."tbl_menu_role" VALUES (1, 8, 153);
INSERT INTO "public"."tbl_menu_role" VALUES (2, 8, 117);
INSERT INTO "public"."tbl_menu_role" VALUES (3, 8, 75);
INSERT INTO "public"."tbl_menu_role" VALUES (4, 8, 78);
INSERT INTO "public"."tbl_menu_role" VALUES (5, 8, 85);
INSERT INTO "public"."tbl_menu_role" VALUES (6, 8, 80);
INSERT INTO "public"."tbl_menu_role" VALUES (7, 8, 16);
INSERT INTO "public"."tbl_menu_role" VALUES (8, 8, 22);
INSERT INTO "public"."tbl_menu_role" VALUES (9, 8, 158);
INSERT INTO "public"."tbl_menu_role" VALUES (10, 8, 39);
INSERT INTO "public"."tbl_menu_role" VALUES (11, 8, 93);
INSERT INTO "public"."tbl_menu_role" VALUES (12, 8, 94);
INSERT INTO "public"."tbl_menu_role" VALUES (13, 8, 47);
INSERT INTO "public"."tbl_menu_role" VALUES (14, 8, 120);
INSERT INTO "public"."tbl_menu_role" VALUES (15, 8, 123);
INSERT INTO "public"."tbl_menu_role" VALUES (16, 8, 122);
INSERT INTO "public"."tbl_menu_role" VALUES (17, 8, 124);
INSERT INTO "public"."tbl_menu_role" VALUES (18, 8, 125);
INSERT INTO "public"."tbl_menu_role" VALUES (19, 8, 15);
INSERT INTO "public"."tbl_menu_role" VALUES (20, 8, 13);
INSERT INTO "public"."tbl_menu_role" VALUES (21, 8, 65);
INSERT INTO "public"."tbl_menu_role" VALUES (22, 8, 5);
INSERT INTO "public"."tbl_menu_role" VALUES (23, 8, 3);
INSERT INTO "public"."tbl_menu_role" VALUES (24, 8, 18);
INSERT INTO "public"."tbl_menu_role" VALUES (25, 8, 14);
INSERT INTO "public"."tbl_menu_role" VALUES (26, 8, 25);
INSERT INTO "public"."tbl_menu_role" VALUES (27, 8, 11);
INSERT INTO "public"."tbl_menu_role" VALUES (28, 8, 7);
INSERT INTO "public"."tbl_menu_role" VALUES (29, 8, 10);
INSERT INTO "public"."tbl_menu_role" VALUES (30, 8, 24);
INSERT INTO "public"."tbl_menu_role" VALUES (31, 8, 4);
INSERT INTO "public"."tbl_menu_role" VALUES (32, 8, 102);
INSERT INTO "public"."tbl_menu_role" VALUES (33, 8, 44);
INSERT INTO "public"."tbl_menu_role" VALUES (34, 8, 35);
INSERT INTO "public"."tbl_menu_role" VALUES (35, 8, 73);
INSERT INTO "public"."tbl_menu_role" VALUES (36, 8, 53);
INSERT INTO "public"."tbl_menu_role" VALUES (37, 8, 82);
INSERT INTO "public"."tbl_menu_role" VALUES (38, 8, 97);
INSERT INTO "public"."tbl_menu_role" VALUES (39, 8, 121);
INSERT INTO "public"."tbl_menu_role" VALUES (40, 8, 56);
INSERT INTO "public"."tbl_menu_role" VALUES (41, 8, 110);
INSERT INTO "public"."tbl_menu_role" VALUES (42, 8, 143);
INSERT INTO "public"."tbl_menu_role" VALUES (43, 8, 12);
INSERT INTO "public"."tbl_menu_role" VALUES (44, 8, 142);
INSERT INTO "public"."tbl_menu_role" VALUES (45, 8, 119);
INSERT INTO "public"."tbl_menu_role" VALUES (46, 8, 140);
INSERT INTO "public"."tbl_menu_role" VALUES (47, 8, 118);
INSERT INTO "public"."tbl_menu_role" VALUES (48, 8, 112);
INSERT INTO "public"."tbl_menu_role" VALUES (49, 8, 108);
INSERT INTO "public"."tbl_menu_role" VALUES (50, 8, 84);
INSERT INTO "public"."tbl_menu_role" VALUES (51, 8, 152);
INSERT INTO "public"."tbl_menu_role" VALUES (52, 8, 160);

-- ----------------------------
-- Table structure for tbl_merk
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_merk";
CREATE TABLE "public"."tbl_merk" (
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default",
  "Kode_Merk" varchar(20) COLLATE "pg_catalog"."default",
  "Merk" varchar(100) COLLATE "pg_catalog"."default",
  "Aktif" varchar(20) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_mutasi_giro
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro";
CREATE TABLE "public"."tbl_mutasi_giro" (
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT 0,
  "dibuat_pada" timestamp(0),
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" timestamp(0),
  "diubah_oleh" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_mutasi_giro
-- ----------------------------
INSERT INTO "public"."tbl_mutasi_giro" VALUES ('5ecf2b5eddd37', '2020-05-28', '0001/MG/V/20', 100000, NULL, NULL, 'cair', '0', '2020-05-28 03:09:18', '4', '2020-05-28 03:09:18', '4');
INSERT INTO "public"."tbl_mutasi_giro" VALUES ('5ecf302b5625e', '2020-05-28', '0002/MG/V/20', 30000, NULL, NULL, 'tes', '0', '2020-05-28 03:29:47', '4', '2020-05-28 03:29:47', '4');

-- ----------------------------
-- Table structure for tbl_mutasi_giro_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_mutasi_giro_detail";
CREATE TABLE "public"."tbl_mutasi_giro_detail" (
  "IDMGDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDMG" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGiro" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_lama" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_baru" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_mutasi_giro_detail
-- ----------------------------
INSERT INTO "public"."tbl_mutasi_giro_detail" VALUES ('5ecf2b5eeb02a', '5ecf2b5eddd37', 'Keluar', '5ecf2b149fc10', 'Cair', '12345', '');
INSERT INTO "public"."tbl_mutasi_giro_detail" VALUES ('5ecf302b5bc38', '5ecf302b5625e', 'Keluar', '5ecf2fd039883', 'Cair', '123', '');
INSERT INTO "public"."tbl_mutasi_giro_detail" VALUES ('5ecf302b5e348', '5ecf302b5625e', 'Keluar', '5ecf2f8c3141f', 'Nomor_baru', '234', '567');

-- ----------------------------
-- Table structure for tbl_out
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_out";
CREATE TABLE "public"."tbl_out" (
  "IDOut" int8 NOT NULL DEFAULT nextval('"tbl_out_IDOut_seq"'::regclass),
  "Tanggal" date,
  "Buyer" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CustDes" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "WarnaCust" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Panjang_Yard" float8,
  "Panjang_Meter" float8,
  "Grade" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_packing_list
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list";
CREATE TABLE "public"."tbl_packing_list" (
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_pcs_grade_a" float8,
  "Total_qty_grade_a" float8,
  "Total_pcs_grade_b" float8,
  "Total_qty_grade_b" float8,
  "Total_pcs_grade_s" float8,
  "Total_qty_grade_s" float8,
  "Total_pcs_grade_e" float8,
  "Total_qty_grade_e" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_packing_list_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_packing_list_detail";
CREATE TABLE "public"."tbl_packing_list_detail" (
  "IDPACDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDPAC" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_pemakaian_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pemakaian_barang";
CREATE TABLE "public"."tbl_pemakaian_barang" (
  "IDPakai" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Tanggal" date,
  "Nomor" varchar(255) COLLATE "pg_catalog"."default",
  "Status" varchar(50) COLLATE "pg_catalog"."default",
  "DID" varchar(64) COLLATE "pg_catalog"."default",
  "DTime" timestamp(0),
  "Delete" varchar(10) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pemakaian_barang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pemakaian_barang_detail";
CREATE TABLE "public"."tbl_pemakaian_barang_detail" (
  "IDPakaiDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDPakai" varchar(64) COLLATE "pg_catalog"."default",
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran";
CREATE TABLE "public"."tbl_pembayaran" (
  "IDFBPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFBPembayaran_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDFB_seq"'::regclass),
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembayaran_IDCOA_seq"'::regclass),
  "NominalPembayaran" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_pembayaran_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Giro" date
)
;

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang";
CREATE TABLE "public"."tbl_pembayaran_hutang" (
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Total" float8,
  "Uang_muka" float8,
  "Selisih" float8,
  "Kompensasi_um" float8,
  "Pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_kurs" float8
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang" VALUES ('5ef1d0720e765', '2020-06-23', '0001/PH/VI/20', '29', '1', 14000, 40000, NULL, NULL, NULL, '40000', 0, NULL, 'aktif', NULL);
INSERT INTO "public"."tbl_pembayaran_hutang" VALUES ('5efaa57306828', '2020-06-30', '0002/PH/VI/20', '30', '1', 14000, 300000, NULL, NULL, NULL, '300000', 0, NULL, 'aktif', NULL);

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_bayar";
CREATE TABLE "public"."tbl_pembayaran_hutang_bayar" (
  "IDBSBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date,
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(6),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(6),
  "Nilai_kurs" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang_bayar
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang_bayar" VALUES ('5ef1d07210e76', '5ef1d0720e765', 'cash', 'P000002', NULL, 40000, 'P000002', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembayaran_hutang_bayar" VALUES ('5efaa5730a2c1', '5efaa57306828', 'cash', 'P000001', NULL, 300000, 'P000002', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_pembayaran_hutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembayaran_hutang_detail";
CREATE TABLE "public"."tbl_pembayaran_hutang_detail" (
  "IDBSDet" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDBS" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fb" date,
  "Nilai_fb" float8,
  "Telah_diterima" float8,
  "Diterima" float8,
  "UM" float8,
  "Retur" float8,
  "Saldo" float8,
  "Selisih" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "IDHutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_pembayaran_hutang_detail
-- ----------------------------
INSERT INTO "public"."tbl_pembayaran_hutang_detail" VALUES ('5ef1d07210a8e', '5ef1d0720e765', '5ef1c07ddecfd', '1-0001/FB/VI/20', '2020-06-23', 40000, 40000, 40000, NULL, NULL, NULL, NULL, 'P000002', 1, '5ef1c07e749e5');
INSERT INTO "public"."tbl_pembayaran_hutang_detail" VALUES ('5efaa57309709', '5efaa57306828', '5efaa2f5c5cea', '1-0013/FB/VI/20', '2020-06-30', 300000, 300000, 300000, NULL, NULL, NULL, NULL, 'P000002', 1, '5efaa2f5cb2db');

-- ----------------------------
-- Table structure for tbl_pembelian
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian";
CREATE TABLE "public"."tbl_pembelian" (
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDFB_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDTBS_seq"'::regclass),
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDSupplier_seq"'::regclass),
  "No_sj_supplier" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status" varchar(11) COLLATE "pg_catalog"."default",
  "Status_pakai" int4,
  "is_paid" varchar(1) COLLATE "pg_catalog"."default",
  "no_pajak" varchar(64) COLLATE "pg_catalog"."default",
  "jenis_ppn" int2
)
;

-- ----------------------------
-- Records of tbl_pembelian
-- ----------------------------
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef1c07ddecfd', '2020-06-23', '1-0001/FB/VI/20', '5ef1c07dee701', '29', '23', NULL, NULL, 'P000002', '1', 1, NULL, 1, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', '11', 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef1cd9cbc2c1', '2020-06-23', '1-0002/FB/VI/20', '5ef1cd9cbd261', '30', '88', NULL, NULL, 'P000002', '1', 1, NULL, 1, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', '3636', 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef2cb34517d9', '2020-06-24', '0-0004/FB/VI/20', '5ef2cde74818a', '30', '222', NULL, NULL, 'P000002', '1', 1, NULL, 1, NULL, 0, 'exclude', NULL, 'aktif', '0', NULL, 'f', '111', 0);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef4016f25ad5', '2020-06-25', '1-0004/FB/VI/20', '5ef4016f2668d', '30', 'INV7761-001', NULL, NULL, 'P000002', '1', 1, NULL, 1, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9b00e98079', '2020-06-29', '1-0005/FB/VI/20', '5ef9b00ea6ad9', '30', 'FB123', NULL, NULL, 'P000002', '1', 3, NULL, 3, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9b5c092cce', '2020-06-29', '1-0006/FB/VI/20', '5ef9b5c093692', '29', 'DF1000', NULL, NULL, 'P000002', '1', 3, NULL, 3, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9b9066f87b', '2020-06-29', '1-0007/FB/VI/20', '5ef9b9067023f', '30', NULL, NULL, NULL, 'P000002', '1', 4, NULL, 4, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9b9b29aa67', '2020-06-29', '1-0008/FB/VI/20', '5ef9b9b29bdef', '30', NULL, NULL, NULL, 'P000002', '1', 6000, NULL, 6000, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9bd48eca62', '2020-06-29', '1-0010/FB/VI/20', '5ef9bd48ed61b', '30', NULL, NULL, NULL, 'P000002', '1', 12, NULL, 12, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9bdc2aceb5', '2020-06-29', '1-0011/FB/VI/20', '5ef9bdc2ada6e', '30', NULL, NULL, NULL, 'P000002', '1', 34, NULL, 34, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);
INSERT INTO "public"."tbl_pembelian" VALUES ('5efa92663c3c9', '2020-06-30', '0-0012/FB/VI/20', '5efa92663cf81', '30', 'B123', NULL, NULL, 'P000002', '1', 100, NULL, 100, NULL, 0, 'exclude', NULL, 'aktif', '0', NULL, 'f', NULL, 0);
INSERT INTO "public"."tbl_pembelian" VALUES ('5ef9bbce4e0a9', '2020-06-29', '0-0013/FB/VI/20', '5efa9933a2ec4', '30', 'BR001', NULL, NULL, 'P000002', '1', 1000, NULL, 1000, NULL, 0, 'exclude', NULL, 'aktif', '0', NULL, 'f', NULL, 0);
INSERT INTO "public"."tbl_pembelian" VALUES ('5efaa2f5c5cea', '2020-06-30', '1-0013/FB/VI/20', '5efaa2f5c68a2', '30', 'INV-001', NULL, NULL, 'P000002', '1', 100, NULL, 100, NULL, 0, 'include', NULL, 'aktif', '0', NULL, 'f', NULL, 1);

-- ----------------------------
-- Table structure for tbl_pembelian_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset";
CREATE TABLE "public"."tbl_pembelian_asset" (
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_IDFBA_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Perolehan" float8,
  "Nilai_Buku" float8,
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_asset_detail";
CREATE TABLE "public"."tbl_pembelian_asset_detail" (
  "IDFBADetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBADetail_seq"'::regclass),
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDFBA_seq"'::regclass),
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDAsset_seq"'::regclass),
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_asset_detail_IDGroupAsset_seq"'::regclass),
  "Nilai_perolehan" float8,
  "Akumulasi_penyusutan" float8,
  "Nilai_buku" float8,
  "Metode_penyusutan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_penyusutan" date,
  "Umur" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_detail";
CREATE TABLE "public"."tbl_pembelian_detail" (
  "IDFBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFBDetail_seq"'::regclass),
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDFB_seq"'::regclass),
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDTBSDetail_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDMerk_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDWarna_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Sub_total" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "diskon" float8
)
;

-- ----------------------------
-- Records of tbl_pembelian_detail
-- ----------------------------
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef1c07def2b9', '5ef1c07ddecfd', '24', NULL, NULL, NULL, NULL, '1', '340', '345', '345', 1, NULL, 1, NULL, NULL, NULL, NULL, '2', 40000, 40000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef1cd9cbda31', '5ef1cd9cbc2c1', '30', NULL, NULL, NULL, NULL, '5', '346', '351', '351', 1, NULL, 1, NULL, NULL, NULL, NULL, '2', 50000, 50000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef2cde748d42', '5ef2cb34517d9', '34', NULL, NULL, NULL, NULL, '1', '350', '355', '355', 1, NULL, 1, NULL, NULL, NULL, NULL, '2', 20000, 22000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef4016f26a75', '5ef4016f25ad5', '35', NULL, NULL, NULL, NULL, '1', '351', '356', '356', 1, NULL, 1, NULL, NULL, NULL, NULL, '2', 300000, 300000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9b00eb9997', '5ef9b00e98079', '36', NULL, NULL, NULL, NULL, '3', '352', '357', '357', 3000, NULL, 3000, NULL, NULL, NULL, NULL, '1', 50000, 150000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9b5c094056', '5ef9b5c092cce', '37', NULL, NULL, NULL, NULL, '2', '353', '358', '358', 3000, NULL, 3000, NULL, NULL, NULL, NULL, '1', 50000, 150000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9b90670c03', '5ef9b9066f87b', '38', NULL, NULL, NULL, NULL, '3', '354', '359', '359', 4000, NULL, 4000, NULL, NULL, NULL, NULL, '1', 40000, 160000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9b9b29c7b3', '5ef9b9b29aa67', '39', NULL, NULL, NULL, NULL, '6', '355', '360', '360', 6000, NULL, 6000, NULL, NULL, NULL, NULL, '2', 130000, 780000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9bd48eda03', '5ef9bd48eca62', '41', NULL, NULL, NULL, NULL, '1', '357', '362', '362', 12, NULL, 12, NULL, NULL, NULL, NULL, '2', 29000, 348000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5ef9bdc2ade56', '5ef9bdc2aceb5', '42', NULL, NULL, NULL, NULL, '3', '358', '363', '363', 34, NULL, 34, NULL, NULL, NULL, NULL, '1', 45000, 1530000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5efa92663d369', '5efa92663c3c9', '45', NULL, NULL, NULL, NULL, '7', '361', '366', '366', 100, NULL, 100, NULL, NULL, NULL, NULL, '2', 100000, 10000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5efa9933a695d', '5ef9bbce4e0a9', '46', NULL, NULL, NULL, NULL, '3', '362', '367', '367', 1000, NULL, 1000, NULL, NULL, NULL, NULL, '1', 3000, 3000000, 'P000002', 0);
INSERT INTO "public"."tbl_pembelian_detail" VALUES ('5efaa2f5c7072', '5efaa2f5c5cea', '47', NULL, NULL, NULL, NULL, '3', '363', '368', '368', 100, NULL, 100, NULL, NULL, NULL, NULL, '1', 3000, 300000, 'P000002', 0);

-- ----------------------------
-- Table structure for tbl_pembelian_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_grand_total";
CREATE TABLE "public"."tbl_pembelian_grand_total" (
  "IDFBGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFBGrandTotal_seq"'::regclass),
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_pembelian_grand_total_IDFB_seq"'::regclass),
  "diskonpersen" float8,
  "diskonrupiah" float8,
  "total_invoice_diskon" float8
)
;

-- ----------------------------
-- Records of tbl_pembelian_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef1c07e313b6', '-', 36363.636363636, 0, 3636.3636363636, 40000, 0, '5ef1c07ddecfd', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef1cd9cbf58a', '-', 45454.545454545, 0, 0, 50000, 0, '5ef1cd9cbc2c1', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef2cde74a0ca', '-', 19800, 0, 0, 19800, 0, '5ef2cb34517d9', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef4016f43384', '-', 272727.27272727, 0, 27272.727272727, 300000, 0, '5ef4016f25ad5', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9b00f06c47', '-', 136363636.36364, 0, 13636363.636364, 150000000, 0, '5ef9b00e98079', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9b5c095da2', '-', 136363636.36364, 0, 13636363.636364, 150000000, 0, '5ef9b5c092cce', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9b90671f8b', '-', 145454545.45455, 0, 14545454.545455, 160000000, 0, '5ef9b9066f87b', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9b9b29db3b', '-', 709090909.09091, 0, 70909090.909091, 780000000, 0, '5ef9b9b29aa67', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9bd48ee9a3', '-', 316363.63636364, 0, 31636.363636364, 348000, 0, '5ef9bd48eca62', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5ef9bdc2af1de', '-', 1390909.0909091, 0, 139090.90909091, 1530000, 0, '5ef9bdc2aceb5', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5efa92663e6f1', '-', 10000000, 0, 0, 11000000, 0, '5efa92663c3c9', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5efa9933a80cd', '-', 3000000, 0, 0, 3300000, 0, '5ef9bbce4e0a9', NULL, NULL, NULL);
INSERT INTO "public"."tbl_pembelian_grand_total" VALUES ('5efaa2f5c8bca', '-', 272727.27272727, 0, 0, 300000, 0, '5efaa2f5c5cea', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_pembelian_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum";
CREATE TABLE "public"."tbl_pembelian_umum" (
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_ppn" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Persen_disc" float8,
  "Disc" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "Grand_total" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jatuh_tempo" int8,
  "Tanggal_jatuh_tempo" date,
  "Status_pakai" int4,
  "is_paid" varchar(5) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_detail";
CREATE TABLE "public"."tbl_pembelian_umum_detail" (
  "IDFBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8,
  "Barcode" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_grand_total";
CREATE TABLE "public"."tbl_pembelian_umum_grand_total" (
  "IDFBUmumGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_pembelian_umum_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pembelian_umum_pembayaran";
CREATE TABLE "public"."tbl_pembelian_umum_pembayaran" (
  "IDFBUmumPembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_barang_jadi
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_barang_jadi";
CREATE TABLE "public"."tbl_penerimaan_barang_jadi" (
  "IDPBJ" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default",
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(6),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(6),
  "Status" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_barang_jadi_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_barang_jadi_detail";
CREATE TABLE "public"."tbl_penerimaan_barang_jadi_detail" (
  "IDPBJDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDPBJ" varchar(64) COLLATE "pg_catalog"."default",
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default",
  "IDTipe" varchar(64) COLLATE "pg_catalog"."default",
  "IDUkuran" varchar(64) COLLATE "pg_catalog"."default",
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default",
  "Berat" int8,
  "Total" float8,
  "Qty" int8
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang";
CREATE TABLE "public"."tbl_penerimaan_piutang" (
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Total" float8,
  "Uang_muka" float8,
  "Selisih" float8,
  "Kompensasi_um" float8,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Kelebihan_bayar" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_bayar
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_bayar";
CREATE TABLE "public"."tbl_penerimaan_piutang_bayar" (
  "IDTPBayar" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal_pembayaran" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Status_giro" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_giro" date
)
;

-- ----------------------------
-- Table structure for tbl_penerimaan_piutang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penerimaan_piutang_detail";
CREATE TABLE "public"."tbl_penerimaan_piutang_detail" (
  "IDTPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date,
  "Nilai_fj" float8,
  "Telah_diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Diterima" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "UM" float8,
  "Retur" float8,
  "Saldo" float8,
  "Selisih" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_penerimaan_piutang_detail
-- ----------------------------
INSERT INTO "public"."tbl_penerimaan_piutang_detail" VALUES ('5ee8313b3f80b', '5ee8313b31960', '5ee82a42a0e5a', NULL, NULL, 1650000, '', '', NULL, NULL, NULL, NULL, '1', 14000, '5ee82a42d69c6');

-- ----------------------------
-- Table structure for tbl_penjualan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan";
CREATE TABLE "public"."tbl_penjualan" (
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama_di_faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSJC" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Total_qty" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8,
  "Status_pakai" int4,
  "DPP" float8,
  "PPN" float8,
  "dibuat_pada" timestamp(6),
  "diubah_pada" timestamp(6),
  "no_pajak" varchar(64) COLLATE "pg_catalog"."default",
  "jenis_ppn" int2
)
;

-- ----------------------------
-- Records of tbl_penjualan
-- ----------------------------
INSERT INTO "public"."tbl_penjualan" VALUES ('5ef3ff823e49d', '2020-06-25', '1-0001/FJ/VI/20', 'P000001', 'FK001', NULL, NULL, NULL, 'P000002', 1, 1003, 0, 'include', NULL, '0', 30060000, NULL, 27327272.727273, 2732727.2727273, '2020-06-25 06:08:00', '2020-06-25 06:08:00', NULL, 1);
INSERT INTO "public"."tbl_penjualan" VALUES ('5ef403f323707', '2020-06-25', '1-0002/FJ/VI/20', 'P000001', '1099816542', NULL, NULL, NULL, 'P000002', 1, 223, 0, 'include', NULL, '0', 85600000, NULL, 77818181.818182, 7781818.1818182, '2020-06-25 06:31:49', '2020-06-25 06:31:49', '1', 1);
INSERT INTO "public"."tbl_penjualan" VALUES ('5ef4255198491', '2020-06-25', '1-0005/FB/VI/20', 'P000002', 'A123', NULL, NULL, NULL, 'P000002', 1, 290, 0, 'include', NULL, '0', 4900000, NULL, 4454545.4545455, 445454.54545455, '2020-06-25 06:51:34', '2020-06-25 06:51:34', NULL, 1);
INSERT INTO "public"."tbl_penjualan" VALUES ('5efc01dc3de07', '2020-07-01', '0-0004/FJ/VII/20', 'P000001', NULL, NULL, NULL, NULL, 'P000002', 1, 1, 0, 'exclude', NULL, '0', 55000, NULL, 50000, 0, '2020-07-01 03:24:12', '2020-07-01 03:24:12', NULL, 0);

-- ----------------------------
-- Table structure for tbl_penjualan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_detail";
CREATE TABLE "public"."tbl_penjualan_detail" (
  "IDFJDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo_qty" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8,
  "diskon" float8
)
;

-- ----------------------------
-- Records of tbl_penjualan_detail
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef43f400a5d9', '5ef3ff823e49d', '6', 1000, 1000, '2', 30000, 30000000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef43f400a9c1', '5ef3ff823e49d', '1', 3, 3, '2', 20000, 60000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef444d576378', '5ef403f323707', '3', 12, 12, '1', 100000, 1200000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef444d576760', '5ef403f323707', '1', 211, 211, '2', 400000, 84400000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef4497628e17', '5ef4255198491', '6', 190, 190, '2', 10000, 1900000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5ef4497629200', '5ef4255198491', '7', 100, 100, '2', 30000, 3000000, 0);
INSERT INTO "public"."tbl_penjualan_detail" VALUES ('5efc01dc3f18f', '5efc01dc3de07', '3', 1, 1, '1', 50000, 50000, 0);

-- ----------------------------
-- Table structure for tbl_penjualan_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_grand_total";
CREATE TABLE "public"."tbl_penjualan_grand_total" (
  "IDFJGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8,
  "Batal" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_penjualan_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5eccacb3dfdea', '5eccacb3b8510', '-', 'P000002', 1, 545454.54545455, 0, 54545.454545455, 600000, 0, '0');
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5ee82a42c1db9', '5ee82a42a0e5a', '-', 'P000002', 1, 1500000, 0, 150000, 1650000, 0, '0');
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5ef43f400b191', '5ef3ff823e49d', '-', 'P000002', 1, 27327272.727273, 0, 2732727.2727273, 30060000, 0, '0');
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5ef444d577319', '5ef403f323707', '-', 'P000002', 1, 77818181.818182, 0, 7781818.1818182, 85600000, 0, '0');
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5ef44976299d0', '5ef4255198491', '-', 'P000002', 1, 4454545.4545455, 0, 445454.54545455, 4900000, 0, '0');
INSERT INTO "public"."tbl_penjualan_grand_total" VALUES ('5efc01dc3f577', '5efc01dc3de07', '-', 'P000002', 1, 50000, 0, NULL, 55000, 0, '0');

-- ----------------------------
-- Table structure for tbl_penjualan_pembayaran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penjualan_pembayaran";
CREATE TABLE "public"."tbl_penjualan_pembayaran" (
  "IDFJPembayaran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_pembayaran" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCOA" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NominalPembayaran" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Tanggal_giro" date,
  "dibuat_pada" timestamp(6),
  "diubah_pada" timestamp(6),
  "Batal" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 0,
  "Nomor_giro" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_penjualan_pembayaran
-- ----------------------------
INSERT INTO "public"."tbl_penjualan_pembayaran" VALUES ('5eccacb42ceb1', '5eccacb3b8510', 'transfer', 'P000086', 500000, '1', 14000, NULL, '2020-05-26 05:44:20', '2020-05-26 05:44:20', '0', NULL);
INSERT INTO "public"."tbl_penjualan_pembayaran" VALUES ('5ee82a42f0fad', '5ee82a42a0e5a', 'cash', 'P000001', 1000000, '1', 14000, NULL, '2020-06-16 02:11:14', '2020-06-16 02:11:14', '0', NULL);
INSERT INTO "public"."tbl_penjualan_pembayaran" VALUES ('5ef4409d16135', '5ef403f323707', 'transfer', 'P000086', 9600000, '1', 14000, NULL, '2020-06-25 06:13:49', '2020-06-25 06:13:49', '0', NULL);
INSERT INTO "public"."tbl_penjualan_pembayaran" VALUES ('5efc01dc44398', '5efc01dc3de07', 'giro', 'P000005', 55000, '1', 14000, '2020-07-31', '2020-07-01 03:24:12', '2020-07-01 03:24:12', '0', 'GR001982');

-- ----------------------------
-- Table structure for tbl_penyusutan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_penyusutan";
CREATE TABLE "public"."tbl_penyusutan" (
  "IDPenyusutan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Tanggal" date,
  "IDFBA" varchar(64) COLLATE "pg_catalog"."default",
  "Nilai_Penyusutan" varchar(64) COLLATE "pg_catalog"."default",
  "IDFBADetail" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_perusahaan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_perusahaan";
CREATE TABLE "public"."tbl_perusahaan" (
  "IDPerusahaan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama" varchar(255) COLLATE "pg_catalog"."default",
  "Alamat" varchar(255) COLLATE "pg_catalog"."default",
  "Kecamatan" varchar(255) COLLATE "pg_catalog"."default",
  "Kota" varchar(255) COLLATE "pg_catalog"."default",
  "Telp" varchar(255) COLLATE "pg_catalog"."default",
  "Fax" varchar(255) COLLATE "pg_catalog"."default",
  "Logo_head" varchar(255) COLLATE "pg_catalog"."default",
  "Kontak" varchar(255) COLLATE "pg_catalog"."default",
  "Email" varchar(255) COLLATE "pg_catalog"."default",
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Logo_text" varchar(255) COLLATE "pg_catalog"."default",
  "Font" varchar(255) COLLATE "pg_catalog"."default",
  "FontSize" int8,
  "FontStyle" varchar(64) COLLATE "pg_catalog"."default",
  "Angkakoma" varchar(64) COLLATE "pg_catalog"."default",
  "NPWP" varchar(25) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_perusahaan
-- ----------------------------
INSERT INTO "public"."tbl_perusahaan" VALUES ('1', 'TOKO 45', 'Losarang, Indramayu Regency, West Java 45253', 'Losarang', 'P000048', '0234 505143', '0234 505143', '1586921043.png', '-', 'abcd@efg.hij', '1', '2020-04-15 03:22:27', '-', 'Arial', 14, 'Reguler', '2', NULL);

-- ----------------------------
-- Table structure for tbl_pindah_gudang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pindah_gudang";
CREATE TABLE "public"."tbl_pindah_gudang" (
  "IDPG" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_IDKPScan_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(25) COLLATE "pg_catalog"."default" DEFAULT 0,
  "IDGudangAwal" varchar(30) COLLATE "pg_catalog"."default",
  "IDGudangTujuan" varchar(30) COLLATE "pg_catalog"."default",
  "dibuat_pada" timestamp(0),
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" timestamp(0),
  "diubah_oleh" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pindah_gudang_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pindah_gudang_detail";
CREATE TABLE "public"."tbl_pindah_gudang_detail" (
  "IDPGDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"'::regclass),
  "IDPG" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDKP_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDBarang_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"'::regclass),
  "Qty" float8,
  "Saldo" float8,
  "Harga" float8,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"'::regclass),
  "Kurs" float8
)
;

-- ----------------------------
-- Table structure for tbl_piutang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_piutang";
CREATE TABLE "public"."tbl_piutang" (
  "Tanggal_Piutang" date,
  "Jatuh_Tempo" date,
  "No_Faktur" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDCustomer_seq"'::regclass),
  "Jenis_Faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_Piutang" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Awal" numeric(100) DEFAULT NULL::numeric,
  "Pembayaran" numeric(100) DEFAULT NULL::numeric,
  "Saldo_Akhir" numeric(100) DEFAULT NULL::numeric,
  "IDPiutang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDPiutang_seq"'::regclass),
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_piutang_IDFaktur_seq"'::regclass),
  "UM" float8 DEFAULT 0,
  "DISC" float8 DEFAULT 0,
  "Retur" float8 DEFAULT 0,
  "Selisih" float8 DEFAULT 0,
  "Kontra_bon" float8 DEFAULT 0,
  "Saldo_kontra_bon" float8 DEFAULT 0,
  "Batal" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 0
)
;

-- ----------------------------
-- Records of tbl_piutang
-- ----------------------------
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0001/FJ/VI/20', 'P000001', 'FJ', 20000, 20000, 0, 20000, '5ef3ff82582b3', '5ef3ff823e49d', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 100000, 100000, 0, 100000, '5ef403f3269d0', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '0-0003/FJ/VI/20', 'P000002', 'FJ', 11000, 11000, 0, 11000, '5ef425519b372', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 8500000, 8500000, 0, 8500000, '5ef4348783f40', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0001/FJ/VI/20', 'P000001', 'FJ', 30020000, 30020000, 0, 30020000, '5ef434b543298', '5ef3ff823e49d', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0005/FB/VI/20', 'P000002', 'FJ', 40000, 40000, 0, 40000, '5ef435f076aed', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0005/FB/VI/20', 'P000002', 'FJ', 0, 0, 0, 0, '5ef437345983a', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0001/FJ/VI/20', 'P000001', 'FJ', 0, 0, 0, 0, '5ef43bd00d755', '5ef3ff823e49d', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0001/FJ/VI/20', 'P000001', 'FJ', 30060000, 30060000, 0, 30060000, '5ef43f400d0d2', '5ef3ff823e49d', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 9600000, 9600000, 0, 9600000, '5ef43f5ba5896', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 900000, 900000, 0, 900000, '5ef44066cd667', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 0, 0, 9600000, -9600000, '5ef4409d141f5', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 0, 0, 0, 0, '5ef4444a92101', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0002/FJ/VI/20', 'P000001', 'FJ', 85600000, 85600000, 0, 85600000, '5ef444d579e11', '5ef403f323707', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0005/FB/VI/20', 'P000002', 'FJ', 310000, 310000, 0, 310000, '5ef44552aa80a', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0005/FB/VI/20', 'P000002', 'FJ', 2200000, 2200000, 0, 2200000, '5ef449475a46b', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-06-25', NULL, '1-0005/FB/VI/20', 'P000002', 'FJ', 4900000, 4900000, 0, 4900000, '5ef449762b910', '5ef4255198491', 0, 0, 0, 0, 0, 0, '0');
INSERT INTO "public"."tbl_piutang" VALUES ('2020-07-01', NULL, '0-0004/FJ/VII/20', 'P000001', 'FJ', 55000, 55000, 55000, 0, '5efc01dc418a0', '5efc01dc3de07', 0, 0, 0, 0, 0, 0, '0');

-- ----------------------------
-- Table structure for tbl_po_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_po_asset";
CREATE TABLE "public"."tbl_po_asset" (
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "PPN" float8,
  "Status_PPN" varchar(64) COLLATE "pg_catalog"."default",
  "Percen_Disc" float8,
  "Disc" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo_Qty" float8,
  "Grand_Total" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(53) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Jenis_PO" varchar(64) COLLATE "pg_catalog"."default",
  "Status" varchar(64) COLLATE "pg_catalog"."default",
  "Status_pakai" int4
)
;

-- ----------------------------
-- Table structure for tbl_po_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_po_asset_detail";
CREATE TABLE "public"."tbl_po_asset_detail" (
  "IDPOAssetDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8
)
;

-- ----------------------------
-- Table structure for tbl_posting
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_posting";
CREATE TABLE "public"."tbl_posting" (
  "idposting" int4 NOT NULL DEFAULT nextval('tbl_posting_id_seq'::regclass),
  "Bulan" varchar(4) COLLATE "pg_catalog"."default",
  "Tahun" varchar(4) COLLATE "pg_catalog"."default",
  "Status" varchar(6) COLLATE "pg_catalog"."default" DEFAULT true,
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0)
)
;

-- ----------------------------
-- Table structure for tbl_produksibahanbaku
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_produksibahanbaku";
CREATE TABLE "public"."tbl_produksibahanbaku" (
  "IDPBB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal" timestamp(0),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default",
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Status" varchar(64) COLLATE "pg_catalog"."default",
  "IDFormula" varchar(64) COLLATE "pg_catalog"."default",
  "Qty_Header" int4
)
;

-- ----------------------------
-- Table structure for tbl_produksibahanbaku_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_produksibahanbaku_detail";
CREATE TABLE "public"."tbl_produksibahanbaku_detail" (
  "IDPBBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDPBB" varchar(64) COLLATE "pg_catalog"."default",
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" int8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default",
  "Total" int8
)
;

-- ----------------------------
-- Table structure for tbl_pu_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset";
CREATE TABLE "public"."tbl_pu_asset" (
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "PPN" float8,
  "Status_PPN" varchar(100) COLLATE "pg_catalog"."default",
  "Percen_Disc" float8,
  "Disc" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo_Qty" float8,
  "Grand_Total" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Jatuh_Tempo" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal_Jatuh_Tempo" date
)
;

-- ----------------------------
-- Table structure for tbl_pu_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset_detail";
CREATE TABLE "public"."tbl_pu_asset_detail" (
  "IDFBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8,
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_pu_asset_grandtotal
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_pu_asset_grandtotal";
CREATE TABLE "public"."tbl_pu_asset_grandtotal" (
  "IDFBAssetGrandTotal" varchar(64) COLLATE "pg_catalog"."default",
  "IDFBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Pembayaran" varchar(255) COLLATE "pg_catalog"."default",
  "IDMataUang" float8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_Total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_purchase_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order";
CREATE TABLE "public"."tbl_purchase_order" (
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDPO_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDSupplier_seq"'::regclass),
  "Jenis_Pesanan" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8,
  "Saldo" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Jenis_PO" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8,
  "Status" varchar(11) COLLATE "pg_catalog"."default" DEFAULT ''::character varying,
  "is_taken" varchar(2) COLLATE "pg_catalog"."default",
  "jenis_ppn" int2,
  "pilihanppn" varchar(64) COLLATE "pg_catalog"."default",
  "approve" varchar(64) COLLATE "pg_catalog"."default",
  "dibuat_pada" timestamp(6),
  "diubah_pada" timestamp(6)
)
;

-- ----------------------------
-- Table structure for tbl_purchase_order_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_detail";
CREATE TABLE "public"."tbl_purchase_order_detail" (
  "IDPODetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDPODetail_seq"'::regclass),
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDPO_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_purchase_order_detail_IDWarna_seq"'::regclass),
  "Qty" float8,
  "Saldo" int8,
  "Harga" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default",
  "PPN" float8 NOT NULL,
  "DPP" float8 NOT NULL,
  "Jenis_ppn" varchar(64) COLLATE "pg_catalog"."default",
  "Qty_terima" float8
)
;

-- ----------------------------
-- Table structure for tbl_purchase_order_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_umum";
CREATE TABLE "public"."tbl_purchase_order_umum" (
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_selesai" date,
  "IDAgen" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8,
  "Catatan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "PPN" float8,
  "Status_ppn" float8,
  "Persen_disc" float8,
  "Disc" float8,
  "IDMataUang" int2,
  "Kurs" float8,
  "Saldo_qty" float8,
  "Grand_total" float8,
  "Status_pakai" int4
)
;

-- ----------------------------
-- Table structure for tbl_purchase_order_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_purchase_order_umum_detail";
CREATE TABLE "public"."tbl_purchase_order_umum_detail" (
  "IDPOUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8,
  "IDGroupBarang" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian";
CREATE TABLE "public"."tbl_retur_pembelian" (
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDRB_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDFB_seq"'::regclass),
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_IDSupplier_seq"'::regclass),
  "Tanggal_fb" date,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_pakai" int4
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_detail";
CREATE TABLE "public"."tbl_retur_pembelian_detail" (
  "IDRBDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDRBDetail_seq"'::regclass),
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDRB_seq"'::regclass),
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDCorak_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDMerk_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDWarna_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(6) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(15) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Subtotal" float8
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_grand_total";
CREATE TABLE "public"."tbl_retur_pembelian_grand_total" (
  "IDRBGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"'::regclass),
  "IDRB" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDRB_seq"'::regclass),
  "Pembayaran" varchar(53) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_retur_pembelian_grand_total_IDMataUang_seq"'::regclass),
  "Kurs" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Records of tbl_retur_pembelian_grand_total
-- ----------------------------
INSERT INTO "public"."tbl_retur_pembelian_grand_total" VALUES ('P000001', 'P000001', NULL, 1, '14000', 20000, 0, 2000, 22000, 1);

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum";
CREATE TABLE "public"."tbl_retur_pembelian_umum" (
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fb" date,
  "PPN" float8,
  "Status_ppn" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Persen_disc" float8,
  "Disc" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "Grand_total" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_pakai" int4
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum_detail";
CREATE TABLE "public"."tbl_retur_pembelian_umum_detail" (
  "IDRBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFBUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_retur_pembelian_umum_grand_total
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_pembelian_umum_grand_total";
CREATE TABLE "public"."tbl_retur_pembelian_umum_grand_total" (
  "IDRBUmumGrandTotal" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRBUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Pembayaran" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "DPP" float8,
  "Discount" float8,
  "PPN" float8,
  "Grand_total" float8,
  "Sisa" float8
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan";
CREATE TABLE "public"."tbl_retur_penjualan" (
  "IDRP" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFJ" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_fj" date,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "Discount" float8,
  "Status_ppn" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Status_pakai" int2,
  "dibuat_pada" timestamp(6),
  "diubah_pada" timestamp(6),
  "Grand_total" float8,
  "DPP" float8,
  "PPN" float8,
  "Pilihan_retur" varchar(5) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_retur_penjualan_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_retur_penjualan_detail";
CREATE TABLE "public"."tbl_retur_penjualan_detail" (
  "IDRPDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDRP" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "Saldo_qty" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_saldo_awal_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_saldo_awal_asset";
CREATE TABLE "public"."tbl_saldo_awal_asset" (
  "IDSaldoAwalAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"'::regclass),
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_saldo_awal_asset_IDAsset_seq"'::regclass),
  "Tanggal_Perolehan" date,
  "Qty" int4,
  "Umur" int4,
  "Nilai_Penyusutan_Asset" numeric(100) DEFAULT NULL::numeric,
  "Nilai_Saldo_awal" numeric(100) DEFAULT NULL::numeric,
  "IDGroupAsset" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_Penyusutan" date,
  "Metode" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_sales_order
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order";
CREATE TABLE "public"."tbl_sales_order" (
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "TOP" int8,
  "Tanggal_jatuh_tempo" date,
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default",
  "Kurs" int8,
  "Total_qty" float8,
  "Saldo_qty" float8,
  "No_po_customer" varchar(18) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Grand_total" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT 0,
  "Status_ppn" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "DPP" float8,
  "PPN" float8,
  "dibuat_pada" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" varchar(255) COLLATE "pg_catalog"."default",
  "Persetujuan" bool DEFAULT true
)
;

-- ----------------------------
-- Table structure for tbl_sales_order_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_sales_order_detail";
CREATE TABLE "public"."tbl_sales_order_detail" (
  "IDSOKDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDSOK" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Sub_total" float8,
  "Saldo_qty" float8
)
;

-- ----------------------------
-- Table structure for tbl_satuan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_satuan";
CREATE TABLE "public"."tbl_satuan" (
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_satuan_IDSatuan_seq"'::regclass),
  "Kode_Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "status_pakai" varchar(20) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_satuan
-- ----------------------------
INSERT INTO "public"."tbl_satuan" VALUES ('1', 'Bks', 'Bks', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('2', 'Btl', 'Btl', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('3', 'Kg', 'Kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('4', 'Klg', 'Klg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('5', 'Pcs', 'Pcs', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('6', 'Sak', 'Sak', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);
INSERT INTO "public"."tbl_satuan" VALUES ('7', 'Set', 'Set', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', NULL);

-- ----------------------------
-- Table structure for tbl_satuan_konversi
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_satuan_konversi";
CREATE TABLE "public"."tbl_satuan_konversi" (
  "IDSatuanKonversi" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "IDSatuanKecil" varchar(30) COLLATE "pg_catalog"."default",
  "IDSatuanBesar" varchar(30) COLLATE "pg_catalog"."default",
  "Qty" int4,
  "IDBarang" varchar(40) COLLATE "pg_catalog"."default",
  "dibuat_pada" timestamp(0),
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "diubah_pada" timestamp(0),
  "diubah_oleh" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_satuan_konversi
-- ----------------------------
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5e97cc2dbdca7', 'P000001', 'P000002', 12, 'P000047', '2020-04-16 03:08:29', '4', '2020-04-16 03:08:29', '4');
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5ea781d3bf698', 'P000001', 'P000002', 25, 'P000006', '2020-04-28 01:07:31', '4', '2020-04-28 01:07:31', '4');
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5ea794f69bdb5', 'P000001', 'P000002', 56, 'P000052', '2020-04-28 02:29:10', '1', '2020-04-28 02:29:10', '1');
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5ea7958515209', 'P000001', 'P000002', 99, 'P000032', '2020-04-28 02:31:33', '1', '2020-04-28 02:31:33', '1');
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5ea798595476a', 'P000001', 'P000002', 44, 'P000018', '2020-04-28 02:43:37', '1', '2020-04-28 02:43:37', '1');
INSERT INTO "public"."tbl_satuan_konversi" VALUES ('5ea8e5503cba4', 'P000001', 'P000002', 56, 'P000029', '2020-04-29 02:24:16', '1', '2020-04-29 02:24:16', '1');

-- ----------------------------
-- Table structure for tbl_setting_cf
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_cf";
CREATE TABLE "public"."tbl_setting_cf" (
  "IDSettingCF" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDCoa" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Kategori" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Perhitungan" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Group" int8,
  "Urutan" int8,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for tbl_setting_labarugi
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_labarugi";
CREATE TABLE "public"."tbl_setting_labarugi" (
  "IDLabaRugi" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default",
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Kategori" varchar(64) COLLATE "pg_catalog"."default",
  "Perhitungan" varchar(64) COLLATE "pg_catalog"."default",
  "Group" varchar(64) COLLATE "pg_catalog"."default",
  "Urutan" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_setting_labarugi
-- ----------------------------
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e854e43e7aa5', 'P000106', 'PENJUALAN', 'judul', 'tidakada', '0', '1');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e854e5eb74a4', 'P000048', '40101 -- PENJUALAN', 'data', 'debet', 'P000106', '1');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e854e9c09f8a', 'P000049', '40102 -- RETUR PENJUALAN', 'data', 'kredit', 'P000106', '2');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e854ec6e733d', 'P000051', '40104 -- POTONGAN PENJUALAN', 'data', 'kredit', 'P000106', '3');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e854ee461713', 'P000107', 'HARGA POKOK PENJUALAN (HPP)', 'judul', 'tidakada', '0', '2');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e85542facade', 'P000111', '10305 -- PEMBELIAN', 'data', 'debet', 'P000107', '1');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e85544f2f718', 'P000110', '40105 -- PERSEDIAAN AWAL', 'data', 'debet', 'P000107', '2');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e85546a7eb18', 'P000112', '10306 -- RETUR PEMBELIAN', 'data', 'debet', 'P000107', '3');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e8554908dde7', 'P000113', '10307 -- PEMBULATAN PEMBELIAN', 'data', 'kredit', 'P000107', '4');
INSERT INTO "public"."tbl_setting_labarugi" VALUES ('5e855bbd12032', 'P000008', '10301 -- PERSEDIAAN DALAM PROSES', 'data', 'kredit', 'P000107', '5');

-- ----------------------------
-- Table structure for tbl_setting_neraca
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_setting_neraca";
CREATE TABLE "public"."tbl_setting_neraca" (
  "IDNeraca" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default",
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Kategori" varchar(64) COLLATE "pg_catalog"."default",
  "Perhitungan" varchar(64) COLLATE "pg_catalog"."default",
  "Group" varchar(64) COLLATE "pg_catalog"."default",
  "Urutan" varchar(64) COLLATE "pg_catalog"."default",
  "Nilai" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_setting_neraca
-- ----------------------------
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aea971de2c', 'P000001', '0100.01 -- KAS BESAR', 'data', 'debet', 'P000093', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeac1e1211', 'P000002', '0100.02 -- KAS KECIL', 'data', 'debet', 'P000093', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb3469af5', 'P000086', '10103.01 -- BCA AHS SBY', 'data', 'debet', 'P000003', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb4acf1e8', 'P000087', '10103.02 -- BCA PT SBY', 'data', 'debet', 'P000003', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb62de773', 'P000088', '10103.03 -- BCA PT USD', 'data', 'debet', 'P000003', '3', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb7b27750', 'P000091', '10103.04 -- BCA PT PWS', 'data', 'debet', 'P000003', '4', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb90e8d29', 'P000092', '10103.05 -- BCA AHS PWS', 'data', 'debet', 'P000003', '5', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aeb076ab8f', 'P000003', 'BANK', 'subjudul', 'debet', 'P000095', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e69e587538f4', 'P000093', 'KAS', 'subjudul', 'tidakada', 'P000095', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e69abcf953cf', 'P000095', 'AKTIVA LANCAR', 'judul', 'tidakada', 'P000094', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e69abadc62c3', 'P000094', 'AKTIVA', 'judul', 'tidakada', '0', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aee4296a79', 'P000007', '10204 -- PIUTANG LAIN-LAIN', 'subjudul', 'debet', 'P000095', '4', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e6aebea11fe5', 'P000004', '10201 -- Piutang Dagang', 'subjudul', 'debet', 'P000095', '3', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e8187e738ee1', 'P000005', '10202 -- PIUTANG GIRO MUNDUR', 'subjudul', 'debet', 'P000095', '5', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81881358b29', 'P000006', '10203 -- PIUTANG USAHA', 'subjudul', 'debet', 'P000095', '6', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818890327a5', 'P000018', '10501 -- BIAYA DIBAYAR DIMUKA', 'subjudul', 'debet', 'P000095', '7', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818905ea83d', 'P000009', '10302 -- PERSEDIAN BARANG', 'subjudul', 'debet', 'P000095', '8', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e8189557246c', 'P000011', '10304 -- UANG MUKA PEMBELIAN', 'subjudul', 'kredit', 'P000095', '9', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81897f6e2e0', 'P000096', 'AKTIVA TETAP', 'judul', 'tidakada', 'P000094', '3', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e8189fb5e5e0', 'P000021', '10602 -- BANGUNAN', 'subjudul', 'debet', 'P000096', '1', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818a4296c67', 'P000022', '10603 -- KENDARAAN', 'subjudul', 'debet', 'P000096', '2', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818c0f1056f', 'P000103', '10701 -- AKUMULASI PENYUSUTAN', 'subjudul', 'tidakada', 'P000096', '3', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818c37d3efd', 'P000024', '10701.1 -- AKUMULASI PENYUSUTAN - BANGUNAN', 'data', 'debet', 'P000103', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818c620de06', 'P000025', '10701.2 -- AKUMULASI PENYUSUTAN - KENDARAAN', 'data', 'debet', 'P000103', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818c9a7fd8e', 'P000026', '10701.3 -- AKUMULASI PENYUSUTAN - INVENTARIS KANTOR', 'data', 'debet', 'P000103', '3', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818cf1d2f82', 'P000020', '10601 -- TANAH', 'subjudul', 'debet', 'P000096', '4', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e818e0be1b80', 'P000097', 'AKTIVA LAIN-LAIN', 'judul', 'tidakada', 'P000094', '4', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81b8d5c6d88', 'P000105', 'TOTAL AKTIVA', 'total', 'debet', 'P000094', '5', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81b91fab898', 'P000104', 'HUTANG DAN MODAL', 'judul', 'tidakada', '0', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81b97187fda', 'P000098', 'HUTANG LANCAR', 'judul', 'tidakada', 'P000104', '1', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81b9bb3d1bb', 'P000030', '20103 -- UTANG USAHA', 'subjudul', 'kredit', 'P000098', '1', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81ba39ce482', 'P000031', '20104 -- UTANG LAIN-LAIN', 'subjudul', 'kredit', 'P000098', '2', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81ba7f07ff3', 'P000029', '20102 -- UTANG GIRO MUNDUR', 'subjudul', 'kredit', 'P000098', '3', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81babf20fca', 'P000040', '20301 -- UANG MUKA PENJUALAN', 'subjudul', 'kredit', 'P000098', '4', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81bb6313bf3', 'P000100', 'MODAL', 'judul', 'tidakada', 'P000104', '2', 'no');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81bb9fb9a72', 'P000043', '20402 -- LABA DITAHAN', 'subjudul', 'kredit', 'P000100', '1', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81bbc9a198b', 'P000047', '30103 -- LABA TAHUN BERJALAN', 'subjudul', 'kredit', 'P000100', '2', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81bbeed3f68', 'P000042', '20401 -- MODAL DISETOR', 'subjudul', 'kredit', 'P000100', '3', 'yes');
INSERT INTO "public"."tbl_setting_neraca" VALUES ('5e81bc1e532c6', 'P000105', 'TOTAL HUTANG DAN MODAL', 'total', 'kredit', 'P000104', '3', 'no');

-- ----------------------------
-- Table structure for tbl_settingcoa
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_settingcoa";
CREATE TABLE "public"."tbl_settingcoa" (
  "IDSETCOA" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDMenu" int8,
  "IDCoa" varchar(64) COLLATE "pg_catalog"."default",
  "Posisi" varchar(255) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(6),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(6),
  "Tingkat" varchar(53) COLLATE "pg_catalog"."default",
  "Cara" varchar(53) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_settingcoa
-- ----------------------------
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3131169caf9', 35, 'P000011', 'debet', '1', '2020-01-29 07:15:34', '1', '2020-02-05 02:49:07', 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e31321b771a5', 47, 'P000030', 'debet', '1', '2020-01-29 07:19:55', '1', '2020-02-05 02:50:22', 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3133f55aac9', 47, 'P000009', 'kredit', '1', '2020-01-29 07:27:49', '1', '2020-02-05 02:51:18', 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e31340b3efd3', 47, 'P000013', 'kredit', '1', '2020-01-29 07:28:11', '1', '2020-02-05 02:51:30', 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3132336bf43', 35, 'P000003', 'kredit', '1', '2020-01-29 07:20:19', '1', '2020-02-05 02:51:47', 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3a443b48287', 44, 'P000009', 'debet', '1', '2020-02-05 04:27:39', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3a445cb993d', 44, 'P000013', 'debet', '1', '2020-02-05 04:28:12', NULL, NULL, 'tambahan', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3a447881961', 44, 'P000030', 'kredit', '1', '2020-02-05 04:28:40', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3b945d7d97c', 93, 'P000030', 'debet', '1', '2020-02-06 04:21:49', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e3b946cea06b', 93, 'P000003', 'kredit', '1', '2020-02-06 04:22:05', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6afff2ecc23', 75, 'P000003', 'debet', '1', '2020-03-13 03:37:22', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6b000d000f9', 75, 'P000040', 'kredit', '1', '2020-03-13 03:37:49', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6b484e5d78d', 80, 'P000003', 'debet', '1', '2020-03-13 08:46:06', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6b485d85ccb', 80, 'P000048', 'kredit', '1', '2020-03-13 08:46:21', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6b48c9a68f4', 80, 'P000040', 'debet', '1', '2020-03-13 08:48:09', NULL, NULL, 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6b488763aad', 80, 'P000033', 'kredit', '1', '2020-03-13 08:47:03', '1', '2020-03-13 08:48:55', 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6ecbf2bb9d2', 80, 'P000004', 'debet', '1', '2020-03-16 00:44:34', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6ecc4b7702f', 80, 'P000048', 'kredit', '1', '2020-03-16 00:46:03', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e6ecc6ad7fe4', 80, 'P000033', 'kredit', '1', '2020-03-16 00:46:34', NULL, NULL, 'tambahan', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8bff28bb0ad', 44, 'P000114', 'kredit', '1', '2020-04-07 04:18:48', NULL, NULL, 'tambahan', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d7ce5dcbc8', 93, 'P000011', 'kredit', '1', '2020-04-08 07:27:33', NULL, NULL, 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d7d2646db7', 93, 'P000030', 'debet', '1', '2020-04-08 07:28:38', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d7d544a2f4', 93, 'P000011', 'kredit', '1', '2020-04-08 07:29:24', NULL, NULL, 'tambahan', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d7d3f1198c', 93, 'P000029', 'kredit', '1', '2020-04-08 07:29:03', '1', '2020-04-08 07:42:45', 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8b0657bc2', 80, 'P000051', 'debet', '1', '2020-04-08 08:27:50', NULL, NULL, 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8d41c8063', 85, 'P000009', 'debet', '1', '2020-04-08 08:37:21', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8d546e8ba', 85, 'P000033', 'debet', '1', '2020-04-08 08:37:40', NULL, NULL, 'tambahan', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8d7180b63', 85, 'P000003', 'kredit', '1', '2020-04-08 08:38:09', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8d9c62372', 85, 'P000009', 'debet', '1', '2020-04-08 08:38:52', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8db19c1ff', 85, 'P000033', 'debet', '1', '2020-04-08 08:39:13', NULL, NULL, 'tambahan', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8de819861', 85, 'P000004', 'kredit', '1', '2020-04-08 08:40:08', NULL, NULL, 'utama', 'utang');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8fd22be00', 78, 'P000107', 'debet', '1', '2020-04-08 08:48:18', NULL, NULL, 'utama', 'lunas');
INSERT INTO "public"."tbl_settingcoa" VALUES ('5e8d8fe59e6b3', 78, 'P000009', 'kredit', '1', '2020-04-08 08:48:37', NULL, NULL, 'utama', 'lunas');

-- ----------------------------
-- Table structure for tbl_stok
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok";
CREATE TABLE "public"."tbl_stok" (
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDStok_seq"'::regclass),
  "Tanggal" date,
  "Nomor_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDBarang_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDGudang_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Total" float8,
  "Qty_pcs" float8,
  "Saldo_pcs" float8,
  "Nama_Barang" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_stok
-- ----------------------------
INSERT INTO "public"."tbl_stok" VALUES ('5ef1cd9cc0cfa', '2020-06-23', '1-0002/FB/VI/20', 'FB', NULL, '5', 'P000001', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 1, NULL, 'Supermes E 1lt');
INSERT INTO "public"."tbl_stok" VALUES ('5ef3ff824907f', '2020-06-25', '1-0001/FJ/VI/20', 'FJ', NULL, '1', 'P000002', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, -302, NULL, 'EM 4 Tambak 1lt');
INSERT INTO "public"."tbl_stok" VALUES ('5ef425519a7ba', '2020-06-25', '0-0003/FJ/VI/20', 'FJ', NULL, '6', 'P000002', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, -1193, NULL, 'Togatsu 100ml');
INSERT INTO "public"."tbl_stok" VALUES ('5ef435f075b4d', '2020-06-25', '1-0005/FB/VI/20', 'FJ', NULL, '7', 'P000002', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, -101, NULL, 'Togatsu 300ml');
INSERT INTO "public"."tbl_stok" VALUES ('5ef9b5c09712a', '2020-06-29', '1-0006/FB/VI/20', 'FB', NULL, '2', 'P000001', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, 3000, NULL, 'Linex 500gr');
INSERT INTO "public"."tbl_stok" VALUES ('5ef9b9b29e4ff', '2020-06-29', '1-0008/FB/VI/20', 'FB', NULL, '6', 'P000001', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 6000, NULL, 'Togatsu 100ml');
INSERT INTO "public"."tbl_stok" VALUES ('5ef1c07e51b45', '2020-06-23', '1-0001/FB/VI/20', 'FB', NULL, '1', 'P000001', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 16, NULL, 'EM 4 Tambak 1lt');
INSERT INTO "public"."tbl_stok" VALUES ('5efa92663f2a9', '2020-06-30', '0-0012/FB/VI/20', 'FB', NULL, '7', 'P000001', NULL, NULL, NULL, NULL, NULL, '2', NULL, NULL, 100, NULL, 'Togatsu 300ml');
INSERT INTO "public"."tbl_stok" VALUES ('5ef9b00f1b850', '2020-06-29', '1-0005/FB/VI/20', 'FB', NULL, '3', 'P000001', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, 9134, NULL, 'Lodan 1kg');
INSERT INTO "public"."tbl_stok" VALUES ('5ef403f325e18', '2020-06-25', '1-0002/FJ/VI/20', 'FJ', NULL, '3', 'P000002', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, -2, NULL, 'Lodan 1kg');

-- ----------------------------
-- Table structure for tbl_stok_backup
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok_backup";
CREATE TABLE "public"."tbl_stok_backup" (
  "IDStok" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDStok_seq"'::regclass),
  "Tanggal" date,
  "Nomor_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFaktur_seq"'::regclass),
  "IDFakturDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDFakturDetail_seq"'::regclass),
  "Jenis_faktur" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDGudang_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(7) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_stok_IDMataUang_seq"'::regclass),
  "Kurs" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total" float8,
  "Tanggal_backup" date,
  "Tanggal_finish" date
)
;

-- ----------------------------
-- Table structure for tbl_stok_history
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok_history";
CREATE TABLE "public"."tbl_stok_history" (
  "IDBarang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDBarang_seq"'::regclass),
  "Asal_jenis_faktur" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor_faktur" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDFaktur" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDFaktur_seq"'::regclass),
  "IDFakturDetail" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDFakturDetail_seq"'::regclass),
  "Jenis_faktur" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDHistoryStok" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDHistoryStok_seq"'::regclass),
  "IDStok" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDStok_seq"'::regclass),
  "Asal_faktur" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "AsalIDFaktur" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_AsalIDFaktur_seq"'::regclass),
  "AsalIDFakturDetail" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_AsalIDFakturDetail_seq"'::regclass),
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDCorak_seq"'::regclass),
  "IDWarna" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDWarna_seq"'::regclass),
  "IDGudang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDGudang_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Grade" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "IDMataUang" int8 NOT NULL DEFAULT nextval('"tbl_stok_history_IDMataUang_seq"'::regclass),
  "Kurs" float8,
  "Total" float8
)
;

-- ----------------------------
-- Table structure for tbl_stok_opname
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_stok_opname";
CREATE TABLE "public"."tbl_stok_opname" (
  "IDStokOpname" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDStokOpname_seq"'::regclass),
  "Tanggal" date,
  "Barcode" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDWarna_seq"'::regclass),
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDGudang_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Grade" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_stok_opname_IDSatuan_seq"'::regclass),
  "Harga" float8
)
;

-- ----------------------------
-- Table structure for tbl_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_supplier";
CREATE TABLE "public"."tbl_supplier" (
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDSupplier_seq"'::regclass),
  "IDGroupSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_suplier_IDGroupSupplier_seq"'::regclass),
  "Kode_Suplier" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Alamat" text COLLATE "pg_catalog"."default",
  "No_Telpon" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDKota" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Fax" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NPWP" varchar(35) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "No_KTP" varchar(25) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Deskripsi" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_supplier
-- ----------------------------
INSERT INTO "public"."tbl_supplier" VALUES ('28', 'P000001', 'SP-1', 'SPL!', 'A', '1', 'P000036', '1', 'email@gmal.com', '12321', 'dfds', 'aktif', NULL);
INSERT INTO "public"."tbl_supplier" VALUES ('29', 'P000001', 'SP-2', '11', '1', '1', 'P000036', '1', '1@nai.com', '1', '1', 'aktif', NULL);
INSERT INTO "public"."tbl_supplier" VALUES ('30', 'P000001', 'SP-3', 'ASAS', 'ASASAS', '111', 'P000036', NULL, NULL, NULL, NULL, 'aktif', NULL);

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer";
CREATE TABLE "public"."tbl_surat_jalan_customer" (
  "idsjc" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "nomor" varchar(191) COLLATE "pg_catalog"."default",
  "tanggal" date,
  "idcustomer" varchar(64) COLLATE "pg_catalog"."default",
  "idso" varchar(64) COLLATE "pg_catalog"."default",
  "nomorso" varchar(191) COLLATE "pg_catalog"."default",
  "asalfaktur" varchar(191) COLLATE "pg_catalog"."default",
  "nomorcustomer" varchar(191) COLLATE "pg_catalog"."default",
  "idgudang" int8,
  "idmatauang" varchar(64) COLLATE "pg_catalog"."default",
  "kurs" int8,
  "qty" int8,
  "saldoqty" int8,
  "top" int8,
  "jatuhtempo" timestamp(0),
  "disc" varchar(255) COLLATE "pg_catalog"."default",
  "persendisc" float8,
  "ppn" varchar(255) COLLATE "pg_catalog"."default",
  "statusppn" varchar(191) COLLATE "pg_catalog"."default",
  "grantotal" varchar(191) COLLATE "pg_catalog"."default",
  "sisa" varchar(191) COLLATE "pg_catalog"."default",
  "saldosisa" varchar(191) COLLATE "pg_catalog"."default",
  "kelebihan" varchar(191) COLLATE "pg_catalog"."default",
  "saldokelebihan" varchar(191) COLLATE "pg_catalog"."default",
  "keterangan" varchar(191) COLLATE "pg_catalog"."default",
  "jenisso" varchar(191) COLLATE "pg_catalog"."default",
  "dpp" float8,
  "nilaippn" float8,
  "iduser" int8,
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "Batal" varchar(255) COLLATE "pg_catalog"."default",
  "confirm" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 1
)
;

-- ----------------------------
-- Table structure for tbl_surat_jalan_customer_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_surat_jalan_customer_detail";
CREATE TABLE "public"."tbl_surat_jalan_customer_detail" (
  "idsjcdetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "idsjc" varchar(64) COLLATE "pg_catalog"."default",
  "idbarang" varchar(64) COLLATE "pg_catalog"."default",
  "qty" float8,
  "saldo" float8,
  "hargasatuan" float8,
  "diskon2" float8,
  "idmatauang" int8,
  "kurs" float8,
  "expiredate" timestamp(6),
  "dpp" float8,
  "statusppn" varchar(191) COLLATE "pg_catalog"."default",
  "nilaippn" float8,
  "subtotal" float8,
  "idso" int8,
  "idsodetail" int8,
  "asalfaktur" varchar(191) COLLATE "pg_catalog"."default",
  "idgudang" int8,
  "nomorsj" varchar(191) COLLATE "pg_catalog"."default",
  "nomorso" varchar(191) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "idsatuan" varchar(53) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_tampungan
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_tampungan";
CREATE TABLE "public"."tbl_tampungan" (
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoPO" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Corak" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(9) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Satuan" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga" float8,
  "Total_harga" float8,
  "IDT" int8 NOT NULL DEFAULT nextval('"tbl_tampungan_IDT_seq"'::regclass)
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier";
CREATE TABLE "public"."tbl_terima_barang_supplier" (
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDTBS_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDSupplier_seq"'::regclass),
  "Nomor_sj" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPO" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_IDPO_seq"'::regclass),
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty_yard" float8,
  "Total_qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Jenis_TBS" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Batal" varchar(50) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_harga" float8,
  "Status_pakai" int4,
  "create_inv" varchar(64) COLLATE "pg_catalog"."default",
  "IDGudang" varchar(64) COLLATE "pg_catalog"."default",
  "IDMataUang" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_asset
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_asset";
CREATE TABLE "public"."tbl_terima_barang_supplier_asset" (
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default",
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDPOAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Nomor_Supplier" varchar(64) COLLATE "pg_catalog"."default",
  "IDMataUang" float8,
  "Kurs" float8,
  "Total_Qty" float8,
  "Saldo" float8,
  "Keterangan" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0)
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_asset_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_asset_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_asset_detail" (
  "IDTBAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDTBAsset" varchar(64) COLLATE "pg_catalog"."default",
  "IDPOAssetDetail" varchar(64) COLLATE "pg_catalog"."default",
  "IDAsset" varchar(64) COLLATE "pg_catalog"."default",
  "Qty" float8,
  "Saldo" float8,
  "Harga_Satuan" float8,
  "IDMataUang" float8,
  "Kurs" float8,
  "Subtotal" float8
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_celup
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_celup";
CREATE TABLE "public"."tbl_terima_barang_supplier_celup" (
  "IDTBSC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDTBSH_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSupplier_seq"'::regclass),
  "IDSJMC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSJH_seq"'::regclass),
  "Nomor_supplier" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_yard" float8,
  "Saldo_yard" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nomor_SJ" varchar(64) COLLATE "pg_catalog"."default",
  "Total_meter" float8,
  "Saldo_meter" float8
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_celup_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_celup_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_celup_detail" (
  "IDTBSCDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"'::regclass),
  "IDTBSC" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"'::regclass),
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Total_harga" float8,
  "Nama_Satuan" varchar(64) COLLATE "pg_catalog"."default",
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default",
  "Grade" varchar(64) COLLATE "pg_catalog"."default",
  "Barcode" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_detail" (
  "IDTBSDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBSDetail_seq"'::regclass),
  "IDTBS" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDTBS_seq"'::regclass),
  "Barcode" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "NoSO" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Party" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Indent" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDBarang_seq"'::regclass),
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDCorak_seq"'::regclass),
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDWarna_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDMerk_seq"'::regclass),
  "Qty_yard" float8,
  "Qty_meter" float8,
  "Saldo_yard" float8,
  "Saldo_meter" float8,
  "Grade" varchar(5) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Remark" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Lebar" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_detail_IDSatuan_seq"'::regclass),
  "Harga" float8
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit" (
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDTBSH_seq"'::regclass),
  "Tanggal" date,
  "Nomor" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSupplier_seq"'::regclass),
  "IDSJH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_IDSJH_seq"'::regclass),
  "Nomor_supplier" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_qty" float8,
  "Saldo" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Barcode" varchar(64) COLLATE "pg_catalog"."default",
  "Status_pakai" int4
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_jahit_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_jahit_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_jahit_detail" (
  "IDTBSHDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"'::regclass),
  "IDTBSH" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"'::regclass),
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"'::regclass),
  "IDMerk" varchar(64) COLLATE "pg_catalog"."default" DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"'::regclass),
  "Qty" float8,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"'::regclass),
  "Harga" float8,
  "Total_harga" float8,
  "Nama_Satuan" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum" (
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Tanggal" date,
  "Nomor" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDPOUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDMataUang" int8,
  "Kurs" float8,
  "Keterangan" text COLLATE "pg_catalog"."default",
  "Total_qty" float8,
  "Saldo" float8,
  "Batal" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Total_harga" float8
)
;

-- ----------------------------
-- Table structure for tbl_terima_barang_supplier_umum_detail
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_terima_barang_supplier_umum_detail";
CREATE TABLE "public"."tbl_terima_barang_supplier_umum_detail" (
  "IDTBSUmumDetail" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "IDTBSUmum" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDBarang" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Qty" float8,
  "IDPOUmumDetail" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Saldo" float8,
  "IDSatuan" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Harga_satuan" float8,
  "IDMataUang" int8,
  "Kurs" float8,
  "Sub_total" float8
)
;

-- ----------------------------
-- Table structure for tbl_tipe_barang
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_tipe_barang";
CREATE TABLE "public"."tbl_tipe_barang" (
  "IDTipe" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Kode_Tipe" varchar(255) COLLATE "pg_catalog"."default",
  "Nama_Tipe" varchar(255) COLLATE "pg_catalog"."default",
  "Status" varchar(255) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0)
)
;

-- ----------------------------
-- Records of tbl_tipe_barang
-- ----------------------------
INSERT INTO "public"."tbl_tipe_barang" VALUES ('1', '-', '-', 'aktif', NULL, '2020-03-18 13:10:56', NULL, '2020-03-18 13:11:00');

-- ----------------------------
-- Table structure for tbl_ukuran
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_ukuran";
CREATE TABLE "public"."tbl_ukuran" (
  "IDUkuran" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "Nama_Ukuran" varchar(255) COLLATE "pg_catalog"."default",
  "Status" varchar(64) COLLATE "pg_catalog"."default",
  "CID" varchar(64) COLLATE "pg_catalog"."default",
  "CTime" timestamp(0),
  "MID" varchar(64) COLLATE "pg_catalog"."default",
  "MTime" timestamp(0),
  "Kode_Ukuran" varchar(64) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_ukuran
-- ----------------------------
INSERT INTO "public"."tbl_ukuran" VALUES ('1', '20', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-01');
INSERT INTO "public"."tbl_ukuran" VALUES ('2', '30', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-02');
INSERT INTO "public"."tbl_ukuran" VALUES ('3', '120', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-03');
INSERT INTO "public"."tbl_ukuran" VALUES ('4', '-', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-04');
INSERT INTO "public"."tbl_ukuran" VALUES ('5', '1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-05');
INSERT INTO "public"."tbl_ukuran" VALUES ('6', '1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-06');
INSERT INTO "public"."tbl_ukuran" VALUES ('7', '1 pak isi 5x 1,6 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-07');
INSERT INTO "public"."tbl_ukuran" VALUES ('8', '1 paket', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-08');
INSERT INTO "public"."tbl_ukuran" VALUES ('9', '1 x 16', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-09');
INSERT INTO "public"."tbl_ukuran" VALUES ('10', '1 x 20 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-10');
INSERT INTO "public"."tbl_ukuran" VALUES ('11', '10 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-11');
INSERT INTO "public"."tbl_ukuran" VALUES ('12', '10 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-12');
INSERT INTO "public"."tbl_ukuran" VALUES ('13', '10 x 1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-13');
INSERT INTO "public"."tbl_ukuran" VALUES ('14', '10 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-14');
INSERT INTO "public"."tbl_ukuran" VALUES ('15', '10 x 2 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-15');
INSERT INTO "public"."tbl_ukuran" VALUES ('16', '10 x 2 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-16');
INSERT INTO "public"."tbl_ukuran" VALUES ('17', '10 x 2,5 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-17');
INSERT INTO "public"."tbl_ukuran" VALUES ('18', '10 x 500 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-18');
INSERT INTO "public"."tbl_ukuran" VALUES ('19', '100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-19');
INSERT INTO "public"."tbl_ukuran" VALUES ('20', '100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-20');
INSERT INTO "public"."tbl_ukuran" VALUES ('21', '100 x 0,6 mm', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-21');
INSERT INTO "public"."tbl_ukuran" VALUES ('22', '100 x 0,8 mm', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-22');
INSERT INTO "public"."tbl_ukuran" VALUES ('23', '100 x 100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-23');
INSERT INTO "public"."tbl_ukuran" VALUES ('24', '100 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-24');
INSERT INTO "public"."tbl_ukuran" VALUES ('25', '100 x 50 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-25');
INSERT INTO "public"."tbl_ukuran" VALUES ('26', '100 x 60 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-26');
INSERT INTO "public"."tbl_ukuran" VALUES ('27', '100 x 80 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-27');
INSERT INTO "public"."tbl_ukuran" VALUES ('28', '1000 x 1,5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-28');
INSERT INTO "public"."tbl_ukuran" VALUES ('29', '100ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-29');
INSERT INTO "public"."tbl_ukuran" VALUES ('30', '10x1kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-30');
INSERT INTO "public"."tbl_ukuran" VALUES ('31', '12 x 1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-31');
INSERT INTO "public"."tbl_ukuran" VALUES ('32', '12 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-32');
INSERT INTO "public"."tbl_ukuran" VALUES ('33', '120 x 100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-33');
INSERT INTO "public"."tbl_ukuran" VALUES ('34', '120 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-34');
INSERT INTO "public"."tbl_ukuran" VALUES ('35', '120 x 25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-35');
INSERT INTO "public"."tbl_ukuran" VALUES ('36', '120x20 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-36');
INSERT INTO "public"."tbl_ukuran" VALUES ('37', '15 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-37');
INSERT INTO "public"."tbl_ukuran" VALUES ('38', '15 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-38');
INSERT INTO "public"."tbl_ukuran" VALUES ('39', '15 x 2 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-39');
INSERT INTO "public"."tbl_ukuran" VALUES ('40', '16 liter Charge', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-40');
INSERT INTO "public"."tbl_ukuran" VALUES ('41', '16 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-41');
INSERT INTO "public"."tbl_ukuran" VALUES ('42', '16 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-42');
INSERT INTO "public"."tbl_ukuran" VALUES ('43', '160 x 25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-43');
INSERT INTO "public"."tbl_ukuran" VALUES ('44', '180x10 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-44');
INSERT INTO "public"."tbl_ukuran" VALUES ('45', '2 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-45');
INSERT INTO "public"."tbl_ukuran" VALUES ('46', '20  x 1750 butir', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-46');
INSERT INTO "public"."tbl_ukuran" VALUES ('47', '20 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-47');
INSERT INTO "public"."tbl_ukuran" VALUES ('48', '20 x 1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-48');
INSERT INTO "public"."tbl_ukuran" VALUES ('49', '20 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-49');
INSERT INTO "public"."tbl_ukuran" VALUES ('50', '20 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-50');
INSERT INTO "public"."tbl_ukuran" VALUES ('51', '20 x 2 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-51');
INSERT INTO "public"."tbl_ukuran" VALUES ('52', '20 x 200 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-52');
INSERT INTO "public"."tbl_ukuran" VALUES ('53', '20 x 200 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-53');
INSERT INTO "public"."tbl_ukuran" VALUES ('54', '20 x 240 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-54');
INSERT INTO "public"."tbl_ukuran" VALUES ('55', '20 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-55');
INSERT INTO "public"."tbl_ukuran" VALUES ('56', '20 x 300 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-56');
INSERT INTO "public"."tbl_ukuran" VALUES ('57', '20 x 400 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-57');
INSERT INTO "public"."tbl_ukuran" VALUES ('58', '20 x 50 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-58');
INSERT INTO "public"."tbl_ukuran" VALUES ('59', '20 x 500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-59');
INSERT INTO "public"."tbl_ukuran" VALUES ('60', '20 x 500 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-60');
INSERT INTO "public"."tbl_ukuran" VALUES ('61', '20 x 800 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-61');
INSERT INTO "public"."tbl_ukuran" VALUES ('62', '200 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-62');
INSERT INTO "public"."tbl_ukuran" VALUES ('63', '200 x 10 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-63');
INSERT INTO "public"."tbl_ukuran" VALUES ('64', '200 x 25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-64');
INSERT INTO "public"."tbl_ukuran" VALUES ('65', '200 x 40 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-65');
INSERT INTO "public"."tbl_ukuran" VALUES ('66', '200 x 5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-66');
INSERT INTO "public"."tbl_ukuran" VALUES ('67', '200x25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-67');
INSERT INTO "public"."tbl_ukuran" VALUES ('68', '20x1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-68');
INSERT INTO "public"."tbl_ukuran" VALUES ('69', '20x500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-69');
INSERT INTO "public"."tbl_ukuran" VALUES ('70', '24 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-70');
INSERT INTO "public"."tbl_ukuran" VALUES ('71', '24 x 500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-71');
INSERT INTO "public"."tbl_ukuran" VALUES ('72', '24 x 500 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-72');
INSERT INTO "public"."tbl_ukuran" VALUES ('73', '240 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-73');
INSERT INTO "public"."tbl_ukuran" VALUES ('74', '240x10 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-74');
INSERT INTO "public"."tbl_ukuran" VALUES ('75', '240x100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-75');
INSERT INTO "public"."tbl_ukuran" VALUES ('76', '240x20 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-76');
INSERT INTO "public"."tbl_ukuran" VALUES ('77', '24x100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-77');
INSERT INTO "public"."tbl_ukuran" VALUES ('78', '25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-78');
INSERT INTO "public"."tbl_ukuran" VALUES ('79', '25 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-79');
INSERT INTO "public"."tbl_ukuran" VALUES ('80', '25 kg / sak', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-80');
INSERT INTO "public"."tbl_ukuran" VALUES ('81', '25 x 1 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-81');
INSERT INTO "public"."tbl_ukuran" VALUES ('82', '25 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-82');
INSERT INTO "public"."tbl_ukuran" VALUES ('83', '25 x 400 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-83');
INSERT INTO "public"."tbl_ukuran" VALUES ('84', '25 x 400 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-84');
INSERT INTO "public"."tbl_ukuran" VALUES ('85', '250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-85');
INSERT INTO "public"."tbl_ukuran" VALUES ('86', '30 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-86');
INSERT INTO "public"."tbl_ukuran" VALUES ('87', '30 x 500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-87');
INSERT INTO "public"."tbl_ukuran" VALUES ('88', '4 x 4 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-88');
INSERT INTO "public"."tbl_ukuran" VALUES ('89', '4 x 5 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-89');
INSERT INTO "public"."tbl_ukuran" VALUES ('90', '40 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-90');
INSERT INTO "public"."tbl_ukuran" VALUES ('91', '40 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-91');
INSERT INTO "public"."tbl_ukuran" VALUES ('92', '40 x 1 lb', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-92');
INSERT INTO "public"."tbl_ukuran" VALUES ('93', '40 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-93');
INSERT INTO "public"."tbl_ukuran" VALUES ('94', '40 x 200 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-94');
INSERT INTO "public"."tbl_ukuran" VALUES ('95', '40 x 250 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-95');
INSERT INTO "public"."tbl_ukuran" VALUES ('96', '40 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-96');
INSERT INTO "public"."tbl_ukuran" VALUES ('97', '40 x 400 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-97');
INSERT INTO "public"."tbl_ukuran" VALUES ('98', '40 x 400 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-98');
INSERT INTO "public"."tbl_ukuran" VALUES ('99', '40 x 500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-99');
INSERT INTO "public"."tbl_ukuran" VALUES ('100', '40 x 500 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-100');
INSERT INTO "public"."tbl_ukuran" VALUES ('101', '400 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-101');
INSERT INTO "public"."tbl_ukuran" VALUES ('102', '400 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-102');
INSERT INTO "public"."tbl_ukuran" VALUES ('103', '400 x 12 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-103');
INSERT INTO "public"."tbl_ukuran" VALUES ('104', '400 x 5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-104');
INSERT INTO "public"."tbl_ukuran" VALUES ('105', '40x250 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-105');
INSERT INTO "public"."tbl_ukuran" VALUES ('106', '40x50 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-106');
INSERT INTO "public"."tbl_ukuran" VALUES ('107', '40x500 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-107');
INSERT INTO "public"."tbl_ukuran" VALUES ('108', '450 x 35 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-108');
INSERT INTO "public"."tbl_ukuran" VALUES ('109', '48 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-109');
INSERT INTO "public"."tbl_ukuran" VALUES ('110', '48 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-110');
INSERT INTO "public"."tbl_ukuran" VALUES ('111', '48 x 300 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-111');
INSERT INTO "public"."tbl_ukuran" VALUES ('112', '5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-112');
INSERT INTO "public"."tbl_ukuran" VALUES ('113', '5 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-113');
INSERT INTO "public"."tbl_ukuran" VALUES ('114', '50 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-114');
INSERT INTO "public"."tbl_ukuran" VALUES ('115', '50 kg /sak', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-115');
INSERT INTO "public"."tbl_ukuran" VALUES ('116', '50 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-116');
INSERT INTO "public"."tbl_ukuran" VALUES ('117', '50 x 0,3 mm', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-117');
INSERT INTO "public"."tbl_ukuran" VALUES ('118', '50 x 0,5 mm', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-118');
INSERT INTO "public"."tbl_ukuran" VALUES ('119', '50 x 1 lt', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-119');
INSERT INTO "public"."tbl_ukuran" VALUES ('120', '50 x 100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-120');
INSERT INTO "public"."tbl_ukuran" VALUES ('121', '50 x 100 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-121');
INSERT INTO "public"."tbl_ukuran" VALUES ('122', '50 x 200 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-122');
INSERT INTO "public"."tbl_ukuran" VALUES ('123', '50 x 200 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-123');
INSERT INTO "public"."tbl_ukuran" VALUES ('124', '50 x 250 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-124');
INSERT INTO "public"."tbl_ukuran" VALUES ('125', '500 ml', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-125');
INSERT INTO "public"."tbl_ukuran" VALUES ('126', '500 x 5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-126');
INSERT INTO "public"."tbl_ukuran" VALUES ('127', '600 x 15 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-127');
INSERT INTO "public"."tbl_ukuran" VALUES ('128', '64 x 200 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-128');
INSERT INTO "public"."tbl_ukuran" VALUES ('129', '7,5gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-129');
INSERT INTO "public"."tbl_ukuran" VALUES ('130', '80 x 100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-130');
INSERT INTO "public"."tbl_ukuran" VALUES ('131', '80 x 250 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-131');
INSERT INTO "public"."tbl_ukuran" VALUES ('132', '800 butir', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-132');
INSERT INTO "public"."tbl_ukuran" VALUES ('133', '800 x 5 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-133');
INSERT INTO "public"."tbl_ukuran" VALUES ('134', '80x100 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-134');
INSERT INTO "public"."tbl_ukuran" VALUES ('135', '8x5 kg', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-135');
INSERT INTO "public"."tbl_ukuran" VALUES ('136', 'x10 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-136');
INSERT INTO "public"."tbl_ukuran" VALUES ('137', 'x20 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-137');
INSERT INTO "public"."tbl_ukuran" VALUES ('138', 'x25 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-138');
INSERT INTO "public"."tbl_ukuran" VALUES ('139', 'x50 gr', 'aktif', '1', '2020-03-30 12:51:00', '1', '2020-03-30 12:51:00', 'UK-139');

-- ----------------------------
-- Table structure for tbl_um_customer
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_customer";
CREATE TABLE "public"."tbl_um_customer" (
  "IDUMCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDUMCustomer_seq"'::regclass),
  "IDCustomer" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDCustomer_seq"'::regclass),
  "Tanggal_UM" date,
  "Nomor_Faktur" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nilai_UM" numeric(100) DEFAULT NULL::numeric,
  "Saldo_UM" numeric(100) DEFAULT NULL::numeric,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_um_customer_IDFaktur_seq"'::regclass),
  "Pembayaran" numeric(64) DEFAULT NULL::numeric,
  "dibuat_pada" timestamp(6),
  "diubah_pada" timestamp(6),
  "Jenis_Pembayaran" varchar(255) COLLATE "pg_catalog"."default",
  "IDCoa" varchar(255) COLLATE "pg_catalog"."default",
  "Batal" varchar(1) COLLATE "pg_catalog"."default" DEFAULT 0,
  "Tanggal_giro" date,
  "Nomor_giro" varchar(50) COLLATE "pg_catalog"."default",
  "IDFJ" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_um_customer
-- ----------------------------
INSERT INTO "public"."tbl_um_customer" VALUES ('5eccac18da3e8', 'P000002', '2020-05-26', '1-0001/SO/V/20', 100000, 100000, '5eccac18aa254', 100000, '2020-05-26 05:41:44', '2020-05-26 05:41:44', 'transfer', 'P000086', '0', NULL, NULL, '5eccacb3b8510');
INSERT INTO "public"."tbl_um_customer" VALUES ('5ee829882dfd0', 'P000002', '2020-06-16', '0-0001/SO/VI/20', 100000, 100000, '5ee8298801cbe', 100000, '2020-06-16 02:08:08', '2020-06-16 02:08:08', 'cash', 'P000001', '0', NULL, NULL, '5ee82a42a0e5a');
INSERT INTO "public"."tbl_um_customer" VALUES ('5ee989efd918b', 'P000002', '2020-06-17', '1-0001/SO/VI/20', 1000, 1000, '5ee989efb2c3a', 1000, '2020-06-17 03:11:43', '2020-06-17 03:11:43', 'transfer', 'P000086', '0', NULL, NULL, NULL);
INSERT INTO "public"."tbl_um_customer" VALUES ('5ee9a5df45668', 'P000002', '2020-06-17', '1-0002/SO/VI/20', 10000, 10000, '5ee9a5df31613', 10000, '2020-06-17 05:10:55', '2020-06-17 05:10:55', 'transfer', 'P000086', '0', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_um_supplier
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_um_supplier";
CREATE TABLE "public"."tbl_um_supplier" (
  "IDUMSupplier" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "Nomor_Faktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDSupplier" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Nominal" float8,
  "IDFaktur" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Tanggal_UM" date,
  "Jenis_pembayaran" varchar(100) COLLATE "pg_catalog"."default",
  "IDCOA" varchar(50) COLLATE "pg_catalog"."default",
  "Nomor_Giro" varchar(100) COLLATE "pg_catalog"."default",
  "IDFB" varchar(255) COLLATE "pg_catalog"."default",
  "Tanggal_giro" date,
  "dibuat_oleh" varchar(255) COLLATE "pg_catalog"."default",
  "dibuat_pada" timestamp(6)
)
;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_user";
CREATE TABLE "public"."tbl_user" (
  "IDUser" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_user_IDUser_seq"'::regclass),
  "Nama" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Username" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Email" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Password" varchar(60) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDGroupUser" int8 NOT NULL DEFAULT nextval('"tbl_user_IDGroupUser_seq"'::regclass),
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO "public"."tbl_user" VALUES ('P000001', 'advess', 'advess', 'advess@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 5, 'Aktif');

-- ----------------------------
-- Table structure for tbl_warna
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_warna";
CREATE TABLE "public"."tbl_warna" (
  "IDWarna" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT nextval('"tbl_warna_IDWarna_seq"'::regclass),
  "Kode_Warna" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Warna" varchar(100) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "Aktif" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "IDCorak" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0),
  "username" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES ('1', 'Admin Developer', 'admindev@mail.com', '$2y$10$DfHRuhMUkqqFSiC7WWKA6OrmLDfm3NpidpOdOxb5R8VNsDXSAdnTS', '1USVDKJAhqW2tKEhIsJ11kaLnaHZ1BfphN1XhVBeRh1zl2EEzeEGXqYZj371', '2019-12-06 08:08:54', '2019-12-06 08:08:54', NULL);
INSERT INTO "public"."users" VALUES ('4', 'Admin', 'admin@mail.com', '$2y$10$DfHRuhMUkqqFSiC7WWKA6OrmLDfm3NpidpOdOxb5R8VNsDXSAdnTS', 'l5BpI54EePTAOnfsmJWYg3OSJUnDvx2rmXM0nEy94TDy1vj1pc0H6onCUMwb', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for users_akses
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_akses";
CREATE TABLE "public"."users_akses" (
  "IDAkses" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "IDUser" varchar(64) COLLATE "pg_catalog"."default",
  "Tambah" varchar(255) COLLATE "pg_catalog"."default",
  "Ubah" varchar(255) COLLATE "pg_catalog"."default",
  "Hapus" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users_akses
-- ----------------------------
INSERT INTO "public"."users_akses" VALUES ('5e743742e80b9', '1', 'yes', 'yes', 'yes');
INSERT INTO "public"."users_akses" VALUES ('5e90053f2932c', '4', 'yes', 'yes', 'yes');
INSERT INTO "public"."users_akses" VALUES ('5e903862b84e4', '3', 'yes', 'yes', 'yes');
INSERT INTO "public"."users_akses" VALUES ('5e90386bc4ac0', '2', 'yes', 'yes', 'yes');

-- ----------------------------
-- Table structure for users_group
-- ----------------------------
DROP TABLE IF EXISTS "public"."users_group";
CREATE TABLE "public"."users_group" (
  "idgu" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "iduser" varchar(64) COLLATE "pg_catalog"."default",
  "idgroupuser" int8
)
;

-- ----------------------------
-- Records of users_group
-- ----------------------------
INSERT INTO "public"."users_group" VALUES ('1', '1', 5);
INSERT INTO "public"."users_group" VALUES ('2', '4', 6);
INSERT INTO "public"."users_group" VALUES ('3', '4', 8);

-- ----------------------------
-- Function structure for ap
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ap"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ap2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_pembelian."Tanggal") = '1' GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_pembayaran_hutang."Total"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier"  AND date_part('month',tbl_pembayaran_hutang."Tanggal") = '1' GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_jurnal."Tanggal", tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8);
CREATE OR REPLACE FUNCTION "public"."ap_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV' AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m1
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_retur_pembelian right join tbl_suplier on tbl_retur_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_retur_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB' AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembayaran_hutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PH' AND date_part('month',tbl_pembayaran_hutang."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan-1 GROUP BY tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ap_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ap_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8);
CREATE OR REPLACE FUNCTION "public"."ap_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "pembelian" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "cari_bulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Pembelian, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."pembelian", m2."retur", m3."bayar", m0."saldo_awal"+m1."pembelian"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Pembelian from tbl_pembelian right join tbl_suplier on tbl_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='INV'  AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_suplier."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_retur_pembelian right join tbl_suplier on tbl_retur_pembelian."IDSupplier"=tbl_suplier."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_retur_pembelian."Nomor" AND tbl_jurnal."Jenis_faktur"='RB'  AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_suplier."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_pembayaran_hutang right join tbl_suplier ON tbl_suplier."IDSupplier"=tbl_pembayaran_hutang."IDSupplier" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_pembayaran_hutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PH' AND date_part('month',tbl_pembayaran_hutang."Tanggal") = cari_bulan GROUP BY tbl_suplier."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_suplier."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_pembelian ON tbl_pembelian."Nomor"=tbl_jurnal."Nomor" right join tbl_suplier on tbl_suplier."IDSupplier"=tbl_pembelian."IDSupplier" AND date_part('month',tbl_jurnal."Tanggal") = cari_bulan-1 GROUP BY tbl_jurnal."Tanggal", tbl_suplier."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ar"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer"  AND date_part('month',tbl_penerimaan_piutang."Tanggal") = '1' GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8);
CREATE OR REPLACE FUNCTION "public"."ar2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ'  AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_penerimaan_piutang."Total"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer"  AND date_part('month',tbl_penerimaan_piutang."Tanggal") = '1' GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = '1' GROUP BY tbl_jurnal."Tanggal", tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."ar_cari"(OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ' AND date_part('month',tbl_penjualan_kain."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penerimaan_piutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PP' AND date_part('month',tbl_penerimaan_piutang."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for ar_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."ar_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."ar_cari2"(OUT "tanggal" date, OUT "nama" varchar, OUT "saldo_awal" float8, OUT "penjualan" float8, OUT "retur" float8, OUT "bayar" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nama, Saldo_awal, Penjualan, Retur, Bayar, Saldo_akhir IN
       
SELECT m0."Tanggal", m1."Nama", m0."saldo_awal", m1."penjualan", m2."retur", m3."bayar", m0."saldo_awal"+m1."penjualan"-m2."retur"- m3."bayar" AS Saldo_akhir FROM
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Penjualan from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='INVP' AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m1
LEFT JOIN
(select tbl_customer."Nama", tbl_jurnal."Nomor",  coalesce(SUM(tbl_jurnal."Debet"),0) AS Retur from tbl_penjualan_kain right join tbl_customer on tbl_penjualan_kain."IDCustomer"=tbl_customer."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penjualan_kain."Nomor" AND tbl_jurnal."Jenis_faktur"='RJ' AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_customer."Nama", tbl_jurnal."Nomor") m2 USING("Nama")
LEFT JOIN
(select tbl_customer."Nama", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Bayar FROM tbl_penerimaan_piutang right join tbl_customer ON tbl_customer."IDCustomer"=tbl_penerimaan_piutang."IDCustomer" left join tbl_jurnal on tbl_jurnal."Nomor"=tbl_penerimaan_piutang."Nomor" AND tbl_jurnal."Jenis_faktur"='PP' AND date_part('month',tbl_penerimaan_piutang."Tanggal") = caribulan GROUP BY tbl_customer."Nama") m3 USING("Nama")
LEFT JOIN
(select tbl_jurnal."Tanggal", tbl_customer."Nama", COALESCE(SUM(tbl_jurnal."Debet"),0) AS Saldo_awal from tbl_jurnal join tbl_penjualan_kain ON tbl_penjualan_kain."Nomor"=tbl_jurnal."Nomor" right join tbl_customer on tbl_customer."IDCustomer"=tbl_penjualan_kain."IDCustomer" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_jurnal."Tanggal", tbl_customer."Nama") m0 USING("Nama")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for average_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."average_kain"();
CREATE OR REPLACE FUNCTION "public"."average_kain"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
declare harga_po integer;
declare idpo varchar;
declare idbarang varchar;
declare idcoark varchar;
declare harga_grade integer;
declare harga_stok integer;
declare saldo_stok float;
declare harga_hpp float;
declare hpp float;
begin

Select idpo = OLD.IDPO From tbl_terima_barang_suppllier where idtbs = NEW.idtbs;

select idbarang = OLD.IDBarang, idcorak = OLD.IDCorak from tbl_purchase_order where IDPO = idpo;

select harga_po = OLD.Harga from tbl_purchase_order_detail where IDPO = idpo and IDWarna = NEW.IDWarna;

IF (NEW.Grade <> Ä) THEN
	harga_grade =(harga_po * 15) /100;
	harga_po = harga_po - harga_grade;
END IF;

select harga_stok = SUM(Saldo_yard * harga) from tbl_stok where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade;

harga_hpp = harga_po + harga_stok;

select saldo_stok = sum(saldo_yard) from tbl_stok where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade;

hpp = harga_hpp / saldo_stok;

UPDATE tbl_stok set "Harga" = hpp where IDBarang = idbarang and IDCorak = idcorak and idwarna = NEW.IDWarna and Grade = NEW.Grade; 

return new;
end $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for bs
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "nilai" varchar);
CREATE OR REPLACE FUNCTION "public"."bs"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "nilai" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan, Nilai IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan", m1."Nilai"
FROM (SELECT tbl_setting_neraca."IDNeraca", tbl_setting_neraca."Kategori",tbl_setting_neraca."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_neraca."Perhitungan", tbl_setting_neraca."Nilai" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA"=tbl_setting_neraca."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_neraca."IDNeraca", tbl_setting_neraca."Kategori",tbl_jurnal."IDCOA", tbl_setting_neraca."Keterangan",tbl_setting_neraca."Perhitungan", tbl_setting_neraca."Nilai" ORDER BY tbl_setting_neraca."IDNeraca" ASC) m1
LEFT JOIN (
SELECT tbl_setting_neraca."IDNeraca", tbl_setting_neraca."Kategori",tbl_setting_neraca."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_neraca."Perhitungan", tbl_setting_neraca."Nilai" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA"=tbl_setting_neraca."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_neraca."IDNeraca", tbl_setting_neraca."Kategori",tbl_jurnal."IDCOA", tbl_setting_neraca."Keterangan",tbl_setting_neraca."Perhitungan", tbl_setting_neraca."Nilai"  ORDER BY tbl_setting_neraca."IDNeraca" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."bs_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_bulan_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = caribulan-1 AND date_part('year',tbl_jurnal."Tanggal") = caritahun
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan" ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('year',tbl_jurnal."Tanggal") = caritahun
GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDSettingLR")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari2"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "debet_bulan_ini" float8, OUT "kredit_bulan_ini" float8, OUT "debet_bulan_lalu" float8, OUT "kredit_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "normal_balance" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."bs_cari2"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "debet_bulan_ini" float8, OUT "kredit_bulan_ini" float8, OUT "debet_bulan_lalu" float8, OUT "kredit_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "normal_balance" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Debet_bulan_ini, Kredit_bulan_ini, Debet_bulan_lalu, Kredit_bulan_lalu, Perhitungan, Normal_balance IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Debet_bulan_lalu, m1.Kredit_bulan_lalu, m2.Debet_bulan_ini, m2.Kredit_bulan_ini, m1."Perhitungan", m2."Normal_Balance"
FROM (SELECT tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet") AS Debet_bulan_lalu, SUM(tbl_jurnal."Kredit") AS Kredit_bulan_lalu, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" LEFT JOIN tbl_coa ON tbl_coa."IDCoa"=tbl_jurnal."IDCOA" LEFT JOIN tbl_group_coa ON tbl_group_coa."IDGroupCOA" = tbl_coa."IDGroupCOA"
AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1
GROUP BY tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance" , tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC) m1
LEFT JOIN (
-- SELECT tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
-- RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" 
-- AND date_part('month',tbl_jurnal."Tanggal") = caribulan
-- GROUP BY tbl_setting_lr."IDSettingLR", tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC

SELECT tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance", tbl_setting_lr."Kategori",tbl_setting_lr."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet") AS Debet_bulan_ini, SUM(tbl_jurnal."Kredit") AS Kredit_bulan_ini, tbl_setting_lr."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_lr ON tbl_jurnal."IDCOA"=tbl_setting_lr."IDCoa" LEFT JOIN tbl_coa ON tbl_coa."IDCoa"=tbl_jurnal."IDCOA" LEFT JOIN tbl_group_coa ON tbl_group_coa."IDGroupCOA" = tbl_coa."IDGroupCOA"
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_lr."IDSettingLR", tbl_group_coa."Normal_Balance" , tbl_setting_lr."Kategori",tbl_jurnal."IDCOA", tbl_setting_lr."Keterangan",tbl_setting_lr."Perhitungan"  ORDER BY tbl_setting_lr."IDSettingLR" ASC
)m2	USING ("IDSettingLR")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for bs_cari3
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."bs_cari3"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "nilai" varchar, IN "caribulan" int8, IN "caribulan1" int8, IN "caritahun" float8, IN "caritahun1" float8);
CREATE OR REPLACE FUNCTION "public"."bs_cari3"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_bulan_lalu" float8, OUT "perhitungan" varchar, OUT "nilai" varchar, IN "caribulan" int8, IN "caribulan1" int8, IN "caritahun" float8, IN "caritahun1" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
		FOR Kategori,
		Keterangan,
		IDCOA,
		Total_bulan_ini,
		Total_bulan_lalu,
		Perhitungan,
		Nilai IN SELECT
		m1."Kategori",
		m1."Keterangan",
		m1."IDCOA",
		m1.Total_bulan_lalu,
		m2.Total_bulan_ini,
		m1."Perhitungan",
	  m1."Nilai"	
	FROM
		(
		SELECT
			tbl_setting_neraca."IDNeraca",
			tbl_setting_neraca."Kategori",
			tbl_setting_neraca."Keterangan",
			tbl_jurnal."IDCOA",
			SUM ( tbl_jurnal."Debet" ) - sum(tbl_jurnal."Kredit" ) AS Total_bulan_lalu,
			tbl_setting_neraca."Perhitungan",
			tbl_setting_neraca."Nilai"	
		FROM
			tbl_jurnal
			RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
			AND date_part( 'month', tbl_jurnal."Tanggal" ) = caribulan1 
			AND date_part( 'year', tbl_jurnal."Tanggal" ) = caritahun1 
		GROUP BY
			tbl_setting_neraca."IDNeraca",
			tbl_setting_neraca."Kategori",
			tbl_jurnal."IDCOA",
			tbl_setting_neraca."Keterangan",
			tbl_setting_neraca."Perhitungan",
			tbl_setting_neraca."Nilai"
		ORDER BY
			tbl_setting_neraca."IDNeraca" ASC 
		) m1
		LEFT JOIN (
		SELECT
			tbl_setting_neraca."IDNeraca",
			tbl_setting_neraca."Kategori",
			tbl_setting_neraca."Keterangan",
			tbl_jurnal."IDCOA",
			SUM ( tbl_jurnal."Debet" ) - sum( tbl_jurnal."Kredit" ) AS Total_bulan_ini,
			tbl_setting_neraca."Perhitungan",
			tbl_setting_neraca."Nilai"
		FROM
			tbl_jurnal
			RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
			AND date_part( 'month', tbl_jurnal."Tanggal" ) = caribulan 
			AND date_part( 'year', tbl_jurnal."Tanggal" ) = caritahun 
		GROUP BY
			tbl_setting_neraca."IDNeraca",
			tbl_setting_neraca."Kategori",
			tbl_jurnal."IDCOA",
			tbl_setting_neraca."Keterangan",
			tbl_setting_neraca."Perhitungan",
			tbl_setting_neraca."Nilai"
		ORDER BY
			tbl_setting_neraca."IDNeraca" ASC 
		) m2 USING ( "IDNeraca" )
		LOOP
		RETURN NEXT;
	
END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA,Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."IDCOA", m1."Nama_COA", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."Debet", m1."Kredit", m2."saldo_awal"+m1."Debet"-m1."Kredit" AS saldo_akhir  FROM (SELECT tbl_jurnal."IDCOA", tbl_coa."Nama_COA",tbl_jurnal."Tanggal", tbl_jurnal."Nomor", tbl_jurnal."Debet", tbl_jurnal."Kredit" FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan) m1
LEFT JOIN
(SELECT tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_jurnal."IDCOA", tbl_jurnal."Tanggal", tbl_jurnal."Nomor") m2 USING("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_cari"(OUT "idcoa" varchar, OUT "nama_coa" varchar, OUT "tanggal" varchar, OUT "nomor" varchar, OUT "saldo_awal" float8, OUT "debet" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "coa" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCOA, Nama_COA, Tanggal, Nomor, Saldo_awal, Debet, Kredit, Saldo_akhir IN
       
SELECT m1."Nomor",m1."Nomor", m1."Tanggal", m1."Nomor", m2."saldo_awal", m1."debet", m1."kredit", m1."debet"-m1."kredit" AS saldo_akhir  FROM (SELECT tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"),0) AS debet, coalesce(SUM(tbl_jurnal."Kredit"),0) AS kredit FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" WHERE tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_jurnal."Nomor", tbl_jurnal."Tanggal" ORDER BY tbl_jurnal."Nomor" DESC) m1
LEFT JOIN
(SELECT tbl_jurnal."Tanggal", tbl_jurnal."Nomor", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"),0) AS Saldo_awal FROM tbl_jurnal JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND tbl_jurnal."IDCOA"=coa AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_jurnal."Nomor", tbl_jurnal."Tanggal" ORDER BY tbl_jurnal."Nomor" DESC) m2 USING("Nomor")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for buku_besar_pembantu_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."buku_besar_pembantu_cari"(OUT "idcoa" varchar, OUT "kode_coa" varchar, OUT "nama_coa" varchar, OUT "saldo_awal" float8, OUT "debit" float8, OUT "kredit" float8, OUT "saldo_akhir" float8, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDCoa, Kode_COA, Nama_COA, Saldo_awal, Debit, Kredit, Saldo_akhir IN
       
SELECT m1."IDCoa", m1."Kode_COA", m1."Nama_COA", m2."saldo_awal", m1."debit", m1."kredit", m2."saldo_awal"+m1."debit"-m1."kredit" AS saldo_akhir  FROM (SELECT tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"), 0) AS Debit, coalesce(SUM(tbl_jurnal."Kredit"), 0) AS Kredit FROM tbl_jurnal join tbl_coa on tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m1
LEFT JOIN
(select tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA", coalesce(SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit"), 0) AS Saldo_awal FROM tbl_jurnal RIGHT JOIN tbl_coa ON tbl_jurnal."IDCOA"=tbl_coa."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan-1 GROUP BY tbl_coa."IDCoa", tbl_coa."Kode_COA", tbl_coa."Nama_COA") m2 USING("IDCoa")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari2_laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari2_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari2_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Nama, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", CUS."Nama" as "Nama",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
							INNER JOIN tbl_customer AS CUS
								ON SOS."IDCustomer" = CUS."IDCustomer"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Tanggal" >= datein
								AND SOS."Tanggal" <= dateuntil
								AND CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party,
-- 		Nama_Barang, 
		Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party",
-- 						g."Nama_Barang" AS "Nama_Barang",
						c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
-- 							INNER JOIN tbl_barang AS "g" 
-- 							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Tanggal" >= datein
								AND a."Tanggal" <= dateuntil
								AND a."Nomor" ilike keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip_like"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal , Barcode, NoSO, Party,
-- 		Nama_Barang,
		Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party",
-- 						g."Nama_Barang" AS "Nama_Barang",
						c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
-- 							INNER JOIN tbl_barang AS "g" 
-- 							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Nomor" like keywords 					
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip_like_sjc
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip_like_sjc"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip_like_sjc"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, 
-- 		Nama_Barang, 
		Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party",
-- 						g."Nama_Barang" AS "Nama_Barang", 
						c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
-- 							INNER JOIN tbl_barang AS "g" 
-- 							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Tanggal" >= datein
								AND a."Tanggal" <= dateuntil
								AND a."Nomor_sj" ilike keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_ip_like_suplier
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_ip_like_suplier"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_ip_like_suplier"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party,
-- 		Nama_Barang, 
		Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party", 
-- 						g."Nama_Barang" AS "Nama_Barang",
						c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar" 
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
-- 							INNER JOIN tbl_barang AS "g" 
-- 							ON c."IDBarang" = g."IDBarang"
								
								WHERE a."Tanggal" >= datein
								AND a."Tanggal" <= dateuntil
								AND b."Nama" ilike keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_lap_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_lap_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_lap_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama, Corak, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
--	BAR."Nama_Barang" AS "Nama_Barang",
	CUS."Nama" AS "Nama",
	COR."Corak" AS "Corak",
--	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
--	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_customer AS CUS ON RPK."IDCustomer" = CUS."IDCustomer"
--	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND RPK."Nomor" ilike keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Tanggal" >= datein
								AND BO."Tanggal" <= dateuntil
								AND BO."Nomor" ilike keywords 					
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_bo_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_bo_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	
	WHERE BO."Nomor" like keywords 				
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_giro
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_giro"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar, IN "keyword" varchar, IN "detail_keyword" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_giro"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar, IN "keyword" varchar, IN "detail_keyword" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDMG, Nomor IN
        SELECT tbl_mutasi_giro."IDMG", tbl_mutasi_giro."Nomor" FROM tbl_mutasi_giro 
				JOIN tbl_mutasi_giro_detail on tbl_mutasi_giro."IDMG"=tbl_mutasi_giro_detail."IDMG" 
				JOIN tbl_giro on tbl_mutasi_giro_detail."IDGiro"=tbl_giro."IDGiro"
				WHERE tbl_mutasi_giro."Tanggal" >= tanggal_mulai
				AND tbl_mutasi_giro."Tanggal" <= tanggal_selesai
				AND tbl_mutasi_giro_detail."Jenis_giro" = jenis_giro
				AND tbl_giro."Status_Giro" = status_giro 
 				AND keyword = detail_keyword
				
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_giro_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_giro_like"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_giro_like"(OUT "idmg" varchar, OUT "nomor" varchar, IN "tanggal_mulai" date, IN "tanggal_selesai" date, IN "jenis_giro" varchar, IN "status_giro" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDMG, Nomor IN
        SELECT tbl_mutasi_giro."IDMG", tbl_mutasi_giro."Nomor" FROM tbl_mutasi_giro 
				JOIN tbl_mutasi_giro_detail on tbl_mutasi_giro."IDMG"=tbl_mutasi_giro_detail."IDMG" 
				JOIN tbl_giro on tbl_mutasi_giro_detail."IDGiro"=tbl_giro."IDGiro"
				WHERE tbl_mutasi_giro."Tanggal" >= tanggal_mulai
				AND tbl_mutasi_giro."Tanggal" <= tanggal_selesai
				AND tbl_mutasi_giro_detail."Jenis_giro" = jenis_giro
				AND tbl_giro."Status_Giro" = status_giro 
-- 				AND keyword LIKE detail_keyword
				
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, IN "datein" date, IN "dateuntil" date, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  LEFT JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  LEFT JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  LEFT JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
-- 	LEFT JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	LEFT JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	LEFT JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	LEFT JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	LEFT JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Tanggal" >= datein
								AND tbl_pembelian."Tanggal" <= dateuntil
								AND tbl_pembelian."Nomor" ilike keywords
								OR	tbl_suplier."Nama" ilike keywords
								OR tbl_pembelian."No_sj_supplier" ilike keywords
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total"
FROM
	tbl_pembelian
  LEFT JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  LEFT JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  LEFT JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
-- 	LEFT JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	LEFT JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	LEFT JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	LEFT JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	LEFT JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Nomor" like keywords 				 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PKD."Harga" as "Harga", PKD."Sub_total" as "Sub_total"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_barang AS BAR
--								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Tanggal" >= datein
								AND PK."Tanggal" <= dateuntil
								AND PK."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain_like"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor, SJCK."Nomor" As "Nomor2", CUS."Nama" As "Nama", PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_barang AS BAR
--								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain_like_customer
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain_like_customer"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain_like_customer"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PKD."Harga" as "Harga", PKD."Sub_total" as "Sub_total"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_barang AS BAR
--								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Tanggal" >= datein
								AND PK."Tanggal" <= dateuntil
								AND CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_kain_like_sj
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_kain_like_sj"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_kain_like_sj"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PKD."Harga" as "Harga", PKD."Sub_total" as "Sub_total"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_barang AS BAR
--								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PK."Tanggal" >= datein
								AND PK."Tanggal" <= dateuntil
								AND SJCK."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nomor_sjcs,Nama, Nama_di_faktur,  Tanggal_jatuh_tempo,Nama_Barang,  Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS",
						PS."Tanggal" As "Tanggal",
						PS."Nomor" As Nomor,
						SJCS."Nomor" As "Nomor2",
						CUS."Nama" As "Nama", 
						PS."Nama_di_faktur" As "Nama_di_faktur",
						PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo",
						BAR."Nama_Barang" as "Nama_Barang",
-- 						COR."Corak" as "Corak",
-- 						WAR."Warna" AS "Warna",
						PSD."Qty_roll" as "Qty_roll",
						PSD."Qty_yard" as "Qty_yard",
						Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Tanggal" >= datein
								AND PS."Tanggal" <= dateuntil
								AND PS."Nomor" ilike keywords
								OR CUS."Nama" ilike keywords
								OR SJCS."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_inv_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_inv_penjualan_seragam_like"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor,  Nomor_sjcs,Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang,  Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS",
						PS."Tanggal" As "Tanggal",
						PS."Nomor" As Nomor, 
						SJCS."Nomor" As "Nomor2",
						CUS."Nama" As "Nama", 
						PS."Nama_di_faktur" As "Nama_di_faktur",
						PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo",
						BAR."Nama_Barang" as "Nama_Barang", 
-- 						COR."Corak" as "Corak", 
-- 						WAR."Warna" AS "Warna",
						PSD."Qty_roll" as "Qty_roll", 
						PSD."Qty_yard" as "Qty_yard",
						Satuan."Satuan" as "Satuan"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_invoice_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Tanggal" >= datein
								AND FBU."Tanggal" <= dateuntil
								AND FBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_invoice_pembelian_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_invoice_pembelian_umum_like"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_invoice_pembelian_umum_like"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Tanggal" >= datein
								AND PL."Tanggal" <= dateuntil
								AND PL."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list_like"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_packing_list_like_customer
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_packing_list_like_customer"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_packing_list_like_customer"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
							WHERE PL."Tanggal" >= datein
								AND PL."Tanggal" <= dateuntil
								AND CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Tanggal"::DATE >= datein
								AND T1."Tanggal"::DATE <= dateuntil
								AND T1."Nomor" ilike keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_pembelian_asset_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_pembelian_asset_like"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	
	WHERE T1."Nomor" like keywords 
								
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	LEFT JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	LEFT JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	LEFT JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	LEFT JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	LEFT JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	LEFT JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND T1."Nomor" ilike keywords 
								AND T1."Jenis_TBS" = jenis_tbs
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_barang_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_barang_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Nomor" like keywords AND T1."Jenis_TBS"=jenis_tbs
ORDER BY T1."IDTBS" DESC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_jahit
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang, 
-- 		Merk,
		Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", 
-- 						MER."Merk" AS "Merk", 
						MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
-- 								INNER JOIN tbl_merk AS MER
-- 								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Tanggal" >= datein
								AND MJ."Tanggal" <= dateuntil
								AND MJ."Nomor" ilike keywords
								OR SUP."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_jahit_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_jahit_like"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_jahit_like"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang,
-- 		Merk, 
		Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", 
-- 						MER."Merk" AS "Merk", 
						MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
-- 								INNER JOIN tbl_merk AS MER
-- 								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							LEFT JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE TBS."Tanggal" >= datein
								AND TBS."Tanggal" <= dateuntil
								AND TBS."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_penerimaan_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_penerimaan_umum_like"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_penerimaan_umum_like"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE 
								TBS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
-- 	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Tanggal" >= datein
				AND tbl_retur_pembelian."Tanggal" <= dateuntil
				AND tbl_retur_pembelian."Nomor" ilike keywords 	
				OR tbl_suplier."Nama" ilike keywords 	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
-- 	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Nomor" like keywords OR  tbl_suplier."Nama" ilike keywords 	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_suplier_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_suplier_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_suplier_like"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
-- 	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	
	WHERE tbl_retur_pembelian."Tanggal" >= datein
				AND tbl_retur_pembelian."Tanggal" <= dateuntil
				AND tbl_suplier."Nama" ilike keywords 	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Tanggal" >= datein
								AND RBU."Tanggal" <= dateuntil
								AND RBU."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_pembelian_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_pembelian_umum_like"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_pembelian_umum_like"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND RPK."Nomor" ilike keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain_like"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Corak, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
--	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
--	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
--	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
--	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain_like_customer
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain_like_customer"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain_like_customer"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama, Corak, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
--	BAR."Nama_Barang" AS "Nama_Barang",
	CUS."Nama" AS "Nama",
	COR."Corak" AS "Corak",
--	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
--	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_customer AS CUS ON RPK."IDCustomer" = CUS."IDCustomer"
--	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND CUS."Nama" ilike keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_kain_like_fj
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_kain_like_fj"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_kain_like_fj"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama, Corak, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
--	BAR."Nama_Barang" AS "Nama_Barang",
	CUS."Nama" AS "Nama",
	COR."Corak" AS "Corak",
--	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
--	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_customer AS CUS ON RPK."IDCustomer" = CUS."IDCustomer"
--	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPK."Tanggal" >= datein
	AND RPK."Tanggal" <= dateuntil
	AND PK."Nomor" ilike keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk,Nama, Nama_Barang,  Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	CUS."Nama" AS "NamaCus",
	BAR."Nama_Barang" AS "Nama_Barang",
-- 	COR."Corak" AS "Corak",
-- 	MERK."Merk" AS "Merk",
-- 	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_customer AS CUS ON RPS."IDCustomer" = CUS."IDCustomer"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
-- 	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
-- 	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
-- 	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Tanggal" >= datein
				AND RPS."Tanggal" <= dateuntil
				AND RPS."Nomor" ilike keywords
			  OR CUS."Nama" ilike keywords
				OR PS."Nomor" ilike keywords		
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_retur_penjualan_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_retur_penjualan_seragam_like"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk,Nama , Nama_Barang,  Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	CUS."Nama" AS "NamaCus",
  BAR."Nama_Barang" AS "Nama_Barang",
-- 	COR."Corak" AS "Corak",
-- 	MERK."Merk" AS "Merk",
-- 	WAR."Warna" AS "Warna",
	RPSD."Qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_customer AS CUS ON RPS."IDCustomer" = CUS."IDCustomer"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
-- 	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
-- 	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
-- 	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
	WHERE RPS."Nomor" like keywords 	
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang,Nama,  Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",CUS."Nama" as "Nama", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							LEFT JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							LEFT JOIN tbl_customer AS CUS
								ON SOK."IDCustomer" = CUS."IDCustomer"
							LEFT JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							LEFT JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							LEFT JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							LEFT JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							LEFT JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Tanggal" >= datein
								AND SOK."Tanggal" <= dateuntil
								AND SOK."Nomor" ilike keywords 
								OR CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain_like"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang,Nama, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", CUS."Nama" as "Nama",COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							LEFT JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							LEFT JOIN tbl_customer AS CUS
								ON SOK."IDCustomer" = CUS."IDCustomer"
							LEFT JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							LEFT JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							LEFT JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							LEFT JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							LEFT JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Nomor" like keywords 				 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain_like_tes
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain_like_tes"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain_like_tes"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang,Nama, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", CUS."Nama" as "Nama",COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							LEFT JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							LEFT JOIN tbl_customer AS CUS
								ON SOK."IDCustomer" = CUS."IDCustomer"
							LEFT JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							LEFT JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							LEFT JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							LEFT JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							LEFT JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Nomor" like keywords 				 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_kain_tes
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_kain_tes"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_kain_tes"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang,Nama,  Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",CUS."Nama" as "Nama", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							LEFT JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							LEFT JOIN tbl_customer AS CUS
								ON SOK."IDCustomer" = CUS."IDCustomer"
							LEFT JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							LEFT JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							LEFT JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							LEFT JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							LEFT JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
							WHERE SOK."Tanggal" >= datein
								AND SOK."Tanggal" <= dateuntil
								AND SOK."Nomor" ilike keywords 
								OR CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Tanggal" >= datein
								AND SOS."Tanggal" <= dateuntil
								AND SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_sales_order_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_sales_order_seragam_like"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
								WHERE SOS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_scan
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDIn, Barcode, Nama_Barang, Corak, Warna, Merk, Panjang_yard, Panjang_meter, Grade, Satuan IN
        SELECT 
						MJ."IDIn" as "IDIn", MJ."Barcode" As Barcode, BAR."Nama_Barang" AS "Nama_Barang", COR."Corak" AS "Corak", WAR."Warna" AS "Warna", MER."Merk" AS "Merk", MJ."Panjang_yard" AS "Panjang_yard", MJ."Panjang_meter" AS "Panjang_meter", MJ."Grade" AS "Grade", SAT."Satuan" AS "Satuan"
							FROM tbl_in AS MJ
							INNER JOIN tbl_satuan AS SAT
								ON MJ."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJ."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJ."IDMerk" = MER."IDMerk"
								INNER JOIN tbl_warna AS WAR
								ON MJ."IDWarna" = WAR."IDWarna"
								INNER JOIN tbl_corak AS COR
								ON MJ."IDCorak" = COR."IDCorak"
								WHERE MJ."Barcode" LIKE keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Tanggal" >= datein
								AND T1."Tanggal" <= dateuntil
								AND (T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords)
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_stokopname_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_stokopname_like"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
WHERE
	T1."Barcode" like keywords OR T3."Corak" like keywords OR T4."Warna" like keywords
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
--							INNER JOIN tbl_barang AS BAR
--								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Tanggal" >= datein
								AND SJCK."Tanggal" <= dateuntil
								AND SJCK."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain_like"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Corak, Merk, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", COR."Corak" as "Corak", MERK."Merk" as "Merk",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
 								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
--							INNER JOIN tbl_barang AS BAR
--								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_kain_like_customer
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_kain_like_customer"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_kain_like_customer"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
--							INNER JOIN tbl_barang AS BAR
--								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
							WHERE SJCK."Tanggal" >= datein
								AND SJCK."Tanggal" <= dateuntil
								AND CUS."Nama" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
--							INNER JOIN tbl_corak AS COR
--								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_merk AS MERK
--								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE SJCS."Tanggal" >= datein
								AND SJCS."Tanggal" <= dateuntil
								AND SJCS."Nomor" ilike keywords
								
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam_like"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
--							INNER JOIN tbl_corak AS COR
--								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_merk AS MERK
--								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE SJCS."Nomor" ilike keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_laporan_surat_jalan_customer_seragam_like_customer
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_laporan_surat_jalan_customer_seragam_like_customer"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_laporan_surat_jalan_customer_seragam_like_customer"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
--							INNER JOIN tbl_corak AS COR
--								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_merk AS MERK
--								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE SJCS."Tanggal" >= datein
								AND SJCS."Tanggal" <= dateuntil
								AND CUS."Nama" ilike keywords
								
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty IN
					 SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in

								AND PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND PO."Nomor" ilike keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty IN
					 SELECT 
					PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in
								AND PO."Nomor" like keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_like_suplier
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_like_suplier"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_like_suplier"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, IN "jenis_po_in" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty IN
					 SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
								
								WHERE PO."Jenis_PO" = jenis_po_in

								AND PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND SUP."Nama" ilike keywords 							
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_umum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_umum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", POD."IDSatuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
								WHERE PO."Tanggal" >= datein
								AND PO."Tanggal" <= dateuntil
								AND PO."Nomor" ilike keywords 
								OR SUP."Nama" ilike keywords 						
								AND PO."Batal" = 'aktif'
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cari_po_umum_like
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cari_po_umum_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."cari_po_umum_like"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", POD."IDSatuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
								WHERE PO."Nomor" like keywords
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for cf_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."cf_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "total_tahun_lalu" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Total_tahun_lalu, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_tahun_lalu, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_tahun_lalu, tbl_setting_cf."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") = '12' AND date_part('years',tbl_jurnal."Tanggal") = date_part('years',current_date)-1 AND date_part('years',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan" ORDER BY tbl_setting_cf."IDSettingCF" ASC) m1
LEFT JOIN 
(SELECT tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_setting_cf."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_cf."Perhitungan" FROM tbl_jurnal RIGHT JOIN tbl_setting_cf ON tbl_jurnal."IDCOA"=tbl_setting_cf."IDCoa" AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('years',tbl_jurnal."Tanggal")=caritahun GROUP BY tbl_setting_cf."IDSettingCF", tbl_setting_cf."Kategori",tbl_jurnal."IDCOA", tbl_setting_cf."Keterangan",tbl_setting_cf."Perhitungan"  ORDER BY tbl_setting_cf."IDSettingCF" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getip
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar);
CREATE OR REPLACE FUNCTION "public"."getip"(OUT "tanggal" date, OUT "nomor_instruksi" varchar, OUT "nama" varchar, OUT "nomor_sj" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "tanggal_kirim" date, OUT "batal" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Tanggal, Nomor_Instruksi, Nama, Nomor_sj, Corak, Merk, Warna, Tanggal_kirim, Batal, Barcode, NoSO, Party, 
-- 		Nama_Barang,
		Qty_yard, Qty_meter, Saldo_yard, Saldo_meter, Grade, Remark, Lebar IN
        SELECT 
						a."Tanggal" as "Tanggal", a."Nomor" as "Nomor_Instruksi", b."Nama" AS "Nama", a."Nomor_sj" AS "Nomor_sj", d."Corak" AS "Corak",
						e."Merk" AS "Merk", f."Warna" AS "Warna", a."Tanggal_kirim" AS "Tanggal_kirim", a."Batal" AS "Batal", c."Barcode" AS "Barcode", c."NoSO" AS "NoSO", c."Party" AS "Party",
-- 						g."Nama_Barang" AS "Nama_Barang",
						c."Qty_yard" AS "Qty_yard", c."Qty_meter" AS "Qty_meter",c."Saldo_yard" AS "Saldo_yard", c."Saldo_meter" AS "Saldo_meter", c."Grade" AS "Grade", c."Remark" AS "Remark", c."Lebar" AS "Lebar"
					from 
						tbl_instruksi_pengiriman As "a"
						INNER JOIN tbl_instruksi_pengiriman_detail AS "c" 
							ON a."IDIP" = c."IDIP"
						INNER JOIN tbl_suplier AS "b" 
							ON a."IDSupplier" = b."IDSupplier"
						INNER JOIN tbl_corak AS "d" 
							ON c."IDCorak" = d."IDCorak"
						INNER JOIN tbl_merk AS "e" 
							ON c."IDMerk" = e."IDMerk"
						INNER JOIN tbl_warna AS "f" 
							ON c."IDWarna" = f."IDWarna"
-- 							INNER JOIN tbl_barang AS "g" 
-- 							ON c."IDBarang" = g."IDBarang"
			WHERE a."Batal"='Aktif'
			ORDER BY a."Nomor" ASC
    LOOP
	
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getpo2
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar);
CREATE OR REPLACE FUNCTION "public"."getpo2"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "kode_corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "lot" varchar, OUT "jenis_pesanan" varchar, OUT "pic" varchar, OUT "prioritas" varchar, OUT "bentuk" varchar, OUT "panjang" varchar, OUT "point" varchar, OUT "kirim" varchar, OUT "stamping" varchar, OUT "posisi" varchar, OUT "posisi1" varchar, OUT "album" varchar, OUT "m10" varchar, OUT "kain" varchar, OUT "lembaran" varchar, OUT "qty" float8, OUT "batal" varchar, IN "jenis_po_in" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Kode_Corak, Warna, Merk, Lot, Jenis_Pesanan, PIC, Prioritas, Bentuk, Panjang, Point, Kirim, Stamping, Posisi, Posisi1, Album, M10, Kain, Lembaran, Qty, Batal IN
        SELECT 
						PO."IDPO" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, COR."Kode_Corak" as "Kode_Corak", WAR."Warna" AS "Warna",
						MERK."Merk" as "Merk", PO."Lot" as "Lot", PO."Jenis_Pesanan" as "Jenis_Pesanan", PO."PIC" as "PIC", PO."Prioritas" as "Prioritas", PO."Bentuk" as "Bentuk", PO."Panjang" as "Panjang", PO."Point" as "Point", PO."Kirim" as "Kirim", PO."Stamping" as "Stamping", PO."Posisi" as "Posisi", PO."Posisi1" as "Posisi1", PO."Album" as "Album", PO."M10" as "M10", PO."Kain" as "Kain", PO."Lembaran" as "Lembaran", POD."Qty" As "Qty", PO."Batal" AS "Batal"
							FROM tbl_purchase_order AS PO
							INNER JOIN tbl_purchase_order_detail AS POD
								ON PO."IDPO" = POD."IDPO"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_corak AS COR
								ON PO."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON POD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PO."IDMerk" = MERK."IDMerk"
							WHERE PO."Jenis_PO" = jenis_po_in 
							AND PO."Batal" = 'aktif'
							ORDER BY PO."IDPO" 
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for getpoumum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."getpoumum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."getpoumum"(OUT "idpo" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPO, Tanggal, Nomor, Tanggal_selesai, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						PO."IDPOUmum" as "IDPO", PO."Tanggal" As Tanggal, PO."Nomor" As Nomor, PO."Tanggal_selesai" As "Tanggal_selesai", SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", POD."Qty" AS "Qty", POD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", POD."Harga_satuan" AS "Harga_satuan", POD."Sub_total" AS "Sub_total"
							FROM tbl_purchase_order_umum AS PO
							INNER JOIN tbl_purchase_order_umum_detail AS POD
								ON PO."IDPOUmum" = POD."IDPOUmum"
							INNER JOIN tbl_suplier AS SUP
								ON PO."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON POD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON POD."IDBarang" = BAR."IDBarang"
							AND PO."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for lap_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."lap_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."lap_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama, Corak, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
--	BAR."Nama_Barang" AS "Nama_Barang",
	CUS."Nama" AS "Nama",
	COR."Corak" AS "Corak",
--	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
--	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
--	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_customer AS CUS ON RPK."IDCustomer" = CUS."IDCustomer"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for lap_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."lap_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."lap_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Nama, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, CUS."Nama" As Nama, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS 
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
							INNER JOIN tbl_customer AS CUS
								ON SOS."IDCustomer" = CUS."IDCustomer"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_bo
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_bo"(OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_selesai" date, OUT "corak" varchar, OUT "qty" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Tanggal_selesai,
	Corak,
	Qty,
	Batal IN SELECT
	BO."Tanggal" AS Tanggal,
	BO."Nomor" AS Nomor,
	BO."Tanggal_selesai" AS "Tanggal_selesai",
	COR."Corak" AS "Corak",
	BO."Qty" AS "Qty",
	BO."Batal" AS "Batal"
FROM
	tbl_booking_order AS BO
	INNER JOIN tbl_corak AS COR ON BO."IDCorak" = COR."IDCorak"
	WHERE BO."Batal" = 'Aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "grand_total" float8, OUT "tanggal_jatuh_tempo" date, OUT "corak" varchar, OUT "sisa" float8, OUT "no_sj_supplier" varchar, OUT "status_ppn" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Tanggal_jatuh_tempo,
  Grand_total,
  Sisa,
	No_sj_supplier,
	Status_ppn,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,Sub_total,
	Batal
	IN SELECT
	tbl_pembelian."Tanggal",
  tbl_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_pembelian."Tanggal_jatuh_tempo",
  tbl_pembelian_grand_total."Grand_total",
  tbl_pembelian_grand_total."Sisa",
	tbl_pembelian."No_sj_supplier",
	tbl_pembelian."Status_ppn",
	tbl_pembelian_detail."Barcode",
	tbl_pembelian_detail."NoSO",
	tbl_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_corak."Corak",
	tbl_merk."Merk",
	tbl_warna."Warna",
	tbl_pembelian_detail."Qty_yard",
	tbl_pembelian_detail."Qty_meter",
	tbl_pembelian_detail."Saldo_yard",
	tbl_pembelian_detail."Saldo_meter",
	tbl_pembelian_detail."Grade",
	tbl_pembelian_detail."Remark",
	tbl_pembelian_detail."Lebar",
	tbl_satuan."Satuan",
	tbl_pembelian_detail."Harga",
	tbl_pembelian_detail."Sub_total",
	tbl_pembelian."Batal"
FROM
	tbl_pembelian
  JOIN tbl_pembelian_detail ON tbl_pembelian."IDFB" = tbl_pembelian_detail."IDFB"
  JOIN tbl_suplier ON tbl_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_pembelian_grand_total ON tbl_pembelian."IDFB" = tbl_pembelian_grand_total."IDFB"
-- 	JOIN tbl_barang ON tbl_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	JOIN tbl_corak ON tbl_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_merk ON tbl_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_warna ON tbl_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_satuan ON tbl_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
	WHERE tbl_pembelian."Batal" = 'aktif'
	ORDER BY tbl_pembelian."Nomor" ASC
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_kain"(OUT "idfjk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjck" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJK, Tanggal, Nomor, Nama, Nama_di_faktur, Nomor_sjck, Tanggal_jatuh_tempo, Corak, Warna, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PK."IDFJK" as "IDFJK", PK."Tanggal" As "Tanggal", PK."Nomor" As Nomor,  CUS."Nama" As "Nama", PK."Nama_di_faktur" As "Nama_di_faktur", SJCK."Nomor" As "Nomor2",PK."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", COR."Corak" as "Corak", WAR."Warna" AS "Warna",PKD."Qty_roll" as "Qty_roll", PKD."Qty_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PKD."Harga" as "Harga", PKD."Sub_total" as "Sub_total"
							FROM tbl_penjualan_kain AS PK
							INNER JOIN tbl_penjualan_kain_detail AS PKD
								ON PK."IDFJK" = PKD."IDFJK"
							INNER JOIN tbl_surat_jalan_customer_kain AS SJCK
								ON PK."IDSJCK" = SJCK."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON PKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PKD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_barang AS BAR
--								ON PKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang, Corak, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS", PS."Tanggal" As "Tanggal", PS."Nomor" As Nomor, CUS."Nama" As "Nama", PS."Nama_di_faktur" As "Nama_di_faktur", PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak",PSD."Saldo_qty_roll" as "Qty_roll", PSD."Saldo_yard" as "Qty_yard",Satuan."Satuan" as "Satuan", PSD."Harga" As "Harga", PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
							INNER JOIN tbl_corak AS COR
								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Nomor" like keywords
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8, IN "datein" date, IN "dateuntil" date, IN "keywords" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor, Nama, Nama_di_faktur, Tanggal_jatuh_tempo,Nama_Barang, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS",
						PS."Tanggal" As "Tanggal",
						PS."Nomor" As Nomor,
						CUS."Nama" As "Nama", 
						PS."Nama_di_faktur" As "Nama_di_faktur",
						PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo", 
						BAR."Nama_Barang" as "Nama_Barang",
-- 						COR."Corak" as "Corak",
						PSD."Saldo_qty_roll" as "Qty_roll",
						PSD."Saldo_yard" as "Qty_yard",
						Satuan."Satuan" as "Satuan",
						PSD."Harga" As "Harga",
						PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
							WHERE PS."Tanggal" >= datein
								AND PS."Tanggal" <= dateuntil
								AND PS."Nomor" like keywords
    LOOP
		
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_inv_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_inv_penjualan_seragam"(OUT "idfjs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "nama_di_faktur" varchar, OUT "nomor_sjcs" varchar, OUT "tanggal_jatuh_tempo" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFJS, Tanggal, Nomor,Nama, Nama_di_faktur, Nomor_sjcs, Tanggal_jatuh_tempo,Nama_Barang, Qty_roll, Qty_yard, Satuan, Harga, Sub_total IN
         SELECT 
						PS."IDFJS" as "IDFJS",
						PS."Tanggal" As "Tanggal",
						PS."Nomor" As Nomor,
						CUS."Nama" As "Nama",
						PS."Nama_di_faktur" As "Nama_di_faktur",
						SJCS."Nomor" As "Nomor2",
						PS."Tanggal_jatuh_tempo" as "Tanggal_jatuh_tempo",
						BAR."Nama_Barang" as "Nama_Barang",
-- 						COR."Corak" as "Corak",
						PSD."Saldo_qty_roll" as "Qty_roll", 
						PSD."Saldo_yard" as "Qty_yard",
						Satuan."Satuan" as "Satuan", 
						PSD."Harga" As "Harga",
						PSD."Sub_total" As "Sub_total"
							FROM tbl_penjualan_seragam AS PS
							INNER JOIN tbl_penjualan_seragam_detail AS PSD
								ON PS."IDFJS" = PSD."IDFJS"
							INNER JOIN tbl_surat_jalan_customer_seragam AS SJCS
								ON PS."IDSJCS" = SJCS."IDSJCS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON PSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON PSD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_barang AS BAR
								ON PSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON PSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_invoice_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_invoice_pembelian_umum"(OUT "idfbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDFBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						FBU."IDFBUmum" as "IDFBUmum", FBU."Tanggal" As Tanggal, FBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", FBUD."Qty" AS "Qty", FBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", FBUD."Harga_satuan" AS "Harga_satuan", FBUD."Sub_total" AS "Sub_total"
							FROM tbl_pembelian_umum AS FBU
							INNER JOIN tbl_pembelian_umum_detail AS FBUD
								ON FBU."IDFBUmum" = FBUD."IDFBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON FBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON FBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON FBUD."IDBarang" = BAR."IDBarang"
							WHERE FBU."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_packing_list
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_packing_list"(OUT "idpac" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sok" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDPAC, Tanggal, Nomor, Nomor_sok, Nama, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan IN
        SELECT 
						PL."IDPAC" as "IDPAC", PL."Tanggal" As "Tanggal", PL."Nomor" As Nomor, SOK."Nomor" As "Nomor2", CUS."Nama" As "Nama", BAR."Nama_Barang" as "Nama_Barang", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						PLD."Qty_yard" as "Qty_yard", PLD."Qty_meter" as "Qty_meter", PLD."Grade" as "Grade", SATUAN."Satuan" as "Satuan"
							FROM tbl_packing_list AS PL
							INNER JOIN tbl_sales_order_kain AS SOK
								ON PL."IDSOK" = SOK."IDSOK"
							INNER JOIN tbl_packing_list_detail AS PLD
								ON PL."IDPAC" = PLD."IDPAC"
							INNER JOIN tbl_corak AS COR
								ON PLD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON PLD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON PLD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON PLD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON PLD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON PL."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_pembelian_asset
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_pembelian_asset"(OUT "nomor" varchar, OUT "tanggal" date, OUT "kode_asset" varchar, OUT "nama_asset" varchar, OUT "nilai_perolehan" float8, OUT "akumulasi_penyusutan" float8, OUT "nilai_buku" float8, OUT "metode_penyusutan" varchar, OUT "tanggal_penyusutan" date, OUT "umur" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Nomor, 
  Tanggal, 
  Kode_Asset,       
  Nama_Asset,
  Nilai_perolehan,
  Akumulasi_penyusutan,
  Nilai_buku,
  Metode_penyusutan,
  Tanggal_penyusutan,
  Umur,
	Batal
	IN SELECT
	T1."Nomor", 
  T1."Tanggal", 
  T3."Kode_Asset",       
  T3."Nama_Asset",
  T2."Nilai_perolehan",
  T2."Akumulasi_penyusutan",
  T2."Nilai_buku",
  T2."Metode_penyusutan",
  T2."Tanggal_penyusutan",
  T2."Umur",
	T1."Batal"
FROM
	tbl_pembelian_asset AS T1
  JOIN tbl_pembelian_asset_detail AS T2 ON T1."IDFBA" = T2."IDFBA"
  JOIN tbl_asset AS T3 ON T3."IDAsset" = T2."IDAsset"
	WHERE T1."Batal"='aktif'
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_barang
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar, IN "jenis_tbs" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_barang"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sj" varchar, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "batal" varchar, IN "jenis_tbs" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Nomor,
	Nomor_sj,
	Barcode,
	NoSO,
	Party,
	Nama_Barang,
	Corak,
	Merk,
	Warna,
	Qty_yard,
	Qty_meter,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Satuan,
	Harga,
	Batal
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1."Nomor" AS Nomor,
	T1."Nomor_sj" AS Nomor_sj,
	T2. "Barcode" AS Barcode,
	T2."NoSO" AS NoSO,
	T2."Party" AS Party,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T5. "Merk" AS Merk,
	T4 . "Warna" AS Warna,
	T2 . "Qty_yard" AS Qty_yard,
	T2 . "Qty_meter" AS Qty_meter,
	T2. "Saldo_yard" AS Saldo_yard,
	T2 . "Saldo_meter" AS Saldo_meter,
	T2 . "Grade" AS Grade,
	T2. "Remark" AS Remark,
	T2. "Lebar" AS Lebar,
	T7. "Satuan" AS Satuan,
	T2. "Harga" AS Harga,
	T1. "Batal" AS Batal
FROM
	tbl_terima_barang_supplier AS T1
	JOIN tbl_terima_barang_supplier_detail AS T2 ON T1."IDTBS" = T2."IDTBS"
	JOIN tbl_corak AS T3 ON T2."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T2."IDWarna" = T4."IDWarna"
	JOIN tbl_merk AS T5 ON T2."IDMerk" = T5."IDMerk"
	JOIN tbl_barang AS T6 ON T2."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T2."IDSatuan" = T7."IDSatuan"
	WHERE T1."Batal" = 'Aktif' AND T1."Jenis_TBS" = jenis_tbs
ORDER BY T1."Nomor" ASC	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_jahit
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_jahit"(OUT "idtbsh" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "nomor_supplier" varchar, OUT "nama_barang" varchar, OUT "merk" varchar, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "total_harga" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSH, Tanggal, Nomor, NamaSup, Nomor_supplier, Nama_Barang, 
-- 		Merk,
		Saldo, Satuan, Harga, Total_harga IN
        SELECT 
						MJ."IDTBSH" as "IDTBSH", MJ."Tanggal" As Tanggal, MJ."Nomor" As Nomor, SUP."Nama" As NamaSup, MJ."Nomor_supplier" As "Nomor_supplier", BAR."Nama_Barang" AS "Nama_Barang", 
-- 						MER."Merk" AS "Merk",
						MJD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", MJD."Harga" AS "Harga", MJD."Total_harga" AS "Total_harga"
							FROM tbl_terima_barang_supplier_jahit AS MJ
							INNER JOIN tbl_terima_barang_supplier_jahit_detail AS MJD
								ON MJ."IDTBSH" = MJD."IDTBSH"
							INNER JOIN tbl_suplier AS SUP
								ON MJ."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON MJD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJD."IDBarang" = BAR."IDBarang"
-- 								INNER JOIN tbl_merk AS MER
-- 								ON MJD."IDMerk" = MER."IDMerk"
							WHERE MJ."Batal" = 'aktif'
							ORDER BY tbl_terima_barang_supplier_jahit."Nomor" ASC
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_penerimaan_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_penerimaan_umum"(OUT "idtbsumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDTBSUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Batal IN
        SELECT 
						TBS."IDTBSUmum" as "IDTBSUmum", TBS."Tanggal" As Tanggal, TBS."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", TBSD."Qty" AS "Qty", TBSD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", TBSD."Harga_satuan" AS "Harga_satuan", TBSD."Sub_total" AS "Sub_total"
							FROM tbl_terima_barang_supplier_umum AS TBS
							INNER JOIN tbl_terima_barang_supplier_umum_detail AS TBSD
								ON TBS."IDTBSUmum" = TBSD."IDTBSUmum"
							INNER JOIN tbl_suplier AS SUP
								ON TBS."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON TBSD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON TBSD."IDBarang" = BAR."IDBarang"
							WHERE TBS."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_pembelian
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_pembelian"(OUT "tanggal" date, OUT "nomor" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "barcode" varchar, OUT "noso" varchar, OUT "party" varchar, OUT "nama_barang" varchar, OUT "saldo_yard" float8, OUT "saldo_meter" float8, OUT "grade" varchar, OUT "remark" varchar, OUT "lebar" varchar, OUT "subtotal" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
  Nomor,
  Nama,
  Corak,
	Warna,
  Merk,
  Qty_yard,
	Qty_meter,
	Satuan,
	Harga,
	Barcode,
	NoSO,
	Party,
-- 	Nama_Barang,
	Saldo_yard,
	Saldo_meter,
	Grade,
	Remark,
	Lebar,
	Subtotal,
	Batal
	IN SELECT
	tbl_retur_pembelian."Tanggal",
  tbl_retur_pembelian."Nomor",
  tbl_suplier."Nama",
  tbl_corak."Corak",
  tbl_warna."Warna",
  tbl_merk."Merk",
	tbl_retur_pembelian_detail."Qty_yard",
	tbl_retur_pembelian_detail."Qty_meter",
	tbl_satuan."Satuan",
	tbl_retur_pembelian_detail."Harga",
	tbl_retur_pembelian_detail."Barcode",
	tbl_retur_pembelian_detail."NoSO",
	tbl_retur_pembelian_detail."Party",
-- 	tbl_barang."Nama_Barang",
	tbl_retur_pembelian_detail."Saldo_yard",
	tbl_retur_pembelian_detail."Saldo_meter",
	tbl_retur_pembelian_detail."Grade",
	tbl_retur_pembelian_detail."Remark",
	tbl_retur_pembelian_detail."Lebar",
	tbl_retur_pembelian_detail."Subtotal",
	tbl_retur_pembelian."Batal"
FROM
	tbl_retur_pembelian
  JOIN tbl_retur_pembelian_detail ON tbl_retur_pembelian."IDRB" = tbl_retur_pembelian_detail."IDRB"
  JOIN tbl_suplier ON tbl_retur_pembelian."IDSupplier" = tbl_suplier."IDSupplier"
  JOIN tbl_corak ON tbl_retur_pembelian_detail."IDCorak" = tbl_corak."IDCorak"
	JOIN tbl_warna ON tbl_retur_pembelian_detail."IDWarna" = tbl_warna."IDWarna"
	JOIN tbl_merk ON tbl_retur_pembelian_detail."IDMerk" = tbl_merk."IDMerk"
	JOIN tbl_satuan ON tbl_retur_pembelian_detail."IDSatuan" = tbl_satuan."IDSatuan"
-- 	JOIN tbl_barang ON tbl_retur_pembelian_detail."IDBarang" = tbl_barang."IDBarang"
	WHERE tbl_retur_pembelian."Batal"='aktif'
	ORDER BY tbl_retur_pembelian."Nomor" ASC
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_pembelian_umum
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_pembelian_umum"(OUT "idrbumum" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "namasup" varchar, OUT "barang" varchar, OUT "qty" float8, OUT "saldo" float8, OUT "satuan" varchar, OUT "harga_satuan" float8, OUT "sub_total" float8, OUT "grand_total" float8, OUT "batal" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRBUmum, Tanggal, Nomor, NamaSup, Barang, Qty, Saldo, Satuan, Harga_satuan, Sub_total, Grand_total, Batal IN
        SELECT 
						RBU."IDRBUmum" as "IDRBUmum", RBU."Tanggal" As Tanggal, RBU."Nomor" As Nomor, SUP."Nama" As NamaSup, BAR."Nama_Barang" AS "Nama_Barang", RBUD."Qty" AS "Qty", RBUD."Saldo" AS "Saldo", SAT."Satuan" AS "Satuan", RBUD."Harga_satuan" AS "Harga_satuan", RBUD."Sub_total" AS "Sub_total"
							FROM tbl_retur_pembelian_umum AS RBU
							INNER JOIN tbl_retur_pembelian_umum_detail AS RBUD
								ON RBU."IDRBUmum" = RBUD."IDRBUmum"
							INNER JOIN tbl_suplier AS SUP
								ON RBU."IDSupplier" = SUP."IDSupplier"
							INNER JOIN tbl_satuan AS SAT
								ON RBUD."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON RBUD."IDBarang" = BAR."IDBarang"
							WHERE RBU."Batal" = 'aktif'
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_kain"(OUT "idrpk" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPK, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Corak, Merk, Warna, Qty_yard, Qty_meter, Grade, Satuan, Harga, Sub_total IN
         SELECT
	RPK."IDRPK" AS "IDRPK",
	RPK."Tanggal" AS "Tanggal",
	RPK."Nomor" AS Nomor,
	RPK."Tanggal_fj" AS Tanggal_fj,
	PK."Nomor" AS "Nomor2",
	BAR."Nama_Barang" AS "Nama_Barang",
	COR."Corak" AS "Corak",
	MERK."Merk" AS "Merk",
	WAR."Warna" AS "Warna",
	RPKD."Qty_yard" AS "Qty_yard",
	RPKD."Qty_meter" AS "Qty_meter",
	RPKD."Grade" AS "Grade",
	Satuan."Satuan" AS "Satuan",
	RPKD."Harga" AS "Harga",
	RPKD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_kain AS RPK
	INNER JOIN tbl_retur_penjualan_kain_detail AS RPKD ON RPK."IDRPK" = RPKD."IDRPK"
	INNER JOIN tbl_penjualan_kain AS PK ON PK."IDFJK" = RPK."IDFJK"
	INNER JOIN tbl_corak AS COR ON RPKD."IDCorak" = COR."IDCorak"
	INNER JOIN tbl_warna AS WAR ON RPKD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPKD."IDBarang" = BAR."IDBarang"
	INNER JOIN tbl_merk AS MERK ON RPKD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPKD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_retur_penjualan_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_retur_penjualan_seragam"(OUT "idrps" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_fj" date, OUT "nomor_fjk" varchar, OUT "nama" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "saldo_qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDRPS, Tanggal, Nomor, Tanggal_fj, Nomor_fjk, Nama_Barang, Nama, Qty, Saldo_qty, Satuan, Harga, Sub_total IN
         SELECT
	RPS."IDRPS" AS "IDRPS",
	RPS."Tanggal" AS "Tanggal",
	RPS."Nomor" AS Nomor,
	RPS."Tanggal_fj" AS Tanggal_fj,
	PS."Nomor" AS "Nomor2",
	CUS."Nama" AS "NamaCus",
	BAR."Nama_Barang" AS "Nama_Barang",
-- 	COR."Corak" AS "Corak",
-- 	MERK."Merk" AS "Merk",
-- 	WAR."Warna" AS "Warna",
	RPSD."Saldo_qty" AS "Qty",
	RPSD."Saldo_qty" AS "Saldo_qty",
	Satuan."Satuan" AS "Satuan",
	RPSD."Harga" AS "Harga",
	RPSD."Sub_total" AS "Sub_total" 
FROM
	tbl_retur_penjualan_seragam AS RPS
	INNER JOIN tbl_retur_penjualan_detail AS RPSD ON RPS."IDRPS" = RPSD."IDRPS"
	INNER JOIN tbl_penjualan_seragam AS PS ON PS."IDFJS" = RPS."IDFJS"
	INNER JOIN tbl_customer AS CUS ON RPS."IDCustomer" = CUS."IDCustomer"
-- 	INNER JOIN tbl_corak AS COR ON RPSD."IDCorak" = COR."IDCorak"
-- 	INNER JOIN tbl_warna AS WAR ON RPSD."IDWarna" = WAR."IDWarna"
	INNER JOIN tbl_barang AS BAR ON RPSD."IDBarang" = BAR."IDBarang"
-- 	INNER JOIN tbl_merk AS MERK ON RPSD."IDMerk" = MERK."IDMerk"
	INNER JOIN tbl_satuan AS SATUAN ON RPSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_kain"(OUT "idsok" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "nama" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOK, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang,Nama, Corak, Merk, Warna, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOK."IDSOK" as "IDSOK", SOK."Tanggal" As SOK, SOK."Nomor" As Nomor, SOK."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOK."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang", CUS."Nama" as "Nama",COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
						SOKD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOKD."Harga" as "Harga", SOKD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_kain AS SOK
							INNER JOIN tbl_sales_order_kain_detail AS SOKD
								ON SOK."IDSOK" = SOKD."IDSOK"
							INNER JOIN tbl_customer AS CUS
								ON SOK."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_corak AS COR
								ON SOKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SOKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SOKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SOKD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOKD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_sales_order_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_sales_order_seragam"(OUT "idsos" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "tanggal_jatuh_tempo" date, OUT "no_po_customer" varchar, OUT "nama_barang" varchar, OUT "qty" float8, OUT "satuan" varchar, OUT "harga" float8, OUT "sub_total" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSOS, Tanggal, Nomor, Tanggal_jatuh_tempo, No_po_customer, Nama_Barang, Qty, Satuan, Harga, Sub_total IN
        SELECT 
						SOS."IDSOS" as "IDSOS", SOS."Tanggal" As SOS, SOS."Nomor" As Nomor, SOS."Tanggal_jatuh_tempo" As "Tanggal_jatuh_tempo", SOS."No_po_customer" As No_po_customer, BAR."Nama_Barang" as "Nama_Barang",
						SOSD."Qty" as "Qty", SATUAN."Satuan" as "Satuan", SOSD."Harga" as "Harga", SOSD."Sub_total" as "Sub_total"
							FROM tbl_sales_order_seragam AS SOS 
							INNER JOIN tbl_sales_order_seragam_detail AS SOSD
								ON SOS."IDSOS" = SOSD."IDSOS"
-- 							INNER JOIN tbl_corak AS COR
-- 								ON SOSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SOSD."IDWarna" = WAR."IDWarna"
-- 							INNER JOIN tbl_merk AS MERK
-- 								ON SOSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
 								ON SOSD."IDSatuan" = SATUAN."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON SOSD."IDBarang" = BAR."IDBarang"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_scan
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_scan"(OUT "idin" varchar, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "panjang_yard" float8, OUT "panjang_meter" float8, OUT "grade" varchar, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDIn, Barcode, Nama_Barang, Corak, Warna, Merk, Panjang_yard, Panjang_meter, Grade, Satuan IN
        SELECT 
						MJ."IDIn" as "IDIn", MJ."Barcode" As Barcode, BAR."Nama_Barang" AS "Nama_Barang", COR."Corak" AS "Corak", WAR."Warna" AS "Warna", MER."Merk" AS "Merk", MJ."Panjang_yard" AS "Panjang_yard", MJ."Panjang_meter" AS "Panjang_meter", MJ."Grade" AS "Grade", SAT."Satuan" AS "Satuan"
							FROM tbl_in AS MJ
							INNER JOIN tbl_satuan AS SAT
								ON MJ."IDSatuan" = SAT."IDSatuan"
							INNER JOIN tbl_barang AS BAR
								ON MJ."IDBarang" = BAR."IDBarang"
								INNER JOIN tbl_merk AS MER
								ON MJ."IDMerk" = MER."IDMerk"
								INNER JOIN tbl_warna AS WAR
								ON MJ."IDWarna" = WAR."IDWarna"
								INNER JOIN tbl_corak AS COR
								ON MJ."IDCorak" = COR."IDCorak"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_stokopname
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8);
CREATE OR REPLACE FUNCTION "public"."laporan_stokopname"(OUT "tanggal" date, OUT "barcode" varchar, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "warna" varchar, OUT "gudang" varchar, OUT "qty_yard" float8, OUT "qty_meter" float8, OUT "grade" varchar, OUT "satuan" varchar, OUT "harga" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$ BEGIN
	FOR 
	Tanggal,
	Barcode,
	Nama_Barang,
	Corak,
	Warna,
	Gudang,
	Qty_yard,
	Qty_meter,
	Grade,
	Satuan,
	Harga
	IN SELECT
	T1."Tanggal" AS Tanggal,
	T1. "Barcode" AS Barcode,
	T6. "Nama_Barang" AS Nama_Barang,
	T3. "Corak" AS Corak,
	T4 . "Warna" AS Warna,
	T1 . "IDGudang" AS Gudang,
	T1 . "Qty_yard" AS Qty_yard,
	T1 . "Qty_meter" AS Qty_meter,
	T1 . "Grade" AS Grade,
	T7. "Satuan" AS Satuan,
	T1. "Harga" AS Harga
FROM
	tbl_stok_opname AS T1
	JOIN tbl_corak AS T3 ON T1."IDCorak" = T3."IDCorak"
	JOIN tbl_warna AS T4 ON T1."IDWarna" = T4."IDWarna"
	JOIN tbl_barang AS T6 ON T1."IDBarang" = T6."IDBarang"
	JOIN tbl_satuan AS T7 ON T1."IDSatuan" = T7."IDSatuan"
	
	LOOP
	RETURN NEXT;

END LOOP;
RETURN;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_kain
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_kain"(OUT "idsjck" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_pac" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty_roll" float8, OUT "qty_yard" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCK, Tanggal, Nomor, Nomor_pac, Nama, Tanggal_kirim, Corak, Merk, Warna, Qty_roll, Qty_yard, Satuan IN
        SELECT 
						SJCK."IDSJCK" as "IDSJCK", SJCK."Tanggal" As "Tanggal", SJCK."Nomor" As Nomor, PL."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCK."Tanggal_kirim" as "Tanggal_kirim", COR."Corak" as "Corak", MERK."Merk" as "Merk", WAR."Warna" AS "Warna",
SJCKD."Qty_roll" as "Qty_roll", SJCKD."Qty_yard" as "Qty_yard", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_kain AS SJCK
							INNER JOIN tbl_packing_list AS PL
								ON SJCK."IDPAC" = PL."IDPAC"
							INNER JOIN tbl_surat_jalan_customer_kain_detail AS SJCKD
								ON SJCK."IDSJCK" = SJCKD."IDSJCK"
							INNER JOIN tbl_corak AS COR
								ON SJCKD."IDCorak" = COR."IDCorak"
							INNER JOIN tbl_warna AS WAR
								ON SJCKD."IDWarna" = WAR."IDWarna"
							INNER JOIN tbl_merk AS MERK
								ON SJCKD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCKD."IDSatuan" = SATUAN."IDSatuan"
--							INNER JOIN tbl_barang AS BAR
--								ON SJCKD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCK."IDCustomer" = CUS."IDCustomer"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for laporan_surat_jalan_customer_seragam
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar);
CREATE OR REPLACE FUNCTION "public"."laporan_surat_jalan_customer_seragam"(OUT "idsjcs" varchar, OUT "tanggal" date, OUT "nomor" varchar, OUT "nomor_sos" varchar, OUT "nama" varchar, OUT "tanggal_kirim" date, OUT "nama_barang" varchar, OUT "corak" varchar, OUT "merk" varchar, OUT "warna" varchar, OUT "qty" float8, OUT "qty_roll" float8, OUT "satuan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR IDSJCS, Tanggal, Nomor, Nomor_sos, Nama, Tanggal_kirim, Nama_Barang, Qty, Qty_roll, Satuan IN
         SELECT 
						SJCS."IDSJCS" as "IDSJCS", SJCS."Tanggal" As "Tanggal", SJCS."Nomor" As Nomor, SOS."Nomor" As "Nomor2", CUS."Nama" As "Nama", SJCS."Tanggal_kirim" as "Tanggal_kirim", BAR."Nama_Barang" as "Nama_Barang",
SJCSD."Saldo_qty" as "Qty", SJCSD."Qty_roll" as "Qty_roll", SATUAN."Satuan" as "Satuan"
							FROM tbl_surat_jalan_customer_seragam AS SJCS
							INNER JOIN tbl_surat_jalan_customer_seragam_detail AS SJCSD
								ON SJCS."IDSJCS" = SJCSD."IDSJCS"
							INNER JOIN tbl_sales_order_seragam AS SOS
								ON SOS."IDSOS" = SJCS."IDSOS"
--							INNER JOIN tbl_corak AS COR
--								ON SJCSD."IDCorak" = COR."IDCorak"
-- 							INNER JOIN tbl_warna AS WAR
-- 								ON SJCSD."IDWarna" = WAR."IDWarna"
--							INNER JOIN tbl_merk AS MERK
--								ON SJCSD."IDMerk" = MERK."IDMerk"
							INNER JOIN tbl_barang AS BAR
								ON SJCSD."IDBarang" = BAR."IDBarang"
							INNER JOIN tbl_customer AS CUS
								ON SJCS."IDCustomer" = CUS."IDCustomer"
							INNER JOIN tbl_satuan AS SATUAN
								ON SJCSD."IDSatuan" = SATUAN."IDSatuan"
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."notes"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for notes_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."notes_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_setting_notes."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_notes."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_notes ON tbl_jurnal."IDCOA"=tbl_setting_notes."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_notes."IDSettingNotes", tbl_setting_notes."Kategori",tbl_jurnal."IDCOA", tbl_setting_notes."Keterangan",tbl_setting_notes."Perhitungan"  ORDER BY tbl_setting_notes."IDSettingNotes" ASC
)m1
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar);
CREATE OR REPLACE FUNCTION "public"."pl"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m1.Bulan_sd_bulan, m2.Total_bulan_ini, m1."Perhitungan"
FROM (SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND '1'
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan" ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC) m1
LEFT JOIN (
SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = '1'
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan"  ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8);
CREATE OR REPLACE FUNCTION "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8, IN "caritahun" float8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m2.Total_bulan_ini, m1.Bulan_sd_bulan,m1."Perhitungan"
FROM (SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" AND
(date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND caribulan) AND date_part('year',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan" ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC) m1
LEFT JOIN (
SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan AND date_part('year',tbl_jurnal."Tanggal")=caritahun
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan"  ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for pl_cari
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8);
CREATE OR REPLACE FUNCTION "public"."pl_cari"(OUT "kategori" varchar, OUT "keterangan" varchar, OUT "idcoa" varchar, OUT "total_bulan_ini" float8, OUT "bulan_sd_bulan" float8, OUT "perhitungan" varchar, IN "caribulan" int8)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Kategori, Keterangan, IDCOA, Total_bulan_ini, Bulan_sd_bulan, Perhitungan IN
       
SELECT m1."Kategori", m1."Keterangan", m1."IDCOA", m2.Total_bulan_ini, m1.Bulan_sd_bulan,m1."Perhitungan"
FROM (SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Bulan_sd_bulan, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" AND
date_part('month',tbl_jurnal."Tanggal") BETWEEN '1' AND caribulan
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan" ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC) m1
LEFT JOIN (
SELECT tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_setting_labarugi."Keterangan", tbl_jurnal."IDCOA", SUM(tbl_jurnal."Debet"-tbl_jurnal."Kredit") AS Total_bulan_ini, tbl_setting_labarugi."Perhitungan" FROM tbl_jurnal 
RIGHT JOIN tbl_setting_labarugi ON tbl_jurnal."IDCOA"=tbl_setting_labarugi."IDCoa" 
AND date_part('month',tbl_jurnal."Tanggal") = caribulan
GROUP BY tbl_setting_labarugi."IDLabaRugi", tbl_setting_labarugi."Kategori",tbl_jurnal."IDCOA", tbl_setting_labarugi."Keterangan",tbl_setting_labarugi."Perhitungan"  ORDER BY tbl_setting_labarugi."IDLabaRugi" ASC
)m2	USING ("IDCOA")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for stock
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."stock"(OUT "corak" varchar, OUT "grade" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "persediaan_awal_qty" float8, OUT "persediaan_awal_nilai" float8, OUT "beli_qty" float8, OUT "beli_nilai" float8, OUT "retur_beli_qty" float8, OUT "retur_beli_nilai" float8, OUT "koreksi_qty" float8, OUT "koreksi_nilai" float8, OUT "koreksi_scan_qty" float8, OUT "koreksi_scan_nilai" float8, OUT "hpp_qty" float8, OUT "hpp_nilai" float8, OUT "sample_qty" float8, OUT "sample_nilai" float8, OUT "retur_jual_qty" float8, OUT "retur_jual_nilai" float8, OUT "persediaan_akhir_qty" float8, OUT "persediaan_akhir_nilai" float8, IN "tanggal_mulai" date, IN "tanggal_selesai" date);
CREATE OR REPLACE FUNCTION "public"."stock"(OUT "corak" varchar, OUT "grade" varchar, OUT "warna" varchar, OUT "merk" varchar, OUT "satuan" varchar, OUT "harga" float8, OUT "persediaan_awal_qty" float8, OUT "persediaan_awal_nilai" float8, OUT "beli_qty" float8, OUT "beli_nilai" float8, OUT "retur_beli_qty" float8, OUT "retur_beli_nilai" float8, OUT "koreksi_qty" float8, OUT "koreksi_nilai" float8, OUT "koreksi_scan_qty" float8, OUT "koreksi_scan_nilai" float8, OUT "hpp_qty" float8, OUT "hpp_nilai" float8, OUT "sample_qty" float8, OUT "sample_nilai" float8, OUT "retur_jual_qty" float8, OUT "retur_jual_nilai" float8, OUT "persediaan_akhir_qty" float8, OUT "persediaan_akhir_nilai" float8, IN "tanggal_mulai" date, IN "tanggal_selesai" date)
  RETURNS SETOF "pg_catalog"."record" AS $BODY$
BEGIN
    FOR Corak, Grade, Warna, Merk, Satuan, Harga, Persediaan_awal_qty, Persediaan_awal_nilai, Beli_qty, Beli_nilai, Retur_beli_qty, Retur_beli_nilai, Koreksi_qty, Koreksi_nilai, Koreksi_scan_qty, Koreksi_scan_nilai, Hpp_qty, Hpp_nilai, Sample_qty, Sample_nilai, Retur_jual_qty, Retur_jual_nilai, Persediaan_akhir_qty, Persediaan_akhir_nilai IN
       
SELECT m1."Corak", m1."Grade", m1."Warna", m1."Merk", m1."Satuan", m1."Harga", m1."persediaan_awal_qty", m1."persediaan_awal_nilai", m2."beli_qty", m2."beli_nilai", m3."retur_beli_qty", m3."retur_beli_nilai", m4."koreksi_qty", m4."koreksi_nilai", m5."koreksi_scan_qty", m5."koreksi_scan_nilai", m6."hpp_qty", m6."hpp_nilai", m7."sample_qty", m7."sample_nilai", m8."retur_jual_qty", m8."retur_jual_nilai", (m1."persediaan_awal_qty"+m2."beli_qty"-m3."retur_beli_qty"+m4."koreksi_qty"-m5."koreksi_scan_qty"-m6."hpp_qty"-m7."sample_qty"+m8."retur_jual_qty") AS persediaan_akhir_qty, (m1."persediaan_awal_nilai"+m2."beli_nilai"-m3."retur_beli_nilai"+m4."koreksi_nilai"-m5."koreksi_scan_nilai"-m6."hpp_nilai"-m7."sample_nilai"+m8."retur_jual_nilai") AS persediaan_akhir_nilai FROM
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS persediaan_awal_qty, coalesce(SUM(tbl_stok."Harga"),0) AS persediaan_awal_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SA' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m1
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS beli_qty, coalesce(SUM(tbl_stok."Harga"),0) AS beli_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='TBS' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m2 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS retur_beli_qty, coalesce(SUM(tbl_stok."Harga"),0) AS retur_beli_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='RB' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m3 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS koreksi_qty, coalesce(SUM(tbl_stok."Harga"),0) AS koreksi_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='KP' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m4 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS koreksi_scan_qty, coalesce(SUM(tbl_stok."Harga"),0) AS koreksi_scan_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='KPS' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m5 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS hpp_qty, coalesce(SUM(tbl_stok."Harga"),0) AS hpp_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SJ' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m6 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS sample_qty, coalesce(SUM(tbl_stok."Harga"),0) AS sample_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='SP' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m7 USING("Corak")
LEFT JOIN
(SELECT tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga", coalesce(SUM(tbl_stok."Saldo_yard"),0) AS retur_jual_qty, coalesce(SUM(tbl_stok."Harga"),0) AS retur_jual_nilai FROM tbl_stok 
join tbl_corak on tbl_stok."IDCorak"=tbl_corak."IDCorak" 
join tbl_warna on tbl_warna."IDWarna"=tbl_stok."IDWarna"
join tbl_merk on tbl_corak."IDMerk"=tbl_merk."IDMerk"
join tbl_satuan on tbl_satuan."IDSatuan"=tbl_stok."IDSatuan" WHERE tbl_stok."Jenis_faktur"='RJ' AND
tbl_stok."Tanggal" BETWEEN tanggal_mulai AND tanggal_selesai GROUP BY tbl_corak."Corak", tbl_stok."Grade", tbl_warna."Warna", tbl_merk."Merk", tbl_satuan."Satuan", tbl_stok."Harga") m8 USING("Corak")
								
							
    LOOP
        RETURN NEXT;
    END LOOP;
    RETURN;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

-- ----------------------------
-- Function structure for testing_insert
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."testing_insert"();
CREATE OR REPLACE FUNCTION "public"."testing_insert"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
         INSERT INTO tbl_coa_saldo_awal(idcoasaldoawal,idcoa,idgroupcoa, nilaisaldoawal, aktif)
         VALUES(NEW.IDCOASaldoAwal,NEW.IDCoa,NEW.IDGroupCOA,NEW.Nilai_Saldo_Awal, NEW.Aktif);
 
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- Function structure for update_stok
-- ----------------------------
DROP FUNCTION IF EXISTS "public"."update_stok"();
CREATE OR REPLACE FUNCTION "public"."update_stok"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
begin
update stokbarang set stok_ketersediaan=stok_ketersediaan-new.jumlah_beli where kode_barang=new.kode_barang;
return new;
end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

-- ----------------------------
-- View structure for V_listpiutang
-- ----------------------------
DROP VIEW IF EXISTS "public"."V_listpiutang";
CREATE VIEW "public"."V_listpiutang" AS  SELECT tbl_piutang."No_Faktur",
    tbl_piutang."Tanggal_Piutang",
    tbl_piutang."Jatuh_Tempo",
    tbl_piutang."Nilai_Piutang",
    tbl_piutang."Pembayaran",
    tbl_piutang."Saldo_Akhir"
   FROM tbl_piutang
  WHERE (((tbl_piutang."IDCustomer")::text = 'P100012'::text) AND (tbl_piutang."Saldo_Akhir" <> (0)::numeric))
  GROUP BY tbl_piutang."No_Faktur", tbl_piutang."Tanggal_Piutang", tbl_piutang."Jatuh_Tempo", tbl_piutang."Nilai_Piutang", tbl_piutang."Pembayaran", tbl_piutang."Saldo_Akhir"
UNION ALL
 SELECT tbl_um_customer."Nomor_Faktur" AS "No_Faktur",
    tbl_um_customer."Tanggal_UM" AS "Tanggal_Piutang",
    tbl_um_customer."Tanggal_UM" AS "Jatuh_Tempo",
    tbl_um_customer."Saldo_UM" AS "Nilai_Piutang",
    tbl_um_customer."Nilai_UM" AS "Pembayaran",
    tbl_um_customer."Saldo_UM" AS "Saldo_Akhir"
   FROM tbl_um_customer
  WHERE (((tbl_um_customer."IDCustomer")::text = 'P100012'::text) AND (tbl_um_customer."Saldo_UM" <> (0)::numeric))
  GROUP BY tbl_um_customer."Nomor_Faktur", tbl_um_customer."Tanggal_UM", tbl_um_customer."Saldo_UM", tbl_um_customer."Nilai_UM";

-- ----------------------------
-- View structure for jurnal_v
-- ----------------------------
DROP VIEW IF EXISTS "public"."jurnal_v";
CREATE VIEW "public"."jurnal_v" AS  SELECT tbl_jurnal."IDJurnal",
    tbl_jurnal."Tanggal",
    tbl_jurnal."Nomor",
    tbl_jurnal."IDFaktur",
    tbl_jurnal."IDFakturDetail",
    tbl_jurnal."Jenis_faktur",
    tbl_jurnal."Debet",
    tbl_jurnal."Kredit",
    tbl_jurnal."IDMataUang",
    tbl_jurnal."Kurs",
    tbl_jurnal."Total_debet",
    tbl_jurnal."Total_kredit",
    tbl_jurnal."Keterangan",
    tbl_jurnal."Saldo",
    tbl_coa."IDCoa",
    tbl_coa."Kode_COA",
    tbl_coa."Nama_COA"
   FROM (tbl_jurnal
     JOIN tbl_coa ON (((tbl_jurnal."IDCOA")::text = (tbl_coa."IDCoa")::text)));

-- ----------------------------
-- View structure for view_returpembelian
-- ----------------------------
DROP VIEW IF EXISTS "public"."view_returpembelian";
CREATE VIEW "public"."view_returpembelian" AS  SELECT tbl_retur_pembelian."IDRB",
    tbl_retur_pembelian."Tanggal",
    tbl_retur_pembelian."Nomor",
    tbl_retur_pembelian."IDFB",
    tbl_retur_pembelian."IDSupplier",
    tbl_retur_pembelian."Tanggal_fb",
    ( SELECT sum(tbl_retur_pembelian_detail."Qty_yard") AS total
           FROM tbl_retur_pembelian_detail
          WHERE ((tbl_retur_pembelian_detail."IDRB")::text = (tbl_retur_pembelian."IDRB")::text)) AS "Total_qty_yard",
    tbl_retur_pembelian."Total_qty_meter",
    tbl_retur_pembelian."Saldo_yard",
    tbl_retur_pembelian."Saldo_meter",
    tbl_retur_pembelian."Discount",
    tbl_retur_pembelian."Status_ppn",
    tbl_retur_pembelian."Keterangan",
    tbl_retur_pembelian."Batal",
    tbl_retur_pembelian."Status_pakai",
    tbl_supplier."Nama",
    tbl_pembelian."Nomor" AS "Nomor_fb"
   FROM ((tbl_retur_pembelian
     LEFT JOIN tbl_supplier ON (((tbl_retur_pembelian."IDSupplier")::text = (tbl_supplier."IDSupplier")::text)))
     LEFT JOIN tbl_pembelian ON (((tbl_retur_pembelian."IDFB")::text = (tbl_pembelian."IDFB")::text)));

-- ----------------------------
-- View structure for vlistgiro
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistgiro";
CREATE VIEW "public"."vlistgiro" AS  SELECT tbl_um_customer."IDUMCustomer" AS "IDGiro",
    tbl_um_customer."IDCustomer" AS "IDPerusahaan",
    tbl_um_customer."Tanggal_UM" AS "Tanggal",
    tbl_um_customer."IDFaktur",
    tbl_sales_order."Nomor" AS "Nomor_faktur",
    tbl_um_customer."Nomor_giro",
    tbl_um_customer."Tanggal_giro",
    tbl_um_customer."Nilai_UM" AS "Nominal_giro",
    tbl_um_customer."Batal",
    'UM Customer'::text AS "Jenis",
    'Customer'::text AS "Kategori"
   FROM (tbl_um_customer
     LEFT JOIN tbl_sales_order ON (((tbl_sales_order."IDSOK")::text = (tbl_um_customer."IDFaktur")::text)))
  WHERE ((tbl_um_customer."Jenis_Pembayaran")::text = 'giro'::text)
UNION
 SELECT tbl_penjualan_pembayaran."IDFJPembayaran" AS "IDGiro",
    tbl_penjualan."IDCustomer" AS "IDPerusahaan",
    tbl_penjualan."Tanggal",
    tbl_penjualan."Nomor" AS "IDFaktur",
    tbl_penjualan_pembayaran."IDFJ" AS "Nomor_faktur",
    tbl_penjualan_pembayaran."Nomor_giro",
    tbl_penjualan_pembayaran."Tanggal_giro",
    tbl_penjualan_pembayaran."NominalPembayaran" AS "Nominal_giro",
    tbl_penjualan_pembayaran."Batal",
    'Pembayaran Invoice'::text AS "Jenis",
    'Customer'::text AS "Kategori"
   FROM (tbl_penjualan_pembayaran
     LEFT JOIN tbl_penjualan ON (((tbl_penjualan."IDFJ")::text = (tbl_penjualan_pembayaran."IDFJ")::text)))
  WHERE ((tbl_penjualan_pembayaran."Jenis_pembayaran")::text = 'giro'::text)
UNION
 SELECT tbl_penerimaan_piutang_bayar."IDTPBayar" AS "IDGiro",
    tbl_penerimaan_piutang."IDCustomer" AS "IDPerusahaan",
    tbl_penerimaan_piutang."Tanggal",
    tbl_penerimaan_piutang."Nomor" AS "IDFaktur",
    tbl_penerimaan_piutang_bayar."IDTP" AS "Nomor_faktur",
    tbl_penerimaan_piutang_bayar."Nomor_giro",
    tbl_penerimaan_piutang_bayar."Tanggal_giro",
    tbl_penerimaan_piutang_bayar."Nominal_pembayaran" AS "Nominal_giro",
    tbl_penerimaan_piutang."Batal",
    'Penerimaan Piutang'::text AS "Jenis",
    'Customer'::text AS "Kategori"
   FROM (tbl_penerimaan_piutang_bayar
     LEFT JOIN tbl_penerimaan_piutang ON (((tbl_penerimaan_piutang."IDTP")::text = (tbl_penerimaan_piutang_bayar."IDTP")::text)))
  WHERE ((tbl_penerimaan_piutang_bayar."Jenis_pembayaran")::text = 'giro'::text);

-- ----------------------------
-- View structure for vlistjurnalumumdetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistjurnalumumdetail";
CREATE VIEW "public"."vlistjurnalumumdetail" AS  SELECT tbl_jurnal_umum_detail."IDJUDetail",
    tbl_jurnal_umum_detail."IDJU",
    tbl_jurnal_umum_detail."IDCOA",
    tbl_jurnal_umum_detail."Posisi",
    tbl_jurnal_umum_detail."Debet",
    tbl_jurnal_umum_detail."Kredit",
    tbl_jurnal_umum_detail."IDMataUang",
    tbl_jurnal_umum_detail."Kurs",
    tbl_jurnal_umum_detail."Keterangan",
    tbl_jurnal_umum."Tanggal",
    tbl_jurnal_umum."Nomor",
    tbl_coa."Kode_COA",
    tbl_coa."Nama_COA",
    tbl_jurnal_umum_detail."Urutan",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Negara",
    tbl_mata_uang."Kode"
   FROM (((tbl_jurnal_umum_detail
     LEFT JOIN tbl_jurnal_umum ON (((tbl_jurnal_umum_detail."IDJU")::text = (tbl_jurnal_umum."IDJU")::text)))
     LEFT JOIN tbl_coa ON (((tbl_jurnal_umum_detail."IDCOA")::text = (tbl_coa."IDCoa")::text)))
     JOIN tbl_mata_uang ON (((tbl_jurnal_umum_detail."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)));

-- ----------------------------
-- View structure for vlistkoreksipersediaan
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistkoreksipersediaan";
CREATE VIEW "public"."vlistkoreksipersediaan" AS  SELECT tbl_koreksi_persediaan."IDKP",
    tbl_koreksi_persediaan."Tanggal",
    tbl_koreksi_persediaan."Nomor",
    tbl_koreksi_persediaan."Total",
    tbl_koreksi_persediaan."IDMataUang",
    tbl_koreksi_persediaan."Kurs",
    tbl_koreksi_persediaan."Jenis",
    tbl_koreksi_persediaan."Keterangan",
    tbl_koreksi_persediaan."Batal",
    tbl_koreksi_persediaan.dibuat_pada,
    tbl_koreksi_persediaan.dibuat_oleh,
    tbl_koreksi_persediaan.diubah_pada,
    tbl_koreksi_persediaan.diubah_oleh,
    tbl_koreksi_persediaan."IDGudang",
    tbl_gudang."Nama_Gudang",
    tbl_gudang."Kode_Gudang"
   FROM (tbl_koreksi_persediaan
     JOIN tbl_gudang ON (((tbl_koreksi_persediaan."IDGudang")::text = (tbl_gudang."IDGudang")::text)));

-- ----------------------------
-- View structure for vlistkoreksipersediaandetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistkoreksipersediaandetail";
CREATE VIEW "public"."vlistkoreksipersediaandetail" AS  SELECT tbl_koreksi_persediaan_detail."IDKPDetail",
    tbl_koreksi_persediaan_detail."IDSatuan",
    tbl_koreksi_persediaan_detail."Barcode",
    tbl_koreksi_persediaan_detail."IDBarang",
    tbl_koreksi_persediaan_detail."IDKP",
    tbl_koreksi_persediaan_detail."IDMataUang",
    tbl_koreksi_persediaan_detail."Qty",
    tbl_koreksi_persediaan_detail."Saldo_qty",
    tbl_koreksi_persediaan_detail."Harga",
    tbl_koreksi_persediaan_detail."Kurs",
    tbl_barang."Kode_Barang",
    tbl_barang."Nama_Barang",
    tbl_satuan."Kode_Satuan",
    tbl_satuan."Satuan",
    tbl_koreksi_persediaan."Tanggal",
    tbl_koreksi_persediaan."Nomor",
    tbl_koreksi_persediaan."Batal",
    tbl_koreksi_persediaan."IDGudang",
    tbl_gudang."Nama_Gudang"
   FROM ((((tbl_koreksi_persediaan_detail
     LEFT JOIN tbl_barang ON (((tbl_koreksi_persediaan_detail."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_koreksi_persediaan_detail."IDSatuan")::text = (tbl_satuan."IDSatuan")::text)))
     LEFT JOIN tbl_koreksi_persediaan ON (((tbl_koreksi_persediaan_detail."IDKP")::text = (tbl_koreksi_persediaan."IDKP")::text)))
     LEFT JOIN tbl_gudang ON (((tbl_koreksi_persediaan."IDGudang")::text = (tbl_gudang."IDGudang")::text)));

-- ----------------------------
-- View structure for vlistpembayaranhutang
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpembayaranhutang";
CREATE VIEW "public"."vlistpembayaranhutang" AS  SELECT tbl_pembayaran_hutang."IDBS",
    tbl_pembayaran_hutang."Tanggal",
    tbl_pembayaran_hutang."Nomor",
    tbl_pembayaran_hutang."IDSupplier",
    tbl_pembayaran_hutang."IDMataUang",
    tbl_pembayaran_hutang."Kurs",
    tbl_pembayaran_hutang."Total",
    tbl_pembayaran_hutang."Uang_muka",
    tbl_pembayaran_hutang."Selisih",
    tbl_pembayaran_hutang."Kompensasi_um",
    tbl_pembayaran_hutang."Pembayaran",
    tbl_pembayaran_hutang."Kelebihan_bayar",
    tbl_pembayaran_hutang."Keterangan",
    tbl_pembayaran_hutang."Batal",
    tbl_pembayaran_hutang."Nilai_kurs",
    tbl_supplier."Nama",
    tbl_supplier."Kode_Suplier",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Kode"
   FROM ((tbl_pembayaran_hutang
     LEFT JOIN tbl_supplier ON (((tbl_pembayaran_hutang."IDSupplier")::text = (tbl_supplier."IDSupplier")::text)))
     LEFT JOIN tbl_mata_uang ON (((tbl_pembayaran_hutang."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)));

-- ----------------------------
-- View structure for vlistpembayaranhutangbayar
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpembayaranhutangbayar";
CREATE VIEW "public"."vlistpembayaranhutangbayar" AS  SELECT tbl_pembayaran_hutang_bayar."IDBSBayar",
    tbl_pembayaran_hutang_bayar."IDBS",
    tbl_pembayaran_hutang_bayar."Jenis_pembayaran",
    tbl_pembayaran_hutang_bayar."IDCOA",
    tbl_pembayaran_hutang_bayar."Nomor_giro",
    tbl_pembayaran_hutang_bayar."Nominal_pembayaran",
    tbl_pembayaran_hutang_bayar."IDMataUang",
    tbl_pembayaran_hutang_bayar."Kurs",
    tbl_pembayaran_hutang_bayar."Status_giro",
    tbl_pembayaran_hutang_bayar."Tanggal_giro",
    tbl_pembayaran_hutang_bayar."CID",
    tbl_pembayaran_hutang_bayar."CTime",
    tbl_pembayaran_hutang_bayar."MID",
    tbl_pembayaran_hutang_bayar."MTime",
    tbl_pembayaran_hutang_bayar."Nilai_kurs",
    tbl_pembayaran_hutang."Nomor",
    tbl_mata_uang."Kode",
    tbl_mata_uang."Mata_uang",
    tbl_coa."Kode_COA",
    tbl_coa."Nama_COA"
   FROM (((tbl_pembayaran_hutang_bayar
     LEFT JOIN tbl_pembayaran_hutang ON (((tbl_pembayaran_hutang_bayar."IDBS")::text = (tbl_pembayaran_hutang."IDBS")::text)))
     LEFT JOIN tbl_mata_uang ON (((tbl_pembayaran_hutang_bayar."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)))
     LEFT JOIN tbl_coa ON (((tbl_pembayaran_hutang_bayar."IDCOA")::text = (tbl_coa."IDCoa")::text)));

-- ----------------------------
-- View structure for vlistpenerimaanpiutang
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpenerimaanpiutang";
CREATE VIEW "public"."vlistpenerimaanpiutang" AS  SELECT tbl_penerimaan_piutang."IDTP",
    tbl_penerimaan_piutang."Tanggal",
    tbl_penerimaan_piutang."Nomor",
    tbl_penerimaan_piutang."IDCustomer",
    tbl_penerimaan_piutang."IDMataUang",
    tbl_penerimaan_piutang."Kurs",
    tbl_penerimaan_piutang."Total",
    tbl_penerimaan_piutang."Uang_muka",
    tbl_penerimaan_piutang."Selisih",
    tbl_penerimaan_piutang."Kompensasi_um",
    tbl_penerimaan_piutang."Pembayaran",
    tbl_penerimaan_piutang."Kelebihan_bayar",
    tbl_penerimaan_piutang."Keterangan",
    tbl_penerimaan_piutang."Batal",
    tbl_mata_uang."Kode",
    tbl_mata_uang."Mata_uang",
    tbl_customer."Kode_Customer",
    tbl_customer."Nama"
   FROM ((tbl_penerimaan_piutang
     LEFT JOIN tbl_mata_uang ON (((tbl_penerimaan_piutang."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)))
     LEFT JOIN tbl_customer ON (((tbl_penerimaan_piutang."IDCustomer")::text = (tbl_customer."IDCustomer")::text)));

-- ----------------------------
-- View structure for vlistpenerimaanpiutangbayar
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpenerimaanpiutangbayar";
CREATE VIEW "public"."vlistpenerimaanpiutangbayar" AS  SELECT tbl_penerimaan_piutang."Tanggal",
    tbl_penerimaan_piutang."Nomor",
    tbl_penerimaan_piutang."IDCustomer",
    tbl_penerimaan_piutang."Keterangan",
    tbl_penerimaan_piutang."Batal",
    tbl_customer."Nama",
    tbl_penerimaan_piutang_bayar."Jenis_pembayaran",
    tbl_penerimaan_piutang_bayar."Nominal_pembayaran",
    tbl_penerimaan_piutang_bayar."IDMataUang",
    tbl_penerimaan_piutang_bayar."Kurs",
    tbl_mata_uang."Kode",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Negara",
    tbl_penerimaan_piutang_bayar."IDTPBayar",
    tbl_penerimaan_piutang_bayar."IDTP",
    tbl_penerimaan_piutang_bayar."IDCOA",
    tbl_penerimaan_piutang_bayar."Nomor_giro",
    tbl_penerimaan_piutang_bayar."Status_giro",
    tbl_penerimaan_piutang_bayar."Tanggal_giro",
    tbl_coa."Nama_COA",
    tbl_coa."Kode_COA"
   FROM ((((tbl_penerimaan_piutang
     JOIN tbl_customer ON (((tbl_penerimaan_piutang."IDCustomer")::text = (tbl_customer."IDCustomer")::text)))
     JOIN tbl_penerimaan_piutang_bayar ON (((tbl_penerimaan_piutang."IDTP")::text = (tbl_penerimaan_piutang_bayar."IDTP")::text)))
     JOIN tbl_mata_uang ON (((tbl_penerimaan_piutang_bayar."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)))
     JOIN tbl_coa ON (((tbl_penerimaan_piutang_bayar."IDCOA")::text = (tbl_coa."IDCoa")::text)));

-- ----------------------------
-- View structure for vlistpiutang
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpiutang";
CREATE VIEW "public"."vlistpiutang" AS  SELECT tbl_piutang."Tanggal_Piutang",
    tbl_piutang."Jatuh_Tempo",
    tbl_piutang."No_Faktur",
    tbl_piutang."IDCustomer",
    tbl_piutang."Jenis_Faktur",
    tbl_piutang."Nilai_Piutang",
    tbl_piutang."Saldo_Awal",
    tbl_piutang."Pembayaran",
    tbl_piutang."Saldo_Akhir",
    tbl_piutang."IDPiutang",
    tbl_piutang."IDFaktur",
    tbl_piutang."UM",
    tbl_piutang."DISC",
    tbl_piutang."Retur",
    tbl_piutang."Selisih",
    tbl_piutang."Kontra_bon",
    tbl_piutang."Saldo_kontra_bon",
    tbl_sales_order."IDMataUang",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Kode",
    tbl_piutang."Batal"
   FROM ((((tbl_piutang
     LEFT JOIN tbl_penjualan ON (((tbl_piutang."IDFaktur")::text = (tbl_penjualan."IDFJ")::text)))
     LEFT JOIN tbl_surat_jalan_customer ON (((tbl_penjualan."IDSJC")::text = (tbl_surat_jalan_customer.idsjc)::text)))
     LEFT JOIN tbl_sales_order ON (((tbl_surat_jalan_customer.idso)::text = (tbl_sales_order."IDSOK")::text)))
     LEFT JOIN tbl_mata_uang ON (((tbl_sales_order."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)));

-- ----------------------------
-- View structure for vlistpenerimaanpiutangdetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpenerimaanpiutangdetail";
CREATE VIEW "public"."vlistpenerimaanpiutangdetail" AS  SELECT tbl_penerimaan_piutang_detail."IDTPDetail",
    tbl_penerimaan_piutang_detail."IDTP",
    tbl_penerimaan_piutang_detail."IDFJ",
    tbl_penerimaan_piutang_detail."Nomor",
    tbl_penerimaan_piutang_detail."Tanggal_fj",
    tbl_penerimaan_piutang_detail."Nilai_fj",
    tbl_penerimaan_piutang_detail."Telah_diterima",
    tbl_penerimaan_piutang_detail."Diterima",
    tbl_penerimaan_piutang_detail."UM",
    tbl_penerimaan_piutang_detail."Retur",
    tbl_penerimaan_piutang_detail."Saldo",
    tbl_penerimaan_piutang_detail."Selisih",
    tbl_penerimaan_piutang_detail."IDMataUang",
    tbl_penerimaan_piutang_detail."Kurs",
    tbl_penerimaan_piutang_detail."IDPiutang",
    vlistpiutang."Nilai_Piutang",
    vlistpiutang."Saldo_Awal",
    vlistpiutang."No_Faktur",
    vlistpiutang."Jenis_Faktur",
    vlistpiutang."Pembayaran",
    vlistpiutang."Tanggal_Piutang",
    vlistpiutang."Jatuh_Tempo",
    tbl_customer."Nama",
    vlistpiutang."Saldo_Akhir"
   FROM ((tbl_penerimaan_piutang_detail
     JOIN vlistpiutang ON (((tbl_penerimaan_piutang_detail."IDPiutang")::text = (vlistpiutang."IDPiutang")::text)))
     JOIN tbl_customer ON (((vlistpiutang."IDCustomer")::text = (tbl_customer."IDCustomer")::text)));

-- ----------------------------
-- View structure for vlistpenjualandetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpenjualandetail";
CREATE VIEW "public"."vlistpenjualandetail" AS  SELECT tbl_penjualan_detail."IDFJDetail",
    tbl_penjualan_detail."IDFJ",
    tbl_penjualan_detail."Qty",
    tbl_penjualan_detail."Saldo_qty",
    tbl_penjualan_detail."IDSatuan",
    tbl_penjualan_detail."Harga",
    tbl_penjualan_detail."Sub_total",
    tbl_penjualan."Nomor",
    tbl_barang."Nama_Barang",
    tbl_satuan."Satuan",
    tbl_penjualan."IDCustomer",
    tbl_customer."Nama",
    tbl_penjualan_detail."IDBarang",
    tbl_barang."Kode_Barang",
    tbl_penjualan."Tanggal",
    tbl_penjualan."IDMataUang",
    tbl_mata_uang."Kode",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Negara"
   FROM (((((tbl_penjualan_detail
     LEFT JOIN tbl_penjualan ON (((tbl_penjualan_detail."IDFJ")::text = (tbl_penjualan."IDFJ")::text)))
     LEFT JOIN tbl_barang ON (((tbl_penjualan_detail."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_penjualan_detail."IDSatuan")::text = (tbl_satuan."IDSatuan")::text)))
     JOIN tbl_customer ON (((tbl_penjualan."IDCustomer")::text = (tbl_customer."IDCustomer")::text)))
     JOIN tbl_mata_uang ON (((tbl_penjualan."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)));

-- ----------------------------
-- View structure for vlistperusahaan
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistperusahaan";
CREATE VIEW "public"."vlistperusahaan" AS  SELECT tbl_supplier."IDSupplier" AS "ID",
    tbl_supplier."Kode_Suplier" AS "Kode",
    tbl_supplier."Nama",
    'Supplier'::text AS "Kategori"
   FROM tbl_supplier
UNION
 SELECT tbl_customer."IDCustomer" AS "ID",
    tbl_customer."Kode_Customer" AS "Kode",
    tbl_customer."Nama",
    'Customer'::text AS "Kategori"
   FROM tbl_customer;

-- ----------------------------
-- View structure for vlistpindahgudang
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpindahgudang";
CREATE VIEW "public"."vlistpindahgudang" AS  SELECT tbl_pindah_gudang."IDPG",
    tbl_pindah_gudang."Tanggal",
    tbl_pindah_gudang."Nomor",
    tbl_pindah_gudang."Total",
    tbl_pindah_gudang."IDMataUang",
    tbl_pindah_gudang."Kurs",
    tbl_pindah_gudang."Keterangan",
    tbl_pindah_gudang."Batal",
    tbl_pindah_gudang."IDGudangAwal",
    tbl_pindah_gudang."IDGudangTujuan",
    tbl_pindah_gudang.dibuat_pada,
    tbl_pindah_gudang.dibuat_oleh,
    tbl_pindah_gudang.diubah_pada,
    tbl_pindah_gudang.diubah_oleh,
    gudang_asal."Nama_Gudang" AS "Nama_gudang_asal",
    gudang_asal."Kode_Gudang" AS "Kode_gudang_asal",
    gudang_tujuan."Kode_Gudang" AS "Kode_gudang_tujuan",
    gudang_tujuan."Nama_Gudang" AS "Nama_gudang_tujuan"
   FROM ((tbl_pindah_gudang
     JOIN tbl_gudang gudang_asal ON (((tbl_pindah_gudang."IDGudangAwal")::text = (gudang_asal."IDGudang")::text)))
     JOIN tbl_gudang gudang_tujuan ON (((tbl_pindah_gudang."IDGudangTujuan")::text = (gudang_tujuan."IDGudang")::text)));

-- ----------------------------
-- View structure for vlistpindahgudangdetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpindahgudangdetail";
CREATE VIEW "public"."vlistpindahgudangdetail" AS  SELECT tbl_pindah_gudang_detail."IDPGDetail",
    tbl_pindah_gudang_detail."IDSatuan",
    tbl_pindah_gudang_detail."Barcode",
    tbl_pindah_gudang_detail."IDBarang",
    tbl_pindah_gudang_detail."IDPG",
    tbl_pindah_gudang_detail."IDMataUang",
    tbl_pindah_gudang_detail."Qty",
    tbl_pindah_gudang_detail."Saldo",
    tbl_pindah_gudang_detail."Harga",
    tbl_pindah_gudang_detail."Kurs",
    tbl_pindah_gudang."Tanggal",
    tbl_pindah_gudang."Nomor",
    tbl_pindah_gudang."Total",
    tbl_pindah_gudang."Keterangan",
    tbl_barang."Kode_Barang",
    tbl_barang."Nama_Barang",
    tbl_satuan."Kode_Satuan",
    tbl_satuan."Satuan",
    tbl_pindah_gudang."IDGudangAwal",
    gudang_asal."Nama_Gudang" AS "Nama_gudang_asal",
    tbl_pindah_gudang."IDGudangTujuan",
    gudang_tujuan."Nama_Gudang" AS "Nama_gudang_tujuan"
   FROM (((((tbl_pindah_gudang_detail
     LEFT JOIN tbl_pindah_gudang ON (((tbl_pindah_gudang_detail."IDPG")::text = (tbl_pindah_gudang."IDPG")::text)))
     LEFT JOIN tbl_barang ON (((tbl_pindah_gudang_detail."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_pindah_gudang_detail."IDSatuan")::text = (tbl_satuan."IDSatuan")::text)))
     JOIN tbl_gudang gudang_asal ON (((tbl_pindah_gudang."IDGudangAwal")::text = (gudang_asal."IDGudang")::text)))
     JOIN tbl_gudang gudang_tujuan ON (((tbl_pindah_gudang."IDGudangTujuan")::text = (gudang_tujuan."IDGudang")::text)));

-- ----------------------------
-- View structure for vlistreturpenjualan
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistreturpenjualan";
CREATE VIEW "public"."vlistreturpenjualan" AS  SELECT tbl_retur_penjualan."IDRP",
    tbl_retur_penjualan."Tanggal",
    tbl_retur_penjualan."Nomor",
    tbl_retur_penjualan."IDFJ",
    tbl_retur_penjualan."IDCustomer",
    tbl_retur_penjualan."Tanggal_fj",
    tbl_retur_penjualan."Total_qty",
    tbl_retur_penjualan."Saldo_qty",
    tbl_retur_penjualan."Discount",
    tbl_retur_penjualan."Status_ppn",
    tbl_retur_penjualan."Keterangan",
    tbl_retur_penjualan."Batal",
    tbl_retur_penjualan."Status_pakai",
    tbl_retur_penjualan.diubah_pada,
    tbl_customer."Nama",
    tbl_retur_penjualan.dibuat_pada,
    tbl_retur_penjualan."Grand_total",
    tbl_retur_penjualan."DPP",
    tbl_retur_penjualan."PPN",
    tbl_customer."Alamat",
    tbl_customer."No_Telpon",
    tbl_penjualan."Nomor" AS "Fj_nomor"
   FROM ((tbl_retur_penjualan
     JOIN tbl_customer ON (((tbl_retur_penjualan."IDCustomer")::text = (tbl_customer."IDCustomer")::text)))
     LEFT JOIN tbl_penjualan ON (((tbl_retur_penjualan."IDFJ")::text = (tbl_penjualan."IDFJ")::text)));

-- ----------------------------
-- View structure for vlistreturpenjualandetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistreturpenjualandetail";
CREATE VIEW "public"."vlistreturpenjualandetail" AS  SELECT tbl_retur_penjualan_detail."IDRPDetail",
    tbl_retur_penjualan_detail."IDRP",
    tbl_retur_penjualan_detail."IDBarang",
    tbl_retur_penjualan_detail."Qty",
    tbl_retur_penjualan_detail."Saldo_qty",
    tbl_retur_penjualan_detail."IDSatuan",
    tbl_retur_penjualan_detail."Harga",
    tbl_retur_penjualan_detail."Sub_total",
    tbl_retur_penjualan."Nomor",
    tbl_barang."Kode_Barang",
    tbl_barang."Nama_Barang",
    tbl_satuan."Satuan",
    tbl_retur_penjualan."Tanggal",
    tbl_customer."Nama",
    tbl_retur_penjualan."IDCustomer"
   FROM ((((tbl_retur_penjualan_detail
     LEFT JOIN tbl_retur_penjualan ON (((tbl_retur_penjualan_detail."IDRP")::text = (tbl_retur_penjualan."IDRP")::text)))
     LEFT JOIN tbl_barang ON (((tbl_retur_penjualan_detail."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_retur_penjualan_detail."IDSatuan")::text = (tbl_satuan."IDSatuan")::text)))
     JOIN tbl_customer ON (((tbl_retur_penjualan."IDCustomer")::text = (tbl_customer."IDCustomer")::text)));

-- ----------------------------
-- View structure for vlistsalesorderdetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistsalesorderdetail";
CREATE VIEW "public"."vlistsalesorderdetail" AS  SELECT tbl_sales_order_detail."IDSOKDetail",
    tbl_sales_order_detail."IDSOK",
    tbl_sales_order_detail."IDBarang",
    tbl_sales_order_detail."Qty",
    tbl_sales_order_detail."Saldo_qty",
    tbl_sales_order_detail."IDSatuan",
    tbl_sales_order_detail."Harga",
    tbl_sales_order_detail."Sub_total",
    tbl_sales_order."Nomor",
    tbl_sales_order."Tanggal",
    tbl_customer."Nama",
    tbl_sales_order."IDCustomer",
    tbl_barang."Nama_Barang",
    tbl_mata_uang."Kode",
    tbl_sales_order."IDMataUang",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Negara",
    tbl_satuan."Kode_Satuan",
    tbl_satuan."Satuan"
   FROM (((((tbl_sales_order_detail
     LEFT JOIN tbl_sales_order ON (((tbl_sales_order_detail."IDSOK")::text = (tbl_sales_order."IDSOK")::text)))
     LEFT JOIN tbl_barang ON (((tbl_sales_order_detail."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_mata_uang ON (((tbl_sales_order."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)))
     LEFT JOIN tbl_customer ON (((tbl_customer."IDCustomer")::text = (tbl_sales_order."IDCustomer")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_sales_order_detail."IDSatuan")::text = (tbl_satuan."IDSatuan")::text)));

-- ----------------------------
-- View structure for vlistsatuankonversi
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistsatuankonversi";
CREATE VIEW "public"."vlistsatuankonversi" AS  SELECT tbl_satuan_konversi."IDSatuanKonversi",
    tbl_satuan_konversi."IDSatuanKecil",
    tbl_satuan_konversi."IDSatuanBesar",
    tbl_satuan_konversi."Qty",
    tbl_satuan_konversi."IDBarang",
    tbl_satuan_konversi.dibuat_pada,
    tbl_satuan_konversi.dibuat_oleh,
    tbl_satuan_konversi.diubah_pada,
    tbl_satuan_konversi.diubah_oleh,
    tbl_barang."Kode_Barang",
    tbl_barang."Nama_Barang",
    tbl_satuan_kecil."Satuan" AS "Satuan_kecil",
    tbl_satuan_besar."Satuan" AS "Satuan_besar",
    tbl_barang."IDSatuan",
    tbl_barang."Aktif",
    tbl_barang."IDKategori",
    tbl_barang."IDUkuran",
    tbl_barang."IDTipe",
    tbl_barang."Berat",
    tbl_barang."StokMinimal"
   FROM (((tbl_satuan_konversi
     LEFT JOIN tbl_barang ON (((tbl_satuan_konversi."IDBarang")::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan tbl_satuan_kecil ON (((tbl_satuan_konversi."IDSatuanKecil")::text = (tbl_satuan_kecil."IDSatuan")::text)))
     LEFT JOIN tbl_satuan tbl_satuan_besar ON (((tbl_satuan_konversi."IDSatuanBesar")::text = (tbl_satuan_besar."IDSatuan")::text)));

-- ----------------------------
-- View structure for vlistsuratjalandetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistsuratjalandetail";
CREATE VIEW "public"."vlistsuratjalandetail" AS  SELECT tbl_surat_jalan_customer.nomor,
    tbl_surat_jalan_customer.idcustomer,
    tbl_customer."Nama",
    tbl_surat_jalan_customer_detail.idbarang,
    tbl_barang."Nama_Barang",
    tbl_surat_jalan_customer_detail.qty,
    tbl_surat_jalan_customer_detail.hargasatuan,
    (tbl_surat_jalan_customer_detail.qty * tbl_surat_jalan_customer_detail.hargasatuan) AS total,
    tbl_surat_jalan_customer.tanggal,
    tbl_surat_jalan_customer_detail.idsjcdetail,
    tbl_surat_jalan_customer_detail.idsjc,
    tbl_surat_jalan_customer_detail.saldo,
    tbl_surat_jalan_customer_detail.idsatuan,
    tbl_surat_jalan_customer_detail.diskon2,
    tbl_surat_jalan_customer_detail.idmatauang,
    tbl_surat_jalan_customer_detail.kurs,
    tbl_surat_jalan_customer_detail.expiredate,
    tbl_surat_jalan_customer_detail.dpp,
    tbl_surat_jalan_customer_detail.statusppn,
    tbl_surat_jalan_customer_detail.nilaippn,
    tbl_surat_jalan_customer_detail.subtotal,
    tbl_surat_jalan_customer_detail.idso,
    tbl_surat_jalan_customer_detail.idsodetail,
    tbl_surat_jalan_customer_detail.asalfaktur,
    tbl_surat_jalan_customer_detail.idgudang,
    tbl_surat_jalan_customer_detail.nomorsj,
    tbl_surat_jalan_customer_detail.nomorso,
    tbl_surat_jalan_customer_detail.created_at,
    tbl_surat_jalan_customer_detail.updated_at,
    tbl_satuan."Satuan"
   FROM ((((tbl_customer
     JOIN tbl_surat_jalan_customer ON (((tbl_surat_jalan_customer.idcustomer)::text = (tbl_customer."IDCustomer")::text)))
     JOIN tbl_surat_jalan_customer_detail ON (((tbl_surat_jalan_customer_detail.idsjc)::text = (tbl_surat_jalan_customer.idsjc)::text)))
     JOIN tbl_barang ON (((tbl_surat_jalan_customer_detail.idbarang)::text = (tbl_barang."IDBarang")::text)))
     LEFT JOIN tbl_satuan ON (((tbl_satuan."IDSatuan")::text = (tbl_surat_jalan_customer_detail.idsatuan)::text)));

-- ----------------------------
-- View structure for vmatauang
-- ----------------------------
DROP VIEW IF EXISTS "public"."vmatauang";
CREATE VIEW "public"."vmatauang" AS  SELECT tbl_idr."IDMU",
    tbl_idr."Nama",
    tbl_idr."Negara"
   FROM tbl_idr
  WHERE ((tbl_idr."Nama")::text <> 'IDR'::text);

-- ----------------------------
-- View structure for vnomorsoretur
-- ----------------------------
DROP VIEW IF EXISTS "public"."vnomorsoretur";
CREATE VIEW "public"."vnomorsoretur" AS  SELECT tbl_sales_order."IDSOK" AS "ID",
    tbl_sales_order."IDCustomer",
    tbl_sales_order."Nomor",
    tbl_sales_order."Batal",
    'SO'::text AS jenis_transaksi
   FROM tbl_sales_order
  WHERE ((tbl_sales_order."Persetujuan" = true) AND (tbl_sales_order."Saldo_qty" > (0)::double precision))
UNION
 SELECT tbl_retur_penjualan."IDRP" AS "ID",
    tbl_retur_penjualan."IDCustomer",
    tbl_retur_penjualan."Nomor",
    tbl_retur_penjualan."Batal",
    'RETUR'::text AS jenis_transaksi
   FROM tbl_retur_penjualan
  WHERE ((tbl_retur_penjualan."Pilihan_retur")::text = 'TM'::text);

-- ----------------------------
-- View structure for vBarangSo
-- ----------------------------
DROP VIEW IF EXISTS "public"."vBarangSo";
CREATE VIEW "public"."vBarangSo" AS  SELECT tbl_barang."IDBarang",
    tbl_barang."IDGroupBarang",
    tbl_barang."Kode_Barang",
    tbl_barang."Nama_Barang",
    tbl_barang."IDSatuan",
    tbl_barang."Aktif",
    tbl_barang."IDKategori",
    tbl_barang."IDUkuran",
    tbl_barang."IDTipe",
    tbl_barang."Berat",
    tbl_barang."StokMinimal",
    tbl_stok."Qty_pcs",
    tbl_stok."Saldo_pcs"
   FROM (tbl_barang
     JOIN tbl_stok ON (((tbl_barang."IDBarang")::text = (tbl_stok."IDBarang")::text)))
  WHERE (tbl_stok."Qty_pcs" > (0)::double precision);

-- ----------------------------
-- View structure for vlistmutasigirodetail
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistmutasigirodetail";
CREATE VIEW "public"."vlistmutasigirodetail" AS  SELECT tbl_mutasi_giro_detail."IDMGDetail",
    tbl_mutasi_giro_detail."IDMG",
    tbl_mutasi_giro_detail."Jenis_giro",
    tbl_mutasi_giro_detail."IDGiro",
    tbl_mutasi_giro_detail."Status",
    tbl_mutasi_giro_detail."Nomor_lama",
    tbl_mutasi_giro_detail."Nomor_baru",
    tbl_customer."Kode_Customer",
    tbl_customer."Nama",
    tbl_coa."Nama_COA" AS "Nama_bank",
    tbl_giro."IDPerusahaan",
    tbl_giro."Jenis_faktur",
    tbl_giro."Nomor_faktur",
    tbl_giro."IDBank",
    tbl_giro."Nomor_giro",
    tbl_giro."Tanggal_faktur",
    tbl_giro."Nilai",
    tbl_giro."Tanggal_giro",
    tbl_giro."Nomor_rekening",
    tbl_mutasi_giro."Tanggal",
    tbl_mutasi_giro."Nomor"
   FROM ((((tbl_mutasi_giro_detail
     LEFT JOIN tbl_giro ON (((tbl_mutasi_giro_detail."IDGiro")::text = (tbl_giro."IDGiro")::text)))
     LEFT JOIN tbl_customer ON (((tbl_giro."IDPerusahaan")::text = (tbl_customer."IDCustomer")::text)))
     LEFT JOIN tbl_coa ON (((tbl_giro."IDBank")::text = (tbl_coa."IDCoa")::text)))
     JOIN tbl_mutasi_giro ON (((tbl_mutasi_giro_detail."IDMG")::text = (tbl_mutasi_giro."IDMG")::text)))
  WHERE ((tbl_giro."Jenis_faktur")::text = 'PP'::text)
UNION ALL
 SELECT tbl_mutasi_giro_detail."IDMGDetail",
    tbl_mutasi_giro_detail."IDMG",
    tbl_mutasi_giro_detail."Jenis_giro",
    tbl_mutasi_giro_detail."IDGiro",
    tbl_mutasi_giro_detail."Status",
    tbl_mutasi_giro_detail."Nomor_lama",
    tbl_mutasi_giro_detail."Nomor_baru",
    tbl_supplier."Kode_Suplier" AS "Kode_Customer",
    tbl_supplier."Nama",
    tbl_coa."Nama_COA" AS "Nama_bank",
    tbl_giro."IDPerusahaan",
    tbl_giro."Jenis_faktur",
    tbl_giro."Nomor_faktur",
    tbl_giro."IDBank",
    tbl_giro."Nomor_giro",
    tbl_giro."Tanggal_faktur",
    tbl_giro."Nilai",
    tbl_giro."Tanggal_giro",
    tbl_giro."Nomor_rekening",
    tbl_mutasi_giro."Tanggal",
    tbl_mutasi_giro."Nomor"
   FROM ((((tbl_mutasi_giro_detail
     LEFT JOIN tbl_giro ON ((((tbl_mutasi_giro_detail."IDGiro")::text = (tbl_giro."IDGiro")::text) AND ((tbl_giro."IDGiro")::text = (tbl_mutasi_giro_detail."IDGiro")::text) AND ((tbl_mutasi_giro_detail."IDGiro")::text = (tbl_giro."IDGiro")::text) AND ((tbl_mutasi_giro_detail."IDGiro")::text = (tbl_giro."IDGiro")::text) AND ((tbl_mutasi_giro_detail."IDGiro")::text = (tbl_giro."IDGiro")::text))))
     LEFT JOIN tbl_coa ON (((tbl_giro."IDBank")::text = (tbl_coa."IDCoa")::text)))
     JOIN tbl_mutasi_giro ON ((((tbl_mutasi_giro_detail."IDMG")::text = (tbl_mutasi_giro."IDMG")::text) AND ((tbl_mutasi_giro_detail."IDMG")::text = (tbl_mutasi_giro."IDMG")::text))))
     JOIN tbl_supplier ON (((tbl_giro."IDPerusahaan")::text = (tbl_supplier."IDSupplier")::text)))
  WHERE ((tbl_giro."Jenis_faktur")::text = 'PH'::text);

-- ----------------------------
-- View structure for vlistsuratjalan
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistsuratjalan";
CREATE VIEW "public"."vlistsuratjalan" AS  SELECT tbl_customer."Nama",
    tbl_customer."IDCustomer",
    tbl_surat_jalan_customer.idsjc,
    tbl_surat_jalan_customer.nomor,
    tbl_surat_jalan_customer.tanggal,
    tbl_surat_jalan_customer.idcustomer,
    tbl_surat_jalan_customer.idso,
    tbl_surat_jalan_customer.asalfaktur,
    tbl_surat_jalan_customer.nomorcustomer,
    tbl_surat_jalan_customer.idgudang,
    tbl_surat_jalan_customer.idmatauang,
    tbl_surat_jalan_customer.kurs,
    tbl_surat_jalan_customer.qty,
    tbl_surat_jalan_customer.saldoqty,
    tbl_surat_jalan_customer.top,
    tbl_surat_jalan_customer.jatuhtempo,
    tbl_surat_jalan_customer.disc,
    tbl_surat_jalan_customer.persendisc,
    tbl_surat_jalan_customer.ppn,
    tbl_surat_jalan_customer.statusppn,
    tbl_surat_jalan_customer.grantotal,
    tbl_surat_jalan_customer.sisa,
    tbl_surat_jalan_customer.saldosisa,
    tbl_surat_jalan_customer.kelebihan,
    tbl_surat_jalan_customer.saldokelebihan,
    tbl_surat_jalan_customer.keterangan,
    tbl_surat_jalan_customer.jenisso,
    tbl_surat_jalan_customer.dpp,
    tbl_surat_jalan_customer.nilaippn,
    tbl_surat_jalan_customer.iduser,
    tbl_surat_jalan_customer.created_at,
    tbl_surat_jalan_customer.updated_at,
    tbl_surat_jalan_customer."Batal",
    tbl_sales_order."Nomor" AS nomorso,
    tbl_customer."Alamat",
    tbl_customer."No_Telpon",
    tbl_surat_jalan_customer.confirm,
    tbl_customer."NPWP"
   FROM ((tbl_surat_jalan_customer
     LEFT JOIN tbl_sales_order ON (((tbl_surat_jalan_customer.idso)::text = (tbl_sales_order."IDSOK")::text)))
     LEFT JOIN tbl_customer ON (((tbl_surat_jalan_customer.idcustomer)::text = (tbl_customer."IDCustomer")::text)));

-- ----------------------------
-- View structure for v_returpembelian
-- ----------------------------
DROP VIEW IF EXISTS "public"."v_returpembelian";
CREATE VIEW "public"."v_returpembelian" AS  SELECT tbl_retur_pembelian."IDRB",
    tbl_retur_pembelian."Tanggal",
    tbl_retur_pembelian."Nomor",
    tbl_retur_pembelian."IDFB",
    tbl_retur_pembelian."IDSupplier",
    tbl_retur_pembelian."Tanggal_fb",
    ( SELECT sum(tbl_retur_pembelian_detail_1."Qty_yard") AS total
           FROM tbl_retur_pembelian_detail tbl_retur_pembelian_detail_1
          WHERE ((tbl_retur_pembelian_detail_1."IDRB")::text = (tbl_retur_pembelian."IDRB")::text)) AS "Total_qty_yard",
    tbl_retur_pembelian."Total_qty_meter",
    tbl_retur_pembelian."Saldo_yard",
    tbl_retur_pembelian."Saldo_meter",
    tbl_retur_pembelian."Discount",
    tbl_retur_pembelian."Status_ppn",
    tbl_retur_pembelian."Keterangan",
    tbl_retur_pembelian."Batal",
    tbl_retur_pembelian."Status_pakai",
    tbl_supplier."Nama",
    tbl_pembelian."Nomor" AS "Nomor_fb",
    tbl_barang."Nama_Barang",
    tbl_retur_pembelian_detail."Harga",
    tbl_retur_pembelian_detail."Subtotal",
    tbl_retur_pembelian_detail."Qty_yard"
   FROM ((((tbl_retur_pembelian
     LEFT JOIN tbl_supplier ON (((tbl_retur_pembelian."IDSupplier")::text = (tbl_supplier."IDSupplier")::text)))
     LEFT JOIN tbl_pembelian ON (((tbl_retur_pembelian."IDFB")::text = (tbl_pembelian."IDFB")::text)))
     JOIN tbl_retur_pembelian_detail ON (((tbl_retur_pembelian_detail."IDRB")::text = (tbl_retur_pembelian."IDRB")::text)))
     JOIN tbl_barang ON (((tbl_barang."IDBarang")::text = (tbl_retur_pembelian_detail."IDBarang")::text)));

-- ----------------------------
-- View structure for vlistpenjualan
-- ----------------------------
DROP VIEW IF EXISTS "public"."vlistpenjualan";
CREATE VIEW "public"."vlistpenjualan" AS  SELECT tbl_penjualan."IDFJ",
    tbl_penjualan."Tanggal",
    tbl_penjualan."Nomor",
    tbl_penjualan."IDCustomer",
    tbl_penjualan."IDSJC",
    tbl_penjualan."TOP",
    tbl_penjualan."Tanggal_jatuh_tempo",
    tbl_penjualan."Kurs",
    tbl_penjualan."Total_qty",
    tbl_penjualan."Discount",
    tbl_penjualan."Status_ppn",
    tbl_penjualan."Keterangan",
    tbl_penjualan."Batal",
    tbl_penjualan."Grand_total",
    tbl_penjualan."Status_pakai",
    tbl_penjualan."DPP",
    tbl_penjualan."PPN",
    tbl_penjualan.dibuat_pada,
    tbl_penjualan.diubah_pada,
    tbl_customer."Nama",
    tbl_penjualan."Nama_di_faktur",
    tbl_penjualan."IDMataUang",
    tbl_mata_uang."Kode",
    tbl_mata_uang."Mata_uang",
    tbl_mata_uang."Negara",
    tbl_customer."Alamat",
    tbl_customer."No_Telpon",
    tbl_penjualan.no_pajak,
    tbl_penjualan.jenis_ppn
   FROM ((tbl_penjualan
     JOIN tbl_customer ON (((tbl_penjualan."IDCustomer")::text = (tbl_customer."IDCustomer")::text)))
     JOIN tbl_mata_uang ON (((tbl_penjualan."IDMataUang")::text = (tbl_mata_uang."IDMataUang")::text)));

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."migrations_id_seq"
OWNED BY "public"."migrations"."id";
SELECT setval('"public"."migrations_id_seq"', 6, true);
ALTER SEQUENCE "public"."tbl_agen_IDAgen_seq"
OWNED BY "public"."tbl_agen"."IDAgen";
SELECT setval('"public"."tbl_agen_IDAgen_seq"', 34, true);
ALTER SEQUENCE "public"."tbl_asset_IDAsset_seq"
OWNED BY "public"."tbl_asset"."IDAsset";
SELECT setval('"public"."tbl_asset_IDAsset_seq"', 39, true);
ALTER SEQUENCE "public"."tbl_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_asset_IDGroupAsset_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_bank_IDBank_seq"
OWNED BY "public"."tbl_bank"."IDBank";
SELECT setval('"public"."tbl_bank_IDBank_seq"', 49, true);
ALTER SEQUENCE "public"."tbl_bank_IDCoa_seq"
OWNED BY "public"."tbl_bank"."IDCoa";
SELECT setval('"public"."tbl_bank_IDCoa_seq"', 20, true);
ALTER SEQUENCE "public"."tbl_barcode_print_IDBarcodePrint_seq"
OWNED BY "public"."tbl_barcode_print"."IDBarcodePrint";
SELECT setval('"public"."tbl_barcode_print_IDBarcodePrint_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_booking_order_IDBO_seq"
OWNED BY "public"."tbl_booking_order"."IDBO";
SELECT setval('"public"."tbl_booking_order_IDBO_seq"', 32, true);
ALTER SEQUENCE "public"."tbl_booking_order_IDCorak_seq"
OWNED BY "public"."tbl_booking_order"."IDCorak";
SELECT setval('"public"."tbl_booking_order_IDCorak_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_booking_order_IDMataUang_seq"
OWNED BY "public"."tbl_booking_order"."IDMataUang";
SELECT setval('"public"."tbl_booking_order_IDMataUang_seq"', 17, false);
SELECT setval('"public"."tbl_coa_IDCoa_seq"', 34, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCOASaldoAwal";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCOASaldoAwal_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDCoa_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDCoa";
SELECT setval('"public"."tbl_coa_saldo_awal_IDCoa_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_coa_saldo_awal_IDGroupCOA_seq"
OWNED BY "public"."tbl_coa_saldo_awal"."IDGroupCOA";
SELECT setval('"public"."tbl_coa_saldo_awal_IDGroupCOA_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_customer_IDCustomer_seq"
OWNED BY "public"."tbl_customer"."IDCustomer";
SELECT setval('"public"."tbl_customer_IDCustomer_seq"', 24, true);
ALTER SEQUENCE "public"."tbl_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_customer_IDGroupCustomer_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_giro_IDFaktur_seq"
OWNED BY "public"."tbl_giro"."IDFaktur";
SELECT setval('"public"."tbl_giro_IDFaktur_seq"', 32, true);
ALTER SEQUENCE "public"."tbl_giro_IDGiro_seq"
OWNED BY "public"."tbl_giro"."IDGiro";
SELECT setval('"public"."tbl_giro_IDGiro_seq"', 32, true);
ALTER SEQUENCE "public"."tbl_giro_IDPerusahaan_seq"
OWNED BY "public"."tbl_giro"."IDPerusahaan";
SELECT setval('"public"."tbl_giro_IDPerusahaan_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_group_asset_IDGroupAsset_seq"
OWNED BY "public"."tbl_group_asset"."IDGroupAsset";
SELECT setval('"public"."tbl_group_asset_IDGroupAsset_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_group_barang_IDGroupBarang_seq"
OWNED BY "public"."tbl_group_barang"."IDGroupBarang";
SELECT setval('"public"."tbl_group_barang_IDGroupBarang_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_group_coa_IDGroupCOA_seq"
OWNED BY "public"."tbl_group_coa"."IDGroupCOA";
SELECT setval('"public"."tbl_group_coa_IDGroupCOA_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_group_customer_IDGroupCustomer_seq"
OWNED BY "public"."tbl_group_customer"."IDGroupCustomer";
SELECT setval('"public"."tbl_group_customer_IDGroupCustomer_seq"', 30, true);
ALTER SEQUENCE "public"."tbl_group_supplier_IDGroupSupplier_seq"
OWNED BY "public"."tbl_group_supplier"."IDGroupSupplier";
SELECT setval('"public"."tbl_group_supplier_IDGroupSupplier_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_group_user_IDGroupUser_seq"
OWNED BY "public"."tbl_group_user"."IDGroupUser";
SELECT setval('"public"."tbl_group_user_IDGroupUser_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_gudang_IDGudang_seq"
OWNED BY "public"."tbl_gudang"."IDGudang";
SELECT setval('"public"."tbl_gudang_IDGudang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDBarang_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDBarang";
SELECT setval('"public"."tbl_harga_jual_barang_IDBarang_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDGroupCustomer_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDGroupCustomer";
SELECT setval('"public"."tbl_harga_jual_barang_IDGroupCustomer_seq"', 87, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDHargaJual_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDHargaJual";
SELECT setval('"public"."tbl_harga_jual_barang_IDHargaJual_seq"', 54, true);
ALTER SEQUENCE "public"."tbl_harga_jual_barang_IDSatuan_seq"
OWNED BY "public"."tbl_harga_jual_barang"."IDSatuan";
SELECT setval('"public"."tbl_harga_jual_barang_IDSatuan_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_hutang_IDFaktur_seq"
OWNED BY "public"."tbl_hutang"."IDFaktur";
SELECT setval('"public"."tbl_hutang_IDFaktur_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_hutang_IDHutang_seq"
OWNED BY "public"."tbl_hutang"."IDHutang";
SELECT setval('"public"."tbl_hutang_IDHutang_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_hutang_IDSupplier_seq"
OWNED BY "public"."tbl_hutang"."IDSupplier";
SELECT setval('"public"."tbl_hutang_IDSupplier_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_in_IDBarang_seq"
OWNED BY "public"."tbl_in"."IDBarang";
SELECT setval('"public"."tbl_in_IDBarang_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_in_IDCorak_seq"
OWNED BY "public"."tbl_in"."IDCorak";
SELECT setval('"public"."tbl_in_IDCorak_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_in_IDIn_seq"
OWNED BY "public"."tbl_in"."IDIn";
SELECT setval('"public"."tbl_in_IDIn_seq"', 86, true);
ALTER SEQUENCE "public"."tbl_in_IDMerk_seq"
OWNED BY "public"."tbl_in"."IDMerk";
SELECT setval('"public"."tbl_in_IDMerk_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_in_IDSatuan_seq"
OWNED BY "public"."tbl_in"."IDSatuan";
SELECT setval('"public"."tbl_in_IDSatuan_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_in_IDWarna_seq"
OWNED BY "public"."tbl_in"."IDWarna";
SELECT setval('"public"."tbl_in_IDWarna_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDIP_seq"', 62, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDPO_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDPO";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDPO_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_IDSupplier_seq"
OWNED BY "public"."tbl_instruksi_pengiriman"."IDSupplier";
SELECT setval('"public"."tbl_instruksi_pengiriman_IDSupplier_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDBarang";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDBarang_seq"', 22, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDCorak";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDCorak_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIPDetail";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIPDetail_seq"', 102, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDIP_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDIP";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDIP_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDMerk";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDMerk_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDSatuan";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDSatuan_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"
OWNED BY "public"."tbl_instruksi_pengiriman_detail"."IDWarna";
SELECT setval('"public"."tbl_instruksi_pengiriman_detail_IDWarna_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDBarang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDBarang";
SELECT setval('"public"."tbl_kartu_stok_IDBarang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFakturDetail_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFakturDetail";
SELECT setval('"public"."tbl_kartu_stok_IDFakturDetail_seq"', 309, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDFaktur_seq"
OWNED BY "public"."tbl_kartu_stok"."IDFaktur";
SELECT setval('"public"."tbl_kartu_stok_IDFaktur_seq"', 309, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDGudang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDGudang";
SELECT setval('"public"."tbl_kartu_stok_IDGudang_seq"', 752, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDKartuStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDKartuStok";
SELECT setval('"public"."tbl_kartu_stok_IDKartuStok_seq"', 216, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDMataUang_seq"
OWNED BY "public"."tbl_kartu_stok"."IDMataUang";
SELECT setval('"public"."tbl_kartu_stok_IDMataUang_seq"', 751, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDSatuan_seq"
OWNED BY "public"."tbl_kartu_stok"."IDSatuan";
SELECT setval('"public"."tbl_kartu_stok_IDSatuan_seq"', 134, true);
ALTER SEQUENCE "public"."tbl_kartu_stok_IDStok_seq"
OWNED BY "public"."tbl_kartu_stok"."IDStok";
SELECT setval('"public"."tbl_kartu_stok_IDStok_seq"', 310, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDKonversi_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDKonversi";
SELECT setval('"public"."tbl_konversi_satuan_IDKonversi_seq"', 28, true);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanBerat_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanBerat";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanBerat_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_konversi_satuan_IDSatuanKecil_seq"
OWNED BY "public"."tbl_konversi_satuan"."IDSatuanKecil";
SELECT setval('"public"."tbl_konversi_satuan_IDSatuanKecil_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_IDKP_seq"', 68, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_IDMataUang_seq"', 15, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDBarang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDBarang_seq"', 26, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKPDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKPDetail_seq"', 86, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDKP_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDKP";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDKP_seq"', 21, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDMataUang_seq"', 20, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_koreksi_persediaan_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_detail_IDSatuan_seq"', 26, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"
OWNED BY "public"."tbl_pindah_gudang"."IDPG";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_IDKPScan_seq"', 41, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"
OWNED BY "public"."tbl_pindah_gudang_detail"."IDBarang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDBarang_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"
OWNED BY "public"."tbl_pindah_gudang_detail"."IDPGDetail";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKPScanDetail_seq"', 45, true);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"
OWNED BY "public"."tbl_pindah_gudang_detail"."IDPG";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDKP_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"
OWNED BY "public"."tbl_pindah_gudang_detail"."IDMataUang";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDMataUang_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"
OWNED BY "public"."tbl_pindah_gudang_detail"."IDSatuan";
SELECT setval('"public"."tbl_koreksi_persediaan_scan_detail_IDSatuan_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_kota_IDKota_seq"
OWNED BY "public"."tbl_kota"."IDKota";
SELECT setval('"public"."tbl_kota_IDKota_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDBarang_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDBarang";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDBarang_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDCorak_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDCorak";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDCorak_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDLapPers_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDLapPers";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDLapPers_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDMerk_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDMerk";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDMerk_seq"', 15, false);
ALTER SEQUENCE "public"."tbl_lap_umur_persediaan_IDWarna_seq"
OWNED BY "public"."tbl_lap_umur_persediaan"."IDWarna";
SELECT setval('"public"."tbl_lap_umur_persediaan_IDWarna_seq"', 15, false);
SELECT setval('"public"."tbl_loguser_id_log_seq"', 1862, true);
ALTER SEQUENCE "public"."tbl_mata_uang_IDMataUang_seq"
OWNED BY "public"."tbl_mata_uang"."IDMataUang";
SELECT setval('"public"."tbl_mata_uang_IDMataUang_seq"', 32, true);
ALTER SEQUENCE "public"."tbl_menu_IDMenu_seq"
OWNED BY "public"."tbl_menu"."IDMenu";
SELECT setval('"public"."tbl_menu_IDMenu_seq"', 37, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_detail_IDMenuDetail_seq"', 129, true);
ALTER SEQUENCE "public"."tbl_menu_detail_IDMenu_seq"
OWNED BY "public"."tbl_menu_detail"."IDMenu";
SELECT setval('"public"."tbl_menu_detail_IDMenu_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_menu_role_IDGroupUser_seq"
OWNED BY "public"."tbl_menu_role"."IDGroupUser";
SELECT setval('"public"."tbl_menu_role_IDGroupUser_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuDetail_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuDetail";
SELECT setval('"public"."tbl_menu_role_IDMenuDetail_seq"', 30, true);
ALTER SEQUENCE "public"."tbl_menu_role_IDMenuRole_seq"
OWNED BY "public"."tbl_menu_role"."IDMenuRole";
SELECT setval('"public"."tbl_menu_role_IDMenuRole_seq"', 4332, true);
ALTER SEQUENCE "public"."tbl_out_IDOut_seq"
OWNED BY "public"."tbl_out"."IDOut";
SELECT setval('"public"."tbl_out_IDOut_seq"', 42, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDCOA_seq"
OWNED BY "public"."tbl_pembayaran"."IDCOA";
SELECT setval('"public"."tbl_pembayaran_IDCOA_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFBPembayaran_seq"
OWNED BY "public"."tbl_pembayaran"."IDFBPembayaran";
SELECT setval('"public"."tbl_pembayaran_IDFBPembayaran_seq"', 40, true);
ALTER SEQUENCE "public"."tbl_pembayaran_IDFB_seq"
OWNED BY "public"."tbl_pembayaran"."IDFB";
SELECT setval('"public"."tbl_pembayaran_IDFB_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_pembayaran_IDMataUang_seq"
OWNED BY "public"."tbl_pembayaran"."IDMataUang";
SELECT setval('"public"."tbl_pembayaran_IDMataUang_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDFB_seq"
OWNED BY "public"."tbl_pembelian"."IDFB";
SELECT setval('"public"."tbl_pembelian_IDFB_seq"', 119, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDMataUang_seq"
OWNED BY "public"."tbl_pembelian"."IDMataUang";
SELECT setval('"public"."tbl_pembelian_IDMataUang_seq"', 26, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDSupplier_seq"
OWNED BY "public"."tbl_pembelian"."IDSupplier";
SELECT setval('"public"."tbl_pembelian_IDSupplier_seq"', 18, true);
ALTER SEQUENCE "public"."tbl_pembelian_IDTBS_seq"
OWNED BY "public"."tbl_pembelian"."IDTBS";
SELECT setval('"public"."tbl_pembelian_IDTBS_seq"', 19, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_IDFBA_seq"', 100, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDAsset_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBADetail_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBADetail";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBADetail_seq"', 190, true);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDFBA_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDFBA";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDFBA_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"
OWNED BY "public"."tbl_pembelian_asset_detail"."IDGroupAsset";
SELECT setval('"public"."tbl_pembelian_asset_detail_IDGroupAsset_seq"', 17, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDBarang_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDBarang";
SELECT setval('"public"."tbl_pembelian_detail_IDBarang_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDCorak_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDCorak";
SELECT setval('"public"."tbl_pembelian_detail_IDCorak_seq"', 364, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFBDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFBDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDFBDetail_seq"', 143, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDFB_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDFB";
SELECT setval('"public"."tbl_pembelian_detail_IDFB_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDMerk_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDMerk";
SELECT setval('"public"."tbl_pembelian_detail_IDMerk_seq"', 369, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDSatuan_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDSatuan";
SELECT setval('"public"."tbl_pembelian_detail_IDSatuan_seq"', 347, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDTBSDetail_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDTBSDetail";
SELECT setval('"public"."tbl_pembelian_detail_IDTBSDetail_seq"', 48, true);
ALTER SEQUENCE "public"."tbl_pembelian_detail_IDWarna_seq"
OWNED BY "public"."tbl_pembelian_detail"."IDWarna";
SELECT setval('"public"."tbl_pembelian_detail_IDWarna_seq"', 369, true);
ALTER SEQUENCE "public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"
OWNED BY "public"."tbl_pembelian_grand_total"."IDFBGrandTotal";
SELECT setval('"public"."tbl_pembelian_grand_total_IDFBGrandTotal_seq"', 58, true);
ALTER SEQUENCE "public"."tbl_pembelian_grand_total_IDFB_seq"
OWNED BY "public"."tbl_pembelian_grand_total"."IDFB";
SELECT setval('"public"."tbl_pembelian_grand_total_IDFB_seq"', 23, true);
ALTER SEQUENCE "public"."tbl_piutang_IDCustomer_seq"
OWNED BY "public"."tbl_piutang"."IDCustomer";
SELECT setval('"public"."tbl_piutang_IDCustomer_seq"', 17, false);
ALTER SEQUENCE "public"."tbl_piutang_IDFaktur_seq"
OWNED BY "public"."tbl_piutang"."IDFaktur";
SELECT setval('"public"."tbl_piutang_IDFaktur_seq"', 27, true);
ALTER SEQUENCE "public"."tbl_piutang_IDPiutang_seq"
OWNED BY "public"."tbl_piutang"."IDPiutang";
SELECT setval('"public"."tbl_piutang_IDPiutang_seq"', 27, true);
SELECT setval('"public"."tbl_posting_id_seq"', 11, true);
SELECT setval('"public"."tbl_purchase_order_IDAgen_seq"', 63, true);
SELECT setval('"public"."tbl_purchase_order_IDBarang_seq"', 181, true);
SELECT setval('"public"."tbl_purchase_order_IDCorak_seq"', 63, true);
SELECT setval('"public"."tbl_purchase_order_IDMataUang_seq"', 16, false);
SELECT setval('"public"."tbl_purchase_order_IDMerk_seq"', 63, true);
SELECT setval('"public"."tbl_purchase_order_IDPO_seq"', 93, true);
SELECT setval('"public"."tbl_purchase_order_IDSupplier_seq"', 16, false);
SELECT setval('"public"."tbl_purchase_order_detail_IDPODetail_seq"', 97, true);
SELECT setval('"public"."tbl_purchase_order_detail_IDPO_seq"', 16, false);
SELECT setval('"public"."tbl_purchase_order_detail_IDWarna_seq"', 16, false);
SELECT setval('"public"."tbl_retur_pembelian_IDFB_seq"', 18, false);
SELECT setval('"public"."tbl_retur_pembelian_IDRB_seq"', 36, true);
SELECT setval('"public"."tbl_retur_pembelian_IDSupplier_seq"', 18, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDBarang_seq"', 18, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDCorak_seq"', 189, true);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDMerk_seq"', 189, true);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRBDetail_seq"', 51, true);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDRB_seq"', 18, false);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDSatuan_seq"', 189, true);
SELECT setval('"public"."tbl_retur_pembelian_detail_IDWarna_seq"', 189, true);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDMataUang_seq"', 18, false);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRBGrandTotal_seq"', 34, true);
SELECT setval('"public"."tbl_retur_pembelian_grand_total_IDRB_seq"', 18, false);
SELECT setval('"public"."tbl_saldo_awal_asset_IDAsset_seq"', 144, true);
SELECT setval('"public"."tbl_saldo_awal_asset_IDSaldoAwalAsset_seq"', 156, true);
SELECT setval('"public"."tbl_satuan_IDSatuan_seq"', 25, true);
SELECT setval('"public"."tbl_stok_IDBarang_seq"', 30, true);
SELECT setval('"public"."tbl_stok_IDCorak_seq"', 142, true);
SELECT setval('"public"."tbl_stok_IDFakturDetail_seq"', 157, true);
SELECT setval('"public"."tbl_stok_IDFaktur_seq"', 157, true);
SELECT setval('"public"."tbl_stok_IDGudang_seq"', 39, true);
SELECT setval('"public"."tbl_stok_IDMataUang_seq"', 152, true);
SELECT setval('"public"."tbl_stok_IDSatuan_seq"', 42, true);
SELECT setval('"public"."tbl_stok_IDStok_seq"', 182, true);
SELECT setval('"public"."tbl_stok_IDWarna_seq"', 142, true);
SELECT setval('"public"."tbl_stok_history_AsalIDFakturDetail_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_AsalIDFaktur_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDBarang_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDCorak_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDFakturDetail_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDFaktur_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDGudang_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDHistoryStok_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDMataUang_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDSatuan_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDStok_seq"', 16, false);
SELECT setval('"public"."tbl_stok_history_IDWarna_seq"', 16, false);
SELECT setval('"public"."tbl_stok_opname_IDBarang_seq"', 16, true);
SELECT setval('"public"."tbl_stok_opname_IDCorak_seq"', 17, true);
SELECT setval('"public"."tbl_stok_opname_IDGudang_seq"', 18, true);
SELECT setval('"public"."tbl_stok_opname_IDSatuan_seq"', 20, true);
SELECT setval('"public"."tbl_stok_opname_IDStokOpname_seq"', 23, true);
SELECT setval('"public"."tbl_stok_opname_IDWarna_seq"', 17, true);
SELECT setval('"public"."tbl_suplier_IDGroupSupplier_seq"', 18, false);
SELECT setval('"public"."tbl_suplier_IDSupplier_seq"', 35, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDPO_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSJM_seq"', 30, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_IDSupplier_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDBarang_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDCorak_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDMerk_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJMDetail_seq"', 33, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSJM_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDSatuan_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_celup_detail_IDWarna_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSJH_seq"', 31, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_IDSupplier_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDBarang_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDCorak_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDMerk_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJHDetail_seq"', 34, true);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSJH_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDSatuan_seq"', 18, false);
SELECT setval('"public"."tbl_surat_jalan_makloon_jahit_detail_IDWarna_seq"', 18, false);
SELECT setval('"public"."tbl_tampungan_IDT_seq"', 552, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDPO_seq"', 62, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDSupplier_seq"', 52, true);
SELECT setval('"public"."tbl_terima_barang_supplier_IDTBS_seq"', 111, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDBarang_seq"', 23, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDCorak_seq"', 417, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDMerk_seq"', 428, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDSatuan_seq"', 42, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBSDetail_seq"', 271, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDTBS_seq"', 19, true);
SELECT setval('"public"."tbl_terima_barang_supplier_detail_IDWarna_seq"', 427, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSJH_seq"', 18, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDSupplier_seq"', 18, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_IDTBSH_seq"', 50, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDBarang_seq"', 18, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDMerk_seq"', 18, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDSatuan_seq"', 18, false);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSHDetail_seq"', 56, true);
SELECT setval('"public"."tbl_terima_barang_supplier_jahit_detail_IDTBSH_seq"', 18, false);
SELECT setval('"public"."tbl_um_customer_IDCustomer_seq"', 20, true);
SELECT setval('"public"."tbl_um_customer_IDFaktur_seq"', 58, true);
SELECT setval('"public"."tbl_um_customer_IDUMCustomer_seq"', 24, true);
SELECT setval('"public"."tbl_user_IDGroupUser_seq"', 18, false);
SELECT setval('"public"."tbl_user_IDUser_seq"', 33, true);
SELECT setval('"public"."tbl_warna_IDWarna_seq"', 26, true);

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table password_resets
-- ----------------------------
CREATE INDEX "password_resets_email_index" ON "public"."password_resets" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table tbl_bulan
-- ----------------------------
ALTER TABLE "public"."tbl_bulan" ADD CONSTRAINT "tbl_bulan_pkey" PRIMARY KEY ("IDBulan");

-- ----------------------------
-- Primary Key structure for table tbl_cara_bayar
-- ----------------------------
ALTER TABLE "public"."tbl_cara_bayar" ADD CONSTRAINT "tbl_cara_bayar_pkey" PRIMARY KEY ("IDCaraBayar");

-- ----------------------------
-- Primary Key structure for table tbl_controlpanel
-- ----------------------------
ALTER TABLE "public"."tbl_controlpanel" ADD CONSTRAINT "tbl_controlpanel_pkey" PRIMARY KEY ("IDCP");

-- ----------------------------
-- Primary Key structure for table tbl_font
-- ----------------------------
ALTER TABLE "public"."tbl_font" ADD CONSTRAINT "tbl_font_pkey" PRIMARY KEY ("IDFont");

-- ----------------------------
-- Primary Key structure for table tbl_fontsize
-- ----------------------------
ALTER TABLE "public"."tbl_fontsize" ADD CONSTRAINT "tbl_fontsize_pkey" PRIMARY KEY ("IDFontSize");

-- ----------------------------
-- Primary Key structure for table tbl_fontstyle
-- ----------------------------
ALTER TABLE "public"."tbl_fontstyle" ADD CONSTRAINT "tbl_fontstyle_pkey" PRIMARY KEY ("IDStyle");

-- ----------------------------
-- Primary Key structure for table tbl_formula
-- ----------------------------
ALTER TABLE "public"."tbl_formula" ADD CONSTRAINT "tbl_formula_pkey" PRIMARY KEY ("IDFormula");

-- ----------------------------
-- Primary Key structure for table tbl_idr
-- ----------------------------
ALTER TABLE "public"."tbl_idr" ADD CONSTRAINT "tbl_idr_pkey" PRIMARY KEY ("IDMU");

-- ----------------------------
-- Primary Key structure for table tbl_kategori
-- ----------------------------
ALTER TABLE "public"."tbl_kategori" ADD CONSTRAINT "tbl_kategori_pkey" PRIMARY KEY ("IDKategori");

-- ----------------------------
-- Primary Key structure for table tbl_kode_transaksi
-- ----------------------------
ALTER TABLE "public"."tbl_kode_transaksi" ADD CONSTRAINT "tbl_kode_transaksi_pkey" PRIMARY KEY ("IDKode");

-- ----------------------------
-- Primary Key structure for table tbl_kode_transaksi_detail
-- ----------------------------
ALTER TABLE "public"."tbl_kode_transaksi_detail" ADD CONSTRAINT "tbl_kode_transaksi_detail_pkey" PRIMARY KEY ("IDKodeDetail");

-- ----------------------------
-- Primary Key structure for table tbl_pemakaian_barang
-- ----------------------------
ALTER TABLE "public"."tbl_pemakaian_barang" ADD CONSTRAINT "tbl_pemakaian_barang_pkey" PRIMARY KEY ("IDPakai");

-- ----------------------------
-- Primary Key structure for table tbl_penerimaan_barang_jadi
-- ----------------------------
ALTER TABLE "public"."tbl_penerimaan_barang_jadi" ADD CONSTRAINT "tbl_penerimaan_barang_jadi_pkey" PRIMARY KEY ("IDPBJ");

-- ----------------------------
-- Primary Key structure for table tbl_penyusutan
-- ----------------------------
ALTER TABLE "public"."tbl_penyusutan" ADD CONSTRAINT "tbl_penyusutan_pkey" PRIMARY KEY ("IDPenyusutan");

-- ----------------------------
-- Primary Key structure for table tbl_perusahaan
-- ----------------------------
ALTER TABLE "public"."tbl_perusahaan" ADD CONSTRAINT "tbl_perusahaan_pkey" PRIMARY KEY ("IDPerusahaan");

-- ----------------------------
-- Primary Key structure for table tbl_po_asset
-- ----------------------------
ALTER TABLE "public"."tbl_po_asset" ADD CONSTRAINT "tbl_po_asset_pkey" PRIMARY KEY ("IDPOAsset");

-- ----------------------------
-- Primary Key structure for table tbl_produksibahanbaku
-- ----------------------------
ALTER TABLE "public"."tbl_produksibahanbaku" ADD CONSTRAINT "tbl_produksibahanbaku_pkey" PRIMARY KEY ("IDPBB");

-- ----------------------------
-- Primary Key structure for table tbl_produksibahanbaku_detail
-- ----------------------------
ALTER TABLE "public"."tbl_produksibahanbaku_detail" ADD CONSTRAINT "tbl_produksibahanbakudetail_pkey" PRIMARY KEY ("IDPBBDetail");

-- ----------------------------
-- Primary Key structure for table tbl_retur_pembelian_umum_detail
-- ----------------------------
ALTER TABLE "public"."tbl_retur_pembelian_umum_detail" ADD CONSTRAINT "tbl_retur_pembelian_umum_detail_pkey" PRIMARY KEY ("IDRBUmumDetail");

-- ----------------------------
-- Primary Key structure for table tbl_retur_pembelian_umum_grand_total
-- ----------------------------
ALTER TABLE "public"."tbl_retur_pembelian_umum_grand_total" ADD CONSTRAINT "tbl_retur_pembelian_umum_grand_total_pkey" PRIMARY KEY ("IDRBUmumGrandTotal");

-- ----------------------------
-- Primary Key structure for table tbl_sales_order_detail
-- ----------------------------
ALTER TABLE "public"."tbl_sales_order_detail" ADD CONSTRAINT "tbl_sales_order_detail_pkey" PRIMARY KEY ("IDSOKDetail");

-- ----------------------------
-- Primary Key structure for table tbl_satuan_konversi
-- ----------------------------
ALTER TABLE "public"."tbl_satuan_konversi" ADD CONSTRAINT "tbl_satuan_konversi_pkey" PRIMARY KEY ("IDSatuanKonversi");

-- ----------------------------
-- Primary Key structure for table tbl_setting_labarugi
-- ----------------------------
ALTER TABLE "public"."tbl_setting_labarugi" ADD CONSTRAINT "tbl_setting_labarugi_pkey" PRIMARY KEY ("IDLabaRugi");

-- ----------------------------
-- Primary Key structure for table tbl_setting_neraca
-- ----------------------------
ALTER TABLE "public"."tbl_setting_neraca" ADD CONSTRAINT "tbl_setting_neraca_pkey" PRIMARY KEY ("IDNeraca");

-- ----------------------------
-- Primary Key structure for table tbl_settingcoa
-- ----------------------------
ALTER TABLE "public"."tbl_settingcoa" ADD CONSTRAINT "tbl_settingcoa_pkey" PRIMARY KEY ("IDSETCOA");

-- ----------------------------
-- Primary Key structure for table tbl_tipe_barang
-- ----------------------------
ALTER TABLE "public"."tbl_tipe_barang" ADD CONSTRAINT "tbl_tipe_barang_pkey" PRIMARY KEY ("IDTipe");

-- ----------------------------
-- Primary Key structure for table tbl_ukuran
-- ----------------------------
ALTER TABLE "public"."tbl_ukuran" ADD CONSTRAINT "tbl_kategori_copy1_pkey" PRIMARY KEY ("IDUkuran");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_email_unique" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table users_akses
-- ----------------------------
ALTER TABLE "public"."users_akses" ADD CONSTRAINT "users_akses_pkey" PRIMARY KEY ("IDAkses");

-- ----------------------------
-- Primary Key structure for table users_group
-- ----------------------------
ALTER TABLE "public"."users_group" ADD CONSTRAINT "users_group_pkey" PRIMARY KEY ("idgu");
