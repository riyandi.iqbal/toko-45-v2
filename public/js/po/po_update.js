var nomor_list = 1;

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.IDPO);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        var selected = (val.IDCoa == IDCoa) ? 'selected' : '';
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'" '+selected+'> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function addRow() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    var html = '';

    var select = '';
    select = '<select name="IDBarang[]" class="form-control select-barang select2" style="width: 100%">';
    select+= '<option value="">- Pilih Barang -</option>';
    $.each(data_barang, function(idx, val) {
        select+= '<option value="'+val.IDBarang+'" data-idsatuan="'+val.IDSatuan+'" data-satuan="'+val.Satuan+'" > ' + val.Kode_Barang + ' - ' + val.Nama_Barang + ' </option>';
    })
    select += '</select>';

    html += '<tr id="rowvar'+nomor_list+'" style="width:100%">';
    html += '<td> ' + select + ' </td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    //html += '<td> <input placeholder="" type="text" name="Satuan[]" class="form-control satuan"><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"> </td>';
    html += '<td> <select name="IDSatuan[]" class="form-control select-satuan select2" style="width: 100%"> </select> </td>';
    html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price"></td>';
    html += '<td><input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-barang').append(html);
    // setSelect2Barang();
    
    $('.select-satuan').select2();
    $('.select2').select2();
    
    setSelect2Selected();

    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.select-satuan').on('change', function() {
        var row = $(this).closest('tr');

        var reqData = {
            IDBarang : row.find('.select-barang').val(),
            IDSatuan : $(this).val(),
            IDMataUang : $('#IDMataUang').val(),
        }

        $.ajax({
            url : urlHargaJual,
            method : 'GET',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : reqData,
            beforeSend: function(data) {
              $('#modal-loading').fadeIn('fast');
            },
            success : function(data) {
                var result = JSON.parse(data);

                if (result != null) {
                    row.find('.price').val(FormatCurrency(result.Modal)).trigger('keyup');
                } else {
                    row.find('.price').val(FormatCurrency(0)).trigger('keyup');
                }
            },
            complete: function(data) {
              $('#modal-loading').fadeOut('fast');
            }
        })
    })

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.'));
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 0,
        ajax: {
            url: urlBarang,
            data: function (params) {
              return {
                q: params.term, // search term
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan : item.Satuan,
                            harga_jual : item.Harga_Jual,
                            item : item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function setSelect2Selected() {
    $('.select-barang').on('change', function(e) {
        var row = $(this).closest('tr');
        var IDSatuan = row.find('.select-barang option:selected').data('idsatuan');
        row.find('.qty').val(1).trigger('keyup');

        row.find('.select-satuan').html('');

        var select = '';
        select += '<option value="' + row.find('.select-barang option:selected').data('idsatuan') + '">' + row.find('.select-barang option:selected').data('satuan') +'</option>';

        $.ajax({
            url : urlSatuan,
            method : 'GET',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : {
                IDBarang : $(this).val()
            },
            beforeSend: function(data) {
              $('#modal-loading').fadeIn('fast');
            },
            success : function(data) {
                var result = JSON.parse(data);

                $.each((result), function(index, val) {
                    select +='<option value="'+val.IDSatuanBesar+'"> ' + val.Satuan_besar + ' </option>';
                });

                row.find('.select-satuan').append(select);
                row.find('.select-satuan').val(IDSatuan).trigger('change');
            },
            complete: function(data) {
              $('#modal-loading').fadeOut('fast');
            }
        })

    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item pesanan barang!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
                setQtyTotal();
                setGrandTotal();
            }
        });
    });
}

function setNumberSO(result) {
    $('#Nomor').val(result)  
}

function getNumberSO() {
    var reqData = {
        'Status_ppn' : $('input[type=radio]:checked').val()
    };

    // ajaxData(urlNumberSO, reqData, setNumberSO, false, false);

    if(reqData.Status_ppn == 'include'){
        if (reqData.Status_ppn == jenis_ppn) {
            $('#Nomor').val($('#Nomor_exist').val())
        } else {
            ambilnomorpononppn();
        }
    }else{
        if (reqData.Status_ppn == jenis_ppn) {
            $('#Nomor').val($('#Nomor_exist').val())
        } else {
            ambilnomorpoppn();
        }
    }
}

$(document).ready(function() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    $.each(purchase_order_detail, function(idx, val) {
        var html = '';
        var select = '';
        select = '<select name="IDBarang[]" id="idbarang'+idx+'" class="form-control select-barang select2" style="width: 100%">';
        select+= '<option value="">- Pilih Barang -</option>';
        $.each(data_barang, function(idx, val) {
            select+= '<option value="'+val.IDBarang+'" data-idsatuan="'+val.IDSatuan+'" data-satuan="'+val.Satuan+'" > ' + val.Kode_Barang + ' - ' + val.Nama_Barang + ' </option>';
        })
        select += '</select>';

        html += '<tr style="width:100%">';
        html += '<td> ' + select + ' </td>';
        html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty" value="'+val.Qty+'" ></td>';
        html += '<td> <select name="IDSatuan[]" id="'+idx+'" class="form-control select-satuan select2" style="width: 100%">  </select> </td>';
        html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price" value="'+ FormatCurrency(val.Harga) +'"></td>';
        html += '<td><input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
        html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal" value="'+ FormatCurrency(val.Saldo) +'"></td>';
        html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
        html += '</tr>';

        $('#list-barang').append(html);

        $('#idbarang' + idx).val(val.IDBarang).trigger('change');

        var row = $(this).closest('tr');

        var select = '';
        select += '<option value="' + val.IDSatuanKecil + '">' + val.Satuan_kecil +'</option>';

        $.ajax({
            url : urlSatuan,
            method : 'GET',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : {
                IDBarang : val.IDBarang
            },
            beforeSend: function(data) {
              $('#modal-loading').fadeIn('fast');
            },
            success : function(data) {
                var result = JSON.parse(data);

                $.each((result), function(index, val) {
                    select +='<option value="'+val.IDSatuanBesar+'"> ' + val.Satuan_besar + ' </option>';
                })

                $('#' + idx).append(select);
                $('#' + idx).val(val.IDSatuan);
            },
            complete: function(data) {
              $('#modal-loading').fadeOut('fast');
            }
        })
    });

    $('.select2').select2();
    setSelect2Selected();
    
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.'));
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });
    
    deleteRow();

    if (Is_taken === false) {
        toggleForm('#form-data', true);
        $('#btn-add-barang').show();  
    } else {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();   
    }

    if (Batal === true) {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();
    }

    $('input[type=radio]').on('click', function() {
        getNumberSO();
    });

    $('#btn-add-barang').on('click', function() {
        addRow();        
    });

    $('input[type=radio]').on('click', function() {
        if ($(this).val() == 'include') {
            $('.ppn').val(0);
        } else {
            $('.ppn').val(10);
        }

        $('.price').trigger('keyup');
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#IDMataUang').on('change', function() {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    })

    $('#Jenis_Pembayaran').val(Jenis_Pembayaran).trigger('change');

    $('#form-data').validate({
        rules : {
            Grand_total : { required : true }
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlUpdate, reqData, refresh, true);
        }
    });
});