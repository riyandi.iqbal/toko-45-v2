var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/edit_barang/'+row.IDBarang+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    if (row.Aktif == 'aktif') {
        button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDBarang+' \', \' ' + url + '/hapus_barang/'+row.IDBarang+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } else {
        button += '<span><a title="Aktifkan" onclick="deleteData(\' '+row.IDBarang+' \', \' ' + url + '/hapus_barang/'+row.IDBarang+' \', refresh, \' Apakah anda akan mengaktifkan kembali ? \')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDBarang', render: renderNumRow },
        {data: 'Group_Barang'},
        {data: 'Kode_Barang'},
        {data: 'Nama_Barang'},
        {data: 'Nama_Kategori'},
        {data: 'Nama_Ukuran'},
        {data: 'Satuan'},
        {data: 'StokMinimal'},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'IDBarang', className: 'dt-body-center', render: renderAction}
    ];
    var key = '';
    if ($('#field').val() == 'tbl_barang.IDGroupBarang') {
        key = $('#keyword1').val();
    } else {
        key = $('#keyword').val();
    }
    var reqData = {
        field : $('#field').val(),
        keyword : key,
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        if ($(this).val() == 'tbl_barang.IDGroupBarang') {
            $('#pilihan').hide();
            $('#select').show();
            $('#keyword_chosen').removeAttr('style');
        } else {
          $('#pilihan').show();
            $('#select').hide();
        }
  })

});