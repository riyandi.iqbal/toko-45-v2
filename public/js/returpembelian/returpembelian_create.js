function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="'+row.IDBarang+'">';
    html += '<input type="hidden" name="Qty_jual[]" value="'+row.Saldo_yard+'">';
    html += '<input type="hidden" name="Harga[]" value="'+row.Harga+'">';
    // html += '<input type="hidden" name="Sub_total[]" value="'+row.Sub_total+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    return html;
}

function renderFormRetur(data, type, row) {
    var html = '';

    html += '<input type="text" name="Qty[]" class="form-control form-with qty" style="width: 60px;"> ';

    html += '<br> <span> </span>'

    $('.qty').on('keyup', function() {
        var qtyTotal = 0;
        var grandTotal = 0;
        var DPP = 0;
        var PPN = 0;
        $('.qty').each(function() {
            var row = $(this).closest('tr');
            var thisVal = (($(this).val()).length > 0) ? parseInt($(this).val()) : 0;

            var thisQtyFJ = parseFloat(row.find('input[type=hidden][name*=Qty_jual]').val());
            var thisSpan = row.find('span');
            if (thisVal > thisQtyFJ) {
                thisSpan.addClass('text-danger');
                thisSpan.html('Qty tidak boleh qty pembelian.');
                $('#simpan').prop('disabled', true);
            } else {
                thisSpan.removeClass('text-danger');
                thisSpan.html('');
                $('#simpan').prop('disabled', false);
            }

            var thisPrice = parseFloat(row.find('input[type=hidden][name*=Harga]').val());
            
            var thisTotal = thisVal * thisPrice;

            row.find('input[type=text][name*=Sub_total]').val(CurrencyFormat(thisTotal));
            qtyTotal += thisVal;

            if ($('#Status_ppn').val() == 'exclude') {
                grandTotal += thisTotal + ((10/100) * thisTotal);

                var thisDPP = parseFloat(thisTotal);
                var thisPPN = ((10 / 100) * thisDPP );

                PPN += thisPPN;
                DPP += thisDPP;
            } else {
                grandTotal += thisTotal;
                var thisDPP = parseFloat(thisTotal);
                
                DPP += thisDPP;
            }
        });

        if ($('#Status_ppn').val() == 'exclude') {
            $('#Grand_total').val(CurrencyFormat(grandTotal));
            $('#DPP').val(CurrencyFormat(DPP));
            $('#PPN').val(CurrencyFormat(PPN));
        } else {
            $('#Grand_total').val(CurrencyFormat(grandTotal));
            $('#DPP').val(CurrencyFormat(parseFloat(DPP) + parseFloat(PPN)));
            $('#PPN').val(CurrencyFormat(PPN));
        }

        $('#Total_qty').val(qtyTotal);
    });
    return html;
}

function renderAction(data, type, row) {
    return '';
    // return '<i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i>';
}

function renderStrip(data) {
    return '-';
}

function renderSubTotal(data, type, row) {
    // return CurrencyFormat(data);
    return '<input type="text" name="Sub_total[]" class="form-control text-right" value="'+CurrencyFormat(row.Sub_total)+'">';
}

function renderHarga(data) {
    return CurrencyFormat(data);
}

function setTableBarang() {
    var colDef = [
        {data: 'Nama_Barang', name: 'Nama_Barang', render: renderForm},
        {data: 'Saldo_yard', className: 'dt-body-right'},
        {data: 'Saldo_yard', className: 'dt-body-right', render: renderFormRetur},
        {data: 'Harga', className: 'dt-body-right', render: renderHarga},
        {data: 'Satuan', name: 'Satuan', className: 'dt-body-center'},
        {data: 'Sub_total', className: 'dt-body-right', render: renderStrip},
        {data: 'Sub_total', className: 'dt-body-right', render: renderSubTotal},
        {data: 'IDFBDetail', render: renderAction},
    ];
    var reqData = {
        IDFB : $('#IDFB').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlPembelianDetail, colDef, reqData, reqOrder );
}

function setFormHeader(result) {
    $('#Status_ppn').val(result.Status_ppn);
    $('#IDSupplier').val(result.IDSupplier);
    $('#NamaSupplier').val(result.Nama);
    $('#Total_qty').val(result.Saldo_yard);
    var date = new Date(result.Tanggal);

    var dateTempo = moment(date).format('DD/MM/YYYY');
    console.log()
    $('#Tanggal_beli').val(dateTempo);
    
    ambilnomor();
    setTableBarang();
}

$(document).ready(function() {
    $('.select2').select2();

    $('#IDFB').on('change', function() {
        if($(this).val() == ''){
            $('#btn-add-barang').show();
            $('#Status_ppn').val('');
            $('#IDSupplier').val('');
            $('#NamaSupplier').val('');
            $('#Total_qty').val('');
            
            setTableBarang();
        } else {            
            var reqData = {
                'id'    : $(this).val()
            }
            ajaxData(urlPembelian, reqData, setFormHeader, false, false);
        }
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
});