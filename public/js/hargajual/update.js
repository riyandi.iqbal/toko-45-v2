function refresh(result) {
    alertSuccess(result.message, url + '/' + IDBarang);
}

$(document).ready(function() {
    $('.select2').select2();

    $('.edit').on('click', function() {
        $('#IDGroupCustomer').prop('disabled', false);
        $('#IDMataUang').prop('disabled', false);

        var row = $(this).closest('tr');

        var IDHargaJual = row.find('.id').val();
        var Modal = row.find('.modal').val();
        var Harga_Jual = row.find('.harga_jual').val();
        var Satuan = row.find('.Satuan').val();
        var IDGroupCustomer = row.find('.IDGroupCustomer').val();
        var IDMataUang = row.find('.IDMataUang').val();
        var IDBarang = row.find('.IDBarang').val();

        $('#IDHargaJual').val(IDHargaJual);
        $('#Modal').val(formatRupiah(Modal));
        $('#Harga_Jual').val(formatRupiah(Harga_Jual));
        $('#Satuan').val(Satuan);
        $('#IDGroupCustomer').val(IDGroupCustomer).trigger('change');
        $('#IDMataUang').val(IDMataUang).trigger('change');

        // if (IDGroupCustomer != 0) {
        //     $('#IDGroupCustomer').prop('disabled', true);
        // }

        if (IDMataUang != 0) {
            $('#IDMataUang').prop('disabled', true);
        }

        $('#IDBarang').val(IDBarang);

        $('#modal-data').modal('show');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
    });

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlUpdate, reqData, refresh, true);
        }
    });
})