function refresh(result) {
    alertSuccess(result.message, url);
}

function fillSatuan(result) {
    $.each(result, function(idx, val) {
        data_satuan.push({
            'IDSatuan' : val.IDSatuanBesar,
            'Satuan'  : val.Satuan_besar
        });
    })

    $('.satuan').html('');

    var select_satuan = '';
    select_satuan += '<option value="">- Pilih Satuan -</option>';

    $.each(data_satuan, function(index, val) {
        select_satuan +='<option value="'+val.IDSatuan+'"> ' + val.Satuan + ' </option>';
    });

    $('.satuan').append(select_satuan);
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
            }
        });
    });
}

function addRow() {
    var html = '';
    var select_group_customer   = '';
    var select_mata_uang        = '';

    select_group_customer = '<select class="form-control select2" name="IDGroupCustomer[]" style="width: 100%">';
    select_group_customer += '<option value="">- Pilih Group Customer -</option>';

    $.each(data_customer_group, function(idx, val) {
        select_group_customer+= '<option value="'+val.IDGroupCustomer+'"> ' + val.Nama_Group_Customer + ' </option>';
    })
    select_group_customer += '</select>';

    select_satuan = '<select class="form-control select2 satuan" name="IDSatuan[]" style="width: 100%">';
    select_satuan += '<option value="">- Pilih Satuan -</option>';

    $.each(data_satuan, function(index, val) {
        select_satuan +='<option value="'+val.IDSatuan+'"> ' + val.Satuan + ' </option>';
    });

    select_satuan += '</select>';

    select_mata_uang = '<select class="form-control select2" name="IDMataUang[]" style="width: 100%">';
    select_mata_uang += '<option value="">- Pilih Mata Uang -</option>';

    $.each(data_mata_uang, function(idx, val) {
        select_mata_uang+= '<option value="'+val.IDMataUang+'"> ' + val.Mata_uang + ' </option>';
    })

    select_mata_uang += '</select>';

    html += '<tr id="rowvar" style="width:100%">';
    html += '<td> '+ select_group_customer +' </td>';
    html += '<td> '+ select_satuan +' </td>';
    html += '<td> '+ select_mata_uang +' </td>';
    html += '<td><input placeholder="" type="text" name="Modal[]" class="form-control text-right price"></td>';
    html += '<td><input placeholder="" type="text" name="Harga_Jual[]" class="form-control text-right price"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-satuan').append(html);    
    
    $('.select2').select2();

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
    });

    deleteRow();
}

$(document).ready(function() {
    $('.select2').select2();
    addRow();

    $('#btn-add-satuan').on('click', function() {
        addRow();        
    })

    $('#IDBarang').on('change', function() {
        data_satuan = [];
        
        var thisIDSatuan = $('#IDBarang option:selected').data('idsatuan');
        var thisSatuan = $('#IDBarang option:selected').data('satuan');

        data_satuan.push({
            'IDSatuan' : thisIDSatuan,
            'Satuan'    : thisSatuan
        });

        var reqData = {
            IDBarang : $(this).val()
        };

        ajaxData(urlSatuan, reqData, fillSatuan);
    })

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
})