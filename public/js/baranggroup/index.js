var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/edit_group_barang/'+row.IDGroupBarang+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    if (row.Aktif == 'aktif') {
        button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDGroupBarang+' \', \' ' + url + '/hapus_group_barang/'+row.IDGroupBarang+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } else {
        button += '<span><a title="Aktifkan" onclick="deleteData(\' '+row.IDGroupBarang+' \', \' ' + url + '/hapus_group_barang/'+row.IDGroupBarang+' \', refresh, \' Apakah anda akan mengaktifkan kembali ? \')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDGroupBarang', render: renderNumRow },
        {data: 'Kode_Group_Barang'},
        {data: 'Group_Barang'},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'IDGroupBarang', className: 'dt-body-center', render: renderAction}
    ];
    var key = '';
    if ($('#field').val() == 'tbl_barang.IDGroupBarang') {
        key = $('#keyword1').val();
    } else {
        key = $('#keyword').val();
    }
    var reqData = {
        field : $('#field').val(),
        keyword : key,
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        if ($(this).val() == 'tbl_barang.IDGroupBarang') {
            $('#pilihan').hide();
            $('#select').show();
            $('#keyword_chosen').removeAttr('style');
        } else {
          $('#pilihan').show();
            $('#select').hide();
        }
  })

});