$( document ).ready(function() {
    //$(".kode2").hide();
    $(".nama2").hide();
    $(".tipe2").hide();
    $(".ukuran2").hide();
    $(".kategori2").hide();

    //$(".kode3").hide();
    $(".nama3").hide();
    $(".tipe3").hide();
    $(".ukuran3").hide();
    $(".kategori3").hide();

    $("#kodelabel").hide();
    $("#namalabel").hide();
    $("#tipelabel").hide();
    $("#ukuranlabel").hide();
    $("#kategorilabel").hide();
    $("#pemisahlabel").hide();
    $("#pemisahlabel2").hide();
    $("#pemisahlabel3").hide();
    $("#pemisahlabel4").hide();
    $("#pemisahlabel5").hide();
    $("#panjanglabel").hide();

    $("#2kodelabel").hide();
    $("#2namalabel").hide();
    $("#2tipelabel").hide();
    $("#2ukuranlabel").hide();
    $("#2kategorilabel").hide();
    $("#2pemisahlabel").hide();
    $("#2pemisahlabel2").hide();
    $("#2pemisahlabel3").hide();
    $("#2pemisahlabel4").hide();
    $("#2pemisahlabel5").hide();
    $("#2panjanglabel").hide();

});
function pemisah()
{
    console.log('PEMISAH');
    if($('#pemisah').val()=='')
    {
        var pemisah = ' ';
    }else{
        var pemisah = $('#pemisah').val();
    }
    if($('#kode2select').val() == 'kode2'){
        document.getElementById('pemisahlabel').innerHTML = pemisah;
        $('#pemisahlabel').show();
    }else{
        document.getElementById('pemisahlabel2').innerHTML = pemisah;
        $('#pemisahlabel2').show();
    }
    document.getElementById('pemisahlabel3').innerHTML = pemisah;
    document.getElementById('pemisahlabel4').innerHTML = pemisah;
    document.getElementById('pemisahlabel5').innerHTML = pemisah;
}

function pemisah2()
{
    console.log('PEMISAH2');
    if($('#pemisah2').val()=='')
    {
        var pemisah = ' ';
    }else{
        var pemisah = $('#pemisah2').val();
    }
    
    if($('#kode2select').val() == 'kode2'){
        document.getElementById('2pemisahlabel').innerHTML = pemisah;
        $('#2pemisahlabel').show();
    }else{
        document.getElementById('2pemisahlabel2').innerHTML = pemisah;
        $('#2pemisahlabel2').show();
    }
    document.getElementById('2pemisahlabel3').innerHTML = pemisah;
    document.getElementById('2pemisahlabel4').innerHTML = pemisah;
    document.getElementById('2pemisahlabel5').innerHTML = pemisah;
    

}

function panjang()
{
    console.log('PANJANG KODE');
    if($('#panjang').val()=='')
    {
        var panjang = '1';
    }else{
        var panjang = '1'.padStart($('#panjang').val(), '0');
    }
    document.getElementById('panjanglabel').innerHTML = panjang;
    $('#panjanglabel').show();
}

function panjang2()
{
    console.log('PANJANG2 KODE');
    if($('#panjang2').val()=='')
    {
        var panjang = '1';
    }else{
        var panjang = '1'.padStart($('#panjang2').val(), '0');
    }
    document.getElementById('2panjanglabel').innerHTML = panjang;
    $('#2panjanglabel').show();
}

function kode()
{
    if(document.getElementById("kode").checked == true){
        console.log("CEK KODE CHECK");
        $(".kode2").show();
        $(".kode3").show();
    }else{
        console.log("CEK KODE UNCHECK");
        $(".kode2").hide();
        $(".kode3").hide();
        document.getElementById("kode2").checked = false;
        document.getElementById("kode3").checked = false;
    }
}

function kode2()
{
    console.log($('#kode2select').val());
    document.getElementById('kodelabel').innerHTML = '';
    document.getElementById('namalabel').innerHTML = '';
    if(document.getElementById("kode2").checked == true){
        if($('#kode2select').val()=='kode2'){
            $('#kodelabel').show();
            document.getElementById('kodelabel').innerHTML = '['+$('#kodegroup').val()+']';
            $('#pemisahlabel').show();
        }else{
            $('#namalabel').show();
            document.getElementById('kodelabel').innerHTML = '['+$('#namagroup').val()+']';
            $('#pemisahlabel').show();
        }
    }else{
        if($('#kode2select').val()=='kode2'){
            $('#kodelabel').hide();
            document.getElementById('kodelabel').innerHTML = '['+$('#kodegroup').val()+']';
            $('#pemisahlabel').hide();
        }else{
            $('#namalabel').hide();
            document.getElementById('kodelabel').innerHTML = '['+$('#namagroup').val()+']';
            $('#pemisahlabel').hide();
        }
    }
    
}

function kode3()
{
    console.log($('#kode3select').val());
    document.getElementById('2kodelabel').innerHTML = '';
    document.getElementById('2namalabel').innerHTML = '';
    if(document.getElementById("kode3").checked == true){
        if($('#kode3select').val()=='kode3'){
            $('#2kodelabel').show();
            document.getElementById('2kodelabel').innerHTML = '['+$('#kodegroup').val()+']';
            $('#2pemisahlabel').show();
        }else{
            $('#2namalabel').show();
            document.getElementById('2kodelabel').innerHTML = '['+$('#namagroup').val()+']';
            $('#2pemisahlabel').show();
        }
    }else{
        if($('#kode3select').val()=='kode3'){
            $('#2kodelabel').hide();
            document.getElementById('2kodelabel').innerHTML = '['+$('#kodegroup').val()+']';
            $('#2pemisahlabel').hide();
        }else{
            $('#2namalabel').hide();
            document.getElementById('2kodelabel').innerHTML = '['+$('#namagroup').val()+']';
            $('#2pemisahlabel').hide();
        }
    }
}

function nama()
{
    if(document.getElementById("nama").checked == true){
        console.log("CEK Nama CHECK");
        $(".nama2").show();
        $(".nama3").show();
    }else{
        console.log("CEK Nama UNCHECK");
        $(".nama2").hide();
        $(".nama3").hide();
        document.getElementById("nama2").checked = false;
        document.getElementById("nama3").checked = false;
    }
}

function nama2()
{
    if(document.getElementById("nama2").checked == true){
        $('#namalabel').show();
        $('#pemisahlabel2').show();
        //$('#panjanglabel').show();
    }else{
        $('#namalabel').hide();
        $('#pemisahlabel2').hide();
        //$('#panjanglabel').hide();
    }
}

function nama3()
{
    if(document.getElementById("nama3").checked == true){
        $('#2namalabel').show();
        $('#2pemisahlabel2').show();
        //$('#panjanglabel').show();
    }else{
        $('#2namalabel').hide();
        $('#2pemisahlabel2').hide();
        //$('#panjanglabel').hide();
    }
}

function tipe()
{
    if(document.getElementById("tipe").checked == true){
        console.log("CEK Tipe CHECK");
        $(".tipe2").show();
        $(".tipe3").show();
    }else{
        console.log("CEK Tipe UNCHECK");
        $(".tipe2").hide();
        $(".tipe3").hide();
        document.getElementById("tipe2").checked = false;
        document.getElementById("tipe3").checked = false;
    }
}

function tipe2()
{
    if(document.getElementById("tipe2").checked == true){
        $('#tipelabel').show();
        $('#pemisahlabel3').show();
        //$('#panjanglabel').show();
    }else{
        $('#tipelabel').hide();
        $('#pemisahlabel3').hide();
        //$('#panjanglabel').hide();
    }
}

function tipe3()
{
    if(document.getElementById("tipe3").checked == true){
        $('#2tipelabel').show();
        $('#2pemisahlabel3').show();
        //$('#panjanglabel').show();
    }else{
        $('#2tipelabel').hide();
        $('#2pemisahlabel3').hide();
        //$('#panjanglabel').hide();
    }
}

function ukuran()
{
    if(document.getElementById("ukuran").checked == true){
        console.log("CEK Ukuran CHECK");
        $(".ukuran2").show();
        $(".ukuran3").show();
    }else{
        console.log("CEK Ukuran UNCHECK");
        $(".ukuran2").hide();
        $(".ukuran3").hide();
        document.getElementById("ukuran2").checked = false;
        document.getElementById("ukuran3").checked = false;
    }
}

function ukuran2()
{
    if(document.getElementById("ukuran2").checked == true){
        $('#ukuranlabel').show();
        $('#pemisahlabel4').show();
        //$('#panjanglabel').show();
    }else{
        $('#ukuranlabel').hide();
        $('#pemisahlabel4').hide();
        //$('#panjanglabel').hide();
    }
}

function ukuran3()
{
    if(document.getElementById("ukuran3").checked == true){
        $('#2ukuranlabel').show();
        $('#2pemisahlabel4').show();
        //$('#panjanglabel').show();
    }else{
        $('#2ukuranlabel').hide();
        $('#2pemisahlabel4').hide();
        //$('#panjanglabel').hide();
    }
}

function kategori()
{
    if(document.getElementById("kategori").checked == true){
        console.log("CEK Kategori CHECK");
        $(".kategori2").show();
        $(".kategori3").show();
    }else{
        console.log("CEK Kategori UNCHECK");
        $(".kategori2").hide();
        $(".kategori3").hide();
        document.getElementById("kategori2").checked = false;
        document.getElementById("kategori3").checked = false;
    }
}

function kategori2()
{
    if(document.getElementById("kategori2").checked == true){
        $('#kategorilabel').show();
        $('#pemisahlabel5').show();
        //$('#panjanglabel').show();
    }else{
        $('#kategorilabel').hide();
        $('#pemisahlabel5').hide();
        //$('#panjanglabel').hide();
    }
}

function kategori3()
{
    if(document.getElementById("kategori3").checked == true){
        $('#2kategorilabel').show();
        $('#2pemisahlabel5').show();
        //$('#panjanglabel').show();
    }else{
        $('#2kategorilabel').hide();
        $('#2pemisahlabel5').hide();
        //$('#panjanglabel').hide();
    }
}


function field()
{
    var data = {
          "kode2"        : document.getElementById("kode2").checked,
          "nama2"        : document.getElementById("nama2").checked, 
          "tipe2"        : document.getElementById("tipe2").checked, 
          "ukuran2"      : document.getElementById("ukuran2").checked, 
          "kategori2"    : document.getElementById("kategori2").checked,
          "pemisah"      : $("#pemisah").val(),
          "panjang"      : $("#panjang").val(),
          "kode2select"  : $("#kode2select").val(), 
        }
        return data;
}

function field2()
{
    var data2 = {
          "kode3"        : document.getElementById("kode3").checked,
          "nama3"        : document.getElementById("nama3").checked, 
          "tipe3"        : document.getElementById("tipe3").checked, 
          "ukuran3"      : document.getElementById("ukuran3").checked, 
          "kategori3"    : document.getElementById("kategori3").checked,
          "pemisah2"      : $("#pemisah2").val(),
          "panjang2"      : $("#panjang2").val(),
          "kode3select"  : $("#kode3select").val(),  
        }
        return data2;
}

function field3()
{
    var data3 = {
        "kodegroup"     : $('#kodegroup').val(),
        "namagroup"     : $('#namagroup').val(),
    }
    return data3;
}

function simpandata()
{
    $("#modal-loading").fadeIn();
    console.log('CEK DATA SIMPANAN');
    var data = field();
    var data2 = field2();
    var data3 = field3();
    console.log(data);
    console.log(data2);
    console.log(data3);

    console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : urlSimpan,
                  type: "POST",
                  data:{"_token": token, data1:data, data2:data2, data3:data3},
                  dataType:'json',
                  beforeSend: function(data) {
                    $('#modal-loading').fadeIn('fast');
                  },
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    $("#modal-loading").fadeOut();
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Simpan Data",
                        icon: "success"
                    }).then(function() {
                        window.location = BackGroupBarang;
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
}