var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/print/'+row.IDSOK+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDSOK+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';
    button += '<span><a title="Edit" href="' + url + '/'+row.IDSOK+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
    if (row.Batal == 1) {
        button += '<span><a title="Aktif" onclick="deleteData(\' '+row.IDSOK+' \', \' ' + url + '/'+row.IDSOK+' \', refresh, \'Apakah anda yakin mengaktifkan sales order ?\')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    } else {
        button += '<span><a title="Batalkan" onclick="deleteData(\' '+row.IDSOK+' \', \' ' + url + '/'+row.IDSOK+' \', refresh, \'Apakah anda yakin membatalkan sales order ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    }

    return button;
}

function renderGrandTotal(data, type, row) {
    return row.Kode + FormatCurrency(data);
}

function setTable() {
    var colDef = [
        {data: 'Nomor', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nama'},
        {data: 'Nama_Barang'},
        {data: 'Qty', className: 'dt-body-right'},
        {data: 'Harga', className: 'dt-body-right', render: renderGrandTotal},
        {data: 'Sub_total', className: 'dt-body-right', render: renderGrandTotal},
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over this page
        pageTotalQty = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 5 ).footer() ).html(
            (pageTotalQty)
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })
});