var nomor_list = 1;

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.IDSOK);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'"> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function addRow() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    var html = '';

    var select = '';
    select = '<select name="IDBarang[]" class="form-control select-barang select2" style="width: 100%">';
    select+= '<option value="">- Pilih Barang -</option>';
    $.each(data_barang, function(idx, val) {
        select+= '<option value="'+val.IDBarang+'" data-idsatuan="'+val.IDSatuan+'" data-satuan="'+val.Satuan+'" > ' + val.Kode_Barang + ' - ' + val.Nama_Barang + ' </option>';
    })
    select += '</select>';

    html += '<tr id="rowvar'+nomor_list+'" style="width:100%">';
    //html += '<td> <select name="IDBarang[]" class="form-control select-barang" style="width: 100%"> </select> </td>';
    html += '<td> ' + select + ' </td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    // html += '<td><input placeholder="" type="text" name="Satuan[]" class="form-control satuan"><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"></td>';
    html += '<td> <select name="IDSatuan[]" class="form-control select-satuan select2" style="width: 100%"> </select> </td>';
    html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price"></td>';
    html += '<td><input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-barang').append(html);
    // setSelect2Barang();

    $('.select-satuan').select2();
    $('.select2').select2();
    
    setSelect2Selected();
    
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.select-satuan').on('change', function() {
        var row = $(this).closest('tr');

        var reqData = {
            IDBarang : row.find('.select-barang').val(),
            IDSatuan : $(this).val(),
            IDGroupCustomer: $('#IDGroupCustomer').val(),
        }

        $.ajax({
            url : urlHargaJual,
            method : 'GET',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : reqData,
            beforeSend: function(data) {
              $('#modal-loading').fadeIn('fast');
            },
            success : function(data) {
                var result = JSON.parse(data);

                if (result != null) {
                    row.find('.price').val(FormatCurrency(result.Harga_Jual)).trigger('keyup');
                } else {
                    row.find('.price').val(FormatCurrency(0)).trigger('keyup');
                }
            },
            complete: function(data) {
              $('#modal-loading').fadeOut('fast');
            }
        })
    })

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.')) : 0;
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 2,
        ajax: {
            url: urlBarang,
            data: function (params) {
              return {
                q: params.term, // search term
                IDGroupCustomer: $('#IDGroupCustomer').val(),
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan : item.Satuan,
                            harga_jual : item.Harga_Jual,
                            item : item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function fillSatun(result) {
    var select = '';

    $.each((result), function(index, val) {
        select +='<option value="'+val.IDSatuan+'"> </option>';
    });

    $('#IDSatuan').append(select);
}

function setSelect2Selected() {
    $('.select-barang').on('change', function(e) {
        var row = $(this).closest('tr');
        var IDSatuan = row.find('.select-barang option:selected').data('idsatuan');
        row.find('.qty').val(1).trigger('keyup');

        row.find('.select-satuan').html('');

        var select = '';
        select += '<option value="' + row.find('.select-barang option:selected').data('idsatuan') + '">' + row.find('.select-barang option:selected').data('satuan') +'</option>';

        $.ajax({
            url : urlSatuan,
            method : 'GET',
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : {
                IDBarang : $(this).val()
            },
            beforeSend: function(data) {
              $('#modal-loading').fadeIn('fast');
            },
            success : function(data) {
                var result = JSON.parse(data);

                $('#modal-loading').fadeIn('fast');
                
                $.each((result), function(index, val) {
                    select +='<option value="'+val.IDSatuanBesar+'"> ' + val.Satuan_besar + ' </option>';
                });

                row.find('.select-satuan').append(select);
                row.find('.select-satuan').val(IDSatuan).trigger('change');

                $('#modal-loading').fadeOut('fast');
            },
            complete: function(data) {
              $('#modal-loading').fadeOut('fast');
            }
        })

    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        console.log(thisSubTotal);
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item sales order barang!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
                setQtyTotal();
                setGrandTotal();
            }
        });
    });
}

function setNumberSO(result) {
    $('#Nomor').val(result)  
}

function getNumberSO() {
    var reqData = {
        'Status_ppn' : $('input[type=radio]:checked').val()
    };
    console.log(reqData);
    if(reqData=='include'){
        ambilnomorsononppn();
    }else{
        ambilnomorsoppn();
    }
    //ajaxData(urlNumberSO, reqData, setNumberSO, false, false);
}

$(document).ready(function() {
    $('.select2').select2();
    getNumberSO();
    setSelect2Barang();
    addRow();

    $('.group').hide();

    $('#btn-add-barang').on('click', function() {
        addRow();        
    })

    $('input[type=radio]').on('click', function() {
        getNumberSO();
    });

    $('input[type=radio]').on('click', function() {
        if ($(this).val() == 'include') {
            $('.ppn').val(0);
        } else {
            $('.ppn').val(10);
        }

        $('.price').trigger('keyup');
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#IDMataUang').on('change', function() {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    })

    $('#IDCustomer').on('change', function() {
        $('#IDGroupCustomer').val($('#IDCustomer option:selected').data('idgroup')).trigger('change')
    })

    $('#form-data').validate({
        rules : {
            Grand_total : { required : true }
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
});