var nomor_list = 1;

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.IDSOK);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        var selected = (val.IDCoa == IDCoa) ? 'selected' : '';
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'" '+selected+'> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function addRow() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    var html = '';

    html += '<tr id="rowvar'+nomor_list+'" style="width:100%">';
    html += '<td> <select name="IDBarang[]" class="form-control select-barang" style="width: 100%"> </select> </td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price"></td>';
    html += '<td> <input placeholder="" type="text" name="Satuan[]" class="form-control satuan"><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"> </td>';
    html += '<td><input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-barang').append(html);
    setSelect2Barang();
    
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.'));
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 2,
        ajax: {
            url: urlBarang,
            data: function (params) {
              return {
                q: params.term, // search term
                IDGroupCustomer: $('#IDGroupCustomer').val(),
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan : item.Satuan,
                            harga_jual : item.Harga_Jual,
                            item : item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function setSelect2Selected() {
    $('.select-barang').on('select2:select', function(e) {
        var result = e.params.data;
        
        var row = $(this).closest('tr');
        row.find('.price').val(FormatCurrency(result.item.Harga_Jual)).trigger('keyup');
        row.find('.qty').val(1).trigger('keyup');
        row.find('.satuan').val(result.item.Satuan);
        row.find('.idsatuan').val(result.item.IDSatuan);
    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item sales order barang!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
                setQtyTotal();
                setGrandTotal();
            }
        });
    });
}

function setNumberSO(result) {
    $('#Nomor').val(result)  
}

function getNumberSO() {
    var reqData = {
        'Status_ppn' : $('input[type=radio]:checked').val()
    };

    ajaxData(urlNumberSO, reqData, setNumberSO, false, false);
}

$(document).ready(function() {
    $('.select2').select2();
    setSelect2Barang();

    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.'));
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();

    if (surat_jalan === false) {
        toggleForm('#form-data', true);
        $('#btn-add-barang').show();  
    } else {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();   
    }

    if (Batal === true) {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();
    }

    $('#IDGroupCustomer').prop('disabled', true);

    $('#btn-add-barang').on('click', function() {
        addRow();        
    });

    $('input[type=radio]').on('click', function() {
        if ($(this).val() == 'include') {
            $('.ppn').val(0);
        } else {
            $('.ppn').val(10);
        }

        $('.price').trigger('keyup');
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#IDMataUang').on('change', function() {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    })

    $('#IDCustomer').on('change', function() {
        $('#IDGroupCustomer').val($('#IDCustomer option:selected').data('idgroup')).trigger('change')
    })

    $('#IDCustomer').trigger('change');

    $('#Jenis_Pembayaran').val(Jenis_Pembayaran).trigger('change');

    $('#form-data').validate({
        rules : {
            Grand_total : { required : true }
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlUpdate, reqData, refresh, true);
        }
    });
});