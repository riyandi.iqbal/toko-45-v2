function refresh(result) {
    alertSuccess(result.message, urlSalesOrder + '/detail/' + result.data.IDSOK);
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        console.log(thisSubTotal);
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

$(document).ready(function() {
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.')) : 0;
        var rowPpn      = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    $('#form-data').validate({
        rules : {
            Grand_total : { required : true }
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlApproved, reqData, refresh, true);
        }
    });
})