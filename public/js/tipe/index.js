var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/'+row.IDTipe+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    if (row.Status == 'aktif') {
        button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDTipe+' \', \' ' + url + '/'+row.IDTipe+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } else {
        button += '<span><a title="Aktifkan" onclick="deleteData(\' '+row.IDTipe+' \', \' ' + url + '/'+row.IDTipe+' \', refresh, \' Apakah anda akan mengaktifkan kembali ? \')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDTipe', render: renderNumRow },
        {data: 'Kode_Tipe'},
        {data: 'Nama_Tipe'},
        {data: 'Status', className: 'dt-body-center'},
        {data: 'IDTipe', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});