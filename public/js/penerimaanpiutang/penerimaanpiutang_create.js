var tablePembayaran;
var dataPembayaran = [];

function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'"> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function setNumber(result) {
    $('#Nomor').val(result)  
}

function getNumber() {
    var reqData = null;

    ajaxData(urlNumber, reqData, setNumber, false, false);
}

function renderForm(data, type, row) {
    var html = '';

    if (row.Saldo_Akhir > 0) {
        html += '<input type="text" class="form-control input-date-padding-3-2 currency text-right" name="Nominal[]" disabled>';
    }

    $('.currency').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()));
        var TotalPembayaran = 0;
        $('.currency').each(function() {
            if ($(this).val().length > 0) {
                var thisVal = parseFloat($(this).val().replace(/\./g,'').replace(',', '.'));
                TotalPembayaran += thisVal;
            }
        });
        
        $('#Total_pembayaran').val(FormatCurrency(TotalPembayaran));
        $('#Total_pembayaran_1').val(FormatCurrency(TotalPembayaran));
    });

    return html;
}

function renderSaldoAkhir(data, type, row) {
    var html = '';

    html += '<input type="text" class="form-control input-date-padding-3-2 currency text-right saldo_piutang" name="Saldo_Akhir[]" value="'+FormatCurrency(data)+'" readonly>';

    return html;
}

function renderCheck(data, type, row) {
    var html = '';

    html += '<input type="checkbox" name="do[]" class="checked" data-IDPiutang="'+row.IDPiutang+'" data-Saldo_Akhir="'+row.Saldo_Akhir+'" value="'+row.IDPiutang+'">';

    $('input[type=checkbox][name*=do]').on('click', function() {
        var TotalPiutang = 0;
        $('input[type=checkbox][name*=do]').each(function() {
            var row = $(this).closest('tr');
            if ($(this).is(':checked')) {
                var thisVal = parseFloat($(this).data('saldo_akhir'));
                TotalPiutang += thisVal;
                row.find('input[type=text][name*=Nominal]').prop('disabled', false);
                row.find('input[type=text][name*=Nominal]').prop('required', true);
            } else {
                row.find('input[type=text][name*=Nominal]').val('').trigger('keyup')
                row.find('input[type=text][name*=Nominal]').prop('disabled', true);
                row.find('input[type=text][name*=Nominal]').prop('required', false);
            }
        });

        $('#Total_piutang').val(FormatCurrency(TotalPiutang));
    });

    return html;
}

function renderPembayaran(data, type, row) {
    return FormatCurrency(parseFloat(row.Pembayaran) + parseFloat(row.Retur));
}

function setTablePiutang() {
    var colDef = [
        {data: 'IDPiutang', render: renderNumRow },
        {data: 'IDPiutang', render: renderCheck },
        {data: 'No_Faktur' },
        {data: 'Mata_uang' },
        {data: 'Tanggal_Piutang', render: renderDate },
        {data: 'Jatuh_Tempo', render: renderDate },
        {data: 'Nilai_Piutang', className: 'dt-body-right', render: FormatCurrency },
        {data: 'UM', className: 'dt-body-right', render: FormatCurrency },
        {data: 'Retur', className: 'dt-body-right', render: FormatCurrency },
        {data: 'Pembayaran', className: 'dt-body-right', render: renderPembayaran },
        {data: 'IDPiutang', render: renderForm },
        {data: 'Saldo_Akhir', className: 'dt-body-right', render: FormatCurrency },
    ];
    var reqData = {
        IDCustomer : $('#IDCustomer').val(),
    };

    var reqOrder = null;

    tablePiutang = setDataTable('#table-piutang', urlPiutang, colDef, reqData, reqOrder );
}

function renderAction(data, type, row, meta) {
    var button = '';
    
    button += '<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(\''+meta.row+'\')"> <span class="fa fa-trash"></span></button>';

    return button;
}

function deleteRow(row) {
    dataPembayaran.splice(row, 1);
    
    setTablePembayaran();
}

function setTablePembayaran() {
    tablePembayaran = $('#table-pembayaran').dataTable({
        data : dataPembayaran,
        columns: [
            { render : renderNumRow, data : "Jenis_pembayaran_text" },
            { data : "Jenis_pembayaran_text" },
            { data : "IDCoa_text" },
            { data : "Tanggal_giro" },
            { data : "Nomor_giro" },
            { data : "Nominal_pembayaran", className: 'dt-body-right', render: FormatCurrency },
            { render : renderAction }
        ],
        destroy : true,
        processing : true,
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/\./g,'').replace(',', '.')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                FormatCurrency(pageTotal) +' ( '+ FormatCurrency(total) +' )'
            );
        }
    });

    var TotalPembayaran1 = ($('#Total_pembayaran_1').val().length > 0 ) ? $('#Total_pembayaran_1').val().replace(/\./g,'').replace(',', '.') : 0;
    var KelebihanBayar = parseFloat(total) - parseFloat( TotalPembayaran1 ) ;

    if (KelebihanBayar < 0) {
        $('#Kelebihan_bayar').val(FormatCurrency(0));
    } else {
        $('#Kelebihan_bayar').val(FormatCurrency(KelebihanBayar));
    }

    if (KelebihanBayar <= 0) {
        $('#IDCoa_kelebihan_bayar').prop('disabled', true);
    } else {
        $('#IDCoa_kelebihan_bayar').prop('disabled', false);
    }
}

function getTotalPiutang() {
    var TotalPiutang = 0;
    $('.saldo_piutang').each(function() {
        var thisVal = parseFloat($(this).val().replace(/\./g,'').replace(',', '.'));
        TotalPiutang += thisVal;
    });
}

$(document).ready(function() {
    $('.select2').select2();
    setTablePiutang();
    setTablePembayaran();

    $('#IDCustomer').on('change', function() {
        if ($(this).val() != '') {
            setTablePiutang();
        } else {

        }
    });
    
    getNumber();

    $('#Jenis_pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#Tambah_pembayaran').on('click', function() {
        if ($('#Jenis_pembayaran').val() == '' || $('#IDCoa').val() == '' || $('#Nominal_pembayaran').val() == '') {
            alert('Lengkapi isian.');
        } else {
            if ($('#Jenis_pembayaran').val() == 'giro') {
                if ($('#Tanggal_giro').val() == '' || $('#Nomor_giro').val() == '') {
                    alert('Lengkapi isian.')
                } else {
                    data = {
                        'Jenis_pembayaran' : $('#Jenis_pembayaran').val(),
                        'Jenis_pembayaran_text' : $( "#Jenis_pembayaran option:selected" ).text(),
                        'IDCoa' : $('#IDCoa').val(),
                        'IDCoa_text' : $( "#IDCoa option:selected" ).text(),
                        'Nomor_giro' : $('#Nomor_giro').val(),
                        'Tanggal_giro' : $('#Tanggal_giro').val(),
                        'Nominal_pembayaran' : parseFloat($('#Nominal_pembayaran').val().replace(/\./g,'')),
                        'IDMataUang' : $('#IDMataUang').val(),
                        'Kurs' : $('#Kurs').val(),
                    };
            
                    dataPembayaran.push(data);
        
                    $('#Jenis_pembayaran').val('');
                    $('#IDCoa').val('');
                    $('#Nomor_giro').val('');
                    $('#Tanggal_giro').val('');
                    $('#Nominal_pembayaran').val('');
                    $('#IDMataUang').val('');
                    $('#Kurs').val('');
                    setTablePembayaran();
                }
            } else {
                data = {
                    'Jenis_pembayaran' : $('#Jenis_pembayaran').val(),
                    'Jenis_pembayaran_text' : $( "#Jenis_pembayaran option:selected" ).text(),
                    'IDCoa' : $('#IDCoa').val(),
                    'IDCoa_text' : $( "#IDCoa option:selected" ).text(),
                    'Nomor_giro' : $('#Nomor_giro').val(),
                    'Tanggal_giro' : $('#Tanggal_giro').val(),
                    'Nominal_pembayaran' : parseFloat($('#Nominal_pembayaran').val().replace(/\./g,'')),
                    'IDMataUang' : $('#IDMataUang').val(),
                    'Kurs' : $('#Kurs').val(),
                };
        
                dataPembayaran.push(data);
    
                $('#Jenis_pembayaran').val('');
                $('#IDCoa').val('');
                $('#Nomor_giro').val('');
                $('#Tanggal_giro').val('');
                $('#Nominal_pembayaran').val('');
                $('#IDMataUang').val('');
                $('#Kurs').val('');
                setTablePembayaran();
            }
        }
    });

    $('#IDMataUang').on('change', function() {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    })

    $('#form-data').validate({
        rules : {

        },
        submitHandler: function(form) {
            var reqData = new FormData(form);
            reqData.append('Pembayaran_detail', JSON.stringify(dataPembayaran));

            ajaxData(urlInsert, reqData, refresh, true);
        }
    })
});