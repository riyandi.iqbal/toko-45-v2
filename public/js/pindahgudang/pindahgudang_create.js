var nomor_list = 1;

function refresh(result) {
    alertSuccess(result.message, url);
}

function addRow() {
    var html = '';

    html += '<tr id="rowvar'+nomor_list+'" style="width:100%">';
    html += '<td> <select name="IDBarang[]" class="form-control select-barang" style="width: 100%"> </select> </td>';
    html += '<td><input placeholder="" type="text" name="Satuan[]" class="form-control satuan" readonly><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"></td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price"></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal" readonly></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-barang').append(html);
    setSelect2Barang();
    
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.')) : 0;
        var rowSubTotal = (rowQty * rowPrice);

        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 2,
        ajax: {
            url: urlBarang,
            data: function (params) {
              return {
                q: params.term, // search term
                IDGudang: $('#IDGudangAwal').val(),
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan : item.Satuan,
                            modal : item.Modal,
                            item : item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function setSelect2Selected() {
    $('.select-barang').on('select2:select', function(e) {
        var result = e.params.data;
        
        var row = $(this).closest('tr');
        row.find('.price').val(FormatCurrency(result.item.Modal)).trigger('keyup');
        row.find('.qty').val(1).trigger('keyup');
        row.find('.satuan').val(result.item.Satuan);
        row.find('.idsatuan').val(result.item.IDSatuan);
    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item barang!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
                setQtyTotal();
                setGrandTotal();
            }
        });
    });
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = null;

    ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

$(document).ready(function() {
    $('.select2').select2();
    getNumberInvoice();

    addRow();

    $('#btn-add-barang').on('click', function() {
        addRow();        
    });

    $('#IDGudangAwal').on('change', function() {
        $('#IDGudangTujuan').val('').trigger('change');

        $('#IDGudangTujuan option').prop('disabled', false);
        
        if ($(this).val() != '') {
            $('#IDGudangTujuan option[value='+$(this).val()+']').prop('disabled', true);
        }
        
        $('#IDGudangTujuan').select2();
    })

    $('#form-data').validate({
        rules : {
            Grand_total : { required : true }
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
});