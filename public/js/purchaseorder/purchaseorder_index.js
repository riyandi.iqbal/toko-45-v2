
    $(document).ready(function() {
        $('#tblpo').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false
        });

        if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        endif
    })

    

    var PM = [];
    function caridata()
    {
        var jenis       = $('#jenispencarian').val();
        var keyword     = $('#keyword').val();
        var date_from   = $('#date_from').val();
        var date_until  = $('#date_until').val();
            $.ajax({
                  url : "{{url('Po/pencarian')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];


                      if(PM[i].Batal=='aktif'){
                         var $status  = "<span><a href='Po/show/"+PM[i].IDPO+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='Po/printpo/"+PM[i].IDPO+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>";
                      }else{
                         var $status  = "<span><a href='Po/show/"+PM[i].IDPO+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='Po/printpo/"+PM[i].IDPO+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>";
                      }

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td style='text-align:right;'>"+PM[i].Qty+"</td>"+
                      "<td style='text-align:right;'>Rp. "+rupiah(PM[i].Harga*PM[i].Qty)+"</td>"+
                      "<td>"+PM[i].Batal+"</td>"+
                      "<td>"+PM[i].approve+"</td>"+
                      "<td style='text-align:center;'>"+$status+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }

                     }
                }
            });
    }