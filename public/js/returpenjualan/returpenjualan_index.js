var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/print/'+row.IDRP+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDRP+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';
    // button += '<span><a title="Edit" href="' + url + '/'+row.IDRP+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
    if (row.Batal == 1) {
        // button += '<span><a title="Aktif" onclick="deleteData(\' '+row.IDRP+' \', \' ' + url + '/'+row.IDRP+' \', refresh, \'Apakah anda yakin mengaktifkan retur penjualan ?\')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    } else {
        button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.IDRP+' \', \' ' + url + '/'+row.IDRP+' \', refresh, \'Apakah anda yakin membatalkan retur penjualan ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    }

    return button;
}

function renderGrandTotal(data, type, row) {
    return FormatCurrency(data);
}

function setTable() {
    var colDef = [
        {data: 'Nomor', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nama'},
        {data: 'Total_qty', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Grand_total', className: 'dt-body-right', render: renderGrandTotal},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        query : $('#keyword').val(),
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'], [2, 'desc']];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over this page
        pageTotalQty = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 4 ).footer() ).html(
            FormatCurrency(pageTotalQty)
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })
});