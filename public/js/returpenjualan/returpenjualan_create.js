var tableData;
var nomor_list = 1;

function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 2,
        ajax: {
            url: urlBarang,
            data: function (params) {
              return {
                q: params.term, // search term
                IDGroupCustomer: $('#IDCustomer option:selected').data('group'),
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan : item.Satuan,
                            harga_jual : item.Harga_Jual,
                            item : item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function setSelect2Selected() {
    $('.select-barang').on('select2:select', function(e) {
        var result = e.params.data;
        
        var row = $(this).closest('tr');
        row.find('.price').val(FormatCurrency(result.item.Harga_Jual)).trigger('keyup');
        row.find('.qty').val(1).trigger('keyup');
        row.find('.satuan').val(result.item.Satuan);
        row.find('.idsatuan').val(result.item.IDSatuan);
    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function() {
        var thisQty = ( $(this).val().length > 0) ? parseFloat($(this).val()) : 0 ;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(qtyTotal);
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function() {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0 ;
        subTotal += thisSubTotal;
    });

    $('#Grand_total').val(FormatCurrency(subTotal));
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item barang!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').remove();
                setQtyTotal();
                setGrandTotal();
            }
        });
    });
}

function addRow() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    var html = '';

    html += '<tr id="rowvar'+nomor_list+'" style="width:100%">';
    html += '<td> <select name="IDBarang[]" class="form-control select-barang" style="width: 100%"> </select> </td>';
    html += '<td>-</td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    html += '<td><input placeholder="" type="text" name="Harga[]" class="form-control price"></td>';
    html += '<td><input placeholder="" type="text" name="Satuan[]" class="form-control satuan"  readonly><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"></td>';
    html += '<td><input placeholder="" type="text" name="PPN[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal" readonly></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-barang').append(html);
    setSelect2Barang();
    
    $('.qty').on('keyup', function() {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty      = (! isNaN(row.find('.qty').val())) ?  parseFloat(row.find('.qty').val()) : 0;
        var rowPrice    = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.')) : 0;
        var rowPpn      = parseFloat(row.find('.ppn').val());
        var rowSubTotal = (rowQty * rowPrice) + ( ( rowPpn / 100 ) * rowQty * rowPrice );

        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });
    
    $('.subtotal').on('keyup', function() {
        setGrandTotal();
    });

    deleteRow();
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="'+row.IDBarang+'">';
    html += '<input type="hidden" name="Qty_jual[]" value="'+row.Qty+'">';
    html += '<input type="hidden" name="Harga[]" value="'+row.Harga+'">';
    html += '<input type="hidden" name="Sub_total[]" value="'+row.Sub_total+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    return html;
}

function renderFormRetur(data, type, row) {
    var html = '';

    html += '<input type="text" name="Qty[]" class="form-control form-with qty" style="width: 60px;"> ';

    html += '<br> <span> </span>'

    $('.qty').on('keyup', function() {
        var qtyTotal = 0;
        var grandTotal = 0;
        var DPP = 0;
        var PPN = 0;
        $('.qty').each(function() {
            var row = $(this).closest('tr');
            var thisVal = (($(this).val()).length > 0) ? parseInt($(this).val()) : 0;

            var thisQtyFJ = parseFloat(row.find('input[type=hidden][name*=Qty_jual]').val());
            var thisSpan = row.find('span');
            if (thisVal > thisQtyFJ) {
                thisSpan.addClass('text-danger');
                thisSpan.html('Qty tidak boleh qty penjualan.');
            } else {
                thisSpan.removeClass('text-danger');
                thisSpan.html('');
            }

            var thisPrice = parseFloat(row.find('input[type=hidden][name*=Harga]').val());
            
            var thisTotal = thisVal * thisPrice;

            row.find('input[type=hidden][name*=Sub_total]').val(thisTotal);
            qtyTotal += thisVal;

            if ($('input[type=radio][name=Status_ppn][value=exclude]').is(':checked')) {
                grandTotal += thisTotal + ((10/100) * thisTotal);

                var thisDPP = parseFloat(thisTotal);
                var thisPPN = ((10 / 100) * thisDPP );

                PPN += thisPPN;
                DPP += thisDPP;
            } else {
                grandTotal += thisTotal;
                var thisDPP = parseFloat(thisTotal);
                
                DPP += thisDPP;
            }
        });

        if ($('input[type=radio][name=Status_ppn][value=exclude]').is(':checked')) {
            $('#Grand_total').val(CurrencyFormat(grandTotal));
            $('#DPP').val(CurrencyFormat(DPP));
            $('#PPN').val(CurrencyFormat(PPN));
        } else {
            $('#Grand_total').val(CurrencyFormat(grandTotal));
            $('#DPP').val(CurrencyFormat(parseFloat(DPP) + parseFloat(PPN)));
            $('#PPN').val(CurrencyFormat(PPN));
        }

        $('#Total_qty').val(qtyTotal);
    });
    return html;
}

function renderAction(data, type, row) {
    return '<i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i>';
}

function renderStrip(data) {
    return '-';
}

function renderSubTotal(data) {
    return CurrencyFormat(data);
}

function setTableBarang() {
    var colDef = [
        {data: 'Nama_Barang', name: 'Nama_Barang', render: renderForm},
        {data: 'Qty', className: 'dt-body-right'},
        {data: 'Qty', className: 'dt-body-right', render: renderFormRetur},
        {data: 'Harga', className: 'dt-body-right', render: renderSubTotal},
        {data: 'Satuan', name: 'Satuan', className: 'dt-body-center'},
        {data: 'Sub_total', className: 'dt-body-right', render: renderStrip},
        {data: 'Sub_total', className: 'dt-body-right', render: renderSubTotal},
        {data: 'IDFJDetail', render: renderAction},
    ];
    var reqData = {
        IDFJ : $('#IDFJ').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlPenjualanDetail, colDef, reqData, reqOrder );
}

function getNumberInvoice() {
    // var reqData = {
    //     'Status_ppn' : $('input[type=radio]:checked').val()
    // };
        var selectedOption = $("input:radio[name=ppn]:checked").val();
        if (selectedOption==0){
            ambilnomorjppn();
        }else{
            ambilnomorrjnonppn();
        }

    //ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

$(document).ready(function() {
    $('.select2').select2();

    $('.datepicker').each(function(){
        $(this).datepicker({
            format : 'dd/mm/yyyy',
            autoclose : true
        });
    });

    addRow()
    getNumberInvoice();

    $('#btn-add-barang').on('click', function() {
        addRow();        
    })

    $('input[type=radio][name=Status_ppn]').on('click', function() {
        if ($(this).val() == 'include') {
            $('.ppn').val(0);
        } else {
            $('.ppn').val(10);
        }
        
        getNumberInvoice();
        $('.price').trigger('keyup');
    });

    $('input[type=checkbox][name=Pilih_invoice]').on('click', function() {
        if ($(this).is(':checked')) {
            $('#btn-add-barang').hide();
            $('#IDFJ').prop('disabled', false);
        } else {
            $('#IDFJ').prop('disabled', true);
            $('#btn-add-barang').show();
        }
    });

    $('#IDFJ').select2({
        minimumInputLength: 2,
        ajax: {
            url: urlPenjualanCustomer,
            data: function (params) {
              return {
                q: params.term, // search term
                IDCustomer: $('#IDCustomer').val(),
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Nomor,
                            id: item.IDFJ,
                            data: item
                        }
                    })
                };
            }
        }
    });

    $('#IDFJ').on('select2:select', function(e) {
        var result = e.params.data;
        
        $('input[type=radio][name=Status_ppn][value='+result.data.Status_ppn+']').prop('checked', true).trigger('click');
    });

    $('#IDFJ').on('change', function() {
        if($(this).val() == ''){
            $('#btn-add-barang').show();
            $('#Status_ppn').val('');
            $('#IDCustomer').val('');
            $('#Total_qty').val('');
            
            setTableBarang();
        } else {
            $('#btn-add-barang').hide();
            var reqData = {
                'id'    : $(this).val()
            }
            ajaxData(urlPenjualan, reqData, setTableBarang, false, false);
        }
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})