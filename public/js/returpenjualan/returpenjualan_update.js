var tableData;

function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY"); 
    var dateObject = dateMomentObject.toDate(); 

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');
    
    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY"); 
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        value : dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);
    
    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn' : $('#Status_ppn').val()
    };

    ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="'+row.IDBarang+'">';
    html += '<input type="hidden" name="Qty[]" value="'+row.Qty+'">';
    html += '<input type="hidden" name="Harga[]" value="'+row.Harga+'">';
    html += '<input type="hidden" name="Sub_total[]" value="'+row.Sub_total+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    return html;
}

function setTableBarang() {
    var colDef = [
        {data: 'IDFJDetail', render: renderNumRow },
        {data: 'Nama_Barang', render: renderForm},
        {data: 'qty', className: 'dt-body-right'},
        {data: 'Harga', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Satuan', name: 'tbl_satuan.Satuan', className: 'dt-body-center'},
        {data: 'Sub_total', className: 'dt-body-right', render: FormatCurrency}
    ];
    var reqData = {
        idsjc : $('#IDSJC').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlSuratJalanDetail, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    $('.select2').select2();

    toggleForm('#form-data', false);

    $('.datepicker').each(function(){
        $(this).datepicker({
            format : 'dd/mm/yyyy',
            autoclose : true
        });
    });

    $('#Jatuh_tempo').on('keyup', function() {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY"); 
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#Tanggal_jatuh_tempo').trigger('change');

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})