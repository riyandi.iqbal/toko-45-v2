function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.ID);
}

$(document).ready(function() {
    $('#btn-save').on('click', function() {
        if ( ! $("input[type=radio][name=Status_so]").is(':checked') ) {
            alert('Pilih salah satu So Hangus atau SO Lanjut.');
        } else {
            var reqData = {
                IDSJ        : $('#idsjc').val(),
                Status_so : $('input[name=Status_so]:checked').val()
            }

            ajaxData(urlSetSO, reqData, refresh)
        }
    })
})