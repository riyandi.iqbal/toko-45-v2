var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/print/'+row.IDFJ+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDFJ+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';
    button += '<span><a title="Edit" href="' + url + '/'+row.IDFJ+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
    if (row.Batal == 1) {
        button += '<span><a title="Aktif" onclick="deleteData(\' '+row.IDFJ+' \', \' ' + url + '/'+row.IDFJ+' \', refresh, \'Apakah anda yakin mengaktifkan sales order ?\')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    } else {
        button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.IDFJ+' \', \' ' + url + '/'+row.IDFJ+' \', refresh, \'Apakah anda yakin membatalkan sales order ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'nomor', render: renderNumRow },
        {data: 'tanggal', render: renderDate},
        {data: 'nomor'},
        {data: 'Nama'},
        {data: 'Nama_Barang'},
        {data: 'qty', className: 'dt-body-right'},
        {data: 'hargasatuan', className: 'dt-body-right', render: FormatCurrency},
        {data: 'total', className: 'dt-body-right', render: FormatCurrency},
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        query : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })
});