var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/print/'+row.idsjc+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.idsjc+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';

    if (row.IDFJ == null && row.Batal == 0) {
        button += '<span><a title="Edit" href="' + url + '/'+row.idsjc+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
        button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.idsjc+' \', \' ' + url + '/'+row.idsjc+' \', refresh, \'Apakah anda yakin membatalkan surat jalan ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'nomor', render: renderNumRow },
        {data: 'tanggal', render: renderDate},
        {data: 'nomor'},
        {data: 'nomorso'},
        {data: 'Nama'},
        {data: 'qty', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        query : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        totalQty = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotalQty = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 5 ).footer() ).html(
            FormatCurrency(pageTotalQty) +' ( '+ FormatCurrency(totalQty) +' )'
        );

    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback );
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="0">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});