var tableData;
var Total_qty, Sub_total;

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);

    updateQty();
}

function updateQty() {
    Total_qty = 0;
    Sub_total = 0;

    $('.qty').each(function() {
        var row = $(this).closest('tr');

        var qty = $(this).val();
        var harga = row.find('.harga').val();

        var subtotal = parseInt(qty) * parseFloat(harga);

        row.find('.subtotal').val(subtotal);

        Sub_total += parseFloat(subtotal);
        Total_qty += parseInt(qty);
    });

    $('#Total_qty').val(Total_qty);

    if ($('#Status_ppn').val() == 'include') {
        DPP_Baru = Sub_total / 1.1;
        PPN_Baru = Sub_total - DPP_Baru;
    } else {
        DPP_Baru = Sub_total;
        PPN_Baru = 0.1 * DPP_Baru;
    }

    $('#DPP').val(CurrencyFormat(parseFloat(DPP_Baru)));
    $('#PPN').val(CurrencyFormat(PPN_Baru));

    setTotalInvoice();
}

function refresh(result) {
    if (result.data.confirm == true) {
        alertSuccess(result.message, url + '/confirm/' + result.data.ID);
    } else {
        alertSuccess(result.message, url + '/detail/' + result.data.ID);
    }
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY"); 
    var dateObject = dateMomentObject.toDate(); 

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');
    
    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY"); 
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        value : dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);
    
    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setTotalInvoice() {
    var addTotal = 0;
    var minTotal = 0;

    $('.total-add').each(function() {
        var thisVal = parseFloat($(this).val().replace(/\./g,'').replace(',', '.'));
        console.log(thisVal);
        addTotal += thisVal;
    });

    $('.total-min').each(function() {
        var thisVal = $(this).val();
        minTotal += thisVal;
    });

    var Grand_total = parseFloat(addTotal) - parseFloat(minTotal);
    $('#total_invoice_diskon').val(CurrencyFormat(Grand_total));
}

function setFormData(result) {
    $('#Status_ppn').val(result.Status_ppn);
    $('#NamaCustomer').val(result.Nama);
    $('#IDCustomer').val(result.IDCustomer);
    $('#Total_qty').val(result.Saldo_qty);
    $('#DPP').val(CurrencyFormat(result.DPP));
    $('#PPN').val(CurrencyFormat(result.PPN));
            
    $('#Jatuh_tempo').val(75).trigger('keyup');

    getNumberInvoice();
    setTableBarang();
    setTotalInvoice();
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn' : $('#Status_ppn').val()
    };
    console.log(reqData);
    ambilnomorsj();
    //ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function renderForm(data, type, row) {
    var html = '';
    
    html += '<input type="hidden" name="IDSOKDetail[]" value="'+row.IDSOKDetail+'">';
    html += '<input type="hidden" name="IDBarang[]" value="'+row.IDBarang+'">';
    html += '<input type="hidden" name="Qty[]" value="'+row.Saldo_qty+'">';
    html += '<input type="hidden" name="Harga[]" class="harga" value="'+row.Harga+'">';
    html += '<input type="hidden" name="Sub_total[]" class="subtotal" value="'+row.Sub_total+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    html += '<input type="text" name="Qty_kirim[]" value="'+row.Saldo_qty+'" class="qty form-control">';

    $('.qty').on('keyup', function() {
        updateQty();
    })

    $('.qty').trigger('keyup');

    return html;
}

function renderAction(data, type, row, meta) {
    var button = '';
    
    button += '<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(this)"> <span class="fa fa-trash"></span></button>';

    return button;
}

function setTableBarang() {
    var colDef = [
        {data: 'IDSOKDetail', render: renderNumRow },
        {data: 'Nama_Barang', name: 'tbl_barang.Nama_Barang'},
        {data: 'Saldo_qty', className: 'dt-body-right'},
        {data: 'Saldo_qty', className: 'dt-body-right', render: renderForm},
        {data: 'Satuan', name: 'tbl_satuan.Satuan', className: 'dt-body-center'},
        {render: renderAction},
        //{data: 'Sub_total', className: 'dt-body-right', render: FormatCurrency}
    ];
    var reqData = {
        IDSOK : $('#ID').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlSalesOrderDetail, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    $('.select2').select2();

    $('.datepicker').each(function(){
        $(this).datepicker({
            format : 'dd/mm/yyyy',
            autoclose : true
        });
    });

    setTableBarang();
    getNumberInvoice();

    $('#ID').on('change', function() {
        if($(this).val() == ''){
            $('#Status_ppn').val('');
            $('#IDCustomer').val('');
            $('#Total_qty').val('');
            $('#DPP').val(CurrencyFormat(''));
            $('#PPN').val(CurrencyFormat(''));
            
            setTableBarang();
        } else {
            var reqData = {
                'id'    : $(this).val()
            }
            ajaxData(urlSalesOrder, reqData, setFormData, false, false)
        }
    });

    $('#Jatuh_tempo').on('keyup', function() {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY"); 
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            /* Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required" */
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
    $('#myTable').on( 'click', 'tbody tr', function () {
        tableData.row( this ).delete();
    } );
})