var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/'+row.IDCoa+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDCoa+' \', \' ' + url + '/'+row.IDCoa+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';

    return button;
}

function setTable() {
    var colDef = [
        {data: 'Kode_COA', render: renderNumRow },
        {data: 'Kode_COA'},
        {data: 'Nama_COA'},
        {data: 'Nama_Group', name: 'tbl_group_coa.Nama_Group', className: 'dt-body-center'},
        {data: 'Status', className: 'dt-body-center'},
        {data: 'Normal_Balance',  name: 'Normal_Balance', className: 'dt-body-center'},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'Kode_COA', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'asc']];

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});