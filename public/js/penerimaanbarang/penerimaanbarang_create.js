var tableData;
var Total_qty, Sub_total;

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);

    updateQty();
}

function updateQty() {
    Total_qty = 0;
    Sub_total = 0;

    $('.qty').each(function() {
        var row = $(this).closest('tr');

        var qty = $(this).val();
        var harga = row.find('.harga').val();

        var subtotal = parseInt(qty) * parseFloat(harga);

        row.find('.subtotal').val(subtotal);

        Sub_total += parseFloat(subtotal);
        Total_qty += parseInt(qty);
    });

    $('#Total_qty').val(Total_qty);

    $('#Total_harga').val(CurrencyFormat(parseFloat(Sub_total)));
}

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.ID);
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY"); 
    var dateObject = dateMomentObject.toDate(); 

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');
    
    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY"); 
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        value : dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);
    
    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setTotalInvoice() {
    var addTotal = 0;
    var minTotal = 0;

    $('.total-add').each(function() {
        var thisVal = parseFloat($(this).val().replace(/\./g,'').replace(',', '.'));
        console.log(thisVal);
        addTotal += thisVal;
    });

    $('.total-min').each(function() {
        var thisVal = $(this).val();
        minTotal += thisVal;
    });

    var Grand_total = parseFloat(addTotal) - parseFloat(minTotal);
    $('#Total_harga').val(CurrencyFormat(Grand_total));
}

function setFormData(result) {
    $('#Status_ppn').val(result.pilihanppn);
    $('#NamaSupplier').val(result.Nama);
    $('#IDSupplier').val(result.IDSupplier);
    $('#Total_qty').val(result.Saldo);
    $('#Total_harga').val(CurrencyFormat(result.Grand_total));
            
    $('#Jatuh_tempo').val(75).trigger('keyup');

    getNumberInvoice();
    setTableBarang();
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn' : $('#Status_ppn').val()
    };
    ambilnomorpb();
}

function renderForm(data, type, row) {
    var html = '';
    
    html += '<input type="hidden" name="IDPODetail[]" value="'+row.IDPODetail+'">';
    html += '<input type="hidden" name="IDBarang[]" value="'+row.IDBarang+'">';
    html += '<input type="hidden" name="Qty[]" value="'+row.Qty_terima+'">';
    html += '<input type="hidden" name="Harga[]" class="harga" value="'+row.Harga+'">';
    html += '<input type="hidden" name="Sub_total[]" class="subtotal" value="'+row.Saldo+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    html += '<input type="text" name="Qty_kirim[]" value="'+row.Qty_terima+'" class="qty form-control">';
    html += '<br> <span class="valid-qty"> </span>'

    $('.qty').on('keyup', function() {
        updateQty();
        $('.qty').each(function() {
            var row = $(this).closest('tr');
            var thisVal = (($(this).val()).length > 0) ? parseInt($(this).val()) : 0;

            var thisQtyFJ = parseFloat(row.find('input[type=hidden][name*=Qty]').val());
            var thisSpan = row.find('.valid-qty');
            if (thisVal > thisQtyFJ) {
                thisSpan.addClass('text-danger');
                thisSpan.html('Qty tidak boleh melebih qty pesanan.');
                $('button[name=simpan]').prop('disabled', true);
            } else {
                thisSpan.removeClass('text-danger');
                thisSpan.html('');
                $('button[name=simpan]').prop('disabled', false);
            }
        });
    })

    $('.qty').trigger('keyup');
    $('input').prop('autocomplete', 'off');

    return html;
}

function renderAction(data, type, row, meta) {
    var button = '';
    
    button += '<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(this)"> <span class="fa fa-trash"></span></button>';

    return button;
}

function setTableBarang() {
    var colDef = [
        {data: 'IDPODetail', render: renderNumRow },
        {data: 'Nama_Barang', name: 'tbl_barang.Nama_Barang'},
        {data: 'Qty_terima', className: 'dt-body-right'},
        {data: 'Qty_terima', className: 'dt-body-right', render: renderForm},
        {data: 'Satuan', name: 'tbl_satuan.Satuan', className: 'dt-body-center'},
        {render: renderAction},
    ];
    var reqData = {
        IDPO : $('#IDPO').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlPurchaseOrderDetail, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    $('.select2').select2();

    $('.datepicker').each(function(){
        $(this).datepicker({
            format : 'dd/mm/yyyy',
            autoclose : true
        });
    });

    setTableBarang();

    $('#IDPO').on('change', function() {
        if($(this).val() == ''){
            $('#Status_ppn').val('');
            $('#IDSupplier').val('');
            $('#Total_qty').val('');
            $('#DPP').val(CurrencyFormat(''));
            $('#PPN').val(CurrencyFormat(''));
            
            setTableBarang();
        } else {
            var reqData = {
                'id'    : $(this).val()
            }
            ajaxData(urlPurchaseOrder, reqData, setFormData, false, false)
        }
    });

    $('#Jatuh_tempo').on('keyup', function() {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY"); 
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            /* Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required" */
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
    $('#myTable').on( 'click', 'tbody tr', function () {
        tableData.row( this ).delete();
    } );
})