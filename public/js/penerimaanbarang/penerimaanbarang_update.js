var tableData;

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.ID);
}

function deleteRow(btn) {
    var row = btn.parentNode.parentNode;
    row.parentNode.removeChild(row);

    updateQty();
}

function updateQty() {
    Total_qty = 0;
    Sub_total = 0;

    $('.qty').each(function() {
        var row = $(this).closest('tr');

        var qty = $(this).val();
        var harga = row.find('.harga').val();

        var subtotal = parseInt(qty) * parseFloat(harga);

        row.find('.subtotal').val(subtotal);

        Sub_total += parseFloat(subtotal);
        Total_qty += parseInt(qty);
    });

    $('#Total_qty').val(Total_qty);

    $('#Total_harga').val(CurrencyFormat(parseFloat(Sub_total)));
}

$(document).ready(function() {
    $('.select2').select2();
    if (Status_pakai == 1) {
        toggleForm('#form-data', true);
    } else {
        toggleForm('#form-data', false);
    }

    $('.qty').on('keyup', function() {
        updateQty();
        $('.qty').each(function() {
            var row = $(this).closest('tr');
            var thisVal = (($(this).val()).length > 0) ? parseInt($(this).val()) : 0;

            var thisQtyFJ = parseFloat(row.find('input[type=hidden][name*=Qty]').val());
            var thisSpan = row.find('.valid-qty');
            if (thisVal > thisQtyFJ) {
                thisSpan.addClass('text-danger');
                thisSpan.html('Qty tidak boleh melebih qty pesanan.');
                $('button[name=simpan]').prop('disabled', true);
            } else {
                thisSpan.removeClass('text-danger');
                thisSpan.html('');
                $('button[name=simpan]').prop('disabled', false);
            }
        });
    })

    $('.qty').trigger('keyup');

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})