var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0 || data == 'aktif' || data == 'Aktif') {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderPersetujuan(data, type, row) {
    var html = '';
    if (data == 'Disetujui') {
        html += 'Disetujui';
    } else if (data == 'Pending') {
        html += 'Ditolak';
    } else {
        html += 'Menunggu Persetujuan';
    }

    return html;
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/print/'+row.IDTBS+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDTBS+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';
    if (row.Status_pakai < 1) {
        if (row.Batal == 'Aktif') {
            button += '<span><a title="Edit" href="' + url + '/'+row.IDTBS+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
        } else {
            button += '<span><a style="cursor: no-drop;" title="LOCK Edit Data">&nbsp;<i class="fa fa-pencil fa-lg" style="color: #999"></i></a></span>';
        } 
        if (row.Batal == 'Aktif') {
            button += '<span><a title="Batalkan" onclick="deleteData(\' '+row.IDTBS+' \', \' ' + url + '/hapus_data/'+row.IDTBS+' \', refresh, \'Apakah anda yakin membatalkan ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
        } else {
            button += '<span><a style="cursor: no-drop;">&nbsp;<i class="fa fa-check fa-lg" style="color: #999" title="Sudah Dibatalkan"></i></a></span>';
        } 
    }

    return button;
}

function renderGrandTotal(data, type, row) {
    return row.Kode + FormatCurrency(data);
}

function setTable() {
    var colDef = [        
        {data: 'IDTBS', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nomor_po'},
        {data: 'Nama'},
        {data: 'Total_qty_yard', className: 'dt-body-center', render: FormatCurrency},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(),
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'], [2, 'desc'] ];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over this page
        pageTotalQty = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 5 ).footer() ).html(
            FormatCurrency(pageTotalQty)
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="aktif">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'tbl_terima_barang_supplier.Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});