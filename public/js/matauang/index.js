var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/edit/'+row.IDMataUang+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    if (row.Aktif == 'aktif') {
        button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDMataUang+' \', \' ' + url + '/editstatus/'+row.IDMataUang+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } 

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDMataUang', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Mata_uang'},
        {data: 'Negara'},
        {data: 'Kurs', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'IDMataUang', className: 'dt-body-center', render: renderAction}
    ];
    
    var reqData = {
        field : 'Mata_uang',
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[5, 'asc'], [1, 'desc']];

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        if ($(this).val() == 'tbl_barang.IDGroupBarang') {
            $('#pilihan').hide();
            $('#select').show();
            $('#keyword_chosen').removeAttr('style');
        } else {
          $('#pilihan').show();
            $('#select').hide();
        }
  })

});