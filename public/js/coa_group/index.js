var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/'+row.IDGroupCOA+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDGroupCOA+' \', \' ' + url + '/'+row.IDGroupCOA+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';

    return button;
}

function setTable() {
    var colDef = [
        {data: 'Kode_Group_COA', render: renderNumRow },
        {data: 'Kode_Group_COA'},
        {data: 'Nama_Group'},
        {data: 'Normal_Balance'},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'asc']];

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});