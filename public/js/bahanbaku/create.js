function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function setKodeBarang() {
    if ($('#IDGroupBarang').val() == 'P000004') {
        var Pemisah_Kode = '-';
        var Kode_Tipe = $('#IDTipe option:selected').data('kode') + Pemisah_Kode;
        $('#Kode_Barang').val(Kode_Tipe);
    } else {
        var Pemisah_Kode = $('#IDGroupBarang option:selected').data('pemisah_kode');

        if (format_kode != false) {
            if (format_kode[0] == 'NAMA') {
                Kode_Group = $('#IDGroupBarang option:selected').data('namagroup') + Pemisah_Kode;
            } else {
                Kode_Group = $('#IDGroupBarang option:selected').data('kode') + Pemisah_Kode;
            }
            var Kode_Tipe = (format_kode[2] != '') ? ($('#IDTipe option').is(':selected')) ? $('#IDTipe option:selected').data('kode') + Pemisah_Kode : '[TIPE]' : '';
            var Kode_Ukuran = (format_kode[3] != '') ? $('#IDUkuran option:selected').data('kode') + Pemisah_Kode : ($('.ukuran #IDUkuran').is(':visible')) ? '[Ukuran]' : '';

            var Kode_Kategori = '';
            if ($('#IDGroupBarang').val() != 'P000005') {
                Kode_Kategori = (format_kode[4] != '') ? $('#IDKategori option:selected').data('kode') + Pemisah_Kode : ($('.kategori #IDKategori').is(':visible')) ? '[Kategori]' : '';
            }
            $('#Kode_Barang').val(Kode_Group + Kode_Tipe + Kode_Ukuran + Kode_Kategori);
        } else {
            var Pemisah_Kode = '-';
            var Kode_Group = $('#IDGroupBarang option:selected').data('kode') + Pemisah_Kode;
            var Kode_Tipe = $('#IDTipe option:selected').data('kode') + Pemisah_Kode;
            var Kode_Ukuran = $('#IDUkuran option:selected').data('kode') + Pemisah_Kode;
            var Kode_Kategori = $('#IDKategori option:selected').data('kode') + Pemisah_Kode;
            $('#Kode_Barang').val(Kode_Group + Kode_Tipe + Kode_Ukuran + Kode_Kategori);
        }
    }

    var Kode_exist = $('#Kode_Barang').val();

    $('#Kode_Barang').val(Kode_exist + urutan)
}

function setNamaBarang() {
    if ($('#IDGroupBarang').val() == 'P000004') {
        var Pemisah_Kode = ' ';
        var Kode_Tipe = $('#IDTipe option:selected').data('nama') + Pemisah_Kode;
        var Nama_Ukuran = $('#IDUkuran option:selected').data('kode');
        $('#Nama_Barang').val(Kode_Tipe + Nama_Ukuran);
    } else {
        if (format_nama != false) {
            var Pemisah_Kode = $('#IDGroupBarang option:selected').data('pemisah_nama');
            if (format_nama[0] == 'NAMA') {
                Nama_Group = $('#IDGroupBarang option:selected').data('namagroup') + Pemisah_Kode;
            } else {
                Nama_Group = $('#IDGroupBarang option:selected').data('kode') + Pemisah_Kode;
            }
            var Nama_Tipe = (format_nama[2] != '') ? ($('#IDTipe option').is(':selected')) ? $('#IDTipe option:selected').data('kode') + Pemisah_Kode : '[TIPE]' : '';
            var Nama_Ukuran = (format_nama[3] != '') ? $('#IDUkuran option:selected').data('kode') + Pemisah_Kode : ($('.ukuran #IDUkuran').is(':visible')) ? '[Ukuran]' : '';
            var Nama_Kategori = (format_nama[4] != '') ? $('#IDKategori option:selected').data('kode') + Pemisah_Kode : ($('.kategori #IDKategori').is(':visible')) ? '[Kategori]' + Pemisah_Kode : '';
            
        } else {
            var Pemisah_Kode = '-';
            var Nama_Group = $('#IDGroupBarang option:selected').data('kode') + Pemisah_Kode;
            var Nama_Tipe = $('#IDTipe option:selected').data('kode') + Pemisah_Kode;
            var Nama_Ukuran = $('#IDUkuran option:selected').data('kode') + Pemisah_Kode;
            var Nama_Kategori = $('#IDKategori option:selected').data('kode') + Pemisah_Kode;
        }

        $('#Nama_Barang').val(Nama_Group + Nama_Tipe + Nama_Ukuran + Nama_Kategori);
    }

    var Kode_exist = $('#Nama_Barang').val();

    $('#Nama_Barang').val(Kode_exist)
}

function fillKategori(result) {
    urutan = result.urutan_kode;
    urutan_nama = result.urutan_nama;
    $('#IDKategori').html('');
    
    var html = '<option value="">-- Pilih Kategori --</option>';
    $.each((result.kategori), function(index, val) {
        html +='<option value="'+val.IDKategori+'" data-kode="'+val.Nama_Kategori+'">'+val.Nama_Kategori+' </option>';
    });

    $('#IDKategori').append(html);

    if ($('#IDGroupBarang').val() == 'P000004') {
        setKodeBarang();
        setNamaBarang();
    } else {
        $('#IDTipe').val('').trigger('change');
        $('#IDUkuran').val('').trigger('change');
        $('#IDKategori').val('').trigger('change');

    }
}

$(document).ready(function() {
    $('.select2').select2();

    $('#IDGroupBarang').on('change', function () {
        if ($(this).val() == '') {
            $('#IDKategori').html('<option value="">-- Pilih Kategori --</option>');
        } else {
            $('#Kode_Barang').val('');
            $('#Nama_Barang').val('');

            if ($(this).val() == 'P000004') {
                $('.tipe').show();
                $('.ukuran').show();
                $('.kategori').show();

                $('.berat').show();
            } else {
                $('.berat').hide();
                $('#Kode_Barang').prop('readonly', true);
                $('#Nama_Barang').prop('readonly', true);

                format_kode = JSON.parse(JSON.stringify($('#IDGroupBarang option:selected').data('group')));
                format_nama = JSON.parse(JSON.stringify($('#IDGroupBarang option:selected').data('nama')));

                if (format_nama == false) {
                    $('#Nama_Barang').prop('readonly', false);
                }

                if (format_kode == false) {
                    $('.tipe').hide();
                    $('.ukuran').hide();
                    $('.kategori').hide();

                    $('#Kode_Barang').prop('readonly', false);
                } else {
                    if (format_kode[2] == 'TIPE') {
                        $('.tipe').show();
                    } else {
                        $('.tipe').hide();
                    }

                    if (format_kode[3] == 'UKURAN') {
                        $('.ukuran').show();
                    } else {
                        $('.ukuran').hide();
                    }

                    if (format_kode[4] == 'KATEGORI') {
                        $('.kategori').show();
                    } else {
                        $('.kategori').hide();
                    }
                }

                if ($(this).val() == 'P000005') {
                    $('.kategori').show();
                    
                }
            }

            var reqData = {
                IDGroupBarang : $(this).val()
            };

            ajaxData(urlKategori, reqData, fillKategori, false, false);
        }
    });

    $('#IDTipe').on('change', function() {
        if ($('#IDGroupBarang').val() == 'P000004') {
            var reqData = {
                IDGroupBarang : $('#IDGroupBarang').val(),
                IDTipe : $(this).val()
            };

            ajaxData(urlKategori, reqData, fillKategori, false, false);
        } else {
            if (format_kode != false) {
                setKodeBarang();
            }
    
            if (format_nama != false) {
                setNamaBarang();
            }
        }
    });

    $('#IDUkuran').on('change', function() {
        if ($('#IDGroupBarang').val() == 'P000004') {
            setKodeBarang();
            setNamaBarang();
        } else {
            if (format_kode != false) {
                setKodeBarang();
            }
    
            if (format_nama != false) {
                setNamaBarang();
            }
        }
    });

    $('#IDKategori').on('change', function() {
        if ($('#IDGroupBarang').val() == 'P000004') {
            setKodeBarang();
            setNamaBarang();
        } else {
            if (format_kode != false) {
                setKodeBarang();
            }
    
            if (format_nama != false) {
                setNamaBarang();
            }
        }
    });

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
})