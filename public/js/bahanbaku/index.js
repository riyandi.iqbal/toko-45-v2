var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + url + '/printdata/'+row.IDPBB+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    
    button += '<span><a class="action-icons c-edit" href="'+url+'/produksi_show/'+row.IDPBB+'" title="Show Data"><i class="fa fa-search fa-lg" style="color: green;"></i></a></span>';

    if (row.Status == 'Aktif') {
        button += '<span><a class="action-icons c-edit" href="'+url+'/produksi_edit/'+row.IDPBB+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';
        button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.IDPBB+' \', \' ' + url + '/'+row.IDPBB+' \', refresh, \'Apakah anda yakin membatalkan data ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDPBB', render: renderNumRow },
        {data: 'Tanggal', name: 'tbl_produksibahanbaku.Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nama_Formula'},
        {data: 'Status', className: 'dt-body-center'},
        {data: 'IDPBB', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        if ($(this).val() == 'tbl_barang.IDGroupBarang') {
            $('#pilihan').hide();
            $('#select').show();
            $('#keyword_chosen').removeAttr('style');
        } else {
          $('#pilihan').show();
            $('#select').hide();
        }
  })

});