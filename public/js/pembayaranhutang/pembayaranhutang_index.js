var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0 || data=='aktif') {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDBS+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';
    
    // if (row.Batal == 1) {
    //     button += '<span><a title="Aktif" onclick="deleteData(\' '+row.IDBS+' \', \' ' + url + '/'+row.IDBS+' \', refresh, \'Apakah anda yakin mengaktifkan invoice ?\')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    // } else {
    //     button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.IDBS+' \', \' ' + url + '/'+row.IDBS+' \', refresh, \'Apakah anda yakin membatalkan invoice ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    // }

    return button;
}

function renderGrandTotal(data, type, row) {
    if (row.Kode == null) {
        row.Kode = ''
    }
    
    return row.Kode + FormatCurrency(data);
}

function setTable() {
    var colDef = [
        {data: 'Nomor', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nama'},
        {data: 'Pembayaran', className: 'dt-body-right', render: renderGrandTotal},
        {data: 'Kelebihan_bayar', className: 'dt-body-right', render: renderGrandTotal},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        query : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages
        totalPrice = api
            .column( 5 )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Total over this page
        pageTotalPrice = api
            .column( 5, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 5 ).footer() ).html(
            CurrencyFormat(pageTotalPrice) +' ( '+ CurrencyFormat(totalPrice) +' )'
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback );
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="0">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});