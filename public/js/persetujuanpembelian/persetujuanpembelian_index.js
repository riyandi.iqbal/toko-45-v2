var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0 || data == 'aktif') {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderPersetujuan(data, type, row) {
    var html = '';
    if (data == 'Disetujui') {
        html += 'Disetujui';
    } else if (data == 'Pending') {
        html += 'Menunggu Persetujuan';
    } else {
        html += 'Ditolak';
    }

    return html;
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Print" href="' + urlPo + '/print/'+row.IDPO+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    button += '<span><a title="Detail" href="' + url + '/show/'+row.IDPO+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';

    return button;
}

function renderGrandTotal(data, type, row) {
    return row.Kode + FormatCurrency(data);
}

function setTable() {
    var colDef = [
        {data: 'IDPO', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Nama', name: 'tbl_customer.Nama'},
        {data: 'Total_qty', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Grand_total', className: 'dt-body-right', render: renderGrandTotal},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'approve', className: 'dt-body-center', render: renderPersetujuan},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(),
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[7, 'desc'], [2, 'desc'], [1, 'desc']];

    var reqFooterCallback = function ( row, data, start, end, display ) {
        var api = this.api(), data;

        // Remove the formatting to get integer data for summation
        var intVal = function ( i ) {
            return typeof i === 'string' ?
                i.replace(/\./g,'').replace(',', '.')*1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over this page
        pageTotalQty = api
            .column( 4, { page: 'current'} )
            .data()
            .reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );

        // Update footer
        $( api.column( 4 ).footer() ).html(
            FormatCurrency(pageTotalQty)
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="aktif">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'tbl_purchase_order.Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});