var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    // button += '<span><a title="Print" href="' + url + '/print/'+row.IDJU+'" target="_blank"><i class="fa fa-print fa-lg" style="color: grey;"></i></a></span>';
    
    button += '<span><a title="Detail" href="' + url + '/detail/'+row.IDJU+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';

    if (row.Batal == 0) {
        button += '<span><a title="Edit" href="' + url + '/'+row.IDJU+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
    }

    if (row.Batal == 0) {
        button += '<span><a title="Batal" onclick="deleteData(\' '+row.IDJU+' \', \' ' + url + '/'+row.IDJU+' \', refresh, \'Apakah anda yakin membatalkan transaksi jurnal umum ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } else {
        // button += '<span><a title="Aktif" onclick="deleteData(\' '+row.IDJU+' \', \' ' + url + '/'+row.IDJU+' \', refresh, \'Apakah anda yakin mengaktifkan transaksi jurnal umum ?\')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    }

    return button;
}

function setTable() {
    var colDef = [
        {render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nomor'},
        {data: 'Total_debit', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Total_kredit', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Batal', className: 'dt-body-center', render: renderStatus},
        {data: 'Nomor', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        tanggal_awal : $('#date_from').val(), 
        tanggal_akhir : $('#date_until').val(), 
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    tableData = setDataTable('#table-data', urlDataTable, colDef, reqData, reqOrder);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="0">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});