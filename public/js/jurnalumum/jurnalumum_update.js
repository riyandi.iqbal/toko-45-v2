var dataDetail = [];

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.id);
}

function deleteRow(row) {
    dataDetail.splice(row, 1);
    
    setTableDetail();
}

function renderAction(data, type, row, meta) {
    var button = '';
    
    button += '<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(\''+meta.row+'\')"> <span class="fa fa-trash"></span></button>';

    return button;
}

function renderPerkiraan(data, type, row) {
    return row.IDCOA + ' - ' + row.Nama_Coa;
}

function setTableDetail() {
    tablePembayaran = $('#table-detail').dataTable({
        data : dataDetail,
        columns: [
            { render : renderNumRow},
            { data : "IDCOA", render: renderPerkiraan },
            { data : "Keterangan" },
            { data : "Debet", className: 'dt-body-right', render: FormatCurrency },
            { data : "Kredit", className: 'dt-body-right', render: FormatCurrency },
            { render : renderAction }
        ],
        destroy : true,
        processing : true,
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/\./g,'').replace(',', '.')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                FormatCurrency(pageTotal) +' ( '+ FormatCurrency(total) +' )'
            );

            // Total over all pages
            totalKredit = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotalKredit = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                FormatCurrency(pageTotalKredit) +' ( '+ FormatCurrency(totalKredit) +' )'
            );
        }
    });
}

$(document).ready(function() {
    $('.select2').select2();
    $('.kredit').hide();

    toggleForm('#form-data', false);

    $('#Posisi_header').on('change', function() {
        if ($(this).val() == 'debet') {
            $('.kredit').show();
            $('.debet').hide();
            $('#Posisi').val('kredit');
        } else {
            $('.kredit').hide();
            $('.debet').show();
            $('#Posisi').val('debet');
        }
    });

    $('#IDMataUang').on('change', function() {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    });

    $('#table-detail').DataTable();

    $('#btn-add-detail').on('click', function() {
        $('.form-detail').each(function() {
            var id = $(this).attr('id');
            if ($(this).val() == '') {
                $('label[id="'+id+'-error"]').remove();
            }
        });

        if ($('#IDCOA').val().length < 1 || $('#IDMataUang').val().length < 1 || $('#Keterangan').val().length < 1 || ( $('#Debet').val().length < 1 && $('#Kredit').val().length < 1 )) {
            $('.form-detail').each(function() {
                var id = $(this).attr('id');
                if ($(this).val() == '') {
                    $('<label id="'+id+'-error" class="error">Harus diisi.</label>').insertAfter($('label[for="'+id+'"]:not([style])'));
                }
            });
        } else {
            data = {
                'IDCOA' : $('#IDCOA').val(),
                'Kode_Coa' : $('#IDCOA option:selected').data('kode'),
                'Nama_Coa' : $('#IDCOA option:selected').data('nama'),
                'IDMataUang'    : $('#IDMataUang').val(),
                'Kurs'    : $('#Kurs').val(),
                'Debet'    : $('#Debet').val().length > 0 ? $('#Debet').val() : 0,
                'Kredit'    : $('#Kredit').val().length > 0 ? $('#Kredit').val() : 0,
                'Posisi'    : $('#Posisi').val(),
                'Keterangan'    : $('#Keterangan').val(),
            }

            dataDetail.push(data);
            
            $('#IDCOA').val('').trigger('change');
            $('#IDMataUang').val('').trigger('change');
            $('#Kurs').val('');
            $('#Debet').val('');
            $('#Kredit').val('');
            $('#Keterangan').val('');

            setTableDetail();
        }
    });

    $('#form-data').validate({
        rules : {
            KodePerkiraan : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);
            reqData.append('Total_debit', total);
            reqData.append('Total_kredit', totalKredit);
            reqData.append('data_detail', JSON.stringify(dataDetail));

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
});