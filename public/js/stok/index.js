var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/'+row.IDUkuran+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    if (row.Status == 'aktif') {
        button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDUkuran+' \', \' ' + url + '/'+row.IDUkuran+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';
    } else {
        button += '<span><a title="Aktifkan" onclick="deleteData(\' '+row.IDUkuran+' \', \' ' + url + '/'+row.IDUkuran+' \', refresh, \' Apakah anda akan mengaktifkan kembali ? \')"><i class="fa fa-check fa-lg"  style="color: green"></i></a></span>';
    }

    return button;
}

function renderLink(data, type, row) {
    return '<a href="'+url+'/kartustok/'+row.IDStok+'"> '+data+' </a>';
}

function setTable() {
    var colDef = [
        {data: 'IDStok', render: renderNumRow },
        {data: 'Nama_Gudang'},
        {data: 'Nama_Barang', render: renderLink},
        {data: 'Satuan'},
        {data: 'Qty_pcs', className: 'dt-body-right', render: FormatCurrency},
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});