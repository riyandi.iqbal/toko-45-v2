function field2()
	{
		var data = {
          "IDPerusahaan"    :$('#IDPerusahaan').val(),
          "Nama"            :$('#nama').val(),
          "Alamat"          :$('#alamat').val(),
          "Kecamatan"      	:$('#kec').val(),
          "Kota"     	      :$("#IDKota").val(),   
          "Telp"            :$("#tel").val(), 
          "Fax"             :$("#fax").val(), 
          "Kontak"          :$("#kontak").val(), 
          "Email"           :$("#email").val(),
          "Font"            :$("#font1").val(), 
          "FontSize"        :$("#font2size").val(),
          "FontStyle"       :$("#font3style").val(),
          "Angkakoma"       :$("#angkakoma").val(),

        }
        return data;
    }
function fieldskode()
{
  var data2 = {
      "IDMenu"              :$('#idmenu').val(),
      "Nama"                :$('#namakode').val(),
  }
  return data2;
}
function simpandata()
{
    $("#modal-loading").fadeIn();
    console.log('LAGI PUSING');
    var data = field2();
    console.log(data);
    console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "ControlPanel/simpandataperusahaan",
                  type: "POST",
                  data:{"_token": $('#token').val(), data1:data},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    $("#modal-loading").fadeOut();
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Edit Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "ControlPanel";
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Di Edit', 'warning');
                 }
               });
}

function simpankodetransaksi()
{
  $("#modal-loading").fadeIn();
  var data = fieldskode();
    console.log(data);
    console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "ControlPanel/simpankodetransaksi",
                  type: "POST",
                  data:{"_token": $('#token').val(), data1:data},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    console.log(data);
                    $("#modal-loading").fadeOut();
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Simpan Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "ControlPanel";
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Di Simpan', 'warning');
                 }
               });
}