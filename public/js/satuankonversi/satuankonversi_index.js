var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Edit" href="' + url + '/'+row.IDBarang+'"><i class="fa fa-pencil fa-lg"></i></a></span>';
    // button += '<span><a title="Tidak Aktif" onclick="deleteData(\' '+row.IDBarang+' \', \' ' + url + '/'+row.IDBarang+' \', refresh, \'Apakah anda yakin menghapus satuan konversi ?\')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';


    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDBarang', render: renderNumRow },
        {data: 'Nama_Barang'},
        {data: 'IDBarang', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = [[1, 'desc'],[2, 'desc']];

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, );
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    })

    $('#field').on('change', function() {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">'+
        '<option value="">- Pilih -</option>'+
        '<option value="0">Aktif</option>'+
        '<option value="1">Dibatalkan</option>'+
        '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }
        
    })
});