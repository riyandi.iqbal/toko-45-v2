function refresh(result) {
    alertSuccess(result.message, url + '/' + IDBarang);
}

$(document).ready(function() {
  $('.select2').select2();

  $('.edit').on('click', function() {
    var row = $(this).closest('tr');
    
    var Qty = row.find('.Qty').val();
    var IDSatuanBesar = row.find('.IDSatuanBesar').val();
    var IDSatuanKonversi = row.find('.IDSatuanKonversi').val();

    $('#IDSatuanKonversi').val(IDSatuanKonversi);
    $('#IDSatuanBesar').val(IDSatuanBesar).trigger('change');
    $('#Qty').val(Qty);

    $('#modal-data').modal('show');
});

  $('#form-data').validate({
      rules : {
        
      },
      submitHandler:function (form) {
          var reqData = new FormData(form);

          ajaxData(urlInsert, reqData, refresh, true);
      }
  });
})