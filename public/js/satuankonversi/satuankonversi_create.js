var satuan_exist = [];

function refresh(result) {
    alertSuccess(result.message, url);
}

function fillSatuan(result) {
    console.log(result);
    $.each(result, function(idx, val) {
        satuan_exist.push({
            'IDSatuan' : val.IDSatuanBesar,
            'Satuan'  : val.Satuan_besar
        });
    });

    $('.select2').select2();
    disabledSatuan();
}

function disabledSatuan() {
    $.each(satuan_exist, function(idx, val) {
        $('.satuan option[value='+val.IDSatuan+']').prop('disabled', true);
    });

    $('.select2').select2();
}

function checkSelected() {
    $('.satuan option:selected').each(function() {
        if ($(this).val() != '') {
            // $('.satuan option[value='+$(this).val()+']').prop('disabled', true);
        }
    });
}

function deleteRow() {
    $('.hapus').on('click', function() {
        swal({
            title: "Apakah anda yakin?",
            text: "Menghapus item!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $(this).closest('tr').find('.satuan option:disabled').prop('disabled', false);
                if ($(this).closest('tr').find('.satuan').val() != '') {
                    $('.satuan option[value='+$(this).closest('tr').find('.satuan').val()+']').prop('disabled', false);
                }
                $(this).closest('tr').remove();
                checkSelected();
            }
        });
    });
}

function addRow() {
    var html = '';
    var select = '';
    select = '<select class="form-control select2 satuan" name="IDSatuanBesar[]" style="width: 100%">';
    select+= '<option value="">- Pilih Satuan -</option>';
    $.each(data_satuan, function(idx, val) {
        select+= '<option value="'+val.IDSatuan+'"> ' + val.Satuan + ' </option>';
    })
    select += '</select>';

    html += '<tr id="rowvar" style="width:100%">';
    html += '<td> '+select+' </td>';
    html += '<td><input placeholder="" type="number" name="Qty[]" class="form-control qty"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';

    $('#list-satuan').append(html);    
    
    $('.select2').select2();

    $('.price').on('keyup', function() {
        $(this).val(formatRupiah($(this).val()))
    });

    disabledSatuan();
    deleteRow();
    checkSelected();
}

$(document).ready(function() {
    $('.select2').select2();
    addRow();

    $('#btn-add-satuan').on('click', function() {
        addRow();        
    })

    $('#IDBarang').on('change', function() {
        $('.satuan').val('').trigger('change');
        $('.satuan option').prop('disabled', false);
        satuan_exist = [];

        var thisSatuanKecil = $('#IDBarang option:selected').data('satuan');

        $('#IDSatuanKecil').val(thisSatuanKecil);
        $('#IDSatuan').val(thisSatuanKecil).trigger('change');

        satuan_exist.push({
            'IDSatuan'  : thisSatuanKecil,
            'Satuan'    : $('#IDSatuan').text()
        });

        var reqData = {
            'IDBarang' : $(this).val()
        };

        ajaxData(urlSatuan, reqData, fillSatuan);
    });

    $('.satuan').on('change', function() {
        checkSelected();
    })

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
})