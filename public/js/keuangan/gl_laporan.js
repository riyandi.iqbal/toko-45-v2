var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function setTable() {
    var colDef = [
        {data: 'IDJurnal', render: renderNumRow },
        {data: 'Tanggal'},
        {data: 'Nomor'},
        {data: 'Kode_COA'},
        {data: 'Nama_COA'},
        {data: 'Debet'},
        {data: 'Kredit'},
        {data: 'Debet'}
    ];

    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});