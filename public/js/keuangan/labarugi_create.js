var dataExist = [];

function refresh(result) {
    alertSuccess(result.message, urlReload);
}

function addRow() {
    var html = '';
    var select2 = '';

    select2 = '<option value="-"> - </option>';
    $.each(data_coa, function(idx, val) {
        select2 += '<option value="' + val.IDCoa + '" data-kode="'+val.Kode_COA+'" data-nama="'+val.Nama_COA+'">' + val.Kode_COA + ' - ' + val.Nama_COA + '</option>'
    })

    html += '<tr> <td style="width: 10%"> <button type="button" class="btn col-20 btn-sm btn-add"> + </button> <button type="button" class="btn col-18 btn-sm btn-remove"> - </button> </td>';
    html += '<td  style="width: 35%">';

    html += '<select name="IDCoa[]" class="select2 idcoa" style="width: 100%">';
    html += select2;
    html += '</select>';

    html += '</td>';

    html += '<td style="width: 25%"> <textarea name="Keterangan[]" class="keterangan"></textarea> </td>';

    html += '<td style="width: 15%"> <select name="Kategori[]" class="select2 kategori" style="width: 100%">     <option value="judul"> Judul </option>    <option value="data"> Data </option>    <option value="hasil"> Hasil </option>    <option value="spasi"> Spasi </option>    <option value="total"> Total </option> </select> </td>';

    html += '<td style="width: 10%"> <input type="text" name="Group[]" class="group"> </td> </tr> ';

    $('#tbody-table-data').append(html);
    $('.select2').select2();
}

function setView(val) {
    $('#tbody-table-data').append(val.view);
    if (val.child.length > 0 ) {
        $.each(val.child, function(idx, value) {
            setView(value)
        });
    }
    $('.select2').select2();
}

function setPreview(id) {
    $('#' + id).html('');
        var html = '';
        var group_start = '-';
        $('.keterangan').each(function() {
            var row = $(this).closest('tr');
            var kategori = row.find('.kategori option:selected').val();

            html += '<tr>';
            
            if (kategori == 'data' ) {
                html += '<td>&nbsp;&nbsp;&nbsp;'+$(this).val()+'</td>';
            } else {
                html += '<td><b>'+$(this).val()+'</b></td>';
            }

            html += '</tr>';
        })
        $('#' + id).append(html);
}

$(document).ready(function() {
    $('.select2').select2();

    setPreview('table-preview-i');

    $('body').on('click','table .btn-add-head',function(e) {
        addRow()
    });

    $('body').on('click','table .btn-add',function(e) {
        var html = '';
        var select2 = '';

        select2 = '<option value="-"> - </option>';
        $.each(data_coa, function(idx, val) {
            select2 += '<option value="' + val.IDCoa + '" data-kode="'+val.Kode_COA+'" data-nama="'+val.Nama_COA+'">' + val.Kode_COA + ' - ' + val.Nama_COA + '</option>'
        })

        html += '<tr> <td style="width: 10%"> <button type="button" class="btn col-20 btn-sm btn-add"> + </button> <button type="button" class="btn col-18 btn-sm btn-remove"> - </button> </td>';
        html += '<td style="width: 35%">';

        html += '<select name="IDCoa[]" class="select2 idcoa" style="width: 100%">';
        html += select2;
        html += '</select>';

        html += '</td>';

        html += '<td style="width: 25%"> <textarea name="Keterangan[]" class="keterangan"></textarea> </td>';

        html += '<td style="width: 15%"> <select name="Kategori[]" class="select2 kategori" style="width: 100%">     <option value="judul"> Judul </option>    <option value="data"> Data </option>    <option value="hasil"> Hasil </option>    <option value="spasi"> Spasi </option>    <option value="total"> Total </option> </select> </td>';

        html += '<td style="width: 15%"> <input type="text" name="Group[]" class="group"> </td> </tr> ';

        $(this).closest('tr').after(html);
        $('.select2').select2();
    });

    $('body').on('click', 'table .btn-remove', function (event) {
        $(this).closest('tr').remove();
        setPreview('table-preview-i');
    });

    $('body').on('change', '.idcoa', function (event) {
        var row = $(this).closest('tr');
        if ($(this).val() != '-') {
            row.find('.keterangan').val(row.find('.idcoa option:selected').text());
            row.find('.keterangan').trigger('keyup')
        } else {
            row.find('.keterangan').val('');
            row.find('.keterangan').trigger('keyup')
        }
    });

    $('body').on('keyup', '.keterangan', function (event) {
        var row = $(this).closest('tr');
        setPreview('table-preview-i');
    });

    $('body').on('change', '.kategori', function (event) {
        var row = $(this).closest('tr');
        setPreview('table-preview-i');
    });

    $('#preview').on('click', function() {
        $('#modal-preview').modal('show');
        
        setPreview('table-preview')
    });

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlSimpan, reqData, refresh, true);
        }
    });
})