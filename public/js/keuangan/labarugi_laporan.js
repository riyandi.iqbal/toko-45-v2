function setTabel(result) {
    $('#table-preview').html('');
    var html ='';

    html +='<thead>';
    html += '<tr>';
    html += '<th> Keterangan </th>';
    html += '<th> '+$('#bulan option[value='+result.bulan+']').text() + ' ' + result.tahun + ' </th>';
    html += '<th> s/d '+$('#bulan option[value='+result.bulan+']').text() + ' ' + result.tahun + ' </th>';
    html += '</tr>';
    html += '</thead>';
    html +='<tbody>';
    $.each(result.data, function(idx, val) {
        html += '<tr>';
        if (val.Kategori == 'data') {
            html += '<td>&nbsp;&nbsp;&nbsp;'+val.Keterangan+' </td>';
        } else {
            html += '<td><b>'+val.Keterangan+'</b></td>';
        }

        if (val.total_now < 0) {
            html += '<td style="text-align: right;">('+CurrencyFormat(val.total_now * -1, angkakoma)+')</td>';
        } else {
            html += '<td style="text-align: right;">'+CurrencyFormat(val.total_now, angkakoma)+'</td>';
        }

        if (val.total_ongoing < 0) {
            html += '<td style="text-align: right;">('+CurrencyFormat(val.total_ongoing * -1, angkakoma)+')</td>';
        } else {
            html += '<td style="text-align: right;">'+CurrencyFormat(val.total_ongoing, angkakoma)+'</td>';
        }


        html += '</tr>';
    })
    html += '</tbody>';

    $('#table-preview').append(html);
}

$(document).ready(function() {
    $('.select2').select2();
    
    $('#btn-search').on('click', function() {
        var reqData = {
            'bulan' : $('#bulan').val(),
            'tahun' : $('#tahun').val(),
        };

        ajaxData(urlData, reqData, setTabel);
    });

    $('#btn-search').trigger('click');
})