function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

$(document).ready(function() {
    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})