var action;

alert = function(str) {
    swal({
        title: "",
        text: str,
        icon: "error",
        confirmButtonColor: "#254283"
    });
}

function alertSuccess(str, url = null) {
    swal({
        title: "Sukses",
        text: str,
        icon: "success",
        confirmButtonColor: "#254283"
    }).then((willDelete) => {
      if (url != null) {
        location.href = url;
      }
    });
}

function renderNumRow(data, type, row, meta) {
    return meta.row + 1 + meta.settings._iDisplayStart;
}

function renderDate(data) {
  var d = new Date(data);
    var month_names =["Jan","Feb","Mar",
                    "Apr","May","Jun",
                    "Jul","Aug","Sep",
                    "Oct","Nov","Dec"];

    var day = d.getDate();
    var month_index = d.getMonth();
    var year = d.getFullYear();

    return "" + day + " " + month_names[month_index] + " " + year;
}

function setDataTable(divId, dataUrl, colDef = [], requestData = null, requestOrder = null, reqDom = null, reqFooterCallback = null, reqSearching = true, reqrowsGroup = null) {
    var  tableConf = {
        select: true,
        responsive: true,
        processing: true,
        serverSide: true,
        destroy: true,
        searching : reqSearching,
        language: {
            lengthMenu: "Display _MENU_ records per page",
            zeroRecords: "Data tidak ditemukan",
            info: "Showing page _PAGE_ of _PAGES_",
            infoEmpty: "Data Kosong",
            infoFiltered: "(filtered from _MAX_ total records)"
        },
        ajax: {
            url: dataUrl,
            type: 'GET',
            data: function(d) {
                if (requestData !== null) {
                $.each(requestData, function(key, value) {
                    d[key] = value;
                });
                }
            },
            complete: function(d) {
              
            }
        },
        columns: colDef
    };
    
    
    if (requestOrder !== null) {
        tableConf.order = requestOrder;
    }
    
    if (reqDom !== null) {
        tableConf.dom = reqDom;
    }

    if (reqFooterCallback != null) {
        tableConf.footerCallback = reqFooterCallback;
    }

    if(reqrowsGroup != null) {
      tableConf.rowsGroup = reqrowsGroup;
    }
    
    return $(divId).DataTable(tableConf);
}

function ajaxData(requestUrl, requestData, callback = false, multipart = false, showAlert = true, type = 'POST') {
    var ajaxSetting = {
      method  : type,
      url     : requestUrl,
      headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
      data    : requestData,
      beforeSend: function(data) {
        $('#modal-loading').fadeIn('fast');
      },
      success: function(data) {
        //try {
          var result = JSON.parse(data);
  
          if (result.status === true) {
            if (callback !== false) callback(result);
          } else if (result.status === false) {
            if (showAlert === true) alert(result.message);
          } else {
            callback(result);
          }
        // } catch (e) {
        //   alert('System Error\n ' + e.message);
        // }
      },
      error: function(data) {
        alert(data.status + '\n' + data.statusText);
      },
      complete: function(data) {
        $('#modal-loading').fadeOut('fast');
      }
    }
  
    if (multipart === true) {
      ajaxSetting.contentType = false;
      ajaxSetting.processData = false;
    }
  
    $.ajax(ajaxSetting);
}

// function formatRupiah(bilangan, prefix) {
//     var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
//     split   = number_string.split(','),
//     sisa    = split[0].length % 3,
//     rupiah  = split[0].substr(0, sisa),
//     ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);
    
//     if (ribuan) {
//         separator = sisa ? '.' : '';
//         rupiah += separator + ribuan.join('.');
//     }
    
//     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
//     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
// }

function FormatCurrency(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

function CurrencyFormat(data, digits = 0) {
  var formatter = new Intl.NumberFormat('id-ID', {
      style: 'decimal',
      minimumFractionDigits: digits,
  });
  
  return formatter.format(data);
}

function toggleForm(elem, role) {
  if (role === true) {
    $(elem + ' :input').not('[type="button"]').prop('disabled', false);
    $(elem + ' :input[type="submit"]').show();
  } else {
    $(elem + ' :input').not('[type="button"]').prop('disabled', true);
    $(elem + ' :input[type="submit"]').hide();
  }
}

function deleteData(id, actionUrl, callback = refresh, message = "Anda yakin akan menghapus data tersebut?") {
  swal({
    title: "Peringatan!",
    text: message,
    icon: "warning",
    buttons: true,
    dangerMode: true,
  }).then((value) => {
    if (value) {
      ajaxData(actionUrl, {id:id}, callback, false, true, 'delete');
    }
  });
}

$(document).ready(function() {
  $('.datepicker').each(function() {
    $(this).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true
    });
  }) 

  $('input').prop('autocomplete', 'off');
  
  jQuery.extend(jQuery.validator.messages, {
    required: "Harus diisi.",
    email: "Email tidak valid.",
    url: "URL tidak valid.",
    number: "Harus angka.",
    maxlength: jQuery.validator.format("Maksimal {0} karakter."),
    minlength: jQuery.validator.format("Minimal {0} karakter."),
    max: jQuery.validator.format("Maksimal {0}."),
    min: jQuery.validator.format("Minimal {0}.")
  });
});
