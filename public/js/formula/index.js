var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';


    button += '<span><a title="Detail" href="' + url + '/show/'+row.IDFormula+'"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>';

    button += '<span><a class="action-icons c-edit" href="'+url+'/edit/'+row.IDFormula+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDFormula+' \', \' ' + url + '/hapus_data/'+row.IDFormula+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDFormula', render: renderNumRow },
        {data: 'Tanggal', render: renderDate},
        {data: 'Nama_Formula'},
        {data: 'Total', className: 'dt-body-right', render: FormatCurrency},
        {data: 'IDFormula', className: 'dt-body-center', render: renderAction}
    ];
    
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

    $('#field').on('change', function() {
        if ($(this).val() == 'tbl_barang.IDGroupBarang') {
            $('#pilihan').hide();
            $('#select').show();
            $('#keyword_chosen').removeAttr('style');
        } else {
          $('#pilihan').show();
            $('#select').hide();
        }
  })

});