function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function setKodeCustomer(result) {
    $('#Kode_Customer').val(result)  
}

function getKodeCustomer() {
    var reqData = {
        'group' : $('#IDGroupCustomer').val()
    };

    ajaxData(urlKodeCustomer, reqData, setKodeCustomer, false, false);
}

$(document).ready(function() {
    $('.select2').select2();

    $('#IDGroupCustomer').on('change', function() {
      getKodeCustomer();
    })

    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})