var tableData;

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a class="action-icons c-edit" href="'+url+'/'+row.IDCustomer+'" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>';

    button += '<span><a title="Hapus" onclick="deleteData(\' '+row.IDCustomer+' \', \' ' + url + '/'+row.IDCustomer+' \', refresh)"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>';

    return button;
}

function setTable() {
    var colDef = [
        {data: 'IDCustomer', render: renderNumRow },
        {data: 'Nama_Group_Customer'},
        {data: 'Kode_Customer'},
        {data: 'Nama'},
        {data: 'Aktif', className: 'dt-body-center'},
        {data: 'IDCustomer', className: 'dt-body-center', render: renderAction}
    ];
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });
});