var dataDetail = [];

function refresh(result) {
    alertSuccess(result.message, url);
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = null;

    ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function deleteRow(row) {
    dataDetail.splice(row, 1);
    
    setTableDetail();
}

function renderAction(data, type, row, meta) {
    var button = '';
    
    button += '<button type="button" class="btn btn-danger btn-sm" onclick="deleteRow(\''+meta.row+'\')"> <span class="fa fa-trash"></span></button>';

    return button;
}

function setTableDetail() {
    tablePembayaran = $('#table-detail').dataTable({
        data : dataDetail,
        columns: [
            { render : renderNumRow},
            { data : "Jenis_giro"},
            { data : "Nama_perusahaan" },
            { data : "Nomor_giro"},
            { data : "Tanggal_giro"},
            { data : "Nomor_faktur"},
            { data : "Tanggal_faktur"},
            { data : "Status"},
            { data : "Nomor_baru"},
            { data : "Nama_bank"},
            { data : "Nomor_rekening"},
            { data : "Nominal_giro", className: 'dt-body-right', render: FormatCurrency},
            { render : renderAction }
        ],
        destroy : true,
        processing : true,
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/\./g,'').replace(',', '.')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 11 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 11, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 11 ).footer() ).html(
                CurrencyFormat(pageTotal) +' ( '+ CurrencyFormat(total) +' )'
            );
        }
    });
}

$(document).ready(function() {
    $('.select2').select2();
    getNumberInvoice();    

    $('#IDPerusahaan').select2({
        minimumInputLength: 0,
        ajax: {
            url: urlPerusahaan,
            data: function (params) {
              return {
                q: params.term, // search term
                Jenis_giro : $('#Jenis_giro').val()
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Nama,
                            id: item.ID,
                            item : item
                        }
                    })
                };
            }
        }
    });

    $('#IDGiro').select2({
        minimumInputLength: 0,
        ajax: {
            url: urlNomorGiro,
            data: function (params) {
              return {
                q: params.term, // search term
                IDPerusahaan: $('#IDPerusahaan').val(),
                Kategori : $('#IDPerusahaan option:selected').data('kategori')
              };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Nomor_giro,
                            id: item.IDGiro,
                            item : item
                        }
                    })
                };
            }
        }
    });

    $('#IDGiro').on('select2:select', function(e) {
        var result = e.params.data;

        var Tanggal_giro = moment(new Date(result.item.Tanggal_giro));
        var Tanggal_faktur = moment(new Date(result.item.Tanggal_faktur));
    
        $('#Tanggal_giro').val(Tanggal_giro.format('DD/MM/YYYY'));
        $('#Nomor_faktur').val(result.item.Nomor_faktur);
        $('#Tanggal_faktur').val(Tanggal_faktur.format('DD/MM/YYYY'));
        $('#Nominal_giro').val(FormatCurrency(result.item.Nilai));
    });

    $('#IDBank').on('change', function() {
        $('#Nomor_rekening').val($('#IDBank option:selected').data('rekening'));
    })

    $('#btn-add-detail').on('click', function() {

        data = {
            'Jenis_giro' : $('#Jenis_giro').val(),
            'IDPerusahaan' : $('#IDPerusahaan').val(),
            'Nama_perusahaan' : $("#IDPerusahaan option:selected").html(),
            'IDGiro' : $('#IDGiro').val(),
            'Nomor_giro' : $('#IDGiro option:selected').html(),
            'Tanggal_giro' : $('#Tanggal_giro').val(),
            'Nomor_faktur' : $('#Nomor_faktur').val(),
            'Tanggal_faktur' : $('#Tanggal_faktur').val(),
            'Status' : $('#Status').val(),
            'Nomor_baru' : $('#Nomor_baru').val(),
            'IDBank' : $('#IDBank').val(),
            'Nama_bank' : $('#IDBank option:selected').html(),
            'Nomor_rekening' : $('#Nomor_rekening').val(),
            'Nominal_giro' : parseFloat($('#Nominal_giro').val().replace(/\./g,'')),
        }

        dataDetail.push(data);

        $('.form-detail').val('').trigger('change');

        setTableDetail();
    });

    $('#IDPerusahaan').on('change', function() {
        $('#IDGiro').val('').trigger('change')
        $('.form-auto').val('');
    })

    $('#Status').on('change', function() {
        if ($(this).val() == 'Nomor_baru') {
            $('#Nomor_baru').prop('disabled', false);
        } else {
            $('#Nomor_baru').prop('disabled', true);
        }
    });

    $('#form-data').validate({
        rules : {
            Nomor : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);
            
            reqData.append('Total', total);
            reqData.append('data_detail', JSON.stringify(dataDetail));

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
});