var tableData;

function refresh(result) {
    alertSuccess(result.message, url + '/show/' + result.data.IDFB);
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY");
    var dateObject = dateMomentObject.toDate();

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');

    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY");
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        value: dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY");
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);

    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setTotalInvoice() {
    var addTotal = 0;
    var minTotal = 0;

    $('.total-add').each(function () {
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) : 0;
        addTotal += thisVal;
    });

    $('.total-min').each(function () {
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) : 0;
        minTotal += thisVal;
    });

    // var DP = ($('#UMSupplier').val().length > 0) ? parseFloat($('#UMSupplier').val().replace(/\./g, '').replace(',', '.')) : 0;

    var Grand_total = parseFloat(addTotal) - parseFloat(minTotal);

    var Total = parseFloat(Grand_total);

    $('.total-invoice').val(CurrencyFormat(Grand_total));
    $('.sisa-total-invoice').val(CurrencyFormat(Total));
}

function setFormData() {
    var reqData = {
        'Status_ppn': $('input[type=radio]:checked').val()
    };
    // $('#Status_ppn').val(result.penerimaan_barang.Status_ppn);
    // $('#NamaSupplier').val(result.penerimaan_barang.Nama);
    // $('#IDSupplier').val(result.penerimaan_barang.IDSupplier);
    // $('#Total_qty').val(result.penerimaan_barang.Total_qty_yard);
    // $('#Nomor_sj').val(result.penerimaan_barang.Nomor_sj);

    if (reqData.Status_ppn == 'exclude') {
        $('.ppn1').show();
        $('#DPP').val(CurrencyFormat($('#total_invoice') - (0.1 * $('#total_invoice'))));
        $('#PPN').val(CurrencyFormat(0.1 * $('#total_invoice')));
    } else {
        $('#PPN').val(CurrencyFormat(0));
        $('#DPP').val(CurrencyFormat($('#total_invoice')));
        $('.ppn1').hide();
    }
    $('#total_invoice').val(CurrencyFormat(result.penerimaan_barang.Total_harga))
    if (result.uang_muka != null) {
        $('#UMSupplier').val(CurrencyFormat(result.uang_muka.Nilai_UM))
    } else {
        $('#UMSupplier').val(CurrencyFormat(0))
    }

    $('#Jatuh_tempo').val(75).trigger('keyup');

    // getNumberInvoice();
    // setTableBarang();
    setTotalInvoice();
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn': $('#Status_ppn').val()
    };

    ambilnomorfb();
    //ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="' + row.IDBarang + '">';
    html += '<input type="hidden" name="Qty[]" value="' + row.Qty_yard + '">';
    html += '<input type="hidden" name="Harga[]" value="' + row.Harga + '">';
    html += '<input type="hidden" name="Sub_total[]" value="' + row.Harga * row.Qty_yard + '">';
    html += '<input type="hidden" name="IDSatuan[]" value="' + row.IDSatuan + '">';

    return html;
}

function renderSubTotal(data, type, row) {
    return FormatCurrency(row.Harga * row.Qty_yard);
}

// function setTableBarang() {
//     var colDef = [{
//             data: 'IDTBS',
//             render: renderNumRow
//         },
//         {
//             data: 'Nama_Barang',
//             name: 'tbl_barang.Nama_Barang',
//             render: renderForm
//         },
//         {
//             data: 'Qty_pesan',
//             className: 'dt-body-right'
//         },
//         {
//             data: 'Qty_yard',
//             className: 'dt-body-right'
//         },
//         {
//             data: 'Harga',
//             className: 'dt-body-right',
//             render: FormatCurrency
//         },
//         {
//             data: 'Satuan',
//             name: 'tbl_satuan.Satuan',
//             className: 'dt-body-center'
//         },
//         {
//             data: 'Harga',
//             className: 'dt-body-right',
//             render: renderSubTotal
//         }
//     ];
//     var reqData = {
//         IDTBS: $('#IDTBS').val(),
//     };

//     var reqOrder = null;

//     tableData = setDataTable('#table-barang', urlSuratJalanDetail, colDef, reqData, reqOrder);
// }

$(document).ready(function () {
    $('.select2').select2();

    // setTableBarang();
    getNumberInvoice();

    $('#IDTBS').on('change', function () {
        if ($(this).val() == '') {
            $('#Status_ppn').val('');
            $('#IDCustomer').val('');
            $('#Total_qty').val('');
            $('#DPP').val('');
            $('#PPN').val('');
            $('#NamaCustomer').val('');
            $('#Nama_di_faktur').val((''));
            $('#total_invoice').val((''));
            $('.total-invoice').val((''));

            // setTableBarang();
        } else {
            var reqData = {
                'id': $(this).val()
            }
            ajaxData(urlSuratJalan, reqData, setFormData, false, false)
        }
    });

    $('#Jatuh_tempo').on('keyup', function () {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY");
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY");
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#diskonpersen').on('keyup', function () {
        var totalInvoice = ($('#total_invoice').val().length > 0) ? parseFloat($('#total_invoice').val().replace(/\./g, '').replace(',', '.')) : 0;
        var thisValRupiah = parseFloat(($(this).val() / 100) * totalInvoice);

        $('#diskonrupiah').val(thisValRupiah).trigger('keyup');
    });

    $('#diskonrupiah').on('keyup', function () {
        setTotalInvoice();

        var totalInvoice = ($('#total_invoice').val().length > 0) ? parseFloat($('#total_invoice').val().replace(/\./g, '').replace(',', '.')) : 0;
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) : 0;
        var thisValPersen = parseFloat((thisVal / totalInvoice) * 100);

        $('#diskonpersen').val(thisValPersen.toFixed(2));
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules: {
            Tanggal: "required",
            IDSJC: "required",
            Jatuh_tempo: {
                required: true,
                number: true,
            },
            Tanggal_jatuh_tempo: "required",
            DPP: "required",
            DPP: "required",
            total_invoice_diskon: "required"
        },
        submitHandler: function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });

})

var nomor_list = 1;

// function refresh(result) {
//     alertSuccess(result.message, url + '/detail/' + result.data.IDPO);
// }

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function (index, val) {
        html += '<option value="' + val.IDCoa + '" data-namacoa="' + val.Nama_COA + '"> ' + val.Kode_COA + ' ' + val.Nama_COA + ' </option>';
    });

    $('#IDCoa').append(html);
}

function addRow() {
    var thisPpn = 10;
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        thisPpn = 0;
    }

    var thisQty='';

    var html = '';

    var select = '';
    select = '<select name="IDBarang[]" class="form-control select-barang select2" style="width: 100%">';
    select += '<option value="">- Pilih Barang -</option>';
    $.each(data_barang, function (idx, val) {
        select += '<option value="' + val.IDBarang + '" data-idsatuan="' + val.IDSatuan + '" data-satuan="' + val.Satuan + '" > ' + val.Kode_Barang + ' - ' + val.Nama_Barang + ' </option>';
    })
    select += '</select>';

    html += '<tr id="rowvar' + nomor_list + '" style="width:100%">';
    html += '<td> ' + select + ' </td>';
    html += '<td><input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty"></td>';
    // html += '<td><input placeholder="" type="text" name="Satuan[]" class="form-control satuan"><input placeholder="" type="hidden" name="IDSatuan[]" class="form-control idsatuan"></td>';
    html += '<td> <select name="IDSatuan[]" class="form-control select-satuan select2" style="width: 100%"> </select> </td>';
    html += '<td><input placeholder="" type="text" name="harga[]" class="form-control price"></td>';
    html += '<td><input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="' + thisPpn + '" readonly></td>';
    html += '<td><input placeholder="" type="text" name="diskon[]" class="form-control diskon harga-123"></td>';
    html += '<td><input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal"></td>';
    html += '<td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>';
    html += '</tr>';



    $('#list-barang').append(html);
    // setSelect2Barang();

    $('.select-satuan').select2();
    $('.select2').select2();

    setSelect2Selected();

    $('.qty').on('keyup', function () {
        setQtyTotal();

        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });
    $('.diskon').on('keyup', function () {
        setQtyTotal();
        var numberField = document.getElementsByClassName('harga-123');
        if (numberField) {
            $.each(numberField, function (key, value) {
                console.log(key);
                $(this).on('keyup', function (e) {
                    $(this).val(formatRupiah($(this).val()));
                });
            });
        }
        var row = $(this).closest('tr');
        row.find('.price').trigger('keyup');
    });

    $('.select-satuan').on('change', function () {
        var row = $(this).closest('tr');

        var reqData = {
            IDBarang: row.find('.select-barang').val(),
            IDSatuan: $(this).val(),
            IDMataUang: $('#IDMataUang').val(),
        }

        $.ajax({
            url: urlHargaJual,
            method: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: reqData,
            beforeSend: function (data) {
                $('#modal-loading').fadeIn('fast');
            },
            success: function (data) {
                var result = JSON.parse(data);

                if (result != null) {
                    row.find('.price').val(FormatCurrency(result.Modal)).trigger('keyup');
                } else {
                    row.find('.price').val(FormatCurrency('')).trigger('keyup');
                }
            },
            complete: function (data) {
                $('#modal-loading').fadeOut('fast');
            }
        })
    })

    $('.price').on('keyup', function () {
        $(this).val(formatRupiah($(this).val()))
        // Get the current row
        var row = $(this).closest('tr');

        var rowQty = (!isNaN(row.find('.qty').val())) ? parseFloat(row.find('.qty').val()) : 0;
        var rowPrice = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g, '').replace(',', '.')) : 0;
        var rowDiskon = ((row.find('.diskon').val().length > 0)) ? parseFloat(row.find('.diskon').val().replace(/\./g, '').replace(',', '.')) : 0;
        var rowPpn = parseFloat(row.find('.ppn').val())
        var rowSubTotal = (rowQty * rowPrice) + ((rowPpn / 100) * rowQty * rowPrice) - rowDiskon;

        console.log(rowPrice)
        row.find('.subtotal').val(FormatCurrency(rowSubTotal)).trigger('keyup');
    });

    $('.subtotal').on('keyup', function () {
        setGrandTotal();
        setTotalInvoice()
    });

    deleteRow();
}

function setSelect2Barang() {
    $('.select-barang').select2({
        minimumInputLength: 0,
        ajax: {
            url: urlBarang,
            data: function (params) {
                return {
                    q: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: $.map(JSON.parse(data), function (item) {
                        return {
                            text: item.Kode_Barang + ' - ' + item.Nama_Barang,
                            id: item.IDBarang,
                            satuan: item.Satuan,
                            modal: item.Modal,
                            item: item
                        }
                    })
                };
            }
        }
    });

    setSelect2Selected();
}

function fillSatun(result) {
    var select = '';

    $.each((result), function (index, val) {
        select += '<option value="' + val.IDSatuan + '"> </option>';
    });

    $('#IDSatuan').append(select);
}

function setSelect2Selected() {
    $('.select-barang').on('change', function (e) {
        var row = $(this).closest('tr');
        var IDSatuan = row.find('.select-barang option:selected').data('idsatuan');
        row.find('.qty').val('').trigger('keyup');

        row.find('.select-satuan').html('');

        var select = '';
        select += '<option value="' + row.find('.select-barang option:selected').data('idsatuan') + '">' + row.find('.select-barang option:selected').data('satuan') + '</option>';

        $.ajax({
            url: urlSatuan,
            method: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                IDBarang: $(this).val()
            },
            beforeSend: function (data) {
                $('#modal-loading').fadeIn('fast');
            },
            success: function (data) {
                var result = JSON.parse(data);

                $('#modal-loading').fadeIn('fast');

                $.each((result), function (index, val) {
                    select += '<option value="' + val.IDSatuanBesar + '"> ' + val.Satuan_besar + ' </option>';
                });

                row.find('.select-satuan').append(select);
                row.find('.select-satuan').val(IDSatuan).trigger('change');

                $('#modal-loading').fadeOut('fast');
            },
            complete: function (data) {
                $('#modal-loading').fadeOut('fast');
            }
        })

    });
}

function setQtyTotal() {
    var qtyTotal = 0;

    $('.qty').each(function () {
        var thisQty = (!isNaN($(this).val())) ? parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) : 0;
        qtyTotal += thisQty;
    });

    $('#Total_qty').val(FormatCurrency(qtyTotal));
}

function setGrandTotal() {
    var subTotal = 0;

    $('.subtotal').each(function () {
        var thisSubTotal = (($(this).val().length > 0)) ? parseFloat($(this).val().replace(/\./g, '').replace(',', '.')) : 0;
        console.log(thisSubTotal);
        subTotal += thisSubTotal;
    });
    if ($('input[type=radio][name=Status_ppn]:checked').val() == 'include') {
        $('#PPN1').val(FormatCurrency(0));
        $('#DPP').val(FormatCurrency(subTotal));
        $('#total_invoice').val(FormatCurrency(subTotal));
    } else {
        $('#PPN1').val(FormatCurrency(subTotal * 0.1));
        $('#DPP').val(FormatCurrency(subTotal));
        $('#total_invoice').val(FormatCurrency(subTotal + (subTotal * 0.1)));
    }

    $('#Grand_total').val(FormatCurrency(subTotal));
    
}

function deleteRow() {
    $('.hapus').on('click', function () {
        swal({
                title: "Apakah anda yakin?",
                text: "Menghapus item pesanan barang!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $(this).closest('tr').remove();
                    setQtyTotal();
                    setGrandTotal();
                }
            });
    });
}

function setNumberSO(result) {
    $('#Nomor').val(result)
}

function getNumberSO() {
    var reqData = {
        'Status_ppn': $('input[type=radio]:checked').val()
    };
    console.log(reqData);
    if (reqData.Status_ppn == 'include') {
        ambilnomorpononppn();
        $('.ppn1').hide();
    } else {
        $('.ppn1').show();
        ambilnomorpoppn();
    }
    //ajaxData(urlNumberSO, reqData, setNumberSO, false, false);
}

$(document).ready(function () {
    $('.select2').select2();
    getNumberSO();
    setSelect2Barang();
    addRow();
    $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    $('.group').hide();

    $('#btn-add-barang').on('click', function () {
        addRow();
    })

    $('input[type=radio]').on('click', function () {
        getNumberSO();
    });

    $('input[type=radio]').on('click', function () {
        if ($(this).val() == 'include') {
            $('.ppn').val(0);
        } else {
            $('.ppn').val(0);
        }

        $('.price').trigger('keyup');
    });

    $('#Jenis_Pembayaran').on('change', function () {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran: $(this).val()
            };

            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#IDMataUang').on('change', function () {
        $('#Kurs').val($('#IDMataUang option:selected').data('kurs'))
    })

    $('#form-data').validate({
        rules: {
            Grand_total: {
                required: true
            }
        },
        submitHandler: function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }


    });


});