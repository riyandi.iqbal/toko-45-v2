var tableData;

function refresh(result) {
    alertSuccess(result.message);
    setTable();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0 || data == 'aktif') {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    button += '<span><a title="Update"><i class="fa fa-pencil-square-o" style="color: grey;"></i></a></span>';
    
    return button;
}

function renderActionUpdate(data, type, row) {
    var button = '';

    button += '<span><a title="Updatess"><i class="i class="fa fa-pencil fa-lg" style="color: grey;"></i></a></span>';
    //$('#modal-data').modal('show');

    return button;
}

function renderGrandTotal(data, type, row) {
    return row.Kode + FormatCurrency(data);
}

function setTable() {
    var colDef = [{
            data: 'IDFB',
            render: renderNumRow
        },
        {
            data: 'Tanggal',
            render: renderDate
        },
        {
            data: 'Nomor'
        },
        {
            data: 'No_sj_supplier'
        },
        {
            data: 'no_pajak'
        },
        // {data: 'Nomor_tbs'},
        {
            data: 'Nama',
            name: 'tbl_supplier.Nama'
        },

        {
            data: 'Total_qty_yard',
            className: 'dt-body-center',
            render: FormatCurrency
        },
        {
            data: 'total_harga',
            className: 'dt-body-right',
            render: renderGrandTotal
        },
        {
            data: 'Batal',
            className: 'dt-body-center',
            render: renderStatus
        },
        {
            data: 'Nomor',
            className: 'dt-body-center',
            render: renderAction
        }
    ];
    var reqData = {
        tanggal_awal: $('#date_from').val(),
        tanggal_akhir: $('#date_until').val(),
        field: $('#field').val(),
        keyword: $('#keyword').val(),
    };

    var reqOrder = [
        [0, 'desc']
    ];

    var reqFooterCallback = function (row, data, start, end, display) {
        var api = this.api(),
            data;

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
                i.replace(/\./g, '').replace(',', '.') * 1 :
                typeof i === 'number' ?
                i : 0;
        };

        // Total over this page
        pageTotalQty = api
            .column(6, {
                page: 'current'
            })
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            }, 0);

        // Update footer
        $(api.column(6).footer()).html(
            FormatCurrency(pageTotalQty)
        );
    };

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, reqFooterCallback);
}

$(document).ready(function () {
    setTable();

    $('#cari-data').on('click', function () {
        setTable();
    });

    $('#field').on('change', function () {
        var select = '<select data-placeholder="Cari Customer" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">' +
            '<option value="">- Pilih -</option>' +
            '<option value="aktif">Aktif</option>' +
            '<option value="1">Dibatalkan</option>' +
            '</select>';

        var input = '<input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">';

        if ($(this).val() == 'tbl_pembelian.Batal') {
            $('#pilihan').html(select);
        } else {
            $('#pilihan').html(input);
        }

    })
});