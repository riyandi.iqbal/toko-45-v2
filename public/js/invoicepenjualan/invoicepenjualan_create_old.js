var tableData;

function refresh(result) {
    alertSuccess(result.message, url + '/detail/' + result.data.id);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'"> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY"); 
    var dateObject = dateMomentObject.toDate(); 

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');
    
    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY"); 
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        value : dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);
    
    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setTotalInvoice() {
    var addTotal = 0;
    var minTotal = 0;

    $('.total-add').each(function() {
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0;
        addTotal += thisVal;
    });

    $('.total-min').each(function() {
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0;
        minTotal += thisVal;
    });

    var DP = ($('#UMCustomer').val().length > 0) ? parseFloat($('#UMCustomer').val().replace(/\./g,'').replace(',', '.')) : 0;

    var Grand_total = parseFloat(addTotal) - parseFloat(minTotal);

    var Total = parseFloat(Grand_total) - parseFloat(DP);
    
    $('.total-invoice').val(CurrencyFormat(Grand_total));
    $('.sisa-total-invoice').val(CurrencyFormat(Total));
}

function setFormData(result) {
    $('#Status_ppn').val(result.surat_jalan.statusppn);
    $('#NamaCustomer').val(result.surat_jalan.Nama);
    $('#IDCustomer').val(result.surat_jalan.idcustomer);
    $('#Total_qty').val(result.surat_jalan.qty);
    $('#Nama_di_faktur').val(result.surat_jalan.nomorcustomer);

    if (result.surat_jalan.statusppn == 'exclude') {
        $('.ppn').show();
        $('#DPP').val(CurrencyFormat(result.surat_jalan.dpp));
        $('#PPN').val(CurrencyFormat(result.surat_jalan.nilaippn));
    } else if(result.surat_jalan.statusppn == 'include') {
        $('#PPN').val(CurrencyFormat(0));
        $('#DPP').val(CurrencyFormat(parseFloat(result.surat_jalan.dpp) + parseFloat(result.surat_jalan.nilaippn)));
        $('.ppn').hide();
    }

    $('#total_invoice').val(CurrencyFormat(result.surat_jalan.grantotal))
    if (result.uang_muka != null) {
        $('#UMCustomer').val(CurrencyFormat(result.uang_muka.Nilai_UM))
    } else {
        $('#UMCustomer').val(CurrencyFormat(0))
    }
            
    $('#Jatuh_tempo').val(75).trigger('keyup');

    getNumberInvoice();
    setTableBarang();
    setTotalInvoice();
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn' : $('#Status_ppn').val()
    };
    console.log(reqData);
    ambilnomorfj();
    //ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="'+row.idbarang+'">';
    html += '<input type="hidden" name="Qty[]" value="'+row.qty+'">';
    html += '<input type="hidden" name="Harga[]" value="'+row.hargasatuan+'">';
    html += '<input type="hidden" name="Sub_total[]" value="'+row.subtotal+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.idsatuan+'">';

    return html;
}

function setTableBarang() {
    var colDef = [
        {data: 'idsjcdetail', render: renderNumRow },
        {data: 'Nama_Barang', name: 'tbl_barang.Nama_Barang', render: renderForm},
        {data: 'qty', className: 'dt-body-right'},
        {data: 'hargasatuan', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Satuan', name: 'tbl_satuan.Satuan', className: 'dt-body-center'},
        {data: 'subtotal', className: 'dt-body-right', render: FormatCurrency}
    ];
    var reqData = {
        idsjc : $('#IDSJC').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlSuratJalanDetail, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    $('.select2').select2();

    setTableBarang();
    getNumberInvoice();

    $('#IDSJC').on('change', function() {
        if($(this).val() == ''){
            $('#Status_ppn').val('');
            $('#IDCustomer').val('');
            $('#Total_qty').val('');
            $('#DPP').val('');
            $('#PPN').val('');
            $('#NamaCustomer').val('');
            $('#Nama_di_faktur').val((''));
            $('#total_invoice').val((''));
            $('.total-invoice').val((''));
            
            setTableBarang();
        } else {
            var reqData = {
                'id'    : $(this).val()
            }
            ajaxData(urlSuratJalan, reqData, setFormData, false, false)
        }
    });

    $('#Jatuh_tempo').on('keyup', function() {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY"); 
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#diskonpersen').on('keyup', function() {
        var totalInvoice = ($('#total_invoice').val().length > 0) ? parseFloat($('#total_invoice').val().replace(/\./g,'').replace(',', '.')) : 0;
        var thisValRupiah = parseFloat(($(this).val() / 100) * totalInvoice);

        $('#diskonrupiah').val(thisValRupiah).trigger('keyup');
    });

    $('#diskonrupiah').on('keyup', function() {
        setTotalInvoice();

        var totalInvoice = ($('#total_invoice').val().length > 0) ? parseFloat($('#total_invoice').val().replace(/\./g,'').replace(',', '.')) : 0;
        var thisVal = ($(this).val().length > 0) ? parseFloat($(this).val().replace(/\./g,'').replace(',', '.')) : 0;
        var thisValPersen = parseFloat((thisVal / totalInvoice) * 100);

        $('#diskonpersen').val(thisValPersen.toFixed(2));
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})