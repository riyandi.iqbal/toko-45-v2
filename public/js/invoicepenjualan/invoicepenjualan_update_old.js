var tableData;

function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function fillCoa(result) {
    $('#IDCoa').html('');
    var html = '<option value="">-- Pilih COA --</option>';

    $.each((result), function(index, val) {
        var selected = (val.IDCoa == IDCoa) ? 'selected' : '';
        html +='<option value="'+val.IDCoa+'" data-namacoa="'+val.Nama_Coa+'" '+selected+'> '+val.Kode_COA+' '+val.Nama_COA+' </option>';
    });

    $('#IDCoa').append(html);
}

function setDateEnd(elemDateStart, Day, elemDateEnd) {
    var dateMomentObject = moment($('#' + elemDateStart).val(), "DD/MM/YYYY"); 
    var dateObject = dateMomentObject.toDate(); 

    var dateBeli = moment(dateObject).add(Day, 'days');
    var dateTempo = dateBeli.format('DD/MM/YYYY');
    
    $('#' + elemDateEnd).datepicker().destroy();

    var dateMomentObject = moment(dateTempo, "DD/MM/YYYY"); 
    var dateEndObject = dateMomentObject.toDate();

    $('#' + elemDateEnd).datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        value : dateTempo,
        change: function (e) {
            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });
}

function dateDiff(elem, dateStart, dateEnd) {
    var thisDateStart = new Date(dateStart);
    var thisDateEnd = new Date(dateEnd);
    
    var diffDays = Math.ceil((thisDateEnd - thisDateStart) / (1000 * 3600 * 24));
    $('#' + elem).val(diffDays);
}

function setNumberInvoice(result) {
    $('#Nomor').val(result)  
}

function getNumberInvoice() {
    var reqData = {
        'Status_ppn' : $('#Status_ppn').val()
    };

    ajaxData(urlNumberInvoice, reqData, setNumberInvoice, false, false);
}

function renderForm(data, type, row) {
    var html = data;

    html += '<input type="hidden" name="IDBarang[]" value="'+row.idbarang+'">';
    html += '<input type="hidden" name="Qty[]" value="'+row.qty+'">';
    html += '<input type="hidden" name="Harga[]" value="'+row.hargasatuan+'">';
    html += '<input type="hidden" name="Sub_total[]" value="'+row.subtotal+'">';
    html += '<input type="hidden" name="IDSatuan[]" value="'+row.IDSatuan+'">';

    return html;
}

function setTableBarang() {
    var colDef = [
        {data: 'idsjcdetail', render: renderNumRow },
        {data: 'Nama_Barang', name: 'tbl_barang.Nama_Barang', render: renderForm},
        {data: 'qty', className: 'dt-body-right'},
        {data: 'hargasatuan', className: 'dt-body-right', render: FormatCurrency},
        {data: 'Satuan', name: 'tbl_satuan.Satuan', className: 'dt-body-center'},
        {data: 'subtotal', className: 'dt-body-right', render: FormatCurrency}
    ];
    var reqData = {
        idsjc : $('#IDSJC').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-barang', urlSuratJalanDetail, colDef, reqData, reqOrder );
}

$(document).ready(function() {
    $('.select2').select2();

    if (retur_penjualan === true) {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();
    }

    if (Batal === true) {
        toggleForm('#form-data', false);
        $('#btn-add-barang, .hapus').hide();
    }
    
    $('#Jatuh_tempo').on('keyup', function() {
        setDateEnd('Tanggal', $(this).val(), 'Tanggal_jatuh_tempo')
    });

    $('#Tanggal_jatuh_tempo').datepicker({
        format : 'dd/mm/yyyy',
        autoclose : true,
        change: function (e) {
            var dateMomentObject = moment($('#Tanggal').val(), "DD/MM/YYYY"); 
            var dateObject = dateMomentObject.toDate();

            var dateMomentObject = moment($(this).val(), "DD/MM/YYYY"); 
            var dateEndObject = dateMomentObject.toDate();

            dateDiff('Jatuh_tempo', dateObject, dateEndObject);
        }
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            if ($(this).val() == 'giro') {
                $('#Nomor_giro').prop('disabled', false);
                $('#Tanggal_giro').prop('disabled', false);
            } else {
                $('#Nomor_giro').prop('disabled', true);
                $('#Tanggal_giro').prop('disabled', true);
            }
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#Jenis_Pembayaran').on('change', function() {
        if ($(this).val() == '') {
            $('#IDCoa').html('<option value="">-- Pilih COA --</option>');
        } else {
            var reqData = {
                Jenis_Pembayaran : $(this).val()
            };
    
            ajaxData(urlCoa, reqData, fillCoa, false, false);
        }
    });

    $('#Tanggal_jatuh_tempo').trigger('change');

    $('#Jenis_Pembayaran').val(Jenis_Pembayaran).trigger('change');

    if ($('#Status_ppn').val() == 'exclude') {
        $('.ppn').show();
    } else {
        $('.ppn').hide();
    }

    $('#form-data').validate({
        ignore: '*:not([name])',
        rules : {
            Tanggal : "required",
            IDSJC   : "required",
            Jatuh_tempo : {
                required : true,
                number : true,
            },
            Tanggal_jatuh_tempo : "required",
            DPP : "required",
            DPP : "required",
            total_invoice_diskon : "required"
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlUpdate, reqData, refresh, true);
        }
    });
    
})