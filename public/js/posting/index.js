var tableData;

var month = new Array();
month[1] = "Januari";
month[2] = "Februari";
month[3] = "Maret";
month[4] = "April";
month[5] = "Mei";
month[6] = "Juni";
month[7] = "Juli";
month[8] = "Agustus";
month[9] = "September";
month[10] = "Oktober";
month[11] = "November";
month[12] = "Desember";

function refresh(result) {
    alertSuccess(result.message);
    tableData.draw();
}

function renderStatus(data, type, row) {
    if (data == null || data == 0) {
        return 'Aktif';
    }

    return 'Dibatalkan';
}

function renderAction(data, type, row) {
    var button = '';

    return button;
}

function renderMonth(data) {
    return month[data];
}

function setTable() {
    var colDef = [
        {data: 'idposting', render: renderNumRow },
        {data: 'CTime', render: renderDate},
        {data: 'Bulan', render: renderMonth},
        {data: 'Tahun'},
        {data: 'idposting', className: 'dt-body-center', render: renderAction}
    ];
    
    var reqData = {
        field : $('#field').val(),
        keyword : $('#keyword1').val(),
    };

    var reqOrder = null;

    tableData = setDataTable('#table-data', urlData, colDef, reqData, reqOrder, null, null, false);
}

$(document).ready(function() {
    setTable();
    
    $('#cari-data').on('click', function() {
        setTable();
    });

});