function refresh(result) {
    alertSuccess(result.message, urlIndex);
}

function setSaldoAkhir(result) {
    $('#Saldo_laba_rugi').val(CurrencyFormat(result.data.saldo_laba_rugi, angkakoma));
    $('#Saldo_neraca').val(CurrencyFormat(result.data.saldo_neraca, angkakoma));
    $('#list-saldo').html('');
    var html ='';

    $.each(result.data.saldo_coa, function(idx, val) {
        if (val.debet == null) {
            val.debet = 0;
        }

        if (val.kredit == null) {
            val.kredit = 0;
        }
        
        html += '<tr>';

        html += '<td>'+(idx+1)+' <input type="hidden" name="IDCoa[]" value="'+val.IDCoa+'"> <input type="hidden" name="Debet[]" value="'+val.debet+'"> <input type="hidden" name="Kredit[]" value="'+val.kredit+'"> </td>';
        html += '<td>'+val.Nama_COA+'</td>';
        html += '<td style="text-align: right;">'+CurrencyFormat(val.debet, angkakoma)+'</td>';
        html += '<td style="text-align: right;">'+CurrencyFormat(val.kredit, angkakoma)+'</td>';

        html += '</tr>';
    });

    $('#list-saldo').append(html);
}

function getSaldoAkhir() {
    var reqData = {
        'Bulan' : $('#Bulan').val(),
        'Tahun' : $('#Tahun').val(),
    };

    ajaxData(urlSaldoAkhir, reqData, setSaldoAkhir);
}

$(document).ready(function() {
    $('.select2').select2();

    getSaldoAkhir();
    
    $('#Bulan').on('change', function() {
        getSaldoAkhir();
    });

    $('#Tahun').on('keyup', function() {
        getSaldoAkhir();
    });
    
    $('#form-data').validate({
        rules : {
            
        },
        submitHandler:function (form) {
            var reqData = new FormData(form);

            ajaxData(urlInsert, reqData, refresh, true);
        }
    });
    
})