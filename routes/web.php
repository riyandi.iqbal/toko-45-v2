<?php

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();
//===========================Route GET
Route::get('/logout', 'HomeController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('home/get_pendapatan', 'HomeController@get_pendapatan')->name('pendapatan');
Route::post('home/get_sales', 'HomeController@get_sales')->name('sales');
Route::post('home/get_purchase', 'HomeController@get_purchase')->name('purchase');

Route::get('Bank/datatable', 'BankController@datatable')->name('Bank.datatable');
Route::resource('Bank', 'BankController');
Route::post('Bank/update_data', 'BankController@update_data')->name('Bank.update_data');

Route::get('GroupCOA/datatable', 'GroupCoaController@datatable')->name('GroupCoa.datatable');
Route::resource('GroupCOA', 'GroupCoaController');
Route::post('GroupCOA/update_data', 'GroupCoaController@update_data')->name('GroupCOA.update_data');

Route::get('Coa/datatable', 'CoaController@datatable')->name('Coa.datatable');
Route::resource('Coa', 'CoaController');
Route::post('Coa/update_data', 'CoaController@update_data')->name('Coa.update_data');

Route::get('Matauang/datatable', 'MatauangController@datatable')->name('Matauang.datatable');
Route::get('/Matauang', 'MatauangController@index');
Route::get('/Matauang/tambah_data', 'MatauangController@tambah_data');
Route::get('/Matauang/edit/{id}', 'MatauangController@edit_data');
Route::delete('/Matauang/editstatus/{id}', 'MatauangController@editstatus');

Route::get('Kota/datatable', 'KotaController@datatable')->name('Kota.datatable');
Route::resource('Kota', 'KotaController');
Route::post('Kota/update_data', 'KotaController@update_data')->name('Kota.update_data');

Route::get('Gudang/datatable', 'GudangController@datatable')->name('Gudang.datatable');
Route::resource('Gudang', 'GudangController');
Route::post('Gudang/update_data', 'GudangController@update_data')->name('Gudang.update_data');

Route::get('GroupSupplier/datatable', 'GroupSupplierController@datatable')->name('GroupSupplier.datatable');
Route::resource('GroupSupplier', 'GroupSupplierController');
Route::post('GroupSupplier/update_data', 'GroupSupplierController@update_data')->name('GroupSupplier.update_data');

Route::get('Supplier/datatable', 'SupplierController@datatable')->name('Supplier.datatable');
Route::resource('Supplier', 'SupplierController');
Route::post('Supplier/update_data', 'SupplierController@update_data')->name('Supplier.update_data');

Route::get('GroupCustomer/datatable', 'GroupCustomerController@datatable')->name('GroupCustomer.datatable');
Route::resource('GroupCustomer', 'GroupCustomerController');
Route::post('GroupCustomer/update_data', 'GroupCustomerController@update_data')->name('GroupCustomer.update_data');

Route::get('Customer/datatable', 'CustomerController@datatable')->name('Customer.datatable');
Route::resource('Customer', 'CustomerController');
Route::post('Customer/update_data', 'CustomerController@update_data')->name('Customer.update_data');

Route::get('GroupBarang/datatable', 'GroupBarangController@datatable')->name('GroupBarang.datatable');
Route::get('/GroupBarang', 'GroupbarangController@index');
Route::get('/Groupbarang/tambah_group', 'GroupbarangController@tambah_data');
Route::get('/GroupBarang/edit_group_barang/{id}', 'GroupbarangController@editdata');
Route::delete('/GroupBarang/hapus_group_barang/{id}', 'GroupbarangController@hapusdata');

Route::get('Kategori/datatable', 'KategoriController@datatable')->name('Kategori.datatable');
Route::resource('Kategori', 'KategoriController');
Route::post('Kategori/update_data', 'KategoriController@update_data')->name('Kategori.update_data');

Route::get('Ukuran/datatable', 'UkuranController@datatable')->name('Ukuran.datatable');
Route::resource('Ukuran', 'UkuranController');
Route::post('Ukuran/update_data', 'UkuranController@update_data')->name('Ukuran.update_data');

Route::get('Satuan/datatable', 'SatuanController@datatable')->name('Satuan.datatable');
Route::resource('Satuan', 'SatuanController');
Route::post('Satuan/update_data', 'SatuanController@update_data')->name('Satuan.update_data');

Route::get('Tipe/datatable', 'TipeController@datatable')->name('Tipe.datatable');
Route::resource('Tipe', 'TipeController');
Route::post('Tipe/update_data', 'TipeController@update_data')->name('Tipe.update_data');

Route::get('/Barang', 'BarangController@index');
Route::get('Barang/datatable', 'BarangController@datatable')->name('Barang.datatable');
Route::get('/Barang/create', 'BarangController@create');
Route::get('/Barang/edit_barang/{id}', 'BarangController@update');
Route::delete('/Barang/hapus_barang/{id}', 'BarangController@editstatus');
Route::get('Barang/pencarian', 'BarangController@pencarian');

// Harga Jual //
Route::get('HargaJualBarang/datatable', 'HargaJualBarangController@datatable')->name('HargaJualBarang.datatable');
Route::post('HargaJualBarang/update_data', 'HargaJualBarangController@update_data')->name('HargaJualBarang.update_data');
Route::resource('HargaJualBarang', 'HargaJualBarangController');
Route::post('HargaJualBarang/get_satuan', 'HargaJualBarangController@get_satuan')->name('HargaJualBarang.get_satuan');

Route::get('Po/laporan', 'POController@laporan')->name('Po.laporan');
Route::get('Po/datatable', 'POController@datatable')->name('Po.datatable');
Route::get('Po/datatable_detail', 'POController@datatable_detail')->name('Po.datatable_detail');
Route::get('Po/get_barang', 'POController@get_barang')->name('Po.get_barang');
Route::get('Po/get_satuan', 'POController@get_satuan')->name('Po.get_satuan');
Route::get('Po/get_harga_jual', 'POController@get_harga_jual')->name('Po.get_harga_jual');
Route::get('Po/laporan_po', 'POController@laporan_po')->name('Po.laporan_po');
Route::resource('Po', 'POController');
Route::post('Po/number_so', 'POController@number_so')->name('Po.number');
Route::post('Po/get_coa', 'POController@get_coa')->name('Po.get_coa');
Route::post('Po/update_data', 'POController@update_data')->name('Po.update_data');
Route::get('Po/detail/{id}', 'POController@detail')->name('Po.detail');
Route::get('Po/print/{id}', 'POController@print')->name('Po.print');

// PENERIMAAN BARANG //
Route::get('PenerimaanBarang/datatable_detail', 'PenerimaanBarangController@datatable_detail')->name('PenerimaanBarang.datatable_detail');
Route::get('PenerimaanBarang/datatable_laporan', 'PenerimaanBarangController@datatable_laporan')->name('PenerimaanBarang.datatable_laporan');
Route::get('PenerimaanBarang/datatable', 'PenerimaanBarangController@datatable')->name('PenerimaanBarang.datatable');
Route::get('PenerimaanBarang/get_purchase_order_detail', 'PenerimaanBarangController@get_purchase_order_detail')->name('PenerimaanBarang.purchase_order_detail');
Route::get('Laporan_penerimaan_barang/index', 'PenerimaanBarangController@laporan')->name('PenerimaanBarang.laporan');
Route::resource('PenerimaanBarang', 'PenerimaanBarangController');
Route::post('PenerimaanBarang/get_purchase_order', 'PenerimaanBarangController@get_purchase_order')->name('PenerimaanBarang.purchase_order');
Route::get('PenerimaanBarang/detail/{id}', 'PenerimaanBarangController@detail')->name('PenerimaanBarang.detail');
Route::get('PenerimaanBarang/print/{id}', 'PenerimaanBarangController@print')->name('PenerimaanBarang.print');
Route::post('PenerimaanBarang/update_data', 'PenerimaanBarangController@update_data')->name('PenerimaanBarang.update_data');

// INVOICE PEMBELIAN //
Route::get('InvoicePembelian/laporan', 'InvoicePembelianController@laporan')->name('InvoicePembelian.laporan');
Route::get('InvoicePembelian/datatable', 'InvoicePembelianController@datatable')->name('InvoicePembelian.datatable');
Route::get('InvoicePembelian/datatable_detail', 'InvoicePembelianController@datatable_detail')->name('InvoicePembelian.datatable_detail');
Route::get('InvoicePembelian/get_penerimaan_barang_detail', 'InvoicePembelianController@get_penerimaan_barang_detail')->name('InvoicePembelian.penerimaan_barang_detail');
Route::get('InvoicePembelian/laporan_invoice', 'InvoicePembelianController@laporan_invoice')->name('InvoicePembelian.laporan_invoice');
Route::get('InvoicePembelian/datatable_laporan', 'InvoicePembelianController@datatable_laporan')->name('InvoicePembelian.datatable_laporan');
Route::resource('InvoicePembelian', 'InvoicePembelianController');
Route::post('InvoicePembelian/number_invoice', 'InvoicePembelianController@number_invoice')->name('InvoicePembelian.number');
Route::post('InvoicePembelian/get_penerimaan_barang', 'InvoicePembelianController@get_penerimaan_barang')->name('InvoicePembelian.penerimaan_barang');
// Route::get('InvoicePembelian/detail/{id}', 'InvoicePembelianController@detail')->name('InvoicePembelian.detail');
Route::get('InvoicePembelian/print/{id}', 'InvoicePembelianController@print')->name('InvoicePembelian.print');
Route::post('InvoicePembelian/get_coa', 'InvoicePembelianController@get_coa')->name('InvoicePembelian.get_coa');
Route::post('InvoicePembelian/update_data', 'InvoicePembelianController@update_data')->name('InvoicePembelian.update_data');
Route::get('InvoicePembelian/get_barang', 'InvoicePembelianController@get_barang')->name('InvoicePembelian.get_barang');
Route::get('InvoicePembelian/get_satuan', 'InvoicePembelianController@get_satuan')->name('InvoicePembelian.get_satuan');
Route::get('InvoicePembelian/get_harga_jual', 'InvoicePembelianController@get_harga_jual')->name('InvoicePembelian.get_harga_jual');
Route::get('/InvoicePembelian/show/{id}', 'InvoicepembelianController@showdata');

Route::get('ReturPembelian/get_pembelian', 'ReturPembelianController@get_pembelian')->name('ReturPembelian.get_pembelian');
Route::get('ReturPembelian/laporan', 'ReturPembelianController@index')->name('ReturPembelian.laporan');
Route::get('ReturPembelian/datatable', 'ReturPembelianController@datatable')->name('ReturPembelian.datatable');
Route::get('ReturPembelian/datatable_detail', 'ReturPembelianController@datatable_detail')->name('ReturPembelian.datatable_detail');
Route::get('ReturPembelian/get_pembelian_detail', 'ReturPembelianController@get_pembelian_detail')->name('ReturPembelian.pembelian_detail');
Route::get('ReturPembelian/laporan_retur_pembelian', 'ReturPembelianController@laporan_retur')->name('ReturPembelian.laporan_retur');
Route::get('ReturPembelian/datatable_laporan', 'ReturPembelianController@datatable_laporan')->name('ReturPembelian.datatable_laporan');
Route::resource('ReturPembelian', 'ReturPembelianController');
Route::post('ReturPembelian/get_pembelian', 'ReturPembelianController@get_pembelian')->name('ReturPembelian.pembelian');
Route::get('ReturPembelian/detail/{id}', 'ReturPembelianController@detail')->name('ReturPembelian.detail');
Route::get('ReturPembelian/print/{id}', 'ReturPembelianController@print')->name('ReturPembelian.print');

//=====================================
//===========================Route POST
//=====================================

Route::post('coa/simpancoa', 'CoaController@simpandatacoa');
Route::post('coa/simpandataedit', 'CoaController@simpandataeditcoa');
Route::post('coa/pencariancoa', 'CoaController@pencariancoa');

Route::post('/Matauang/pencarian', 'MatauangController@pencarian');
Route::post('/Matauang/simpandata', 'MatauangController@simpandata');
Route::post('/Matauang/addidr', 'MatauangController@addidr');
Route::post('/Matauang/edit_matauang', 'MatauangController@edit_matauang');

Route::post('/Customer/caricustomer', 'CustomerController@pencarian');
Route::post('/Customer/simpangroup', 'CustomerController@groupcreate');
Route::post('/Customer/simpankota', 'CustomerController@kotacreate');
Route::post('/Customer/simpandatacustomer', 'CustomerController@simpancustomer');
Route::post('/Customer/simpanedit', 'CustomerController@editcustomersimpan');
Route::post('/Customer/kode_customer', 'CustomerController@kode_customer')->name('Customer.kode');

Route::post('/Groupbarang/pencarian', 'GroupbarangController@pencarian');
Route::post('/Groupbarang/simpandata', 'GroupbarangController@simpandata');
Route::post('/GroupBarang/simpandataedit', 'GroupbarangController@simpandataedit');
Route::post('GroupBarang/simpangroupbaru', 'GroupbarangController@simpangroupbaru');

Route::post('/Satuan/pencarian', 'SatuanController@pencarian');
Route::post('/Satuan/simpandata', 'SatuanController@simpandata');
Route::post('/Satuan/simpanedit', 'SatuanController@simpanedit');

Route::post('/Barang/pencarian', 'BarangController@pencarian');
Route::post('/Barang/proses_group_barang', 'BarangController@simpangroup');
Route::post('/Barang/ambiltipe', 'BarangController@caritipe');
Route::post('/Barang/ambilukuran', 'BarangController@cariukuran');
Route::post('/Barang/ambilcode', 'BarangController@caricode');
Route::post('/Barang/simpandata', 'BarangController@simpandata');
Route::post('/Barang/simpandataedit', 'BarangController@simpandataedit');
Route::post('/Barang/ambilcode2', 'BarangController@caricode2');
Route::post('/Barang/ambilcode3', 'BarangController@caricode3');
Route::post('/Barang/get_kategori', 'BarangController@get_kategori')->name('Barang.kategori');
Route::post('Barang/ambilgroup', 'BarangController@carigroup');
Route::post('Barang/ambilkodedarigroup', 'BarangController@kodedarigroup');
Route::post('Barang/ambilnamadarigroup', 'BarangController@namadarigroup');
Route::post('Barang/ambiltypekodedarigroup', 'BarangController@typekodedarigroup');
Route::post('Barang/ambilkategorikodedarigroup', 'BarangController@kategorikodedarigroup');
Route::post('Barang/ambilukurankodedarigroup', 'BarangController@ukurankodedarigroup');
Route::post('Barang/getinukuran', 'BarangController@getinukuran');
Route::post('Barang/getinkategori', 'BarangController@getinkategori');
Route::post('Barang/getintype', 'BarangController@getintype');
Route::post('Barang/ambiltypenamadarigroup', 'BarangController@typenamadarigroup');
Route::post('Barang/ambilkategorinamadarigroup', 'BarangController@kategorinamadarigroup');
Route::post('Barang/ambilukurannamadarigroup', 'BarangController@ukurannamadarigroup');

// PEMBAYARAN HUTANG //
Route::get('PembayaranHutang/datatable', 'PembayaranHutangController@datatable')->name('PembayaranHutang.datatable');
Route::get('PembayaranHutang/laporan', 'PembayaranHutangController@laporan')->name('PembayaranHutang.laporan');
Route::get('PembayaranHutang/get_hutang', 'PembayaranHutangController@get_hutang')->name('PembayaranHutang.hutang');
Route::get('PembayaranHutang/datatable', 'PembayaranHutangController@datatable')->name('PembayaranHutang.datatable');
Route::get('PembayaranHutang/datatable_detail', 'PembayaranHutangController@datatable_detail')->name('PembayaranHutang.datatable_detail');
Route::resource('PembayaranHutang', 'PembayaranHutangController');
Route::post('PembayaranHutang/number_invoice', 'PembayaranHutangController@number_invoice')->name('PembayaranHutang.number');
Route::post('PembayaranHutang/get_coa', 'PembayaranHutangController@get_coa')->name('PembayaranHutang.get_coa');
Route::get('PembayaranHutang/detail/{id}', 'PembayaranHutangController@detail')->name('PembayaranHutang.detail');

// ROUTE SALES ORDER //
Route::get('SalesOrder/laporan', 'SalesOrderController@laporan')->name('SalesOrder.laporan');
Route::get('SalesOrder/datatable', 'SalesOrderController@datatable')->name('SalesOrder.datatable');
Route::get('SalesOrder/datatable_detail', 'SalesOrderController@datatable_detail')->name('SalesOrder.datatable_detail');
Route::get('SalesOrder/get_barang', 'SalesOrderController@get_barang')->name('SalesOrder.get_barang');
Route::get('SalesOrder/get_satuan', 'SalesOrderController@get_satuan')->name('SalesOrder.get_satuan');
Route::get('SalesOrder/get_harga_jual', 'SalesOrderController@get_harga_jual')->name('SalesOrder.get_harga_jual');
Route::resource('SalesOrder', 'SalesOrderController');
Route::post('SalesOrder/number_so', 'SalesOrderController@number_so')->name('SalesOrder.number');
Route::post('SalesOrder/get_coa', 'SalesOrderController@get_coa')->name('SalesOrder.get_coa');
Route::post('SalesOrder/update_data', 'SalesOrderController@update_data')->name('SalesOrder.update_data');
Route::get('SalesOrder/detail/{id}', 'SalesOrderController@detail')->name('SalesOrder.detail');
Route::get('SalesOrder/print/{id}', 'SalesOrderController@print')->name('SalesOrder.print');

Route::get('PersetujuanSalesOrder/datatable', 'PersetujuanSalesOrderController@datatable')->name('PersetujuanSalesOrder.datatable');
Route::resource('PersetujuanSalesOrder', 'PersetujuanSalesOrderController');
Route::post('PersetujuanSalesOrder/approved', 'PersetujuanSalesOrderController@approved')->name('PersetujuanSalesOrder.approved');
Route::get('PersetujuanSalesOrder/rejected/{id}', 'PersetujuanSalesOrderController@rejected')->name('PersetujuanSalesOrder.rejected');

// SURAT JALAN //
Route::get('SuratJalan/laporan', 'SuratJalanController@laporan')->name('SuratJalan.laporan');
Route::get('SuratJalan/datatable_detail', 'SuratJalanController@datatable_detail')->name('SuratJalan.datatable_detail');
Route::get('SuratJalan/datatable', 'SuratJalanController@datatable')->name('SuratJalan.datatable');
Route::get('SuratJalan/get_sales_order_detail', 'SuratJalanController@get_sales_order_detail')->name('SuratJalan.sales_order_detail');
Route::resource('SuratJalan', 'SuratJalanController');
Route::post('SuratJalan/number_invoice', 'SuratJalanController@number_invoice')->name('SuratJalan.number');
Route::post('SuratJalan/get_sales_order', 'SuratJalanController@get_sales_order')->name('SuratJalan.sales_order');
Route::get('SuratJalan/detail/{id}', 'SuratJalanController@detail')->name('SuratJalan.detail');
Route::get('SuratJalan/print/{id}', 'SuratJalanController@print')->name('SuratJalan.print');
Route::get('SuratJalan/confirm/{id}', 'SuratJalanController@confirm')->name('SuratJalan.confirm');
Route::post('SuratJalan/set_so', 'SuratJalanController@set_so')->name('SuratJalan.set_so');

// INVOICE PENJUALAN //
Route::get('InvoicePenjualan/laporan', 'InvoicePenjualanController@laporan')->name('InvoicePenjualan.laporan');
Route::get('InvoicePenjualan/datatable', 'InvoicePenjualanController@datatable')->name('InvoicePenjualan.datatable');
Route::get('InvoicePenjualan/datatable_detail', 'InvoicePenjualanController@datatable_detail')->name('InvoicePenjualan.datatable_detail');
Route::get('InvoicePenjualan/get_surat_jalan_detail', 'InvoicePenjualanController@get_surat_jalan_detail')->name('InvoicePenjualan.surat_jalan_detail');
Route::resource('InvoicePenjualan', 'InvoicePenjualanController');
Route::post('InvoicePenjualan/number_invoice', 'InvoicePenjualanController@number_invoice')->name('InvoicePenjualan.number');
Route::post('InvoicePenjualan/get_surat_jalan', 'InvoicePenjualanController@get_surat_jalan')->name('InvoicePenjualan.surat_jalan');
Route::get('InvoicePenjualan/detail/{id}', 'InvoicePenjualanController@detail')->name('InvoicePenjualan.detail');
Route::get('InvoicePenjualan/print/{id}', 'InvoicePenjualanController@print')->name('InvoicePenjualan.print');
Route::post('InvoicePenjualan/get_coa', 'InvoicePenjualanController@get_coa')->name('InvoicePenjualan.get_coa');
Route::post('InvoicePenjualan/update_data2', 'InvoicePenjualanController@update_data2')->name('InvoicePenjualan.update_data2');
Route::get('InvoicePenjualan/get_barang', 'InvoicePenjualanController@get_barang')->name('InvoicePenjualan.get_barang');
Route::get('InvoicePenjualan/get_satuan', 'InvoicePenjualanController@get_satuan')->name('InvoicePenjualan.get_satuan');
Route::get('InvoicePenjualan/get_harga_jual', 'InvoicePenjualanController@get_harga_jual')->name('InvoicePenjualan.get_harga_jual');

// Bahan Baku //
Route::get('Bahan_baku/datatable', 'BahanbakuController@datatable')->name('Bahan_baku.datatable');
Route::get('/Bahan_baku', 'BahanbakuController@index');
Route::get('/Bahanbaku', 'BahanbakuController@index');
Route::get('/Bahan_baku/produksi', 'BahanbakuController@index');
Route::get('/Bahan_baku/tambah_produksi', 'BahanbakuController@tambahdata');
Route::get('/Bahan_baku/tambah_produksigudang/{id}', 'BahanbakuController@tambahdata2');
Route::post('/Bahanbaku/ambilsatuan', 'BahanbakuController@ambilsatuan');
Route::post('/Bahan_baku/cekstok', 'BahanbakuController@cekstok');
Route::post('/Bahan_baku/simpandataproduksi', 'BahanbakuController@simpandata');
Route::post('Bahanbaku/cekdatadetailformula', 'BahanbakuController@cekdetailformula');
Route::get('Bahan_baku/produksi_show/{id}', 'BahanbakuController@showdata');
Route::get('Bahan_baku/produksi_edit/{id}', 'BahanbakuController@editdata');
Route::post('Bahan_baku/simpandataproduksiedit', 'BahanbakuController@simpanedit');
Route::get('Bahanbaku/printdata/{id}', 'BahanbakuController@printdata');
Route::post('Bahanbaku/pencarian', 'BahanbakuController@caridata');

Route::delete('Bahan_baku/{id}', 'BahanbakuController@destroy');

// RETUR PENJUALAN //
Route::get('ReturPenjualan/get_penjualan_customer', 'ReturPenjualanController@get_penjualan_customer')->name('ReturPenjualan.get_penjualan_customer');
Route::get('ReturPenjualan/laporan', 'ReturPenjualanController@index')->name('ReturPenjualan.laporan');
Route::get('ReturPenjualan/datatable', 'ReturPenjualanController@datatable')->name('ReturPenjualan.datatable');
Route::get('ReturPenjualan/datatable_detail', 'ReturPenjualanController@datatable_detail')->name('ReturPenjualan.datatable_detail');
Route::get('ReturPenjualan/get_penjualan_detail', 'ReturPenjualanController@get_penjualan_detail')->name('ReturPenjualan.penjualan_detail');
Route::resource('ReturPenjualan', 'ReturPenjualanController');
Route::post('ReturPenjualan/number_invoice', 'ReturPenjualanController@number_invoice')->name('ReturPenjualan.number');
Route::post('ReturPenjualan/get_penjualan', 'ReturPenjualanController@get_penjualan')->name('ReturPenjualan.penjualan');
Route::get('ReturPenjualan/detail/{id}', 'ReturPenjualanController@detail')->name('ReturPenjualan.detail');
Route::get('ReturPenjualan/print/{id}', 'ReturPenjualanController@print')->name('ReturPenjualan.print');

// FORMULA //
Route::get('Formula/datatable', 'FormulaController@datatable')->name('Formula.datatable');
Route::get('Formula', 'FormulaController@index');
Route::get('/Formula/tambahdata', 'FormulaController@tambah_data');
Route::post('Formula/simpandata', 'FormulaController@simpandata');
Route::get('Formula/show/{id}', 'FormulaController@showdata');
Route::delete('Formula/hapus_data/{id}', 'FormulaController@hapusdata');
Route::post('Formula/getsatuan', 'FormulaController@getsatuan');
Route::get('Formula/edit/{id}', 'FormulaController@editdata');
Route::post('Formula/simpandataedit', 'FormulaController@simpandataedit');

// PENERIMAAN BARANG JADI //
Route::get('BarangJadi/datatable', 'PenerimaanBarangJadiController@datatable')->name('BarangJadi.datatable');
Route::get('BarangJadi', 'PenerimaanBarangJadiController@index');
Route::get('BarangJadi/tambahdata', 'PenerimaanBarangJadiController@create');
Route::post('PenerimaanBarangJadi/ambildetail', 'PenerimaanBarangJadiController@ambildetail');
Route::post('BarangJadi/simpandata', 'PenerimaanBarangJadiController@simpandata');
Route::get('BarangJadi/showdata/{id}', 'PenerimaanBarangJadiController@showdata');
Route::get('BarangJadi/printdata/{id}', 'PenerimaanBarangJadiController@printdata');
Route::get('BarangJadi/edit/{id}', 'PenerimaanBarangJadiController@editdata');
Route::post('BarangJadi/simpandataedit', 'PenerimaanBarangJadiController@editdatasimpan');
Route::delete('BarangJadi/{id}', 'PenerimaanBarangJadiController@destroy');

// PENERIMAAN PIUTANG //
Route::get('PenerimaanPiutang/laporan', 'PenerimaanPiutangController@laporan')->name('PenerimaanPiutang.laporan');
Route::get('PenerimaanPiutang/get_piutang', 'PenerimaanPiutangController@get_piutang')->name('PenerimaanPiutang.piutang');
Route::get('PenerimaanPiutang/datatable', 'PenerimaanPiutangController@datatable')->name('PenerimaanPiutang.datatable');
Route::get('PenerimaanPiutang/datatable_detail', 'PenerimaanPiutangController@datatable_detail')->name('PenerimaanPiutang.datatable_detail');
Route::resource('PenerimaanPiutang', 'PenerimaanPiutangController');
Route::post('PenerimaanPiutang/number_invoice', 'PenerimaanPiutangController@number_invoice')->name('PenerimaanPiutang.number');
Route::post('PenerimaanPiutang/get_coa', 'PenerimaanPiutangController@get_coa')->name('PenerimaanPiutang.get_coa');
Route::get('PenerimaanPiutang/detail/{id}', 'PenerimaanPiutangController@detail')->name('PenerimaanPiutang.detail');

// LAPORAN STOK //
Route::get('Stok/datatable', 'StokController@datatable')->name('Stok.datatable');
Route::get('Laporan_stok', 'StokController@index');
Route::post('stok/pencarian', 'StokController@caridata');
Route::get('stok/kartustok/{id}', 'StokController@kartustok');

// Persetujuan Pesanan //
Route::get('PersetujuanPesanan', 'PersetujuanPembelianController@index');
Route::get('PersetujuanPesanan/show/{id}', 'PersetujuanPembelianController@show');
Route::get('PersetujuanPesanan/approve/{id}', 'PersetujuanPembelianController@approve');
Route::get('PersetujuanPesanan/reject/{id}', 'PersetujuanPembelianController@reject');
Route::get('PersetujuanPesanan/datatable', 'PersetujuanPembelianController@datatable')->name('PersetujuanPesanan.datatable');

// Controll Panel //
Route::get('ControlPanel', 'ControlPanelController@index');
Route::get('settingcoa', 'ControlPanelController@settingcoa');
Route::post('ControlPanel/simpandatacoa', 'ControlPanelController@simpancoa');
Route::get('settingcoa/edit/{id}', 'ControlPanelController@editsetcoa');
Route::get('settingcoa/hapus/{id}', 'ControlPanelController@hapussetcoa');
Route::post('ControlPanel/simpandatacoaedit', 'ControlPanelController@simpancoaedit');
Route::get('settingperusahaan', 'ControlPanelController@setperusahaan');
Route::post('ControlPanel/simpandataperusahaan', 'ControlPanelController@simpanperusahaan');
Route::get('settingcarabayar', 'ControlPanelController@setcarabayar');
Route::post('ControlPanel/simpandatacarabayar', 'ControlPanelController@simpancarabayar');
Route::get('settingcarabayar/edit/{id}', 'ControlPanelController@editcarabayar');
Route::post('ControlPanel/simpancarabayaredit', 'ControlPanelController@simpancarabayaredit');
Route::get('settingcarabayarmenu', 'ControlPanelController@setcarabayarmenu');
Route::post('ControlPanel/simpanbayarmenu', 'ControlPanelController@simpanbayarmenu');
Route::get('ControlPanelCoa', 'ControlPanelController@indexcoa');
Route::get('uploadlogohead', 'ControlPanelController@uploadlogohead');
Route::post('ControlPanel/Simpangambarhead', 'ControlPanelController@simpanheadlogo');
Route::get('ControlPanelUser', 'ControlPanelController@indexuser');
Route::get('RegisterUser', 'ControlPanelController@tambahuser');
Route::post('RegisterUser/Simpan', 'ControlPanelController@simpanuser');
Route::get('User/edit/{id}', 'ControlPanelController@edituser');
Route::post('RegisterUser/Simpanedit', 'ControlPanelController@simpanedituser');
Route::get('ControlPanelGroupUser', 'ControlPanelController@indexgroupuser');
Route::get('TambahGroupUser', 'ControlPanelController@tambahgroupuser');
Route::post('GroupUser/simpandata', 'ControlPanelController@simpangroupuser');
Route::get('GroupUser/edit/{id}', 'ControlPanelController@editgroupuser');
Route::post('GroupUser/simpandataedit', 'ControlPanelController@simpangroupuseredit');
Route::get('GroupUser/akses/{id}', 'ControlPanelController@aksesgroup');
Route::post('GroupAksesSimpan', 'ControlPanelController@simpanakses');
Route::get('User/grouping/{id}', 'ControlPanelController@creategrouping');
Route::post('GroupingUserSimpan', 'ControlPanelController@simpangrouping');
Route::get('ControlPanelKeuangan', 'ControlPanelController@indexkeuangan');
Route::get('User/permission/{id}', 'ControlPanelController@indexpermission');
Route::post('PermissionSimpan', 'ControlPanelController@simpanpermission');
Route::post('ControlPanel/simpankodetransaksi', 'ControlPanelController@simpankodetrans');

// Pemakaian Barang //
Route::get('PemakaianBarang/datatable', 'PemakaianBarangController@datatable')->name('PemakaianBarang.datatable');
Route::get('PemakaianBarang', 'PemakaianBarangController@index');
Route::post('PemakaianBarang/indexcari', 'PemakaianBarangController@pencarian');
Route::get('PemakaianBarang/create', 'PemakaianBarangController@create');
Route::post('PemakaianBarang/simpandata', 'PemakaianBarangController@simpandata');
Route::get('PemakaianBarang/show/{id}', 'PemakaianBarangController@showdata');
Route::get('PemakaianBarang/printdata/{id}', 'PemakaianBarangController@printdata');
Route::delete('PemakaianBarang/hapus_data/{id}', 'PemakaianBarangController@hapusdata');

// Jurnal //
Route::get('Jurnal', 'JurnalController@index');

// Jurnal Umum //
Route::get('JurnalUmum/datatable', 'JurnalUmumController@datatable')->name('JurnalUmum.datatable');
Route::resource('JurnalUmum', 'JurnalUmumController');
Route::post('JurnalUmum/number_jurnal_umum', 'JurnalUmumController@number_jurnal_umum')->name('JurnalUmum.number');
Route::post('JurnalUmum/update_data', 'JurnalUmumController@update_data')->name('JurnalUmum.update_data');
Route::get('JurnalUmum/detail/{id}', 'JurnalUmumController@detail')->name('JurnalUmum.detail');

//LAPORAN//
Route::get('Laporan', 'LaporanController@index');
Route::get('Po/toexcel', 'LaporanController@purchaseorderexcel');
Route::get('Laporan_penerimaan_barang/toexcel', 'LaporanController@penerimaanbarangexcel');
Route::get('LaporanInvoice/toexcel', 'LaporanController@invoicebeliexcel');
Route::get('LaporanReturBeli/toexcel', 'LaporanController@returbeliexcel');
Route::get('LaporanSalesOrder/toexcel', 'LaporanController@salesorderexcel');
Route::get('LaporanInvoicejual/toexcel', 'LaporanController@invoicejualexcel');
Route::get('LaporanReturjual/toexcel', 'LaporanController@returjualexcel');
Route::get('LaporanSuratjalan/toexcel', 'LaporanController@suratjalanexcel');
Route::get('LaporanPembayaranhutang/toexcel', 'LaporanController@bayarhutangexcel');
Route::get('LaporanPenerimaanpiutang/toexcel', 'LaporanController@terimapiutangexcel');
Route::get('Laporan/Pembelian', 'LaporanController@pembelian');
Route::get('Laporan/Penjualan', 'LaporanController@penjualan');
Route::get('Laporan/Keuangan', 'LaporanController@keuangan');
Route::get('Laporan/Persediaan', 'LaporanController@persediaan');

// KEUANGAN //
Route::get('BalanceSheet', 'KeuanganController@balancesheet');
Route::post('BalanceSheet/simpandata', 'KeuanganController@simpanbs');
Route::get('LaporanNeraca', 'KeuanganController@balancesheetindex');
Route::post('LaporanNeraca/cari_bulan_lr', 'KeuanganController@cari_bulan_lr');
Route::post('BalanceSheet/ambilkode', 'KeuanganController@carikodecoa');
Route::get('BalanceSheet/editdata/{id}', 'KeuanganController@editbs');
Route::post('BalanceSheet/get_laporan', 'KeuanganController@get_laporan_neraca')->name('neraca.laporan');

Route::get('LabaRugi', 'KeuanganController@labarugi');
Route::post('LabaRugi/simpandata', 'KeuanganController@simpanlabarugi');
Route::get('LaporanLabaRugi', 'KeuanganController@labarugiindex');
Route::post('LabaRugi/get_laporan', 'KeuanganController@get_laporan')->name('labarugi.laporan');

Route::get('LaporanGeneralLedger', 'KeuanganController@glindex');
Route::post('GeneralLedger/get_laporan', 'KeuanganController@get_laporan')->name('generalledger.laporan');
Route::get('GeneralLedger/datatable', 'KeuanganController@datatable')->name('GeneralLedger.datatable');

// MUTASI GIRO //
Route::get('MutasiGiro/laporan', 'MutasiGiroController@laporan')->name('MutasiGiro.laporan');
Route::get('MutasiGiro/datatable_detail', 'MutasiGiroController@datatable_detail')->name('MutasiGiro.datatable_detail');
Route::get('MutasiGiro/datatable', 'MutasiGiroController@datatable')->name('MutasiGiro.datatable');
Route::get('MutasiGiro/get_perusahaan', 'MutasiGiroController@get_perusahaan')->name('MutasiGiro.perusahaan');
Route::get('MutasiGiro/get_nomor_giro', 'MutasiGiroController@get_nomor_giro')->name('MutasiGiro.nomor_giro');
Route::resource('MutasiGiro', 'MutasiGiroController');
Route::post('MutasiGiro/number', 'MutasiGiroController@number')->name('MutasiGiro.number');
Route::post('MutasiGiro/update_data', 'MutasiGiroController@update_data')->name('MutasiGiro.update_data');
Route::get('MutasiGiro/detail/{id}', 'MutasiGiroController@detail')->name('MutasiGiro.detail');
Route::post('MutasiGiro/get_mutasi_giro_detail', 'MutasiGiroController@get_mutasi_giro_detail')->name('MutasiGiro.mutasi_giro_detail');

Route::post('LaporanLabaRugi/cari_bulan_pl', 'KeuanganController@cari_bulan_pl');

// PERMISSION //
Route::post('Permission/tambah', 'PermissionController@cektambah');
Route::post('Permission/ubah', 'PermissionController@cekubah');

// Koreksi Persediaan //
Route::get('KoreksiPersediaan/laporan', 'KoreksiPersediaanController@laporan')->name('KoreksiPersediaan.laporan');
Route::get('KoreksiPersediaan/datatable_detail', 'KoreksiPersediaanController@datatable_detail')->name('KoreksiPersediaan.datatable_detail');
Route::get('KoreksiPersediaan/datatable', 'KoreksiPersediaanController@datatable')->name('KoreksiPersediaan.datatable');
Route::get('KoreksiPersediaan/get_barang', 'KoreksiPersediaanController@get_barang')->name('KoreksiPersediaan.get_barang');
Route::resource('KoreksiPersediaan', 'KoreksiPersediaanController');
Route::post('KoreksiPersediaan/number', 'KoreksiPersediaanController@number')->name('KoreksiPersediaan.number');
Route::post('KoreksiPersediaan/update_data', 'KoreksiPersediaanController@update_data')->name('KoreksiPersediaan.update_data');
Route::get('KoreksiPersediaan/detail/{id}', 'KoreksiPersediaanController@detail')->name('KoreksiPersediaan.detail');
Route::post('KoreksiPersediaan/get_mutasi_giro_detail', 'KoreksiPersediaanController@get_mutasi_giro_detail')->name('KoreksiPersediaan.mutasi_giro_detail');

// Pindah Gudang //
Route::get('PindahGudang/laporan', 'PindahGudangController@laporan')->name('PindahGudang.laporan');
Route::get('PindahGudang/datatable', 'PindahGudangController@datatable')->name('PindahGudang.datatable');
Route::get('PindahGudang/datatable_detail', 'PindahGudangController@datatable_detail')->name('PindahGudang.datatable_detail');
Route::get('PindahGudang/get_barang', 'PindahGudangController@get_barang')->name('PindahGudang.get_barang');
Route::resource('PindahGudang', 'PindahGudangController');
Route::post('PindahGudang/number', 'PindahGudangController@number')->name('PindahGudang.number');
Route::post('PindahGudang/update_data', 'PindahGudangController@update_data')->name('PindahGudang.update_data');
Route::get('PindahGudang/detail/{id}', 'PindahGudangController@detail')->name('PindahGudang.detail');
Route::post('PindahGudang/get_mutasi_giro_detail', 'PindahGudangController@get_mutasi_giro_detail')->name('PindahGudang.mutasi_giro_detail');

// SATUAN KONVERSI //
Route::get('SatuanKonversi/datatable', 'SatuanKonversiController@datatable')->name('SatuanKonversi.datatable');
Route::post('SatuanKonversi/update_data', 'SatuanKonversiController@update_data')->name('SatuanKonversi.update_data');
Route::resource('SatuanKonversi', 'SatuanKonversiController');
Route::post('SatuanKonversi/get_satuan', 'SatuanKonversiController@get_satuan')->name('SatuanKonversi.get_satuan');

// KODE TRANSAKSI //
Route::get('KodeTransaksi', 'KodeTransaksiController@index');
Route::get('KodeTransaksi/tambah_data', 'KodeTransaksiController@create');
Route::get('Editkodetransaksi/{id}', 'KodeTransaksiController@edit');
Route::post('KodeTransaksi/simpan_kodetransaksi', 'KodeTransaksiController@simpandata');
Route::get('ambilnomorpononppn', 'KodeTransaksiController@purchaseordernonppn');
Route::get('ambilnomorpoppn', 'KodeTransaksiController@purchaseorderppn');
Route::get('ambilnomorpb', 'KodeTransaksiController@penerimaanbarang');
Route::get('ambilnomorinvoice', 'KodeTransaksiController@invoicepembelian');
Route::get('ambilnomorrb', 'KodeTransaksiController@returbeli');
Route::get('ambilnomorsononppn', 'KodeTransaksiController@salesordernonppn');
Route::get('ambilnomorsoppn', 'KodeTransaksiController@salesorderppn');
Route::get('ambilnomorsj', 'KodeTransaksiController@suratjalan');
Route::get('ambilnomorfj', 'KodeTransaksiController@invoicepenjualan');
Route::get('ambilnomorrjnonppn', 'KodeTransaksiController@returjualnonppn');
Route::get('ambilnomorrjppn', 'KodeTransaksiController@returjualppn');

// POSTING //

Route::get('Posting/datatable', 'PostingController@datatable')->name('Posting.datatable');
Route::resource('Posting', 'PostingController');
Route::post('Posting/get_saldo_akhir', 'PostingController@get_saldo_akhir')->name('Posting.get_saldo_akhir');

Route::get('/UpdatePajakPembelian', 'InvoicePembelianController@index2');