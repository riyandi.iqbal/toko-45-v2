<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_bank', function (Blueprint $table) {
            $table->string('IDBank', 64);
            $table->string('Nomor_Rekening', 25)->nullable();
            $table->string('IDCoa', 64);
            $table->string('Atas_Nama', 100)->nullable();
            $table->string('Aktif', 64);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_bank');
    }
}
