<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_customer', function (Blueprint $table) {
            $table->string('IDCustomer', 64);
            $table->string('IDGroupCustomer', 64);
            $table->string('Kode_Customer', 64);
            $table->string('Nama', 100);
            $table->text('Alamat', 64);
            $table->string('No_Telpon', 20);
            $table->string('IDKota', 64);
            $table->string('Fax', 64);
            $table->string('Email', 100);
            $table->string('NPWP', 64);
            $table->string('No_KTP', 20);
            $table->string('Aktif', 20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_customer');
    }
}
