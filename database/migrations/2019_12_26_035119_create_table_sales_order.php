<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSalesOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sales_order', function (Blueprint $table) {
            $table->string('IDSOK', 64);
            $table->date('Tanggal')->nullable();
            $table->string('Nomor', 64);
            $table->string('IDCustomer', 64);
            $table->integer('TOP', 64);
            $table->date('Tanggal_jatuh_tempo')->nullable();
            $table->string('IDMataUang', 64);
            $table->integer('Kurs', 64);
            $table->float('Total_qty', 53, 0);
            $table->float('Saldo_qty', 53, 0);
            $table->string('No_po_customer', 64)->nullable();
            $table->float('Grand_total', 53, 0);
            $table->text('Keterangan')->nullable();
            $table->string('Batal', 50)->default(0);
            $table->string('Status_ppn', 50);
            $table->float('DPP', 53, 0);
            $table->float('PPN', 53, 0);
            $table->string('dibuat_pada', 64);
            $table->string('diubah_pada', 64);

            $table->primary('IDSOK');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sales_order');
    }
}
