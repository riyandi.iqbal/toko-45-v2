<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSalesOrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sales_order_detail', function (Blueprint $table) {
            $table->string('IDSOKDetail', 64);
            $table->string('IDSOK', 64);
            $table->string('IDBarang', 64);
            $table->string('IDCorak', 64);
            $table->string('IDMerk', 64);
            $table->string('IDWarna', 64);
            $table->float('Qty', 53, 0);
            $table->string('IDSatuan', 64);
            $table->float('Harga', 53, 0);
            $table->float('Sub_total', 53, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_sales_order_detail');
    }
}
