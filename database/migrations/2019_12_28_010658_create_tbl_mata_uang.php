<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblMataUang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_mata_uang', function (Blueprint $table) {
            $table->string('IDMataUang', 64);
            $table->string('Kode', 30)->nullable();
            $table->string('Mata_uang', 30)->nullable();
            $table->string('Negara', 200)->nullable();
            $table->string('Default', 30)->nullable();
            $table->string('Kurs', 100)->nullable();
            $table->string('Aktif', 40)->nullable();
            $table->timestamp('Tanggal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_mata_uang');
    }
}
