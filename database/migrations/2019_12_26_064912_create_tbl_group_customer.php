<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblGroupCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl__group_customer', function (Blueprint $table) {
            $table->string('IDGroupCustomer', 64);
            $table->string('Kode_Group_Customer', 64);
            $table->string('Nama_Group_Customer', 64);
            $table->string('Aktif', 64);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl__group_customer');
    }
}
