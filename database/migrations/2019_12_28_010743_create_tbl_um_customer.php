<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUmCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_um_customer', function (Blueprint $table) {
            $table->string('IDUMCustomer', 64);
            $table->string('IDCustomer', 30);
            $table->date('Tanggal_UM');
            $table->string('Nomor_Faktur', 20)->nullable();
            $table->float('Nilai_UM', 53, 0)->nullable();
            $table->float('Saldo_UM', 53, 0)->nullable();
            $table->string('IDFaktur', 64)->nullable();
            $table->float('Pembayaran', 53, 0)->nullable();
            $table->dateTime('dibuat_pada');
            $table->dateTime('diubah_pada');
            $table->string('Jenis_Pembayaran', 64)->nullable();
            $table->string('IDCoa', 64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_um_customer');
    }
}
