<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPenjualanDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_penjualan_detail', function (Blueprint $table) {
            $table->string('IDFJDetail', 64);
            $table->string('IDFJ', 64);
            $table->string('IDBarang', 64);
            $table->float('Qty', 64, 0);
            $table->float('Saldo_qty', 64, 0);
            $table->string('IDSatuan', 64);
            $table->float('Harga', 64, 0);
            $table->float('Sub_total', 64, 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_penjualan_detail');
    }
}
