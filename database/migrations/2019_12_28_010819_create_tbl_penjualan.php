<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_penjualan', function (Blueprint $table) {
            $table->string('IDFJ', 64);
            $table->date('Tanggal');
            $table->string('Nomor', 64);
            $table->string('IDCustomer', 64);
            $table->string('Nama_di_faktur', 64);
            $table->string('IDSJCK', 64);
            $table->integer('TOP', 64);
            $table->date('Tanggal_jatuh_tempo')->nullable();
            $table->string('IDMataUang', 64);
            $table->integer('Kurs', 64);
            $table->float('Total_qty', 53, 0);
            $table->float('Saldo_qty', 53, 0);
            $table->float('Discount', 53, 2);
            $table->float('Grand_total', 53, 0);
            $table->text('Keterangan')->nullable();
            $table->string('Batal', 50)->default(0);
            $table->string('Status_ppn', 50);
            $table->float('DPP', 53, 2);
            $table->float('PPN', 53, 2);
            $table->dateTime('dibuat_pada');
            $table->dateTime('diubah_pada');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_penjualan');
    }
}
