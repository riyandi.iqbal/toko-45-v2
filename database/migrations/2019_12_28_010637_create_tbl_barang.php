<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_barang', function (Blueprint $table) {
            $table->string('IDBarang', 64);
            $table->string('IDGroupBarang', 64)->nullable();
            $table->string('Kode_Barang', 64)->nullable();
            $table->string('Nama_Barang', 200)->nullable();
            $table->string('IDSatuan', 64)->nullable();
            $table->string('Aktif', 64)->nullable();
            $table->string('IDKategori', 64)->nullable();
            $table->string('IDUkuran', 64)->nullable();
            $table->string('IDTipe', 64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_barang');
    }
}
