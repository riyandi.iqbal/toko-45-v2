<?php
namespace App\Helpers;

use Carbon\Carbon;
use DB;

class AppHelper{
    public static function BulanRomawi($bulan) {
        $array_romawi = array(
            '1'     => 'I',
            '01'    => 'I',
            '2'     => 'II',
            '02'    => 'II',
            '3'     => 'III',
            '03'    => 'III',
            '4'     => 'IV',
            '04'    => 'IV',
            '5'     => 'V',
            '05'    => 'V',
            '6'     => 'VI',
            '06'    => 'VI',
            '7'     => 'VII',
            '07'    => 'VII',
            '8'     => 'VIII',
            '9'     => 'IX',
            '10'    => 'X',
            '11'    => 'XI',
            '12'    => 'XII',
        );

        return $array_romawi[$bulan];
    }

    public static function StrReplace($value) {
        $new_value = str_replace(".", "", $value);
        return str_replace(",", ".", $new_value);
    }

    public static function NumberFormat($value, $decimal = 0) {
        $coreset = DB::table('tbl_perusahaan')->first();
        return number_format($value, $coreset->Angkakoma, ',', '.');
    }

    public static function PpnExclude($value) {
        $NilaiPpn = ( 10 / 100 ) * $value;

        return $NilaiPpn;
    }

    public static function PpnInclude($value) {
        $NilaiPpn = $value / 1.1;

        return $value - $NilaiPpn;
    }

    public static function DateFormat($date) {
        return Carbon::createFromFormat('d/m/Y', $date)->toDateTimeString();
    }

    public static function DateIndo($date) {
        return date("d M Y", strtotime($date));
    }

    public static function NumberJurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }
}