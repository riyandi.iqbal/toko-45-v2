<?php
/*=====Create DEDY @20/12/2019====*/

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use App\Models\BankModel;
use App\Models\BarangModel;
use App\Models\CaraBayarModel;
use App\Models\GudangModel;
use App\Models\HargaJualModel;
use App\Models\HutangModel;
use App\Models\InvoicePembelianDetailModel;
use App\Models\InvoicePembelianGrandTotalModel;
use App\Models\InvoicePembelianModel;
use App\Models\JurnalModel;
use App\Models\KartustokModel;
use App\Models\MataUangModel;
use App\Models\PenerimaanBarangDetailModel;
use App\Models\PenerimaanBarangModel;
use App\Models\PerusahaanModel;
use App\Models\PurchaseOrderDetailModel;
use App\Models\PurchaseOrderModel;
use App\Models\SatuanKonversiModel;
use App\Models\SettingCOAModel;
use App\Models\StokModel;
use App\Models\UMSupplierModel;
use App\Models\VListSatuanKonversiModel;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// HELPERS //
use Illuminate\Support\Facades\Input;

// LIB //
use Validator;

class InvoicePembelianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        set_time_limit(8000000);
    }

    public function index(Request $request)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('invoicepembelian/index', compact('aksesmenu', 'coreset', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function index2(Request $request)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('updatepajakpembelian/index', compact('aksesmenu', 'coreset', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request)
    {
        $data = InvoicePembelianModel::Getindex();

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_pembelian.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_pembelian.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%' . $request->get('keyword') . '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create()
    {
        $iduser = Auth::user()->id;
        $akses = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if ($akses == null) {
            return redirect('InvoicePembelian')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $supplier = DB::table('tbl_supplier')->orderBy('Nama', 'asc')->get();
        $penerimaan_barang = PenerimaanBarangModel::whereNull('Status_pakai')->join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_terima_barang_supplier.IDSupplier')->get();
        $cara_bayar = CaraBayarModel::GetFB();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $bank = BankModel::get();
        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');
        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);
        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');
        $barang = $barangBuilder->get();
        $gudang = GudangModel::whereNotIn('Kode_Gudang', ['GBJ'])->get();

        return view('invoicepembelian.create', compact('coreset', 'gudang', 'mata_uang', 'barang', 'bank', 'supplier', 'cara_bayar', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penerimaan_barang'));
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'Nomor' => 'required',
            // 'IDTBS' => 'required',
            'IDSupplier' => 'required',
            'Tanggal' => 'required||date_format:"d/m/Y"',
            // 'Tanggal_jatuh_tempo' => 'required||date_format:"d/m/Y"',
            'Total_qty' => 'required|numeric',
            'total_invoice_diskon' => 'required',
            'Status_ppn' => 'required',
            'Keterangan' => 'nullable',
            'DPP' => 'required',
            'PPN1' => 'required',
        ])->setAttributeNames([
            'Nomor' => 'Nomor',
            // 'IDTBS' => 'Nomor Surat Jalan',
            'IDSupplier' => 'Supplier',
            'Tanggal' => 'Tanggal',
            // 'Tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'Total_qty' => 'Total Qty',
            'total_invoice_diskon' => 'Grand Total',
            'Status_ppn' => 'Jenis PPN',
            'Keterangan' => 'Keterangan',
            'DPP' => 'DPP',
            'PPN1' => 'PPN',
        ]);

        if ($validate->fails()) {
            $data = [
                'status' => false,
                'message' => strip_tags($validate->errors()->first()),
            ];
            return json_encode($data);
        }

// save table fb
        $IDFB = uniqid();
        // $penerimaan_barang = PenerimaanBarangModel::findOrfail($request->IDTBS);
        // $purchase_order = PurchaseOrderModel::findOrfail($penerimaan_barang->IDPO);

        $ceknopb = DB::table('tbl_pembelian')
            ->where('Nomor', $request->Nomor)
            ->first();
        if ($ceknopb == '') {
            $Nomor = $request->Nomor;
        } else {
            $hasilkeluaran = substr($ceknopb->Nomor, 2, 4) + 1;
            $cekagain = substr($request->Nomor, 0, 2);
            $ambilsetelahpenambahan = substr($request->Nomor, 6);
            $Nomor = $cekagain . str_pad($hasilkeluaran, 4, 0, STR_PAD_LEFT) . $ambilsetelahpenambahan;
        }

        DB::beginTransaction();

        $pembelian = new InvoicePembelianModel();

        if ($request->Status_ppn == 'include') {
            $pembelian_PPN = AppHelper::PpnInclude(AppHelper::StrReplace($request->DPP));
            $pembelian_DPP = floatval(AppHelper::StrReplace($request->DPP)) - floatval($pembelian_PPN);
        } else {
            $pembelian_PPN = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;
            $pembelian_DPP = floatval(AppHelper::StrReplace($request->DPP));
        }
        $IDTBS = uniqid();
        $pembelian->IDFB = $IDFB;
        $pembelian->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $pembelian->Nomor = $Nomor;
        $pembelian->IDSupplier = $request->IDSupplier;
        $pembelian->No_sj_supplier = $request->Nomor_sj;
        $pembelian->no_pajak = $request->Nomor_pajak;
        $pembelian->IDTBS = $IDTBS;
        // $pembelian->Tanggal_jatuh_tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $pembelian->IDMataUang = $request->IDMataUang;
        $pembelian->Kurs = $request->Kurs;
        $pembelian->Total_qty_yard = $request->Total_qty ? AppHelper::StrReplace($request->Total_qty) : 0;
        $pembelian->Saldo_yard = $request->Total_qty ? AppHelper::StrReplace($request->Total_qty) : 0;
        $pembelian->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $pembelian->Status_ppn = $request->Status_ppn;
        $pembelian->Keterangan = $request->Keterangan;
        $pembelian->Batal = 'aktif';
        $pembelian->Status = 0;
        $pembelian->is_paid = 'f';
        $pembelian->jenis_ppn = $request->Status_ppn == 'include' ? '1' : '0';
        // $pembelian->DPP = $pembelian_DPP;
        // $pembelian->PPN = $pembelian_PPN;

        // $pembelian->dibuat_pada = date('Y-m-d H:i:s');
        // $pembelian->diubah_pada = date('Y-m-d H:i:s');

        $pembelian->save();

        foreach ($request->IDBarang as $key => $value) {
            $IDFBDetail = uniqid();
            $pembelian_detail = new InvoicePembelianDetailModel;

            $pembelian_detail->IDFBDetail = $IDFBDetail;
            $pembelian_detail->IDFB = $IDFB;
            $pembelian_detail->IDBarang = $value;
            $pembelian_detail->Qty_yard = AppHelper::StrReplace($request->Qty[$key]);
            $pembelian_detail->Saldo_yard = AppHelper::StrReplace($request->Qty[$key]);
            $pembelian_detail->IDSatuan = $request->IDSatuan[$key];
            $pembelian_detail->IDMataUang = $request->IDMataUang;
            $pembelian_detail->diskon = $request->diskon[$key] ? AppHelper::StrReplace($request->diskon[$key]) : 0;
            $pembelian_detail->Harga = AppHelper::StrReplace($request->harga[$key]);
            $pembelian_detail->Sub_total = AppHelper::StrReplace($request->Sub_total[$key]);

            $pembelian_detail->save();

            // UPDATE HARGA MODAL //
            $data_harga_jual_exist = HargaJualModel::where('IDBarang', $value)->where('IDSatuan', $request->IDSatuan[$key])->where('IDMataUang', $request->IDMataUang)->first();

            if ($data_harga_jual_exist) {
                $data = HargaJualModel::find($data_harga_jual_exist->IDHargaJual);

                $data->Modal = AppHelper::StrReplace($request->harga[$key]);
                $data->MID = Auth::user()->id;
                $data->MTime = date('Y-m-d H:i:s');

                $data->save();
            } else {
                $data = new HargaJualModel;

                $data->IDHargaJual = uniqid();
                $data->IDBarang = $value;
                $data->IDSatuan = $request->IDSatuan[$key];
                $data->IDGroupCustomer = 'P000001';
                $data->IDMataUang = 'P000002';
                $data->Modal = AppHelper::StrReplace($request->harga[$key]);
                $data->Harga_Jual = 0;
                $data->CID = Auth::user()->id;
                $data->CTime = date('Y-m-d H:i:s');
                $data->Status = 'aktif';

                $data->save();
            }
        }

        // SAVE TO GRANDTOTAL //
        $pembelian_grand_total = new InvoicePembelianGrandTotalModel;

        $pembelian_grand_total->IDFBGrandTotal = uniqid();
        $pembelian_grand_total->IDFB = $IDFB;
        $pembelian_grand_total->Pembayaran = '-';
        $pembelian_grand_total->DPP = $pembelian_DPP;
        $pembelian_grand_total->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $pembelian_grand_total->PPN = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;
        $pembelian_grand_total->Grand_total = AppHelper::StrReplace($request->total_invoice_diskon);
        $pembelian_grand_total->Sisa = 0;

        $pembelian_grand_total->save();
        // END GRAND TOTAL //

        // CEK UM //
        // $UM_Supplier = UMSupplierModel::where('IDFaktur', $penerimaan_barang->IDTBS)->whereNull('IDFB')->first();

        // if ($UM_Supplier) {
        //     $IDFB_piutang = UMSupplierModel::findOrfail($UM_Supplier->IDUMSupplier);

        //     $IDFB_piutang->IDFB = $IDFB;

        //     $IDFB_piutang->save();
        // }
        // END CEK UM //
        $data_barang = BarangModel::find($value);
        if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
            $Qty_konversi = AppHelper::StrReplace($request->Qty[$key]);
        } else {
            $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

            $Qty_konversi = $data_satuan_konversi->Qty * AppHelper::StrReplace($request->Qty[$key]);
        }

        // KARTU STOK //
        $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
            ->where('IDGudang', '=', $request->IDGudang)->first();

        if ($data_stok) {
            $stok = StokModel::find($data_stok->IDStok);

            $stok->Qty_pcs = $data_stok->Qty_pcs + $Qty_konversi;

            $stok->save();
        } else {
            $IDStok = uniqid();
            $stok = new StokModel;

            $stok->IDStok = $IDStok;
            $stok->Tanggal = date('Y-m-d H:i:s');
            $stok->Nomor_faktur = $Nomor;
            $stok->IDBarang = $request->IDBarang[$key];
            $stok->Qty_pcs = $Qty_konversi;
            $stok->Nama_Barang = $data_barang->Nama_Barang;
            $stok->Jenis_faktur = 'FB';
            $stok->IDGudang = $request->IDGudang;
            $stok->IDSatuan = $data_barang->IDSatuan;

            $stok->save();
        }

        $kartu_stok = new KartustokModel;

        $kartu_stok->IDKartuStok = uniqid();
        $kartu_stok->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $kartu_stok->Nomor_faktur = $Nomor;
        $kartu_stok->IDFaktur = $IDFB;
        $kartu_stok->IDFakturDetail = $IDFBDetail;
        $kartu_stok->IDStok = $data_stok ? $data_stok->IDStok : $IDStok;
        $kartu_stok->Jenis_faktur = 'FB';
        $kartu_stok->IDBarang = $request->IDBarang[$key];
        $kartu_stok->Harga = AppHelper::StrReplace($request->harga[$key]);
        $kartu_stok->Nama_Barang = $data_barang->Nama_Barang;
        $kartu_stok->Masuk_pcs = AppHelper::StrReplace($request->Qty[$key]);
        $kartu_stok->IDSatuan = $request->IDSatuan[$key];

        $kartu_stok->save();
        // END KARTU STOK //
        // SAVE TO HUTANG //
        $hutang = new HutangModel;

        $hutang->IDHutang = uniqid();
        $hutang->IDFaktur = $IDFB;
        $hutang->IDSupplier = $request->IDSupplier;
        $hutang->Tanggal_Hutang = AppHelper::DateFormat($request->Tanggal);
        // $hutang->Jatuh_Tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $hutang->No_Faktur = $Nomor;
        $hutang->Nilai_Hutang = AppHelper::StrReplace($request->total_invoice_diskon);
        $hutang->Saldo_Awal = AppHelper::StrReplace($request->total_invoice_diskon);
        // $hutang->UM = $UM_Supplier ? $UM_Supplier->Nilai_UM : 0;
        $hutang->Pembayaran = floatval($hutang->UM);
        $hutang->Saldo_Akhir = floatval(AppHelper::StrReplace($request->total_invoice_diskon)) - floatval($hutang->Pembayaran);
        $hutang->Jenis_Faktur = 'INV';
        $hutang->Retur = 0;
        $hutang->Batal = 'aktif';
        $hutang->IDMataUang = $request->IDMataUang;
        $hutang->is_paid = 'f';
        $hutang->no_inv = $request->Nomor_sj;

        $hutang->save();

        // END SAVE HUTANG //
        $ambilcoadebetdpp = SettingCOAModel::Getdppfaktur();

        if ($hutang->Pembayaran == 0) {
            // SAVE JURNAL DEBET //
            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor = $Nomor;
            $jurnal_pembelian_debet->IDFaktur = $IDFB;
            $jurnal_pembelian_debet->IDFakturDetail = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet->IDCOA = $ambilcoadebetdpp->IDCoa;
            $jurnal_pembelian_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Kredit = 0;
            $jurnal_pembelian_debet->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet->Kurs = $request->Kurs;
            $jurnal_pembelian_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Total_kredit = 0;
            $jurnal_pembelian_debet->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $ambilcoakredit = SettingCOAModel::Getkreditfaktur();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur = $IDFB;
            $jurnal_pembelian_kredit->IDFakturDetail = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'FB';
            $jurnal_pembelian_kredit->IDCOA = $ambilcoakredit->IDCoa;
            $jurnal_pembelian_kredit->Debet = 0;
            $jurnal_pembelian_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_kredit->Kurs = $request->Kurs;
            $jurnal_pembelian_kredit->Total_debet = 0;
            $jurnal_pembelian_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_kredit->save();
        } else {
            // SAVE JURNAL DEBET //
            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor = $Nomor;
            $jurnal_pembelian_debet->IDFaktur = $IDFB;
            $jurnal_pembelian_debet->IDFakturDetail = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet->IDCOA = $ambilcoadebetdpp->IDCoa;
            $jurnal_pembelian_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Kredit = 0;
            $jurnal_pembelian_debet->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet->Kurs = $request->Kurs;
            $jurnal_pembelian_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Total_kredit = 0;
            $jurnal_pembelian_debet->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $ambilcoakredit = SettingCOAModel::Getkreditfaktur();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur = $IDFB;
            $jurnal_pembelian_kredit->IDFakturDetail = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'FB';
            $jurnal_pembelian_kredit->IDCOA = $ambilcoakredit->IDCoa;
            $jurnal_pembelian_kredit->Debet = 0;
            $jurnal_pembelian_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_kredit->Kurs = $request->Kurs;
            $jurnal_pembelian_kredit->Total_debet = 0;
            $jurnal_pembelian_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_kredit->save();
        }

        $ambilcoadebetppn = SettingCOAModel::Getppnfaktur();

        if ($pembelian->Status_ppn == 'exclude') {
            // SAVE JURNAL PPN DEBET //
            $jurnal_pembelian_debet_ppn = new JurnalModel;

            $jurnal_pembelian_debet_ppn->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet_ppn->Tanggal = $pembelian->Tanggal;
            $jurnal_pembelian_debet_ppn->Nomor = $Nomor;
            $jurnal_pembelian_debet_ppn->IDFaktur = $IDFB;
            $jurnal_pembelian_debet_ppn->IDFakturDetail = '-';
            $jurnal_pembelian_debet_ppn->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet_ppn->IDCOA = $ambilcoadebetppn->IDCoa;
            $jurnal_pembelian_debet_ppn->Debet = AppHelper::StrReplace($pembelian_PPN);
            $jurnal_pembelian_debet_ppn->Kredit = 0;
            $jurnal_pembelian_debet_ppn->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet_ppn->Kurs = $request->Kurs;
            $jurnal_pembelian_debet_ppn->Total_debet = AppHelper::StrReplace($pembelian_PPN);
            $jurnal_pembelian_debet_ppn->Total_kredit = 0;
            $jurnal_pembelian_debet_ppn->Keterangan = 'PPN Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet_ppn->Saldo = AppHelper::StrReplace($pembelian_PPN);

            $jurnal_pembelian_debet_ppn->save();
        }

        // UPDATE PENERIMAAN STATUS PAKAI //
        // $penerimaan_barang = PenerimaanBarangModel::findOrfail($request->IDTBS);

        // $penerimaan_barang->Status_pakai = 1;

        // $penerimaan_barang->save();
        // END UPDATE PENERIMAAN //

        DB::commit();

        $data = array(
            'status' => true,
            'message' => 'Data berhasil disimpan.',
            'data' => [
                'IDFB' => $IDFB,
            ],
        );

        return json_encode($data);
    }

    public function show($id)
    {
        $iduser = Auth::user()->id;
        $akses = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if ($akses == null) {
            return redirect('InvoicePembelian')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $supplier = DB::table('tbl_supplier')->orderBy('Nama', 'asc')->get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();
        $gudang = GudangModel::whereNotIn('Kode_Gudang', ['GBJ'])->get();

        $cara_bayar = CaraBayarModel::GetFB();

        $purchase_order = DB::table('tbl_pembelian')
            ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_pembelian.IDMataUang')
            ->join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_pembelian.IDSupplier')
            ->join("tbl_pembelian_detail", "tbl_pembelian_detail.IDFB", "=", "tbl_pembelian.IDFB")
            ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB", "=", "tbl_pembelian.IDFB")
            ->where('tbl_pembelian.IDFB', $id)
            ->select('tbl_pembelian.*', 'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang', DB::raw('SUM(tbl_pembelian_detail."Sub_total") AS total_harga'), 'tbl_supplier.Nama', 'tbl_pembelian_grand_total.DPP', 'tbl_pembelian_grand_total.Discount as Diskon', 'tbl_pembelian_grand_total.PPN')
            ->groupBy("tbl_pembelian.IDFB", "tbl_pembelian.Tanggal", "tbl_pembelian.Nomor", "tbl_pembelian.IDTBS", "tbl_pembelian.IDSupplier", "tbl_pembelian.No_sj_supplier",
                "tbl_pembelian.Batal", "tbl_pembelian.TOP", "tbl_pembelian.Discount", "tbl_pembelian.Tanggal_jatuh_tempo", "tbl_pembelian.IDMataUang", "tbl_pembelian.Kurs",
                "tbl_pembelian.Total_qty_yard", "tbl_pembelian.Total_qty_meter", "tbl_pembelian.Saldo_yard", "tbl_pembelian.Saldo_meter", "tbl_pembelian.Status_ppn",
                'tbl_supplier.Nama', 'tbl_pembelian_grand_total.DPP', 'tbl_pembelian_grand_total.Discount', 'tbl_pembelian_grand_total.PPN', "tbl_pembelian.Keterangan", "tbl_pembelian.Status", "tbl_pembelian.jenis_ppn", "tbl_pembelian.no_pajak", "tbl_pembelian.Status_pakai", "tbl_pembelian.is_paid", "tbl_supplier.Nama", "tbl_pembelian_detail.IDMataUang", "tbl_mata_uang.Mata_uang", "tbl_mata_uang.Kode")
            ->first();

        $purchase_order_detail = DB::table('tbl_pembelian_detail')
            ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_pembelian_detail.IDBarang')
            ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_pembelian_detail.IDSatuan')
            ->join('tbl_satuan as satuan_kecil', 'satuan_kecil.IDSatuan', '=', 'tbl_barang.IDSatuan')
            ->where('tbl_pembelian_detail.IDFB', $id)
            ->select('tbl_pembelian_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang', 'tbl_barang.IDSatuan as IDSatuanKecil', 'satuan_kecil.Satuan as Satuan_kecil')
            ->get();

        $um_supplier = DB::table('tbl_um_supplier')
            ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor', '=', 'tbl_um_supplier.Nomor_Faktur')
            ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_um_supplier.IDCOA')
            ->where('tbl_purchase_order.IDPO', $id)
            ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
            ->first();

        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);

        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        $barang = $barangBuilder->get();

        return view('invoicepembelian.update', compact('coreset', 'gudang', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'supplier', 'bank', 'mata_uang', 'cara_bayar', 'purchase_order', 'purchase_order_detail', 'um_supplier', 'barang'));
    }
    public function update_data(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'Nomor' => 'required',
            // 'IDTBS' => 'required',
            //'IDSupplier' => 'required',
            'Tanggal' => 'required||date_format:"d/m/Y"',
            // 'Tanggal_jatuh_tempo' => 'required||date_format:"d/m/Y"',
            'Total_qty' => 'required|numeric',
            'total_invoice_diskon' => 'required',
            'Status_ppn' => 'required',
            'Keterangan' => 'nullable',
            'DPP' => 'required',
            'PPN1' => 'nullable',
        ])->setAttributeNames([
            'Nomor' => 'Nomor',
            // 'IDTBS' => 'Nomor Surat Jalan',
            //'IDSupplier' => 'Supplier',
            'Tanggal' => 'Tanggal',
            // 'Tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'Total_qty' => 'Total Qty',
            'total_invoice_diskon' => 'Grand Total',
            'Status_ppn' => 'Jenis PPN',
            'Keterangan' => 'Keterangan',
            'DPP' => 'DPP',
            'PPN1' => 'PPN',
        ]);

        if ($validate->fails()) {
            $data = [
                'status' => false,
                'message' => strip_tags($validate->errors()->first()),
            ];
            return json_encode($data);
        }

// save table fb
        // $IDFB = uniqid();
        // // $penerimaan_barang = PenerimaanBarangModel::findOrfail($request->IDTBS);
        // // $purchase_order = PurchaseOrderModel::findOrfail($penerimaan_barang->IDPO);

        // $ceknopb = DB::table('tbl_pembelian')
        //     ->where('Nomor', $request->Nomor)
        //     ->first();
        // if ($ceknopb == '') {
        //     $Nomor = $request->Nomor;
        // } else {
        //     $hasilkeluaran = substr($ceknopb->Nomor, 2, 4) + 1;
        //     $cekagain = substr($request->Nomor, 0, 2);
        //     $ambilsetelahpenambahan = substr($request->Nomor, 6);
        //     $Nomor = $cekagain . str_pad($hasilkeluaran, 4, 0, STR_PAD_LEFT) . $ambilsetelahpenambahan;
        // }
        DB::beginTransaction();
        $Nomor = $request->Nomor;
        $IDFB = $request->IDFB;
        $pembelian = InvoicePembelianModel::findOrfail($request->IDFB);

        if ($request->Status_ppn == 'include') {
            $pembelian_PPN = AppHelper::PpnInclude(AppHelper::StrReplace($request->PPN));
            $pembelian_DPP = floatval(AppHelper::StrReplace($request->DPP)) - floatval($pembelian_PPN);
        } else {
            $pembelian_PPN = AppHelper::StrReplace($request->PPN);
            $pembelian_DPP = floatval(AppHelper::StrReplace($request->DPP));
        }
        $IDTBS = uniqid();

        $pembelian->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $pembelian->Nomor = $Nomor;
        $pembelian->IDSupplier = $request->IDSupplier;
        $pembelian->No_sj_supplier = $request->Nomor_sj;
        $pembelian->no_pajak = $request->Nomor_pajak;
        $pembelian->IDTBS = $IDTBS;
        // $pembelian->Tanggal_jatuh_tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $pembelian->IDMataUang = $request->IDMataUang;
        $pembelian->Kurs = $request->Kurs;
        $pembelian->Total_qty_yard = $request->Total_qty ? AppHelper::StrReplace($request->Total_qty) : 0;
        $pembelian->Saldo_yard = $request->Total_qty ? AppHelper::StrReplace($request->Total_qty) : 0;
        $pembelian->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $pembelian->Status_ppn = $request->Status_ppn;
        $pembelian->Keterangan = $request->Keterangan;
        $pembelian->Batal = 'aktif';
        $pembelian->Status = 0;
        $pembelian->is_paid = 'f';
        $pembelian->jenis_ppn = $request->Status_ppn == 'include' ? '1' : '0';
        // $pembelian->DPP = $pembelian_DPP;
        // $pembelian->PPN = $pembelian_PPN;

        // $pembelian->dibuat_pada = date('Y-m-d H:i:s');
        // $pembelian->diubah_pada = date('Y-m-d H:i:s');

        $pembelian->save();
        $pembelian_detail_exist = InvoicePembelianDetailModel::where('IDFB', $pembelian->IDFB)->delete();

        foreach ($request->IDBarang as $key => $value) {
            $IDFBDetail = uniqid();
            $pembelian_detail = new InvoicePembelianDetailModel;

            $pembelian_detail->IDFBDetail = $IDFBDetail;
            $pembelian_detail->IDFB = $IDFB;
            $pembelian_detail->IDBarang = $value;
            $pembelian_detail->Qty_yard = AppHelper::StrReplace($request->Qty[$key]);
            $pembelian_detail->Saldo_yard = AppHelper::StrReplace($request->Qty[$key]);
            $pembelian_detail->IDSatuan = $request->IDSatuan[$key];
            $pembelian_detail->IDMataUang = $request->IDMataUang;
            $pembelian_detail->diskon = $request->diskon[$key] ? AppHelper::StrReplace($request->diskon[$key]) : 0;
            $pembelian_detail->Harga = AppHelper::StrReplace($request->harga[$key]);
            $pembelian_detail->Sub_total = AppHelper::StrReplace($request->Sub_total[$key]);

            $pembelian_detail->save();

            // UPDATE HARGA MODAL //
            $data_harga_jual_exist = HargaJualModel::where('IDBarang', $value)->where('IDSatuan', $request->IDSatuan[$key])->where('IDMataUang', $request->IDMataUang)->first();

            if ($data_harga_jual_exist) {
                $data = HargaJualModel::find($data_harga_jual_exist->IDHargaJual);

                $data->Modal = AppHelper::StrReplace($request->harga[$key]);
                $data->MID = Auth::user()->id;
                $data->MTime = date('Y-m-d H:i:s');

                $data->save();
            } else {
                $data = new HargaJualModel;

                $data->IDHargaJual = uniqid();
                $data->IDBarang = $value;
                $data->IDSatuan = $request->IDSatuan[$key];
                $data->IDGroupCustomer = 'P000001';
                $data->IDMataUang = 'P000002';
                $data->Modal = AppHelper::StrReplace($request->harga[$key]);
                $data->Harga_Jual = 0;
                $data->CID = Auth::user()->id;
                $data->CTime = date('Y-m-d H:i:s');
                $data->Status = 'aktif';

                $data->save();
            }
        }

        // SAVE TO GRANDTOTAL //
        $pembelian_grand_total_exist = InvoicePembelianGrandTotalModel::where('IDFB', $pembelian->IDFB)->delete();
        $pembelian_grand_total = new InvoicePembelianGrandTotalModel;

        $pembelian_grand_total->IDFBGrandTotal = uniqid();
        $pembelian_grand_total->IDFB = $IDFB;
        $pembelian_grand_total->Pembayaran = '-';
        $pembelian_grand_total->DPP = $pembelian_DPP;
        $pembelian_grand_total->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $pembelian_grand_total->PPN = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;
        $pembelian_grand_total->Grand_total = AppHelper::StrReplace($request->total_invoice_diskon);
        $pembelian_grand_total->Sisa = 0;

        $pembelian_grand_total->save();
        // END GRAND TOTAL //

        // CEK UM //
        // $UM_Supplier = UMSupplierModel::where('IDFaktur', $penerimaan_barang->IDTBS)->whereNull('IDFB')->first();

        // if ($UM_Supplier) {
        //     $IDFB_piutang = UMSupplierModel::findOrfail($UM_Supplier->IDUMSupplier);

        //     $IDFB_piutang->IDFB = $IDFB;

        //     $IDFB_piutang->save();
        // }
        // END CEK UM //
        $data_barang = BarangModel::find($value);
        if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
            $Qty_konversi = AppHelper::StrReplace($request->Qty[$key]);
        } else {
            $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

            $Qty_konversi = $data_satuan_konversi->Qty * AppHelper::StrReplace($request->Qty[$key]);
        }

        // KARTU STOK //
        $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
            ->where('IDGudang', '=', $request->IDGudang)->first();

        if ($data_stok) {
            $stok = StokModel::find($data_stok->IDStok);

            $stok->Qty_pcs = $data_stok->Qty_pcs + $Qty_konversi;

            $stok->save();
        } else {
            $IDStok = uniqid();
            $stok = new StokModel;

            $stok->IDStok = $IDStok;
            $stok->Tanggal = date('Y-m-d H:i:s');
            $stok->Nomor_faktur = $Nomor;
            $stok->IDBarang = $request->IDBarang[$key];
            $stok->Qty_pcs = $Qty_konversi;
            $stok->Nama_Barang = $data_barang->Nama_Barang;
            $stok->Jenis_faktur = 'FB';
            $stok->IDGudang = $request->IDGudang;
            $stok->IDSatuan = $data_barang->IDSatuan;

            $stok->save();
        }

        $kartu_stok = new KartustokModel;

        $kartu_stok->IDKartuStok = uniqid();
        $kartu_stok->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $kartu_stok->Nomor_faktur = $Nomor;
        $kartu_stok->IDFaktur = $IDFB;
        $kartu_stok->IDFakturDetail = $IDFBDetail;
        $kartu_stok->IDStok = $data_stok ? $data_stok->IDStok : $IDStok;
        $kartu_stok->Jenis_faktur = 'FB';
        $kartu_stok->IDBarang = $request->IDBarang[$key];
        $kartu_stok->Harga = AppHelper::StrReplace($request->harga[$key]);
        $kartu_stok->Nama_Barang = $data_barang->Nama_Barang;
        $kartu_stok->Masuk_pcs = AppHelper::StrReplace($request->Qty[$key]);
        $kartu_stok->IDSatuan = $request->IDSatuan[$key];

        $kartu_stok->save();
        // END KARTU STOK //
        // SAVE TO HUTANG //
        $hutang = new HutangModel;

        $hutang->IDHutang = uniqid();
        $hutang->IDFaktur = $IDFB;
        $hutang->IDSupplier = $request->IDSupplier;
        $hutang->Tanggal_Hutang = AppHelper::DateFormat($request->Tanggal);
        // $hutang->Jatuh_Tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $hutang->No_Faktur = $Nomor;
        $hutang->Nilai_Hutang = AppHelper::StrReplace($request->total_invoice_diskon);
        $hutang->Saldo_Awal = AppHelper::StrReplace($request->total_invoice_diskon);
        // $hutang->UM = $UM_Supplier ? $UM_Supplier->Nilai_UM : 0;
        $hutang->Pembayaran = floatval($hutang->UM);
        $hutang->Saldo_Akhir = floatval(AppHelper::StrReplace($request->total_invoice_diskon)) - floatval($hutang->Pembayaran);
        $hutang->Jenis_Faktur = 'INV';
        $hutang->Retur = 0;
        $hutang->Batal = 'aktif';
        $hutang->IDMataUang = $request->IDMataUang;
        $hutang->is_paid = 'f';
        $hutang->no_inv = $request->Nomor_sj;

        $hutang->save();

        // END SAVE HUTANG //
        $ambilcoadebetdpp = SettingCOAModel::Getdppfaktur();

        if ($hutang->Pembayaran == 0) {
            // SAVE JURNAL DEBET //
            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor = $Nomor;
            $jurnal_pembelian_debet->IDFaktur = $IDFB;
            $jurnal_pembelian_debet->IDFakturDetail = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet->IDCOA = $ambilcoadebetdpp->IDCoa;
            $jurnal_pembelian_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Kredit = 0;
            $jurnal_pembelian_debet->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet->Kurs = $request->Kurs;
            $jurnal_pembelian_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Total_kredit = 0;
            $jurnal_pembelian_debet->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $ambilcoakredit = SettingCOAModel::Getkreditfaktur();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur = $IDFB;
            $jurnal_pembelian_kredit->IDFakturDetail = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'FB';
            $jurnal_pembelian_kredit->IDCOA = $ambilcoakredit->IDCoa;
            $jurnal_pembelian_kredit->Debet = 0;
            $jurnal_pembelian_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_kredit->Kurs = $request->Kurs;
            $jurnal_pembelian_kredit->Total_debet = 0;
            $jurnal_pembelian_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_kredit->save();
        } else {
            // SAVE JURNAL DEBET //
            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor = $Nomor;
            $jurnal_pembelian_debet->IDFaktur = $IDFB;
            $jurnal_pembelian_debet->IDFakturDetail = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet->IDCOA = $ambilcoadebetdpp->IDCoa;
            $jurnal_pembelian_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Kredit = 0;
            $jurnal_pembelian_debet->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet->Kurs = $request->Kurs;
            $jurnal_pembelian_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_debet->Total_kredit = 0;
            $jurnal_pembelian_debet->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $ambilcoakredit = SettingCOAModel::Getkreditfaktur();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur = $IDFB;
            $jurnal_pembelian_kredit->IDFakturDetail = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'FB';
            $jurnal_pembelian_kredit->IDCOA = $ambilcoakredit->IDCoa;
            $jurnal_pembelian_kredit->Debet = 0;
            $jurnal_pembelian_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_kredit->Kurs = $request->Kurs;
            $jurnal_pembelian_kredit->Total_debet = 0;
            $jurnal_pembelian_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_pembelian_kredit->Keterangan = 'Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_pembelian_kredit->save();
        }

        $ambilcoadebetppn = SettingCOAModel::Getppnfaktur();

        if ($pembelian->Status_ppn == 'exclude') {
            // SAVE JURNAL PPN DEBET //
            $jurnal_pembelian_debet_ppn = new JurnalModel;

            $jurnal_pembelian_debet_ppn->IDJurnal = $this->number_jurnal();
            $jurnal_pembelian_debet_ppn->Tanggal = $pembelian->Tanggal;
            $jurnal_pembelian_debet_ppn->Nomor = $Nomor;
            $jurnal_pembelian_debet_ppn->IDFaktur = $IDFB;
            $jurnal_pembelian_debet_ppn->IDFakturDetail = '-';
            $jurnal_pembelian_debet_ppn->Jenis_faktur = 'FB';
            $jurnal_pembelian_debet_ppn->IDCOA = $ambilcoadebetppn->IDCoa;
            $jurnal_pembelian_debet_ppn->Debet = $pembelian_PPN ? AppHelper::StrReplace($pembelian_PPN) : 0;
            $jurnal_pembelian_debet_ppn->Kredit = 0;
            $jurnal_pembelian_debet_ppn->IDMataUang = $request->IDMataUang;
            $jurnal_pembelian_debet_ppn->Kurs = $request->Kurs;
            $jurnal_pembelian_debet_ppn->Total_debet = $pembelian_PPN ? AppHelper::StrReplace($pembelian_PPN) : 0;
            $jurnal_pembelian_debet_ppn->Total_kredit = 0;
            $jurnal_pembelian_debet_ppn->Keterangan = 'PPN Invoice Pembelian Nomor ' . $Nomor;
            $jurnal_pembelian_debet_ppn->Saldo = $pembelian_PPN ? AppHelper::StrReplace($pembelian_PPN) : 0;

            $jurnal_pembelian_debet_ppn->save();
        }

        // UPDATE PENERIMAAN STATUS PAKAI //
        // $penerimaan_barang = PenerimaanBarangModel::findOrfail($request->IDTBS);

        // $penerimaan_barang->Status_pakai = 1;

        // $penerimaan_barang->save();
        // END UPDATE PENERIMAAN //

        DB::commit();

        $data = array(
            'status' => true,
            'message' => 'Data berhasil disimpan.',
            'data' => [
                'IDFB' => $IDFB,
            ],
        );

        return json_encode($data);
    }

    public function detail($id)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pembelian = InvoicePembelianModel::join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB", "=", "tbl_pembelian.IDFB")
            ->join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->leftjoin("tbl_terima_barang_supplier", "tbl_pembelian.IDTBS", "=", "tbl_terima_barang_supplier.IDTBS")
            ->where("tbl_pembelian.IDFB", $id)
            ->select('tbl_pembelian.*', 'tbl_terima_barang_supplier.Nomor as No_PB', 'tbl_pembelian.Nomor as No_INV', 'tbl_supplier.Nama', 'tbl_pembelian_grand_total.DPP', 'tbl_pembelian_grand_total.Discount as Diskon', 'tbl_pembelian_grand_total.PPN')
            ->first();
        $total_harga_inv = InvoicePembelianDetailModel::select(DB::raw('SUM("Sub_total") AS total_harga'))
            ->where("IDFB", $id)
            ->first();

        $pembelian_detail = InvoicePembelianDetailModel::GetItem()->where('IDFB', $pembelian->IDFB)->get();

        // $um = UMSupplierModel::where('IDFB', '=', $pembelian->IDFB)
        //     ->select('tbl_um_supplier.*')
        //     ->first();

        return view('invoicepembelian.show', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pembelian', 'pembelian_detail', 'total_harga_inv'));
    }
    public function showdata(Request $request)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        //DB::enableQueryLog();
        $pembelian = DB::table('tbl_pembelian')
            ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB", "=", "tbl_pembelian.IDFB")
            ->join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->leftjoin("tbl_terima_barang_supplier", "tbl_pembelian.IDTBS", "=", "tbl_terima_barang_supplier.IDTBS")
            ->where("tbl_pembelian.IDFB", $request->id)
            ->select('tbl_pembelian.*', 'tbl_terima_barang_supplier.Nomor as No_PB', 'tbl_pembelian.Nomor as No_INV', 'tbl_supplier.Nama', 'tbl_pembelian_grand_total.DPP', 'tbl_pembelian_grand_total.Discount as Diskon', 'tbl_pembelian_grand_total.PPN')
            ->first();
        //$query = DB::getQueryLog();
        //print_r($query);
        //die();
        $total_harga_inv = DB::table('tbl_pembelian_detail')
            ->select(DB::raw('SUM("Sub_total") AS total_harga'))
            ->where("IDFB", $request->id)
            ->first();
        $pembelian_detail = DB::table('tbl_pembelian')
            ->join("tbl_pembelian_detail", "tbl_pembelian.IDFB", "=", "tbl_pembelian_detail.IDFB")
            ->join("tbl_barang", 'tbl_pembelian_detail.IDBarang', '=', 'tbl_barang.IDBarang')
            ->join("tbl_satuan", 'tbl_pembelian_detail.IDSatuan', '=', 'tbl_satuan.IDSatuan')
            ->where("tbl_pembelian_detail.IDFB", $request->id)
            ->get();

        $um = DB::table('tbl_um_supplier')
            ->join('tbl_terima_barang_supplier', 'tbl_um_supplier.Nomor_Faktur', '=', 'tbl_terima_barang_supplier.IDPO')
            ->where('tbl_terima_barang_supplier.Nomor', '=', $pembelian->No_PB)
            ->select('tbl_um_supplier.*')
            ->first();

        return view('invoicepembelian/show', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pembelian', 'total_harga_inv', 'pembelian_detail', 'um'));
    }

    function print($id) {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pembelian = InvoicePembelianModel::join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB", "=", "tbl_pembelian.IDFB")
            ->where("tbl_pembelian.IDFB", $id)
            ->select('tbl_pembelian.*', 'tbl_pembelian.Nomor as No_INV', 'tbl_supplier.Nama', 'tbl_pembelian_grand_total.DPP', 'tbl_pembelian_grand_total.Discount as Diskon', 'tbl_pembelian_grand_total.PPN')
            ->first();

        $total_harga_inv = InvoicePembelianDetailModel::select(DB::raw('SUM("Sub_total") AS total_harga'))
            ->where("IDFB", $id)
            ->first();

        $pembelian_detail = InvoicePembelianDetailModel::GetItem()->where("tbl_pembelian_detail.IDFB", $id)->get();

        $UMSupplier = UMSupplierModel::where('IDFB', '=', $id)
            ->select('tbl_um_supplier.*')
            ->first();

        $perusahaan = PerusahaanModel::Getforprint();

        return view('invoicepembelian.print', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pembelian', 'pembelian_detail', 'perusahaan', 'UMSupplier', 'total_harga_inv'));
    }

    public function laporan(Request $request)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();

        $invoice = DB::table('tbl_pembelian')
            ->join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->join("tbl_pembelian_grand_total", "tbl_pembelian_grand_total.IDFB", "=", "tbl_pembelian.IDFB")
            ->select("tbl_pembelian.*", "tbl_supplier.Nama", "tbl_pembelian_grand_total.Grand_total")
            ->orderBy('tbl_pembelian.Nomor', 'asc')
            ->get();

        return view('invoicepembelian/laporan', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'invoice'));
    }

    public function get_penerimaan_barang(Request $request)
    {
        $penerimaan_barang = PenerimaanBarangModel::join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_terima_barang_supplier.IDSupplier')
            ->where('IDTBS', $request->id)
            ->first();

        if ($penerimaan_barang) {
            $pesanan = PurchaseOrderModel::where('IDPO', $penerimaan_barang->IDPO)->first();

            $penerimaan_barang->Status_ppn = $pesanan ? $pesanan->pilihanppn : 'include';
        }

        $uang_muka = UMSupplierModel::where('IDFaktur', $penerimaan_barang->IDTBS)->whereNull('IDFB')->first();

        $data = [
            'penerimaan_barang' => $penerimaan_barang,
            'uang_muka' => $uang_muka,
        ];

        return json_encode($data);
    }

    public function get_penerimaan_barang_detail(Request $request)
    {
        $penerimaan_barang = PenerimaanBarangModel::join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_terima_barang_supplier.IDSupplier')
            ->where('IDTBS', $request->IDTBS)
            ->first();
        $penerimaan_barang_detail = PenerimaanBarangDetailModel::getItem()->where('IDTBS', $request->IDTBS)->get();

        foreach ($penerimaan_barang_detail as $key => $value) {
            $pesanan_detail = PurchaseOrderDetailModel::where('IDBarang', $value->IDBarang)
                ->where('IDSatuan', $value->IDSatuan)
                ->where('IDPO', $penerimaan_barang->IDPO)
                ->first();

            $value->Qty_pesan = $pesanan_detail->Qty;
        }

        return Datatables::of($penerimaan_barang_detail)->make(true);
    }

    public function number_jurnal()
    {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();

        if ($nomor->nonext == '') {
            $nomor_baru = 'P000001';
        } else {
            $hasil5 = substr($nomor->nonext, 2, 6) + 1;
            $nomor_baru = 'P' . str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    public function laporan_invoice(Request $request)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $mata_uang = MataUangModel::where('Aktif', 'aktif')->get();
        return view('invoicepembelian.laporan_invoice', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'mata_uang'));
    }

    public function datatable_laporan(Request $request)
    {
        $data = InvoicePembelianModel::select('Nama', 'tbl_pembelian.Tanggal', 'tbl_pembelian.Nomor as NomorINV', 'tbl_pembelian.Tanggal_jatuh_tempo as TanggalJT', 'tbl_pembelian_detail.*', 'tbl_purchase_order.Nomor', 'tbl_terima_barang_supplier.Nomor as NomorPB', 'tbl_barang.Nama_Barang', 'tbl_mata_uang.Kode', 'tbl_pembelian_detail.Qty_yard as Qty', 'tbl_pembelian_detail.Sub_total as Saldo')
            ->join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->join("tbl_pembelian_detail", "tbl_pembelian_detail.IDFB", "=", "tbl_pembelian.IDFB")
            ->join("tbl_barang", "tbl_pembelian_detail.IDBarang", "=", "tbl_barang.IDBarang")
            ->join("tbl_mata_uang", "tbl_mata_uang.IDMataUang", "=", "tbl_pembelian.IDMataUang")
            ->join("tbl_terima_barang_supplier", "tbl_terima_barang_supplier.IDTBS", "=", "tbl_pembelian.IDTBS")
            ->join("tbl_purchase_order", "tbl_purchase_order.IDPO", "=", "tbl_terima_barang_supplier.IDPO");

        if ($request->get('tanggal_awal')) {
            //tanggal awal tidak kosong
            if ($request->get('tanggal_akhir')) {
                if ($request->get('mata_uang')) {
                    $data->whereBetween('tbl_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));
                } else {
                    $data->whereBetween('tbl_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                }
                if ($request->get('field') && $request->get('keyword')) {
                    $data->whereBetween('tbl_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));
                    if ($request->get('field') == 'NomorINV') {
                        $data->where('tbl_pembelian.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    } elseif ($request->get('field') == 'NomorPB') {
                        $data->where('tbl_terima_barang_supplier.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    } elseif ($request->get('field') == 'Nomor') {
                        $data->where('tbl_purchase_order.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    } else {
                        $data->where('' . $request->get('field') . '', 'ilike', '%' . $request->get('keyword') . '%');
                    }
                }
            } else {
                $data->where('tbl_pembelian.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            //tanggal awal kosong
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_pembelian.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        $data->orderBy('tbl_pembelian.Tanggal', 'asc');

        $data->get();
        return Datatables::of($data)->make(true);
    }
    public function get_barang()
    {
        DB::enableQueryLog();
        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        if (Input::get('q')) {
            $queryBuilder->where('Nama_Barang', 'iLike', '%' . Input::get('q') . '%');
        }

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_satuan()
    {
        $queryBuilder = VListSatuanKonversiModel::select('*');

        if (Input::get('q')) {
            $queryBuilder->where('Satuan', 'iLike', '%' . Input::get('q') . '%');
        }
        $queryBuilder->where('IDBarang', Input::get('IDBarang'));

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_harga_jual()
    {
        $queryBuilder = HargaJualModel::select('*');

        $queryBuilder->where('IDBarang', '=', Input::get('IDBarang'));
        $queryBuilder->where('IDSatuan', '=', Input::get('IDSatuan'));
        $queryBuilder->where('IDMataUang', '=', Input::get('IDMataUang'));

        $response = $queryBuilder->first();

        return json_encode($response);
    }
    public function get_coa(Request $request)
    {
        $jenis = $request->Jenis_Pembayaran;

        if ($jenis == 'transfer') {
            $data = DB::table('tbl_coa')
                ->where('Kode_COA', 'LIKE', '10103.%')
                ->get();
        } elseif ($jenis == 'giro') {
            $data = DB::table('tbl_coa')
                ->where('Kode_COA', 'LIKE', '20102')
                ->get();
        } elseif ($jenis == 'cash') {
            $data = DB::table('tbl_coa')
                ->where('Nama_COA', 'LIKE', 'Kas%')
                ->get();
        }

        return json_encode($data);
    }
}
