<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Auth;

class Aksesmenu extends Controller
{
    public function aksesmenu()
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        if($iduser=='1'){
            return DB::table('tbl_menu')
                    ->join('tbl_menu_detail', 'tbl_menu.IDMenu', '=', 'tbl_menu_detail.IDMenu')
                    
                    ->select('tbl_menu.*')
                    ->groupBy('tbl_menu.IDMenu', 'tbl_menu.Menu', 'tbl_menu.Urutan_menu', 'tbl_menu.icon')
                    ->orderBy('tbl_menu.Urutan_menu', 'asc')->get();
        }else{
            return DB::table('tbl_menu')
                    ->join('tbl_menu_detail', 'tbl_menu.IDMenu', '=', 'tbl_menu_detail.IDMenu')
                    ->join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                    ->join('users_group', 'users_group.idgroupuser', '=', 'tbl_menu_role.IDGroupUser')
                    ->join('users', 'users.id', '=', 'users_group.iduser')
                    ->where('users.id', '=', $iduser)
                    ->select('tbl_menu.*')
                    ->groupBy('tbl_menu.IDMenu', 'tbl_menu.Menu', 'tbl_menu.Urutan_menu', 'tbl_menu.icon')
                    ->orderBy('tbl_menu.Urutan_menu', 'asc')->get();
        }
        
        // return DB::table('tbl_menu')
        //         ->whereNotIn('Menu', ['Setting','Asset','Stok Opname', 'Saldo Awal'])
        //         ->orderBy('Urutan_menu', 'asc')
        //         ->get();
    }

    public function aksesmenudetail()
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        if($iduser=='1'){
            return DB::table('tbl_menu_detail')
                        //->where('tbl_menu_detail.Menu_Detail', 'not like', '%Laporan%')
                        ->whereNotIn('tbl_menu_detail.Menu_Detail', [
                            'Laporan Pesanan Pembelian',
                            'Laporan Penerimaan Barang',
                            'Laporan Faktur Pembelian',
                            'Laporan Retur Pembelian',
                            'Laporan Sales Order',
                            'Laporan Surat Jalan',
                            'Laporan Faktur Penjualan',
                            'Laporan Retur Penjualan',
                            'Laporan Pembayaran Hutang',
                            'Laporan Penerimaan Piutang',
                            'Laporan Jurnal Transaksi', 
                            'Laporan Stok', 
                            'Laporan Pindah Gudang', 
                            'Laporan Koreksi Persediaan', 
                            'Laporan Mutasi Giro', 
                            'Laporan Laba/Rugi',
                            'Laporan Neraca'
                        ])
                        ->select('tbl_menu_detail.*')
                        ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }else{
            return DB::table('tbl_menu_detail')
                        ->join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                        ->join('users_group', 'users_group.idgroupuser', '=', 'tbl_menu_role.IDGroupUser')
                        ->join('users', 'users.id', '=', 'users_group.iduser')
                        ->where('users.id', '=', $iduser)
                        //->where('tbl_menu_detail.Menu_Detail', 'not like', '%Laporan%')
                        ->whereNotIn('tbl_menu_detail.Menu_Detail', [
                            'Laporan Pesanan Pembelian',
                            'Laporan Penerimaan Barang',
                            'Laporan Faktur Pembelian',
                            'Laporan Retur Pembelian',
                            'Laporan Sales Order',
                            'Laporan Surat Jalan',
                            'Laporan Faktur Penjualan',
                            'Laporan Retur Penjualan',
                            'Laporan Pembayaran Hutang',
                            'Laporan Penerimaan Piutang',
                            'Laporan Jurnal Transaksi', 
                            'Laporan Laba/Rugi',
                            'Laporan Neraca',
                            'Laporan Stok', 
                            'Laporan Pindah Gudang', 
                            'Laporan Koreksi Persediaan', 
                            'Laporan Mutasi Giro',
                        ])
                        ->select('tbl_menu_detail.*')
                        ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }
        
        
    }

    public function aksessetting()
    {
        return DB::table('tbl_menu_detail')->where('IDMenu', ['17'])->get();  
    }

    public function menusettingcoa()
    {
        return DB::table('tbl_menu_detail')
                    ->whereIn('Menu_Detail', 
                    [
                        'Pesanan Pembelian', 
                        'Penerimaan Barang',
                        'Faktur Pembelian', 
                        'Retur Pembelian', 
                        'Persetujuan Pesanan Pembelian',
                        'Pembayaran Hutang',
                         
                        'Sales Order',
                        'Surat Jalan', 
                        'Faktur Penjualan', 
                        'Retur Penjualan',
                        'Pembayaran Piutang',
                        'Mutasi Giro'
                    ])
                    ->orderBy('Urutan', 'asc')
                    ->get();
    }

    public function menushow()
    {
        return DB::table('tbl_menu_detail')
                        ->whereNotIn('tbl_menu_detail.Menu_Detail', [
                            'Laporan Pesanan Pembelian',
                            'Laporan Penerimaan Barang',
                            'Laporan Faktur Pembelian',
                            'Laporan Retur Pembelian',
                            'Laporan Sales Order',
                            'Laporan Surat Jalan',
                            'Laporan Faktur Penjualan',
                            'Laporan Retur Penjualan',
                            'Laporan Pembayaran Hutang',
                            'Laporan Penerimaan Piutang',
                            'Laporan Jurnal Transaksi', 
                            'Laporan Laba/Rugi',
                            'Laporan Neraca'
                        ])
                        ->orderBy('tbl_menu_detail.IDMenuDetail', 'asc')->get();
    }

    public function coreset()
    {
        return DB::table('tbl_perusahaan')->first();
    }

    
}
