<?php
/*=====Create DEDY @17/02/2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Datatables;
use Excel;

use App\Models\LaporanModel;
use App\Models\PurchaseOrderModel;
use App\Models\PenerimaanBarangModel;
use App\Models\InvoicePembelianModel;
use App\Models\ReturPembelianModel;
use App\Models\SalesOrderModel;
use App\Models\InvoicePenjualanModel;
use App\Models\ReturPenjualanModel;
use App\Models\SuratJalanModel;
use App\Models\PembayaranHutangModel;
use App\Models\PenerimaanPiutangModel;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        
        if($iduser=='1'){
            $laporanpembelian   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['12'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpenjualan   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['19'])->orderBy('Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['20'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpersediaan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['18'])->orderBy('IDMenuDetail', 'asc')->get();
        }else{
            $laporanpenjualan   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['19'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpembelian   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['12'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['20'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpersediaan  = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['18'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }
        
        
        

        return view('laporan/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'laporanpembelian', 'laporanpenjualan', 'laporankeuangan', 'laporanpersediaan'));
    }

    public function pembelian(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        if($iduser=='1'){
            $laporanpembelian   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['12'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpenjualan   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['19'])->orderBy('Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['20'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpersediaan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['18'])->orderBy('IDMenuDetail', 'asc')->get();
        }else{
            $laporanpenjualan   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['19'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpembelian   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['12'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['20'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpersediaan  = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['18'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }

        return view('laporan/indexpembelian', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'laporanpembelian', 'laporanpenjualan', 'laporankeuangan', 'laporanpersediaan'));
    }

    public function penjualan(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        if($iduser=='1'){
            $laporanpembelian   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['12'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpenjualan   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['19'])->orderBy('Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['20'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpersediaan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['18'])->orderBy('IDMenuDetail', 'asc')->get();
        }else{
            $laporanpenjualan   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['19'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpembelian   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['12'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['20'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpersediaan  = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['18'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }

        return view('laporan/indexpenjualan', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'laporanpembelian', 'laporanpenjualan', 'laporankeuangan', 'laporanpersediaan'));
    }

    public function keuangan(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        if($iduser=='1'){
            $laporanpembelian   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['12'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpenjualan   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['19'])->orderBy('Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['20'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpersediaan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['18'])->orderBy('IDMenuDetail', 'asc')->get();
        }else{
            $laporanpenjualan   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['19'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpembelian   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['12'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['20'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpersediaan  = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['18'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }

        return view('laporan/indexkeuangan', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'laporanpembelian', 'laporanpenjualan', 'laporankeuangan', 'laporanpersediaan'));
    }

    public function persediaan(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        if($iduser=='1'){
            $laporanpembelian   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['12'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpenjualan   = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['19'])->orderBy('Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['20'])->orderBy('IDMenuDetail', 'asc')->get();
            $laporanpersediaan    = LaporanModel::where('Menu_Detail', 'like', '%Laporan%')->whereIn('IDMenu',  ['18'])->orderBy('IDMenuDetail', 'asc')->get();
        }else{
            $laporanpenjualan   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['19'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpembelian   = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                ->whereIn('tbl_menu_detail.IDMenu',  ['12'])
                                ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporankeuangan    = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['20'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
            $laporanpersediaan  = LaporanModel::join('tbl_menu_role', 'tbl_menu_role.IDMenuDetail', '=', 'tbl_menu_detail.IDMenuDetail')
                                    ->where('tbl_menu_detail.Menu_Detail', 'like', '%Laporan%')
                                    ->whereIn('tbl_menu_detail.IDMenu',  ['18'])
                                    ->orderBy('tbl_menu_detail.Urutan', 'asc')->get();
        }

        return view('laporan/indexpersediaan', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'laporanpembelian', 'laporanpenjualan', 'laporankeuangan', 'laporanpersediaan'));
    }

    public function purchaseorderexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = PurchaseOrderModel::
                                    join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_purchase_order.IDSupplier')
                                    ->join('tbl_purchase_order_detail', 'tbl_purchase_order_detail.IDPO', '=', 'tbl_purchase_order.IDPO')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang','=', 'tbl_purchase_order_detail.IDBarang')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_purchase_order.IDMataUang')
                                    ->select('tbl_purchase_order.Tanggal',
                                                'tbl_purchase_order.Nomor',
                                                'tbl_supplier.Nama as Nama_Supplier',
                                                'tbl_barang.Nama_Barang',
                                                'tbl_purchase_order_detail.Qty',
                                                'tbl_mata_uang.Mata_uang',
                                                'tbl_purchase_order_detail.Harga as Harga_Satuan'
                                                )
                                    ->get()->toArray(); 
		return Excel::create('Laporan_PurchaseOrder', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function penerimaanbarangexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = PenerimaanBarangModel::
                                    join('tbl_terima_barang_supplier_detail','tbl_terima_barang_supplier_detail.IDTBS', '=', 'tbl_terima_barang_supplier.IDTBS')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_terima_barang_supplier_detail.IDBarang')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_terima_barang_supplier.IDSupplier')
                                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_terima_barang_supplier_detail.IDSatuan')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_terima_barang_supplier.IDMataUang')
                                    ->select('tbl_terima_barang_supplier.Tanggal', 
                                                'tbl_terima_barang_supplier.Nomor', 
                                                'tbl_terima_barang_supplier.IDPO', 
                                                'tbl_barang.Nama_Barang',
                                                'tbl_supplier.Nama as Nama_Supplier',
                                                'tbl_terima_barang_supplier_detail.Qty_yard as Qty',
                                                'tbl_mata_uang.Mata_uang',
                                                'tbl_terima_barang_supplier_detail.Harga as Harga_Satuan',
                                                'tbl_satuan.Satuan as Satuan_Barang')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_PenerimaanBarang', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function invoicebeliexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = InvoicePembelianModel::
                                    join('tbl_pembelian_detail', 'tbl_pembelian_detail.IDFB', '=', 'tbl_pembelian.IDFB')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_pembelian.IDSupplier')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_pembelian_detail.IDBarang')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_pembelian.IDMataUang')
                                    ->select('tbl_pembelian.Tanggal', 
                                                'tbl_pembelian.Nomor', 
                                                'tbl_supplier.Nama as Nama_Supplier',
                                                'tbl_barang.Nama_Barang',
                                                'tbl_pembelian_detail.Qty_yard as Qty',
                                                'tbl_mata_uang.Mata_uang',
                                                'tbl_pembelian_detail.Harga as Harga_Satuan'
                                                )
                                    ->get()->toArray(); 
		return Excel::create('Laporan_InvoicePembelian', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function returbeliexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = ReturPembelianModel::join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_retur_pembelian.IDSupplier')
                                    ->join('tbl_retur_pembelian_detail', 'tbl_retur_pembelian.IDRB', '=', 'tbl_retur_pembelian_detail.IDRB')
                                    ->select('tbl_retur_pembelian.Tanggal', 
                                                'tbl_retur_pembelian.Nomor', 
                                                'tbl_supplier.Nama', 
                                                'tbl_retur_pembelian_detail.Qty_yard as Qty_Retur',
                                                'tbl_retur_pembelian.Keterangan')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_ReturPembelian', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function salesorderexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = SalesOrderModel::join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_sales_order.IDCustomer')
                            ->join('tbl_sales_order_detail', 'tbl_sales_order.IDSOK', '=', 'tbl_sales_order_detail.IDSOK')
                            ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                            ->select('tbl_sales_order.Tanggal', 
                                        'tbl_sales_order.Nomor', 
                                        'tbl_customer.Nama as Nama_Customer',
                                        'tbl_barang.Nama_Barang',
                                        'tbl_sales_order_detail.Qty',
                                        'tbl_sales_order_detail.Harga as Harga_Satuan',
                                        'tbl_sales_order_detail.Sub_total as Subtotal_Harga')
                            ->get()->toArray(); 
		return Excel::create('Laporan_SalesOrder', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function invoicejualexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = InvoicePenjualanModel::
                                    join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_penjualan.IDCustomer')
                                    ->join('tbl_penjualan_detail', 'tbl_penjualan_detail.IDFJ', '=', 'tbl_penjualan.IDFJ')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_penjualan_detail.IDBarang')
                                    ->select('tbl_penjualan.Tanggal', 
                                                'tbl_penjualan.Nomor', 
                                                'tbl_customer.Nama as Nama_Customer',
                                                'tbl_barang.Nama_Barang',
                                                'tbl_penjualan_detail.Qty',
                                                'tbl_penjualan_detail.Harga as Harga_Satuan',
                                                'tbl_penjualan_detail.Sub_total as Subtotal_Harga')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_InvoicePenjualan', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function returjualexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = ReturPenjualanModel::join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_retur_penjualan.IDCustomer')
                                    ->join('tbl_retur_penjualan_detail', 'tbl_retur_penjualan_detail.IDRP', '=', 'tbl_retur_penjualan.IDRP')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_retur_penjualan_detail.IDBarang')
                                    ->select('tbl_retur_penjualan.Tanggal',
                                                'tbl_retur_penjualan.Nomor',
                                                'tbl_customer.Nama as Nama_Customer',
                                                'tbl_barang.Nama_Barang',
                                                'tbl_retur_penjualan_detail.Qty',
                                                'tbl_retur_penjualan_detail.Harga',
                                                'tbl_retur_penjualan_detail.Sub_total')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_ReturPenjualan', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function suratjalanexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = SuratJalanModel::join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_surat_jalan_customer.idcustomer')
                                 ->join('tbl_surat_jalan_customer_detail', 'tbl_surat_jalan_customer.idsjc', '=', 'tbl_surat_jalan_customer_detail.idsjc')
                                 ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_surat_jalan_customer_detail.idbarang')
                                 ->select('tbl_surat_jalan_customer.tanggal as Tanggal',
                                                'tbl_surat_jalan_customer.nomor as Nomor',
                                                'tbl_customer.Nama as Nama_Customer',
                                                'tbl_barang.Nama_Barang',
                                                'tbl_surat_jalan_customer_detail.qty as Qty',
                                                'tbl_surat_jalan_customer_detail.hargasatuan as Harga_Satuan',
                                                'tbl_surat_jalan_customer_detail.subtotal as Harga_Total')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_SuratJalan', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function bayarhutangexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = PembayaranHutangModel::join('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_pembayaran_hutang.IDSupplier')
                                    ->select('tbl_pembayaran_hutang.Tanggal', 
                                                'tbl_pembayaran_hutang.Nomor',
                                                'tbl_supplier.Nama as Nama_Supplier',
                                                'tbl_pembayaran_hutang.Pembayaran')
                                    ->get()->toArray(); 
		return Excel::create('Laporan_PembayaranHutang', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

    public function terimapiutangexcel(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');
        
        $data = PenerimaanPiutangModel::join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_penerimaan_piutang.IDCustomer')
                                        ->select('tbl_penerimaan_piutang.Tanggal',
                                                    'tbl_penerimaan_piutang.Nomor',
                                                    'tbl_customer.Nama as Nama_Customer',
                                                    'tbl_penerimaan_piutang.Pembayaran')
                                        ->get()->toArray(); 
		return Excel::create('Laporan_PenerimaanPiutang', function($excel) use ($data) {
			$excel->sheet('mySheet', function($sheet) use ($data)
	        {
				$sheet->fromArray($data);
	        });
		})->download('xlsx');
    }

}
