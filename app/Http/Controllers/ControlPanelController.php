<?php
/*=====Create DEDY @29/01/2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Illuminate\Support\Facades\Storage;

use Datatables;
use App\Models\ControlPanelModel;
use App\Models\CoaModel;
use App\Models\SettingCOAModel;
use App\Models\PerusahaanModel;
use App\Models\CaraBayarModel;
use App\Models\UserModel;
use App\Models\GroupUserModel;
use App\Models\UserPermissionModel;
use App\Models\KodeTransaksiModel;


// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class ControlPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $controlpanel       = ControlPanelModel::where('Group', '=', '1')->orderBy('Urutan', 'asc')->get();
        $perusahaan         = ControlPanelModel::GetPerusahaan();
        $DataKota           = DB::table('tbl_kota')
                                ->orderBy('tbl_kota.Kota', 'asc')
                                ->get();
        $DataFont           = DB::table('tbl_font')
                                ->orderBy('Nama', 'asc')
                                ->get();
        $logohead           = PerusahaanModel::first();
        $DataFontSize       = DB::table('tbl_fontsize')
                                ->orderBy('Angka', 'asc')
                                ->get();
        $DataFontStyle      = DB::table('tbl_fontstyle')
                                ->orderBy('IDStyle', 'asc')
                                ->get();
        $IDMenu             = ControlPanelModel::Getformenusetting();
        $datakode           = DB::table('tbl_kode_transaksi')->join('tbl_menu_detail', 'tbl_menu_detail.IDMenuDetail', '=', 'tbl_kode_transaksi.IDMenuDetail')
                                ->get();
        return view('controlpanel/index2', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset', 'namauser', 'controlpanel','perusahaan', 
        'DataKota',
        'DataFont',
        'logohead',
        'DataFontSize',
        'DataFontStyle',
        'IDMenu',
        'datakode'    
        ));
    
    }

    public function settingcoa(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = CoaModel::orderBy('IDCoa', 'asc')->get();
        $menu               = app('App\Http\Controllers\Aksesmenu')->menusettingcoa();
        $settingcoa         = SettingCOAModel::Getindex()->get();
                                
    
        return view('controlpanel/coa', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa', 'menu', 'settingcoa'));
    
    }

    public function simpancoa(Request $request)
    {
        $data = $request->get('data1');

        $IDSETCOA = uniqid();
        DB::beginTransaction();
        $setcoa = new SettingCOAModel;

        $setcoa->IDSETCOA     = $IDSETCOA;
        $setcoa->IDMenu       = $data['IDMenu'];
        $setcoa->IDCoa        = $data['IDCoa'];
        $setcoa->Posisi       = $data['Posisi'];
        $setcoa->CID          = Auth::user()->id;
        $setcoa->CTime        = date('Y-m-d H:i:s');
        $setcoa->Tingkat      = $data['Tingkat'];
        $setcoa->Cara         = $data['Cara'];
        $setcoa->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'IDSETCOA'     => $IDSETCOA,
            ]
        );
        return json_encode($data);
    }

    public function editsetcoa(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = CoaModel::orderBy('IDCoa', 'asc')->get();
        $menu               = app('App\Http\Controllers\Aksesmenu')->menusettingcoa();
        $settingcoa         = SettingCOAModel::Getindex()->get();
        $detaildata         = SettingCOAModel::where('IDSETCOA', '=', $request->id)->first();
    
        return view('controlpanel/coaedit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa', 'menu', 'settingcoa', 'detaildata'));
    
    }

    public function simpancoaedit(Request $request)
    {
        $data = $request->get('data1');
        DB::beginTransaction();
        $IDSETCOA = $data['IDSETCOA'];

        $setcoa = SettingCOAModel::findOrfail($IDSETCOA);
        $setcoa->IDMenu       = $data['IDMenu'];
        $setcoa->IDCoa        = $data['IDCoa'];
        $setcoa->Posisi       = $data['Posisi'];
        $setcoa->MID          = Auth::user()->id;
        $setcoa->MTime        = date('Y-m-d H:i:s');
        $setcoa->Tingkat      = $data['Tingkat'];
        $setcoa->Cara         = $data['Cara'];
        $setcoa->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diedit.',
            'data'      => [
                'IDSETCOA'     => $IDSETCOA,
            ]
        );
        return json_encode($data);
    }

    public function hapussetcoa(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('settingcoa')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }

        DB::table('tbl_settingcoa')->where('IDSETCOA', '=', $request->id)->delete();

        return redirect('settingcoa')->with('alert','Berhasil Hapus Data');
    }

    public function setperusahaan(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $perusahaan         = ControlPanelModel::GetPerusahaan();
        $DataKota           = DB::table('tbl_kota')
                                ->orderBy('tbl_kota.Kota', 'asc')
                                ->get();
        $DataFont           = DB::table('tbl_font')
                                ->orderBy('Nama', 'asc')
                                ->get();
        return view('controlpanel/indexperusahaan', 
                compact('aksesmenu', 
                        'aksesmenudetail', 
                        'aksessetting', 'coreset',
                        'namauser', 
                        'perusahaan', 
                        'DataKota',
                        'DataFont'
                    ));
    
    }

    public function simpanperusahaan(Request $request)
    {
        $data = $request->get('data1');

        DB::beginTransaction();
        $IDPerusahaan = $data['IDPerusahaan'];

        $setdata = PerusahaanModel::findOrfail($IDPerusahaan);
        $setdata->Nama          = $data['Nama'];
        $setdata->Alamat        = $data['Alamat'];
        $setdata->Kecamatan     = $data['Kecamatan'];
        $setdata->Kota          = $data['Kota'];
        $setdata->Telp          = $data['Telp'];
        $setdata->Fax           = $data['Fax'];
        //$setdata->Logo          = '-';
        $setdata->Kontak        = $data['Kontak'];
        $setdata->Email         = $data['Email'];
        $setdata->MID           = Auth::user()->id;
        $setdata->MTime         = date('Y-m-d H:i:s');
        $setdata->Font          = $data['Font'];
        $setdata->FontSize      = $data['FontSize'];
        $setdata->FontStyle     = $data['FontStyle'];
        $setdata->Angkakoma     = $data['Angkakoma'];
        $setdata->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diedit.',
            'data'      => [
                'IDPerusahaan'     => $IDPerusahaan,
            ]
        );
        return json_encode($data);
    }

    public function setcarabayar(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $coa                = ControlPanelModel::Getcoabayar();
        $carabayar          = CaraBayarModel::Getforindex();
        
        return view('controlpanel/indexcarabayar', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'carabayar', 'coa'));
    
    }

    public function simpancarabayar(Request $request)
    {
        $data = $request->get('data1');

        $IDCaraBayar = uniqid();
        DB::beginTransaction();
        $setcarabayar = new CaraBayarModel;

        $setcarabayar->IDCaraBayar  = $IDCaraBayar;
        $setcarabayar->Nama         = $data['CaraBayar'];
        $setcarabayar->IDCoa        = $data['IDCoa'];
        $setcarabayar->CID          = Auth::user()->id;
        $setcarabayar->CTime        = date('Y-m-d H:i:s');
        $setcarabayar->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'IDCaraBayar'     => $IDCaraBayar,
            ]
        );
        return json_encode($data);
    }

    public function editcarabayar(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = ControlPanelModel::Getcoabayar();
        $carabayar          = CaraBayarModel::Getforindex();
        $detaildata         = CaraBayarModel::where('IDCaraBayar', '=', $request->id)->first();
    
        return view('controlpanel/carabayaredit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'carabayar', 'coa', 'detaildata'));
    
    }

    public function simpancarabayaredit(Request $request)
    {
        $data = $request->get('data1');
        DB::beginTransaction();
        $IDCaraBayar = $data['IDCaraBayar'];

        $setbayar = CaraBayarModel::findOrfail($IDCaraBayar);
        $setbayar->IDCaraBayar    = $data['IDCaraBayar'];
        $setbayar->Nama           = $data['CaraBayar'];
        $setbayar->IDCoa          = $data['IDCoa'];
        $setbayar->MID            = Auth::user()->id;
        $setbayar->MTime          = date('Y-m-d H:i:s');
        $setbayar->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diedit.',
            'data'      => [
                'IDCaraBayar'     => $IDCaraBayar,
            ]
        );
        return json_encode($data);
    }

    public function setcarabayarmenu(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $coa                = ControlPanelModel::Getcoabayar();
        $carabayar          = CaraBayarModel::Getforindex();
        
        return view('controlpanel/carabayarmenu', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'carabayar', 'coa'));
    
    }

    public function simpanbayarmenu(Request $request)
    {
        $po  = $request->get('PO_array');
        $fb  = $request->get('FB_array');
        $ph  = $request->get('PH_array');

        $so  = $request->get('SO_array');
        $fj  = $request->get('FJ_array');
        $pp  = $request->get('PP_array');

        $yes = 'yes';
        $no  = 'no';
        
        if($po!=''){
            $links = implode("','", $po);
            $jml = count($request->get('PO_array'));

            DB::update('UPDATE tbl_cara_bayar SET "PO"=\''.$yes.'\' WHERE "Nama" IN (\''.$links.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "PO"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links.'\')');
        }
        if($fb!=''){
            $links2 = implode("','", $fb);
            $jml2 = count($request->get('FB_array'));

            DB::update('UPDATE tbl_cara_bayar SET "FB"=\''.$yes.'\' WHERE "Nama" IN (\''.$links2.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "FB"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links2.'\')');
        }
        if($ph!=''){
            $links3 = implode("','", $ph);
            $jml3 = count($request->get('PH_array'));

            DB::update('UPDATE tbl_cara_bayar SET "PH"=\''.$yes.'\' WHERE "Nama" IN (\''.$links3.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "PH"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links3.'\')');
        }

        
        
        if($so!=''){
            $links = implode("','", $so);
            $jml = count($request->get('SO_array'));

            DB::update('UPDATE tbl_cara_bayar SET "SO"=\''.$yes.'\' WHERE "Nama" IN (\''.$links.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "SO"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links.'\')');
        }
        if($fj!=''){
            $links2 = implode("','", $fj);
            $jml2 = count($request->get('FJ_array'));

            DB::update('UPDATE tbl_cara_bayar SET "FJ"=\''.$yes.'\' WHERE "Nama" IN (\''.$links2.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "FJ"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links2.'\')');
        }
        if($pp!=''){
            $links3 = implode("','", $pp);
            $jml3 = count($request->get('PP_array'));

            DB::update('UPDATE tbl_cara_bayar SET "PP"=\''.$yes.'\' WHERE "Nama" IN (\''.$links3.'\')');
            DB::update('UPDATE tbl_cara_bayar SET "PP"=\''.$no.'\' WHERE "Nama" NOT IN (\''.$links3.'\')');
        }
        
        //cek dedy
        return redirect('settingcarabayarmenu')->with('alert', 'Data Berhasil Disimpan');
    }

    public function indexcoa(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $controlpanel       = ControlPanelModel::where('Group', '=', '2')->orderBy('Urutan', 'asc')->get();
        return view('controlpanel/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'controlpanel'));
    
    }

    public function uploadlogohead(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $logohead           = PerusahaanModel::first();
        return view('controlpanel/uploadhead', compact('aksesmenu', 
                                                        'aksesmenudetail', 
                                                        'aksessetting', 'coreset',
                                                        'namauser',
                                                        'logohead'));
    }

    public function simpanheadlogo(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/newtemp');
            $image->move($destinationPath, $name);
            //$this->save();
            $dataup = [
                'Logo_head' => $name
            ];
            DB::table('tbl_perusahaan')->where('IDPerusahaan', '=', '1')->update($dataup);

            return back()->with('success','Berhasil Upload Image');
        }
    }

    public function indexuser(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $user               = UserModel::orderBy('id', 'asc')->get();
        return view('user/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'user'));
    
    }

    public function tambahuser(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $user               = UserModel::orderBy('id', 'asc')->get();
        return view('auth/register', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'user'));
    
    }

    public function simpanuser(Request $request)
    {
        $id = UserModel::max('id');
        
        UserModel::create([
            'id'   => $id+1,
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        return back()->with('alert','Berhasil Simpan User Baru');
    }

    public function edituser(Request $request)
    {
        $iduser = $request->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $user               = UserModel::where('id', '=', $iduser)->first();
        return view('auth/edit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'user'));
    }

    public function simpanedituser(Request $request)
    {
        $dataupdate = [
            'name'  => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ];
        DB::table('users')->where('id', '=', $request->get('id'))->update($dataupdate);

        return back()->with('alert2','Berhasil Edit User');
    }

    public function indexgroupuser(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $user               = GroupUserModel::orderBy('IDGroupUser', 'asc')->get();
        return view('user/indexgroup', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'user'));
    
    }

    public function tambahgroupuser(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('user/groupadd', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    
    }

    public function simpangroupuser(Request $request)
    {
        $id = DB::table('tbl_group_user')->max('IDGroupUser');
        $data = $request->get('data1');

        $datasimpan = [
            'IDGroupUser' => $id+1,
            'Group_User'  => $data['Nama'],
        ];
        $hasil = DB::table('tbl_group_user')->insert($datasimpan);

        return response()->json($hasil); 
    }

    public function editgroupuser(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $group              = DB::table('tbl_group_user')->where('IDGroupUser', '=', $request->id)->first();
        return view('user/groupedit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'group'));
    
    }

    public function simpangroupuseredit(Request $request)
    {
        $data = $request->get('data1');
        
        $datasimpan = [
            'Group_User' => $data['Nama'],
        ];

        $hasil = DB::table('tbl_group_user')->where('IDGroupUser', '=', $data['IDGroup'])->update($datasimpan);

        return response()->json($hasil);
    }

    public function aksesgroup(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $group              = DB::table('tbl_group_user')->where('IDGroupUser', '=', $request->id)->first();
        $menu               = DB::select('SELECT * 
                                            FROM tbl_menu_detail 
                                            WHERE "IDMenuDetail" NOT IN 
                                                (SELECT "IDMenuDetail" FROM tbl_menu_role WHERE "IDGroupUser"='.$request->id.')
                                            ORDER BY "Menu_Detail" ASC
                                        ');
        $menurole           = DB::table('tbl_menu_role')
                                ->join('tbl_menu_detail', 'tbl_menu_detail.IDMenuDetail', '=', 'tbl_menu_role.IDMenuDetail')
                                ->select('tbl_menu_detail.IDMenuDetail', 'tbl_menu_detail.Menu_Detail')
                                ->where('tbl_menu_role.IDGroupUser', '=', $request->id)->get();
        //var_dump($menurole);
        //die();
        return view('user/groupakses', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'group', 'menu', 'menurole'));
    
    }

    public function simpanakses(Request $request)
    {
        $idgroup = $request->get('idgroup');
        $idmenu  = $request->get('idmenu_array');
        $idmenu2 = $request->get('idmenu2_array');
        

        //===========hapus dulu data lama
        DB::table('tbl_menu_role')->where('IDGroupUser', '=', $idgroup)->delete();
        //===========insert data baru
        
        if($idmenu!='' || $idmenu=null){
            $jml = count($idmenu);
            for ($x = 0; $x < $jml; ++$x) {
                $idakses = DB::table('tbl_menu_role')->max('IDMenuRole');
                $datasimpan = array(
                    'IDMenuRole'    => $idakses+1,
                    'IDGroupUser'   => $idgroup,
                    'IDMenuDetail'  => $idmenu[$x],
                );
                DB::table('tbl_menu_role')->insert($datasimpan);
            }
        }
        // var_dump($idmenu);
        // die();
        if($idmenu2!='' || $idmenu2=null){
            $jml2 = count($idmenu2);
            
            for ($x = 0; $x < $jml2; ++$x) {
                $idakses = DB::table('tbl_menu_role')->max('IDMenuRole');
                $datasimpan = array(
                    'IDMenuRole'    => $idakses+1,
                    'IDGroupUser'   => $idgroup,
                    'IDMenuDetail'  => $idmenu2[$x],
                );
                DB::table('tbl_menu_role')->insert($datasimpan);
            }
        }

        return back()->with('alert','Berhasil Simpan Akses Baru');
    }

    public function creategrouping(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $group              = DB::table('tbl_group_user')->orderBy('IDGroupUser', 'asc')->get();

        $user               = DB::table('users')->where('id', '=', $request->id)->first();
        
        return view('user/grouping', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'group', 'user'));
    
    }

    public function simpangrouping(Request $request)
    {
        $id = DB::table('users_group')->max('idgu');
        $cekdata = DB::table('users_group')->where('iduser', '=', $request->get('iduser'))->where('idgroupuser', '=', $request->get('idgroup'))->first();
        if($cekdata==''){
            $simpan = [
                'idgu'          => $id+1,
                'iduser'        => $request->get('iduser'),
                'idgroupuser'   => $request->get('idgroup'),
            ];
            $hasil = DB::table('users_group')->insert($simpan);
        }else{
            $simpan = [
                'iduser'        => $request->get('iduser'),
                'idgroupuser'   => $request->get('idgroup'),
            ];
            $hasil = DB::table('users_group')->where('idgu', '=', $cekdata->idgu)->update($simpan);
        }
        

        return redirect('ControlPanelUser')->with('alert','Berhasil Simpan Group User Baru');
    }

    public function indexkeuangan(Request $request)
    {
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('keuangan/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    
    }

    public function indexpermission(Request $request)
    {
        $iduser             = $request->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $userakses          = DB::table('users_akses')->where('IDUser', '=', $iduser)->first();
        $menushow           = app('App\Http\Controllers\Aksesmenu')->menushow();

        return view('user/permission', compact('aksesmenu', 'aksesmenudetail', 'coreset','aksessetting','coreset', 'namauser', 'iduser', 'userakses', 'menushow'));
    
    }

    public function simpanpermission(Request $request)
    {
        //=======CEK DI DB
        $cekuser = UserPermissionModel::where('IDUser', '=', $request->user)->first();
        if($request->tambah=='on')
        {
            $tambah = 'yes';
        }else{
            $tambah = 'no';
        }

        if($request->ubah=='on')
        {
            $ubah = 'yes';
        }else{
            $ubah = 'no';
        }

        if($request->hapus=='on')
        {
            $hapus = 'yes';
        }else{
            $hapus = 'no';
        }
        
        if($cekuser=='')
        {
            $IDAkses = uniqid();
            $data = [
                'IDAkses' => $IDAkses,
                'IDUser'  => $request->user,
                'Tambah'  => $tambah,
                'Ubah'    => $ubah,
                'Hapus'   => $hapus,
            ];
            DB::table('users_akses')->insert($data);
        }else{
            DB::table('users_akses')->where('IDUser', '=', $request->user)->delete();
            $IDAkses = uniqid();
            $data = [
                'IDAkses' => $IDAkses,
                'IDUser'  => $request->user,
                'Tambah'  => $tambah,
                'Ubah'    => $ubah,
                'Hapus'   => $hapus,
            ];
            DB::table('users_akses')->insert($data);
        }
        
        return redirect('ControlPanelUser')->with('alert','Berhasil Simpan Permission');
    }

    public function simpankodetrans(Request $request)
    {
        $data = $request->get('data1');
        
        $cekdata = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', $data['IDMenu'])->first();
        if($cekdata==''){
            $IDKode = uniqid();
            DB::beginTransaction();
            $setkode = new KodeTransaksiModel;

            $setkode->IDKode       = $IDKode;
            $setkode->Kode         = $data['Nama'];
            $setkode->IDMenuDetail = $data['IDMenu'];
            $setkode->save();
            $data = DB::commit();
            
            return json_encode($data);
        }else{
            $IDKode = $data['IDMenu'];
            $dataupdate = [
                "Kode"  => $data['Nama'],
            ];
            $data = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', $IDKode)->update($dataupdate);
            return json_encode($data);
        }
    }
    

}