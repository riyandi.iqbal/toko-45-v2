<?php
/*=====Create TIAR @12/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\BarangModel;
use App\Models\SatuanModel;
use App\Models\HargaJualModel;
use App\Models\GroupCustomerModel;
use App\Models\MataUangModel;
use App\Models\VListSatuanKonversiModel;

// HELPERS //
use App\Helpers\AppHelper;

class HargaJualBarangController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('hargajualbarang.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function datatable(Request $request) {
        $data = HargaJualModel::Datatable();

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $data_barang        = BarangModel::leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan')->get();
        $data_customer_group = GroupCustomerModel::get();
        $data_mata_uang     = MataUangModel::where('Aktif', 'aktif')->OrderBy('Kurs', 'asc')->get();

        return view('hargajualbarang.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'data_barang', 'data_customer_group', 'data_mata_uang')); 
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDBarang'             => 'required',
            'IDSatuan.*'           => 'required',
            'IDGroupCustomer.*'    => 'required',
            'IDMataUang.*'         => 'required',
            'Modal.*'              => 'required',
            'Harga_Jual.*'         => 'required',
        ])->setAttributeNames([
            'IDBarang'             => 'Barang',
            'IDSatuan.*'           => 'Satuan', 
            'IDGroupCustomer.*'    => 'Group Customer',
            'IDMataUang.*'         => 'Mata Uang',
            'Modal.*'              => 'Modal',
            'Harga_Jual.*'         => 'Harga Jual',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        foreach ($request->IDGroupCustomer as $key => $value) {
            $data_harga_jual_exist = HargaJualModel::where('IDBarang', $request->IDBarang)->where('IDSatuan', $request->IDSatuan[$key])->where('IDGroupCustomer', $value)->where('IDMataUang', $request->IDMataUang[$key])->first();

            if ($data_harga_jual_exist) {
                $data = HargaJualModel::find($data_harga_jual_exist->IDHargaJual);

                $data->Modal    = AppHelper::StrReplace($request->Modal[$key]);
                $data->Harga_Jual = AppHelper::StrReplace($request->Harga_Jual[$key]);
                $data->MID              = Auth::user()->id;
                $data->MTime            = date('Y-m-d H:i:s');

                $data->save();
            } else {
                $data = new HargaJualModel;

                $data->IDHargaJual = uniqid();
                $data->IDBarang         = $request->IDBarang;
                $data->IDSatuan         = $request->IDSatuan[$key];
                $data->IDMataUang       = $request->IDMataUang[$key];
                $data->IDGroupCustomer  = $request->IDGroupCustomer[$key];
                $data->Modal            = AppHelper::StrReplace($request->Modal[$key]);
                $data->Harga_Jual       = AppHelper::StrReplace($request->Harga_Jual[$key]);
                $data->CID              = Auth::user()->id;
                $data->CTime            = date('Y-m-d H:i:s');
                $data->Status           = 'aktif';

                $data->save();
            }
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $data_barang        = BarangModel::findOrfail($id);

        $data_harga_jual_barang     = HargaJualModel::Getforindex()->where('tbl_barang.IDBarang', $data_barang->IDBarang)->get();
        
        $data_customer_group = GroupCustomerModel::get();
        $data_mata_uang     = MataUangModel::where('Aktif', 'aktif')->OrderBy('Kurs', 'asc')->get();

        return view('hargajualbarang.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'coreset', 'data_barang', 'data_harga_jual_barang', 'data_customer_group', 'data_mata_uang'));
    }

    public function update_data(Request $request) {
        DB::beginTransaction();

        $data_harga_jual = HargaJualModel::findOrfail($request->IDHargaJual);

        $data_exist = HargaJualModel::where('IDBarang', $request->IDBarang)
                                    ->where('IDGroupCustomer', $request->IDGroupCustomer ? $request->IDGroupCustomer : $data_harga_jual->IDGroupCustomer )
                                    ->where('IDMataUang', $request->IDMataUang ? $request->IDMataUang : $data_harga_jual->IDMataUang )
                                    ->where('IDSatuan', $request->IDSatuan)
                                    ->first();

        if ($data_exist) {
            if ($data_exist->IDHargaJual != $request->IDHargaJual) {
                $data = array (
                    'status'    => false,
                    'message'   => 'Data telah ada.',
                );
        
                return json_encode($data);
            }
        }

        $data = HargaJualModel::findOrfail($request->IDHargaJual);
        
        $data->IDGroupCustomer  = $request->IDGroupCustomer ? $request->IDGroupCustomer : $data_harga_jual->IDGroupCustomer;
        $data->IDMataUang       = $request->IDMataUang ? $request->IDMataUang : $data_harga_jual->IDMataUang;
        $data->Modal            = AppHelper::StrReplace($request->Modal);
        $data->Harga_Jual       = AppHelper::StrReplace($request->Harga_Jual);
        $data->MID              = Auth::user()->id;
        $data->MTime            = date('Y-m-d H:i:s');

        $data->save(); 
        
        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.',
        );

        return json_encode($data);
    }

    public function destroy($id) {
        DB::beginTransaction();

        $data_exist = HargaJualModel::findOrfail($id);

        $data_exist->delete();

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function get_satuan(Request $request) {
        $queryBuilder = VListSatuanKonversiModel::select('*');

        $queryBuilder->where('IDBarang', $request->IDBarang);

        $response = $queryBuilder->get();

        return json_encode($response);
    }
}