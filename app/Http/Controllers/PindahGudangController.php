<?php
/*=====Created by Tiar Sagita Rahman @16 Maret 2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

use App\Models\PindahGudangModel;
use App\Models\PindahGudangDetailModel;
use App\Models\VListPindahGudangModel;
use App\Models\VListPindahGudangDetailModel;
use App\Models\BarangModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\GudangModel;

class PindahGudangController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('pindahgudang.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('pindahgudang.laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = VListPindahGudangModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListPindahGudangDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $gudang     = GudangModel::get();

        return view('pindahgudang.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'gudang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Nomor'             => 'required',
            'Keterangan'        => 'nullable',
            'Total_qty'         => 'required',
            'IDGudangAwal'      => 'required',
            'IDGudangTujuan'    => 'required',
            'Grand_total'       => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'Tanggal'           => 'Tanggal',
            'Keterangan'        => 'Keterangan',
            'Total_qty'         => 'Total Qty',
            'IDGudangAwal'      => 'Gudang Awal',
            'IDGudangTujuan'    => 'Gudang Tujuan',
            'Grand_total'       => 'Grand Total',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDPG = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor);

        DB::beginTransaction();

        $PindahGudang = new PindahGudangModel;

        $PindahGudang->IDPG    = $IDPG;
        $PindahGudang->Tanggal     = AppHelper::DateFormat($request->Tanggal);
        $PindahGudang->Nomor       = $Nomor;
        $PindahGudang->Total       = $request->Total_qty;
        $PindahGudang->IDGudangAwal         = $request->IDGudangAwal;
        $PindahGudang->IDGudangTujuan       = $request->IDGudangTujuan;
        $PindahGudang->IDMataUang  = 'P000001';
        $PindahGudang->Keterangan  = $request->Keterangan;
        $PindahGudang->dibuat_pada    = date('Y-m-d H:i:s');
        $PindahGudang->dibuat_oleh    = Auth::user()->id;
        $PindahGudang->diubah_pada    = date('Y-m-d H:i:s');
        $PindahGudang->diubah_oleh    = Auth::user()->id;

        $PindahGudang->save();

        foreach ($request->IDBarang as $key => $value) {
            $data_barang = BarangModel::find($value);

            $IDPGDetail = uniqid();
            $PindahGudangDetail = new PindahGudangDetailModel();

            $PindahGudangDetail->IDPGDetail   = uniqid();
            $PindahGudangDetail->IDPG         = $IDPG;
            $PindahGudangDetail->IDBarang     = $value;
            $PindahGudangDetail->IDSatuan     = $request->IDSatuan[$key];
            $PindahGudangDetail->Qty          = $request->Qty[$key];
            $PindahGudangDetail->Saldo        = $request->Qty[$key];
            $PindahGudangDetail->Harga        = AppHelper::StrReplace($request->harga[$key]);
            $PindahGudangDetail->IDMataUang   = 'P000001';

            $PindahGudangDetail->save();

            // UPDATE STOK //
            $data_stok_awal = StokModel::where('IDBarang', $value)->where('IDGudang', '=', $request->IDGudangAwal)->first();
            if ($data_stok_awal) {
                
                $stok = StokModel::find($data_stok_awal->IDStok);
    
                $stok->Qty_pcs = $data_stok_awal->Qty_pcs - $request->Qty[$key];

                $stok->save();

                $kartu_stok_keluar     = new KartustokModel;

                $kartu_stok_keluar->IDKartuStok        = uniqid();
                $kartu_stok_keluar->Tanggal            = AppHelper::DateFormat($request->Tanggal);
                $kartu_stok_keluar->Nomor_faktur       = $Nomor;
                $kartu_stok_keluar->IDFaktur           = $IDPG;
                $kartu_stok_keluar->IDFakturDetail     = $IDPGDetail;
                $kartu_stok_keluar->IDStok         = $data_stok_awal ? $data_stok_awal->IDStok : $IDStok;
                $kartu_stok_keluar->Jenis_faktur   = 'PG';
                $kartu_stok_keluar->IDBarang       = $request->IDBarang[$key];
                $kartu_stok_keluar->Harga          = AppHelper::StrReplace($request->harga[$key]);
                $kartu_stok_keluar->Nama_Barang    = $data_barang->Nama_Barang;
                $kartu_stok_keluar->Keluar_pcs     = $request->Qty[$key];
                $kartu_stok_keluar->IDSatuan       = $request->IDSatuan[$key];

                $kartu_stok_keluar->save();
            } else {
                // $IDStok = uniqid();
                // $stok = new StokModel;

                // $stok->IDStok   = $IDStok;
                // $stok->Tanggal  = date('Y-m-d H:i:s');
                // $stok->Nomor_faktur     = $Nomor;
                // $stok->IDBarang     = $request->IDBarang[$key];
                // $stok->Qty_pcs      = $request->Qty[$key];
                // $stok->Nama_Barang      = $data_barang->Nama_Barang;
                // $stok->Jenis_faktur     = 'PG';
                // $stok->IDGudang     = $request->IDGudangAwal;
                // $stok->IDSatuan     = $request->IDSatuan[$key];

                // $stok->save();
            }

            $data_stok_tujuan = StokModel::where('IDBarang', $value)->where('IDGudang', '=', $request->IDGudangTujuan)->first();

            if ($data_stok_tujuan) {
                $stok = StokModel::find($data_stok_tujuan->IDStok);
    
                $stok->Qty_pcs = $data_stok_tujuan->Qty_pcs + $request->Qty[$key];

                $stok->save();
            } else {
                $IDStok = uniqid();
                $stok = new StokModel;

                $stok->IDStok   = $IDStok;
                $stok->Tanggal         = date('Y-m-d');
                $stok->Nomor_faktur    = $Nomor;
                $stok->Jenis_faktur    = 'PG';
                $stok->IDBarang        = $value;
                $stok->IDSatuan        = $request->IDSatuan[$key];
                $stok->IDGudang        = $request->IDGudangTujuan;
                $stok->Qty_pcs         = $request->Qty[$key];

                $stok->save();

            }

            $kartu_stok_masuk     = new KartustokModel;

            $kartu_stok_masuk->IDKartuStok        = uniqid();
            $kartu_stok_masuk->Tanggal            = AppHelper::DateFormat($request->Tanggal);
            $kartu_stok_masuk->Nomor_faktur       = $Nomor;
            $kartu_stok_masuk->IDFaktur           = $IDPG;
            $kartu_stok_masuk->IDFakturDetail     = $IDPGDetail;
            $kartu_stok_masuk->IDStok         = $data_stok_tujuan ? $data_stok_tujuan->IDStok : $IDStok;
            $kartu_stok_masuk->Jenis_faktur   = 'PG';
            $kartu_stok_masuk->IDBarang       = $request->IDBarang[$key];
            $kartu_stok_masuk->Harga          = AppHelper::StrReplace($request->harga[$key]);
            $kartu_stok_masuk->Nama_Barang    = $data_barang->Nama_Barang;
            $kartu_stok_masuk->Masuk_pcs      = $request->Qty[$key];
            $kartu_stok_masuk->IDSatuan       = $request->IDSatuan[$key];

            $kartu_stok_masuk->save();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }    

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $PindahGudang = VListPindahGudangModel::findOrfail($id);

        $PindahGudangDetail = VListPindahGudangDetailModel::where('IDPG', $PindahGudang->IDPG)->get();

        return view('pindahgudang.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'PindahGudang', 'PindahGudangDetail'));
    }

    public function number(Request $request) {
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = PindahGudangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/PG/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }        
    }

    public function get_barang() {
        DB::enableQueryLog();

        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_harga_jual_barang.Harga_Jual', 'tbl_satuan.Satuan', 'tbl_harga_jual_barang.Modal');
        $queryBuilder->leftJoin('tbl_harga_jual_barang', function($leftJoin)
        {
            $leftJoin->on('tbl_harga_jual_barang.IDBarang', '=', 'tbl_barang.IDBarang');
            $leftJoin->where('tbl_harga_jual_barang.IDSatuan', '=', 'tbl_barang.IDSatuan');
        });
        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');
        $queryBuilder->leftJoin('tbl_stok', 'tbl_stok.IDBarang', '=', 'tbl_barang.IDBarang');

        if (Input::get('q')) {
            $queryBuilder->where('tbl_barang.Nama_Barang', 'iLike', '%' . Input::get('q') . '%' );
        }

        $queryBuilder->where('tbl_stok.IDGudang', '=', Input::get('IDGudang'));

        $response = $queryBuilder->take(10)->get();

        return json_encode($response);
    }

    private function _cek_nomor($nomor) {
        $nomor_exist = PindahGudangModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number();
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number(){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = PindahGudangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/PG/' . $bulan_romawi . '/' . date('y');
        
        return $kode;
    }
}
