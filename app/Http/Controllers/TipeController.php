<?php
/*=====Create TIAR @14/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\TipeModel;

class TipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('tipe.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = TipeModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Tipe')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $kode               = TipeModel::selectRaw(DB::raw('MAX(SUBSTRING("Kode_Tipe", 4, 4)) as curr_number'))->first();
        
        return view('tipe.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'kode'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Kode_Tipe'    => 'required|unique:tbl_tipe_barang,Kode_Tipe',
            'Nama_Tipe'             => 'required',
        ])->setAttributeNames([
            'Kode_Tipe'    => 'Kode',
            'Nama_Tipe'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = TipeModel::selectRaw(DB::raw('MAX("IDTipe") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new TipeModel;

        $data->IDTipe            = $urutan_id;
        $data->Kode_Tipe         = $request->Kode_Tipe;
        $data->Nama_Tipe         = $request->Nama_Tipe;
        $data->Status            = 'aktif';
        $data->CID               = Auth::user()->id;
        $data->CTime             = date('Y-m-d H:i:s');
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Tipe')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $tipe               = TipeModel::findOrfail($id);

        return view('tipe.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'tipe'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDTipe'  => 'required',
            'Kode_Tipe'    => 'required',
            'Nama_Tipe'             => 'required',
        ])->setAttributeNames([
            'IDTipe'  => 'Data',
            'Kode_Tipe'    => 'Kode',
            'Nama_Tipe'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = TipeModel::findOrfail($request->IDTipe);

        $data->Kode_Tipe         = $request->Kode_Tipe;
        $data->Nama_Tipe         = $request->Nama_Tipe;
        $data->Status                = 'aktif';
        $data->MID               = Auth::user()->id;
        $data->MTime             = date('Y-m-d H:i:s');
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = TipeModel::findOrfail($id);

        if ($data_exist->Status == 'aktif') {
            $data_exist->Status = 'nonaktif';
        } else {
            $data_exist->Status = 'aktif';
        }
        
        $data_exist->save();
        
        $data = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($data);
    }
}