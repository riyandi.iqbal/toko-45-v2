<?php
/*=====Create TIAR @06/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use App\Models\GroupCoaModel;
use App\Models\CoaModel;
use Validator;

class GroupCoaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('coa_group.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = GroupCoaModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupCOA')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('coa_group.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Kode_Group_COA'    => 'required|unique:tbl_group_coa,Kode_Group_COA',
            'Nama_Group'             => 'required',
            'Normal_Balance'             => 'required',
        ])->setAttributeNames([
            'Kode_Group_COA'     => 'Kode',
            'Nama_Group'             => 'Nama',
            'Normal_Balance'             => 'Normal Balance',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }        

        $nextnumber = DB::table('tbl_group_coa')->selectRaw(DB::raw('MAX("IDGroupCOA") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        DB::beginTransaction();

        $group_coa  = new GroupCoaModel;

        $group_coa->IDGroupCOA = $urutan_id;
        $group_coa->Kode_Group_COA = $request->Kode_Group_COA;
        $group_coa->Nama_Group = $request->Nama_Group;
        $group_coa->Normal_Balance = $request->Normal_Balance;
        $group_coa->Aktif   = 'aktif';

        $group_coa->save();
        
        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupCOA')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $group_coa               = GroupCoaModel::findOrfail($id);

        return view('coa_group.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'group_coa'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDGroupCOA'        => 'required',
            'Kode_Group_COA'    => 'required',
            'Nama_Group'        => 'required',
            'Normal_Balance'    => 'required',
        ])->setAttributeNames([
            'IDGroupCOA'        => 'Data',
            'Kode_Group_COA'    => 'Kode',
            'Nama_Group'        => 'Nama',
            'Normal_Balance'    => 'Normal Balance',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $group_coa  = GroupCoaModel::findOrfail($request->IDGroupCOA);

        $group_coa->Kode_Group_COA  = $request->Kode_Group_COA;
        $group_coa->Nama_Group      = $request->Nama_Group;
        $group_coa->Normal_Balance  = $request->Normal_Balance;

        $group_coa->save();
        
        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($data);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $group_coa  = GroupCoaModel::findOrfail($id)->delete();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }
}