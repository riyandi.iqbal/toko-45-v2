<?php
/*=====Create TIAR @06/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use App\Models\GroupCoaModel;
use App\Models\CoaModel;
use Validator;

class CoaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('coa.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = CoaModel::select('tbl_group_coa.Nama_Group', 'tbl_coa.*');

        $data->join('tbl_group_coa', 'tbl_group_coa.IDGroupCOA', '=', 'tbl_coa.IDGroupCOA');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Coa')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $groupcoa           = GroupCoaModel::orderBy('Kode_Group_COA', 'asc')
                                ->get();

        return view('coa.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'groupcoa'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'kodecoa'             => 'required|unique:tbl_coa,Kode_COA',
            'namacoa'                  => 'required',
            'statuscoa'                => 'required',
            'Normal_Balance'                => 'required',
            'namagroup'                => 'required',
            'updatesaldo'                => 'required',
        ])->setAttributeNames([
            'kodecoa'             => 'Kode COA',
            'namacoa'                  => 'Nama COA',
            'statuscoa'                => 'Status COA',
            'Normal_Balance'                => 'Normal Balance COA',
            'namagroup'                => 'Nama Group COA',
            'updatesaldo'                => 'Update Saldo Awal',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $nextnumber = DB::table('tbl_coa')->selectRaw(DB::raw('MAX("IDCoa") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $modal= str_replace(".", "", $request->Modal);

        $coa = new CoaModel;

            $coa->IDCoa             = $urutan_id;
            $coa->Kode_COA          = $request->kodecoa;
            $coa->Nama_COA          = $request->namacoa;
            $coa->Status            = $request->statuscoa;
            $coa->Normal_Balance            = $request->Normal_Balance;
            $coa->IDGroupCOA        = $request->namagroup;
            $coa->Modal             = $modal;
            $coa->Aktif             = 'aktif';
            $coa->Saldo_Awal        = $request->updatesaldo;
            
        $coa->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupCOA')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $coa               = CoaModel::findOrfail($id);
        $groupcoa           = DB::table('tbl_group_coa') 
                                ->orderBy('Kode_Group_COA', 'asc')
                                ->get();
        return view('coa.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa', 'groupcoa'));

    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'idcoa'             => 'required',
            'kodecoa'             => 'required',
            'namacoa'                  => 'required',
            'statuscoa'                => 'required',
            'Normal_Balance'                => 'required',
            'namagroup'                => 'required',
            'updatesaldo'                => 'required',
        ])->setAttributeNames([
            'idcoa'             => 'Data',
            'kodecoa'             => 'Kode COA',
            'namacoa'                  => 'Nama COA',
            'statuscoa'                => 'Status COA',
            'Normal_Balance'                => 'Normal Balance COA',
            'namagroup'                => 'Nama Group COA',
            'updatesaldo'                => 'Update Saldo Awal',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $coa = CoaModel::findOrfail($request->idcoa);

            $coa->Kode_COA          = $request->kodecoa;
            $coa->Nama_COA          = $request->namacoa;
            $coa->Status            = $request->statuscoa;
            $coa->IDGroupCOA        = $request->namagroup;
            $coa->Normal_Balance            = $request->Normal_Balance;
            $coa->Aktif             = 'aktif';
            $coa->Saldo_Awal        = $request->updatesaldo;
            
        $coa->save();

        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($data);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        DB::table('tbl_coa')
            ->where('IDCoa', $id)
            ->delete();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function exporttoexcel(Request $request) {
        // create file name
        $fileName = 'Cost Of Account '.time().'.xlsx';  
        // load excel library
        $this->load->library('excel');
        $empInfo = $this->M_coa->exsport_excel();
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'No');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Kode COA');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Nama COA');  
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Nama Group'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Status');  
        // set Row
        $rowCount = 2;
        $i = 1;
        foreach ($empInfo as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $i);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element->Kode_COA);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element->Nama_COA);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element->Nama_Group);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element->Status);
            $rowCount++;
            $i++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($fileName);
        // download file
        header("Content-Type: application/vnd.ms-excel");
        redirect($fileName);
    }

}