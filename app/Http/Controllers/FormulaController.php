<?php
/*=====Create DEDY @03/01/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use App\Models\SatuanModel;
use App\Models\FormulaModel;
use App\Models\FormulaDetailModel;
use App\Models\BarangModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class FormulaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $formula            = FormulaModel::get();

        return view('formula/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'formula'));
    }

    public function datatable(Request $request) {
        $data = FormulaModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function showdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $formula            = FormulaModel::where('IDFormula', '=', $request->id)->first();
        $formuladetail      = DB::table('tbl_formula_detail')
                                ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_formula_detail.IDSatuan')
                                ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_formula_detail.IDBarang')
                                ->where('tbl_formula_detail.IDFormula', '=', $request->id)
                                ->select('tbl_barang.Nama_Barang', 'tbl_satuan.Satuan', 'tbl_formula_detail.Berat')
                                ->get();
        return view('formula/show', compact('aksesmenu',
            'aksesmenudetail',
            'aksessetting',
            'namauser',
            'formula',
            'formuladetail',
            'coreset'
        ));
    }

    public function hapusdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Formula')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        //==========CEK DATA DI PRODUKSI
        $cekdataproduksi = DB::table('tbl_produksibahanbaku')->where('IDFormula', '=', $request->id)->first();
        if(! $cekdataproduksi ){
            DB::table('tbl_formula')->where('IDFormula', '=', $request->id)->delete();
            DB::table('tbl_formula_detail')->where('IDFormula', '=', $request->id)->delete();
            
            $response = array (
                'status'    => true,
                'message'   => 'Data berhasil dihapus.'
            );
    
            return json_encode($response);
        }else{
            $response = array (
                'status'    => true,
                'message'   => 'Data gagal dihapus. Data telah digunakan.'
            );
    
            return json_encode($response);
        }

    }

    public function getsatuan(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT b."Satuan", a."IDSatuan" 
                                        FROM tbl_barang a, tbl_satuan b 
                                        WHERE a."IDSatuan"=b."IDSatuan" AND a."IDBarang"=\''.$data.'\'
                                        ');
        return response()->json($datapencarian);
    }

    public function tambah_data(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Formula')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $satuan             = SatuanModel::get();
        $barang             = BarangModel::where('IDGroupBarang', '!=', 'P000004')->get();

        return view('formula/create', compact('aksesmenu', 
        'aksesmenudetail', 'aksessetting', 'namauser', 'satuan',
        'barang', 'coreset'
        ));
    }

    public function simpandata(Request $request)
    {
        //===============get header
        $tanggal    = $request->get('tanggal');
        $nama       = $request->get('nama');
        $total      = $request->get('total');
        //===============get detail
        $barang     = $request->get('Barang_array');
        $berat      = $request->get('berat_array');
        $satuan     = $request->get('Satuan_array');
        $kodesatuan = $request->get('kode_satuan_array');

        $jml_data = count($barang);
        //=============================simpan header
        $nextnumber = FormulaModel::selectRaw(DB::raw('MAX("IDFormula") as nonext'))->first();
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $formulaheader = new FormulaModel;
        $formulaheader->IDFormula       = $urutan_id;
        $formulaheader->Nama_Formula    = $nama;
        $formulaheader->Total           = $total;
        $formulaheader->Tanggal         = $tanggal;
        $formulaheader->CID             = Auth::user()->id;
        $formulaheader->CTime           = date('Y/m/d H:i:s');
        $formulaheader->save();

        
        for ($x = 0; $x < $jml_data; ++$x) {
            $nextnumber2 = FormulaDetailModel::selectRaw(DB::raw('MAX("IDFormulaDetail") as nonext'))->first();
            if($nextnumber2->nonext==''){
                $urutan_id2 = 'P000001';
            }else{
                $hasil2 = substr($nextnumber2->nonext,2,6) + 1;
                $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $formuladetail = new FormulaDetailModel;
            $formuladetail->IDFormulaDetail         = $urutan_id2;
            $formuladetail->IDFormula               = $urutan_id;
            $formuladetail->IDBarang                = $barang[$x];
            $formuladetail->Berat                   = $berat[$x];
            $formuladetail->IDSatuan                = $kodesatuan[$x];
            $formuladetail->save();
        }

        return redirect('Formula')->with('alert', 'Data Berhasil Disimpan');

    }

    public function editdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Formula')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $header             = FormulaModel::where('IDFormula', '=', $request->id)->first();
//                                    DB::enableQueryLog();
                                    $hdetail            = DB::table('tbl_formula_detail')
                                    ->join('tbl_barang', 'tbl_formula_detail.IDBarang', '=', 'tbl_barang.IDBarang')
                                    ->join('tbl_satuan', 'tbl_formula_detail.IDSatuan', '=', 'tbl_satuan.IDSatuan')
                                    ->join('tbl_formula', 'tbl_formula.IDFormula', '=', 'tbl_formula_detail.IDFormula')
                                    ->where('tbl_formula.IDFormula', '=', $request->id)
                                    ->select('tbl_formula_detail.*', 'tbl_barang.Nama_Barang', 'tbl_satuan.Satuan')
                                    ->get();
//                                    $query = DB::getQueryLog();
//                                    print_r($query);
//                                    die();
        
        $satuan             = SatuanModel::get();
        $barang             = BarangModel::where('IDGroupBarang', '!=', 'P000004')->get();
                                    //var_dump($hdetail);
        return view('formula/edit', compact('aksesmenu', 
        'aksesmenudetail', 'aksessetting', 'namauser', 'satuan',
        'barang', 'header', 'hdetail', 'coreset'
        ));
    }

    public function simpandataedit(Request $request)
    {
        //===============get header
        $idformula  = $request->get('idformula');
        $tanggal    = $request->get('tanggal');
        $nama       = $request->get('nama');
        $total      = $request->get('total');
        //===============get detail
        $barang     = $request->get('Barang_array');
        $berat      = $request->get('berat_array');
        $satuan     = $request->get('Satuan_array');
        $kodesatuan = $request->get('kode_satuan_array');

        $jml_data = count($barang);
        //=============================simpan header
        $dataheader = [
            'Nama_Formula'      => $nama,
            'Total'             => $total,
            'Tanggal'           => $tanggal,
            'MID'               => Auth::user()->id,
            'MTime'             => date('Y/m/d H:i:s'),
        ];
        DB::table('tbl_formula')->where('IDFormula', '=', $idformula)->update($dataheader);
        DB::table('tbl_formula_detail')->where('IDFormula', '=', $idformula)->delete();
        
        for ($x = 0; $x < $jml_data; ++$x) {
            $nextnumber2 = FormulaDetailModel::selectRaw(DB::raw('MAX("IDFormulaDetail") as nonext'))->first();
            if($nextnumber2->nonext==''){
                $urutan_id2 = 'P000001';
            }else{
                $hasil2 = substr($nextnumber2->nonext,2,6) + 1;
                $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $formuladetail = new FormulaDetailModel;
            $formuladetail->IDFormulaDetail         = $urutan_id2;
            $formuladetail->IDFormula               = $idformula;
            $formuladetail->IDBarang                = $barang[$x];
            $formuladetail->Berat                   = $berat[$x];
            $formuladetail->IDSatuan                = $kodesatuan[$x];
            $formuladetail->save();
        }

        return redirect('Formula')->with('alert', 'Data Berhasil Di Edit');
    }

}

?>
