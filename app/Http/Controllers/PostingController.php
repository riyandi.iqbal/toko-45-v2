<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

use App\Models\PostingModel;
use App\Models\LabaRugiModel;
use App\Models\CoaModel;
use App\Models\JurnalModel;
use App\Models\NeracaModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PostingController extends Controller
{
    public function __construct() {
        set_time_limit(0);
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('posting/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function datatable(Request $request) {
        $data = PostingModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('CTime', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('CTime', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('CTime', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('posting/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Bulan'    => 'required',
            'Tahun'             => 'required',
        ])->setAttributeNames([
            'Bulan'     => 'Bulan',
            'Tahun'             => 'Tahun',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $posting_exist = PostingModel::where('Bulan', $request->Bulan)->where('Tahun', $request->Tahun)->first();

        if ($posting_exist) {
            $data = array (
                'status'    => false,
                'message'   => 'Periode tersebut telah diposting.'
            );
    
            return json_encode($data);
        }

        DB::beginTransaction();

        $idposting = uniqid();

        $posting  = new PostingModel;

        $posting->idposting = $idposting;
        $posting->Bulan = $request->Bulan;
        $posting->Tahun = $request->Tahun;
        $posting->Status   = 'aktif';
        $posting->CID      = Auth::user()->id;
        $posting->CTime    = date('Y-m-d H:i:s');

        $posting->save();

        foreach ($request->IDCoa as $key => $value) {
            $jurnal_penjualan = new JurnalModel;

            $jurnal_penjualan->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan->Tanggal      = Date('Y-m-d');
            $jurnal_penjualan->Nomor        = $idposting;
            $jurnal_penjualan->IDFaktur     = $idposting;
            $jurnal_penjualan->IDFakturDetail   = '-';
            $jurnal_penjualan->Jenis_faktur = 'Saldo Awal';
            $jurnal_penjualan->IDCOA        = $request->IDCoa[$key];
            $jurnal_penjualan->Debet        = $request->Debet[$key];
            $jurnal_penjualan->Kredit       = $request->Kredit[$key];
            $jurnal_penjualan->IDMataUang       = 'P000002';
            $jurnal_penjualan->Kurs             = 1;
            $jurnal_penjualan->Total_debet      = $request->Debet[$key];
            $jurnal_penjualan->Total_kredit     = $request->Kredit[$key];
            $jurnal_penjualan->Keterangan       = 'Posting Bulanan';
            $jurnal_penjualan->Saldo            = floatval($request->Debet[$key]) - floatval($request->Kredit[$key]);

            $jurnal_penjualan->save();
        }

        
        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function get_saldo_akhir(Request $request) {
        $coa = CoaModel::get();

        $laba_rugi = LabaRugiModel::select('tbl_setting_labarugi.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_labarugi."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$request->Bulan.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$request->Tahun.'\' GROUP BY "IDCOA") as Total_now'))
                                    ->orderBy('Urutan', 'Asc')
                                    ->get();
        $laba = 0;

        foreach ($laba_rugi as $key => $value) {
            $laba += $value->total_now ? $value->total_now : 0;
        }

        $neraca = NeracaModel::select('tbl_setting_neraca.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_neraca."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$request->Bulan.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$request->Tahun.'\' GROUP BY "IDCOA") as Total_now'))
                                    ->orderBy('Urutan', 'Asc')
                                    ->get();

        $saldo_neraca = 0;
        foreach ($neraca as $key => $value) {
            if ($value->Keterangan == '30103 -- LABA TAHUN BERJALAN') {
                $value->total_now = $laba;
            }

            $saldo_neraca += $value->total_now ? $value->total_now : 0;
        }

        $coa = CoaModel::select('tbl_coa.*', DB::raw('(select SUM(("Debet") * "Kurs") as Debet FROM tbl_jurnal where "IDCOA" = tbl_coa."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$request->Bulan.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$request->Tahun.'\' GROUP BY "IDCOA") as Debet'), DB::raw('(select SUM(("Kredit") * "Kurs") as Kredit FROM tbl_jurnal where "IDCOA" = tbl_coa."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$request->Bulan.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$request->Tahun.'\' GROUP BY "IDCOA") as Kredit'))
                                    ->orderBy('Kode_COA', 'Asc')
                                    ->get();

        $data = array (
            'status'    => true,
            'message'   => '',
            'data'      => [
                'saldo_laba_rugi'   => $laba,
                'saldo_neraca'   => $saldo_neraca,
                'saldo_coa'     => $coa,
            ]
        );

        return json_encode($data);
    }
}
