<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use App\Models\PurchaseOrderModel;
use App\Models\SalesOrderModel;
use App\Models\SuratJalanModel;
use App\Models\PenjualanModel;
use App\Models\ReturPenjualanModel;

use App\Models\PenerimaanBarangModel;
use App\Models\InvoicePembelianModel;
use App\Models\ReturPembelianModel;
class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting(); 
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $data_persetujuan_purchase_order = PurchaseOrderModel::where('approve', 'Pending')->count();
        $data_persetujuan_sales_order = SalesOrderModel::where('Persetujuan', false)->count();

        // $data_sales = array(
        //     array(
        //         'name'  => 'Sales Order',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Persetujuan Sales Order',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Surat Jalan',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Invoice Penjualan',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Retur',
        //         'data'  => []
        //     )
        // );

        // $data_purchase = array(
        //     array(
        //         'name'  => 'Purchase Order',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Persetujuan Purchase Order',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Penerimaan Barang',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Invoice Pembelian',
        //         'data'  => []
        //     ),
        //     array(
        //         'name'  => 'Retur Pembelian',
        //         'data'  => []
        //     )
        // );

        // $bulan = $this->_bulan();
        // $tahun = date('Y');

        // foreach ($bulan as $key => $value) {
        //     $sales_order = SalesOrderModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Persetujuan', true)
        //                                     ->where('Batal', '=', 0)
        //                                     ->count();
            
        //     $persetujuan_sales_order = SalesOrderModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Persetujuan', false)
        //                                     ->where('Batal', '=', 0)
        //                                     ->count();

        //     $surat_jalan = SuratJalanModel::whereYear('tanggal', '=', $tahun)
        //                                     ->whereMonth('tanggal', '=', $key)
        //                                     ->where('Batal', '=', 0)
        //                                     ->count();

        //     $penjualan = PenjualanModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Batal', '=', 0)
        //                                     ->count();

        //     $retur_penjualan = ReturPenjualanModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Batal', '=', 0)
        //                                     ->count();

        //     array_push($data_sales[0]['data'], $sales_order);
        //     array_push($data_sales[1]['data'], $persetujuan_sales_order);
        //     array_push($data_sales[2]['data'], $surat_jalan);
        //     array_push($data_sales[3]['data'], $penjualan);
        //     array_push($data_sales[4]['data'], $retur_penjualan);

        //     $purchase_order = PurchaseOrderModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('approve', '=', 'Disetujui')
        //                                     ->where('Batal', '=', 'aktif')
        //                                     ->count();
            
        //     $persetujuan_purchase_order = PurchaseOrderModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('approve', '=', 'Pending')
        //                                     ->where('Batal', '=', 'aktif')
        //                                     ->count();

        //     $penerimaan_barang = PenerimaanBarangModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Batal', '=', 'aktif')
        //                                     ->count();

        //     $pembelian = InvoicePembelianModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Batal', '=', 'aktif')
        //                                     ->count();

        //     $retur_pembelian = ReturPembelianModel::whereYear('Tanggal', '=', $tahun)
        //                                     ->whereMonth('Tanggal', '=', $key)
        //                                     ->where('Batal', '=', 'aktif')
        //                                     ->count();

        //     array_push($data_purchase[0]['data'], $purchase_order);
        //     array_push($data_purchase[1]['data'], $persetujuan_purchase_order);
        //     array_push($data_purchase[2]['data'], $penerimaan_barang);
        //     array_push($data_purchase[3]['data'], $pembelian);
        //     array_push($data_purchase[4]['data'], $retur_pembelian);
        // }

        return view('home', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'data_persetujuan_purchase_order', 'data_persetujuan_sales_order'));
    }

    public function get_pendapatan(Request $request) {
        $d = cal_days_in_month(CAL_GREGORIAN, $request->bulan, $request->tahun);

        $data_purchase = array(
            array(
                'name'  => 'Income',
                'data'  => []
            )
        );

        $data_date = [];

        for ($i=1; $i <= $d ; $i++) { 
            $penjualan = PenjualanModel::whereYear('Tanggal', '=', $request->tahun)
                                            ->whereMonth('Tanggal', '=', $request->bulan)
                                            ->whereDay('Tanggal', '=', $i)
                                            ->where('Batal', '=', 0)
                                            ->select(DB::raw('SUM("Grand_total") as total'))
                                            ->first();

            array_push($data_purchase[0]['data'], $penjualan ? (int) $penjualan->total : 0);
            array_push($data_date, $i);
        }

        $response = array(
            'categories'    => $data_date,
            'series'        => $data_purchase
        );

        return json_encode($response);
    }

    public function get_sales(Request $request) {
        $data_sales = array(
            array(
                'name'  => 'Sales Order',
                'data'  => []
            ),
            array(
                'name'  => 'Persetujuan Sales Order',
                'data'  => []
            ),
            array(
                'name'  => 'Surat Jalan',
                'data'  => []
            ),
            array(
                'name'  => 'Invoice Penjualan',
                'data'  => []
            ),
            array(
                'name'  => 'Retur',
                'data'  => []
            )
        );

        $bulan = $request->bulan;
        $tahun = $request->tahun;
        
        $sales_order = SalesOrderModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Persetujuan', true)
                                        ->where('Batal', '=', 0)
                                        ->count();
        
        $persetujuan_sales_order = SalesOrderModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Persetujuan', false)
                                        ->where('Batal', '=', 0)
                                        ->count();

        $surat_jalan = SuratJalanModel::whereYear('tanggal', '=', $tahun)
                                        ->whereMonth('tanggal', '=', $bulan)
                                        ->where('Batal', '=', 0)
                                        ->count();

        $penjualan = PenjualanModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Batal', '=', 0)
                                        ->count();

        $retur_penjualan = ReturPenjualanModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Batal', '=', 0)
                                        ->count();

        array_push($data_sales[0]['data'], $sales_order);
        array_push($data_sales[1]['data'], $persetujuan_sales_order);
        array_push($data_sales[2]['data'], $surat_jalan);
        array_push($data_sales[3]['data'], $penjualan);
        array_push($data_sales[4]['data'], $retur_penjualan);

        $response = array(
            'categories'    => array(date('F', mktime(0, 0, 0, $bulan, 10))),
            'series'        => $data_sales
        );

        return json_encode($response);
    }

    public function get_purchase(Request $request) {
        $data_purchase = array(
            array(
                'name'  => 'Purchase Order',
                'data'  => []
            ),
            array(
                'name'  => 'Persetujuan Purchase Order',
                'data'  => []
            ),
            array(
                'name'  => 'Penerimaan Barang',
                'data'  => []
            ),
            array(
                'name'  => 'Invoice Pembelian',
                'data'  => []
            ),
            array(
                'name'  => 'Retur Pembelian',
                'data'  => []
            )
        );

        $bulan = $request->bulan;
        $tahun = $request->tahun;

        $purchase_order = PurchaseOrderModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('approve', '=', 'Disetujui')
                                        ->where('Batal', '=', 'aktif')
                                        ->count();
        
        $persetujuan_purchase_order = PurchaseOrderModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('approve', '=', 'Pending')
                                        ->where('Batal', '=', 'aktif')
                                        ->count();

        $penerimaan_barang = PenerimaanBarangModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Batal', '=', 'aktif')
                                        ->count();

        $pembelian = InvoicePembelianModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Batal', '=', 'aktif')
                                        ->count();

        $retur_pembelian = ReturPembelianModel::whereYear('Tanggal', '=', $tahun)
                                        ->whereMonth('Tanggal', '=', $bulan)
                                        ->where('Batal', '=', 'aktif')
                                        ->count();

        array_push($data_purchase[0]['data'], $purchase_order);
        array_push($data_purchase[1]['data'], $persetujuan_purchase_order);
        array_push($data_purchase[2]['data'], $penerimaan_barang);
        array_push($data_purchase[3]['data'], $pembelian);
        array_push($data_purchase[4]['data'], $retur_pembelian);

        $response = array(
            'categories'    => array(date('F', mktime(0, 0, 0, $bulan, 10))),
            'series'        => $data_purchase
        );

        return json_encode($response);
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    protected function guard() {
        return Auth::guard();
    }

    private function _bulan() {
        $month_list = array(
            '01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', 
            '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember',
        );

        return $month_list;
    }
}
