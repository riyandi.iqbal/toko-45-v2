<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;
use Illuminate\Support\Facades\Input;

// HELPERS //
use App\Helpers\AppHelper;

use App\Models\MutasiGiroModel;
use App\Models\MutasiGiroDetailModel;
use App\Models\VListPerusahaanModel;
use App\Models\CoaModel;
use App\Models\GiroModel;
use App\Models\VListMutasiGiroDetailModel;
use App\Models\SettingCOAModel;
use App\Models\JurnalModel;

class MutasiGiroController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('mutasigiro.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = MutasiGiroModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('mutasigiro.laporan', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'coreset'));
    }

    public function datatable_detail(Request $request) {
        $data = VListMutasiGiroDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $perusahaan         = VListPerusahaanModel::get();

        // $bank       = CoaModel::where('Kode_COA', 'like', '10103%')->where('Status', 'anak')->leftJoin('tbl_bank', )->get();

        $bank = DB::table('tbl_bank')                
        ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_bank.IDCoa')
        ->select('tbl_bank.*', 'tbl_coa.Nama_COA')
        ->orderBy('tbl_bank.Atas_Nama', 'asc')
        ->get();

        return view('mutasigiro.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'perusahaan', 'bank'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Nomor'             => 'required',
            'Keterangan'        => 'required',
            'data_detail.Jenis_giro.*'      => 'required',
            'data_detail.IDPerusahaan.*'    => 'required',
            'data_detail.IDGiro.*'          => 'required',
            'data_detail.Tanggal_giro.*'    => 'required',
            'data_detail.Nomor_faktur.*'    => 'required',
            'data_detail.Tanggal_faktur.*'  => 'required',
            'data_detail.Status.*'          => 'required',
            'data_detail.IDBank.*'          => 'required',
            'data_detail.Nomor_rekening.*'  => 'required',
            'data_detail.Nominal_giro.*'    => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'Tanggal'           => 'Tanggal',
            'Keterangan'        => 'Keterangan',            
            'data_detail.Jenis_giro.*'      => 'Jenis Giro',
            'data_detail.IDPerusahaan.*'    => 'Nama Perusahaan',
            'data_detail.IDGiro.*'          => 'Giro',
            'data_detail.Tanggal_giro.*'    => 'Tanggal giro',
            'data_detail.Nomor_faktur.*'    => 'Nomor faktur',
            'data_detail.Tanggal_faktur.*'  => 'Tanggal faktur',
            'data_detail.Status.*'          => 'Status',
            'data_detail.IDBank.*'          => 'Bank',
            'data_detail.Nomor_rekening.*'  => 'Rekening',
            'data_detail.Nominal_giro.*'    => 'Nominal Giro',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDMG = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor);

        DB::beginTransaction();

        $MutasiGiro = new MutasiGiroModel;

        $MutasiGiro->IDMG       = $IDMG;
        $MutasiGiro->Tanggal    = AppHelper::DateFormat($request->Tanggal);
        $MutasiGiro->Nomor      = $Nomor;
        $MutasiGiro->Total      = $request->Total;
        $MutasiGiro->Keterangan = $request->Keterangan;
        $MutasiGiro->dibuat_pada    = date('Y-m-d H:i:s');
        $MutasiGiro->dibuat_oleh    = Auth::user()->id;
        $MutasiGiro->diubah_pada    = date('Y-m-d H:i:s');
        $MutasiGiro->diubah_oleh    = Auth::user()->id;

        $MutasiGiro->save();

        $data_detail = json_decode($request->data_detail);
        foreach ($data_detail as $key => $value) {

            $Giro = GiroModel::findOrfail($value->IDGiro);

            $Giro->Tanggal_cair = AppHelper::DateFormat($request->Tanggal);
            $Giro->Status_giro  = $value->Status;
            $Giro->Jenis_giro   = $value->Jenis_giro;
            if ($value->Nomor_baru) {
                $Giro->Nomor_giro   = $value->Nomor_baru;
            }

            $Giro->IDBank               = $value->IDBank;
            $Giro->Nomor_rekening       = $value->Nomor_rekening;

            $Giro->save();

            $IDMGDetail = uniqid();
            $MutasiGiroDetail = new MutasiGiroDetailModel();

            $MutasiGiroDetail->IDMGDetail   = uniqid();
            $MutasiGiroDetail->IDMG         = $IDMG;
            $MutasiGiroDetail->Jenis_giro   = $value->Jenis_giro;
            $MutasiGiroDetail->IDGiro       = $value->IDGiro;
            $MutasiGiroDetail->Status       = $value->Status;
            $MutasiGiroDetail->Nomor_lama   = $value->Nomor_giro;
            $MutasiGiroDetail->Nomor_baru   = $value->Nomor_baru;

            $MutasiGiroDetail->save();

            // SAVE JURNAL DEBET //
            if ($value->Jenis_giro == 'Keluar'){
            
            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '117')
                                        ->where('Posisi', '=', 'debet')
                                        ->where('Tingkat', '=', 'utama')
                                        ->where('Cara', '=', 'keluar')
                                        ->first();

            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal     = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor        = $Nomor;
            $jurnal_pembelian_debet->IDFaktur     = $IDMG;
            $jurnal_pembelian_debet->IDFakturDetail   = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'MG';
            $jurnal_pembelian_debet->IDCOA        = $CoaDebet->IDCoa;
            $jurnal_pembelian_debet->Debet        = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_debet->Kredit       = 0;
            $jurnal_pembelian_debet->IDMataUang       = 0;
            $jurnal_pembelian_debet->Kurs             = 0;
            $jurnal_pembelian_debet->Total_debet      = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_debet->Total_kredit     = 0;
            $jurnal_pembelian_debet->Keterangan       = 'Mutasi Giro Keluar ' . $Nomor;
            $jurnal_pembelian_debet->Saldo            = AppHelper::StrReplace($request->Total);

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '117')
                                        ->where('Posisi', '=', 'kredit')
                                        ->where('Tingkat', '=', 'utama')
                                        ->where('Cara', '=', 'keluar')
                                        ->first();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal     = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor        = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur     = $IDMG;
            $jurnal_pembelian_kredit->IDFakturDetail   = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'MG';
            $jurnal_pembelian_kredit->IDCOA        = $CoaKredit->IDCoa;
            $jurnal_pembelian_kredit->Debet        = 0;
            $jurnal_pembelian_kredit->Kredit       = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_kredit->IDMataUang       = 0;
            $jurnal_pembelian_kredit->Kurs             = 0;
            $jurnal_pembelian_kredit->Total_debet      = 0;
            $jurnal_pembelian_kredit->Total_kredit     = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_kredit->Keterangan       = 'Mutasi Giro Keluar ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo            = AppHelper::StrReplace($request->Total);

            $jurnal_pembelian_kredit->save();
            }else{
                if ($value->Jenis_giro == 'Masuk'){
                // SAVE JURNAL DEBET //

                $CoaDebet = SettingCOAModel::where('IDMenu', '=', '117')
                                        ->where('Posisi', '=', 'debet')
                                        ->where('Tingkat', '=', 'utama')
                                        ->where('Cara', '=', 'masuk')
                                        ->first();

            $jurnal_pembelian_debet = new JurnalModel;

            $jurnal_pembelian_debet->IDJurnal     = $this->number_jurnal();
            $jurnal_pembelian_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_debet->Nomor        = $Nomor;
            $jurnal_pembelian_debet->IDFaktur     = $IDMG;
            $jurnal_pembelian_debet->IDFakturDetail   = '-';
            $jurnal_pembelian_debet->Jenis_faktur = 'MG';
            $jurnal_pembelian_debet->IDCOA        = $CoaDebet->IDCoa;
            $jurnal_pembelian_debet->Debet        = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_debet->Kredit       = 0;
            $jurnal_pembelian_debet->IDMataUang       = 0;
            $jurnal_pembelian_debet->Kurs             = 0;
            $jurnal_pembelian_debet->Total_debet      = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_debet->Total_kredit     = 0;
            $jurnal_pembelian_debet->Keterangan       = 'Mutasi Giro Masuk ' . $Nomor;
            $jurnal_pembelian_debet->Saldo            = AppHelper::StrReplace($request->Total);

            $jurnal_pembelian_debet->save();

            // SAVE JURNAL KREDIT //
            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '117')
                                        ->where('Posisi', '=', 'kredit')
                                        ->where('Tingkat', '=', 'utama')
                                        ->where('Cara', '=', 'masuk')
                                        ->first();

            $jurnal_pembelian_kredit = new JurnalModel;

            $jurnal_pembelian_kredit->IDJurnal     = $this->number_jurnal();
            $jurnal_pembelian_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_pembelian_kredit->Nomor        = $Nomor;
            $jurnal_pembelian_kredit->IDFaktur     = $IDMG;
            $jurnal_pembelian_kredit->IDFakturDetail   = '-';
            $jurnal_pembelian_kredit->Jenis_faktur = 'MG';
            $jurnal_pembelian_kredit->IDCOA        = $CoaKredit->IDCoa;
            $jurnal_pembelian_kredit->Debet        = 0;
            $jurnal_pembelian_kredit->Kredit       = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_kredit->IDMataUang       = 0;
            $jurnal_pembelian_kredit->Kurs             = 0;
            $jurnal_pembelian_kredit->Total_debet      = 0;
            $jurnal_pembelian_kredit->Total_kredit     = AppHelper::StrReplace($request->Total);
            $jurnal_pembelian_kredit->Keterangan       = 'Mutasi Giro Masuk ' . $Nomor;
            $jurnal_pembelian_kredit->Saldo            = AppHelper::StrReplace($request->Total);

            $jurnal_pembelian_kredit->save();
                }
            }
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $MutasiGiro = MutasiGiroModel::findOrfail($id);

        $MutasiGiroDetail = VListMutasiGiroDetailModel::where('IDMG', $MutasiGiro->IDMG)->get();

        return view('mutasigiro.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'MutasiGiro', 'MutasiGiroDetail'));
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $perusahaan = VListPerusahaanModel::get();

        $bank       = CoaModel::where('Kode_COA', 'like', '10103%')->where('Status', 'anak')->get();

        $MutasiGiro = MutasiGiroModel::findOrfail($id);

        $MutasiGiroDetail = VListMutasiGiroDetailModel::where('IDMG', $MutasiGiro->IDMG)->get();

        return view('mutasigiro.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'MutasiGiro', 'MutasiGiroDetail', 'perusahaan', 'bank'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Nomor'             => 'required',
            'Keterangan'        => 'required',
            'data_detail.Jenis_giro.*'      => 'required',
            'data_detail.IDPerusahaan.*'    => 'required',
            'data_detail.IDGiro.*'          => 'required',
            'data_detail.Tanggal_giro.*'    => 'required',
            'data_detail.Nomor_faktur.*'    => 'required',
            'data_detail.Tanggal_faktur.*'  => 'required',
            'data_detail.Status.*'          => 'required',
            'data_detail.IDBank.*'          => 'required',
            'data_detail.Nomor_rekening.*'  => 'required',
            'data_detail.Nominal_giro.*'    => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'Tanggal'           => 'Tanggal',
            'Keterangan'        => 'Keterangan',            
            'data_detail.Jenis_giro.*'      => 'Jenis Giro',
            'data_detail.IDPerusahaan.*'    => 'Nama Perusahaan',
            'data_detail.IDGiro.*'          => 'Giro',
            'data_detail.Tanggal_giro.*'    => 'Tanggal giro',
            'data_detail.Nomor_faktur.*'    => 'Nomor faktur',
            'data_detail.Tanggal_faktur.*'  => 'Tanggal faktur',
            'data_detail.Status.*'          => 'Status',
            'data_detail.IDBank.*'          => 'Bank',
            'data_detail.Nomor_rekening.*'  => 'Rekening',
            'data_detail.Nominal_giro.*'    => 'Nominal Giro',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDMG = $request->IDMG;
        $Nomor = $request->Nomor;

        DB::beginTransaction();

        $MutasiGiro = MutasiGiroModel::findOrfail($IDMG);

        $MutasiGiro->Tanggal    = AppHelper::DateFormat($request->Tanggal);
        $MutasiGiro->Nomor      = $Nomor;
        $MutasiGiro->Total      = $request->Total;
        $MutasiGiro->Keterangan = $request->Keterangan;
        $MutasiGiro->diubah_pada    = date('Y-m-d H:i:s');
        $MutasiGiro->diubah_oleh    = Auth::user()->id;

        $MutasiGiro->save();

        $MutasiGiroDetailDelete = MutasiGiroDetailModel::where('IDMG', $IDMG)->delete();

        $data_detail = json_decode($request->data_detail);
        foreach ($data_detail as $key => $value) {

            $Giro = GiroModel::findOrfail($value->IDGiro);

            $Giro->Tanggal_cair = AppHelper::DateFormat($request->Tanggal);
            $Giro->Status_giro  = $value->Status;
            $Giro->Jenis_giro   = $value->Jenis_giro;
            if ($value->Nomor_baru) {
                $Giro->Nomor_giro   = $value->Nomor_baru;
            }

            $Giro->IDBank               = $value->IDBank;
            $Giro->Nomor_rekening       = $value->Nomor_rekening;

            $Giro->save();

            $IDMGDetail = uniqid();
            $MutasiGiroDetail = new MutasiGiroDetailModel();

            $MutasiGiroDetail->IDMGDetail   = uniqid();
            $MutasiGiroDetail->IDMG         = $IDMG;
            $MutasiGiroDetail->Jenis_giro   = $value->Jenis_giro;
            $MutasiGiroDetail->IDGiro       = $value->IDGiro;
            $MutasiGiroDetail->Status       = $value->Status;
            $MutasiGiroDetail->Nomor_lama   = $value->Nomor_giro;
            $MutasiGiroDetail->Nomor_baru   = $value->Nomor_baru;

            $MutasiGiroDetail->save();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function destroy($id) {
        DB::beginTransaction();

        $MutasiGiro = MutasiGiroModel::findOrfail($id);

        if ($MutasiGiro->Batal == 1) {
            $status = false;
            $message = 'Data tidak bisa diaktifkan.';
        } else {
            $MutasiGiro->Batal = 1;

            $MutasiGiro->save();
            $message = 'Data berhasil dibatalkan.';
        }

        DB::commit();

        $data = array (
            'status'    => (isset($status)) ? $status : true ,
            'message'   => $message
        );

        return json_encode($data);
    }

    public function number(Request $request) {
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = MutasiGiroModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');
        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/MG/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }        
    }

    public function get_nomor_giro() {
        $queryBuilder = GiroModel::select('*');

        if (Input::get('q')) {
            $queryBuilder->where('Nomor_giro', 'iLike', '%' . Input::get('q') . '%' );
        }

        $queryBuilder->where('IDPerusahaan', Input::get('IDPerusahaan'));
        
        // if (Input::get('Kategori') == 'Customer') {
        //     $queryBuilder->whereIn('Jenis_faktur', ['SO', 'FJ', 'PP']);
        // } else {
        //     $queryBuilder->whereNotIn('Jenis_faktur', ['SO', 'FJ', 'PP']);
        // }

        $response = $queryBuilder->get();

        return json_encode($response);
    }    

    public function get_perusahaan() {
        $queryBuilder = VListPerusahaanModel::select('*');

        if (Input::get('q')) {
            $queryBuilder->where('Nama', 'iLike', '%' . Input::get('q') . '%' );
        }
        
        if (Input::get('Jenis_giro') == 'Masuk') {
            $queryBuilder->where('Kategori', '=', 'Customer');
        } else {
            $queryBuilder->where('Kategori', '!=', 'Customer');
        }

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_mutasi_giro_detail(Request $request) {
        $MutasiGiroDetail = VListMutasiGiroDetailModel::where('IDMG', $request->IDMG)->get();

        return json_encode($MutasiGiroDetail);
    }

    private function _cek_nomor($nomor) {
        $nomor_exist = MutasiGiroModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number();
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number(){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = MutasiGiroModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');
        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/MG/' . $bulan_romawi . '/' . date('y');
        
        return $kode;
    }

    public function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }
}
