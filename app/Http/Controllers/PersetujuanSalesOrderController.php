<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

use App\Helpers\AppHelper;
use App\Models\SalesOrderModel;
use App\Models\CustomerModel;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\SalesOrderDetailModel;
use App\Models\UMCustomerModel;
use App\Models\VListSalesOrderDetailModel;
use App\Models\HargaJualModel;
use App\Models\JurnalModel;
use App\Models\PerusahaanModel;

class PersetujuanSalesOrderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('salesorder.persetujuan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = SalesOrderModel::select('tbl_sales_order.*', 'tbl_customer.Nama as Nama', 'tbl_mata_uang.Kode', 'tbl_surat_jalan_customer.idsjc');
        
        $data->leftJoin('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_sales_order.IDMataUang');
        $data->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_sales_order.IDCustomer');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_sales_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_sales_order.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_sales_order.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->where('tbl_customer.IDGroupCustomer', '=', 'P000003');

        $data->leftJoin('tbl_surat_jalan_customer', function($join){
            $join->on('tbl_surat_jalan_customer.idso', '=', 'tbl_sales_order.IDSOK')
            ->where('tbl_surat_jalan_customer.Batal', '=', '0'); 
        });

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $sales_order = SalesOrderModel::findOrfail($id);
        $customer = CustomerModel::findOrfail($sales_order->IDCustomer);
        $kurs = MataUangModel::findOrfail($sales_order->IDMataUang);
        $sales_order_detail = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)
                                ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                                ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_sales_order_detail.IDSatuan')
                                ->get();
                                
        $um_customer = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)
                                ->leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_um_customer.IDCoa')
                                ->first();

        return view('salesorder.persetujuan_detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'sales_order', 'customer', 'kurs', 'sales_order_detail', 'um_customer'));
    }

    public function approved(Request $request) {
        DB::beginTransaction();
        $IDSOK = $request->IDSOK;

        $sales_order = SalesOrderModel::findOrfail($IDSOK);

        $sales_order->Total_qty         = $request->Total_qty;
        $sales_order->Saldo_qty         = $request->Total_qty;
        $sales_order->Grand_total       = AppHelper::StrReplace($request->Grand_total);

        if ($sales_order->Status_ppn == 'exclude' || $sales_order->Status_ppn == 'include') {
            $sales_order->PPN       = AppHelper::PpnInclude(AppHelper::StrReplace($request->Grand_total));
        } else {
            $sales_order->PPN       = 0;
        }
        
        $sales_order->DPP       = floatval(AppHelper::StrReplace($request->Grand_total)) - floatval(($sales_order->PPN));

        $sales_order->Persetujuan   = true;

        $sales_order->diubah_pada       = date('Y-m-d H:i:s');

        $sales_order->save();

        foreach ($request->IDSOKDetail as $key => $value) {
            $sales_order_detail = SalesOrderDetailModel::findOrfail($value);

            $sales_order_detail->IDSOK          = $IDSOK;
            $sales_order_detail->Qty            = $request->Qty_disetujui[$key];
            $sales_order_detail->Saldo_qty      = $request->Qty_disetujui[$key];
            $sales_order_detail->IDSatuan       = $request->IDSatuan[$key];
            $sales_order_detail->Harga          = AppHelper::StrReplace($request->Harga[$key]);
            $sales_order_detail->Sub_total      = AppHelper::StrReplace($request->Sub_total[$key]);

            $sales_order_detail->save();

            // UPDATE HARGA JUAL //
            /* $data_harga_jual = HargaJualModel::where('IDBarang', $request->IDBarang)
                                            ->where('IDSatuan', $request->IDSatuan[$key])
                                            ->first();

            $harga_jual = HargaJualModel::find($data_harga_jual->IDHargaJual);
            
            $harga_jual->Harga_Jual = AppHelper::StrReplace($request->Harga[$key]);

            $harga_jual->save(); */
            // END HARGA JUAL
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'IDSOK'     => $IDSOK,
            ]
        );

        return json_encode($data);
    }

    public function rejected($id) {
        DB::beginTransaction();

        $sales_order = SalesOrderModel::findOrfail($id);
        
        $sales_order->Persetujuan = false;

        $sales_order->save();

        $data_um = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->first();

        if ($data_um) {
            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_debet->Tanggal      = date('Y-m-d');
            $jurnal_penjualan_debet->Nomor        = $data_um->Nomor_Faktur;
            $jurnal_penjualan_debet->IDFaktur     = $data_um->IDFaktur;
            $jurnal_penjualan_debet->IDFakturDetail   = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'SO';
            $jurnal_penjualan_debet->IDCOA        = $data_um->IDCoa;
            $jurnal_penjualan_debet->Debet        = $data_um->Nilai_UM;
            $jurnal_penjualan_debet->Kredit       = 0;
            $jurnal_penjualan_debet->IDMataUang       = $sales_order->IDMataUang;
            $jurnal_penjualan_debet->Kurs             = $sales_order->Kurs;
            $jurnal_penjualan_debet->Total_debet      = $data_um->Nilai_UM;
            $jurnal_penjualan_debet->Total_kredit     = 0;
            $jurnal_penjualan_debet->Keterangan       = 'Sales Order No. ' . $data_um->Nomor_Faktur . ' ditolak.';
            $jurnal_penjualan_debet->Saldo            = $data_um->Nilai_UM;

            $jurnal_penjualan_debet->save();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_kredit->Tanggal      = date('Y-m-d');
            $jurnal_penjualan_kredit->Nomor        = $data_um->Nomor_Faktur;
            $jurnal_penjualan_kredit->IDFaktur     = $data_um->IDFaktur;
            $jurnal_penjualan_kredit->IDFakturDetail   = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'SO';
            $jurnal_penjualan_kredit->IDCOA        = $data_um->IDCoa;
            $jurnal_penjualan_kredit->Debet        = 0;
            $jurnal_penjualan_kredit->Kredit       = $data_um->Nilai_UM;
            $jurnal_penjualan_kredit->IDMataUang       = $sales_order->IDMataUang;
            $jurnal_penjualan_kredit->Kurs             = $sales_order->Kurs;
            $jurnal_penjualan_kredit->Total_debet      = 0;
            $jurnal_penjualan_kredit->Total_kredit     = $data_um->Nilai_UM;
            $jurnal_penjualan_kredit->Keterangan       = 'Sales Order No. ' . $data_um->Nomor_Faktur . ' ditolak.';
            $jurnal_penjualan_kredit->Saldo            = $data_um->Nilai_UM;

            $jurnal_penjualan_kredit->save();
        }

        $delete_um = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->update(['Batal' => 1]);

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Sales Order berhasil ditolak.'
        );

        return json_encode($data);
    }
}
