<?php
/*=====Created by Tiar Sagita Rahman @28 Desember 2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

use App\Models\CustomerModel;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\PenjualanModel;
use App\Models\ReturPenjualanModel;
use App\Models\ReturPenjualanDetailModel;
use App\Models\VListPenjualanModel;
use App\Models\VListPenjualanDetailModel;
use App\Models\VListReturPenjualanModel;
use App\Models\VListReturPenjualanDetailModel;
use App\Models\JurnalModel;
use App\Models\PiutangModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\PerusahaanModel;
use App\Models\SettingCOAModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class ReturPenjualanController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('returpenjualan.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer           = CustomerModel::get();

        return view('returpenjualan.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer'));
    }

    public function datatable(Request $request) {
        $data = VListReturPenjualanModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }


    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Nomor'             => 'required',
            'IDCustomer'        => 'required', 
            'Tanggal'           => 'required|date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'Grand_total'       => 'required',
            'Pilihan_retur'     => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'IDCustomer'        => 'Customer',
            'Tanggal'           => 'Tanggal',
            'Total_qty'         => 'Total Qty',
            'Grand_total'       => 'Grand Total',
            'Pilihan_retur'     => 'Pilihan Retur',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDRP = uniqid();

        $Nomor = $this->_cek_nomor($request->Nomor, $request->Status_ppn);
        $customer = CustomerModel::findOrfail($request->IDCustomer);

        DB::beginTransaction();
        $retur_penjualan = new ReturPenjualanModel();

        $retur_penjualan->IDRP              = $IDRP;
        $retur_penjualan->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $retur_penjualan->IDFJ              = $request->IDFJ;
        $retur_penjualan->Nomor             = $Nomor;
        $retur_penjualan->IDCustomer        = $request->IDCustomer;
        $retur_penjualan->Total_qty         = $request->Total_qty;
        $retur_penjualan->Saldo_qty         = $request->Total_qty;
        $retur_penjualan->Discount          = 0;
        $retur_penjualan->Grand_total       = AppHelper::StrReplace($request->Grand_total);
        $retur_penjualan->Status_ppn        = $request->Status_ppn;
        $retur_penjualan->Keterangan        = $request->Keterangan;
        $retur_penjualan->Batal             = 0;
        $retur_penjualan->Pilihan_retur     = $request->Pilihan_retur;

        $retur_penjualan->dibuat_pada       = date('Y-m-d H:i:s');
        $retur_penjualan->diubah_pada       = date('Y-m-d H:i:s');

        $retur_penjualan->save();

        // Pengurangan Piutang //
        if ($request->Pilihan_retur != 'TM') {
            // SAVE PIUTANG PENGURANGAN //
            /* DIJADIKAN SEBAGAI PENGURANGAN PIUTANG KARENA BARANG TIDAK KEMBALI DIKIRIM KE CUSTOMER */

            $piutang = new PiutangModel;

            $piutang->IDPiutang         = uniqid();
            $piutang->IDFaktur          = $IDRP;
            $piutang->IDCustomer        = $request->IDCustomer;
            $piutang->Tanggal_Piutang   = date('Y-m-d');
            $piutang->No_Faktur         = $Nomor;
            $piutang->Nilai_Piutang     = 0;
            $piutang->Saldo_Awal        = AppHelper::StrReplace($request->Grand_total) * -1;
            $piutang->UM                = 0;
            $piutang->Pembayaran        = 0;
            $piutang->Saldo_Akhir       = AppHelper::StrReplace($request->Grand_total) * -1;
            $piutang->Jenis_Faktur      = 'RETUR';
            $piutang->Retur             = 0;

            $piutang->save();
            // END SAVE PIUTANG //
        }

        foreach ($request->IDBarang as $key => $value) {
            if ($request->Qty[$key]) {
                $data_barang = BarangModel::find($value);
    
                $IDRPDetail = uniqid();
                $retur_penjualan_detail = new ReturPenjualanDetailModel;
    
                $retur_penjualan_detail->IDRPDetail     = $IDRPDetail;
                $retur_penjualan_detail->IDRP           = $IDRP;
                $retur_penjualan_detail->IDBarang       = $value;
                $retur_penjualan_detail->Qty            = $request->Qty[$key];
                $retur_penjualan_detail->Saldo_qty      = $request->Qty[$key];
                $retur_penjualan_detail->IDSatuan       = $request->IDSatuan[$key];
                $retur_penjualan_detail->Harga          = AppHelper::StrReplace($request->Harga[$key]);
                $retur_penjualan_detail->Sub_total      = AppHelper::StrReplace($request->Sub_total[$key]);
    
                $retur_penjualan_detail->save();

                // KARTU STOK //
                $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])->where('IDGudang', $request->IDGudang)->first();
    
                if ($data_stok) {
                    $stok = StokModel::find($data_stok->IDStok);
        
                    $stok->Qty_pcs = $data_stok->Qty_pcs + $request->Qty[$key];
    
                    $stok->save();
                } else {
                    $IDStok = uniqid();
                    $stok = new StokModel;

                    $stok->IDStok = $IDStok;
                    $stok->Tanggal  = date('Y-m-d');
                    $stok->Nomor_faktur     = $Nomor;
                    $stok->Jenis_faktur     = 'Retur';
                    $stok->IDBarang         = $value;
                    $stok->IDGudang         = $request->IDGudang;
                    $stok->IDSatuan         = $data_barang->IDSatuan;
                    $stok->Qty_pcs          = $request->Qty[$key];

                    $stok->save();
                }

                // KARTU STOK //
                $kartu_stok     = new KartustokModel;
    
                $kartu_stok->IDKartuStok        = uniqid();
                $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
                $kartu_stok->Nomor_faktur       = $Nomor;
                $kartu_stok->IDFaktur           = $IDRP;
                $kartu_stok->IDFakturDetail     = $IDRPDetail;
                $kartu_stok->IDStok             = $data_stok ? $data_stok->IDStok : $IDStok;
                $kartu_stok->Jenis_faktur       = 'RJ';
                $kartu_stok->IDBarang           = $value;
                $kartu_stok->Harga              = AppHelper::StrReplace($request->Harga[$key]);;
                $kartu_stok->Nama_Barang        = $data_barang->Nama_Barang;
                $kartu_stok->Masuk_pcs          = $request->Qty[$key];
                $kartu_stok->IDSatuan           = $request->IDSatuan[$key];
    
                $kartu_stok->save();
                // END KARTU STOK //
            }
        }

        // SAVE JURNAL DEBET //
        $CoaDebet = SettingCOAModel::where('IDMenu', '=', '85')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->where('cara', '=', 'lunas')
                            ->first();

        $jurnal_penjualan_debet = new JurnalModel;

        $jurnal_penjualan_debet->IDJurnal     = $this->number_jurnal();
        $jurnal_penjualan_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penjualan_debet->Nomor        = $Nomor;
        $jurnal_penjualan_debet->IDFaktur     = $IDRP;
        $jurnal_penjualan_debet->IDFakturDetail   = '-';
        $jurnal_penjualan_debet->Jenis_faktur = 'RJ';
        $jurnal_penjualan_debet->IDCOA        = $CoaDebet->IDCoa;
        $jurnal_penjualan_debet->Debet        = AppHelper::StrReplace($request->Grand_total);
        $jurnal_penjualan_debet->Kredit       = 0;
        $jurnal_penjualan_debet->IDMataUang       = 1;
        $jurnal_penjualan_debet->Kurs             = 1;
        $jurnal_penjualan_debet->Total_debet      = AppHelper::StrReplace($request->Grand_total);
        $jurnal_penjualan_debet->Total_kredit     = 0;
        $jurnal_penjualan_debet->Keterangan       = 'Retur Penjualan ' . $Nomor . ' a.n. ' . $customer->Nama;
        $jurnal_penjualan_debet->Saldo            = AppHelper::StrReplace($request->Grand_total);

        $jurnal_penjualan_debet->save();

        // SAVE JURNAL KREDIT //

        $CoaKredit = SettingCOAModel::where('IDMenu', '=', '85')
                            ->where('Posisi', '=', 'kredit')
                            ->where('Tingkat', '=', 'utama')
                            ->where('cara', '=', 'lunas')
                            ->first();

        $jurnal_penjualan_kredit = new JurnalModel;

        $jurnal_penjualan_kredit->IDJurnal     = $this->number_jurnal();
        $jurnal_penjualan_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penjualan_kredit->Nomor        = $Nomor;
        $jurnal_penjualan_kredit->IDFaktur     = $IDRP;
        $jurnal_penjualan_kredit->IDFakturDetail   = '-';
        $jurnal_penjualan_kredit->Jenis_faktur = 'RJ';
        $jurnal_penjualan_kredit->IDCOA        = $CoaKredit->IDCoa;
        $jurnal_penjualan_kredit->Debet        = 0;
        $jurnal_penjualan_kredit->Kredit       = AppHelper::StrReplace($request->Grand_total);
        $jurnal_penjualan_kredit->IDMataUang       = 1;
        $jurnal_penjualan_kredit->Kurs             = 1;
        $jurnal_penjualan_kredit->Total_debet      = 0;
        $jurnal_penjualan_kredit->Total_kredit     = AppHelper::StrReplace($request->Grand_total);
        $jurnal_penjualan_kredit->Keterangan       = 'Retur Penjualan ' . $Nomor . ' a.n. ' . $customer->Nama;
        $jurnal_penjualan_kredit->Saldo            = AppHelper::StrReplace($request->Grand_total);

        $jurnal_penjualan_kredit->save();

        if ($request->PPN > 0) {
            // SAVE JURNAL KREDIT //
            $CoaDebetPPN = SettingCOAModel::where('IDMenu', '=', '85')
                                ->where('Posisi', '=', 'debet')
                                ->where('Tingkat', '=', 'tambahan')
                                ->where('cara', '=', 'lunas')
                                ->first();
    
            $jurnal_penjualan_debet_ppn = new JurnalModel;
      
            $jurnal_penjualan_debet_ppn->IDJurnal     = $this->number_jurnal();
            $jurnal_penjualan_debet_ppn->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet_ppn->Nomor        = $Nomor;
            $jurnal_penjualan_debet_ppn->IDFaktur     = $IDRP;
            $jurnal_penjualan_debet_ppn->IDFakturDetail   = '-';
            $jurnal_penjualan_debet_ppn->Jenis_faktur = 'RJ';
            $jurnal_penjualan_debet_ppn->IDCOA        = $CoaDebetPPN->IDCoa;
            $jurnal_penjualan_debet_ppn->Debet        = AppHelper::StrReplace($request->Grand_total);
            $jurnal_penjualan_debet_ppn->Kredit       = 0;
            $jurnal_penjualan_debet_ppn->IDMataUang       = 1;
            $jurnal_penjualan_debet_ppn->Kurs             = 1;
            $jurnal_penjualan_debet_ppn->Total_debet      = AppHelper::StrReplace($request->Grand_total);
            $jurnal_penjualan_debet_ppn->Total_kredit     = 0;
            $jurnal_penjualan_debet_ppn->Keterangan       = 'PPN Retur Penjualan ' . $Nomor;
            $jurnal_penjualan_debet_ppn->Saldo            = AppHelper::StrReplace($request->Grand_total);
      
            $jurnal_penjualan_debet_ppn->save();
          }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penjualan = VListReturPenjualanModel::findOrfail($id);

        $penjualan_detail = VListReturPenjualanDetailModel::where('IDRP', $penjualan->IDRP)->get();

        return view('returpenjualan.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penjualan', 'penjualan_detail'));
    }

    public function print($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penjualan = VListReturPenjualanModel::findOrfail($id);

        $penjualan_detail = VListReturPenjualanDetailModel::where('IDRP', $penjualan->IDRP)->get();
        $perusahaan          = PerusahaanModel::Getforprint();
        return view('returpenjualan.print', compact('coreset','aksesmenu', 'perusahaan','aksesmenudetail', 'aksessetting', 'namauser', 'penjualan', 'penjualan_detail'));
    }

    public function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    public function get_penjualan(Request $request) {
        $penjualan = VListPenjualanModel::where('IDFJ', $request->id)->first();
        
        return json_encode($penjualan);
    }

    public function get_penjualan_customer() {
        DB::enableQueryLog();

        $penjualan = VListPenjualanModel::select('*');

        $penjualan->whereNotIn('IDFJ', function($q) {
            $q->select('IDFJ')->where('Batal', 0)->whereNotNull('IDFJ')->from('tbl_retur_penjualan')->get();
        });

        if (Input::get('q')) {
            $penjualan->where('Nomor', 'iLike', '%' . Input::get('q') . '%' );
        }

        $penjualan->where('IDCustomer', Input::get('IDCustomer'));

        $response = $penjualan->get();
        
        return json_encode($response);
    }

    public function get_penjualan_detail(Request $request) {
        $penjualan_detail = VListPenjualanDetailModel::where('IDFJ', $request->get('IDFJ'))->get();
        
        return Datatables::of($penjualan_detail)->make(true);
    }

    public function number_invoice(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = ReturPenjualanModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');

        if ($request->Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } else if ($request->Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        } else {
            $Status_ppn_kode = 0;
        }
        $queryBuilder->where('Status_ppn', $request->Status_ppn);
        
        $queryBuilder->where(DB::raw('substring("Nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/RJ/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    private function _cek_nomor($nomor, $Status_ppn) {
        $nomor_exist = ReturPenjualanModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number($Status_ppn);
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number($Status_ppn){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = ReturPenjualanModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');
        
        if ($Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } else if ($Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        } else {
            $Status_ppn_kode = 0;
        }
        $queryBuilder->where('Status_ppn', $Status_ppn);

        $queryBuilder->where(DB::raw('substring("Nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/RJ/' . $bulan_romawi . '/' . date('y');

        return $kode;
    
    }
}