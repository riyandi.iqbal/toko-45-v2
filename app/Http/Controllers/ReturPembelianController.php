<?php
/*=====Create DEDY @24/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use App\Models\PerusahaanModel;
use App\Models\MataUangModel;
use App\Models\InvoicePembelianModel;
use App\Models\InvoicePembelianDetailModel;
use App\Models\ReturPembelianModel;
use App\Models\ReturPembelianDetailModel;
use App\Models\PenerimaanBarangModel;
use App\Models\ReturPembelianGrandTotalModel;
use App\Models\JurnalModel;
use App\Models\HutangModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\BarangModel;
use App\Models\SatuanKonversiModel;
use App\Models\SettingCOAModel;

use Datatables;

// HELPERS //
use App\Helpers\AppHelper;

class ReturPembelianController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $retur              = DB::table('view_returpembelian')->get();

        return view('retur_beli/index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'retur'));
    }

    public function datatable(Request $request) {
        $data = DB::table('view_returpembelian')->select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
      $iduser             = Auth::user()->id;
      $namauser           = Auth::user()->name;
      $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
      $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
      $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
      $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

      $data_pembelian     = InvoicePembelianModel::GetIndex()->where('tbl_pembelian.Batal', '=', 'aktif')->where('is_paid', 'f')->where('tbl_pembelian.Saldo_yard', '>', 0)->get();

      return view('retur_beli.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'data_pembelian'));
    }

    public function store(Request $request) {
      $validate = Validator::make($request->all(), [
        'Nomor'             => 'required',
        'IDSupplier'        => 'required', 
        'Tanggal'           => 'required|date_format:"d/m/Y"',
        'Total_qty'         => 'required|numeric',
        'Grand_total'       => 'required',
      ])->setAttributeNames([
          'Nomor'             => 'Nomor',
          'IDSupplier'        => 'Supplier',
          'Tanggal'           => 'Tanggal',
          'Total_qty'         => 'Total Qty',
          'Grand_total'       => 'Grand Total',
      ]);
      
      if ($validate->fails()) {
          $data = [
              'status'    => false,
              'message'   => strip_tags($validate->errors()->first())
          ];
          return json_encode($data);
      }

      $IDRB = uniqid();

      $Nomor = $request->Nomor;

      DB::beginTransaction();
      $pembelian = InvoicePembelianModel::findOrfail($request->IDFB);
      $tbs      = PenerimaanBarangModel::findOrfail($pembelian->IDTBS);

      $retur_pembelian = new ReturPembelianModel();

      $retur_pembelian->IDRB              = $IDRB;
      $retur_pembelian->Tanggal           = AppHelper::DateFormat($request->Tanggal);
      $retur_pembelian->IDFB              = $request->IDFB;
      $retur_pembelian->Nomor             = $Nomor;
      $retur_pembelian->Tanggal_fb        = AppHelper::DateFormat($request->Tanggal_beli);
      $retur_pembelian->IDSupplier        = $request->IDSupplier;
      $retur_pembelian->Total_qty_yard    = $request->Total_qty;
      $retur_pembelian->Total_qty_meter   = $request->Total_qty;
      $retur_pembelian->Saldo_yard        = $request->Total_qty;
      $retur_pembelian->Saldo_meter       = $request->Total_qty;
      $retur_pembelian->Discount          = 0;
      $retur_pembelian->Status_ppn        = $request->Status_ppn;
      $retur_pembelian->Keterangan        = $request->Keterangan;
      $retur_pembelian->Batal             = 'aktif';

      $retur_pembelian->save();

      $data_pembelian_update = InvoicePembelianModel::findOrfail($request->IDFB);

      $data_pembelian_update->Saldo_yard = $data_pembelian_update->Saldo_yard - $request->Total_qty;

      $data_pembelian_update->save();

      $retur_pembelian_grand_total = new ReturPembelianGrandTotalModel;

      $retur_pembelian_grand_total->IDRBGrandTotal = uniqid();
      $retur_pembelian_grand_total->IDRB           = $IDRB;
      $retur_pembelian_grand_total->IDMataUang     = $pembelian->IDMataUang;
      $retur_pembelian_grand_total->Kurs           = $pembelian->Kurs;
      $retur_pembelian_grand_total->DPP            = AppHelper::StrReplace($request->DPP);
      $retur_pembelian_grand_total->Discount       = 0;
      $retur_pembelian_grand_total->PPN            = AppHelper::StrReplace($request->PPN);
      $retur_pembelian_grand_total->Grand_total    = AppHelper::StrReplace($request->Grand_total);

      $retur_pembelian_grand_total->save();


      foreach ($request->IDBarang as $key => $value) {
          if ($request->Qty[$key]) {
              $data_barang = BarangModel::find($value);

              $pembelian_detail = InvoicePembelianDetailModel::where('IDBarang', $value)->where('IDSatuan', $request->IDSatuan[$key])->where('IDFB', $pembelian->IDFB)->first();

              $data_pembelian_detail = InvoicePembelianDetailModel::findOrfail($pembelian_detail->IDFBDetail);

              $data_pembelian_detail->Saldo_yard = $data_pembelian_detail->Saldo_yard - $request->Qty[$key];

              $data_pembelian_detail->save();

  
              $IDRBDetail = uniqid();
              $retur_pembelian_detail = new ReturPembelianDetailModel;
  
              $retur_pembelian_detail->IDRBDetail     = $IDRBDetail;
              $retur_pembelian_detail->IDRB           = $IDRB;
              $retur_pembelian_detail->IDBarang       = $value;
              $retur_pembelian_detail->Qty_yard       = $request->Qty[$key];
              $retur_pembelian_detail->Saldo_yard     = $request->Qty[$key];
              $retur_pembelian_detail->IDSatuan       = $request->IDSatuan[$key];
              $retur_pembelian_detail->Harga          = AppHelper::StrReplace($request->Harga[$key]);
              $retur_pembelian_detail->Subtotal       = AppHelper::StrReplace($request->Sub_total[$key]);
  
              $retur_pembelian_detail->save();

              if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
                  $Qty_konversi = $request->Qty[$key];
              } else {
                  $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

                  $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty[$key];
              }

              // KARTU STOK //
              $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])->where('IDGudang', $tbs->IDGudang)->first();
  
              if ($data_stok) {
                  $stok = StokModel::find($data_stok->IDStok);
      
                  $stok->Qty_pcs = $data_stok->Qty_pcs - $Qty_konversi;
  
                  $stok->save();
              } else {
                  $IDStok = uniqid();
                  $stok = new StokModel;

                  $stok->IDStok = $IDStok;
                  $stok->Tanggal  = date('Y-m-d');
                  $stok->Nomor_faktur     = $Nomor;
                  $stok->Jenis_faktur     = 'Retur';
                  $stok->IDBarang         = $value;
                  $stok->IDGudang         = $tbs->IDGudang;
                  $stok->IDSatuan         = $data_barang->IDSatuan;
                  $stok->Qty_pcs          = $Qty_konversi;

                  $stok->save();
              }

              // KARTU STOK //
              $kartu_stok     = new KartustokModel;
  
              $kartu_stok->IDKartuStok        = uniqid();
              $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
              $kartu_stok->Nomor_faktur       = $Nomor;
              $kartu_stok->IDFaktur           = $IDRB;
              $kartu_stok->IDFakturDetail     = $IDRBDetail;
              $kartu_stok->IDStok             = $data_stok ? $data_stok->IDStok : $IDStok;
              $kartu_stok->Jenis_faktur       = 'RB';
              $kartu_stok->IDBarang           = $value;
              $kartu_stok->Harga              = AppHelper::StrReplace($request->Harga[$key]);;
              $kartu_stok->Nama_Barang        = $data_barang->Nama_Barang;
              $kartu_stok->Keluar_pcs         = $request->Qty[$key];
              $kartu_stok->IDSatuan           = $request->IDSatuan[$key];
  
              $kartu_stok->save();
              // END KARTU STOK //
          }
      }

      $data_hutang = HutangModel::where('IDFaktur', $pembelian->IDFB)->first();

      $hutang = HutangModel::findOrfail($data_hutang->IDHutang);

      $hutang->Saldo_Akhir  = $data_hutang->Saldo_Akhir - AppHelper::StrReplace($request->Grand_total);
      $hutang->Retur        = $data_hutang->Retur + AppHelper::StrReplace($request->Grand_total);

      $hutang->save();
      
      // SAVE JURNAL DEBET //
      $CoaDebet = SettingCOAModel::where('IDMenu', '=', '47')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->where('Cara', '=', 'lunas')
                            ->first();

      $jurnal_pembelian_debet = new JurnalModel;

      $jurnal_pembelian_debet->IDJurnal     = $this->number_jurnal();
      $jurnal_pembelian_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
      $jurnal_pembelian_debet->Nomor        = $Nomor;
      $jurnal_pembelian_debet->IDFaktur     = $IDRB;
      $jurnal_pembelian_debet->IDFakturDetail   = '-';
      $jurnal_pembelian_debet->Jenis_faktur = 'RB';
      $jurnal_pembelian_debet->IDCOA        = $CoaDebet->IDCoa;
      $jurnal_pembelian_debet->Debet        = AppHelper::StrReplace($request->Grand_total);
      $jurnal_pembelian_debet->Kredit       = 0;
      $jurnal_pembelian_debet->IDMataUang       = $pembelian->IDMataUang;
      $jurnal_pembelian_debet->Kurs             = $pembelian->Kurs;
      $jurnal_pembelian_debet->Total_debet      = AppHelper::StrReplace($request->Grand_total);
      $jurnal_pembelian_debet->Total_kredit     = 0;
      $jurnal_pembelian_debet->Keterangan       = 'Retur Pembelian ' . $Nomor;
      $jurnal_pembelian_debet->Saldo            = AppHelper::StrReplace($request->Grand_total);

      $jurnal_pembelian_debet->save();

      // SAVE JURNAL KREDIT //
      $CoaKredit = SettingCOAModel::where('IDMenu', '=', '47')
                            ->where('Posisi', '=', 'kredit')
                            ->where('Tingkat', '=', 'utama')
                            ->where('Cara', '=', 'lunas')
                            ->first();

      $jurnal_pembelian_kredit = new JurnalModel;

      $jurnal_pembelian_kredit->IDJurnal     = $this->number_jurnal();
      $jurnal_pembelian_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
      $jurnal_pembelian_kredit->Nomor        = $Nomor;
      $jurnal_pembelian_kredit->IDFaktur     = $IDRB;
      $jurnal_pembelian_kredit->IDFakturDetail   = '-';
      $jurnal_pembelian_kredit->Jenis_faktur = 'RB';
      $jurnal_pembelian_kredit->IDCOA        = $CoaKredit->IDCoa;
      $jurnal_pembelian_kredit->Debet        = 0;
      $jurnal_pembelian_kredit->Kredit       = AppHelper::StrReplace($request->DPP);
      $jurnal_pembelian_kredit->IDMataUang       = $pembelian->IDMataUang;
      $jurnal_pembelian_kredit->Kurs             = $pembelian->Kurs;
      $jurnal_pembelian_kredit->Total_debet      = 0;
      $jurnal_pembelian_kredit->Total_kredit     = AppHelper::StrReplace($request->DPP);
      $jurnal_pembelian_kredit->Keterangan       = 'Retur Pembelian ' . $Nomor;
      $jurnal_pembelian_kredit->Saldo            = AppHelper::StrReplace($request->DPP);

      $jurnal_pembelian_kredit->save();

      if ($request->PPN > 0) {
        // SAVE JURNAL KREDIT //
        $CoaKreditPPN = SettingCOAModel::where('IDMenu', '=', '47')
                            ->where('Posisi', '=', 'kredit')
                            ->where('Tingkat', '=', 'tambahan')
                            ->where('Cara', '=', 'lunas')
                            ->first();

        $jurnal_pembelian_kredit_ppn = new JurnalModel;
  
        $jurnal_pembelian_kredit_ppn->IDJurnal     = $this->number_jurnal();
        $jurnal_pembelian_kredit_ppn->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_pembelian_kredit_ppn->Nomor        = $Nomor;
        $jurnal_pembelian_kredit_ppn->IDFaktur     = $IDRB;
        $jurnal_pembelian_kredit_ppn->IDFakturDetail   = '-';
        $jurnal_pembelian_kredit_ppn->Jenis_faktur = 'RB';
        $jurnal_pembelian_kredit_ppn->IDCOA        = $CoaKreditPPN->IDCoa;
        $jurnal_pembelian_kredit_ppn->Debet        = 0;
        $jurnal_pembelian_kredit_ppn->Kredit       = AppHelper::StrReplace($request->Grand_total);
        $jurnal_pembelian_kredit_ppn->IDMataUang       = $pembelian->IDMataUang;
        $jurnal_pembelian_kredit_ppn->Kurs             = $pembelian->Kurs;
        $jurnal_pembelian_kredit_ppn->Total_debet      = 0;
        $jurnal_pembelian_kredit_ppn->Total_kredit     = AppHelper::StrReplace($request->Grand_total);
        $jurnal_pembelian_kredit_ppn->Keterangan       = 'PPN Retur Pembelian ' . $Nomor;
        $jurnal_pembelian_kredit_ppn->Saldo            = AppHelper::StrReplace($request->Grand_total);
  
        $jurnal_pembelian_kredit_ppn->save();
      }
      
      DB::commit();

      $response = array (
          'status'    => true,
          'message'   => 'Data berhasil disimpan.',
          'data'      => [
              'ID'    => $IDRB
          ]
      );

      return json_encode($response);
    }

    public function detail($id) {
      $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $data              = ReturPembelianModel::getIndex()->where('IDRB', $id)->first();

        $datadetail        = ReturPembelianDetailModel::getItem()->where('tbl_retur_pembelian_detail.IDRB', $data->IDRB)->get();

        return view('retur_beli.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'data', 'datadetail'));
    }

    public function print($id) {
      $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $data              = ReturPembelianModel::getIndex()->where('IDRB', $id)->first();

        $datadetail        = ReturPembelianDetailModel::getItem()->where('tbl_retur_pembelian_detail.IDRB', $data->IDRB)->get();

        $perusahaan          = PerusahaanModel::Getforprint();

        return view('retur_beli.print', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'data', 'datadetail', 'perusahaan'));
    }

    public function laporan(Request $request) {
      $iduser             = Auth::user()->id;
      $namauser           = Auth::user()->name;
      $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
      $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
      $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
      $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
      $retur              = DB::table('view_returpembelian')->get();

      return view('retur_beli/laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'retur'));
    }

    public function get_pembelian(Request $request) {
        $pembelian = InvoicePembelianModel::GetIndex()->where('tbl_pembelian.IDFB', $request->id)->first();
        
        return json_encode($pembelian);
    }

    public function get_pembelian_detail(Request $request) {
        $pembelian_detail = InvoicePembelianDetailModel::GetItem()->where('IDFB', $request->get('IDFB'))->where('Saldo_yard', '>', 0)->get();
        
        return Datatables::of($pembelian_detail)->make(true);
    }

    private function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    public function laporan_retur(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $mata_uang          = MataUangModel::where('Aktif', 'aktif')->get();
        return view('retur_beli.laporan_retur', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'mata_uang'));   
    }

    public function datatable_laporan(Request $request) {
        $data = ReturPembelianModel::select('tbl_retur_pembelian.Tanggal', 'tbl_retur_pembelian.Nomor as NomorRB', 'tbl_pembelian.Nomor as NomorINV', 'tbl_terima_barang_supplier.Nomor as NomorPB', 'Nama', 'Kode', 'Nama_Barang', 'tbl_retur_pembelian_detail.Qty_yard as Qty', 'tbl_retur_pembelian_detail.Harga', 'tbl_retur_pembelian_detail.Subtotal as Saldo');
        $data->join('tbl_retur_pembelian_detail', 'tbl_retur_pembelian.IDRB', '=', 'tbl_retur_pembelian_detail.IDRB')
             ->join('tbl_retur_pembelian_grand_total', 'tbl_retur_pembelian.IDRB', '=', 'tbl_retur_pembelian_grand_total.IDRB')
             ->join('tbl_pembelian', 'tbl_retur_pembelian.IDFB', '=', 'tbl_pembelian.IDFB')             
             ->join("tbl_supplier", "tbl_retur_pembelian.IDSupplier","=","tbl_supplier.IDSupplier")
             ->join("tbl_barang", "tbl_retur_pembelian_detail.IDBarang", "=", "tbl_barang.IDBarang")
             ->join("tbl_mata_uang", "tbl_retur_pembelian_grand_total.IDMataUang", "=", "tbl_mata_uang.IDMataUang")
             ->join("tbl_terima_barang_supplier", "tbl_pembelian.IDTBS", "=", "tbl_terima_barang_supplier.IDTBS");

        if ($request->get('tanggal_awal')) {
            //tanggal awal tidak kosong            
            if ($request->get('tanggal_akhir')) {
                if($request->get('mata_uang')){
                    $data->whereBetween('tbl_retur_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));
                }else{
                    $data->whereBetween('tbl_retur_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                }
                if($request->get('field')&&$request->get('keyword')) {
                    $data->whereBetween('tbl_retur_pembelian.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang')); 
                                       
                    if($request->get('field') == 'NomorINV'){
                        $data->where('tbl_pembelian.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    }elseif($request->get('field') == 'NomorPB') {
                        $data->where('tbl_terima_barang_supplier.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    }elseif($request->get('field') == 'NomorRB') {
                        $data->where('tbl_retur_pembelian.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    }else {
                        $data->where(''.$request->get('field').'', 'ilike', '%' . $request->get('keyword') . '%');
                    }
                }
            } else {                
                $data->where('tbl_retur_pembelian.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            //tanggal awal kosong
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_retur_pembelian.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        
        $data->get();
               
        return Datatables::of($data)->make(true);        
    }
}

?>
