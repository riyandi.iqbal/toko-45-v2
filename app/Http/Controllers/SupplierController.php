<?php
/*=====Create TIAR @06/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use App\Models\GroupSupplierModel;
use App\Models\KotaModel;
use App\Models\SupplierModel;
use Validator;

class SupplierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('supplier.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = SupplierModel::select('*');

        $data->join('tbl_group_supplier', 'tbl_group_supplier.IDGroupSupplier', '=', 'tbl_supplier.IDGroupSupplier');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Supplier')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $DataGroup          = GroupSupplierModel::get();
        $DataKota           = KotaModel::get();
        $kode               = SupplierModel::select(DB::raw('MAX(SUBSTRING("Kode_Suplier", 4, 5)) as curr_number'))->first();

        return view('supplier.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'DataGroup', 'DataKota', 'kode'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDGroupSupplier'              => 'required',
            'Kode_Suplier'              => 'required|unique:tbl_supplier,Kode_Suplier',
            'Nama'             => 'required',
            'Alamat'             => 'required',
            'No_Telpon'             => 'required',
            'IDKota'             => 'required',
            'Fax'             => 'required',
            'Email'             => 'required',
            'NPWP'             => 'required',
            'No_KTP'             => 'required',

        ])->setAttributeNames([
            'IDGroupSupplier'  => 'Group Supplier',
            'Kode_Suplier'    => 'Kode',
            'Nama'             => 'Nama',
            'Alamat'  => 'Alamat',
            'No_Telpon'    => 'Telp',
            'IDKota'             => 'Kota',
            'Fax'  => 'Fax',
            'Email'    => 'Email',
            'NPWP'             => 'NPWP',
            'No_KTP'  => 'KTP',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = new SupplierModel;

        $data->IDGroupSupplier   = $request->IDGroupSupplier;
        $data->Kode_Suplier      = $request->Kode_Suplier;
        $data->Nama              = $request->Nama;
        $data->Alamat            = $request->Alamat;
        $data->No_Telpon         = $request->No_Telpon;
        $data->IDKota            = $request->IDKota;
        $data->Fax               = $request->Fax;
        $data->Email             = $request->Email;
        $data->NPWP              = $request->NPWP;
        $data->No_KTP            = $request->No_KTP;
        $data->Aktif             = 'aktif';
            
        $data->save();

        DB::commit();
        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupCOA')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $DataGroup          = GroupSupplierModel::get();
        $DataKota           = KotaModel::get();

        $Supplier           = SupplierModel::findOrfail($id);

        return view('supplier.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'DataGroup', 'DataKota', 'Supplier'));   
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDSupplier'              => 'required',
            'IDGroupSupplier'              => 'required',
            'Kode_Suplier'              => 'required',
            'Nama'             => 'required',
            'Alamat'             => 'required',
            'No_Telpon'             => 'required',
            'IDKota'             => 'required',
            'Fax'             => 'required',
            'Email'             => 'required',
            'NPWP'             => 'required',
            'No_KTP'             => 'required',

        ])->setAttributeNames([
            'IDSupplier'              => 'Data',
            'IDGroupSupplier'  => 'Group Supplier',
            'Kode_Suplier'    => 'Kode',
            'Nama'             => 'Nama',
            'Alamat'  => 'Alamat',
            'No_Telpon'    => 'Telp',
            'IDKota'             => 'Kota',
            'Fax'  => 'Fax',
            'Email'    => 'Email',
            'NPWP'             => 'NPWP',
            'No_KTP'  => 'KTP',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = SupplierModel::findOrfail($request->IDSupplier);

        $data->IDGroupSupplier   = $request->IDGroupSupplier;
        $data->Kode_Suplier      = $request->Kode_Suplier;
        $data->Nama              = $request->Nama;
        $data->Alamat            = $request->Alamat;
        $data->No_Telpon         = $request->No_Telpon;
        $data->IDKota            = $request->IDKota;
        $data->Fax               = $request->Fax;
        $data->Email             = $request->Email;
        $data->NPWP              = $request->NPWP;
        $data->No_KTP            = $request->No_KTP;
        $data->Aktif             = 'aktif';
            
        $data->save();

        DB::commit();
        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }
        
        $data_exist  = SupplierModel::findOrfail($id)->delete();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function simpandatagroupsupplier(Request $request)
    {
        $nextnumber = DB::table('tbl_group_supplier')->selectRaw(DB::raw('MAX("IDGroupSupplier") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDGroupSupplier'           => $urutan_id,
            'Kode_Group_Supplier'       => $request->Kode_Group_Supplier,
            'Group_Supplier'            => $request->Group_Supplier,
            'Aktif'                     => 'aktif',
        ];

        $createheader=DB::table('tbl_group_supplier')->insert($header);

        return redirect()->action('GroupSupplierController@tambah_supplier');
    }

    public function simpandatakota(Request $request)
    {
        $nextnumber = DB::table('tbl_kota')->selectRaw(DB::raw('MAX("IDKota") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDKota'            => $urutan_id,
            'Kode_Kota'         => $request->kode,
            'Provinsi'          => $request->provinsi,
            'Kota'              => $request->kota,
            'Aktif'             => 'Aktif',
        ];

        $createheader=DB::table('tbl_kota')->insert($header);

        return redirect()->action('GroupSupplierController@tambah_supplier');
    }

}