<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Illuminate\Support\Facades\Storage;

use Datatables;
use App\Models\NeracaModel;
use App\Models\CoaModel;
use App\Models\LabaRugiModel;
use App\Models\PostingModel;
use App\Models\JurnalModel;



// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class KeuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function balancesheet()
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = DB::table('tbl_coa')->orderBy('Kode_COA', 'asc')->get();

        $data1        = NeracaModel::leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_neraca.IDCoa')
                        ->select('tbl_setting_neraca.*','tbl_coa.Nama_COA', 'tbl_coa.IDCoa', 'tbl_coa.Kode_COA')
                        ->orderBy('tbl_setting_neraca.Urutan', 'asc')
                        ->get();

        return view('keuangan/settingbs', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa', 'data1'));
        
    }

    public function simpanbs(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'IDCoa.*'           => 'nullable',
            'Keterangan.*'             => 'required',
            'Kategori.*'             => 'required',
            'Group.*'             => 'required',
        ])->setAttributeNames([
            'IDCoa.*'    => 'COA',
            'Keterangan.*'             => 'Keterangan',
            'Kategori.*'             => 'Kategori',
            'Group.*'             => 'Group',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data_exist_delete = NeracaModel::query()->truncate();

        $urutan = 0;
        foreach ($request->Keterangan as $key => $value) {
            $coa = CoaModel::find($request->IDCoa[$key]);
            $data = new NeracaModel;

            $data->IDNeraca = uniqid();
            $data->IDCoa        = $request->IDCoa[$key];
            $data->Keterangan   = $request->Keterangan[$key];
            $data->Kategori   = $request->Kategori[$key];
            $data->Perhitungan   = $coa ? $coa->Normal_Balance : '-';
            $data->Group    = $request->Group[$key] ? $request->Group[$key] : 0 ;
            $data->Urutan   = $urutan;
            $data->Nilai = 'on';

            $data->save();

            $urutan++;
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
        );

        return json_encode($data);
    }

    public function balancesheetindex(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $set_lr             = DB::select('SELECT * FROM bs()');

        return view('keuangan/indexneraca', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'set_lr'));
    }

    public function datatable(Request $request) {
        $data = JurnalModel::select('*');

        $data->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_jurnal.IDCOA');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function get_laporan_neraca(Request $request) {
        $validate = Validator::make($request->all(), [
            'bulan'           => 'required',
            'tahun'             => 'required',
            'bulan_awal'           => 'required',
            'tahun_awal'             => 'required',
        ])->setAttributeNames([
            'bulan'    => 'Bulan',
            'tahun'             => 'Tahun',
            'bulan_awal'    => 'Bulan',
            'tahun_awal'             => 'Tahun',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $month_now = $request->bulan;
        $month_ongoing = $request->bulan_awal;

        $year = $request->tahun;
        $year_ongoing = $request->tahun_awal;

        $laba = 0;
        $cek_posting = PostingModel::where('Bulan', $month_now)->where('Tahun', $year)->where('Status', 'aktif')->first();

        if ($cek_posting) {
            $laba_rugi = LabaRugiModel::select('tbl_setting_labarugi.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_labarugi."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$month_now.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year.'\' and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_now'))
                                        ->orderBy('Urutan', 'Asc')
                                        ->get();
            
            foreach ($laba_rugi as $key => $value) {
                $laba += $value->total_now ? $value->total_now : 0;
            }
        }

        $laba_sebelumnya = 0;
        $cek_posting_sebelumnya = PostingModel::where('Bulan', $month_ongoing)->where('Tahun', $year_ongoing)->where('Status', 'aktif')->first();

        if ($cek_posting_sebelumnya) {
            $laba_rugi = LabaRugiModel::select('tbl_setting_labarugi.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_labarugi."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")<=\''.$month_now.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year.'\' and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_now'))
                                        ->orderBy('Urutan', 'Asc')
                                        ->get();
            
            foreach ($laba_rugi as $key => $value) {
                $laba_sebelumnya += $value->total_now ? $value->total_now : 0;
            }
        }

        $neraca = NeracaModel::select('tbl_setting_neraca.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_neraca."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$month_now.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year.'\'  and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_now'), DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_neraca."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")<=\''.$month_ongoing.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year_ongoing.'\' and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_ongoing'))
                                    ->orderBy('Urutan', 'Asc')
                                    ->get();
        $sum_now = array();
        $sum_ongoing = array();
        foreach ($neraca as $key => $value) {
            $value->total_now = $value->total_now ? $value->total_now : 0 ;
            $value->total_ongoing = $value->total_ongoing ? $value->total_ongoing : 0 ;

            if ($value->Keterangan == '30103 -- LABA TAHUN BERJALAN') {
                $value->total_now = $laba;
                $value->total_ongoing = $laba_sebelumnya;
            }

            if (isset($sum_now[$value->Group])) {
                $sum_now[$value->Group] += $value->total_now;
                $sum_ongoing[$value->Group] += $value->total_ongoing;
            } else {
                $sum_now[$value->Group]   = $value->total_now;
                $sum_ongoing[$value->Group]   = $value->total_ongoing;
            }
            

            if ($value->Kategori == 'hasil' || $value->Kategori == 'total') {
                $array_data = explode(',', $value->Group);
                
                foreach ($array_data as $keys => $values) {
                    $value->total_now += $sum_now[$values];
                    $value->total_ongoing += $sum_ongoing[$values];
                }
            }

            $value->selisih = $value->total_now + $value->total_ongoing;
        }
        
        $response = array(
            'bulan' => $month_now,
            'bulan_ongoing' => $month_ongoing,
            'tahun' => $year,
            'tahun_ongoing' => $year_ongoing,
            'data'  => $neraca,
            'sum_now'   => $sum_now,
            'sum_ongoing'   => $sum_ongoing
        );
        
        return json_encode($response);
    }

    public function cari_bulan_lr(Request $request)
    {
        
        $bulan= $request->get('bulan');
        $tahun= $request->get('tahun');
        $data['bulan']= $request->get('bulan');
        $data['bulan1']= $request->get('bulan1');
        $data['tahun']= $request->get('tahun');
        $data['tahun1']= $request->get('tahun1');
        $bulan_dikurang= $request->get('bulan1');
        $tahun1= $request->get('tahun1');
        $data['bulan_ini']= $request->get('bulan');
        $data['bulan_ini_dikurang']= $bulan_dikurang;

        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        // laba rugi tahun berjalan
        $laba_rugi = DB::select('SELECT
                                        m1."Kategori",
                                        m1."Keterangan",
                                        m1."IDCOA",
                                        m2.Total_bulan_ini,
                                        m1.Bulan_sd_bulan,
                                        m1."Perhitungan",
                                        m1."Group" 
                                    FROM
                                        (
                                    SELECT
                                        tbl_setting_neraca."IDNeraca",
                                        tbl_setting_neraca."Kategori",
                                        tbl_setting_neraca."Group",
                                        tbl_setting_neraca."Keterangan",
                                        tbl_jurnal."IDCOA",
                                        SUM ( tbl_jurnal."Debet" - tbl_jurnal."Kredit" ) AS Bulan_sd_bulan,
                                        tbl_setting_neraca."Perhitungan" 
                                    FROM
                                        tbl_jurnal
                                        RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
                                        AND ( date_part( \'month\', tbl_jurnal."Tanggal" ) BETWEEN \'1\' AND \''.$bulan.'\' ) 
                                        AND date_part( \'year\', tbl_jurnal."Tanggal" ) = \''.$tahun.'\' 
                                    WHERE
                                        COALESCE(tbl_jurnal."Batal",\'aktif\') = \'aktif\'
                                    GROUP BY
                                    tbl_setting_neraca."IDNeraca",
                                    tbl_setting_neraca."Kategori",
                                    tbl_setting_neraca."Group",
                                    tbl_setting_neraca."IDCoa",
                                    tbl_setting_neraca."Keterangan",
                                    tbl_setting_neraca."Perhitungan",
                                    tbl_jurnal."IDCOA" 
                                    ORDER BY
                                    tbl_setting_neraca."IDNeraca" ASC 
                                    ) m1
                                    LEFT JOIN (
                                    SELECT
                                        tbl_jurnal."IDCOA",
                                        tbl_setting_neraca."Group",
                                        SUM ( tbl_jurnal."Debet" - tbl_jurnal."Kredit" ) AS Total_bulan_ini 
                                    FROM
                                        tbl_jurnal
                                        RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
                                        AND date_part( \'month\', tbl_jurnal."Tanggal" ) = \''.$bulan.'\' 
                                        AND date_part( \'year\', tbl_jurnal."Tanggal" ) = \''.$tahun.'\' 
                                    WHERE
                                        COALESCE(tbl_jurnal."Batal",\'aktif\') = \'aktif\'
                                    GROUP BY
                                    tbl_setting_neraca."Group",
                                        tbl_jurnal."IDCOA" 
                                    ) m2 USING ("IDCOA")');
        // $hasil_kiri = array();
        // foreach($laba_rugi as $lr) {
        //     if($lr->Kategori=="data"){
        //         $hasil_kiri[$lr->Group] += ($lr->total_bulan_ini);
        //     }
            
        //     if($lr->Kategori=="total"){
        //         $groups = explode(',',$lr->Group);
        //         $total_kiri = 0;
        //         for($i= 0;$i < count($groups) ; $i++){
        //             $total_kiri += $hasil_kiri[$groups[$i]];
        //         }
                
        //     } 
        // }
        //$data['laba'] = $total_kiri; // laba rugi berjalan
        
        $laba_rugi_kanan = DB::select('SELECT
                                    m1."Kategori",
                                    m1."Keterangan",
                                    m1."IDCOA",
                                    m2.Total_bulan_ini,
                                    m1.Bulan_sd_bulan,
                                    m1."Perhitungan",
                                    m1."Group" 
                                FROM
                                    (
                                SELECT
                                    tbl_setting_neraca."IDNeraca",
                                    tbl_setting_neraca."Kategori",
                                    tbl_setting_neraca."Group",
                                    tbl_setting_neraca."Keterangan",
                                    tbl_jurnal."IDCOA",
                                    SUM ( tbl_jurnal."Debet" - tbl_jurnal."Kredit" ) AS Bulan_sd_bulan,
                                    tbl_setting_neraca."Perhitungan" 
                                FROM
                                    tbl_jurnal
                                    RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
                                    AND ( date_part( \'month\', tbl_jurnal."Tanggal" ) BETWEEN \'1\' AND \''.$bulan.'\' ) 
                                    AND date_part( \'year\', tbl_jurnal."Tanggal" ) = \''.$tahun.'\' 
                                WHERE
                                    COALESCE(tbl_jurnal."Batal",\'aktif\') = \'aktif\'
                                GROUP BY
                                tbl_setting_neraca."IDNeraca",
                                tbl_setting_neraca."Kategori",
                                tbl_setting_neraca."Group",
                                    tbl_jurnal."IDCOA",
                                    tbl_setting_neraca."Keterangan",
                                    tbl_setting_neraca."Perhitungan" 
                                ORDER BY
                                tbl_setting_neraca."IDNeraca" ASC 
                                ) m1
                                LEFT JOIN (
                                SELECT
                                    tbl_jurnal."IDCOA",
                                    tbl_setting_neraca."Group",
                                    SUM ( tbl_jurnal."Debet" - tbl_jurnal."Kredit" ) AS Total_bulan_ini 
                                FROM
                                    tbl_jurnal
                                    RIGHT JOIN tbl_setting_neraca ON tbl_jurnal."IDCOA" = tbl_setting_neraca."IDCoa" 
                                    AND date_part( \'month\', tbl_jurnal."Tanggal" ) = \''.$request->get('bulan1').'\' 
                                    AND date_part( \'year\', tbl_jurnal."Tanggal" ) = \''.$request->get('tahun1').'\' 
                                WHERE
                                    COALESCE(tbl_jurnal."Batal",\'aktif\') = \'aktif\'
                                GROUP BY
                                tbl_setting_neraca."Group",
                                    tbl_jurnal."IDCOA" 
                                ) m2 USING ("IDCOA")');
        // $hasil_kanan = array();
        // foreach($laba_rugi_kanan as $lr_kanan) {
        //     if($lr_kanan->Kategori=="data"){
        //         $hasil_kanan[$lr_kanan->Group] += ($lr_kanan->total_bulan_ini);
        //     }
            
        //     if($lr_kanan->Kategori=="total"){
        //         $groups = explode(',',$lr_kanan->Group);
        //         $total_kanan = 0;
        //         for($n= 0;$n < count($groups) ; $n++){
        //             $total_kanan += $hasil_kanan[$groups[$n]];
        //         }
        //     }
        // }
        //$data['laba_kanan'] = $total_kanan;        
        // end laba rugi tahun berjalan 

        //$set_lr             = DB::select('SELECT * FROM bs()');
                                if($bulan=='1')
                                {
                                    $query = DB::select('SELECT * FROM bs()');
                                }else{
                                    $query = DB::select('SELECT * FROM bs_cari3(\''.$bulan.'\',\''.$request->get('bulan1').'\',\''.$tahun.'\',\''.$request->get('tahun1').'\')');
                                }
        $set_lr =  $query;
        return view('keuangan/indexneraca2', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'set_lr', 'data'));
    }

    public function labarugi(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = DB::table('tbl_coa')->orderBy('Kode_COA', 'asc')->get();

        // $data1        = $this->get_child_view(0);
        $data1          = LabaRugiModel::leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_labarugi.IDCoa')
                        ->select('tbl_setting_labarugi.*','tbl_coa.Nama_COA', 'tbl_coa.IDCoa', 'tbl_coa.Kode_COA')
                        ->orderBy('tbl_setting_labarugi.Urutan', 'asc')
                        ->get();
                                    
        return view('keuangan/settinglabarugi', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa', 'data1'));
        
    }

    private function get_child($group)
    {
        $data = LabaRugiModel::leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_labarugi.IDCoa')
                        ->select('tbl_setting_labarugi.*','tbl_coa.Nama_COA', 'tbl_coa.IDCoa', 'tbl_coa.Kode_COA')
                        ->where('tbl_setting_labarugi.Group', '=', $group)
                        ->orderBy('tbl_setting_labarugi.Urutan', 'asc')
                        ->get();

        foreach ($data as $key => $value) {
            $value->child = $this->get_child($value->IDCoa);
        }

        return $data;
    }

    private function get_child_view($group)
    {
        $coa                = DB::table('tbl_coa')->orderBy('Kode_COA', 'asc')->get();

        $data = LabaRugiModel::leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_labarugi.IDCoa')
                        ->select('tbl_setting_labarugi.*','tbl_coa.Nama_COA', 'tbl_coa.IDCoa', 'tbl_coa.Kode_COA')
                        ->where('tbl_setting_labarugi.Group', '=', $group)
                        ->orderBy('tbl_setting_labarugi.Urutan', 'asc')
                        ->get();

        $kategori = array(
            'judul' => 'Judul', 
            'data' => 'Data', 
            'hasil' => 'Hasil', 
            'spasi' => 'Spasi', 
            'total' => 'Total', 
        );

        foreach ($data as $key => $value) {
            $value->view = '<tr>
            <td style="width: 10%"> 
                <button type="button" class="btn col-20 btn-sm btn-add"> + </button> <button type="button" class="btn col-18 btn-sm btn-remove"> - </button>
            </td>
            <td style="width: 35%">
                <select name="IDCoa[]" class="select2 idcoa" style="width: 100%">
                    <option value="-">-</option>';
                    foreach ($coa as $key => $val) {
                        $selected = $val->IDCoa == $value->IDCoa ? 'selected' : '';
                        $value->view .= '<option value="'.$val->IDCoa.'" data-kode="'.$val->Kode_COA.'" data-nama="'.$val->Nama_COA.'" '.$selected.' >'.$val->Kode_COA . ' - ' . $val->Nama_COA.'</option>';
                    }
                    $value->view .= '</select></td>';

            $value->view .= '<td style="width: 25%">    
                <textarea name="Keterangan[]" class="keterangan">' . $value->Keterangan . '</textarea>
            </td>';

            $value->view .= '<td> <select name="Kategori[]" class="select2 kategori" style="width: 100%">';
            foreach ($kategori as $key_kat => $kat) {
                $sel = $key_kat == $value->Kategori ? 'selected' : '';
                $value->view .= '<option value="'.$key_kat.'" '.$sel.' > '.$kat.' </option>';
            }
            $value->view .= '</select></td>';

            // $value->view .= '<td style="width: 15%">
            //                 <select name="Group[]" class="select2 group" style="width: 100%">
            //         <option value="">-</option>';
            //         foreach ($coa as $key => $val) {
            //             $selected = $val->IDCoa == $value->Group ? 'selected' : '';
            //             $value->view .= '<option value="'.$val->IDCoa.'" data-kode="'.$val->Kode_COA.'" data-nama="'.$val->Nama_COA.'" '.$selected.' >'.$val->Kode_COA . ' - ' . $val->Nama_COA.'</option>';
            //         }
            //         $value->view .= '</select></td></tr>';

            $value->view .= '<td style="width: 10%"> <input type="text" name="Group[]" class="group" value="'.$value->Group.'"> </td> </tr>';

            $value->child = $this->get_child_view($value->IDCoa);
        }

        return $data;
    }

    public function simpanlabarugi(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDCoa.*'           => 'nullable',
            'Keterangan.*'             => 'required',
            'Kategori.*'             => 'required',
            'Group.*'             => 'required',
        ])->setAttributeNames([
            'IDCoa.*'    => 'COA',
            'Keterangan.*'             => 'Keterangan',
            'Kategori.*'             => 'Kategori',
            'Group.*'             => 'Group',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data_exist_delete = LabaRugiModel::query()->truncate();

        $urutan = 0;
        foreach ($request->Keterangan as $key => $value) {
            $coa = CoaModel::find($request->IDCoa[$key]);
            $data = new LabaRugiModel;

            $data->IDLabaRugi = uniqid();
            $data->IDCoa        = $request->IDCoa[$key];
            $data->Keterangan   = $request->Keterangan[$key];
            $data->Kategori   = $request->Kategori[$key];
            $data->Perhitungan   = $coa ? $coa->Normal_Balance : '-';
            $data->Group    = $request->Group[$key] ? $request->Group[$key] : 0 ;
            $data->Urutan   = $urutan;

            $data->save();

            $urutan++;
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
        );

        return json_encode($data);
    }

    public function labarugiindex(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $set_pl             = DB::select('SELECT * FROM pl()');

        return view('keuangan/indexlabarugi', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'set_pl'));
    }

    public function glindex(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $set_pl             = DB::select('SELECT * FROM pl()');

        $coa          = DB::table('tbl_coa')->orderBy('Nama_COA', 'asc')->get();
        $jurnal             = JurnalModel::Getforjurnal();

        return view('keuangan/indexgl', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'set_pl','coa', 'jurnal'));
    }

    public function cari_bulan_pl(Request $request)
    {
        $bulan= $request->get('bulan');
        $tahun= $request->get('tahun');
        $data['bulan']= $request->get('bulan');
        $data['tahun']= $request->get('tahun');
        $bulan_dikurang= $request->get('bulan')-1;
        $data['bulan_ini']= $request->get('bulan');
        $data['bulan_ini_dikurang']= $bulan_dikurang;

        if($bulan=='1')
        {
            $query = DB::select('SELECT * FROM pl()');
        }else{
            $query = DB::select('SELECT * FROM pl_cari(\''.$bulan.'\', \''.$tahun.'\')');
        }

        $set_pl = $query;

        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('keuangan/indexlabarugi2', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'set_pl', 'data'));
    }

    public function get_laporan(Request $request) {
        $validate = Validator::make($request->all(), [
            'bulan'           => 'required',
            'tahun'             => 'required',
        ])->setAttributeNames([
            'bulan'    => 'Bulan',
            'tahun'    => 'Tahun',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $month_now = $request->bulan;
        $month_ongoing = $request->bulan - 1;

        $year = $request->tahun;

        $laba_rugi = LabaRugiModel::select('tbl_setting_labarugi.*', DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_labarugi."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")=\''.$month_now.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year.'\' and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_now'), DB::raw('(select SUM(("Debet"- "Kredit") * "Kurs") as Total FROM tbl_jurnal where "IDCOA" = tbl_setting_labarugi."IDCoa" and date_part(\'month\', tbl_jurnal."Tanggal")<=\''.$month_now.'\' and date_part(\'year\', tbl_jurnal."Tanggal")=\''.$year.'\' and "Jenis_faktur" != \'Saldo Awal\' GROUP BY "IDCOA") as Total_ongoing'))
                                    ->orderBy('Urutan', 'Asc')
                                    ->get();
        $sum_now = array();
        $sum_ongoing = array();
        foreach ($laba_rugi as $key => $value) {
            $value->total_now = $value->total_now ? $value->total_now : 0 ;
            $value->total_ongoing = $value->total_ongoing ? $value->total_ongoing : 0 ;

            if (isset($sum_now[$value->Group])) {
                $sum_now[$value->Group] += $value->total_now;
                $sum_ongoing[$value->Group] += $value->total_ongoing;
            } else {
                $sum_now[$value->Group]   = 0;
                $sum_ongoing[$value->Group]   = 0;
            }

            if ($value->Kategori == 'hasil' || $value->Kategori == 'total') {
                $value->total_now = 0;
                $value->total_ongoing = 0;
                $array_data = explode(',', $value->Group);
                
                foreach ($array_data as $keys => $values) {
                    $value->total_now += $sum_now[$values];
                    $value->total_ongoing += $sum_ongoing[$values];
                }
            }
        }
        
        $response = array(
            'bulan' => $month_now,
            'bulan_ongoing' => $month_ongoing,
            'tahun' => $year,
            'data'  => $laba_rugi,
            'sum_now'   => $sum_now,
            'sum_ongoing'   => $sum_ongoing
        );
        
        return json_encode($response);
    }

    // public function get_laporan_gl(Request $request) {
    //     $validate = Validator::make($request->all(), [
    //         'bulan'           => 'required',
    //         'tahun'             => 'required',
    //     ])->setAttributeNames([
    //         'bulan'    => 'Bulan',
    //         'tahun'    => 'Tahun',
    //     ]);
        
    //     if ($validate->fails()) {
    //         $data = [
    //             'status'    => false,
    //             'message'   => strip_tags($validate->errors()->first())
    //         ];
    //         return json_encode($data);
    //     }

        
        
    //     $response = array(
    //         'bulan' => $month_now,
    //     );
        
    //     return json_encode($response);
    // }

    public function carikodecoa(Request $request)
    {
        $data = $request->get('data1');
        $dataambil = DB::table('tbl_coa')->where('IDCoa', '=', $data)->first();
        return json_encode($dataambil);
    }

    public function editbs(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $coa                = DB::table('tbl_coa')->orderBy('Kode_COA', 'asc')->get();

        $headergroup        = NeracaModel::join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_neraca.IDCoa')
                                    ->select('tbl_setting_neraca.*','tbl_coa.Nama_COA', 'tbl_coa.IDCoa', 'tbl_coa.Kode_COA')
                                    ->where('tbl_setting_neraca.Group', '=', '0')
                                    ->orderBy('tbl_setting_neraca.IDNeraca', 'asc')
                                    ->get();
        
        $dataedit           = NeracaModel::where('IDNeraca', '=', $request->get('id'))->first();

        return view('keuangan/settingbsedit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'coa', 'headergroup', 'dataedit'));
    }
    
}
