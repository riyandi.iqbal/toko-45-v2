<?php
/*=====Create TIAR @06/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use App\Models\GroupCustomerModel;
use App\Models\KotaModel;
use App\Models\CustomerModel;
use Validator;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('customer.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = CustomerModel::select('*');
        $data->join('tbl_group_customer', 'tbl_customer.IDGroupCustomer','=','tbl_group_customer.IDGroupCustomer');
        $data->select('tbl_customer.*','tbl_group_customer.Nama_Group_Customer');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Customer')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $groupcustomer          = GroupCustomerModel::get();
        $kota           = KotaModel::get();
        $kode_customer      = '';

        return view('customer.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'groupcustomer', 'kota', 'kode_customer'));
    }

    public function kode_customer(Request $request) {
        $groupcustomer = GroupCustomerModel::where('IDGroupCustomer', $request->group)
                        ->first();

        $group = $groupcustomer->Kode_Group_Customer;

        $nomor = CustomerModel::selectRaw(DB::raw('MAX("Kode_Customer") as nonext'))
        ->where('IDGroupCustomer', '=', $request->group)
        ->first();
        
        if ($nomor->nonext == '') {
            $nomor_baru = $group.'001';
        } else{
            $hasil5 = substr($nomor->nonext,3,5) + 1;
            $nomor_baru = $group.str_pad($hasil5, 3, 0, STR_PAD_LEFT);
        }

        if($request->ajax()){
            return json_encode($nomor_baru);
        } else {
            return $nomor_baru;
        }
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDGroupCustomer'              => 'required',
            'Kode_Customer'              => 'required|unique:tbl_customer,Kode_Customer',
            'Nama'             => 'required',
            'Alamat'             => 'required',
            'No_Telpon'             => 'required',
            'IDKota'             => 'required',
            'Fax'             => 'required',
            'Email'             => 'required',
            'NPWP'             => 'required',
            'No_KTP'             => 'required',

        ])->setAttributeNames([
            'IDGroupCustomer'  => 'Group Customer',
            'Kode_Customer'    => 'Kode',
            'Nama'             => 'Nama',
            'Alamat'  => 'Alamat',
            'No_Telpon'    => 'Telp',
            'IDKota'             => 'Kota',
            'Fax'  => 'Fax',
            'Email'    => 'Email',
            'NPWP'             => 'NPWP',
            'No_KTP'  => 'KTP',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $nextnumber = CustomerModel::selectRaw(DB::raw('MAX("IDCustomer") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new CustomerModel;

        $data->IDCustomer        = $urutan_id;
        $data->IDGroupCustomer   = $request->IDGroupCustomer;
        $data->Kode_Customer      = $request->Kode_Customer;
        $data->Nama              = $request->Nama;
        $data->Alamat            = $request->Alamat;
        $data->No_Telpon         = $request->No_Telpon;
        $data->IDKota            = $request->IDKota;
        $data->Fax               = $request->Fax;
        $data->Email             = $request->Email;
        $data->NPWP              = $request->NPWP;
        $data->No_KTP            = $request->No_KTP;
        $data->Aktif             = 'aktif';
            
        $data->save();

        DB::commit();
        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupCOA')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $groupcustomer      = GroupCustomerModel::get();
        $kota               = KotaModel::get();

        $edit           = CustomerModel::findOrfail($id);

        return view('customer.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'groupcustomer', 'kota', 'edit'));   
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDCustomer'              => 'required',
            'IDGroupCustomer'              => 'required',
            'Kode_Customer'              => 'required',
            'Nama'             => 'required',
            'Alamat'             => 'required',
            'No_Telpon'             => 'required',
            'IDKota'             => 'required',
            'Fax'             => 'required',
            'Email'             => 'required',
            'NPWP'             => 'required',
            'No_KTP'             => 'required',

        ])->setAttributeNames([
            'IDCustomer'              => 'Data',
            'IDGroupCustomer'  => 'Group Customer',
            'Kode_Customer'    => 'Kode',
            'Nama'             => 'Nama',
            'Alamat'  => 'Alamat',
            'No_Telpon'    => 'Telp',
            'IDKota'             => 'Kota',
            'Fax'  => 'Fax',
            'Email'    => 'Email',
            'NPWP'             => 'NPWP',
            'No_KTP'  => 'KTP',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = CustomerModel::findOrfail($request->IDCustomer);

        $data->IDGroupCustomer   = $request->IDGroupCustomer;
        $data->Kode_Customer      = $request->Kode_Customer;
        $data->Nama              = $request->Nama;
        $data->Alamat            = $request->Alamat;
        $data->No_Telpon         = $request->No_Telpon;
        $data->IDKota            = $request->IDKota;
        $data->Fax               = $request->Fax;
        $data->Email             = $request->Email;
        $data->NPWP              = $request->NPWP;
        $data->No_KTP            = $request->No_KTP;
        $data->Aktif             = 'aktif';
            
        $data->save();

        DB::commit();
        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }
        
        $data_exist  = CustomerModel::findOrfail($id)->delete();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function groupcreate(Request $request)
    {
        $nextnumber = DB::table('tbl_group_customer')->selectRaw(DB::raw('MAX("IDGroupCustomer") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDGroupCustomer'           => $urutan_id,
            'Kode_Group_Customer'       => $request->kode_group,
            'Nama_Group_Customer'       => $request->groupcustomer,
            'Aktif'                     => 'aktif',
        ];

        $createheader=DB::table('tbl_group_customer')->insert($header);

        return redirect()->action('CustomerController@tambah_customer');
    }

    public function kotacreate(Request $request)
    {
        $nextnumber = DB::table('tbl_kota')->selectRaw(DB::raw('MAX("IDKota") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDKota'            => $urutan_id,
            'Kode_Kota'         => $request->kode,
            'Provinsi'          => $request->provinsi,
            'Kota'              => $request->kota,
            'Aktif'             => 'Aktif',
        ];

        $createheader=DB::table('tbl_kota')->insert($header);

        return redirect()->action('CustomerController@tambah_customer');
    }

}