<?php
/*=====Create DEDY @27/01/2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\PurchaseOrderModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PersetujuanPembelianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $po                 = PurchaseOrderModel::Getindex()->get();


        return view('persetujuanpembelian/index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'po'));
    }

    public function datatable(Request $request) {
        $data = PurchaseOrderModel::Getindex();

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_purchase_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_purchase_order.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_purchase_order.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function show(Request $request)
    {
      $iduser             = Auth::user()->id;
      $namauser           = Auth::user()->name;
      $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
      $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
      $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
      $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
      $po                 = DB::table('tbl_purchase_order')
                                ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang','=','tbl_purchase_order.IDMataUang')
                                ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_purchase_order.IDSupplier')
                                ->where('tbl_purchase_order.IDPO', $request->id)
                                ->select('tbl_purchase_order.*',  'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang')
                                ->first();
      $podetail           = DB::table('tbl_purchase_order_detail')
                                  ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
                                  ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')
                                  ->where('tbl_purchase_order_detail.IDPO', $request->id)
                                  ->select('tbl_purchase_order_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang')
                                  
                                  ->get();
      $um                 = DB::table('tbl_um_supplier')
                                  ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor','=','tbl_um_supplier.Nomor_Faktur')
                                  ->join('tbl_coa', 'tbl_coa.IDCoa','=','tbl_um_supplier.IDCOA')
                                  ->where('tbl_purchase_order.IDPO', $request->id)
                                  ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
                                  ->first();
      $totalqtypo         = DB::table('tbl_purchase_order_detail')
                                ->select(DB::raw('SUM("Qty") as totqty'))
                                ->where('IDPO', '=', $request->id)
                                ->first();
      
      return view('persetujuanpembelian/show', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'po', 'podetail', 'um', 'totalqtypo'));
    }

    public function approve(Request $request)
    {
        $idpo = $request->id;
        $dataupdate = [
            'approve' => 'Disetujui'
        ];
        PurchaseOrderModel::where('IDPO', '=', $idpo)->update($dataupdate);

        return redirect('PersetujuanPesanan')->with('alert', 'Data Berhasil Disimpan');
    }

    public function reject(Request $request)
    {
        $idpo = $request->id;
        $dataupdate = [
            'approve' => 'Ditolak'
        ];
        PurchaseOrderModel::where('IDPO', '=', $idpo)->update($dataupdate);

        return redirect('PersetujuanPesanan')->with('alert', 'Data Berhasil Disimpan');
    }
    

}
