<?php
/*=====Create DEDY @10/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\MataUangModel;

class MatauangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $matauang           = DB::table('tbl_mata_uang')
                                ->get();

        return view('matauang/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser', 'matauang'));
    }

    public function datatable(Request $request) {
      $data = MataUangModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function tambah_data(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Matauang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $kode				= DB::table('tbl_mata_uang')->selectRaw(DB::raw('MAX(SUBSTRING("Kode", 4, 4)) as curr_number'))->first();
       	$matauang 			= DB::table('tbl_mata_uang')->get();
       	$idr 				= DB::table('vmatauang')->get();

        return view('matauang/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset', 'namauser', 'matauang', 'kode', 'idr'));
    }

    public function simpandata(Request $request)
    {
        $data = $request->get('data1');

        $nextnumber = DB::table('tbl_mata_uang')->selectRaw(DB::raw('MAX("IDMataUang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $datenow = date('Y-m-d');
        $cekdata = DB::table('tbl_mata_uang')->selectRaw(DB::raw('"IDMataUang"'))
        				->where('Mata_uang', 'like', ' \'%'.$data['Idmatauang'].'%\' ')
        				->where('Tanggal','=','\''.$datenow.'\'')
        				->first(); 
		    
		    if($cekdata['IDMataUang']!=''){
		    echo "
		        <script>
		          swal('Perhatian!', 'Data Sudah Ada', 'warning');
		        </script>
		    "; 
		      
		    }else{

		        $header = [
		            'IDMataUang'            => $urutan_id,
		            'Kode'    				=> '$',
		            'Mata_uang'             => $data['Idmatauang'],
		            'Negara'         		=> $data['Idmatauang'],
		            'Kurs'             		=> $data['Kurs'],
		            'Aktif'					=> 'aktif',
		            'Tanggal'				=> $data['Tanggal'],
		        ];

		        $createheader=DB::table('tbl_mata_uang')->insert($header);		        
        	
		       	$non = 'non aktif';
		        DB::update('UPDATE tbl_mata_uang SET "Aktif"=\''.$non.'\' WHERE "Mata_uang" ILIKE \'%'.$data['Idmatauang'].'%\' AND "Tanggal"<\''.$data['Tanggal'].'\'');

		        return response()->json($createheader); 
        	}
    }

    public function edit_data(Request $request)
    {
        
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Matauang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
       	$editing 			= DB::table('tbl_mata_uang')->where('IDMataUang','=', $request->id)->first();
       	$idr 				= DB::table('vmatauang')->get();

        return view('matauang/edit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset', 'namauser', 'idr', 'editing'));
    }

    public function edit_matauang(Request $request)
    {
        $data = $request->get('data1');
        $header = [
		    'Mata_uang'             => $data['Idmatauang'],
		    'Negara'         		=> $data['Idmatauang'],
		    'Kurs'             		=> $data['Kurs'],
		    'Aktif'					=> 'aktif',
		    'Tanggal'				=> $data['Tanggal'],
		];
        $createheader=DB::table('tbl_mata_uang')
                            ->where('IDMataUang', $data['Kodematauang'])
                            ->update($header);

        return response()->json($createheader); 

    }

    public function editstatus(Request $request)
    {
      $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if(! $akses){
          $data = [
              'status'    => false,
              'message'   => 'Anda Tidak Memiliki Akses'
          ];
          return json_encode($data);
      }

      $data_exist  = MataUangModel::findOrfail($id);

      $data_exist->Aktif = 'non aktif';
      
      $data_exist->save();
      
      $data = array (
          'status'    => true,
          'message'   => 'Status berhasil diubah.'
      );

      return json_encode($data);
    }

    public function pencarian(Request $request)
    {
        $keyword = $request->get('data');
        
        if ($keyword!="") {                             
            $datapencarian = DB::select('SELECT tbl_mata_uang.* FROM tbl_mata_uang
                                        WHERE tbl_mata_uang."Mata_uang" ILIKE \''.$keyword.'\'
                                        ');                                
            return response()->json($datapencarian);                 
                            
        }else {
            $datapencarian = DB::select('SELECT tbl_mata_uang.* FROM tbl_mata_uang
                                        ');
            return response()->json($datapencarian); 
        }
    }

    public function addidr(Request $request)
    {
    	$nextnumber =  DB::table('tbl_idr')->selectRaw(DB::raw('MAX("IDMU") as nonext'))->first();
 
	    if($nextnumber->nonext==''){
	      $urutan_id = 'P000001';
	    }else{
	      $hasil = substr($nextnumber->nonext,2,6) + 1;
	      $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
	    }

	    $cekidr = DB::table('tbl_idr')->selectRaw(DB::raw('"IDMU", "Nama"'))
        				->where('Nama', 'like', ' \'%'.$request->get('namaid').'%\' ')
        				->first(); 
	    if($cekidr['IDMU']==''){
	      $dataidr = array(
	        'IDMU'  => $urutan_id,
	        'Nama'  => $request->get('namaid')
	      );
	      DB::table('tbl_idr')->insert($dataidr);
	      
	    }else{
	      $dataidr = array(
	        'Nama'  => $request->get('namaid')
	      );
	      DB::table('tbl_idr')
                            ->where('IDMU', $cekidr['IDMU'])
                            ->update($dataidr);
	      
	    }

	    return redirect()->action('MatauangController@index');
    }


}
