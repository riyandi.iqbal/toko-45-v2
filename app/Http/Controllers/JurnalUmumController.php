<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

// HELPERS //
use App\Helpers\AppHelper;

use App\Models\CoaModel;
use App\Models\MataUangModel;
use App\Models\JurnalUmumModel;
use App\Models\JurnalUmumDetailModel;
use App\Models\VListJurnalUmumDetailModel;
use App\Models\JurnalModel;

class JurnalUmumController extends Controller {
    
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('jurnalumum.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = JurnalUmumModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $COA = DB::table('tbl_group_coa')->get();
        foreach ($COA as $key => $value) {
            $value->data = CoaModel::where('IDGroupCOA', $value->IDGroupCOA)->get();
        }
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();

        return view('jurnalumum.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'COA', 'mata_uang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Nomor'             => 'required',
            'Total_debit'       => 'required',
            'Total_kredit'      => 'required',
            'KodePerkiraan'     => 'required',
            'Posisi_header'     => 'required',
            'data_detail.IDCOA.*' => 'required',
            'data_detail.Debet.*' => 'required',
            'data_detail.Kredit.*'=> 'required',
            'data_detail.Posisi.*'=> 'required',
            'data_detail.Keterangan.*' => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'Tanggal'           => 'Tanggal',
            'Total_debit'       => 'Total Debit',
            'Total_kredit'      => 'Total Kredit',
            'KodePerkiraan'     => 'Kode Perkiraan',
            'Posisi_header'     => 'Posisi',
            'data_detail.IDCOA.*' => 'IDCOA',
            'data_detail.Debet.*' => 'Debet',
            'data_detail.Kredit.*'=> 'Kredit',
            'data_detail.Posisi.*'=> 'Posisi',
            'data_detail.Keterangan.*' => 'Keterangan',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDJU = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor);

        DB::beginTransaction();

        $JurnalUmum = new JurnalUmumModel();

        $JurnalUmum->IDJU   = $IDJU;
        $JurnalUmum->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $JurnalUmum->Nomor  = $Nomor;
        $JurnalUmum->Total_debit    = $request->Total_debit;
        $JurnalUmum->Total_kredit   = $request->Total_kredit;
        $JurnalUmum->dibuat_pada    = date('Y-m-d H:i:s');
        $JurnalUmum->dibuat_oleh    = Auth::user()->id;
        $JurnalUmum->diubah_pada    = date('Y-m-d H:i:s');
        $JurnalUmum->diubah_oleh    = Auth::user()->id;

        $JurnalUmum->save();

        $data_detail = json_decode($request->data_detail);
        $no = 0;

        foreach ($data_detail as $key => $value) {
            if ($request->Posisi_header == 'debet') {
                $IDJUDetail       = uniqid();
                $JurnalUmumDetail = new JurnalUmumDetailModel();
    
                $JurnalUmumDetail->IDJUDetail   = $IDJUDetail;
                $JurnalUmumDetail->IDJU         = $IDJU;
                $JurnalUmumDetail->IDCOA        = $request->KodePerkiraan;
                $JurnalUmumDetail->Posisi       = $request->Posisi_header;
                $JurnalUmumDetail->Debet        = AppHelper::StrReplace($value->Kredit);
                $JurnalUmumDetail->Kredit       = AppHelper::StrReplace($value->Debet);
                $JurnalUmumDetail->IDMataUang   = $value->IDMataUang;
                $JurnalUmumDetail->Kurs         = $value->Kurs;
                $JurnalUmumDetail->Keterangan   = $value->Keterangan;
                $JurnalUmumDetail->Urutan       = ++$no;
    
                $JurnalUmumDetail->save();

                // SAVE JURNAL //
                $JurnalDebet = new JurnalModel;

                $JurnalDebet->IDJurnal     = $this->number_jurnal();
                $JurnalDebet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
                $JurnalDebet->Nomor        = $Nomor;
                $JurnalDebet->IDFaktur     = $IDJU;
                $JurnalDebet->IDFakturDetail   = $IDJUDetail;
                $JurnalDebet->Jenis_faktur = 'JU';
                $JurnalDebet->IDCOA        = $request->KodePerkiraan;
                $JurnalDebet->Debet        = AppHelper::StrReplace($value->Kredit);
                $JurnalDebet->Kredit       = AppHelper::StrReplace($value->Debet);
                $JurnalDebet->IDMataUang       = $value->IDMataUang;
                $JurnalDebet->Kurs             = $value->Kurs;
                $JurnalDebet->Total_debet      = AppHelper::StrReplace($value->Kredit);
                $JurnalDebet->Total_kredit     = AppHelper::StrReplace($value->Debet);
                $JurnalDebet->Keterangan       = $value->Keterangan;
                $JurnalDebet->Saldo            = AppHelper::StrReplace($value->Kredit);

                $JurnalDebet->save();
                // END JURNAL //
            }
            
            $IDJUDetail       = uniqid();
            $JurnalUmumDetail = new JurnalUmumDetailModel();

            $JurnalUmumDetail->IDJUDetail   = $IDJUDetail;
            $JurnalUmumDetail->IDJU         = $IDJU;
            $JurnalUmumDetail->IDCOA        = $value->IDCOA;
            $JurnalUmumDetail->Posisi       = $value->Posisi;
            $JurnalUmumDetail->Debet        = AppHelper::StrReplace($value->Debet);
            $JurnalUmumDetail->Kredit       = AppHelper::StrReplace($value->Kredit);
            $JurnalUmumDetail->IDMataUang   = $value->IDMataUang;
            $JurnalUmumDetail->Kurs         = $value->Kurs;
            $JurnalUmumDetail->Keterangan   = $value->Keterangan;
            $JurnalUmumDetail->Urutan       = ++$no;

            $JurnalUmumDetail->save();

            // SAVE JURNAL //
            $JurnalDebet = new JurnalModel;

            $JurnalDebet->IDJurnal     = $this->number_jurnal();
            $JurnalDebet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $JurnalDebet->Nomor        = $Nomor;
            $JurnalDebet->IDFaktur     = $IDJU;
            $JurnalDebet->IDFakturDetail   = $IDJUDetail;
            $JurnalDebet->Jenis_faktur = 'JU';
            $JurnalDebet->IDCOA        = $value->IDCOA;
            $JurnalDebet->Debet        = AppHelper::StrReplace($value->Debet);
            $JurnalDebet->Kredit       = AppHelper::StrReplace($value->Kredit);
            $JurnalDebet->IDMataUang       = $value->IDMataUang;
            $JurnalDebet->Kurs             = $value->Kurs;
            $JurnalDebet->Total_debet      = AppHelper::StrReplace($value->Debet);
            $JurnalDebet->Total_kredit     = AppHelper::StrReplace($value->Kredit);
            $JurnalDebet->Keterangan       = $value->Keterangan;
            $JurnalDebet->Saldo            = ($value->Posisi == 'debet') ? AppHelper::StrReplace($value->Debet) : AppHelper::StrReplace($value->Kredit);

            $JurnalDebet->save();
            // END JURNAL //

            if ($request->Posisi_header == 'kredit') {
                $IDJUDetail       = uniqid();
                $JurnalUmumDetail = new JurnalUmumDetailModel();
    
                $JurnalUmumDetail->IDJUDetail   = $IDJUDetail;
                $JurnalUmumDetail->IDJU         = $IDJU;
                $JurnalUmumDetail->IDCOA        = $request->KodePerkiraan;
                $JurnalUmumDetail->Posisi       = $request->Posisi_header;
                $JurnalUmumDetail->Debet        = AppHelper::StrReplace($value->Kredit);
                $JurnalUmumDetail->Kredit       = AppHelper::StrReplace($value->Debet);
                $JurnalUmumDetail->IDMataUang   = $value->IDMataUang;
                $JurnalUmumDetail->Kurs         = $value->Kurs;
                $JurnalUmumDetail->Keterangan   = $value->Keterangan;
                $JurnalUmumDetail->Urutan       = ++$no;
    
                $JurnalUmumDetail->save();

                // SAVE JURNAL //
                $JurnalDebet = new JurnalModel;

                $JurnalDebet->IDJurnal     = $this->number_jurnal();
                $JurnalDebet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
                $JurnalDebet->Nomor        = $Nomor;
                $JurnalDebet->IDFaktur     = $IDJU;
                $JurnalDebet->IDFakturDetail   = $IDJUDetail;
                $JurnalDebet->Jenis_faktur = 'JU';
                $JurnalDebet->IDCOA        = $request->KodePerkiraan;
                $JurnalDebet->Debet        = AppHelper::StrReplace($value->Kredit);
                $JurnalDebet->Kredit       = AppHelper::StrReplace($value->Debet);
                $JurnalDebet->IDMataUang       = $value->IDMataUang;
                $JurnalDebet->Kurs             = $value->Kurs;
                $JurnalDebet->Total_debet      = AppHelper::StrReplace($value->Kredit);
                $JurnalDebet->Total_kredit     = AppHelper::StrReplace($value->Debet);
                $JurnalDebet->Keterangan       = $value->Keterangan;
                $JurnalDebet->Saldo            = AppHelper::StrReplace($value->Debet);

                $JurnalDebet->save();
                // END JURNAL //
            }
        }
        
        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'id'        => $IDJU
            ]
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $COA = DB::table('tbl_group_coa')->get();
        foreach ($COA as $key => $value) {
            $value->data = CoaModel::where('IDGroupCOA', $value->IDGroupCOA)->get();
        }
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();


        $jurnal_umum = JurnalUmumModel::findOrfail($id);

        $jurnal_umum_detail = VListJurnalUmumDetailModel::where('IDJU', $jurnal_umum->IDJU)->orderBy('Urutan', 'ASC')->get();

        return view('jurnalumum.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'COA', 'mata_uang', 'jurnal_umum', 'jurnal_umum_detail'));
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $COA = DB::table('tbl_group_coa')->get();
        foreach ($COA as $key => $value) {
            $value->data = CoaModel::where('IDGroupCOA', $value->IDGroupCOA)->get();
        }
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();


        $jurnal_umum = JurnalUmumModel::findOrfail($id);

        $jurnal_umum_detail = VListJurnalUmumDetailModel::where('IDJU', $jurnal_umum->IDJU)->orderBy('Urutan', 'ASC')->get();

        return view('jurnalumum.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'COA', 'mata_uang', 'jurnal_umum', 'jurnal_umum_detail'));
    }

    public function destroy($id) {
        DB::beginTransaction();

        $jurnal_umum = JurnalUmumModel::findOrfail($id);

        if ($jurnal_umum->Batal == 1) {
            $status = false;
            $message = 'Data tidak bisa diaktifkan.';
        } else {
            $jurnal_umum->Batal = 1;

            $jurnal_umum->save();
            $message = 'Data berhasil dibatalkan.';
        }

        DB::commit();

        $data = array (
            'status'    => (isset($status)) ? $status : true ,
            'message'   => $message
        );

        return json_encode($data);
    }

    public function number_jurnal_umum(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = JurnalUmumModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/JU/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    private function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    private function _cek_nomor($nomor) {
        $nomor_exist = JurnalUmumModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number();
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number(){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = JurnalUmumModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/JU/' . $bulan_romawi . '/' . date('y');
        
        return $kode;
    }
}
