<?php
/*=====Create DEDY @31/01/2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\PemakaianBarangModel;
use App\Models\UserModel;
use App\Models\PerusahaanModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PemakaianBarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pakai              = PemakaianBarangModel::Getforindex()->get();

        return view('pemakaianbarang/index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pakai'));
    }

    public function datatable(Request $request) {
        $data = PemakaianBarangModel::Getforindex();

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function pencarian(Request $request)
    {
        $tanggalawal = $request->get('date_from');
        $tanggalakhir = $request->get('date_until');
        $keyword = $request->get('keyword');
        if($keyword!=''){
            $datacari = DB::select('SELECT a.*, b."name" 
                                    FROM tbl_pemakaian_barang a, users b
                                    WHERE a."CID"=b."id"
                                    AND a."Tanggal" 
                                    BETWEEN \''.$tanggalawal.'\' 
                                    AND \''.$tanggalakhir.'\' 
                                    AND b."Nama" LIKE \'%'.$keyword.'%\'
                                    ');
        
            return response()->json($datacari);
        }else{
            $datacari = DB::select('SELECT a.*, b."name" 
                                    FROM tbl_pemakaian_barang a, users b
                                    WHERE a."CID"=b."id"
                                    AND a."Tanggal" 
                                    BETWEEN \''.$tanggalawal.'\' 
                                    AND \''.$tanggalakhir.'\'
                                    ');
            return response()->json($datacari);
        }
        
    }

    public function create(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;

        $tahun              = date('Y'); 
        $aktif              = 'aktif';

        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $user              = UserModel::where('id', '=', Auth::user()->id)->first();
        
        $kode              = DB::table('tbl_pemakaian_barang')
                                ->whereYear('Tanggal', $tahun)
                                ->select(DB::raw('count("IDPakai") as curr_number'))
                                ->first();
        $barang            = DB::table('tbl_barang')->where('Aktif', $aktif)->orderBy('Nama_Barang', 'asc')->get();
        $satuan            = DB::table('tbl_satuan')->get();
        $gudang            = DB::table('tbl_gudang')->get();

        return view('pemakaianbarang/create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'user', 'kode', 'barang', 'satuan', 'gudang'));
    }

    public function simpandata(Request $request)
    {
        $tanggal        = $request->get('Tanggal');
        $nomor          = $request->get('Nomor');
        $userid         = $request->get('userid');
        $keterangan     = $request->get('Keterangan');
        $gudang         = $request->get('IDGudang');
        
        $brg            = $request->get('barang_array');
        $qty            = $request->get('qty_array');
        $stn            = $request->get('satuan_array');
        $kodestn        = $request->get('kode_satuan_array');

        $jml            = count($brg);
        
        DB::beginTransaction();

        $IDPakai = uniqid();
        $dataheader = [
            'IDPakai'       => $IDPakai,
            'Keterangan'    => $keterangan,
            'CID'           => $userid,
            'CTime'         => date('Y-m-d H:i:s'),
            'Tanggal'       => $tanggal,
            'Nomor'         => $nomor,
            'IDGudang'      => $gudang,
            'Status'        => 'aktif',
            'Delete'        => 'f'
        ];
        DB::table('tbl_pemakaian_barang')->insert($dataheader);
        
        for ($x = 0; $x < $jml; ++$x) {
            $IDPakaiDetail = uniqid();
            $datadetail = array(
                'IDPakaiDetail'     => $IDPakaiDetail,
                'IDPakai'           => $IDPakai,
                'IDBarang'          => $brg[$x],
                'Qty'               => $qty[$x],
                'IDSatuan'          => $kodestn[$x],
            );
            DB::table('tbl_pemakaian_barang_detail')->insert($datadetail);

            $nextnumber3 = DB::table('tbl_stok')->selectRaw(DB::raw('MAX("IDStok") as nonext'))->first();
            if($nextnumber3->nonext==''){
            $urutan_id3 = 'P000001';
            }else{
            $hasil3 = substr($nextnumber3->nonext,2,6) + 1;
            $urutan_id3 = 'P'.str_pad($hasil3, 6, 0, STR_PAD_LEFT);
            }

            //==================================Nomor Table Kartu Stok
            $nextnumber4 = DB::table('tbl_kartu_stok')->selectRaw(DB::raw('MAX("IDKartuStok") as nonext'))->first();
            if($nextnumber4->nonext==''){
            $urutan_id4 = 'P000001';
            }else{
            $hasil4 = substr($nextnumber4->nonext,2,6) + 1;
            $urutan_id4 = 'P'.str_pad($hasil4, 6, 0, STR_PAD_LEFT);
            }

            $cekdatastok = DB::table('tbl_stok')->select('*')->where('IDBarang', $brg[$x])->where('IDGudang', $gudang)->first();

            if($cekdatastok!=''){
                $qtyupdate = $cekdatastok->Qty_pcs - $qty[$x];
                $datastok = [
                    'Tanggal'       => date('Y-m-d H:i:s'),
                    'Nomor_faktur'  => $nomor,
                    'Qty_pcs'       => $qtyupdate,
                    'Nama_Barang'   => $brg[$x],
                    'Jenis_faktur'  => 'PBJ',
                    'IDGudang'      => $gudangid,
                    'IDSatuan'      => $kodestn[$x]
                ];
                DB::table('tbl_stok')->where('IDStok', $cekdatastok->IDStok)->update($datastok);

                $dataks = [
                    'IDKartuStok'   => $urutan_id4,
                    'Nomor_faktur'  => $nomor,
                    'IDStok'        => $cekdatastok->IDStok,
                    'IDBarang'      => $brg[$x],
                    'Keluar_pcs'     => $qty[$x],
                    'IDSatuan'      => $kodestn[$x],
                    'Tanggal'       => date('Y-m-d'),
                ];
                DB::table('tbl_kartu_stok')->insert($dataks);

            }else{
                $datastok = [
                    'IDStok'        => $urutan_id3,
                    'Tanggal'       => date('Y-m-d H:i:s'),
                    'Nomor_faktur'  => $nomor,
                    'IDBarang'      => $brg[$x],
                    'Qty_pcs'       => ($qty[$x] * -1),
                    'Nama_Barang'   => $brg[$x],
                    'Jenis_faktur'  => 'PBJ',
                    'IDGudang'      => $gudang,
                    'IDSatuan'      => $kodestn[$x]
                ]; 
                DB::table('tbl_stok')->insert($datastok);

                $dataks = [
                    'IDKartuStok'   => $urutan_id4,
                    'IDStok'        => $urutan_id3,
                    'Nomor_faktur'  => $nomor,
                    'IDBarang'      => $brg[$x],
                    'Keluar_pcs'     => $qty[$x],
                    'IDSatuan'      => $kodestn[$x],
                    'Tanggal'       => date('Y-m-d'),
                ];

                DB::table('tbl_kartu_stok')->insert($dataks);
            }
        }
        
        DB::commit();
        return redirect('PemakaianBarang')->with('alert', 'Data Berhasil Disimpan');
        
    }

    public function showdata(Request $request)
    {
        $idshow = $request->id;
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pakaiheader              = PemakaianBarangModel::Getforshowheader()->where('IDPakai', '=', $idshow)->first();
        $pakaidetail              = PemakaianBarangModel::Getforshow()->where('tbl_pemakaian_barang.IDPakai', '=', $idshow)->get();
        return view('pemakaianbarang/show', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pakaiheader', 'pakaidetail'));
    }

    public function printdata(Request $request)
    {
        $idshow = $request->id;
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $perusahaan          = PerusahaanModel::Getforprint();
        $pakaiheader              = PemakaianBarangModel::Getforshowheader()->where('IDPakai', '=', $idshow)->first();
        $pakaidetail              = PemakaianBarangModel::Getforshow()->where('tbl_pemakaian_barang.IDPakai', '=', $idshow)->get();
        return view('pemakaianbarang/printdata', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pakaiheader', 'pakaidetail', 'perusahaan'));
    }

    public function hapusdata(Request $request)
    {
        $dataheader = [
            'DID'           => Auth::user()->id,
            'DTime'         => date('Y-m-d H:i:s'),
            'Delete'        => 't',
        ];
        DB::table('tbl_pemakaian_barang')
                ->where('IDPakai', '=', $request->id)
                ->update($dataheader);
        
        $response = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($response);
    }

}
