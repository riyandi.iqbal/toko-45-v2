<?php
/*=====Create TIAR @14/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\UkuranModel;

class UkuranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('ukuran.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = UkuranModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Ukuran')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $kode               = UkuranModel::selectRaw(DB::raw('MAX(SUBSTRING("Kode_Ukuran", 4, 4)) as curr_number'))->first();
        
        return view('ukuran.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'kode'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Kode_Ukuran'    => 'required|unique:tbl_ukuran,Kode_Ukuran',
            'Nama_Ukuran'             => 'required',
        ])->setAttributeNames([
            'Kode_Ukuran'    => 'Kode',
            'Nama_Ukuran'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = UkuranModel::selectRaw(DB::raw('MAX("IDUkuran") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new UkuranModel;

        $data->IDUkuran            = $urutan_id;
        $data->Kode_Ukuran         = $request->Kode_Ukuran;
        $data->Nama_Ukuran         = $request->Nama_Ukuran;
        $data->Status            = $request->Status;
        $data->CID               = Auth::user()->id;
        $data->CTime             = date('Y-m-d H:i:s');
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Ukuran')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $ukuran               = UkuranModel::findOrfail($id);

        return view('ukuran.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'ukuran'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDUkuran'  => 'required',
            'Kode_Ukuran'    => 'required',
            'Nama_Ukuran'             => 'required',
        ])->setAttributeNames([
            'IDUkuran'  => 'Data',
            'Kode_Ukuran'    => 'Kode',
            'Nama_Ukuran'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = UkuranModel::findOrfail($request->IDUkuran);

        $data->Kode_Ukuran         = $request->Kode_Ukuran;
        $data->Nama_Ukuran         = $request->Nama_Ukuran;
        $data->Status                = $request->Status;
        $data->MID               = Auth::user()->id;
        $data->MTime             = date('Y-m-d H:i:s');
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = UkuranModel::findOrfail($id);

        if ($data_exist->Status == 'aktif') {
            $data_exist->Status = 'nonaktif';
        } else {
            $data_exist->Status = 'aktif';
        }
        
        $data_exist->save();
        
        $data = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($data);
    }
}