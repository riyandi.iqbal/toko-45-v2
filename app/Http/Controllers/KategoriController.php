<?php
/*=====Create TIAR @13/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\KategoriModel;
use App\Models\GroupBarangModel;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('kategori.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = KategoriModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Kategori')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $kode               = KategoriModel::selectRaw(DB::raw('MAX(SUBSTRING("Kode_Kategori", 4, 4)) as curr_number'))->first();  
        $groupbarang        = GroupBarangModel::orderBy('Group_Barang', 'asc')->get();
        
        return view('kategori.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'kode', 'groupbarang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Kode_Kategori'    => 'required|unique:tbl_kategori,Kode_Kategori',
            'IDGroupBarang'             => 'required',
            'Nama_Kategori'             => 'required',
        ])->setAttributeNames([
            'Kode_Kategori'    => 'Kode',
            'IDGroupBarang'             => 'Grup Barang',
            'Nama_Kategori'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = KategoriModel::selectRaw(DB::raw('MAX("IDKategori") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new KategoriModel;

        $data->IDKategori            = $urutan_id;
        $data->Kode_Kategori         = $request->Kode_Kategori;
        $data->IDGroupBarang         = $request->IDGroupBarang;
        $data->Nama_Kategori         = $request->Nama_Kategori;
        $data->Status                 = $request->Status;
        $data->CID                 = Auth::user()->id;
        $data->CTime               = date('Y-m-d H:i:s');
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Kategori')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $kategori               = KategoriModel::findOrfail($id);

        $groupbarang        = GroupBarangModel::orderBy('Group_Barang', 'asc')->get();

        return view('kategori.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'kategori', 'groupbarang'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDKategori'  => 'required',
            'Kode_Kategori'    => 'required',
            'Nama_Kategori'             => 'required',
        ])->setAttributeNames([
            'IDKategori'  => 'Data',
            'Kode_Kategori'    => 'Kode',
            'Nama_Kategori'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = KategoriModel::findOrfail($request->IDKategori);

        $data->IDGroupBarang         = $request->IDGroupBarang;
        $data->Kode_Kategori         = $request->Kode_Kategori;
        $data->Nama_Kategori         = $request->Nama_Kategori;
        $data->Status                = $request->Status;
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = KategoriModel::findOrfail($id);

        if ($data_exist->Status == 'aktif') {
            $data_exist->Status = 'nonaktif';
        } else {
            $data_exist->Status = 'aktif';
        }
        
        $data_exist->save();
        
        $data = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($data);
    }
}