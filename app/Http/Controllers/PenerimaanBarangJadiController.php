<?php
/*=====Create DEDY @06/01/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\PenerimaanBarangJadiModel;
use App\Models\PenerimaanBarangJadiDetailModel;
use App\Models\BarangModel;
use App\Models\SatuanModel;
use App\Models\KartustokModel;
use App\Models\PerusahaanModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PenerimaanBarangJadiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pbj                = PenerimaanBarangJadiModel::get();
        return view('barangjadi/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'pbj'));
    }

    public function datatable(Request $request) {
        $data = PenerimaanBarangJadiModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $barang             = BarangModel::get();
        $satuan             = SatuanModel::get();
        $kode               = PenerimaanBarangJadiModel::whereYear('Tanggal', $tahun)
                                ->select(DB::raw('MAX(SUBSTRING("Nomor", 0, 5)) as curr_number'))
                                ->first();
        return view('barangjadi/create', compact('aksesmenu', 'aksesmenudetail', 
                                                'aksessetting', 'namauser',
                                                'barang', 'satuan', 'kode', 'coreset'
                                            ));
    }

    public function ambildetail(Request $request)
    {
        $idbrg          = $request->get('data');
        $caridata       = BarangModel::Getdetail()->where('tbl_barang.IDBarang', '=', $idbrg)->first();

        return response()->json($caridata);
    }

    public function simpandata(Request $request)
    {
        $tanggal        = $request->get('Tanggal');
        $nomor          = $request->get('Nomor');
        $gudangid       = $request->get('gudang');
        $keterangan     = $request->get('Keterangan');

        $brg            = $request->get('barang_array');
        $tipe           = $request->get('kode_tipe_array');
        $ukuran         = $request->get('kode_ukuran_array');
        $berat          = $request->get('berat_array');
        $qty            = $request->get('qty_array');
        $satuan         = $request->get('kode_satuan_array');
        $totalin        = $request->get('total_array');

        DB::beginTransaction();

        //===========simpan header
        $nextnumber =  PenerimaanBarangJadiModel::select(DB::raw('MAX("IDPBJ") as nonext'))->first(); 

        if($nextnumber['nonext']==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber['nonext'],2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        //===========validasi nomor
        $ceknopbj =  PenerimaanBarangJadiModel::where('Nomor', $nomor)->first();    
        if($ceknopbj==''){
          $nosimpan = $nomor;
        }else{
          $hasilkeluaran = substr($ceknopbj->Nomor,2,4) + 1;
          $cekagain = substr($nomor, 0, 2);
          $ambilsetelahpenambahan = substr($nomor, 6);
          $nosimpan = $cekagain.str_pad($hasilkeluaran, 4, 0, STR_PAD_LEFT).$ambilsetelahpenambahan;
        }

        $header = new PenerimaanBarangJadiModel;
        $header->IDPBJ      = $urutan_id;
        $header->Tanggal    = $tanggal;
        $header->Nomor      = $nosimpan;
        $header->Keterangan = $keterangan;
        $header->IDGudang   = $gudangid;
        $header->CID        = Auth::user()->id;
        $header->CTime      = date('Y/m/d H:i:s');
        $header->Status     = 'Aktif';
        $header->save();

        //==============simpan detail
        $jml_data = count($brg);

        for ($x = 0; $x < $jml_data; ++$x) {
            $nextnumber2 =  PenerimaanBarangJadiDetailModel::select(DB::raw('MAX("IDPBJDetail") as nonext'))->first(); 

            if($nextnumber2['nonext']==''){
            $urutan_id2 = 'P000001';
            }else{
            $hasil2 = substr($nextnumber2['nonext'],2,6) + 1;
            $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $detail = new PenerimaanBarangJadiDetailModel;
            $detail->IDPBJDetail        = $urutan_id2;
            $detail->IDPBJ              = $urutan_id;
            $detail->IDBarang           = $brg[$x];
            $detail->IDTipe             = $tipe[$x];
            $detail->IDUkuran           = $ukuran[$x];
            $detail->IDSatuan           = $satuan[$x];
            $detail->Berat              = $berat[$x];
            $detail->Qty                = $qty[$x];
            $detail->Total              = $qty[$x]*$berat[$x];
            $detail->save();

            //==================================Nomor Table Stok
            $nextnumber3 = DB::table('tbl_stok')->selectRaw(DB::raw('MAX("IDStok") as nonext'))->first();
            if($nextnumber3->nonext==''){
            $urutan_id3 = 'P000001';
            }else{
            $hasil3 = substr($nextnumber3->nonext,2,6) + 1;
            $urutan_id3 = 'P'.str_pad($hasil3, 6, 0, STR_PAD_LEFT);
            }

            //==================================Nomor Table Kartu Stok
            $nextnumber4 = DB::table('tbl_kartu_stok')->selectRaw(DB::raw('MAX("IDKartuStok") as nonext'))->first();
            if($nextnumber4->nonext==''){
            $urutan_id4 = 'P000001';
            }else{
            $hasil4 = substr($nextnumber4->nonext,2,6) + 1;
            $urutan_id4 = 'P'.str_pad($hasil4, 6, 0, STR_PAD_LEFT);
            }

            $cekdatastok = DB::table('tbl_stok')->select('*')->where('IDBarang', $brg[$x])->where('IDGudang', $gudangid)->first();

            if($cekdatastok!=''){
                $qtyupdate = $cekdatastok->Qty_pcs + ($qty[$x]*$berat[$x]);
                $datastok = [
                    'Tanggal'       => date('Y-m-d H:i:s'),
                    'Nomor_faktur'  => $nosimpan,
                    'Qty_pcs'       => $qtyupdate,
                    'Nama_Barang'   => $brg[$x],
                    'Jenis_faktur'  => 'PBJ',
                    'IDGudang'      => $gudangid,
                    'IDSatuan'      => $satuan[$x]
                ];
                DB::table('tbl_stok')->where('IDStok', $cekdatastok->IDStok)->update($datastok);

                $dataks = [
                    'IDKartuStok'   => $urutan_id4,
                    'Nomor_faktur'  => $nosimpan,
                    'IDStok'        => $cekdatastok->IDStok,
                    'IDBarang'      => $brg[$x],
                    'Masuk_pcs'     => $qty[$x],
                    'IDSatuan'      => $satuan[$x],
                    'Tanggal'       => date('Y-m-d'),
                    'Berat'         => $berat[$x], 
                ];
                DB::table('tbl_kartu_stok')->insert($dataks);

            }else{
                $datastok = [
                    'IDStok'        => $urutan_id3,
                    'Tanggal'       => date('Y-m-d H:i:s'),
                    'Nomor_faktur'  => $nosimpan,
                    'IDBarang'      => $brg[$x],
                    'Qty_pcs'       => $qty[$x],
                    'Nama_Barang'   => $brg[$x],
                    'Jenis_faktur'  => 'PBJ',
                    'IDGudang'      => $gudangid,
                    'IDSatuan'      => $satuan[$x]
                ]; 
                DB::table('tbl_stok')->insert($datastok);

                $dataks = [
                    'IDKartuStok'   => $urutan_id4,
                    'IDStok'        => $urutan_id3,
                    'Nomor_faktur'  => $nosimpan,
                    'IDBarang'      => $brg[$x],
                    'Masuk_pcs'     => $qty[$x],
                    'IDSatuan'      => $satuan[$x],
                    'Tanggal'       => date('Y-m-d'),
                    'Berat'         => $berat[$x], 
                ];

                DB::table('tbl_kartu_stok')->insert($dataks);
            }
        }
        DB::commit();

        return redirect('BarangJadi')->with('alert', 'Data Berhasil Disimpan');
    }

    public function showdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $header             = PenerimaanBarangJadiModel::where('IDPBJ', '=', $request->id)->first();
        $detail             = PenerimaanBarangJadiDetailModel::Getdetail()->where('tbl_penerimaan_barang_jadi_detail.IDPBJ', '=', $request->id)->get();
        
        return view('barangjadi/showdata', compact('aksesmenu', 'aksesmenudetail', 
                                                'aksessetting', 'namauser', 'header', 'detail', 'coreset'
                                            ));
    }

    public function printdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $header             = PenerimaanBarangJadiModel::where('IDPBJ', '=', $request->id)->first();
        $detail             = PenerimaanBarangJadiDetailModel::Getdetail()->where('tbl_penerimaan_barang_jadi_detail.IDPBJ', '=', $request->id)->get();
        $perusahaan          = PerusahaanModel::Getforprint();
        return view('barangjadi/printdata', compact('aksesmenu', 'aksesmenudetail', 'coreset',
                                                'aksessetting', 'namauser', 'header', 'detail', 'perusahaan'
                                            ));
    }

    public function editdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $barang             = BarangModel::get();
        $satuan             = SatuanModel::get();
        $header             = PenerimaanBarangJadiModel::where('IDPBJ', '=', $request->id)->first();
        $detail             = PenerimaanBarangJadiDetailModel::Getdetail()->where('tbl_penerimaan_barang_jadi_detail.IDPBJ', '=', $request->id)->get();
                                
        return view('barangjadi/editdata', compact('aksesmenu', 'aksesmenudetail', 'coreset',
                                                'aksessetting', 'namauser', 'header',
                                                'barang', 'satuan', 'detail'
                                            ));
    }

    public function editdatasimpan(Request $request)
    {
        $tanggal        = $request->get('Tanggal');
        $nomor          = $request->get('Nomor');
        $gudangid       = $request->get('gudangid');
        $keterangan     = $request->get('Keterangan');
        $idpbj          = $request->get('IDPBJ');

        $brg            = $request->get('barang_array');
        $tipe           = $request->get('kode_tipe_array');
        $ukuran         = $request->get('kode_ukuran_array');
        $berat          = $request->get('berat_array');
        $qty            = $request->get('qty_array');
        $satuan         = $request->get('kode_satuan_array');
        $totalin        = $request->get('total_array');

        //===========simpan header
        $header = PenerimaanBarangJadiModel::findOrfail($idpbj);
        $header->Tanggal    = $tanggal;
        $header->Nomor      = $nomor;
        $header->Keterangan = $keterangan;
        $header->IDGudang   = $gudangid;
        $header->MID        = Auth::user()->id;
        $header->MTime      = date('Y/m/d H:i:s');
        $header->Status     = 'Aktif';
        $header->save();

        //==============simpan detail
        $jml_data = count($brg);
        for ($x = 0; $x < $jml_data; ++$x) {
            //tambahkan dulu stok yang dulu
            $ambildetailsebelumnya = DB::select('SELECT "Total" FROM tbl_penerimaan_barang_jadi_detail WHERE "IDPBJ"=\''.$idpbj.'\' AND "IDBarang"=\''.$brg[$x].'\'');

            $ambilstok = DB::select('SELECT "Qty_pcs" FROM tbl_stok WHERE "IDBarang"=\''.$brg[$x].'\' ');
    		$hasilplusstok = $ambilstok[0]->Qty_pcs+$ambildetailsebelumnya[0]->Total;
            
    		$datastok = [
    			'Qty_pcs'	=> $hasilplusstok,
            ];
            DB::table('tbl_stok')->where('IDBarang', '=', $brg[$x])->update($datastok);
            
            $nextnumber3 =  KartustokModel::select(DB::raw('MAX("IDKartuStok") as nonext'))->first(); 

            if($nextnumber3['nonext']==''){
            $urutan_id3 = 'P000001';
            }else{
            $hasil3 = substr($nextnumber3['nonext'],2,6) + 1;
            $urutan_id3 = 'P'.str_pad($hasil3, 6, 0, STR_PAD_LEFT);
            }
        }
        
        DB::table('tbl_penerimaan_barang_jadi_detail')->where('IDPBJ', '=', $idpbj)->delete();
        DB::table('tbl_kartu_stok')->where('Nomor_faktur', '=', $nomor)->delete();
        for($y=0; $y < $jml_data; ++$y)
        {
            //================insert detail plus pengurangan stok baru
            $nextnumber2 =  PenerimaanBarangJadiDetailModel::select(DB::raw('MAX("IDPBJDetail") as nonext'))->first(); 

            if($nextnumber2['nonext']==''){
            $urutan_id2 = 'P000001';
            }else{
            $hasil2 = substr($nextnumber2['nonext'],2,6) + 1;
            $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $detail = new PenerimaanBarangJadiDetailModel;
            $detail->IDPBJDetail        = $urutan_id2;
            $detail->IDPBJ              = $idpbj;
            $detail->IDBarang           = $brg[$y];
            $detail->IDTipe             = $tipe[$y];
            $detail->IDUkuran           = $ukuran[$y];
            $detail->IDSatuan           = $satuan[$y];
            $detail->Berat              = $berat[$y];
            $detail->Qty                = $qty[$y];
            $detail->Total              = $qty[$y]*$berat[$y];
            $detail->save();

            $ambilstok = DB::select('SELECT "Qty_pcs" FROM tbl_stok WHERE "IDBarang"=\''.$brg[$y].'\' ');
    		$hasilpenguranganstok = $ambilstok[0]->Qty_pcs-($qty[$y]*$berat[$y]);
            
    		$datastok = [
    			'Qty_pcs'	=> $hasilpenguranganstok,
            ];
            DB::table('tbl_stok')->where('IDBarang', '=', $brg[$y])->update($datastok);
    		//=============insert kartu stok
    		$dataks = [
		       		'IDKartuStok'	=> $urutan_id3,
		       		'Nomor_faktur'	=> $nomor,
		       		'IDBarang'		=> $brg[$y],
		       		'Keluar_pcs'	=> $qty[$y],
                       'IDSatuan'	=> $satuan[$y], 
                       'Tanggal'    => date('Y-m-d'),
                       'Berat'      => $berat[$y]
            ];
            DB::table('tbl_kartu_stok')->insert($dataks);
        }
        return redirect('BarangJadi')->with('alert', 'Data Berhasil Disimpan');
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses              = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        DB::table('tbl_penerimaan_barang_jadi')
            ->where('IDPBJ', $id)
            ->update(['Status' => 'Dibatalkan']);

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }
}

?>