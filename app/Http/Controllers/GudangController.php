<?php
/*=====Create TIAR @13/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\GudangModel;

class GudangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('gudang.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = GudangModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Gudang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        return view('gudang.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'kodegudang'    => 'required|unique:tbl_gudang,Kode_Gudang',
            'nama'             => 'required',
        ])->setAttributeNames([
            'kodegudang'    => 'Kode',
            'nama'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = DB::table('tbl_gudang')->selectRaw(DB::raw('MAX("IDGudang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new GudangModel;

        $data->IDGudang            = $urutan_id;
        $data->Kode_Gudang         = $request->kodegudang;
        $data->Nama_Gudang         = $request->nama;
        $data->Aktif               = 'Aktif';
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Gudang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $gudang               = GudangModel::findOrfail($id);

        return view('gudang.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'gudang'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'idgudang'  => 'required',
            'kodegudang'    => 'required',
            'nama'             => 'required',
        ])->setAttributeNames([
            'idgudang'  => 'Data',
            'kodegudang'    => 'Kode',
            'nama'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = GudangModel::findOrfail($request->idgudang);

        $data->Kode_Gudang         = $request->kodegudang;
        $data->Nama_Gudang         = $request->nama;
        $data->Aktif               = 'Aktif';
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = GudangModel::findOrfail($id)->delete();
        
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }
}