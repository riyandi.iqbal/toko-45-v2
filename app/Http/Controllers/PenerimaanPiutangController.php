<?php
/*=====Created by Tiar Sagita Rahman @28 Desember 2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;
use App\Helpers\AppHelper;

use App\Models\CustomerModel;
use App\Models\CoaModel;
use App\Models\PiutangModel;
use App\Models\PenerimaanPiutangModel;
use App\Models\PenerimaanPiutangDetailModel;
use App\Models\PenerimaanPiutangBayarModel;
use App\Models\VListPenerimaanPiutangModel;
use App\Models\VListPenerimaanPiutangBayarModel;
use App\Models\VListPenerimaanPiutangDetailModel;
use App\Models\PenjualanModel;
use App\Models\MataUangModel;
use App\Models\VListPiutangModel;
use App\Models\JurnalModel;
use App\Models\CaraBayarModel;
use App\Models\GiroModel;
use App\Models\SettingCOAModel;

class PenerimaanPiutangController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('penerimaanpiutang.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = VListPenerimaanPiutangModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListPenerimaanPiutangBayarModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer   = CustomerModel::get();
        $coa        = CoaModel::get();
        $kurs       = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $cara_bayar     = CaraBayarModel::GetPP();

        return view('penerimaanpiutang.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer', 'coa', 'kurs', 'cara_bayar'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required',
            'Nomor'             => 'required',
            'IDCustomer'        => 'required', 
            'Keterangan'        => 'nullable',
            'Total_pembayaran'        => 'required',
        ])->setAttributeNames([
            'Tanggal'           => 'Tanggal',
            'Nomor'             => 'Nomor',
            'IDCustomer'        => 'Customer',
            'Keterangan'        => 'Keterangan',
            'Total_pembayaran'  => 'Pembayaran',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }
        
        DB::beginTransaction();
        $IDTP = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor);

        $penerimaan_piutang = new PenerimaanPiutangModel;

        $penerimaan_piutang->IDTP           = $IDTP;
        $penerimaan_piutang->Tanggal        = AppHelper::DateFormat($request->Tanggal);
        $penerimaan_piutang->Nomor          = $Nomor;
        $penerimaan_piutang->IDCustomer     = $request->IDCustomer;
        $penerimaan_piutang->IDMataUang     = 1;
        $penerimaan_piutang->Kurs           = 14000;
        $penerimaan_piutang->Total          = AppHelper::StrReplace($request->Total_piutang);
        $penerimaan_piutang->Pembayaran         = AppHelper::StrReplace($request->Total_pembayaran);
        $penerimaan_piutang->Kelebihan_bayar    = AppHelper::StrReplace($request->Kelebihan_bayar);
        $penerimaan_piutang->Keterangan         = $request->Keterangan;
        $penerimaan_piutang->Batal              = 0;

        $penerimaan_piutang->save();

        foreach ($request->do as $key => $value) {
            // UPDATE PIUTANG //
            $piutang = PiutangModel::find($value);
            
            if ($piutang->Jenis_Faktur == 'RETUR') {
                $piutang->Saldo_Akhir   = 0;
            } else {
                $piutang->Pembayaran        = $piutang->Pembayaran + floatval(AppHelper::StrReplace($request->Nominal[$key])) ;
                $piutang->Saldo_Akhir       = floatval(AppHelper::StrReplace($piutang->Saldo_Akhir)) - floatval(AppHelper::StrReplace($request->Nominal[$key]));
            }
            
            $piutang->save();
            // END PIUTANG //

            // SAVE PENERIMAAN PIUTANG DETAIL //
            $IDTPDetail = uniqid();
            $penerimaan_piutang_detail = new PenerimaanPiutangDetailModel;

            $penerimaan_piutang_detail->IDTPDetail  = $IDTPDetail;
            $penerimaan_piutang_detail->IDTP        = $IDTP;
            $penerimaan_piutang_detail->IDFJ        = $piutang->IDFaktur;
            $penerimaan_piutang_detail->Nomor       = $piutang->No_faktur;
            $penerimaan_piutang_detail->Tanggal_fj  = $piutang->Tanggal_piutang;
            $penerimaan_piutang_detail->Nilai_fj    = $piutang->Nilai_Piutang;
            $penerimaan_piutang_detail->Diterima        = AppHelper::StrReplace($request->Nominal[$key]);
            $penerimaan_piutang_detail->Telah_diterima  = AppHelper::StrReplace($request->Nominal[$key]);
            $penerimaan_piutang_detail->IDMataUang  = 1;
            $penerimaan_piutang_detail->Kurs        = 14000;
            $penerimaan_piutang_detail->IDPiutang   = $piutang->IDPiutang;

            $penerimaan_piutang_detail->save();
            // END SAVE PENERIMAAN PIUTANG DETAIL //
        }

        $data_pembayaran = json_decode($request->Pembayaran_detail);

        foreach ($data_pembayaran as $key => $value) {
            $penerimaan_piutang_bayar = new PenerimaanPiutangBayarModel;

            $penerimaan_piutang_bayar->IDTPBayar        = uniqid();
            $penerimaan_piutang_bayar->IDTP             = $IDTP;
            $penerimaan_piutang_bayar->Jenis_pembayaran = $value->Jenis_pembayaran;
            $penerimaan_piutang_bayar->IDCOA            = $value->IDCoa;
            $penerimaan_piutang_bayar->Nomor_giro       = $value->Nomor_giro ? $value->Nomor_giro : null;
            $penerimaan_piutang_bayar->Nominal_pembayaran   = AppHelper::StrReplace($value->Nominal_pembayaran);
            $penerimaan_piutang_bayar->IDMataUang           = $value->IDMataUang;
            $penerimaan_piutang_bayar->Kurs                 = $value->Kurs;
            $penerimaan_piutang_bayar->Status_giro          = null ;
            $penerimaan_piutang_bayar->Tanggal_giro         = $value->Tanggal_giro ? AppHelper::DateFormat($value->Tanggal_giro) : null;

            $penerimaan_piutang_bayar->save();

            if ($value->Jenis_pembayaran == 'giro') {
                $giro = new GiroModel;

                $giro->IDGiro           = uniqid();
                $giro->IDFaktur         = $IDTP;
                $giro->IDPerusahaan     = $request->IDCustomer;
                $giro->Jenis_faktur     = 'PP';
                $giro->Nomor_faktur     = $Nomor;
                $giro->Nomor_giro       = $value->Nomor_giro;
                $giro->Tanggal_giro     = $value->Tanggal_giro ? AppHelper::DateFormat($value->Tanggal_giro) : null;
                $giro->Tanggal_faktur   = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai            = AppHelper::StrReplace($value->Nominal_pembayaran);

                $giro->save();
            }

            $jurnal_penerimaan_piutang_debet = new JurnalModel;

            $jurnal_penerimaan_piutang_debet->IDJurnal     = $this->number_jurnal();
            $jurnal_penerimaan_piutang_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penerimaan_piutang_debet->Nomor        = $Nomor;
            $jurnal_penerimaan_piutang_debet->IDFaktur     = $IDTP;
            $jurnal_penerimaan_piutang_debet->IDFakturDetail   = $penerimaan_piutang_bayar->IDTPBayar;
            $jurnal_penerimaan_piutang_debet->Jenis_faktur = 'PP';
            $jurnal_penerimaan_piutang_debet->IDCOA        = $value->IDCoa;
            $jurnal_penerimaan_piutang_debet->Debet        = AppHelper::StrReplace($value->Nominal_pembayaran);
            $jurnal_penerimaan_piutang_debet->Kredit       = 0;
            $jurnal_penerimaan_piutang_debet->IDMataUang       = $value->IDMataUang;
            $jurnal_penerimaan_piutang_debet->Kurs             = $value->Kurs;
            $jurnal_penerimaan_piutang_debet->Total_debet      = AppHelper::StrReplace($value->Nominal_pembayaran);
            $jurnal_penerimaan_piutang_debet->Total_kredit     = 0;
            $jurnal_penerimaan_piutang_debet->Keterangan       = $request->Keterangan;
            $jurnal_penerimaan_piutang_debet->Saldo            = AppHelper::StrReplace($value->Nominal_pembayaran);

            $jurnal_penerimaan_piutang_debet->save();
        }

        // SAVE JURNAL KREDIT //

        $CoaKredit = SettingCOAModel::where('IDMenu', '=', '80')
                                        ->where('Posisi', '=', 'kredit')
                                        ->where('Tingkat', '=', 'utama')
                                        ->where('Cara', '=', 'utang')
                                        ->first();

        $jurnal_penerimaan_piutang_kredit = new JurnalModel;

        $jurnal_penerimaan_piutang_kredit->IDJurnal     = $this->number_jurnal();
        $jurnal_penerimaan_piutang_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penerimaan_piutang_kredit->Nomor        = $Nomor;
        $jurnal_penerimaan_piutang_kredit->IDFaktur     = $IDTP;
        $jurnal_penerimaan_piutang_kredit->IDFakturDetail   = '-';
        $jurnal_penerimaan_piutang_kredit->Jenis_faktur = 'PP';
        $jurnal_penerimaan_piutang_kredit->IDCOA        = $CoaKredit->IDCoa;
        $jurnal_penerimaan_piutang_kredit->Debet        = 0;
        $jurnal_penerimaan_piutang_kredit->Kredit       = AppHelper::StrReplace($request->Total_pembayaran);
        $jurnal_penerimaan_piutang_kredit->IDMataUang       = 1;
        $jurnal_penerimaan_piutang_kredit->Kurs             = 1;
        $jurnal_penerimaan_piutang_kredit->Total_debet      = 0;
        $jurnal_penerimaan_piutang_kredit->Total_kredit     = AppHelper::StrReplace($request->Total_pembayaran);
        $jurnal_penerimaan_piutang_kredit->Keterangan       = $request->Keterangan;
        $jurnal_penerimaan_piutang_kredit->Saldo            = AppHelper::StrReplace($request->Total_pembayaran);

        $jurnal_penerimaan_piutang_kredit->save();

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penerimaan_piutang = VListPenerimaanPiutangModel::find($id);
        $penerimaan_piutang_bayar = VListPenerimaanPiutangBayarModel::where('IDTP', $id)->get();
        $penerimaan_piutang_detail = VListPenerimaanPiutangDetailModel::where('IDTP', $id)->get();

        return view('penerimaanpiutang.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penerimaan_piutang', 'penerimaan_piutang_bayar', 'penerimaan_piutang_detail'));
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('penerimaanpiutang.laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function number_invoice(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = VListPenerimaanPiutangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/PP/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    public function get_piutang(Request $request) {
        $piutang = VListPiutangModel::where('IDCustomer', $request->get('IDCustomer'))
                                ->where('Saldo_Akhir', '!=', 0)
                                ->where('Batal', 0)
                                ->get();
        
        return Datatables::of($piutang)->make(true);
    }
    
    public function get_coa(Request $request) {
        $jenis = $request->Jenis_Pembayaran;

        if($jenis=='transfer'){
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '10103.%')
                        ->get();
        } elseif($jenis=='giro') {
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '20102')
                        ->get();
        } elseif($jenis=='cash') {
            $data = DB::table('tbl_coa')
                        ->where('Nama_COA', 'LIKE', 'Kas%')
                        ->get();
        }

        return json_encode($data);
    }

    private function _cek_nomor($nomor) {
        $nomor_exist = VListPenerimaanPiutangModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number();
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number(){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = VListPenerimaanPiutangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/PP/' . $bulan_romawi . '/' . date('y');
        
        return $kode;
    }

    public function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }
}