<?php
/*=====Created by Tiar Sagita Rahman @17 Mei 2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\CustomerModel;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\GudangModel;
use App\Models\PenerimaanBarangModel;
use App\Models\PenerimaanBarangDetailModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\PerusahaanModel;
use App\Models\PurchaseOrderModel;
use App\Models\PurchaseOrderDetailModel;
use App\Models\SatuanKonversiModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PenerimaanBarangController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('penerimaanbarang.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data   = DB::table('tbl_terima_barang_supplier');
        $data->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_terima_barang_supplier.IDSupplier');
        $data->leftJoin('tbl_purchase_order', 'tbl_purchase_order.IDPO','=','tbl_terima_barang_supplier.IDPO');
        $data->select('tbl_terima_barang_supplier.*', 'tbl_supplier.Nama', 'tbl_purchase_order.Nomor as Nomor_po');


        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_terima_barang_supplier.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_terima_barang_supplier.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_terima_barang_supplier.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListPenerimaanBarangDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tanggal', [$request->get('tanggal_awal'), $request->get('tanggal_akhir')]);
            } else {
                $data->where('tanggal', $request->get('tanggal_awal'));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tanggal', '<=', $request->get('tanggal_akhir'));
            }
        }

        $data->orderBy('tanggal', 'asc');
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $kurs = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();
        $gudang = GudangModel::whereNotIn('Kode_Gudang', ['GBJ'])->get();

        $list_transaksi = DB::select('SELECT a."Nomor", a."IDPO", c."Nama"
                                FROM tbl_purchase_order a, tbl_purchase_order_detail b, tbl_supplier c
                                WHERE a."IDSupplier"=c."IDSupplier"
                                AND a."IDPO"=b."IDPO"
                                AND a."approve"=\'Disetujui\'
                                AND b."Qty_terima" > 0
                                GROUP BY a."Nomor", a."IDPO", c."Nama"
                                ORDER BY a."IDPO" ASC
                                ');

        return view('penerimaanbarang.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'bank', 'kurs', 'list_transaksi', 'gudang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Nomor'             => 'required',
            'IDPO'             => 'required',
            'IDSupplier'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Tanggal_jatuh_tempo'   => 'nullable||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'IDGudang'  => 'required',
            'Status_ppn'        => 'required',
            'Nama_di_faktur'    => 'nullable',
            'Keterangan'        => 'nullable',
            'Total_harga'       => 'required',
            'IDBarang.*'        => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'IDPO'              => 'Nomor Pesanan Pembelian',
            'IDSupplier'        => 'Supplier',
            'Tanggal'           => 'Tanggal',
            'Tanggal_jatuh_tempo'           => 'Tanggal Jatuh Tempo',
            'Total_qty'         => 'Total Qty',
            'IDGudang'          => 'Gudang',
            'Status_ppn'        => 'Jenis PPN',
            'Nama_di_faktur'    => 'Nama Di Faktur',
            'Keterangan'        => 'Keterangan',
            'Total_harga'       => 'Grand_total',
            'IDBarang.*'        => 'Barang',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDTBS = uniqid();

        $ceknopb =  DB::table('tbl_terima_barang_supplier')
                        ->where('Nomor', $request->Nomor)
                        ->first();

        if($ceknopb==''){
            $Nomor = $request->Nomor;
        }else{
            $hasilkeluaran = substr($ceknopb->Nomor,2,4) + 1;
            $cekagain = substr($request->Nomor, 0, 2);
            $ambilsetelahpenambahan = substr($request->Nomor, 6);
            $Nomor = $cekagain.str_pad($hasilkeluaran, 4, 0, STR_PAD_LEFT).$ambilsetelahpenambahan;
        }

        DB::beginTransaction();

        $purchase_order = PurchaseOrderModel::findOrfail($request->IDPO);

        $data = new PenerimaanBarangModel();

        $data->IDTBS             = $IDTBS;
        $data->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $data->Nomor             = $Nomor;
        $data->IDSupplier        = $request->IDSupplier;
        $data->IDPO              = $request->IDPO;
        $data->Total_harga       = AppHelper::StrReplace($request->Total_harga);
        $data->Total_qty_yard    = $request->Total_qty;
        $data->Keterangan        = $request->Keterangan;
        $data->Jenis_TBS         = 'PB';
        $data->Batal             = 'Aktif';
        $data->create_inv        = 'f';
        $data->IDGudang          = $request->IDGudang;
        $data->IDMataUang        = $purchase_order->IDMataUang;

        $data->save();

        foreach ($request->IDBarang as $key => $value) {
            $data_barang = BarangModel::find($value);
            
            $IDTBSDetail = uniqid();

            $data_detail = new PenerimaanBarangDetailModel;

            $data_detail->IDTBSDetail    = $IDTBSDetail;
            $data_detail->IDTBS          = $IDTBS;
            $data_detail->IDBarang       = $value;
            $data_detail->Qty_yard       = $request->Qty_kirim[$key];
            $data_detail->Qty_meter      = $request->Qty_kirim[$key];
            $data_detail->IDSatuan       = $request->IDSatuan[$key];
            $data_detail->Harga          = AppHelper::StrReplace($request->Harga[$key]);

            $data_detail->save();

            $purchase_order_detail = PurchaseOrderDetailModel::findOrfail($request->IDPODetail[$key]);

            $purchase_order_detail->Qty_terima = $purchase_order_detail->Qty_terima - $request->Qty_kirim[$key];

            $purchase_order_detail->save();

            if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
                $Qty_konversi = $request->Qty_kirim[$key];
            } else {
                $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

                $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty_kirim[$key];
            }

            // KARTU STOK //
            $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
                                    ->where('IDGudang', '=', $request->IDGudang)->first();

            if ($data_stok) {
                $stok = StokModel::find($data_stok->IDStok);
    
                $stok->Qty_pcs = $data_stok->Qty_pcs + $Qty_konversi;

                $stok->save();
            } else {
                $IDStok = uniqid();
                $stok = new StokModel;

                $stok->IDStok   = $IDStok;
                $stok->Tanggal  = date('Y-m-d H:i:s');
                $stok->Nomor_faktur     = $Nomor;
                $stok->IDBarang     = $request->IDBarang[$key];
                $stok->Qty_pcs      = $Qty_konversi;
                $stok->Nama_Barang      = $data_barang->Nama_Barang;
                $stok->Jenis_faktur     = 'TBS';
                $stok->IDGudang     = $request->IDGudang;
                $stok->IDSatuan     = $data_barang->IDSatuan;

                $stok->save();
            }

            $kartu_stok     = new KartustokModel;

            $kartu_stok->IDKartuStok        = uniqid();
            $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
            $kartu_stok->Nomor_faktur       = $Nomor;
            $kartu_stok->IDFaktur           = $IDTBS;
            $kartu_stok->IDFakturDetail     = $IDTBSDetail;
            $kartu_stok->IDStok         = $data_stok ? $data_stok->IDStok : $IDStok;
            $kartu_stok->Jenis_faktur   = 'TBS';
            $kartu_stok->IDBarang       = $request->IDBarang[$key];
            $kartu_stok->Harga          = AppHelper::StrReplace($request->Harga[$key]);;
            $kartu_stok->Nama_Barang    = $data_barang->Nama_Barang;
            $kartu_stok->Masuk_pcs     = $request->Qty_kirim[$key];
            $kartu_stok->IDSatuan       = $request->IDSatuan[$key];

            $kartu_stok->save();
            // END KARTU STOK //
        }

        $datapo = [
            'is_taken' => 't',
        ];
        
        DB::table('tbl_purchase_order')->where('IDPO', $request->IDPO)->update($datapo);

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'ID'    => $IDTBS
            ]
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $penerimaan_barang = PenerimaanBarangModel::where('tbl_terima_barang_supplier.IDTBS', $id)
        ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_terima_barang_supplier.IDSupplier')
        ->leftJoin('tbl_purchase_order', 'tbl_purchase_order.IDPO','=','tbl_terima_barang_supplier.IDPO')
        ->select('tbl_terima_barang_supplier.*', 'tbl_supplier.Nama', 'tbl_purchase_order.Nomor as Nomor_po', 'tbl_supplier.Alamat', 'tbl_supplier.No_Telpon')->first();

        $penerimaan_barang_detail = PurchaseOrderDetailModel::where('IDPO', $penerimaan_barang->IDPO)
        ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
        ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')->get();

        foreach ($penerimaan_barang_detail as $key => $value) {
            $value->penerimaan_detail = PenerimaanBarangDetailModel::where('IDBarang', $value->IDBarang)
                                                ->where('IDSatuan', $value->IDSatuan)
                                                ->where('IDTBS', $penerimaan_barang->IDTBS)
                                                ->first();
        }
        
        $gudang = GudangModel::get();

        return view('penerimaanbarang.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penerimaan_barang', 'penerimaan_barang_detail', 'gudang'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDTBS'             => 'required',
            'Nomor'             => 'required',
            'IDPO'             => 'required',
            'IDSupplier'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Tanggal_jatuh_tempo'   => 'nullable||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'IDGudang'  => 'required',
            'Nama_di_faktur'    => 'nullable',
            'Keterangan'        => 'nullable',
            'Total_harga'       => 'required',
            'IDBarang.*'        => 'required',
        ])->setAttributeNames([
            'IDTBS'             => 'Data',
            'Nomor'             => 'Nomor',
            'IDPO'              => 'Nomor Pesanan Pembelian',
            'IDSupplier'        => 'Supplier',
            'Tanggal'           => 'Tanggal',
            'Tanggal_jatuh_tempo'           => 'Tanggal Jatuh Tempo',
            'Total_qty'         => 'Total Qty',
            'IDGudang'          => 'Gudang',
            'Nama_di_faktur'    => 'Nama Di Faktur',
            'Keterangan'        => 'Keterangan',
            'Total_harga'       => 'Grand_total',
            'IDBarang.*'        => 'Barang',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDTBS = $request->IDTBS;
        $Nomor = $request->Nomor;

        DB::beginTransaction();

        $purchase_order = PurchaseOrderModel::findOrfail($request->IDPO);
        $data_exist = PenerimaanBarangModel::findOrfail($IDTBS);

        $data = PenerimaanBarangModel::findOrfail($IDTBS);

        $data->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $data->Nomor             = $Nomor;
        $data->IDSupplier        = $request->IDSupplier;
        $data->IDPO              = $request->IDPO;
        $data->Total_harga       = AppHelper::StrReplace($request->Total_harga);
        $data->Total_qty_yard    = $request->Total_qty;
        $data->Keterangan        = $request->Keterangan;
        $data->Jenis_TBS         = 'PB';
        $data->Batal             = 'Aktif';
        $data->create_inv        = 'f';
        $data->IDGudang          = $request->IDGudang;

        $data->save();

        $penerimaan_barang_detail_exist = PenerimaanBarangDetailModel::where('IDTBS', $IDTBS)->get();

        foreach ($penerimaan_barang_detail_exist as $key => $value) {
            $data_barang = BarangModel::find($value->IDBarang);

            $purchase_order_detail = PurchaseOrderDetailModel::findOrfail($request->IDPODetail[$key]);

            $purchase_order_detail->Qty_terima = $purchase_order_detail->Qty_terima + $value->Qty_yard;

            $purchase_order_detail->save();

            if ($data_barang->IDSatuan == $value->IDSatuan) {
                $Qty_konversi = $value->Qty_yard;
            } else {
                $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $value->IDSatuan)->first();

                $Qty_konversi = $data_satuan_konversi->Qty * $value->Qty_yard;
            }

            $data_stok = StokModel::where('IDBarang', $value->IDBarang)
                                    ->where('IDGudang', '=', $data_exist->IDGudang)->first();

            if ($data_stok) {
                $stok = StokModel::find($data_stok->IDStok);
    
                $stok->Qty_pcs = $data_stok->Qty_pcs - $Qty_konversi;

                $stok->save();
            }

            $kartu_stok_delete = KartuStokModel::where('IDFakturDetail', $value->IDTBSDetail)->delete();
        }

        $penerimaan_barang_detail_delete = PenerimaanBarangDetailModel::where('IDTBS', $IDTBS)->delete();

        foreach ($request->IDBarang as $key => $value) {
            $data_barang = BarangModel::find($value);
            
            $IDTBSDetail = uniqid();

            $data_detail = new PenerimaanBarangDetailModel;

            $data_detail->IDTBSDetail    = $IDTBSDetail;
            $data_detail->IDTBS          = $IDTBS;
            $data_detail->IDBarang       = $value;
            $data_detail->Qty_yard       = $request->Qty_kirim[$key];
            $data_detail->Qty_meter      = $request->Qty_kirim[$key];
            $data_detail->IDSatuan       = $request->IDSatuan[$key];
            $data_detail->Harga          = AppHelper::StrReplace($request->Harga[$key]);

            $data_detail->save();

            $purchase_order_detail = PurchaseOrderDetailModel::findOrfail($request->IDPODetail[$key]);

            $purchase_order_detail->Qty_terima = $request->Qty_kirim[$key];

            $purchase_order_detail->save();

            if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
                $Qty_konversi = $request->Qty_kirim[$key];
            } else {
                $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

                $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty_kirim[$key];
            }

            // KARTU STOK //
            $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
                                    ->where('IDGudang', '=', $request->IDGudang)->first();

            if ($data_stok) {
                $stok = StokModel::find($data_stok->IDStok);
    
                $stok->Qty_pcs = $data_stok->Qty_pcs + $Qty_konversi;

                $stok->save();
            } else {
                $IDStok = uniqid();
                $stok = new StokModel;

                $stok->IDStok   = $IDStok;
                $stok->Tanggal  = date('Y-m-d H:i:s');
                $stok->Nomor_faktur     = $Nomor;
                $stok->IDBarang     = $request->IDBarang[$key];
                $stok->Qty_pcs      = $Qty_konversi;
                $stok->Nama_Barang      = $data_barang->Nama_Barang;
                $stok->Jenis_faktur     = 'TBS';
                $stok->IDGudang     = $request->IDGudang;
                $stok->IDSatuan     = $data_barang->IDSatuan;

                $stok->save();
            }

            $kartu_stok     = new KartustokModel;

            $kartu_stok->IDKartuStok        = uniqid();
            $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
            $kartu_stok->Nomor_faktur       = $Nomor;
            $kartu_stok->IDFaktur           = $IDTBS;
            $kartu_stok->IDFakturDetail     = $IDTBSDetail;
            $kartu_stok->IDStok         = $data_stok ? $data_stok->IDStok : $IDStok;
            $kartu_stok->Jenis_faktur   = 'TBS';
            $kartu_stok->IDBarang       = $request->IDBarang[$key];
            $kartu_stok->Harga          = AppHelper::StrReplace($request->Harga[$key]);;
            $kartu_stok->Nama_Barang    = $data_barang->Nama_Barang;
            $kartu_stok->Masuk_pcs     = $request->Qty_kirim[$key];
            $kartu_stok->IDSatuan       = $request->IDSatuan[$key];

            $kartu_stok->save();
            // END KARTU STOK //
        }

        $datapo = [
            'is_taken' => 't',
        ];
        
        DB::table('tbl_purchase_order')->where('IDPO', $request->IDPO)->update($datapo);

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'ID'    => $IDTBS
            ]
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $penerimaan_barang = PenerimaanBarangModel::where('tbl_terima_barang_supplier.IDTBS', $id)
        ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_terima_barang_supplier.IDSupplier')
        ->leftJoin('tbl_purchase_order', 'tbl_purchase_order.IDPO','=','tbl_terima_barang_supplier.IDPO')
        ->select('tbl_terima_barang_supplier.*', 'tbl_supplier.Nama', 'tbl_purchase_order.Nomor as Nomor_po')->first();

        $penerimaan_barang_detail = PenerimaanBarangDetailModel::GetItem()->where('IDTBS', $id)->get();

        return view('penerimaanbarang.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penerimaan_barang', 'penerimaan_barang_detail'));
    }

    public function print($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $penerimaan_barang = PenerimaanBarangModel::where('tbl_terima_barang_supplier.IDTBS', $id)
        ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_terima_barang_supplier.IDSupplier')
        ->leftJoin('tbl_purchase_order', 'tbl_purchase_order.IDPO','=','tbl_terima_barang_supplier.IDPO')
        ->select('tbl_terima_barang_supplier.*', 'tbl_supplier.Nama', 'tbl_purchase_order.Nomor as Nomor_po', 'tbl_supplier.Alamat', 'tbl_supplier.No_Telpon')->first();

        $penerimaan_barang_detail = PenerimaanBarangDetailModel::GetItem()->where('IDTBS', $id)->get();
        $perusahaan          = PerusahaanModel::Getforprint();

        return view('penerimaanbarang.print', compact('coreset','aksesmenu', 'perusahaan','aksesmenudetail', 'aksessetting', 'namauser', 'penerimaan_barang', 'penerimaan_barang_detail'));
    }

    public function destroy($id) {
        DB::beginTransaction();

        $penerimaan_barang = PenerimaanBarangModel::findOrfail($id);

        if ($penerimaan_barang->Batal == 'Aktif') {
            $data = array (
                'status'    => false,
                'message'   => 'Data tidak bisa diaktifkan.'
            );
    
            return json_encode($data);
        } else {
            $penjualan = PenjualanModel::where('IDTBS', $penerimaan_barang->IDTBS)->where('Batal', 0)->first();
            if ($penjualan) {
                $status = false;
                $message = 'Data tidak bisa dibatalkan, Nomor surat jalan telah dilakukan penjualan.';
            } else {                
                $penerimaan_barang->Batal = 'Nonaktif';

                $data_penerimaan_barang_detail = PenerimaanBarangDetailModel::where('IDTBS', $penerimaan_barang->IDTBS)->get();

                foreach ($data_penerimaan_barang_detail as $key => $value) {

                    if ($data_barang->IDSatuan == $value->IDSatuan) {
                        $Qty_konversi = $value->Qty_yard;
                    } else {
                        $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value->IDBarang)->where('IDSatuanBesar', $value->IDSatuan)->first();
        
                        $Qty_konversi = $data_satuan_konversi->Qty * $value->Qty_yard;
                    }
        
                    // KARTU STOK //
                    $data_stok = StokModel::where('IDBarang', $value->IDBarang)
                                            ->where('IDGudang', '=', $penerimaan_barang->IDGudang)->first();

                    if ($data_stok) {
                        $stok = StokModel::find($data_stok->IDStok);
            
                        $stok->Qty_pcs = $data_stok->Qty_pcs - $value->Qty_yard;

                        $stok->save();
                    }
                }

                $message = 'Data berhasil dibatalkan.';
            }
        }

        $penerimaan_barang->save();

        DB::commit();

        $data = array (
            'status'    => (isset($status)) ? $status : true,
            'message'   => $message
        );

        return json_encode($data);
    }

    public function get_purchase_order(Request $request) {
        $sales_order = DB::table('tbl_purchase_order')
                        ->leftJoin('tbl_supplier', 'tbl_supplier.IDSupplier', '=', 'tbl_purchase_order.IDSupplier')
                        ->where('IDPO', $request->id)
                        ->first();
        
        return json_encode($sales_order);
    }

    public function get_purchase_order_detail(Request $request) {
        $data = DB::table('tbl_purchase_order_detail')
                        ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
                        ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')
                        ->where('IDPO', $request->get('IDPO'))
                        ->where('tbl_purchase_order_detail.Qty_terima', '>', 0)
                        ->get();
        
        return Datatables::of($data)->make(true);
    }

    public function laporan(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $mata_uang          = MataUangModel::where('Aktif', 'aktif')->get();
        return view('penerimaanbarang.laporan_pb', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'mata_uang'));
    }

    public function datatable_laporan(Request $request) {        
        $data = PenerimaanBarangModel::select('tbl_terima_barang_supplier.Nomor as NomorPB', 'tbl_terima_barang_supplier.IDTBS', 'tbl_terima_barang_supplier.Tanggal', 'tbl_terima_barang_supplier_detail.Qty_yard as Qty', 'tbl_terima_barang_supplier_detail.Harga', 'tbl_terima_barang_supplier.Total_harga as Saldo', 'tbl_purchase_order.Nomor', 'tbl_supplier.Nama', 'tbl_barang.Nama_Barang', 'tbl_mata_uang.Kode')
        ->join('tbl_terima_barang_supplier_detail', 'tbl_terima_barang_supplier.IDTBS', '=', 'tbl_terima_barang_supplier_detail.IDTBS')
        ->join('tbl_purchase_order', 'tbl_purchase_order.IDPO', '=', 'tbl_terima_barang_supplier.IDPO')
        ->join('tbl_supplier', 'tbl_terima_barang_supplier.IDSupplier', '=', 'tbl_supplier.IDSupplier')
        ->join('tbl_barang', 'tbl_terima_barang_supplier_detail.IDBarang', '=', 'tbl_barang.IDBarang')
        ->join('tbl_mata_uang', 'tbl_terima_barang_supplier.IDMataUang', '=', 'tbl_mata_uang.IDMataUang');

        if ($request->get('tanggal_awal')) {
            //tanggal awal tidak kosong            
            if ($request->get('tanggal_akhir')) {
                if($request->get('mata_uang')){
                    $data->whereBetween('tbl_terima_barang_supplier.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));
                }else{
                    $data->whereBetween('tbl_terima_barang_supplier.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                }
                if($request->get('field')&&$request->get('keyword')) {
                    $data->whereBetween('tbl_terima_barang_supplier.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));                    
                    if($request->get('field') == 'NomorPB') {
                        $data->where('tbl_terima_barang_supplier.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    }elseif($request->get('field') == 'Nomor') {
                        $data->where('tbl_purchase_order.Nomor', 'ilike', '%' . $request->get('keyword') . '%');
                    }else {
                        $data->where(''.$request->get('field').'', 'ilike', '%' . $request->get('keyword') . '%');
                    }
                }
            } else {                
                $data->where('tbl_terima_barang_supplier.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            //tanggal awal kosong
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_terima_barang_supplier.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }
        $data->orderBy('tbl_terima_barang_supplier.Tanggal', 'asc');

        $data->get();        
        return Datatables::of($data)->make(true);
    }
}