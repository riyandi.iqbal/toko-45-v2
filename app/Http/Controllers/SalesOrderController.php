<?php
/*=====Created by Tiar Sagita Rahman @26 Desember 2019
-- Editing Dedy @12 Maret 2020 line 65 permission tambah data
-- Editing Dedy @20 Maret 2020 tiap controller tambah coreset untuk setting font
====*/


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\CustomerModel;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\SalesOrderModel;
use App\Models\SalesOrderDetailModel;
use App\Models\UMCustomerModel;
use App\Models\VListSalesOrderDetailModel;
use App\Models\HargaJualModel;
use App\Models\JurnalModel;
use App\Models\PerusahaanModel;
use App\Models\GiroModel;
use App\Models\CaraBayarModel;
use App\Models\VListSatuanKonversiModel;
use App\Models\SettingCOAModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class SalesOrderController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('salesorder.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('salesorder.laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('SalesOrder')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer = CustomerModel::get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();

        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $barangBuilder->whereIn('IDGroupBarang', ['P000005', 'P000004']);
        
        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        $barang = $barangBuilder->get();

        $bank = BankModel::get();
        $groupcustomer = DB::table('tbl_group_customer')                
        ->orderBy('tbl_group_customer.Nama_Group_Customer', 'asc')
        ->get();

        $cara_bayar     = CaraBayarModel::GetSO();

        return view('salesorder.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer', 'bank', 'mata_uang', 'cara_bayar', 'groupcustomer', 'barang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Nomor'             => 'required',
            'IDCustomer'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'Grand_total'       => 'required',
            'IDMataUang'        => 'required',
            'Kurs'              => 'required',
            'Status_ppn'        => 'required',
            'Keterangan'        => 'nullable',
            'Jenis_Pembayaran'  => 'nullable',
            'IDCoa'             => 'nullable',
            'pembayaran'        => 'nullable',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'IDCustomer'        => 'Customer', 
            'Tanggal'           => 'Tanggal',
            'Total_qty'         => 'Total Qty',
            'Grand_total'       => 'Grand Total',
            'IDMataUang'        => 'Mata Uang',
            'Kurs'              => 'Kurs',
            'Status_ppn'        => 'Jenis PPn',
            'Keterangan'        => 'Keterangan',
            'Jenis_Pembayaran'  => 'Jenis Pembayaran',
            'IDCoa'             => 'IDCoa',
            'pembayaran'        => 'Pembayaran',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDSOK = uniqid();

        DB::beginTransaction();
        $Nomor = $this->_cek_nomor($request->Nomor, $request->Status_ppn);

        $sales_order = new SalesOrderModel;

        $sales_order->IDSOK             = $IDSOK;
        $sales_order->Nomor             = $Nomor;
        $sales_order->IDCustomer        = $request->IDCustomer;
        $sales_order->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $sales_order->Total_qty         = $request->Total_qty;
        $sales_order->Saldo_qty         = $request->Total_qty;
        $sales_order->Grand_total       = AppHelper::StrReplace($request->Grand_total);
        $sales_order->IDMataUang        = $request->IDMataUang;
        $sales_order->Kurs              = $request->Kurs;
        $sales_order->Status_ppn        = $request->Status_ppn;

        if ($request->IDGroupCustomer == 'P000003') {
            $sales_order->Persetujuan   = null;
        } else {
            $sales_order->Persetujuan   = 't';
        }

        if ($request->Status_ppn == 'exclude' || $request->Status_ppn == 'include') {
            $sales_order->PPN       = AppHelper::PpnInclude(AppHelper::StrReplace($request->Grand_total));
        } else {
            $sales_order->PPN       = 0;
        }
        
        $sales_order->DPP       = floatval(AppHelper::StrReplace($request->Grand_total)) - floatval(($sales_order->PPN));

        $sales_order->Keterangan        = $request->Keterangan;
        $sales_order->dibuat_pada       = date('Y-m-d H:i:s');
        $sales_order->diubah_pada       = date('Y-m-d H:i:s');

        $sales_order->save();

        foreach ($request->IDBarang as $key => $value) {
            $sales_order_detail = new SalesOrderDetailModel;

            $sales_order_detail->IDSOKDetail    = uniqid();
            $sales_order_detail->IDSOK          = $IDSOK;
            $sales_order_detail->IDBarang       = $value;
            $sales_order_detail->Qty            = $request->Qty[$key];
            $sales_order_detail->Saldo_qty      = $request->Qty[$key];
            $sales_order_detail->IDSatuan       = $request->IDSatuan[$key];            
            $sales_order_detail->Harga          = AppHelper::StrReplace($request->harga[$key]);
            $sales_order_detail->Sub_total      = AppHelper::StrReplace($request->Sub_total[$key]);

            $sales_order_detail->save();

            // UPDATE HARGA JUAL //
            $data_harga_jual = HargaJualModel::where('IDBarang', $value)
                                            ->where('IDSatuan', $request->IDSatuan[$key])
                                            ->where('IDGroupCustomer', $request->IDGroupCustomer)
                                            ->first();
            if ($data_harga_jual) {
                $harga_jual = HargaJualModel::find($data_harga_jual->IDHargaJual);
                
                $harga_jual->Harga_Jual = AppHelper::StrReplace($request->harga[$key]);
                $harga_jual->MTime = date('Y-m-d H:i:s');
    
                $harga_jual->save();
            } else {
                $nextnumber = DB::table('tbl_harga_jual_barang')->selectRaw(DB::raw('MAX("IDHargaJual") as nonext'))->first();   
                if($nextnumber->nonext==''){
                    $urutan_id = 'P000001';
                }else{
                    $hasil = substr($nextnumber->nonext,2,6) + 1;
                    $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
                }

                $harga_jual = new HargaJualModel;
                
                $harga_jual->IDHargaJual    = $urutan_id;
                $harga_jual->IDBarang       = $value;
                $harga_jual->IDSatuan       = $request->IDSatuan[$key];
                $harga_jual->IDGroupCustomer = $request->IDGroupCustomer;
                $harga_jual->Modal          = 0;
                $harga_jual->Harga_Jual     = AppHelper::StrReplace($request->harga[$key]);

                $harga_jual->save();
            }
            // END HARGA JUAL
        }

        if ($request->Jenis_Pembayaran) {
            $um_customer = new UMCustomerModel;
    
            $um_customer->IDUMCustomer  = uniqid();
            $um_customer->IDCustomer    = $request->IDCustomer;
            $um_customer->Nomor_Faktur  = $Nomor;
            $um_customer->Tanggal_UM    = AppHelper::DateFormat($request->Tanggal);
            $um_customer->Jenis_Pembayaran  = $request->Jenis_Pembayaran;
            $um_customer->Nilai_UM          = AppHelper::StrReplace($request->pembayaran);
            $um_customer->Saldo_UM          = AppHelper::StrReplace($request->pembayaran);
            $um_customer->IDFaktur          = $IDSOK;
            $um_customer->Pembayaran        = AppHelper::StrReplace($request->pembayaran);
            $um_customer->IDCoa             = ($request->IDCoa);
            $um_customer->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
            $um_customer->Nomor_giro        = $request->Nomor_giro;
            $um_customer->dibuat_pada       = date('Y-m-d H:i:s');
            $um_customer->diubah_pada       = date('Y-m-d H:i:s');
    
            $um_customer->save();

            if ($request->Jenis_Pembayaran == 'giro') {
                $giro = new GiroModel;

                $giro->IDGiro           = uniqid();
                $giro->IDFaktur         = $IDSOK;
                $giro->IDPerusahaan     = $request->IDCustomer;
                $giro->Jenis_faktur     = 'SO';
                $giro->Nomor_faktur     = $Nomor;
                $giro->Nomor_giro       = $request->Nomor_giro;
                $giro->Tanggal_giro     = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $giro->Tanggal_faktur   = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai            = AppHelper::StrReplace($request->pembayaran);

                $giro->save();
            }

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '75')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '75')
                                ->where('Posisi', '=', 'kredit')
                                ->where('Tingkat', '=', 'utama')
                                ->first();

            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor        = $Nomor;
            $jurnal_penjualan_debet->IDFaktur     = $IDSOK;
            $jurnal_penjualan_debet->IDFakturDetail   = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'SO';
            $jurnal_penjualan_debet->IDCOA        = $request->IDCoa;
            $jurnal_penjualan_debet->Debet        = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_debet->Kredit       = 0;
            $jurnal_penjualan_debet->IDMataUang       = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs             = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet      = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_debet->Total_kredit     = 0;
            $jurnal_penjualan_debet->Keterangan       = 'Uang Muka Penjualan';
            $jurnal_penjualan_debet->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_penjualan_debet->save();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor        = $Nomor;
            $jurnal_penjualan_kredit->IDFaktur     = $IDSOK;
            $jurnal_penjualan_kredit->IDFakturDetail   = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'SO';
            $jurnal_penjualan_kredit->IDCOA        = $CoaKredit->IDCoa;
            $jurnal_penjualan_kredit->Debet        = 0;
            $jurnal_penjualan_kredit->Kredit       = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_kredit->IDMataUang       = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs             = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet      = 0;
            $jurnal_penjualan_kredit->Total_kredit     = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_kredit->Keterangan       = 'Sales Order No ' . $Nomor;
            $jurnal_penjualan_kredit->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_penjualan_kredit->save();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'IDSOK'     => $IDSOK,
            ]
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer = CustomerModel::get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();

        $sales_order = SalesOrderModel::findOrfail($id);
        $sales_order_detail = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)
                                ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                                ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_sales_order_detail.IDSatuan')
                                ->get();
                                
        $surat_jalan_customer = DB::table('tbl_surat_jalan_customer')
                                ->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_surat_jalan_customer.idcustomer')
                                ->where('idso', $sales_order->IDSOK)
                                ->first();

        $um_customer = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->first();

        $cara_bayar     = CaraBayarModel::GetSO();

        $groupcustomer = DB::table('tbl_group_customer')                
        ->orderBy('tbl_group_customer.Nama_Group_Customer', 'asc')
        ->get();

        return view('salesorder.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer', 'bank', 'mata_uang', 'sales_order', 'sales_order_detail', 'um_customer', 'surat_jalan_customer', 'cara_bayar', 'groupcustomer'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDSOK'             => 'required',
            'IDCustomer'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'Grand_total'       => 'required',
            'IDMataUang'        => 'required',
            'Kurs'              => 'required',
            'Status_ppn'        => 'required',
            'Keterangan'        => 'nullable',
            'Jenis_Pembayaran'  => 'nullable',
            'IDCoa'             => 'nullable',
            'pembayaran'        => 'nullable',
        ])->setAttributeNames([
            'IDSOK'             => 'Data',
            'Nomor'             => 'Nomor',
            'IDCustomer'        => 'Customer', 
            'Tanggal'           => 'Tanggal',
            'Total_qty'         => 'Total Qty',
            'Grand_total'       => 'Grand Total',
            'IDMataUang'        => 'Mata Uang',
            'Kurs'              => 'Kurs',
            'Status_ppn'        => 'Jenis PPn',
            'Keterangan'        => 'Keterangan',
            'Jenis_Pembayaran'  => 'Jenis Pembayaran',
            'IDCoa'             => 'IDCoa',
            'pembayaran'        => 'Pembayaran',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $IDSOK = $request->IDSOK;

        $sales_order = SalesOrderModel::findOrfail($IDSOK);
        $kurs = MataUangModel::findOrfail($request->IDMataUang);

        $sales_order->Nomor             = $request->Nomor;
        $sales_order->IDCustomer        = $request->IDCustomer;
        $sales_order->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $sales_order->Total_qty         = $request->Total_qty;
        $sales_order->Saldo_qty         = $request->Total_qty;
        $sales_order->Grand_total       = AppHelper::StrReplace($request->Grand_total);
        $sales_order->IDMataUang        = $request->IDMataUang;
        $sales_order->Kurs              = $request->Kurs;
        $sales_order->Status_ppn        = $request->Status_ppn;

        if ($request->Status_ppn == 'exclude' || $request->Status_ppn == 'include') {
            $sales_order->PPN       = AppHelper::PpnInclude(AppHelper::StrReplace($request->Grand_total));
        } else {
            $sales_order->PPN       = 0;
        }
        
        $sales_order->DPP       = floatval(AppHelper::StrReplace($request->Grand_total)) - floatval(($sales_order->PPN));

        $sales_order->Keterangan        = $request->Keterangan;
        $sales_order->diubah_pada       = date('Y-m-d H:i:s');

        $sales_order->save();

        $sales_order_detail_exist = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)->delete();

        foreach ($request->IDBarang as $key => $value) {
            $sales_order_detail = new SalesOrderDetailModel;

            $sales_order_detail->IDSOKDetail    = uniqid();
            $sales_order_detail->IDSOK          = $IDSOK;
            $sales_order_detail->IDBarang       = $value;
            $sales_order_detail->Qty            = $request->Qty[$key];
            $sales_order_detail->Saldo_qty      = $request->Qty[$key];
            $sales_order_detail->IDSatuan       = $request->IDSatuan[$key];
            $sales_order_detail->Harga          = AppHelper::StrReplace($request->harga[$key]);
            $sales_order_detail->Sub_total      = AppHelper::StrReplace($request->Sub_total[$key]);

            $sales_order_detail->save();

            // UPDATE HARGA JUAL //
            $data_harga_jual = HargaJualModel::where('IDBarang', $value)
                                            ->where('IDSatuan', $request->IDSatuan[$key])
                                            ->first();

            $harga_jual = HargaJualModel::find($data_harga_jual->IDHargaJual);
            
            $harga_jual->Harga_Jual = AppHelper::StrReplace($request->harga[$key]);

            $harga_jual->save();
            // END HARGA JUAL
        }

        if ($request->Jenis_Pembayaran) {
            if ( ! $request->IDUMCustomer) {
                $um_customer = new UMCustomerModel;
    
                $um_customer->IDUMCustomer  = uniqid();
                $um_customer->IDCustomer    = $request->IDCustomer;
                $um_customer->Nomor_Faktur  = $request->Nomor;
                $um_customer->Tanggal_UM    = AppHelper::DateFormat($request->Tanggal);
                $um_customer->Jenis_Pembayaran  = $request->Jenis_Pembayaran;
                $um_customer->Nilai_UM          = AppHelper::StrReplace($request->pembayaran);
                $um_customer->Saldo_UM          = AppHelper::StrReplace($request->pembayaran);
                $um_customer->IDFaktur          = $IDSOK;
                $um_customer->Pembayaran        = AppHelper::StrReplace($request->pembayaran);
                $um_customer->IDCoa             = ($request->IDCoa);
                $um_customer->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $um_customer->Nomor_giro        = $request->Nomor_giro;
                $um_customer->dibuat_pada       = date('Y-m-d H:i:s');
                $um_customer->diubah_pada       = date('Y-m-d H:i:s');
        
                $um_customer->save();
            } else {
                $um_customer = UMCustomerModel::findOrfail($request->IDUMCustomer);
                
                $um_customer->IDCustomer    = $request->IDCustomer;
                $um_customer->Nomor_Faktur  = $request->Nomor;
                $um_customer->Tanggal_UM    = AppHelper::DateFormat($request->Tanggal);
                $um_customer->Jenis_Pembayaran  = $request->Jenis_Pembayaran;
                $um_customer->Nilai_UM          = AppHelper::StrReplace($request->pembayaran);
                $um_customer->Saldo_UM          = AppHelper::StrReplace($request->pembayaran);
                $um_customer->IDFaktur          = $IDSOK;
                $um_customer->Pembayaran        = AppHelper::StrReplace($request->pembayaran);
                $um_customer->IDCoa             = ($request->IDCoa);
                $um_customer->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $um_customer->Nomor_giro        = $request->Nomor_giro;
                $um_customer->diubah_pada       = date('Y-m-d H:i:s');

                $um_customer->save();
            }

            $delete_jurnal_awal = JurnalModel::where('IDFaktur', $IDSOK)->delete();

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '75')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '75')
                                ->where('Posisi', '=', 'kredit')
                                ->where('Tingkat', '=', 'utama')
                                ->first();

            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor        = $request->Nomor;
            $jurnal_penjualan_debet->IDFaktur     = $IDSOK;
            $jurnal_penjualan_debet->IDFakturDetail   = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'SO';
            $jurnal_penjualan_debet->IDCOA        = $CoaDebet->IDCoa;
            $jurnal_penjualan_debet->Debet        = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_debet->Kredit       = 0;
            $jurnal_penjualan_debet->IDMataUang       = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs             = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet      = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_debet->Total_kredit     = 0;
            $jurnal_penjualan_debet->Keterangan       = $request->Keterangan;
            $jurnal_penjualan_debet->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_penjualan_debet->save();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_penjualan_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor        = $request->Nomor;
            $jurnal_penjualan_kredit->IDFaktur     = $IDSOK;
            $jurnal_penjualan_kredit->IDFakturDetail   = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'SO';
            $jurnal_penjualan_kredit->IDCOA        = $request->IDCoa;
            $jurnal_penjualan_kredit->Debet        = 0;
            $jurnal_penjualan_kredit->Kredit       = AppHelper::StrReplace($request->pembayaran);;
            $jurnal_penjualan_kredit->IDMataUang       = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs             = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet      = 0;
            $jurnal_penjualan_kredit->Total_kredit     = AppHelper::StrReplace($request->pembayaran);
            $jurnal_penjualan_kredit->Keterangan       = $request->Keterangan;
            $jurnal_penjualan_kredit->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_penjualan_kredit->save();
        } else {
            $delete_um = UMCustomerModel::where('IDFaktur', $IDSOK)->delete();
            $delete_jurnal_awal = JurnalModel::where('IDFaktur', $IDSOK)->delete();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.',
            'data'      => [
                'IDSOK'     => $IDSOK,
            ]
        );

        return json_encode($data);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('SalesOrder')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        DB::beginTransaction();

        $sales_order = SalesOrderModel::findOrfail($id);

        if ($sales_order->Batal == 1) {
            // $sales_order->Batal = 0;

            // $delete_um = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->update(['Batal' => 0]);
            // $delete_jurnal_awal = JurnalModel::where('IDFaktur', $sales_order->IDSOK)->update(['Batal' => 0]);
            // $message = 'Data berhasil diaktifkan.';
            
            $data = array (
                'status'    => false,
                'message'   => 'Data tidak bisa diaktifkan.'
            );
    
            return json_encode($data);

        } else {
            $surat_jalan_customer = DB::table('tbl_surat_jalan_customer')
                    ->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_surat_jalan_customer.idcustomer')
                    ->where('idso', $sales_order->IDSOK)
                    ->first();

            if ($surat_jalan_customer) {
                $data = array (
                    'status'    => false,
                    'message'   => 'Data tidak bisa dibatalkan.'
                );
        
                return json_encode($data);
            }
            $sales_order->Batal = 1;

            $data_um = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->first();

            if ($data_um) {
                $jurnal_penjualan_debet = new JurnalModel;
    
                $jurnal_penjualan_debet->IDJurnal     = AppHelper::NumberJurnal();
                $jurnal_penjualan_debet->Tanggal      = date('Y-m-d');
                $jurnal_penjualan_debet->Nomor        = $data_um->Nomor_Faktur;
                $jurnal_penjualan_debet->IDFaktur     = $data_um->IDFaktur;
                $jurnal_penjualan_debet->IDFakturDetail   = '-';
                $jurnal_penjualan_debet->Jenis_faktur = 'SO';
                $jurnal_penjualan_debet->IDCOA        = 'P000040';
                $jurnal_penjualan_debet->Debet        = $data_um->Nilai_UM;
                $jurnal_penjualan_debet->Kredit       = 0;
                $jurnal_penjualan_debet->IDMataUang       = $sales_order->IDMataUang;
                $jurnal_penjualan_debet->Kurs             = $sales_order->Kurs;
                $jurnal_penjualan_debet->Total_debet      = $data_um->Nilai_UM;
                $jurnal_penjualan_debet->Total_kredit     = 0;
                $jurnal_penjualan_debet->Keterangan       = $sales_order->Keterangan;
                $jurnal_penjualan_debet->Saldo            = $data_um->Nilai_UM;
    
                $jurnal_penjualan_debet->save();
    
                $jurnal_penjualan_kredit = new JurnalModel;
    
                $jurnal_penjualan_kredit->IDJurnal     = AppHelper::NumberJurnal();
                $jurnal_penjualan_kredit->Tanggal      = date('Y-m-d');
                $jurnal_penjualan_kredit->Nomor        = $data_um->Nomor_Faktur;
                $jurnal_penjualan_kredit->IDFaktur     = $data_um->IDFaktur;
                $jurnal_penjualan_kredit->IDFakturDetail   = '-';
                $jurnal_penjualan_kredit->Jenis_faktur = 'SO';
                $jurnal_penjualan_kredit->IDCOA        = $data_um->IDCoa;
                $jurnal_penjualan_kredit->Debet        = 0;
                $jurnal_penjualan_kredit->Kredit       = $data_um->Nilai_UM;
                $jurnal_penjualan_kredit->IDMataUang       = $sales_order->IDMataUang;
                $jurnal_penjualan_kredit->Kurs             = $sales_order->Kurs;
                $jurnal_penjualan_kredit->Total_debet      = 0;
                $jurnal_penjualan_kredit->Total_kredit     = $data_um->Nilai_UM;
                $jurnal_penjualan_kredit->Keterangan       = $sales_order->Keterangan;
                $jurnal_penjualan_kredit->Saldo            = $data_um->Nilai_UM;
    
                $jurnal_penjualan_kredit->save();
            }

            $delete_um = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)->update(['Batal' => 1]);

            //$delete_jurnal_awal = JurnalModel::where('IDFaktur', $sales_order->IDSOK)->update(['Batal' => 1]);

            $message = 'Data berhasil dibatalkan.';
        }

        $sales_order->save();

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => $message
        );

        return json_encode($data);
    }

    public function print($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $sales_order = SalesOrderModel::findOrfail($id);
        $customer = CustomerModel::findOrfail($sales_order->IDCustomer);
        $kurs = MataUangModel::findOrfail($sales_order->IDMataUang);
        $sales_order_detail = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)
                                ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                                ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_sales_order_detail.IDSatuan')
                                ->get();
                                
        
        $um_customer = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)
                        ->leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_um_customer.IDCoa')
                        ->first();
        $perusahaan          = PerusahaanModel::Getforprint();
        return view('salesorder.print', compact('coreset','aksesmenu','perusahaan', 'aksesmenudetail', 'aksessetting', 'namauser', 'sales_order', 'customer', 'kurs', 'sales_order_detail', 'um_customer'));
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $sales_order = SalesOrderModel::findOrfail($id);
        $customer = CustomerModel::findOrfail($sales_order->IDCustomer);
        $kurs = MataUangModel::findOrfail($sales_order->IDMataUang);
        $sales_order_detail = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)
                                ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                                ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_sales_order_detail.IDSatuan')
                                ->get();
                                
        $um_customer = UMCustomerModel::where('IDFaktur', $sales_order->IDSOK)
                                ->leftJoin('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_um_customer.IDCoa')
                                ->first();

        return view('salesorder.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'sales_order', 'customer', 'kurs', 'sales_order_detail', 'um_customer'));
    }

    public function datatable(Request $request) {
        $data = SalesOrderModel::select('tbl_sales_order.*', 'tbl_customer.Nama as Nama', 'tbl_mata_uang.Kode', 'tbl_surat_jalan_customer.idsjc');
        
        $data->leftJoin('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_sales_order.IDMataUang');
        $data->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_sales_order.IDCustomer');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_sales_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_sales_order.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_sales_order.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->where('tbl_sales_order.Persetujuan', true);

        $data->leftJoin('tbl_surat_jalan_customer', function($join){
            $join->on('tbl_surat_jalan_customer.idso', '=', 'tbl_sales_order.IDSOK')
            ->where('tbl_surat_jalan_customer.Batal', '=', '0'); 
        });

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListSalesOrderDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        $data->orderBy('Tanggal', 'asc');
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function number_so(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = SalesOrderModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');
        if ($request->Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } if ($request->Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        }        
        $queryBuilder->where('Status_ppn', $request->Status_ppn);
        $queryBuilder->where(DB::raw('substring("Nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/SO/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    public function get_barang() {
        DB::enableQueryLog();
        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');
        
        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        if (Input::get('q')) {
            $queryBuilder->where('Nama_Barang', 'iLike', '%' . Input::get('q') . '%' );
        }
        
        $queryBuilder->whereIn('IDGroupBarang', ['P000005', 'P000004']);

        $response = $queryBuilder->take(10)->get();

        return json_encode($response);
    }

    public function get_satuan() {
        $queryBuilder = VListSatuanKonversiModel::select('*');
        
        if (Input::get('q')) {
            $queryBuilder->where('Satuan', 'iLike', '%' . Input::get('q') . '%' );
        }
        $queryBuilder->where('IDBarang', Input::get('IDBarang'));

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_harga_jual() {
        $queryBuilder = HargaJualModel::select('*');
        
        $queryBuilder->where('IDBarang', '=', Input::get('IDBarang'));
        $queryBuilder->where('IDSatuan', '=', Input::get('IDSatuan'));
        $queryBuilder->where('IDGroupCustomer', '=', Input::get('IDGroupCustomer'));

        $response = $queryBuilder->first();

        return json_encode($response);
    }

    public function get_coa(Request $request) {
        $jenis = $request->Jenis_Pembayaran;

        if($jenis=='transfer'){
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '10103.%')
                        ->get();
        } elseif($jenis=='giro') {
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '20102')
                        ->get();
        } elseif($jenis=='cash') {
            $data = DB::table('tbl_coa')
                        ->where('Nama_COA', 'LIKE', 'Kas%')
                        ->get();
        }

        return json_encode($data);
    }

    private function _cek_nomor($nomor, $Status_ppn) {
        $nomor_exist = SalesOrderModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number($Status_ppn);
            $this->_cek_nomor($nomor_baru, $Status_ppn);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number($Status_ppn){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = SalesOrderModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');
        if ($Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } if ($Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        }        
        $queryBuilder->where('Status_ppn', $Status_ppn);
        $queryBuilder->where(DB::raw('substring("Nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/SO/' . $bulan_romawi . '/' . date('y');

        return $kode;
    }
}