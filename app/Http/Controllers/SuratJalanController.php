<?php
/*=====Created by Riyandi Muhammad Iqbal @02 Januari 2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\CustomerModel;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\SuratJalanModel;
use App\Models\SuratJalanDetailModel;
use App\Models\VListSuratJalanModel;
use App\Models\VListSuratJalanDetailModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\PenjualanModel;
use App\Models\PerusahaanModel;
use App\Models\SalesOrderModel;
use App\Models\SalesOrderDetailModel;
use App\Models\VNomorSoReturModel;
use App\Models\VListReturPenjualanDetailModel;
use App\Models\VListReturPenjualanModel;
use App\Models\VListSalesOrderDetailModel;
use App\Models\SatuanKonversiModel;
use App\Models\SettingCOAModel;
use App\Models\JurnalModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class SuratJalanController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('suratjalan.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = VListSuratJalanModel::select('vlistsuratjalan.*', 'tbl_penjualan.IDFJ');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->leftJoin('tbl_penjualan', function($join){
            $join->on('tbl_penjualan.IDSJC', '=', 'vlistsuratjalan.idsjc')
            ->where('tbl_penjualan.Batal', '=', '0'); 
        });
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListSuratJalanDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tanggal', [$request->get('tanggal_awal'), $request->get('tanggal_akhir')]);
            } else {
                $data->where('tanggal', $request->get('tanggal_awal'));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tanggal', '<=', $request->get('tanggal_akhir'));
            }
        }

        $data->orderBy('tanggal', 'asc');
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer = CustomerModel::get();
        $kurs = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();

        $list_transaksi = VNomorSoReturModel::select('vnomorsoretur.*', 'tbl_customer.Nama')
        ->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'vnomorsoretur.IDCustomer')->where('Batal', 0)->get();

        return view('suratjalan.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer', 'bank', 'kurs', 'list_transaksi'));
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('suratjalan.laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Nomor'             => 'required',
            'ID'             => 'required',
            'IDCustomer'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Tanggal_jatuh_tempo'   => 'nullable||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'total_invoice_diskon'  => 'required',
            'Status_ppn'        => 'required',
            'Nama_di_faktur'    => 'nullable',
            'Keterangan'        => 'nullable',
            'DPP'               => 'required',
            'PPN'               => 'required',
            'IDBarang'          => 'required',
            'IDBarang.*'        => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'ID'              => 'Nomor Surat Jalan',
            'IDCustomer'        => 'Customer',
            'Tanggal'           => 'Tanggal',
            'Tanggal_jatuh_tempo'           => 'Tanggal Jatuh Tempo',
            'Total_qty'         => 'Total Qty',
            'total_invoice_diskon'       => 'Grand Total',
            'Status_ppn'        => 'Jenis PPN',
            'Nama_di_faktur'    => 'Nama Di Faktur',
            'Keterangan'        => 'Keterangan',
            'DPP'               => 'DPP',
            'PPN'               => 'PPN',
            'IDBarang'          => 'Barang',
            'IDBarang.*'        => 'Barang',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDSJC = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor, $request->Status_ppn);

        DB::beginTransaction();
        $customer = CustomerModel::findOrfail($request->IDCustomer);

        $confirm = false;

        // SALES ORDER UPDATE SALDO QTY //
        $sales_order = SalesOrderModel::find($request->ID);

        if ($sales_order) {
            if ($customer->IDGroupCustomer == 'P000003') {
                $sales_order->Saldo_qty     = $sales_order->Saldo_qty - $request->Total_qty;
                if ($sales_order->Saldo_qty > 0) {
                    $confirm = true;
                }
            } else {
                $sales_order->Saldo_qty     = 0;
            }
    
            $sales_order->save();
        }
        // END UPDATE SALES ORDER //

        $surat_jalan = new SuratJalanModel();

        $surat_jalan->idsjc              = $IDSJC;
        $surat_jalan->tanggal           = AppHelper::DateFormat($request->Tanggal);
        $surat_jalan->nomor             = $Nomor;
        $surat_jalan->idcustomer        = $request->IDCustomer;
        $surat_jalan->nomorcustomer    = $request->Nama_di_faktur;
        $surat_jalan->idso             = $request->ID;
        $surat_jalan->jatuhtempo        = $request->Tanggal_jatuh_tempo ? AppHelper::DateFormat($request->Tanggal_jatuh_tempo) : null;
        $surat_jalan->qty               = $request->Total_qty;
        $surat_jalan->grantotal          = AppHelper::StrReplace($request->total_invoice_diskon);
        $surat_jalan->persendisc       = $request->persendisc?$request->persendisc:0;
        $surat_jalan->statusppn        = $request->Status_ppn;
        $surat_jalan->keterangan        = $request->Keterangan;
        $surat_jalan->Batal             = 0;
        $surat_jalan->dpp               = AppHelper::StrReplace($request->DPP);
        $surat_jalan->nilaippn               = AppHelper::StrReplace($request->PPN);

        if ($confirm == true) {
            $surat_jalan->confirm = 0;
        }

        $surat_jalan->created_at       = date('Y-m-d H:i:s');
        $surat_jalan->updated_at       = date('Y-m-d H:i:s');

        $surat_jalan->save();

        $message = '';
        // foreach ($request->IDBarang as $key => $value) {
        //     $data_barang = BarangModel::find($value);
        //     $cekstok = StokModel::where('IDBarang',$value)->first();
        //     if ($cekstok){
        //         if ($cekstok->Qty_pcs < $request->Qty[$key]){
        //             $message .= 'Stok barang ' . $data_barang->Nama_Barang . ' kurang | ';
        //         }
        //     } else {                
        //         $message .= 'Stok barang ' . $data_barang->Nama_Barang . ' tidak ada | ';
        //     }
        // }

        if ($message != '') {
            $data = array (
                'status'    => false,
                'message'   => nl2br($message)
            );
    
            return json_encode($data);
        }

        foreach ($request->IDBarang as $key => $value) {
            $data_barang = BarangModel::find($value);

            if ($sales_order) {
                $sales_order_detail = SalesOrderDetailModel::findOrfail($request->IDSOKDetail[$key]);
    
                if ($customer->IDGroupCustomer == 'P000003') {
                    $sales_order_detail->Saldo_qty = $sales_order_detail->Saldo_qty - $request->Qty_kirim[$key];
                } else {
                    $sales_order_detail->Saldo_qty  = 0;
                }
    
                $sales_order_detail->save();
            }
            
            $IDSJCDetail = uniqid();
            $surat_jalan_detail = new SuratJalanDetailModel;

            $surat_jalan_detail->idsjcdetail     = $IDSJCDetail;
            $surat_jalan_detail->idsjc           = $IDSJC;
            $surat_jalan_detail->idbarang       = $value;
            $surat_jalan_detail->qty            = $request->Qty_kirim[$key];
            $surat_jalan_detail->saldo          = $request->Qty_kirim[$key];
            $surat_jalan_detail->idsatuan       = $request->IDSatuan[$key];
            $surat_jalan_detail->hargasatuan    = AppHelper::StrReplace($request->Harga[$key]);
            $surat_jalan_detail->subtotal       = AppHelper::StrReplace($request->Sub_total[$key]);

            $surat_jalan_detail->save();

            if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
                $Qty_konversi = $request->Qty_kirim[$key];
            } else {
                $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

                $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty_kirim[$key];
            }

            // KARTU STOK //
            $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
                                    ->where('IDGudang', '=', 'P000002')->first();

            if ($data_stok) {
                $stok = StokModel::find($data_stok->IDStok);
    
                $stok->Qty_pcs = $data_stok->Qty_pcs - $Qty_konversi;

                $stok->save();
            } else {
                $IDStok = uniqid();
                $stok = new StokModel;

                $stok->IDStok   = $IDStok;
                $stok->Tanggal  = date('Y-m-d H:i:s');
                $stok->Nomor_faktur     = $Nomor;
                $stok->IDBarang     = $request->IDBarang[$key];
                $stok->Qty_pcs      = $Qty_konversi * -1;
                $stok->Nama_Barang      = $data_barang->Nama_Barang;
                $stok->Jenis_faktur     = 'SJ';
                $stok->IDGudang     = 'P000002';
                $stok->IDSatuan     = $data_barang->IDSatuan;

                $stok->save();
            }

            $kartu_stok     = new KartustokModel;

            $kartu_stok->IDKartuStok        = uniqid();
            $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
            $kartu_stok->Nomor_faktur       = $Nomor;
            $kartu_stok->IDFaktur           = $IDSJC;
            $kartu_stok->IDFakturDetail     = $IDSJCDetail;
            $kartu_stok->IDStok         = $data_stok ? $data_stok->IDStok : $IDStok;
            $kartu_stok->Jenis_faktur   = 'SJ';
            $kartu_stok->IDBarang       = $request->IDBarang[$key];
            $kartu_stok->Harga          = AppHelper::StrReplace($request->Harga[$key]);;
            $kartu_stok->Nama_Barang    = $data_barang->Nama_Barang;
            $kartu_stok->Keluar_pcs     = $request->Qty_kirim[$key];
            $kartu_stok->IDSatuan       = $request->IDSatuan[$key];

            $kartu_stok->save();
            // END KARTU STOK //
        }

        $CoaDebet = SettingCOAModel::where('IDMenu', '=', '78')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

        $CoaKredit = SettingCOAModel::where('IDMenu', '=', '78')
                            ->where('Posisi', '=', 'kredit')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

        $jurnal_penjualan_debet = new JurnalModel;

        $jurnal_penjualan_debet->IDJurnal     = AppHelper::NumberJurnal();
        $jurnal_penjualan_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penjualan_debet->Nomor        = $Nomor;
        $jurnal_penjualan_debet->IDFaktur     = $IDSJC;
        $jurnal_penjualan_debet->IDFakturDetail   = '-';
        $jurnal_penjualan_debet->Jenis_faktur = 'SJ';
        $jurnal_penjualan_debet->IDCOA        = $CoaDebet->IDCoa;
        $jurnal_penjualan_debet->Debet        = AppHelper::StrReplace($request->total_invoice_diskon);
        $jurnal_penjualan_debet->Kredit       = 0;
        $jurnal_penjualan_debet->IDMataUang       = $sales_order->IDMataUang;
        $jurnal_penjualan_debet->Kurs             = $sales_order->Kurs;
        $jurnal_penjualan_debet->Total_debet      = AppHelper::StrReplace($request->total_invoice_diskon);
        $jurnal_penjualan_debet->Total_kredit     = 0;
        $jurnal_penjualan_debet->Keterangan       = 'Surat Jalan No. ' . $Nomor;
        $jurnal_penjualan_debet->Saldo            = AppHelper::StrReplace($request->total_invoice_diskon);

        $jurnal_penjualan_debet->save();

        $jurnal_penjualan_kredit = new JurnalModel;

        $jurnal_penjualan_kredit->IDJurnal     = AppHelper::NumberJurnal();
        $jurnal_penjualan_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penjualan_kredit->Nomor        = $Nomor;
        $jurnal_penjualan_kredit->IDFaktur     = $IDSJC;
        $jurnal_penjualan_kredit->IDFakturDetail   = '-';
        $jurnal_penjualan_kredit->Jenis_faktur = 'SJ';
        $jurnal_penjualan_kredit->IDCOA        = $CoaKredit->IDCoa;
        $jurnal_penjualan_kredit->Debet        = 0;
        $jurnal_penjualan_kredit->Kredit       = AppHelper::StrReplace($request->total_invoice_diskon);
        $jurnal_penjualan_kredit->IDMataUang       = $sales_order->IDMataUang;
        $jurnal_penjualan_kredit->Kurs             = $sales_order->Kurs;
        $jurnal_penjualan_kredit->Total_debet      = 0;
        $jurnal_penjualan_kredit->Total_kredit     = AppHelper::StrReplace($request->total_invoice_diskon);
        $jurnal_penjualan_kredit->Keterangan       = 'Surat Jalan No. ' . $Nomor;
        $jurnal_penjualan_kredit->Saldo            = AppHelper::StrReplace($request->total_invoice_diskon);

        $jurnal_penjualan_kredit->save();

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'ID'    => $IDSJC,
                'confirm'   => $confirm
            ]
        );

        return json_encode($data);
    }

    public function confirm($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $surat_jalan = VListSuratJalanModel::findOrfail($id);

        $surat_jalan_detail = VListSuratJalanDetailModel::where('idsjc', $surat_jalan->idsjc)->get();

        $sales_order_detail = VListSalesOrderDetailModel::where('IDSOK', $surat_jalan->idso)->where('Saldo_qty', '>', 0)->get();

        return view('suratjalan.confirm', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'surat_jalan', 'surat_jalan_detail', 'sales_order_detail'));
    }

    public function set_so(Request $request) {
        DB::beginTransaction();

        $data = SuratJalanModel::findOrfail($request->IDSJ);

        $data->confirm = 1;

        $data->save();

        if ($request->Status_so == 'SOHangus') {
            $sales_order = SalesOrderModel::findOrfail($data->idso);
            
            $sales_order->Saldo_qty = 0;

            $sales_order->save();

            $sales_order_detail = SalesOrderDetailModel::where('IDSOK', $sales_order->IDSOK)->update(['Saldo_qty' => 0]);
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'ID'    => $data->idsjc
            ]
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $surat_jalan = VListSuratJalanModel::findOrfail($id);

        $surat_jalan_detail = VListSuratJalanDetailModel::where('idsjc', $surat_jalan->idsjc)->get();

        return view('suratjalan.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'surat_jalan', 'surat_jalan_detail'));
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $surat_jalan = VListSuratJalanModel::findOrfail($id);

        $surat_jalan_detail = VListSuratJalanDetailModel::where('idsjc', $surat_jalan->idsjc)->get();

        return view('suratjalan.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'surat_jalan', 'surat_jalan_detail'));
    }

    public function print($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $surat_jalan = VListSuratJalanModel::findOrfail($id);

        $surat_jalan_detail = VListSuratJalanDetailModel::where('idsjc', $surat_jalan->idsjc)->get();
        $perusahaan          = PerusahaanModel::Getforprint();
        return view('suratjalan.print', compact('coreset','aksesmenu', 'perusahaan','aksesmenudetail', 'aksessetting', 'namauser', 'surat_jalan', 'surat_jalan_detail'));
    }

    public function destroy($id) {
        DB::beginTransaction();

        $surat_jalan = SuratJalanModel::findOrfail($id);

        if ($surat_jalan->Batal == 1) {
            // $data_exist = SuratJalanModel::where('idsjc', $surat_jalan->idsjc)->where('Batal', 0)->whereNotIn('idso', [$surat_jalan->idso])->first();
            // if ($data_exist) {
            //     $status = false;
            //     $message = 'Data tidak bisa diaktifkan, Surat Jalan telah digunakan di Faktur lain yang masih aktif.';
            // } else {
            //     $surat_jalan->Batal = 0;
            //     $message = 'Data berhasil diaktifkan.';
            // }
            $data = array (
                'status'    => false,
                'message'   => 'Data tidak bisa diaktifkan.'
            );
    
            return json_encode($data);
        } else {
            $penjualan = PenjualanModel::where('IDSJC', $surat_jalan->idsjc)->where('Batal', 0)->first();
            if ($penjualan) {
                $status = false;
                $message = 'Data tidak bisa dibatalkan, Nomor surat jalan telah dilakukan penjualan.';
            } else {                
                $surat_jalan->Batal = 1;

                $data_surat_jalan_detail = SuratJalanDetailModel::where('idsjc', $surat_jalan->idsjc)->get();

                foreach ($data_surat_jalan_detail as $key => $value) {
                    $data_stok = StokModel::where('IDBarang', $value->idbarang)->first();

                    if ($data_stok) {
                        $stok = StokModel::find($data_stok->IDStok);
            
                        $stok->Qty_pcs = $data_stok->Qty_pcs + $value->qty;

                        $stok->save();
                    }
                }

                $message = 'Data berhasil dibatalkan.';
            }
        }

        $surat_jalan->save();

        DB::commit();

        $data = array (
            'status'    => (isset($status)) ? $status : true,
            'message'   => $message
        );

        return json_encode($data);
    }

    public function number_invoice(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = SuratJalanModel::selectRaw('max(substring("nomor", 3, 4)) as kode');
        if ($request->Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } else if ($request->Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        } else {
            $Status_ppn_kode = 0;
        }
        $queryBuilder->where('statusppn', $request->Status_ppn);
        $queryBuilder->where(DB::raw('substring("nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/SJ/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    public function get_sales_order(Request $request) {
        $sales_order = DB::table('tbl_sales_order')
                        ->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_sales_order.IDCustomer')
                        ->where('IDSOK', $request->id)
                        ->first();

        if (! $sales_order) {
            $sales_order = VListReturPenjualanModel::where('IDRP', $request->id)->first();
        }
        
        return json_encode($sales_order);
    }

    public function get_sales_order_detail(Request $request) {
        $data = DB::table('tbl_sales_order_detail')
                        ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_sales_order_detail.IDBarang')
                        ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_sales_order_detail.IDSatuan')
                        ->where('IDSOK', $request->get('IDSOK'))
                        ->where('tbl_sales_order_detail.Saldo_qty', '>', 0)
                        ->get();

        if (count($data) < 1) {
            $data = VListReturPenjualanDetailModel::where('IDRP', $request->get('IDSOK'))->get();
        }
        
        return Datatables::of($data)->make(true);
    }

    private function _cek_nomor($nomor, $Status_ppn) {
        $nomor_exist = SuratJalanModel::where('nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number($Status_ppn);
            $this->_cek_nomor($nomor_baru, $Status_ppn);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number($Status_ppn){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = SuratJalanModel::selectRaw('max(substring("nomor", 3, 4)) as kode');
        if ($Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } if ($Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        }        
        $queryBuilder->where('Status_ppn', $Status_ppn);
        $queryBuilder->where(DB::raw('substring("nomor", 11, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/SJ/' . $bulan_romawi . '/' . date('y');

        return $kode;
    }
}