<?php
/*=====Create DEDY @12/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\BarangModel;


class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $barang             = BarangModel::Getforindex()->paginate(100);
        $groupbarang        = DB::table('tbl_group_barang')->orderBy('Group_Barang', 'asc')->get();

        return view('barang/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'barang', 'groupbarang'));
    }

    public function datatable(Request $request) {
        $data = BarangModel::Getforindex();

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Barang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $groupbarang        = DB::table('tbl_group_barang')->orderBy('Group_Barang', 'asc')->get();
        $kategori           = DB::table('tbl_kategori')->orderBy('Nama_Kategori', 'asc')->get();
        $ukuran             = DB::table('tbl_ukuran')->orderBy('Nama_Ukuran', 'asc')->get();
        $satuan             = DB::table('tbl_satuan')->orderBy('Satuan', 'asc')->get();
        $tipe               = DB::table('tbl_tipe_barang')->orderBy('Nama_Tipe', 'asc')->get();

        return view('barang/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'groupbarang', 'kategori', 'ukuran', 'satuan', 'tipe'));
    }

    public function simpandata(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'IDGroupBarang'             => 'required',
            'Kode_Barang'                  => 'required|unique:tbl_barang,Kode_Barang',
            'Nama_Barang'                  => 'required',
            'IDSatuan'                  => 'required',
            'IDKategori'                  => 'nullable',
            'IDUkuran'                  => 'nullable',
            'IDTipe'                  => 'nullable',
            'Berat'                  => 'nullable',
            'StokMinimal'             => 'nullable',
        ])->setAttributeNames([
            'IDGroupBarang'              => 'Group',
            'Kode_Barang'                  => 'Kode',
            'Nama_Barang'                  => 'Nama',
            'IDSatuan'                  => 'Satuan',
            'IDKategori'                  => 'Kategori',
            'IDUkuran'                  => 'Ukuran',
            'IDTipe'                  => 'Tipe',
            'Berat'                  => 'nullable',
            'StokMinimal'             => 'StokMinimal',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $data = $request->get('data1');

        $nextnumber = DB::table('tbl_barang')->selectRaw(DB::raw('MAX("IDBarang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDBarang'              => $urutan_id,
            'IDGroupBarang'         => $request->IDGroupBarang,
            'Kode_Barang'           => $request->Kode_Barang,
            'Nama_Barang'           => $request->Nama_Barang,
            'IDSatuan'              => $request->IDSatuan,
            'Aktif'                 => 'aktif',
            'IDKategori'            => $request->IDKategori==''?'P000006':$request->IDKategori,
            'IDUkuran'              => $request->IDUkuran==''?'P000003':$request->IDUkuran,
            'IDTipe'                => $request->IDTipe==''?'P000001':$request->IDTipe,
            'Berat'                 => $request->Berat,
            'StokMinimal'           => $request->StokMinimal,
        ];

        $createheader=DB::table('tbl_barang')->insert($header);
        DB::commit();
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($data); 

    }

    public function update(Request $request)
    {
        
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Barang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $edit               = DB::table('tbl_barang')->where('IDBarang', $request->id)->first();
        $groupbarang        = DB::table('tbl_group_barang')->orderBy('Group_Barang', 'asc')->get();
        $kategori           = DB::table('tbl_kategori')->orderBy('Nama_Kategori', 'asc')->get();
        $ukuran             = DB::table('tbl_ukuran')->orderBy('Nama_Ukuran', 'asc')->get();
        $satuan             = DB::table('tbl_satuan')->orderBy('Satuan', 'asc')->get();
        $tipe               = DB::table('tbl_tipe_barang')->orderBy('Nama_Tipe', 'asc')->get();

        return view('barang/edit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'tipe', 'edit', 'groupbarang', 'kategori', 'ukuran', 'satuan'));
    }

    public function simpandataedit(Request $request)
    {
        
        $validate = Validator::make($request->all(), [
            'data1.IDBarang'             => 'required',
            'data1.Groupbarang'             => 'required',
            'data1.Kodebarang'                  => 'required',
            'data1.Namabarang'                  => 'required',
            'data1.Satuan'                  => 'required',
            'data1.Kategori'                  => 'nullable',
            'data1.Ukuran'                  => 'nullable',
            'data1.Tipe'                  => 'nullable',
            'data1.Berat'                  => 'nullable',
            'data1.StokMinimal'             => 'nullable',
        ])->setAttributeNames([
            'data1.IDBarang'              => 'Group',
            'data1.Groupbarang'              => 'Group',
            'data1.Kodebarang'                  => 'Kode',
            'data1.Namabarang'                  => 'Nama',
            'data1.Satuan'                  => 'Satuan',
            'data1.Kategori'                  => 'Kategori',
            'data1.Ukuran'                  => 'Ukuran',
            'data1.Tipe'                  => 'Tipe',
            'data1.Berat'                  => 'Berat',
            'data1.StokMinimal'             => 'StokMinimal',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = $request->get('data1');
        $header = [
            'IDGroupBarang'         => $data['Groupbarang'],
            'Kode_Barang'           => $data['Kodebarang'],
            'Nama_Barang'           => $data['Namabarang'],
            'IDSatuan'              => $data['Satuan'],
            'Aktif'                 => 'aktif',
            'IDKategori'            => $data['Kategori'],
            'IDUkuran'              => $data['Ukuran'],
            'IDTipe'                => $data['Tipe'],
            'Berat'                 => $data['Berat'],
            'StokMinimal'           => $data['StokMinimal'],
        ];
        $createheader=DB::table('tbl_barang')
                            ->where('IDBarang', $data['IDBarang'])
                            ->update($header);

        return response()->json($createheader); 

    }

    public function editstatus($id)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = BarangModel::findOrfail($id);

        if ($data_exist->Aktif == 'aktif') {
            $data_exist->Aktif = 'nonaktif';
        } else {
            $data_exist->Aktif = 'aktif';
        }
        
        $data_exist->save();
        
        $response = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($response);
    }

    public function pencarian(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $keyword = $request->get('keyword');
        $status = $request->get('statuspencarian');
        $kolom = $request->get('jenispencarian');
        $groupbarang        = DB::table('tbl_group_barang')->orderBy('Group_Barang', 'asc')->get();

        if ($kolom!="" || $status!="" || $keyword!="") {     
            if ($kolom == 'Group_Barang') {
                $barang = BarangModel::Getforindex()->where('tbl_group_barang.IDGroupBarang', '=', "$keyword")
                                                        ->where('tbl_barang.Aktif', '=', $status)
                                                        ->orderBy('tbl_barang.Kode_Barang', 'asc')
                                                        ->paginate(100);
            } else {
                $barang = BarangModel::Getforindex()->where('tbl_group_barang.Group_Barang', 'like', "%$keyword%")
                                                        ->orWhere('tbl_barang.Kode_Barang', 'like', "%$keyword%")
                                                        ->orWhere('tbl_barang.Nama_Barang', 'like', "%$keyword%")
                                                        ->where('tbl_barang.Aktif', '=', $status)
                                                        ->orderBy('tbl_barang.Kode_Barang', 'asc')
                                                        ->paginate(100);
            }
                                                    //->get();
            $barang->appends($request->only('keyword'));
            return view('barang/pencarian', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'barang', 'keyword', 'groupbarang'));                 
                           
        }else {
            $barang = BarangModel::Getforindex()->paginate(100);
            return view('barang/pencarian', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'barang', 'groupbarang')); 
        }
    }

    public function simpangroup(Request $request)
    {
        $nextnumber = DB::table('tbl_group_barang')->selectRaw(DB::raw('MAX("IDGroupBarang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDGroupBarang'     => $urutan_id,
            'Kode_Group_Barang' => $request->get('kode'),
            'Group_Barang'      => $request->get('nama'),
            'Aktif'             => 'Aktif',
        ];

        $createheader=DB::table('tbl_group_barang')->insert($header);

        return redirect()->action('BarangController@create');
    }

    public function caritipe(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT * FROM tbl_tipe_barang
                                        WHERE "IDTipe"=\''.$data.'\'
                                        ');                                
        return response()->json($datapencarian);  
    }

    public function cariukuran(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT * FROM tbl_ukuran
                                        WHERE "IDUkuran"=\''.$data.'\'
                                        ');                                
        return response()->json($datapencarian);  
    }

    public function caricode(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT count("IDBarang") as CODE FROM tbl_barang
                                        WHERE "IDTipe"=\''.$data.'\'
                                        ');                                
        return response()->json($datapencarian);  
    }

    public function caricode2(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT count("IDBarang") as CODE FROM tbl_barang
                                        WHERE "IDGroupBarang"=\''.$data.'\'
                                        ');                                
        return response()->json($datapencarian);  
    }

    public function caricode3(Request $request)
    {
        $data = $request->get('data');
        $datapencarian = DB::select('SELECT count("IDBarang") as CODE FROM tbl_barang
                                        WHERE "IDGroupBarang"=\''.$data.'\'
                                        ');                                
        return response()->json($datapencarian);  
    }

    public function get_kategori(Request $request) {        
        $datagroup = DB::table('tbl_group_barang')->where('IDGroupBarang', '=', $request->IDGroupBarang)->first();

        $data = DB::table('tbl_kategori')
                        ->where('IDGroupBarang', '=', $request->IDGroupBarang)
                        ->get();

        $barangQuery = DB::table('tbl_barang')->where('IDGroupBarang', '=', $request->IDGroupBarang);
        if ($request->IDTipe) {
            $barangQuery->where('IDTipe', '=', $request->IDTipe);
        }
        $barangQuery->orderBy('Kode_Barang', 'DESC');
        $barang = $barangQuery->first();

        if (! $barang) {
            $panjang = ($datagroup) ? ($datagroup->Panjang_Kode) ? $datagroup->Panjang_Kode : 2 : 2;
            $panjang_nama = ($datagroup) ? ($datagroup->Panjang_Nama) ? $datagroup->Panjang_Nama : 2 : 2;
            $urutan_kode = str_pad(1, $panjang, "0", STR_PAD_LEFT);
            $urutan_nama = str_pad(1, $panjang_nama, "0", STR_PAD_LEFT);
        } else {
            $panjang = ($datagroup) ? ($datagroup->Panjang_Kode) ? $datagroup->Panjang_Kode : 2 : 2;
            $panjang_nama = ($datagroup) ? ($datagroup->Panjang_Nama) ? $datagroup->Panjang_Nama : 2 : 2;
            if ((int) substr($barang->Kode_Barang, ($panjang * -1)) > 0) {
                $str = (int) substr($barang->Kode_Barang, ($panjang * -1)) + 1;
            } else {
                $str = ((int) substr($barang->Kode_Barang, ($panjang * -1)) * -1 ) + 1;
            }

            if ((int) substr($barang->Nama_Barang, ($panjang_nama * -1)) > 0) {
                $str_nama = (int) substr($barang->Nama_Barang, ($panjang_nama * -1)) + 1;
            } else {
                $str_nama = ((int) substr($barang->Nama_Barang, ($panjang_nama * -1)) * -1 ) + 1;
            }

            $urutan_kode = str_pad($str, $panjang, "0", STR_PAD_LEFT);
            $urutan_nama = str_pad($str_nama, $panjang_nama, "0", STR_PAD_LEFT);
        }

        $response = [
            'kategori'  => $data,
            'urutan_kode'    => $urutan_kode,
            'urutan_nama'    => $urutan_nama
        ];
        return json_encode($response);
    }

    public function carigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->where('IDGroupBarang', '=', $data)->first();
        return json_encode($datagroup);
    }

    public function kodedarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Kode')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Kode);
    
        if($daridb[0]!=''){
            $carikode = DB::table('tbl_group_barang')->select('Kode_Group_Barang')->where('IDGroupBarang', '=', $data)->first();
            $hasil = $carikode->Kode_Group_Barang;
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function namadarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Nama')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Nama);
    
        if($daridb[0]!=''){
            $carikode = DB::table('tbl_group_barang')->select('Group_Barang')->where('IDGroupBarang', '=', $data)->first();
            $hasil = $carikode->Group_Barang;
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function typekodedarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Kode')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Kode);
    
        if($daridb[2]!=''){
            $hasil = 'TIPE';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function typenamadarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Nama')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Nama);
    
        if($daridb[2]!=''){
            $hasil = 'TIPE';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function kategorikodedarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Kode')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Kode);
    
        if($daridb[4]!=''){
            $hasil = 'KATEGORI';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function kategorinamadarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Nama')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Nama);
    
        if($daridb[4]!=''){
            $hasil = 'KATEGORI';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function ukurankodedarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Kode')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Kode);
    
        if($daridb[3]!=''){
            $hasil = 'UKURAN';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function ukurannamadarigroup(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_group_barang')->select('Cetak_Nama')->where('IDGroupBarang', '=', $data)->first();
        $daridb = unserialize($datagroup->Cetak_Nama);
    
        if($daridb[3]!=''){
            $hasil = 'UKURAN';
        }else{
            $hasil = '0';
        }
        //var_dump($hasil);
        return json_encode($hasil);
    }

    public function getinukuran(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_ukuran')->select('Nama_Ukuran')->where('IDUkuran', '=', $data)->first();
        $daridb = $datagroup->Nama_Ukuran;

        return json_encode($daridb);
    }

    public function getinkategori(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_kategori')->select('Nama_Kategori')->where('IDKategori', '=', $data)->first();
        $daridb = $datagroup->Nama_Kategori;

        return json_encode($daridb);
    }

    public function getintype(Request $request)
    {
        $data = $request->get('data');
        $datagroup = DB::table('tbl_tipe_barang')->select('Nama_Tipe')->where('IDTipe', '=', $data)->first();
        $daridb = $datagroup->Nama_Tipe;

        return json_encode($daridb);
    }

}
