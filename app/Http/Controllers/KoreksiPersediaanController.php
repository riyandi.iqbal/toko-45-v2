<?php
/*=====Created by Tiar Sagita Rahman @11 Maret 2020====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

use App\Models\KoreksiPersediaanModel;
use App\Models\KoreksiPersediaanDetailModel;
use App\Models\VListKoreksiPersediaanModel;
use App\Models\VListKoreksiPersediaanDetailModel;
use App\Models\BarangModel;
use App\Models\KartustokModel;
use App\Models\StokModel;
use App\Models\GudangModel;

class KoreksiPersediaanController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('koreksipersediaan.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function laporan() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('koreksipersediaan.laporan', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser','coreset'));
    }

    public function datatable(Request $request) {
        $data = VListKoreksiPersediaanModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListKoreksiPersediaanDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $gudang     = GudangModel::get();

        return view('koreksipersediaan.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'gudang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Nomor'             => 'required',
            'Keterangan'        => 'nullable',
            'Total_qty'         => 'required',
            'IDGudang'          => 'required',
            'Grand_total'       => 'required',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'Tanggal'           => 'Tanggal',
            'Keterangan'        => 'Keterangan',
            'Total_qty'         => 'Total Qty',
            'IDGudang'          => 'Gudang',
            'Grand_total'       => 'Grand Total',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $IDKP = uniqid();
        $Nomor = $this->_cek_nomor($request->Nomor);

        DB::beginTransaction();

        $KoreksiPersediaan = new KoreksiPersediaanModel;

        $KoreksiPersediaan->IDKP    = $IDKP;
        $KoreksiPersediaan->Tanggal     = AppHelper::DateFormat($request->Tanggal);
        $KoreksiPersediaan->Nomor       = $Nomor;
        $KoreksiPersediaan->Total       = $request->Total_qty;
        $KoreksiPersediaan->IDGudang    = $request->IDGudang;
        $KoreksiPersediaan->IDMataUang  = 'P000001';
        $KoreksiPersediaan->Keterangan  = $request->Keterangan;
        $KoreksiPersediaan->dibuat_pada    = date('Y-m-d H:i:s');
        $KoreksiPersediaan->dibuat_oleh    = Auth::user()->id;
        $KoreksiPersediaan->diubah_pada    = date('Y-m-d H:i:s');
        $KoreksiPersediaan->diubah_oleh    = Auth::user()->id;

        $KoreksiPersediaan->save();

        foreach ($request->IDBarang as $key => $value) {
            $data_barang = BarangModel::find($value);

            $IDKPDetail = uniqid();
            $KoreksiPersediaanDetail = new KoreksiPersediaanDetailModel();

            $KoreksiPersediaanDetail->IDKPDetail   = uniqid();
            $KoreksiPersediaanDetail->IDKP         = $IDKP;
            $KoreksiPersediaanDetail->IDBarang     = $value;
            $KoreksiPersediaanDetail->IDSatuan     = $request->IDSatuan[$key];
            $KoreksiPersediaanDetail->Qty          = $request->Qty[$key];
            $KoreksiPersediaanDetail->Saldo_qty    = $request->Qty[$key];
            $KoreksiPersediaanDetail->Harga        = AppHelper::StrReplace($request->harga[$key]);
            $KoreksiPersediaanDetail->IDMataUang   = 'P000001';

            $KoreksiPersediaanDetail->save();

            // UPDATE STOK //
            $data_stok = StokModel::where('IDBarang', $value)->where('IDGudang', '=', $request->IDGudang)->first();
            if ($data_stok) {
                $stok = StokModel::find($data_stok->IDStok);
    
                $stok->Qty_pcs = $data_stok->Qty_pcs + $request->Qty[$key];

                $stok->save();
            }

            if ($request->Qty[$key] > 0) {
                // KARTU STOK //
                $kartu_stok     = new KartustokModel;

                $kartu_stok->IDKartuStok        = uniqid();
                $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
                $kartu_stok->Nomor_faktur       = $Nomor;
                $kartu_stok->IDFaktur           = $IDKP;
                $kartu_stok->IDFakturDetail     = $IDKPDetail;
                $kartu_stok->IDStok             = $data_stok ? $data_stok->IDStok : '-';
                $kartu_stok->Jenis_faktur       = 'KP';
                $kartu_stok->IDBarang           = $value;
                $kartu_stok->Harga              = AppHelper::StrReplace($request->harga[$key]);;
                $kartu_stok->Nama_Barang        = $data_barang->Nama_Barang;
                $kartu_stok->Masuk_pcs          = $request->Qty[$key];
                $kartu_stok->IDSatuan           = $request->IDSatuan[$key];

                $kartu_stok->save();
                // END KARTU STOK //
            } else {
                // KARTU STOK //
                $kartu_stok     = new KartustokModel;

                $kartu_stok->IDKartuStok        = uniqid();
                $kartu_stok->Tanggal            = AppHelper::DateFormat($request->Tanggal);
                $kartu_stok->Nomor_faktur       = $Nomor;
                $kartu_stok->IDFaktur           = $IDKP;
                $kartu_stok->IDFakturDetail     = $IDKPDetail;
                $kartu_stok->IDStok             = $data_stok ? $data_stok->IDStok : '-';
                $kartu_stok->Jenis_faktur       = 'KP';
                $kartu_stok->IDBarang           = $value;
                $kartu_stok->Harga              = AppHelper::StrReplace($request->harga[$key]);;
                $kartu_stok->Nama_Barang        = $data_barang->Nama_Barang;
                $kartu_stok->Keluar_pcs         = $request->Qty[$key];
                $kartu_stok->IDSatuan           = $request->IDSatuan[$key];

                $kartu_stok->save();
                // END KARTU STOK //
            }
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }    

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $KoreksiPersediaan = VListKoreksiPersediaanModel::findOrfail($id);

        $KoreksiPersediaanDetail = VListKoreksiPersediaanDetailModel::where('IDKP', $KoreksiPersediaan->IDKP)->get();

        return view('koreksipersediaan.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'KoreksiPersediaan', 'KoreksiPersediaanDetail'));
    }

    public function number(Request $request) {
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = KoreksiPersediaanModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/KP/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }        
    }

    public function get_barang() {
        DB::enableQueryLog();

        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_harga_jual_barang.Harga_Jual', 'tbl_satuan.Satuan', 'tbl_harga_jual_barang.Modal');
        $queryBuilder->leftJoin('tbl_harga_jual_barang', function($leftJoin)
        {
            $leftJoin->on('tbl_harga_jual_barang.IDBarang', '=', 'tbl_barang.IDBarang');
            $leftJoin->where('tbl_harga_jual_barang.IDSatuan', '=', 'tbl_barang.IDSatuan');
        });
        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');
        $queryBuilder->leftJoin('tbl_stok', 'tbl_stok.IDBarang', '=', 'tbl_barang.IDBarang');

        if (Input::get('q')) {
            $queryBuilder->where('tbl_barang.Nama_Barang', 'iLike', '%' . Input::get('q') . '%' );
        }

        $queryBuilder->where('tbl_stok.IDGudang', '=', Input::get('IDGudang'));

        $response = $queryBuilder->take(10)->get();

        return json_encode($response);
    }

    private function _cek_nomor($nomor) {
        $nomor_exist = KoreksiPersediaanModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number();
            $this->_cek_nomor($nomor_baru);
        } else {
            $nomor_baru = $nomor;
        }
        
        return $nomor_baru;
    }

    private function _generate_number(){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = KoreksiPersediaanModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $queryBuilder->where(DB::raw('substring("Nomor", 9, '.strlen($bulan_romawi).')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/KP/' . $bulan_romawi . '/' . date('y');
        
        return $kode;
    }
}
