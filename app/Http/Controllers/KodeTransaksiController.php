<?php
// CREATE BY DEDY 27 MARET 2020

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use App\Models\KodeTransaksiModel;
use App\Models\KodeTransaksiDetailModel;
use App\Models\ControlPanelModel;

class KodeTransaksiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $kode               = KodeTransaksiModel::join('tbl_menu_detail', 'tbl_menu_detail.IDMenuDetail', '=', 'tbl_kode_transaksi.IDMenuDetail')
                                ->get();
        return view('kodetransaksi/index', 
                compact('aksesmenu', 
                        'aksesmenudetail', 
                        'aksessetting',
                        'coreset', 
                        'namauser',
                        'kode'
                        ));
    }

    public function create(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses              = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('PenerimaanBarang')->with('alert', 'Anda Tidak Memiliki Akses');
        }   
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $IDMenu             = ControlPanelModel::Getformenusetting();

        return view('kodetransaksi/create', 
                compact('aksesmenu', 
                        'aksesmenudetail', 
                        'aksessetting',
                        'coreset', 
                        'namauser',
                        'IDMenu'
                        ));
    }

    public function simpandata(Request $request)
    {
        $data = $request->get("data1");

        $IDKode = uniqid();
        //=========simpan header
        $dataheader = new KodeTransaksiModel;
            $dataheader->IDKode         = $IDKode;
            $dataheader->Kode           = $data['KodeView'];
            $dataheader->IDMenuDetail   = $data['IDMenu'];
            $dataheader->Jadi1          = $data['Jadi1'];
            $dataheader->Jadi2          = $data['Jadi2'];
            $dataheader->Jadi3          = $data['Jadi3'];
            $dataheader->Jadi4          = $data['Jadi4'];
            $dataheader->Pisah1          = $data['Pisah1'];
            $dataheader->Pisah2          = $data['Pisah2'];
            $dataheader->Pisah3          = $data['Pisah3'];
            $dataheader->Pisah4          = $data['Pisah4'];
        $dataheader->save();

        //=========simpan detail
        $datadetail = new kodeTransaksiDetailModel;
            $datadetail->IDKode         = $IDKode;
            $datadetail->IDKodeDetail   = uniqid();
            $datadetail->Asal1          = $data['Asal1'];
            $datadetail->Tampil1        = $data['Tampil1'];
            $datadetail->Manual1        = $data['Manual1'];
            $datadetail->Pemisah1       = $data['Pemisah1'];
            $datadetail->Format1        = $data['Format1'];
            $datadetail->Jenis1         = $data['Jenis1'];
            $datadetail->Asal2          = $data['Asal2'];
            $datadetail->Tampil2        = $data['Tampil2'];
            $datadetail->Manual2        = $data['Manual2'];
            $datadetail->Pemisah2       = $data['Pemisah2'];
            $datadetail->Format2        = $data['Format2'];
            $datadetail->Jenis2         = $data['Jenis2'];

            $datadetail->Asal3          = $data['Asal3'];
            $datadetail->Tampil3        = $data['Tampil3'];
            $datadetail->Manual3        = $data['Manual3'];
            $datadetail->Pemisah3       = $data['Pemisah3'];
            $datadetail->Format3        = $data['Format3'];
            $datadetail->Jenis3         = $data['Jenis3'];

            $datadetail->Asal4          = $data['Asal4'];
            $datadetail->Tampil4        = $data['Tampil4'];
            $datadetail->Manual4        = $data['Manual4'];
            $datadetail->Pemisah4       = $data['Pemisah4'];
            $datadetail->Format4        = $data['Format4'];
            $datadetail->Jenis4         = $data['Jenis4'];
        $response = $datadetail->save();
        DB::commit();
        
        return json_encode($response);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $iduser             = Auth::user()->id;
        $akses              = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('PenerimaanBarang')->with('alert', 'Anda Tidak Memiliki Akses');
        }   
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $dataedit = DB::table('tbl_kode_transaksi')
                    ->join('tbl_kode_transaksi_detail', 'tbl_kode_transaksi.IDKode', '=', 'tbl_kode_transaksi_detail.IDKode')
                    ->where('tbl_kode_transaksi.IDKode', '=', $id)
                    ->first();
        $IDMenu             = ControlPanelModel::Getformenusetting();
        
        return view('kodetransaksi/edit', 
                compact('aksesmenu', 
                        'aksesmenudetail', 
                        'aksessetting',
                        'coreset', 
                        'namauser',
                        'dataedit',
                        'IDMenu'
                        ));
        

    }

    public function purchaseordernonppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '35')->first();

        $nomorpo  = DB::table('tbl_purchase_order')->select(DB::raw('count("Nomor") as pomax'))->where('pilihanppn', '=', 'include')->first();
        $urutanjadi = sprintf("%04d", ($nomorpo->pomax+1));

        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        
        if($datakode->Jadi1=='0001'){
            $Jadi1 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function purchaseorderppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '35')->first();
        $nomorpo  = DB::table('tbl_purchase_order')->select(DB::raw('count("Nomor") as pomax'))->where('pilihanppn', '=', 'exclude')->first();
        $urutanjadi = sprintf("%04d", ($nomorpo->pomax+1));
        
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }

        if($datakode->Jadi1=='0001'){
            $Jadi1 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function penerimaanbarang(Request $request)
    {
        $nopo = $request->get('nomopo');
       
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '39')->first();
        $nomorpo  = DB::table('tbl_purchase_order')->where('IDPO', '=', $nopo)->first();
        $nomorpb  = DB::table('tbl_terima_barang_supplier')->select(DB::raw('count("Nomor") as pbmax'))->first();
        $urutanjadi = sprintf("%04d", ($nomorpb->pbmax+1));

        //==================penentuan kode 0/1
        if($nomorpo->pilihanppn=='include'){
            $headerkode = '1-';
        }else{
            $headerkode = '0-';
        }
        //==================return jika belum ada settingan di master kode transaksi 
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        //==================setting sesuai dengan db kode transaksi
        if($datakode->Jadi1=='0001'){
            $Jadi1 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }
        //==========================gabungkan kode
        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function invoicepembelian(Request $request)
    {
        $status = $request->get('status');
       
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '44')->first();
        $nomorpb  = DB::table('tbl_pembelian')->select(DB::raw('count("Nomor") as fbmax'))->first();
        $urutanjadi = sprintf("%04d", ($nomorpb->fbmax+1));

        //==================penentuan kode 0/1
        if($status=='include'){
            $headerkode = '1-';
        }else{
            $headerkode = '0-';
        }
        //==================return jika belum ada settingan di master kode transaksi 
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        //==================setting sesuai dengan db kode transaksi
        if($datakode->Jadi1=='0001'){
            $Jadi1 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }
        //==========================gabungkan kode
        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function returbeli(Request $request)
    {
        $status = $request->get('status');
       
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '47')->first();
        $nomorrb  = DB::table('tbl_retur_pembelian')->select(DB::raw('count("Nomor") as rbmax'))->first();
        $urutanjadi = sprintf("%04d", ($nomorrb->rbmax+1));

        //==================penentuan kode 0/1
        if($status=='include'){
            $headerkode = '1-';
        }else{
            $headerkode = '0-';
        }
        //==================return jika belum ada settingan di master kode transaksi 
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        //==================setting sesuai dengan db kode transaksi
        if($datakode->Jadi1=='0001'){
            $Jadi1 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }
        //==========================gabungkan kode
        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function salesordernonppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '75')->first();

        $nomorso  = DB::table('tbl_sales_order')->select(DB::raw('count("Nomor") as somax'))->where('Status_ppn', '=', 'include')->first();
        $urutanjadi = sprintf("%04d", ($nomorso->somax+1));

        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        
        if($datakode->Jadi1=='0001'){
            $Jadi1 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function salesorderppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '75')->first();
        $nomorso  = DB::table('tbl_sales_order')->select(DB::raw('count("Nomor") as somax'))->where('Status_ppn', '=', 'exclude')->first();
        $urutanjadi = sprintf("%04d", ($nomorso->somax+1));
        
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }

        if($datakode->Jadi1=='0001'){
            $Jadi1 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function suratjalan(Request $request)
    {
        $status = $request->get('status');
       
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '78')->first();
        $nomorsj  = DB::table('tbl_surat_jalan_customer')->select(DB::raw('count("nomor") as sjmax'))->first();
        $urutanjadi = sprintf("%04d", ($nomorsj->sjmax+1));

        //==================penentuan kode 0/1
        if($status=='include'){
            $headerkode = '1-';
        }else{
            $headerkode = '0-';
        }
        //==================return jika belum ada settingan di master kode transaksi 
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        //==================setting sesuai dengan db kode transaksi
        if($datakode->Jadi1=='0001'){
            $Jadi1 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }
        //==========================gabungkan kode
        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function invoicepenjualan(Request $request)
    {
        $status = $request->get('status');
       
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '80')->first();
        $nomorfj  = DB::table('tbl_penjualan')->select(DB::raw('count("Nomor") as fjmax'))->first();
        $urutanjadi = sprintf("%04d", ($nomorfj->fjmax+1));

        //==================penentuan kode 0/1
        if($status=='include'){
            $headerkode = '1-';
        }else{
            $headerkode = '0-';
        }
        //==================return jika belum ada settingan di master kode transaksi 
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        //==================setting sesuai dengan db kode transaksi
        if($datakode->Jadi1=='0001'){
            $Jadi1 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = $headerkode.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }
        //==========================gabungkan kode
        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function returjualnonppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '85')->first();

        $nomorrj  = DB::table('tbl_retur_penjualan')->select(DB::raw('count("Nomor") as rjmax'))->where('Status_ppn', '=', 'include')->first();
        $urutanjadi = sprintf("%04d", ($nomorrj->rjmax+1));

        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }
        
        if($datakode->Jadi1=='0001'){
            $Jadi1 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '1-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

    public function returjualppn()
    {
        $datakode = DB::table('tbl_kode_transaksi')->where('IDMenuDetail', '=', '85')->first();
        $nomorrj  = DB::table('tbl_retur_penjualan')->select(DB::raw('count("Nomor") as rjmax'))->where('Status_ppn', '=', 'exclude')->first();
        $urutanjadi = sprintf("%04d", ($nomorrj->rjmax+1));
        
        if($datakode==''){
            $datareturn = "KODE BLM DISET";
            return json_encode($datareturn);
        }

        if($datakode->Jadi1=='0001'){
            $Jadi1 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi1=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi1 = $array_bulan[date('n')];
        }elseif($datakode->Jadi1=='XX' || $datakode->Jadi1=='20'){
            $Jadi1 = date('y');
        }else{
            $Jadi1 = $datakode->Jadi1;
        }

        if($datakode->Jadi2=='0001'){
            $Jadi2 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi2=='I' || $datakode->Jadi2=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi2 = $array_bulan[date('n')];
        }elseif($datakode->Jadi2=='XX' || $datakode->Jadi2=='20'){
            $Jadi2 = date('y');
        }else{
            $Jadi2 = $datakode->Jadi2;
        }

        if($datakode->Jadi3=='0001'){
            $Jadi3 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi3=='I' || $datakode->Jadi3=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi3 = $array_bulan[date('n')];
        }elseif($datakode->Jadi3=='XX' || $datakode->Jadi3=='20'){
            $Jadi3 = date('y');
        }else{
            $Jadi3 = $datakode->Jadi3;
        }

        if($datakode->Jadi4=='0001'){
            $Jadi4 = '0-'.$urutanjadi;
        }elseif($datakode->Jadi4=='I' || $datakode->Jadi4=='01'){
            $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
            $Jadi4 = $array_bulan[date('n')];
        }elseif($datakode->Jadi4=='XX' || $datakode->Jadi4=='20'){
            $Jadi4 = date('y');
        }else{
            $Jadi4 = $datakode->Jadi4;
        }

        $kodegabung = $Jadi1.
                        $datakode->Pisah2.
                        $Jadi2.
                        $datakode->Pisah3.
                        $Jadi3.
                        $datakode->Pisah4.
                        $Jadi4;
        return json_encode($kodegabung);
    }

}    
?>