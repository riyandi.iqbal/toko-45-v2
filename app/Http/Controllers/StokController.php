<?php
/*=====Create DEDY @07/01/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\BarangModel;
use App\Models\SatuanModel;
use App\Models\StokModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class StokController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        //DB::enableQueryLog();
        $stok               = StokModel::Tampilstok()->get();
        //$query = DB::getQueryLog();
        //print_r($query);
        return view('stok/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'stok'));
    }

    public function datatable(Request $request) {
        $data = StokModel::Tampilstok();

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function caridata(Request $request)
    {
        $keyword    = $request->get('keyword');
        $sdate      = $request->get('date1');
        $edate      = $request->get('date2');

        if ($sdate!="" || $edate!="" || $keyword!="" ) {

            
            $filter = 'AND c."Nama_Barang" ILIKE \'%'.$keyword.'%\'';
            $date = 'AND a."Tanggal" BETWEEN \''.$sdate.'\' AND \''.$edate.'\'';
            $datapencarian = DB::select('SELECT * FROM tbl_stok a,
                                                            tbl_satuan b,
                                                            tbl_barang c
                                            WHERE a."IDSatuan"=b."IDSatuan"
                                            AND a."IDBarang"=c."IDBarang" 
                                            
                                            '.$date.' '.$filter.'
                                            ');
            return response()->json($datapencarian);

        }
    }

    public function kartustok(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $headerstok         = StokModel::Tampilstok()
                                    ->where('IDStok', '=', $request->id)
                                    ->first();
        //DB::enableQueryLog();
        $kartustok          = StokModel::Tampildetailstok()->where('tbl_barang.IDBarang', '=', $headerstok->IDBarang)->where('IDStok', '=', $headerstok->IDStok)->get();
        //$query = DB::getQueryLog();
        //print_r($query);
        
        return view('stok/kartustok', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'headerstok', 'kartustok'));
    }

}
?>