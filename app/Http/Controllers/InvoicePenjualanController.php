<?php
/*=====Created by Tiar Sagita Rahman @28 Desember 2019====*/

namespace App\Http\Controllers;

use App\Helpers\AppHelper;
use App\Models\BankModel;
use App\Models\BarangModel;
use App\Models\CaraBayarModel;
use App\Models\CustomerModel;
use App\Models\GiroModel;
use App\Models\JurnalModel;
use App\Models\KartustokModel;
use App\Models\MataUangModel;
use App\Models\PenjualanDetailModel;
use App\Models\PenjualanGrandTotalModel;
use App\Models\PenjualanModel;

use App\Models\InvoicePenjualanModel;
use App\Models\PenjualanPembayaranModel;
use App\Models\PerusahaanModel;
use App\Models\PiutangModel;
use App\Models\ReturPenjualanModel;
use App\Models\SalesOrderModel;
use App\Models\SettingCOAModel;
use App\Models\StokModel;
use App\Models\SuratJalanModel;
use App\Models\UMCustomerModel;
use App\Models\VListPenjualanDetailModel;
use App\Models\VListPenjualanModel;
use App\Models\VListSatuanKonversiModel;
use App\Models\VListSuratJalanModel;
use Auth;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// HELPERS //
use Illuminate\Support\Facades\Input;

// LIB //
use Validator;

class InvoicePenjualanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index()
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('invoicepenjualan.index', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function laporan()
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('invoicepenjualan.laporan', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request)
    {
        $data = VListPenjualanModel::select('vlistpenjualan.*', 'tbl_retur_penjualan.IDRP');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('vlistpenjualan.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('vlistpenjualan.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('vlistpenjualan.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%' . $request->get('keyword') . '%');
        }

        $data->leftJoin('tbl_retur_penjualan', function ($join) {
            $join->on('tbl_retur_penjualan.IDFJ', '=', 'vlistpenjualan.IDFJ')
                ->where('tbl_retur_penjualan.Batal', '=', '0');
        });

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request)
    {
        $data = VListPenjualanDetailModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%' . $request->get('keyword') . '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create()
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $customer = CustomerModel::get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');
        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);
        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');
        $barang = $barangBuilder->get();
        $bank = BankModel::get();

        $surat_jalan_customer = VListSuratJalanModel::select('vlistsuratjalan.*', 'tbl_customer.Nama')
            ->whereNotIn('idsjc', function ($queryBuilder) {
                $queryBuilder->select('IDSJC')->where('Batal', 0)->from('tbl_penjualan')->get();
            })->whereNotNull('nomorso')->where('confirm', 1)->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'vlistsuratjalan.idcustomer')->where('Batal', 0)->get();

        $cara_bayar = CaraBayarModel::GetFJ();

        return view('invoicepenjualan.create', compact('coreset', 'barang', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'customer', 'bank', 'mata_uang', 'surat_jalan_customer', 'cara_bayar'));
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'Nomor' => 'required',
            // 'IDSJC' => 'required',
            'IDCustomer' => 'required',
            'Tanggal' => 'required||date_format:"d/m/Y"',
            // 'Tanggal_jatuh_tempo' => 'required||date_format:"d/m/Y"',
            'Total_qty' => 'required|numeric',
            'total_invoice_diskon' => 'required',
            'Status_ppn' => 'required',
            // 'Nama_di_faktur' => 'required',
            'Keterangan' => 'nullable',
            'DPP' => 'required',
            'PPN1' => 'required',
        ])->setAttributeNames([
            'Nomor' => 'Nomor',
            // 'IDSJC' => 'Nomor Surat Jalan',
            'IDCustomer' => 'Customer',
            'Tanggal' => 'Tanggal',
            // 'Tanggal_jatuh_tempo' => 'Tanggal Jatuh Tempo',
            'Total_qty' => 'Total Qty',
            'total_invoice_diskon' => 'Grand Total',
            'Status_ppn' => 'Jenis PPN',
            'Nama_di_faktur' => 'Nama Di Faktur',
            'Keterangan' => 'Keterangan',
            'DPP' => 'DPP',
            'PPN1' => 'PPN',
        ]);

        if ($validate->fails()) {
            $data = [
                'status' => false,
                'message' => strip_tags($validate->errors()->first()),
            ];
            return json_encode($data);
        }

        $IDFJ = uniqid();
        // $surat_jalan = SuratJalanModel::findOrfail($request->IDSJC);
        // $sales_order = SalesOrderModel::findOrfail($surat_jalan->idso);

        $Nomor = $this->_cek_nomor($request->Nomor, $request->Status_ppn);

        DB::beginTransaction();
        $penjualan = new PenjualanModel();

        if ($request->Status_ppn == 'include') {
            $Penjualan_PPN = AppHelper::PpnInclude(AppHelper::StrReplace($request->DPP));
            $Penjualan_DPP = floatval(AppHelper::StrReplace($request->DPP)) - floatval($Penjualan_PPN);
        } else {
            $Penjualan_PPN = AppHelper::StrReplace($request->PPN);
            $Penjualan_DPP = floatval(AppHelper::StrReplace($request->DPP));
        }

        $IDSJC = uniqid();
        $penjualan->IDFJ = $IDFJ;
        $penjualan->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $penjualan->Nomor = $Nomor;
        $penjualan->IDCustomer = $request->IDCustomer;
        $penjualan->Nama_di_faktur = $request->Nama_di_faktur;
        $penjualan->no_pajak = $request->Nomor_pajak;
        $penjualan->$IDSJC;
        // $penjualan->Tanggal_jatuh_tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $penjualan->IDMataUang = $request->IDMataUang;
        $penjualan->Kurs = $request->Kurs;
        $penjualan->Total_qty = $request->Total_qty;
        $penjualan->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $penjualan->Grand_total = AppHelper::StrReplace($request->total_invoice_diskon);
        $penjualan->Status_ppn = $request->Status_ppn;
        $penjualan->Keterangan = $request->Keterangan;
        $penjualan->Batal = 0;
        $penjualan->jenis_ppn = $request->Status_ppn == 'include' ? '1' : '0';

        $penjualan->DPP = $Penjualan_DPP;
        $penjualan->PPN = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;

        $penjualan->dibuat_pada = date('Y-m-d H:i:s');
        $penjualan->diubah_pada = date('Y-m-d H:i:s');

        $penjualan->save();

        foreach ($request->IDBarang as $key => $value) {
            $IDFJDetail = uniqid();
            $penjualan_detail = new PenjualanDetailModel;

            $penjualan_detail->IDFJDetail = $IDFJDetail;
            $penjualan_detail->IDFJ = $IDFJ;
            $penjualan_detail->IDBarang = $value;
            $penjualan_detail->Qty = $request->Qty[$key];
            $penjualan_detail->Saldo_qty = $request->Qty[$key];
            $penjualan_detail->IDSatuan = $request->IDSatuan[$key];
            $penjualan_detail->Harga = AppHelper::StrReplace($request->harga[$key]);
            $penjualan_detail->Sub_total = AppHelper::StrReplace($request->Sub_total[$key]);
            $penjualan_detail->diskon = $request->diskon[$key] ? AppHelper::StrReplace($request->diskon[$key]) : 0;

            $penjualan_detail->save();
        }

        // SAVE TO GRANDTOTAL //
        $penjualan_grand_total = new PenjualanGrandTotalModel;

        $penjualan_grand_total->IDFJGrandTotal = uniqid();
        $penjualan_grand_total->IDFJ = $IDFJ;
        $penjualan_grand_total->Pembayaran = '-';
        $penjualan_grand_total->IDMataUang = $request->IDMataUang;
        $penjualan_grand_total->Kurs = $request->Kurs;
        $penjualan_grand_total->DPP = $Penjualan_DPP;
        $penjualan_grand_total->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $penjualan_grand_total->PPN ? AppHelper::StrReplace($request->PPN) : 0;
        $penjualan_grand_total->Grand_total = AppHelper::StrReplace($request->total_invoice_diskon);
        $penjualan_grand_total->Sisa = 0;

        $penjualan_grand_total->save();

        // CEK UM //
        // $UM_Customer = UMCustomerModel::where('IDFaktur', $surat_jalan->idso)->whereNull('IDFJ')->first();

        // if ($UM_Customer) {
        //     $IDFJ_piutang = UMCustomerModel::findOrfail($UM_Customer->IDUMCustomer);

        //     $IDFJ_piutang->IDFJ = $IDFJ;

        //     $IDFJ_piutang->save();
        // }
        // KARTU STOK //
        $data_barang = BarangModel::find($value);
        if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
            $Qty_konversi = $request->Qty[$key];
        } else {
            $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

            $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty[$key];
        }

        $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
            ->where('IDGudang', '=', 'P000002')->first();

        if ($data_stok) {
            $stok = StokModel::find($data_stok->IDStok);

            $stok->Qty_pcs = $data_stok->Qty_pcs - $Qty_konversi;

            $stok->save();
        } else {
            $IDStok = uniqid();
            $stok = new StokModel;

            $stok->IDStok = $IDStok;
            $stok->Tanggal = date('Y-m-d H:i:s');
            $stok->Nomor_faktur = $Nomor;
            $stok->IDBarang = $request->IDBarang[$key];
            $stok->Qty_pcs = $Qty_konversi * -1;
            $stok->Nama_Barang = $data_barang->Nama_Barang;
            $stok->Jenis_faktur = 'FJ';
            $stok->IDGudang = 'P000002';
            $stok->IDSatuan = $data_barang->IDSatuan;

            $stok->save();
        }

        $kartu_stok = new KartustokModel;

        $kartu_stok->IDKartuStok = uniqid();
        $kartu_stok->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $kartu_stok->Nomor_faktur = $Nomor;
        $kartu_stok->IDFaktur = $IDFJ;
        $kartu_stok->IDFakturDetail = $IDFJDetail;
        $kartu_stok->IDStok = $data_stok ? $data_stok->IDStok : $IDStok;
        $kartu_stok->Jenis_faktur = 'SJ';
        $kartu_stok->IDBarang = $request->IDBarang[$key];
        $kartu_stok->Harga = AppHelper::StrReplace($request->harga[$key]);
        $kartu_stok->Nama_Barang = $data_barang->Nama_Barang;
        $kartu_stok->Keluar_pcs = $request->Qty[$key];
        $kartu_stok->IDSatuan = $request->IDSatuan[$key];

        $kartu_stok->save();
// END KARTU STOK //

        // SAVE TO PIUTANG //
        $piutang = new PiutangModel;

        $piutang->IDPiutang = uniqid();
        $piutang->IDFaktur = $IDFJ;
        $piutang->IDCustomer = $request->IDCustomer;
        $piutang->Tanggal_Piutang = AppHelper::DateFormat($request->Tanggal);
        // $piutang->Jatuh_Tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $piutang->No_Faktur = $Nomor;
        $piutang->Nilai_Piutang = AppHelper::StrReplace($request->total_invoice_diskon);
        $piutang->Saldo_Awal = AppHelper::StrReplace($request->total_invoice_diskon);
        // $piutang->UM = $UM_Customer ? $UM_Customer->Nilai_UM : 0;
        $piutang->Pembayaran = floatval($request->pembayaran ? AppHelper::StrReplace($request->pembayaran) : 0) + floatval($piutang->UM);
        $piutang->Saldo_Akhir = floatval(AppHelper::StrReplace($request->total_invoice_diskon)) - floatval($piutang->Pembayaran);
        $piutang->Jenis_Faktur = 'FJ';
        $piutang->Retur = 0;

        $piutang->save();

        if ($piutang->Pembayaran == 0) {
            // SAVE JURNAL DEBET //

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'debet')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor = $Nomor;
            $jurnal_penjualan_debet->IDFaktur = $IDFJ;
            $jurnal_penjualan_debet->IDFakturDetail = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
            $jurnal_penjualan_debet->IDCOA = $CoaDebet->IDCoa;
            $jurnal_penjualan_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Kredit = 0;
            $jurnal_penjualan_debet->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Total_kredit = 0;
            $jurnal_penjualan_debet->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_debet->save();

            // SAVE JURNAL KREDIT //

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor = $Nomor;
            $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit->IDFakturDetail = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit->IDCOA = $CoaKredit->IDCoa;
            $jurnal_penjualan_kredit->Debet = 0;
            $jurnal_penjualan_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet = 0;
            $jurnal_penjualan_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_kredit->save();
        } else {

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'debet')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'lunas')
                ->first();

            // SAVE JURNAL DEBET //
            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor = $Nomor;
            $jurnal_penjualan_debet->IDFaktur = $IDFJ;
            $jurnal_penjualan_debet->IDFakturDetail = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
            $jurnal_penjualan_debet->IDCOA = $CoaDebet->IDCoa;
            $jurnal_penjualan_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Kredit = 0;
            $jurnal_penjualan_debet->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Total_kredit = 0;
            $jurnal_penjualan_debet->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_debet->save();

            // SAVE JURNAL KREDIT //
            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'lunas')
                ->first();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor = $Nomor;
            $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit->IDFakturDetail = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit->IDCOA = $CoaKredit->IDCoa;
            $jurnal_penjualan_kredit->Debet = 0;
            $jurnal_penjualan_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet = 0;
            $jurnal_penjualan_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_kredit->save();
        }

        if ($request->Status_ppn == 'exclude') {
            // SAVE JURNAL PPN KREDIT //

            $CoaKreditPPN = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'tambahan')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_kredit_ppn = new JurnalModel;

            $jurnal_penjualan_kredit_ppn->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit_ppn->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit_ppn->Nomor = $Nomor;
            $jurnal_penjualan_kredit_ppn->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit_ppn->IDFakturDetail = '-';
            $jurnal_penjualan_kredit_ppn->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit_ppn->IDCOA = $CoaKreditPPN->IDCoa;
            $jurnal_penjualan_kredit_ppn->Debet = 0;
            $jurnal_penjualan_kredit_ppn->Kredit = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;
            $jurnal_penjualan_kredit_ppn->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit_ppn->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit_ppn->Total_debet = 0;
            $jurnal_penjualan_kredit_ppn->Total_kredit = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;
            $jurnal_penjualan_kredit_ppn->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit_ppn->Saldo = $request->PPN ? AppHelper::StrReplace($request->PPN) : 0;

            $jurnal_penjualan_kredit_ppn->save();
        }

        if ($request->Jenis_Pembayaran) {
            $pembayaran = new PenjualanPembayaranModel;

            $pembayaran->IDFJPembayaran = uniqid();
            $pembayaran->IDFJ = $IDFJ;
            $pembayaran->Jenis_pembayaran = $request->Jenis_Pembayaran;
            $pembayaran->IDCOA = $request->IDCoa;
            $pembayaran->NominalPembayaran = AppHelper::StrReplace($request->pembayaran);
            $pembayaran->IDMataUang = 1;
            $pembayaran->Kurs = 14000;
            $pembayaran->Tanggal_giro = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
            $pembayaran->Nomor_giro = $request->Nomor_giro;

            $pembayaran->dibuat_pada = date('Y-m-d H:i:s');
            $pembayaran->diubah_pada = date('Y-m-d H:i:s');

            $pembayaran->save();

            if ($request->Jenis_Pembayaran == 'giro') {
                $giro = new GiroModel;

                $giro->IDGiro = uniqid();
                $giro->IDFaktur = $IDFJ;
                $giro->IDPerusahaan = $request->IDCustomer;
                $giro->Jenis_faktur = 'FJ';
                $giro->Nomor_faktur = $Nomor;
                $giro->Nomor_giro = $request->Nomor_giro;
                $giro->Tanggal_giro = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $giro->Tanggal_faktur = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai = AppHelper::StrReplace($request->pembayaran);

                $giro->save();
            }
        }

        DB::commit();

        $data = array(
            'status' => true,
            'message' => 'Data berhasil disimpan.',
            'data' => [
                'id' => $IDFJ,
            ],
        );

        return json_encode($data);
    }

    public function update_data2(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'Nomor' => 'required',
        ])->setAttributeNames([
            'Nomor' => 'Nomor',
        ]);

        if ($validate->fails()) {
            $data = [
                'status' => false,
                'message' => strip_tags($validate->errors()->first()),
            ];
            return json_encode($data);
        }

        // $IDFJ = uniqid();
        // $surat_jalan = SuratJalanModel::findOrfail($request->IDSJC);
        // $sales_order = SalesOrderModel::findOrfail($surat_jalan->idso);

        // $Nomor = $this->_cek_nomor($request->Nomor, $request->Status_ppn);

        DB::beginTransaction();
        // $penjualan = new PenjualanModel();
        $Nomor = $request->Nomor;
        $IDFJ = $request->IDFJ;
        $penjualan = InvoicePenjualanModel::findOrfail($request->IDFJ);

        if ($request->Status_ppn == 'include') {
            $Penjualan_PPN = AppHelper::PpnInclude(AppHelper::StrReplace($request->DPP));
            $Penjualan_DPP = floatval(AppHelper::StrReplace($request->DPP)) - floatval($Penjualan_PPN);
        } else {
            $Penjualan_PPN = AppHelper::StrReplace($request->PPN);
            $Penjualan_DPP = floatval(AppHelper::StrReplace($request->DPP));
        }

        $IDSJC = uniqid();
        // $penjualan->IDFJ = $IDFJ;
        $penjualan->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $penjualan->Nomor = $Nomor;
        $penjualan->IDCustomer = $request->IDCustomer;
        $penjualan->Nama_di_faktur = $request->Nama_di_faktur;
        $penjualan->no_pajak = $request->Nomor_pajak;
        $penjualan->$IDSJC;
        // $penjualan->Tanggal_jatuh_tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $penjualan->IDMataUang = $request->IDMataUang;
        $penjualan->Kurs = $request->Kurs;
        $penjualan->Total_qty = $request->Total_qty;
        $penjualan->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $penjualan->Grand_total = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
        $penjualan->Status_ppn = $request->Status_ppn;
        $penjualan->Keterangan = $request->Keterangan;
        $penjualan->Batal = 0;
        $penjualan->jenis_ppn = $request->Status_ppn == 'include' ? '1' : '0';

        $penjualan->DPP = $Penjualan_DPP;
        $penjualan->PPN = $Penjualan_PPN;

        $penjualan->dibuat_pada = date('Y-m-d H:i:s');
        $penjualan->diubah_pada = date('Y-m-d H:i:s');

        $penjualan->save();
        $penjualan_detail_exist = PenjualanDetailModel::where('IDFJ', $penjualan->IDFJ)->delete();

        foreach ($request->IDBarang as $key => $value) {
            $IDFJDetail = uniqid();
            $penjualan_detail = new PenjualanDetailModel;

            $penjualan_detail->IDFJDetail = $IDFJDetail;
            $penjualan_detail->IDFJ = $IDFJ;
            $penjualan_detail->IDBarang = $value;
            $penjualan_detail->Qty = $request->Qty[$key];
            $penjualan_detail->Saldo_qty = $request->Qty[$key];
            $penjualan_detail->IDSatuan = $request->IDSatuan[$key];
            $penjualan_detail->Harga = AppHelper::StrReplace($request->harga[$key]);
            $penjualan_detail->Sub_total = AppHelper::StrReplace($request->Sub_total[$key]);
            $penjualan_detail->diskon = $request->diskon[$key] ? AppHelper::StrReplace($request->diskon[$key]) : 0;

            $penjualan_detail->save();
        }

        // SAVE TO GRANDTOTAL //
        $pembelian_grand_total_exist = PenjualanGrandTotalModel::where('IDFJ', $penjualan->IDFJ)->delete();

        $penjualan_grand_total = new PenjualanGrandTotalModel;

        $penjualan_grand_total->IDFJGrandTotal = uniqid();
        $penjualan_grand_total->IDFJ = $IDFJ;
        $penjualan_grand_total->Pembayaran = '-';
        $penjualan_grand_total->IDMataUang = $request->IDMataUang;
        $penjualan_grand_total->Kurs = $request->Kurs;
        $penjualan_grand_total->DPP = $Penjualan_DPP;
        $penjualan_grand_total->Discount = $request->diskonrupiah ? AppHelper::StrReplace($request->diskonrupiah) : 0;
        $penjualan_grand_total->PPN = $Penjualan_PPN;
        $penjualan_grand_total->Grand_total = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
        $penjualan_grand_total->Sisa = 0;

        $penjualan_grand_total->save();

        // CEK UM //
        // $UM_Customer = UMCustomerModel::where('IDFaktur', $surat_jalan->idso)->whereNull('IDFJ')->first();

        // if ($UM_Customer) {
        //     $IDFJ_piutang = UMCustomerModel::findOrfail($UM_Customer->IDUMCustomer);

        //     $IDFJ_piutang->IDFJ = $IDFJ;

        //     $IDFJ_piutang->save();
        // }
        // KARTU STOK //
        $data_barang = BarangModel::find($value);
        if ($data_barang->IDSatuan == $request->IDSatuan[$key]) {
            $Qty_konversi = $request->Qty[$key];
        } else {
            $data_satuan_konversi = SatuanKonversiModel::where('IDBarang', $value)->where('IDSatuanBesar', $request->IDSatuan[$key])->first();

            $Qty_konversi = $data_satuan_konversi->Qty * $request->Qty[$key];
        }

        $data_stok = StokModel::where('IDBarang', $request->IDBarang[$key])
            ->where('IDGudang', '=', 'P000002')->first();

        if ($data_stok) {
            $stok = StokModel::find($data_stok->IDStok);

            $stok->Qty_pcs = $data_stok->Qty_pcs - $Qty_konversi;

            $stok->save();
        } else {
            $IDStok = uniqid();
            $stok = new StokModel;

            $stok->IDStok = $IDStok;
            $stok->Tanggal = date('Y-m-d H:i:s');
            $stok->Nomor_faktur = $Nomor;
            $stok->IDBarang = $request->IDBarang[$key];
            $stok->Qty_pcs = $Qty_konversi * -1;
            $stok->Nama_Barang = $data_barang->Nama_Barang;
            $stok->Jenis_faktur = 'FJ';
            $stok->IDGudang = 'P000002';
            $stok->IDSatuan = $data_barang->IDSatuan;

            $stok->save();
        }

        $kartu_stok = new KartustokModel;

        $kartu_stok->IDKartuStok = uniqid();
        $kartu_stok->Tanggal = AppHelper::DateFormat($request->Tanggal);
        $kartu_stok->Nomor_faktur = $Nomor;
        $kartu_stok->IDFaktur = $IDFJ;
        $kartu_stok->IDFakturDetail = $IDFJDetail;
        $kartu_stok->IDStok = $data_stok ? $data_stok->IDStok : $IDStok;
        $kartu_stok->Jenis_faktur = 'SJ';
        $kartu_stok->IDBarang = $request->IDBarang[$key];
        $kartu_stok->Harga = AppHelper::StrReplace($request->harga[$key]);
        $kartu_stok->Nama_Barang = $data_barang->Nama_Barang;
        $kartu_stok->Keluar_pcs = $request->Qty[$key];
        $kartu_stok->IDSatuan = $request->IDSatuan[$key];

        $kartu_stok->save();
// END KARTU STOK //

        // SAVE TO PIUTANG //
        $piutang = new PiutangModel;

        $piutang->IDPiutang = uniqid();
        $piutang->IDFaktur = $IDFJ;
        $piutang->IDCustomer = $request->IDCustomer;
        $piutang->Tanggal_Piutang = AppHelper::DateFormat($request->Tanggal);
        // $piutang->Jatuh_Tempo = AppHelper::DateFormat($request->Tanggal_jatuh_tempo);
        $piutang->No_Faktur = $Nomor;
        $piutang->Nilai_Piutang = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
        $piutang->Saldo_Awal = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
        // $piutang->UM = $UM_Customer ? $UM_Customer->Nilai_UM : 0;
        $piutang->Pembayaran = floatval($request->pembayaran ? AppHelper::StrReplace($request->pembayaran) : 0) + floatval($piutang->UM);
        $piutang->Saldo_Akhir = floatval(AppHelper::StrReplace($request->total_invoice_diskon)) - floatval($piutang->Pembayaran);
        $piutang->Jenis_Faktur = 'FJ';
        $piutang->Retur = 0;

        $piutang->save();

        if ($piutang->Pembayaran == 0) {
            // SAVE JURNAL DEBET //

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'debet')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor = $Nomor;
            $jurnal_penjualan_debet->IDFaktur = $IDFJ;
            $jurnal_penjualan_debet->IDFakturDetail = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
            $jurnal_penjualan_debet->IDCOA = $CoaDebet->IDCoa;
            $jurnal_penjualan_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Kredit = 0;
            $jurnal_penjualan_debet->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Total_kredit = 0;
            $jurnal_penjualan_debet->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_debet->save();

            // SAVE JURNAL KREDIT //

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor = $Nomor;
            $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit->IDFakturDetail = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit->IDCOA = $CoaKredit->IDCoa;
            $jurnal_penjualan_kredit->Debet = 0;
            $jurnal_penjualan_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet = 0;
            $jurnal_penjualan_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_kredit->save();
        } else {

            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'debet')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'lunas')
                ->first();

            // SAVE JURNAL DEBET //
            $jurnal_penjualan_debet = new JurnalModel;

            $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_debet->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_debet->Nomor = $Nomor;
            $jurnal_penjualan_debet->IDFaktur = $IDFJ;
            $jurnal_penjualan_debet->IDFakturDetail = '-';
            $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
            $jurnal_penjualan_debet->IDCOA = $CoaDebet->IDCoa;
            $jurnal_penjualan_debet->Debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Kredit = 0;
            $jurnal_penjualan_debet->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_debet->Kurs = $request->Kurs;
            $jurnal_penjualan_debet->Total_debet = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_debet->Total_kredit = 0;
            $jurnal_penjualan_debet->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_debet->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_debet->save();

            // SAVE JURNAL KREDIT //
            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'utama')
                ->where('Cara', '=', 'lunas')
                ->first();

            $jurnal_penjualan_kredit = new JurnalModel;

            $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit->Nomor = $Nomor;
            $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit->IDFakturDetail = '-';
            $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit->IDCOA = $CoaKredit->IDCoa;
            $jurnal_penjualan_kredit->Debet = 0;
            $jurnal_penjualan_kredit->Kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit->Total_debet = 0;
            $jurnal_penjualan_kredit->Total_kredit = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;
            $jurnal_penjualan_kredit->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit->Saldo = $request->total_invoice_diskon ? AppHelper::StrReplace($request->total_invoice_diskon) : 0;

            $jurnal_penjualan_kredit->save();
        }

        if ($request->Status_ppn == 'exclude') {
            // SAVE JURNAL PPN KREDIT //

            $CoaKreditPPN = SettingCOAModel::where('IDMenu', '=', '80')
                ->where('Posisi', '=', 'kredit')
                ->where('Tingkat', '=', 'tambahan')
                ->where('Cara', '=', 'utang')
                ->first();

            $jurnal_penjualan_kredit_ppn = new JurnalModel;

            $jurnal_penjualan_kredit_ppn->IDJurnal = $this->number_jurnal();
            $jurnal_penjualan_kredit_ppn->Tanggal = AppHelper::DateFormat($request->Tanggal);
            $jurnal_penjualan_kredit_ppn->Nomor = $Nomor;
            $jurnal_penjualan_kredit_ppn->IDFaktur = $IDFJ;
            $jurnal_penjualan_kredit_ppn->IDFakturDetail = '-';
            $jurnal_penjualan_kredit_ppn->Jenis_faktur = 'FJ';
            $jurnal_penjualan_kredit_ppn->IDCOA = $CoaKreditPPN->IDCoa;
            $jurnal_penjualan_kredit_ppn->Debet = 0;
            $jurnal_penjualan_kredit_ppn->Kredit = AppHelper::StrReplace($request->PPN);
            $jurnal_penjualan_kredit_ppn->IDMataUang = $request->IDMataUang;
            $jurnal_penjualan_kredit_ppn->Kurs = $request->Kurs;
            $jurnal_penjualan_kredit_ppn->Total_debet = 0;
            $jurnal_penjualan_kredit_ppn->Total_kredit = AppHelper::StrReplace($request->PPN);
            $jurnal_penjualan_kredit_ppn->Keterangan = 'Invoice Penjualan No. ' . $Nomor;
            $jurnal_penjualan_kredit_ppn->Saldo = AppHelper::StrReplace($request->PPN);

            $jurnal_penjualan_kredit_ppn->save();
        }

        if ($request->Jenis_Pembayaran) {
            $pembayaran = new PenjualanPembayaranModel;

            $pembayaran->IDFJPembayaran = uniqid();
            $pembayaran->IDFJ = $IDFJ;
            $pembayaran->Jenis_pembayaran = $request->Jenis_Pembayaran;
            $pembayaran->IDCOA = $request->IDCoa;
            $pembayaran->NominalPembayaran = AppHelper::StrReplace($request->pembayaran);
            $pembayaran->IDMataUang = 1;
            $pembayaran->Kurs = 14000;
            $pembayaran->Tanggal_giro = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
            $pembayaran->Nomor_giro = $request->Nomor_giro;

            $pembayaran->dibuat_pada = date('Y-m-d H:i:s');
            $pembayaran->diubah_pada = date('Y-m-d H:i:s');

            $pembayaran->save();

            if ($request->Jenis_Pembayaran == 'giro') {
                $giro = new GiroModel;

                $giro->IDGiro = uniqid();
                $giro->IDFaktur = $IDFJ;
                $giro->IDPerusahaan = $request->IDCustomer;
                $giro->Jenis_faktur = 'FJ';
                $giro->Nomor_faktur = $Nomor;
                $giro->Nomor_giro = $request->Nomor_giro;
                $giro->Tanggal_giro = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $giro->Tanggal_faktur = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai = AppHelper::StrReplace($request->pembayaran);

                $giro->save();
            }
        }

        DB::commit();

        $data = array(
            'status' => true,
            'message' => 'Data berhasil disimpan.',
            'data' => [
                'id' => $IDFJ,
            ],
        );

        return json_encode($data);
    }

    public function show($id)
    {
        $iduser = Auth::user()->id;
        $akses = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if ($akses == null) {
            return redirect('InvoicePenjualan')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penjualan = VListPenjualanModel::findOrfail($id);
        $customer = DB::table('tbl_customer')->orderBy('Nama', 'asc')->get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();
        // $gudang = GudangModel::whereNotIn('Kode_Gudang', ['GBJ'])->get();
        $purchase_order = DB::table('tbl_penjualan')
            ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_penjualan.IDMataUang')
            ->join('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_penjualan.IDCustomer')
            ->join("tbl_penjualan_detail", "tbl_penjualan_detail.IDFJ", "=", "tbl_penjualan.IDFJ")
            ->join("tbl_penjualan_grand_total", "tbl_penjualan_grand_total.IDFJ", "=", "tbl_penjualan.IDFJ")
            ->where('tbl_penjualan.IDFJ', $id)
            ->select('tbl_penjualan.*', 'tbl_customer.Nama', 'tbl_mata_uang.Mata_uang', DB::raw('SUM(tbl_penjualan_detail."Sub_total") AS total_harga'), 'tbl_customer.Nama', 'tbl_penjualan_grand_total.DPP', 'tbl_penjualan_grand_total.Discount as Diskon', 'tbl_penjualan_grand_total.PPN')
            ->groupBy("tbl_penjualan.IDFJ", "tbl_penjualan.Tanggal", "tbl_penjualan.Nomor", "tbl_penjualan.IDSJC", "tbl_penjualan.IDCustomer", "tbl_penjualan.Nama_di_faktur",
                "tbl_penjualan.Batal", "tbl_penjualan.TOP", "tbl_penjualan.Discount", "tbl_penjualan.Tanggal_jatuh_tempo", "tbl_penjualan.IDMataUang", "tbl_penjualan.Kurs",
                "tbl_penjualan.Total_qty", "tbl_penjualan.Status_ppn", "tbl_penjualan.Grand_total", "tbl_penjualan.DPP", "tbl_penjualan.PPN", "tbl_penjualan.dibuat_pada", "tbl_penjualan.diubah_pada",
                'tbl_customer.Nama', 'tbl_penjualan_grand_total.DPP', 'tbl_penjualan_grand_total.Discount', 'tbl_penjualan_grand_total.PPN', "tbl_penjualan.Keterangan", "tbl_penjualan.jenis_ppn", "tbl_penjualan.no_pajak", "tbl_penjualan.Status_pakai", "tbl_customer.Nama", "tbl_penjualan.IDMataUang", "tbl_mata_uang.Mata_uang", "tbl_mata_uang.Kode")
            ->first();

        $purchase_order_detail = DB::table('tbl_penjualan_detail')
            ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_penjualan_detail.IDBarang')
            ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_penjualan_detail.IDSatuan')
            ->join('tbl_satuan as satuan_kecil', 'satuan_kecil.IDSatuan', '=', 'tbl_barang.IDSatuan')
            ->where('tbl_penjualan_detail.IDFJ', $id)
            ->select('tbl_penjualan_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang', 'tbl_barang.IDSatuan as IDSatuanKecil', 'satuan_kecil.Satuan as Satuan_kecil')
            ->get();
        $um_supplier = DB::table('tbl_um_supplier')
            ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor', '=', 'tbl_um_supplier.Nomor_Faktur')
            ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_um_supplier.IDCOA')
            ->where('tbl_purchase_order.IDPO', $id)
            ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
            ->first();

        $penjualan_pembayaran = PenjualanPembayaranModel::where('IDFJ', $penjualan->IDFJ)->first();
        $retur_penjualan = ReturPenjualanModel::where('IDFJ', $penjualan->IDFJ)->where('Batal', 0)->first();

        $penjualan_detail = VListPenjualanDetailModel::where('IDFJ', $penjualan->IDFJ)->get();
        $cara_bayar = CaraBayarModel::GetFJ();
        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);

        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        $barang = $barangBuilder->get();

        return view('invoicepenjualan.update', compact('coreset', 'akses', 'um_supplier', 'customer', 'mata_uang', 'bank', 'barang', 'purchase_order', 'purchase_order_detail', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penjualan', 'penjualan_detail', 'penjualan_pembayaran', 'retur_penjualan', 'cara_bayar'));
    }

    public function detail($id)
    {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penjualan = VListPenjualanModel::findOrfail($id);

        $penjualan_detail = VListPenjualanDetailModel::where('IDFJ', $penjualan->IDFJ)->get();
        $penjualan_pembayaran = PenjualanPembayaranModel::where('IDFJ', $penjualan->IDFJ)->first();

        return view('invoicepenjualan.detail', compact('coreset', 'aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'penjualan', 'penjualan_detail', 'penjualan_pembayaran'));
    }

    function print($id) {
        $iduser = Auth::user()->id;
        $namauser = Auth::user()->name;
        $tahun = date('Y');
        $aktif = 'aktif';
        $aksesmenu = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset = app('App\Http\Controllers\Aksesmenu')->coreset();
        $penjualan = VListPenjualanModel::findOrfail($id);

        $UMCustomer = DB::table('tbl_penjualan')->select(
            'tbl_penjualan.IDFJ',
            'tbl_penjualan.Tanggal',
            'tbl_penjualan.IDSJC',
            'tbl_sales_order.IDSOK',
            'tbl_um_customer.Nilai_UM')->leftJoin('tbl_surat_jalan_customer', 'tbl_penjualan.IDSJC', '=', 'tbl_surat_jalan_customer.idsjc')->leftJoin('tbl_sales_order', 'tbl_sales_order.IDSOK', '=', 'tbl_surat_jalan_customer.idso')->leftJoin('tbl_um_customer', 'tbl_um_customer.IDFaktur', '=', 'tbl_sales_order.IDSOK')->where('tbl_penjualan.IDFJ', $id)->first();

        $penjualan_detail = VListPenjualanDetailModel::where('IDFJ', $penjualan->IDFJ)->get();
        $perusahaan = PerusahaanModel::Getforprint();

        $penjualan_pembayaran = PenjualanPembayaranModel::where('IDFJ', $penjualan->IDFJ)->first();

        return view('invoicepenjualan.print', compact('coreset', 'aksesmenu', 'perusahaan', 'aksesmenudetail', 'aksessetting', 'namauser', 'penjualan', 'penjualan_detail', 'UMCustomer', 'penjualan_pembayaran'));
    }

    public function destroy($id)
    {
        DB::beginTransaction();

        $penjualan = PenjualanModel::findOrfail($id);

        $IDFJ = $penjualan->IDFJ;

        if ($penjualan->Batal == 1) {
            // $data_exist = PenjualanModel::where('IDSJC', $penjualan->IDSJC)->where('Batal', 0)->whereNotIn('IDFJ', [$penjualan->IDFJ])->first();
            // if ($data_exist) {
            //     $status = false;
            //     $message = 'Data tidak bisa diaktifkan, Surat Jalan telah digunakan di Faktur lain yang masih aktif.';
            // } else {
            //     $penjualan->Batal = 0;
            //     $message = 'Data berhasil diaktifkan.';
            // }

            $data = array(
                'status' => false,
                'message' => 'Data tidak bisa diaktifkan.',
            );

            return json_encode($data);
        } else {
            $data_retur = ReturPenjualanModel::where('IDFJ', $penjualan->IDFJ)->where('Batal', 0)->first();
            if ($data_retur) {
                $status = false;
                $message = 'Data tidak bisa dibatalkan, Nomor penjualan telah dilakukan retur.';
            } else {
                $penjualan->Batal = 1;

                $penjualan_grand_total = PenjualanGrandTotalModel::where('IDFJ', $penjualan->IDFJ)->update(['Batal' => 1]);

                $penjualan_pembayaran = PenjualanPembayaranModel::where('IDFJ', $penjualan->IDFJ)->update(['Batal' => 1]);

                $surat_jalan = SuratJalanModel::findOrfail($penjualan->IDSJC);
                $sales_order = SalesOrderModel::findOrfail($surat_jalan->idso);

                $piutang_exist = PiutangModel::where('IDFaktur', $IDFJ)->first();
                $piutang = PiutangModel::find($piutang_exist->IDPiutang);
                $piutang->Batal = 1;

                if ($piutang->Pembayaran == 0) {
                    // SAVE JURNAL KREDIT //
                    $jurnal_penjualan_kredit = new JurnalModel;

                    $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
                    $jurnal_penjualan_kredit->Tanggal = $penjualan->Tanggal;
                    $jurnal_penjualan_kredit->Nomor = $penjualan->Nomor;
                    $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
                    $jurnal_penjualan_kredit->IDFakturDetail = '-';
                    $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
                    $jurnal_penjualan_kredit->IDCOA = 'P000001';
                    $jurnal_penjualan_kredit->Debet = 0;
                    $jurnal_penjualan_kredit->Kredit = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_kredit->IDMataUang = $sales_order->IDMataUang;
                    $jurnal_penjualan_kredit->Kurs = $sales_order->Kurs;
                    $jurnal_penjualan_kredit->Total_debet = 0;
                    $jurnal_penjualan_kredit->Total_kredit = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_kredit->Keterangan = $penjualan->Keterangan;
                    $jurnal_penjualan_kredit->Saldo = AppHelper::StrReplace($penjualan->Grand_total);

                    $jurnal_penjualan_kredit->save();

                    // SAVE JURNAL debet //
                    $jurnal_penjualan_debet = new JurnalModel;

                    $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
                    $jurnal_penjualan_debet->Tanggal = $penjualan->Tanggal;
                    $jurnal_penjualan_debet->Nomor = $penjualan->Nomor;
                    $jurnal_penjualan_debet->IDFaktur = $IDFJ;
                    $jurnal_penjualan_debet->IDFakturDetail = '-';
                    $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
                    $jurnal_penjualan_debet->IDCOA = 'P000048';
                    $jurnal_penjualan_debet->Debet = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_debet->Kredit = 0;
                    $jurnal_penjualan_debet->IDMataUang = $sales_order->IDMataUang;
                    $jurnal_penjualan_debet->Kurs = $sales_order->Kurs;
                    $jurnal_penjualan_debet->Total_debet = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_debet->Total_kredit = 0;
                    $jurnal_penjualan_debet->Keterangan = $penjualan->Keterangan;
                    $jurnal_penjualan_debet->Saldo = AppHelper::StrReplace($penjualan->Grand_total);

                    $jurnal_penjualan_debet->save();
                } else {
                    // SAVE JURNAL KREDIT //
                    $jurnal_penjualan_kredit = new JurnalModel;

                    $jurnal_penjualan_kredit->IDJurnal = $this->number_jurnal();
                    $jurnal_penjualan_kredit->Tanggal = $penjualan->Tanggal;
                    $jurnal_penjualan_kredit->Nomor = $penjualan->Nomor;
                    $jurnal_penjualan_kredit->IDFaktur = $IDFJ;
                    $jurnal_penjualan_kredit->IDFakturDetail = '-';
                    $jurnal_penjualan_kredit->Jenis_faktur = 'FJ';
                    $jurnal_penjualan_kredit->IDCOA = 'P000004';
                    $jurnal_penjualan_kredit->Debet = 0;
                    $jurnal_penjualan_kredit->Kredit = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_kredit->IDMataUang = $sales_order->IDMataUang;
                    $jurnal_penjualan_kredit->Kurs = $sales_order->Kurs;
                    $jurnal_penjualan_kredit->Total_debet = 0;
                    $jurnal_penjualan_kredit->Total_kredit = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_kredit->Keterangan = $penjualan->Keterangan;
                    $jurnal_penjualan_kredit->Saldo = AppHelper::StrReplace($penjualan->Grand_total);

                    $jurnal_penjualan_kredit->save();

                    // SAVE JURNAL DEBET //
                    $jurnal_penjualan_debet = new JurnalModel;

                    $jurnal_penjualan_debet->IDJurnal = $this->number_jurnal();
                    $jurnal_penjualan_debet->Tanggal = $penjualan->Tanggal;
                    $jurnal_penjualan_debet->Nomor = $penjualan->Nomor;
                    $jurnal_penjualan_debet->IDFaktur = $IDFJ;
                    $jurnal_penjualan_debet->IDFakturDetail = '-';
                    $jurnal_penjualan_debet->Jenis_faktur = 'FJ';
                    $jurnal_penjualan_debet->IDCOA = 'P000048';
                    $jurnal_penjualan_debet->Debet = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_debet->Kredit = 0;
                    $jurnal_penjualan_debet->IDMataUang = $sales_order->IDMataUang;
                    $jurnal_penjualan_debet->Kurs = $sales_order->Kurs;
                    $jurnal_penjualan_debet->Total_debet = AppHelper::StrReplace($penjualan->Grand_total);
                    $jurnal_penjualan_debet->Total_kredit = 0;
                    $jurnal_penjualan_debet->Keterangan = $penjualan->Keterangan;
                    $jurnal_penjualan_debet->Saldo = AppHelper::StrReplace($penjualan->Grand_total);

                    $jurnal_penjualan_debet->save();
                }

                $piutang->save();

                if ($penjualan->Status_ppn == 'exclude') {
                    // SAVE JURNAL PPN DEBET //
                    $jurnal_penjualan_debet_ppn = new JurnalModel;

                    $jurnal_penjualan_debet_ppn->IDJurnal = $this->number_jurnal();
                    $jurnal_penjualan_debet_ppn->Tanggal = $penjualan->Tanggal;
                    $jurnal_penjualan_debet_ppn->Nomor = $Nomor;
                    $jurnal_penjualan_debet_ppn->IDFaktur = $IDFJ;
                    $jurnal_penjualan_debet_ppn->IDFakturDetail = '-';
                    $jurnal_penjualan_debet_ppn->Jenis_faktur = 'FJ';
                    $jurnal_penjualan_debet_ppn->IDCOA = 'P000033';
                    $jurnal_penjualan_debet_ppn->Debet = AppHelper::StrReplace($penjualan->PPN);
                    $jurnal_penjualan_debet_ppn->Kredit = 0;
                    $jurnal_penjualan_debet_ppn->IDMataUang = $sales_order->IDMataUang;
                    $jurnal_penjualan_debet_ppn->Kurs = $sales_order->Kurs;
                    $jurnal_penjualan_debet_ppn->Total_debet = AppHelper::StrReplace($penjualan->PPN);
                    $jurnal_penjualan_debet_ppn->Total_kredit = 0;
                    $jurnal_penjualan_debet_ppn->Keterangan = $penjualan->Keterangan;
                    $jurnal_penjualan_debet_ppn->Saldo = AppHelper::StrReplace($penjualan->PPN);

                    $jurnal_penjualan_debet_ppn->save();
                }

                $message = 'Data berhasil dibatalkan.';
            }
        }

        $penjualan->save();

        DB::commit();

        $data = array(
            'status' => (isset($status)) ? $status : true,
            'message' => $message,
        );

        return json_encode($data);
    }

    public function number_invoice(Request $request)
    {
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = PenjualanModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');
        if ($request->Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        } else if ($request->Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        } else {
            $Status_ppn_kode = 0;
        }
        $queryBuilder->where('Status_ppn', $request->Status_ppn);
        $queryBuilder->where(DB::raw('substring("Nomor", 11, ' . strlen($bulan_romawi) . ')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (!$number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/FJ/' . $bulan_romawi . '/' . date('y');

        if ($request->ajax()) {
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    public function number_jurnal()
    {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();

        if ($nomor->nonext == '') {
            $nomor_baru = 'P000001';
        } else {
            $hasil5 = substr($nomor->nonext, 2, 6) + 1;
            $nomor_baru = 'P' . str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    public function get_surat_jalan(Request $request)
    {
        $surat_jalan = DB::table('tbl_surat_jalan_customer')
            ->leftJoin('tbl_customer', 'tbl_customer.IDCustomer', '=', 'tbl_surat_jalan_customer.idcustomer')
            ->where('idsjc', $request->id)
            ->first();

        $uang_muka = UMCustomerModel::where('IDFaktur', $surat_jalan->idso)->whereNull('IDFJ')->first();

        $data = [
            'surat_jalan' => $surat_jalan,
            'uang_muka' => $uang_muka,
        ];

        return json_encode($data);
    }

    public function get_surat_jalan_detail(Request $request)
    {
        $surat_jalan_detail = DB::table('tbl_surat_jalan_customer_detail')
            ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_surat_jalan_customer_detail.idbarang')
            ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_surat_jalan_customer_detail.idsatuan')
            ->where('idsjc', $request->get('idsjc'))
            ->get();

        return Datatables::of($surat_jalan_detail)->make(true);
    }

    public function get_coa(Request $request)
    {
        $jenis = $request->Jenis_Pembayaran;

        if ($jenis == 'transfer') {
            $data = DB::table('tbl_coa')
                ->where('Kode_COA', 'LIKE', '10103.%')
                ->get();
        } elseif ($jenis == 'giro') {
            $data = DB::table('tbl_coa')
                ->where('Kode_COA', 'LIKE', '10202')
                ->get();
        } elseif ($jenis == 'cash') {
            $data = DB::table('tbl_coa')
                ->where('Nama_COA', 'LIKE', 'Kas%')
                ->get();
        }

        return json_encode($data);
    }

    private function _cek_nomor($nomor, $Status_ppn)
    {
        $nomor_exist = PenjualanModel::where('Nomor', $nomor)->first();
        if ($nomor_exist) {
            $nomor_baru = $this->_generate_number($Status_ppn);
            $this->_cek_nomor($nomor_baru, $Status_ppn);
        } else {
            $nomor_baru = $nomor;
        }

        return $nomor_baru;
    }

    private function _generate_number($Status_ppn)
    {
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = PenjualanModel::selectRaw('max(substring("Nomor", 3, 4)) as kode');
        if ($Status_ppn == 'include') {
            $Status_ppn_kode = 1;
        }if ($Status_ppn == 'exclude') {
            $Status_ppn_kode = 0;
        }
        $queryBuilder->where('Status_ppn', $Status_ppn);
        $queryBuilder->where(DB::raw('substring("Nomor", 11, ' . strlen($bulan_romawi) . ')'), '=', $bulan_romawi);

        $number_exist = $queryBuilder->first();

        if (!$number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $Status_ppn_kode . '-' . $urutan . '/FJ/' . $bulan_romawi . '/' . date('y');

        return $kode;
    }
    public function get_barang()
    {
        DB::enableQueryLog();
        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        if (Input::get('q')) {
            $queryBuilder->where('Nama_Barang', 'iLike', '%' . Input::get('q') . '%');
        }

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_satuan()
    {
        $queryBuilder = VListSatuanKonversiModel::select('*');

        if (Input::get('q')) {
            $queryBuilder->where('Satuan', 'iLike', '%' . Input::get('q') . '%');
        }
        $queryBuilder->where('IDBarang', Input::get('IDBarang'));

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_harga_jual()
    {
        $queryBuilder = HargaJualModel::select('*');

        $queryBuilder->where('IDBarang', '=', Input::get('IDBarang'));
        $queryBuilder->where('IDSatuan', '=', Input::get('IDSatuan'));
        $queryBuilder->where('IDMataUang', '=', Input::get('IDMataUang'));

        $response = $queryBuilder->first();

        return json_encode($response);
    }

}
