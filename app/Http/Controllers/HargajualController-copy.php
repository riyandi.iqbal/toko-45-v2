<?php
/*=====Create DEDY @13/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;
use Datatables;
use App\Models\HargaJualModel;

class HargajualController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $hargajual          = DB::table('tbl_harga_jual_barang')
                                ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_harga_jual_barang.IDBarang')
                                ->join('tbl_satuan', 'tbl_satuan.IDSatuan','=','tbl_harga_jual_barang.IDSatuan')
                                ->join('tbl_group_customer', 'tbl_group_customer.IDGroupCustomer','=','tbl_harga_jual_barang.IDGroupCustomer')
                                ->select('tbl_barang.Nama_Barang', 'tbl_satuan.Satuan','tbl_harga_jual_barang.*', 'tbl_group_customer.Nama_Group_Customer')                
                                ->orderBy('tbl_barang.Nama_Barang', 'asc')
                                ->get();

        return view('hargajualbarang/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function datatable(Request $request) {
        $data = HargaJualModel::Getforindex();

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function tambah_data(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('HargaJualBarang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $barang             = DB::table('tbl_barang')->orderBy('Nama_Barang', 'asc')->get();
        $satuan             = DB::table('tbl_satuan')->orderBy('Satuan', 'asc')->get();
        $groupcustomer      = DB::table('tbl_group_customer')->orderBy('Nama_Group_Customer', 'asc')->get();  

        return view('hargajualbarang/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'barang', 'satuan', 'groupcustomer'));
    }

    public function simpandata(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'data.Barang'             => 'required',
            'data.Satuan'                  => 'required',
            'data.Group'                  => 'required',
            'data.Modal'                  => 'required',
            'data.Hargajual'                  => 'required',
        ])->setAttributeNames([
            'data.Barang'              => 'Barang',
            'data.Satuan'                  => 'Satuan',
            'data.Group'                  => 'Group',
            'data.Modal'                  => 'Modal',
            'data.Hargajual'                  => 'Harga Jual',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = $request->get('data1');

        $nextnumber = DB::table('tbl_harga_jual_barang')->selectRaw(DB::raw('MAX("IDHargaJual") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDHargaJual'       => $urutan_id,
            'IDBarang'          => $data['Barang'],
            'IDSatuan'          => $data['Satuan'],
            'IDGroupCustomer'   => $data['Group'],
            'Modal'             => $data['Modal'],
            'Harga_Jual'        => $data['Hargajual'],
            'CID'               => Auth::user()->id,
            'CTime'             => date('Y-m-d H:i:s'),
            'Status'            => 'aktif',
        ];

        $createheader=DB::table('tbl_harga_jual_barang')->insert($header);

        return response()->json($createheader); 

    }

    public function editdata(Request $request)
    {
        
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('HargaJualBarang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $barang             = DB::table('tbl_barang')->orderBy('Nama_Barang', 'asc')->get();
        $satuan             = DB::table('tbl_satuan')->orderBy('Satuan', 'asc')->get();
        $groupcustomer      = DB::table('tbl_group_customer')->orderBy('Nama_Group_Customer', 'asc')->get();
        $edit               = DB::table('tbl_harga_jual_barang')->where('IDHargaJual', $request->id)->first();  

        return view('hargajualbarang/edit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'barang', 'satuan', 'groupcustomer', 'edit'));
    }

    public function simpandataedit(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'data.IDHarga'             => 'required',
            'data.Barang'             => 'required',
            'data.Satuan'                  => 'required',
            'data.Group'                  => 'required',
            'data.Modal'                  => 'required',
            'data.Hargajual'                  => 'required',
        ])->setAttributeNames([
            'data.IDHarga'             => 'Data',
            'data.Barang'              => 'Barang',
            'data.Satuan'                 => 'Satuan',
            'data.Group'                  => 'Group',
            'data.Modal'                  => 'Modal',
            'data.Hargajual'                  => 'Harga Jual',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = $request->get('data1');
        $header = [
            'IDBarang'          => $data['Barang'],
            'IDSatuan'          => $data['Satuan'],
            'IDGroupCustomer'   => $data['Group'],
            'Modal'             => $data['Modal'],
            'Harga_Jual'        => $data['Hargajual'],
            'MID'               => Auth::user()->id,
            'MTime'             => date('Y-m-d H:i:s'),
        ];
        $createheader=DB::table('tbl_harga_jual_barang')
                            ->where('IDHargaJual', $data['IDHarga'])
                            ->update($header);

        return response()->json($createheader); 

    }

    public function editstatus($id)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = HargaJualModel::findOrfail($id);

        if ($data_exist->Status == 'aktif') {
            $data_exist->Status = 'nonaktif';
        } else {
            $data_exist->Status = 'aktif';
        }
        
        $data_exist->save();
        
        $data = array (
            'status'    => true,
            'message'   => 'Status berhasil diubah.'
        );

        return json_encode($data);
    }

    public function pencarian(Request $request)
    {
        $keyword = $request->get('keyword');
        $kolom = $request->get('jenis');
        
        if ($kolom!="" || $keyword!="") {             
            if($kolom){
                $filter = '"'.$kolom.'" ILIKE \'%'.$keyword.'%\' ';
            } else {
                $filter = ' "Kode_Barang" ILIKE \'%'.$keyword.'%\'
                            OR "Nama_Barang" ILIKE \'%'.$keyword.'%\' 
                            OR "Satuan" ILIKE \'%'.$keyword.'%\' 
                            OR "Kode_Group_Customer" ILIKE \'%'.$keyword.'%\' 
                            OR "Nama_Group_Customer" ILIKE \'%'.$keyword.'%\' ';
            }
                
            $datapencarian = DB::select('SELECT * 
                                                FROM tbl_harga_jual_barang thjb 
                                                JOIN tbl_barang tb ON thjb."IDBarang" = tb."IDBarang"
                                                JOIN tbl_satuan ts ON thjb."IDSatuan" = ts."IDSatuan"
                                                JOIN tbl_group_customer tgc ON thjb."IDGroupCustomer" = tgc."IDGroupCustomer"
                                                WHERE '.$filter.'
                                        ');                                
            return response()->json($datapencarian);                 
                            
        }else {
            $datapencarian = DB::select('SELECT * 
                                                FROM tbl_harga_jual_barang thjb 
                                                JOIN tbl_barang tb ON thjb."IDBarang" = tb."IDBarang"
                                                JOIN tbl_satuan ts ON thjb."IDSatuan" = ts."IDSatuan"
                                                JOIN tbl_group_customer tgc ON thjb."IDGroupCustomer" = tgc."IDGroupCustomer"
                                        ');
            return response()->json($datapencarian); 
        }



    }


}
