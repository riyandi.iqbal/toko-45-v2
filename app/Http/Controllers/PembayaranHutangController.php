<?php
/*=====Created by Tiar Sagita Rahman @28 Desember 2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\PembayaranHutangModel;
use App\Models\BarangModel;
use App\Models\CoaModel;
use App\Models\DownPaymentModel;
use App\Models\JurnalModel;
use App\Models\PembelianModel;
use App\Models\PembelianUmumModel;
use App\Models\PembayaranHutangDetailModel;
use App\Models\PembayaranHutangBayarModel;
use App\Models\VListPembayaranHutangModel;
use App\Models\VListPembayaranHutangBayarModel;
use App\Models\HutangModel;
use App\Models\CaraBayarModel;
use App\Models\SettingCOAModel;
use App\Models\PerusahaanModel;
use App\Models\MataUangModel;
use App\Models\GiroModel;
use App\Models\SupplierModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class PembayaranHutangController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->data = new \stdClass();
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('pembayaran_hutang.index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser'));
    }

    public function datatable(Request $request) {
      $data = VListPembayaranHutangModel::select('*');

      if ($request->get('tanggal_awal')) {
          if ($request->get('tanggal_akhir')) {
              $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
          } else {
              $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
          }
      } else {
          if ($request->get('tanggal_akhir')) {
              $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
          }
      }

      if ($request->get('field')) {
          $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
      }
      
      $data->get();
      
      return Datatables::of($data)->make(true);
    }

    public function datatable_detail(Request $request) {
        $data = VListPenerimaanHutangBayarModel::select('*');

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $supplier           = SupplierModel::get();
        $coa                = CoaModel::get();
        $kurs               = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $cara_bayar         = CaraBayarModel::Getph();

        return view('pembayaran_hutang.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'supplier', 'coa', 'kurs', 'cara_bayar'));
    }

    public function store(Request $request) {
      $validate = Validator::make($request->all(), [
          'Tanggal'           => 'required',
          'Nomor'             => 'required',
          'IDSupplier'        => 'required', 
          'Keterangan'        => 'nullable',
          'Total_pembayaran'        => 'required',
      ])->setAttributeNames([
          'Tanggal'           => 'Tanggal',
          'Nomor'             => 'Nomor',
          'IDSupplier'        => 'Supplier',
          'Keterangan'        => 'Keterangan',
          'Total_pembayaran'  => 'Pembayaran',
      ]);
      
      if ($validate->fails()) {
          $data = [
              'status'    => false,
              'message'   => strip_tags($validate->errors()->first())
          ];
          return json_encode($data);
      }
      
      DB::beginTransaction();
      $IDBS = uniqid();
      $Nomor = $this->_cek_nomor($request->Nomor);

      $pembayaran_hutang = new PembayaranHutangModel;

      $pembayaran_hutang->IDBS           = $IDBS;
      $pembayaran_hutang->Tanggal        = AppHelper::DateFormat($request->Tanggal);
      $pembayaran_hutang->Nomor          = $Nomor;
      $pembayaran_hutang->IDSupplier     = $request->IDSupplier;
      $pembayaran_hutang->IDMataUang     = 1;
      $pembayaran_hutang->Kurs           = 14000;
      $pembayaran_hutang->Total          = AppHelper::StrReplace($request->Total_hutang);
      $pembayaran_hutang->Pembayaran         = AppHelper::StrReplace($request->Total_pembayaran);
      $pembayaran_hutang->Kelebihan_bayar    = AppHelper::StrReplace($request->Kelebihan_bayar);
      $pembayaran_hutang->Keterangan         = $request->Keterangan;
      $pembayaran_hutang->Batal              = 'aktif';

      $pembayaran_hutang->save();

      foreach ($request->do as $key => $value) {
        // UPDATE PIUTANG //
        $hutang = HutangModel::find($value);

        $hutang->Pembayaran        = $hutang->Pembayaran + floatval(AppHelper::StrReplace($request->Nominal[$key])) ;
        $hutang->Saldo_Akhir       = floatval(AppHelper::StrReplace($hutang->Saldo_Akhir)) - floatval(AppHelper::StrReplace($request->Nominal[$key]));

        if ($hutang->Saldo_Akhir == 0) {
          $hutang->is_paid = 't';
        }
        
        $hutang->save();
        // END PIUTANG //

        // SAVE PENERIMAAN PIUTANG DETAIL //
        $IDBSDet = uniqid();
        $penerimaan_hutang_detail = new PembayaranHutangDetailModel;

        $penerimaan_hutang_detail->IDBSDet  = $IDBSDet;
        $penerimaan_hutang_detail->IDBS        = $IDBS;
        $penerimaan_hutang_detail->IDFB        = $hutang->IDFaktur;
        $penerimaan_hutang_detail->Nomor       = $hutang->No_Faktur;
        $penerimaan_hutang_detail->Tanggal_fb  = $hutang->Tanggal_Hutang;
        $penerimaan_hutang_detail->Nilai_fb    = $hutang->Nilai_Hutang;
        $penerimaan_hutang_detail->Diterima        = AppHelper::StrReplace($request->Nominal[$key]);
        $penerimaan_hutang_detail->Telah_diterima  = AppHelper::StrReplace($request->Nominal[$key]);
        $penerimaan_hutang_detail->IDMataUang  = 'P000002';
        $penerimaan_hutang_detail->Kurs        = 1;
        $penerimaan_hutang_detail->IDHutang   = $hutang->IDHutang;

        $penerimaan_hutang_detail->save();
        // END SAVE PENERIMAAN PIUTANG DETAIL //
      }

      $data_pembayaran = json_decode($request->Pembayaran_detail);

      foreach ($data_pembayaran as $key => $value) {
        $penerimaan_hutang_bayar = new PembayaranHutangBayarModel;

        $penerimaan_hutang_bayar->IDBSBayar        = uniqid();
        $penerimaan_hutang_bayar->IDBS             = $IDBS;
        $penerimaan_hutang_bayar->Jenis_pembayaran = $value->Jenis_pembayaran;
        $penerimaan_hutang_bayar->IDCOA            = $value->IDCoa;
        $penerimaan_hutang_bayar->Nomor_giro       = $value->Nomor_giro ? $value->Nomor_giro : null;
        $penerimaan_hutang_bayar->Nominal_pembayaran   = AppHelper::StrReplace($value->Nominal_pembayaran);
        $penerimaan_hutang_bayar->IDMataUang           = 'P000002';
        $penerimaan_hutang_bayar->Kurs                 = 1;
        $penerimaan_hutang_bayar->Status_giro          = null ;
        $penerimaan_hutang_bayar->Tanggal_giro         = $value->Tanggal_giro ? AppHelper::DateFormat($value->Tanggal_giro) : null;

        $penerimaan_hutang_bayar->save();

        if ($value->Jenis_pembayaran == 'giro') {
            $giro = new GiroModel;

            $giro->IDGiro           = uniqid();
            $giro->IDFaktur         = $IDBS;
            $giro->IDPerusahaan     = $request->IDSupplier;
            $giro->Jenis_faktur     = 'PH';
            $giro->Nomor_faktur     = $Nomor;
            $giro->Nomor_giro       = $value->Nomor_giro;
            $giro->Tanggal_giro     = $value->Tanggal_giro ? AppHelper::DateFormat($value->Tanggal_giro) : null;
            $giro->Tanggal_faktur   = AppHelper::DateFormat($request->Tanggal);
            $giro->Nilai            = AppHelper::StrReplace($value->Nominal_pembayaran);

            $giro->save();
        }

      // SAVE JURNAL DEBET //
     
      $CoaDebet = SettingCOAModel::where('IDMenu', '=', '93')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

      $jurnal_penerimaan_hutang_debet = new JurnalModel;

      $jurnal_penerimaan_hutang_debet->IDJurnal     = $this->number_jurnal();
      $jurnal_penerimaan_hutang_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
      $jurnal_penerimaan_hutang_debet->Nomor        = $Nomor;
      $jurnal_penerimaan_hutang_debet->IDFaktur     = $IDBS;
      $jurnal_penerimaan_hutang_debet->IDFakturDetail   = '-';
      $jurnal_penerimaan_hutang_debet->Jenis_faktur = 'PH';
      $jurnal_penerimaan_hutang_debet->IDCOA        = $CoaDebet->IDCoa;
      $jurnal_penerimaan_hutang_debet->Debet        = AppHelper::StrReplace($request->Total_pembayaran);
      $jurnal_penerimaan_hutang_debet->Kredit       = 0;
      $jurnal_penerimaan_hutang_debet->IDMataUang       = 'P000002';
      $jurnal_penerimaan_hutang_debet->Kurs             = 1;
      $jurnal_penerimaan_hutang_debet->Total_debet      = AppHelper::StrReplace($request->Total_pembayaran);
      $jurnal_penerimaan_hutang_debet->Total_kredit     = 0;
      $jurnal_penerimaan_hutang_debet->Keterangan       = 'Pembayaran Hutang Nomor ' . $Nomor;
      $jurnal_penerimaan_hutang_debet->Saldo            = AppHelper::StrReplace($request->Total_pembayaran);

      $jurnal_penerimaan_hutang_debet->save();

    }

      // SAVE JURNAL KREDIT //
      $jurnal_penerimaan_hutang_kredit = new JurnalModel;

      $jurnal_penerimaan_hutang_kredit->IDJurnal          = $this->number_jurnal();
      $jurnal_penerimaan_hutang_kredit->Tanggal           = AppHelper::DateFormat($request->Tanggal);
      $jurnal_penerimaan_hutang_kredit->Nomor             = $Nomor;
      $jurnal_penerimaan_hutang_kredit->IDFaktur          = $IDBS;
      $jurnal_penerimaan_hutang_kredit->IDFakturDetail    = $penerimaan_hutang_bayar->IDBSBayar;
      $jurnal_penerimaan_hutang_kredit->Jenis_faktur      = 'PH';
      $jurnal_penerimaan_hutang_kredit->IDCOA             = $value->IDCoa;
      $jurnal_penerimaan_hutang_kredit->Debet             = 0;
      $jurnal_penerimaan_hutang_kredit->Kredit            = AppHelper::StrReplace($value->Nominal_pembayaran);
      $jurnal_penerimaan_hutang_kredit->IDMataUang        = 'P000002';
      $jurnal_penerimaan_hutang_kredit->Kurs              = 1;
      $jurnal_penerimaan_hutang_kredit->Total_debet       = 0;
      $jurnal_penerimaan_hutang_kredit->Total_kredit      = AppHelper::StrReplace($value->Nominal_pembayaran);
      $jurnal_penerimaan_hutang_kredit->Keterangan        = 'Pembayaran Hutang Nomor ' . $Nomor;
      $jurnal_penerimaan_hutang_kredit->Saldo             = AppHelper::StrReplace($value->Nominal_pembayaran);

      $jurnal_penerimaan_hutang_kredit->save();

      if (AppHelper::StrReplace($request->Kelebihan_bayar) > 0) {
        // SAVE JURNAL DEBET //
        $jurnal_penerimaan_hutang_debet = new JurnalModel;

        $jurnal_penerimaan_hutang_debet->IDJurnal     = $this->number_jurnal();
        $jurnal_penerimaan_hutang_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
        $jurnal_penerimaan_hutang_debet->Nomor        = $Nomor;
        $jurnal_penerimaan_hutang_debet->IDFaktur     = $IDBS;
        $jurnal_penerimaan_hutang_debet->IDFakturDetail   = '-';
        $jurnal_penerimaan_hutang_debet->Jenis_faktur = 'PH';
        $jurnal_penerimaan_hutang_debet->IDCOA        = $request->IDCoa_kelebihan_bayar;
        $jurnal_penerimaan_hutang_debet->Debet        = AppHelper::StrReplace($request->Kelebihan_bayar);
        $jurnal_penerimaan_hutang_debet->Kredit       = 0;
        $jurnal_penerimaan_hutang_debet->IDMataUang       = 'P000002';
        $jurnal_penerimaan_hutang_debet->Kurs             = 1;
        $jurnal_penerimaan_hutang_debet->Total_debet      = AppHelper::StrReplace($request->Kelebihan_bayar);
        $jurnal_penerimaan_hutang_debet->Total_kredit     = 0;
        $jurnal_penerimaan_hutang_debet->Keterangan       = 'Kelebihan Pembayaran Hutang Nomor ' . $Nomor;
        $jurnal_penerimaan_hutang_debet->Saldo            = AppHelper::StrReplace($request->Kelebihan_bayar);

        $jurnal_penerimaan_hutang_debet->save();
      }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $pembayaran_hutang  = VListPembayaranHutangModel::find($id);
        $pembayaran_hutang_bayar = VListPembayaranHutangBayarModel::where('IDBS', $id)->get();
        $pembayaran_hutang_detail = PembayaranHutangDetailModel::where('IDBS', $id)
                      ->leftJoin('tbl_hutang', 'tbl_hutang.IDHutang', '=', 'tbl_pembayaran_hutang_detail.IDHutang')->get();

        return view('pembayaran_hutang.detail', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'pembayaran_hutang', 'pembayaran_hutang_bayar', 'pembayaran_hutang_detail'));
    }

    public function laporan(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $datas              = DB::table('tbl_pembayaran_hutang')
                                ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_pembayaran_hutang.IDSupplier')
                                ->join('tbl_pembayaran_hutang_bayar', 'tbl_pembayaran_hutang.IDBS', '=', 'tbl_pembayaran_hutang_bayar.IDBS')
                                ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_pembayaran_hutang.IDMataUang')
                                ->select('tbl_pembayaran_hutang.*', 'tbl_supplier.Nama', 'tbl_pembayaran_hutang_bayar.Nominal_pembayaran', 'tbl_pembayaran_hutang_bayar.Jenis_pembayaran', 'tbl_mata_uang.Mata_uang')
                                ->get();

        return view('pembayaran_hutang/laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'datas'));
    }

    public function get_hutang(Request $request) {
        $hutang = HutangModel::where('IDSupplier', $request->get('IDSupplier'))
                                ->leftJoin('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_hutang.IDMataUang')
                                ->where('Saldo_Akhir', '!=', 0)
                                ->where('is_paid', 'f')
                                ->where('Batal', 'aktif')
                                ->get();
        
        return Datatables::of($hutang)->make(true);
    }
    
    public function get_coa(Request $request) {
        $jenis = $request->Jenis_Pembayaran;

        if($jenis=='transfer'){
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '10103.%')
                        ->get();
        } elseif($jenis=='giro') {
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '20102')
                        ->get();
        } elseif($jenis=='cash') {
            $data = DB::table('tbl_coa')
                        ->where('Nama_COA', 'LIKE', 'Kas%')
                        ->get();
        }

        return json_encode($data);
    }

    public function number_jurnal() {
        $nomor = DB::table('tbl_jurnal')->selectRaw(DB::raw('MAX("IDJurnal") as nonext'))->first();
        
        if ($nomor->nonext=='') {
            $nomor_baru = 'P000001';
        } else{
            $hasil5 = substr($nomor->nonext,2,6) + 1;
            $nomor_baru = 'P'.str_pad($hasil5, 6, 0, STR_PAD_LEFT);
        }

        return $nomor_baru;
    }

    public function number_invoice(Request $request){
        $bulan = date('m');

        $bulan_romawi = AppHelper::BulanRomawi($bulan);

        $queryBuilder = VListPembayaranHutangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

        $number_exist = $queryBuilder->first();

        if (! $number_exist->kode) {
            $urutan = '0001';
        } else {
            $urutan = (int) $number_exist->kode + 1;
            $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
        }

        $kode = $urutan . '/PH/' . $bulan_romawi . '/' . date('y');

        if($request->ajax()){
            return json_encode($kode);
        } else {
            return $kode;
        }
    }

    private function _cek_nomor($nomor) {
      $nomor_exist = VListPembayaranHutangModel::where('Nomor', $nomor)->first();
      if ($nomor_exist) {
          $nomor_baru = $this->_generate_number();
          $this->_cek_nomor($nomor_baru);
      } else {
          $nomor_baru = $nomor;
      }
      
      return $nomor_baru;
  }

  private function _generate_number(){
      $bulan = date('m');

      $bulan_romawi = AppHelper::BulanRomawi($bulan);

      $queryBuilder = VListPembayaranHutangModel::selectRaw('max(substring("Nomor", 1, 4)) as kode');

      $number_exist = $queryBuilder->first();

      if (! $number_exist->kode) {
          $urutan = '0001';
      } else {
          $urutan = (int) $number_exist->kode + 1;
          $urutan = str_pad($urutan, 4, "0", STR_PAD_LEFT);
      }

      $kode = $urutan . '/PH/' . $bulan_romawi . '/' . date('y');
      
      return $kode;
  }
}