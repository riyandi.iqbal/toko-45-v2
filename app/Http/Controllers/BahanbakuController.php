<?php
/*=====Create DEDY @02/01/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\BahanbakuModel;
use App\Models\BahanbakuDetailModel;
use App\Models\BarangModel;
use App\Models\GudangModel;
use App\Models\SatuanModel;
use App\Models\StokModel;
use App\Models\KartustokModel;
use App\Models\FormulaModel;
use App\Models\FormulaDetailModel;
use App\Models\PerusahaanModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;

class BahanbakuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $produksi           = BahanbakuModel::GetBahanBaku()->get();

        return view('bahanbaku/index', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'produksi'));
    }

    public function datatable(Request $request) {
        $data = BahanbakuModel::GetBahanBaku();

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_produksibahanbaku.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_produksibahanbaku.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_produksibahanbaku.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function tambahdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $kodepbb            = DB::table('tbl_produksibahanbaku')
                                    ->whereYear('Tanggal', $tahun)
                                    ->select(DB::raw('MAX(SUBSTRING("IDPBB", 4, 4)) as curr_number'))
                                    ->first();
        $formula            = FormulaModel::orderBy('Nama_Formula', 'asc')->get();
        $formuladetail      = FormulaDetailModel::get();
        $gudang             = GudangModel::get();
        $barang             = BarangModel::get();
        $satuan             = SatuanModel::get();


        return view('bahanbaku/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 
                                                'namauser', 'kodepbb', 'gudang', 'barang', 'coreset',
                                                'satuan', 'formula', 'formuladetail'));
    }

    public function cekdetailformula(Request $request)
    {
        $Nomor          = $request->get('Nomor');
        $cekdata        = FormulaDetailModel::Getdetail()->where('IDFormula', '=', $Nomor)->get();
        return response()->json($cekdata); 

    }

    public function ambilsatuan(Request $request)
    {
        $idbarang = $request->get('idbrg');
        $datacari = DB::select('SELECT b."Satuan", a."IDSatuan" 
                                    FROM tbl_barang a, tbl_satuan b 
                                    WHERE a."IDSatuan"=b."IDSatuan" AND a."IDBarang"=\''.$idbarang.'\'');
        return response()->json($datacari); 
    }    

    public function simpandata(Request $request)
    {
        $tanggal    = $request->get('Tanggal');
        $nomor      = $request->get('Nomor');
        $keterangan = $request->get('Keterangan');
        $formula    = $request->get('formula');
        $qtyheader  = $request->get('qty');

        $brg        = $request->get('idbarang_array');
        $qty        = $request->get('berat_array');
        $stn        = $request->get('idsatuan_array');
        $total      = $request->get('total_array');

        $jml_data = count($brg);

        for($y = 0; $y < $jml_data; ++$y){
            $cekdatabarang = DB::table('tbl_stok')->where('IDBarang', '=', $brg[$y])->where('IDGudang', '=', $request->get('IDGudang'))->where('Qty_pcs', '>=', $total[$y])->first();
            if($cekdatabarang==''){
                return redirect('Bahan_baku')->with('alert2', 'Ada Barang Yang Stoknya Kurang');
            }
        }

        DB::beginTransaction();

        $nextnumber =  BahanbakuModel::select(DB::raw('MAX("IDPBB") as nonext'))->first(); 

        if($nextnumber['nonext']==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber['nonext'],2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }
        //========================SIMPAN HEADER
        $header = new BahanbakuModel;

        $header->IDPBB      = $urutan_id;
        $header->Nomor      = $nomor;
        $header->Tanggal    = $tanggal;
        $header->Keterangan = $keterangan;
        $header->IDFormula  = $formula;
        $header->Status     = 'Aktif';
        $header->Qty_Header = $qtyheader;
        $header->CID        = Auth::user()->id;
        $header->CTime      = date('Y/m/d H:i:s');
        
        $header->save();

        
        for ($x = 0; $x < $jml_data; ++$x) {
            $nextnumber2 =  BahanbakuDetailModel::select(DB::raw('MAX("IDPBBDetail") as nonext'))->first(); 

            if($nextnumber2['nonext']==''){
            $urutan_id2 = 'P000001';
            }else{
            $hasil2 = substr($nextnumber2['nonext'],2,6) + ($x + 1);
            $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $detail = new BahanbakuDetailModel;
            $detail->IDPBBDetail    = $urutan_id2;
            $detail->IDPBB          = $urutan_id;
            $detail->IDBarang       = $brg[$x];
            $detail->Qty            = $qty[$x];
            $detail->IDSatuan       = $stn[$x];
            $detail->Total          = $total[$x];
            $detail->save();

            $ambilstok = StokModel::select('*')->where('IDBarang', '=', $brg[$x])->where('IDGudang', $request->get('IDGudang'))->first();

            if ($ambilstok) {
                $hasilpenguranganstok = $ambilstok->Qty_pcs - $total[$x];
                
                $datastok = [
                    'Qty_pcs'	=> $hasilpenguranganstok,
                ];
                DB::table('tbl_stok')->where('IDBarang', '=', $brg[$x])->update($datastok);
            } else {
                $IDStok = uniqid();
                $stok = new StokModel;

                $stok->IDStok   = $IDStok;
                $stok->Tanggal  = date('Y-m-d H:i:s');
                $stok->Nomor_faktur     = $nomor;
                $stok->IDBarang     = $brg[$x];
                $stok->Qty_pcs      = $total[$x] * -1;
                $stok->Nama_Barang      = '-';
                $stok->Jenis_faktur     = 'SJ';
                $stok->IDGudang     = $request->get('IDGudang');
                $stok->IDSatuan     = $stn[$x];

                $stok->save();
            }
            
            $nextnumber3 =  KartustokModel::select(DB::raw('MAX("IDKartuStok") as nonext'))->first(); 

            if($nextnumber3['nonext']==''){
            $urutan_id3 = 'P000001';
            }else{
            $hasil3 = substr($nextnumber3['nonext'],2,6) + ($x + 1);
            $urutan_id3 = 'P'.str_pad($hasil3, 6, 0, STR_PAD_LEFT);
            }

    		//=============insert kartu stok
    		$dataks = [
		       		'IDKartuStok'	=> $urutan_id3,
                    'Nomor_faktur'	=> $nomor,
                    'IDStok'        => $ambilstok ? $ambilstok->IDStok : $IDStok,
		       		'IDBarang'		=> $brg[$x],
		       		'Keluar_pcs'	=> $total[$x],
                    'IDSatuan'		=> $stn[$x],
                    'Tanggal'    => date('Y/m/d'), 
            ];

            DB::table('tbl_kartu_stok')->insert($dataks);
        }

        DB::commit();

        return redirect('Bahan_baku')->with('alert', 'Data Berhasil Disimpan');

    }

    public function showdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $formula            = BahanbakuModel::Getheader()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->first();
        $formuladetail      = BahanbakuDetailModel::Getdetail()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->get(); 
        return view('bahanbaku/showdata', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'formula', 'formuladetail'));
    }

    public function editdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $formula            = BahanbakuModel::Getheader()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->first();
        $formuladetail      = BahanbakuDetailModel::Getdetail()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->get();
        return view('bahanbaku/editdata', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'formula', 'formuladetail'));
    }

    public function simpanedit(Request $request)
    {
        $tanggal    = $request->get('Tanggal');
        $nomor      = $request->get('Nomor');
        $keterangan = $request->get('Keterangan');
        $formula    = $request->get('formula');
        $idformula  = $request->get('formulaid');
        $qtyheader  = $request->get('qty');
        $idpbb      = $request->get('idpbb');

        $brg        = $request->get('idbarang_array');
        $qty        = $request->get('berat_array');
        $stn        = $request->get('idsatuan_array');
        $total      = $request->get('total_array');

        $jml_data = count($brg);

        for($y = 0; $y < $jml_data; ++$y){
            $cekdatabarang = DB::table('tbl_stok')->where('IDBarang', '=', $brg[$y])->where('Qty_pcs', '>=', $total[$y])->first();
            if($cekdatabarang==''){
                return redirect('Bahan_baku')->with('alert2', 'Ada Barang Yang Stoknya Kurang');
            }
        }

        //========================SIMPAN HEADER
        $dataheader = [
            'Nomor'     => $nomor,
            'Tanggal'   => $tanggal,
            'Keterangan'=> $keterangan,
            'IDFormula' => $idformula,
            'Status'    => 'Aktif',
            'Qty_Header'=> $qtyheader,
            'MID'       => Auth::user()->id,
            'MTime'     => date('Y/m/d H:i:s'),
        ];
        DB::table('tbl_produksibahanbaku')->where('IDPBB', '=', $idpbb)->update($dataheader);

        
        for ($x = 0; $x < $jml_data; ++$x) {
            //tambahkan dulu stok yang dulu
            $ambildetailsebelumnya = DB::select('SELECT "Total" FROM tbl_produksibahanbaku_detail WHERE "IDPBB"=\''.$idpbb.'\' AND "IDBarang"=\''.$brg[$x].'\'');

            $ambilstok = DB::select('SELECT "Qty_pcs" FROM tbl_stok WHERE "IDBarang"=\''.$brg[$x].'\' ');
    		$hasilplusstok = $ambilstok[0]->Qty_pcs+$ambildetailsebelumnya[0]->Total;
            
    		$datastok = [
    			'Qty_pcs'	=> $hasilplusstok,
            ];
            DB::table('tbl_stok')->where('IDBarang', '=', $brg[$x])->update($datastok);
            
            $nextnumber3 =  KartustokModel::select(DB::raw('MAX("IDKartuStok") as nonext'))->first(); 

            if($nextnumber3['nonext']==''){
            $urutan_id3 = 'P000001';
            }else{
            $hasil3 = substr($nextnumber3['nonext'],2,6) + 1;
            $urutan_id3 = 'P'.str_pad($hasil3, 6, 0, STR_PAD_LEFT);
            }
        }
        
        DB::table('tbl_produksibahanbaku_detail')->where('IDPBB', '=', $idpbb)->delete();
        DB::table('tbl_kartu_stok')->where('Nomor_faktur', '=', $nomor)->delete();
        for($y=0; $y < $jml_data; ++$y)
        {
            //================insert detail plus pengurangan stok baru
            $nextnumber2 =  BahanbakuDetailModel::select(DB::raw('MAX("IDPBBDetail") as nonext'))->first(); 

            if($nextnumber2['nonext']==''){
            $urutan_id2 = 'P000001';
            }else{
            $hasil2 = substr($nextnumber2['nonext'],2,6) + 1;
            $urutan_id2 = 'P'.str_pad($hasil2, 6, 0, STR_PAD_LEFT);
            }

            $detail = new BahanbakuDetailModel;
            $detail->IDPBBDetail    = $urutan_id2;
            $detail->IDPBB          = $idpbb;
            $detail->IDBarang       = $brg[$y];
            $detail->Qty            = $qty[$y];
            $detail->IDSatuan       = $stn[$y];
            $detail->Total          = $total[$y];
            $detail->save();

            $ambilstok = DB::select('SELECT "Qty_pcs" FROM tbl_stok WHERE "IDBarang"=\''.$brg[$y].'\' ');
    		$hasilpenguranganstok = $ambilstok[0]->Qty_pcs-$total[$y];
            
    		$datastok = [
    			'Qty_pcs'	=> $hasilpenguranganstok,
            ];
            DB::table('tbl_stok')->where('IDBarang', '=', $brg[$y])->update($datastok);
    		//=============insert kartu stok
    		$dataks = [
		       		'IDKartuStok'	=> $urutan_id3,
		       		'Nomor_faktur'	=> $nomor,
		       		'IDBarang'		=> $brg[$y],
		       		'Keluar_pcs'	=> $total[$y],
                       'IDSatuan'		=> $stn[$y],
                       'Tanggal'    => date('Y/m/d'), 
            ];
            DB::table('tbl_kartu_stok')->insert($dataks);
        }
        return redirect('Bahan_baku')->with('alert', 'Data Berhasil Disimpan');
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses              = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        DB::table('tbl_produksibahanbaku')
            ->where('IDPBB', $id)
            ->update(['Status' => 'Dibatalkan']);

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function printdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $formula            = BahanbakuModel::Getheader()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->first();
        $formuladetail      = BahanbakuDetailModel::Getdetail()->where('tbl_produksibahanbaku.IDPBB', '=', $request->id)->get(); 
        $perusahaan          = PerusahaanModel::Getforprint();
        return view('bahanbaku/printdata', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset',
                                                'namauser', 'formula', 'formuladetail', 'perusahaan'));
    }

    public function caridata(Request $request)
    {
        $keyword    = $request->get('keyword');
        $kolom      = $request->get('jenis');
        $sdate      = $request->get('date_from');
        $edate      = $request->get('date_until');

        if ($sdate!="" || $edate!="" || $kolom!="" || $keyword!="" ) {

            if($kolom){
                $filter = 'AND '.$kolom.' ILIKE \'%'.$keyword.'%\' ';
            } else {
                $filter = 'AND a."Nomor" ILIKE \'%'.$keyword.'%\' OR c."Nama_Formula" ILIKE \'%'.$keyword.'%\'';
            }

            $date = 'AND a."Tanggal" BETWEEN \''.$sdate.'\' AND \''.$edate.'\'';

            $datapencarian = DB::select('SELECT * FROM tbl_produksibahanbaku a,
                                                        tbl_produksibahanbaku_detail b,
                                                        tbl_formula c
                                         WHERE a."IDPBB"=b."IDPBB"
                                         AND a."IDFormula"=c."IDFormula" 
                                         
                                         '.$date.' '.$filter.'
                                        ');
            return response()->json($datapencarian);
        }
    }


}
