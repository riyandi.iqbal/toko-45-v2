<?php
/*=====Create TIAR @12/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\BarangModel;
use App\Models\SatuanModel;
use App\Models\SatuanKonversiModel;
use App\Models\HargaJualModel;
use App\Models\VListSatuanKonversiModel;

// HELPERS //
use App\Helpers\AppHelper;

class SatuanKonversiController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('satuankonversi.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function datatable(Request $request) {
        $data = SatuanKonversiModel::Datatable();

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }
        
        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $data_barang        = BarangModel::get();
        $data_satuan        = SatuanModel::get();

        return view('satuankonversi.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'data_barang', 'data_satuan')); 
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDBarang'             => 'required',
            'IDSatuanKecil'        => 'required',
            'IDSatuanBesar.*'      => 'required',
            'Qty.*'                => 'required',
            'Modal.*'              => 'required',
            'Harga_Jual.*'         => 'required',
        ])->setAttributeNames([
            'IDBarang'             => 'Barang',
            'IDSatuanKecil'        => 'Satuan Kecil', 
            'IDSatuanBesar.*'      => 'Satuan Besar',
            'Qty.*'                => 'Qty',
            'Modal.*'              => 'Modal',
            'Harga_Jual.*'         => 'Harga Jual',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        foreach ($request->IDSatuanBesar as $key => $value) {
            $data_satuan_konversi_exist = SatuanKonversiModel::where('IDBarang', $request->IDBarang)->where('IDSatuanBesar', $request->IDSatuanBesar[$key])->first();
            if (! $data_satuan_konversi_exist) {
                $IDSatuanKonversi = uniqid();
                // SIMPAN KE TABLE SATUAN KONVERSI //
                $SatuanKonvesi = new SatuanKonversiModel;
    
                $SatuanKonvesi->IDSatuanKonversi = $IDSatuanKonversi;
                $SatuanKonvesi->IDBarang        = $request->IDBarang;
                $SatuanKonvesi->IDSatuanBesar   = $request->IDSatuanBesar[$key];
                $SatuanKonvesi->Qty             = $request->Qty[$key];
                $SatuanKonvesi->IDSatuanKecil   = $request->IDSatuanKecil;
                $SatuanKonvesi->dibuat_pada        = date('Y-m-d H:i:s');
                $SatuanKonvesi->dibuat_oleh        = Auth::user()->id;
                $SatuanKonvesi->diubah_pada        = date('Y-m-d H:i:s');
                $SatuanKonvesi->diubah_oleh        = Auth::user()->id;
    
                $SatuanKonvesi->save(); 
                // END SIMPAN KE TABLE SATUAN KONVERSI //
            }

            $data_harga_jual_exist = HargaJualModel::where('IDBarang', $request->IDBarang)->where('IDSatuan', $request->IDSatuanBesar[$key])->first();
            if (! $data_harga_jual_exist) {
                // SIMPAN KE TABLE HARGA JUAL //
                $HargaJual = new HargaJualModel;
                
                $HargaJual->IDBarang    = $request->IDBarang;
                $HargaJual->IDSatuan    = $request->IDSatuanBesar[$key];
                $HargaJual->IDMataUang  = 0;
                $HargaJual->IDGroupCustomer  = 0;
                $HargaJual->Modal       = 0;
                $HargaJual->Harga_Jual  = 0;
                $HargaJual->CID              = Auth::user()->id;
                $HargaJual->CTime            = date('Y-m-d H:i:s');
                $HargaJual->Status           = 'aktif';
    
                $HargaJual->save(); 
                // END SIMPAN KE TABLE HARGA JUAL //
            }
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        $satuankonversi             = VListSatuanKonversiModel::where('IDBarang', $id)->get();
        
        $data_barang        = BarangModel::Getforindex()->where('IDBarang', $id)->first();
        $data_satuan        = SatuanModel::get();

        return view('satuankonversi.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'coreset', 'data_barang', 'data_satuan', 'satuankonversi'));
    }

    public function update_data(Request $request) {
        DB::beginTransaction();

        $SatuanKonvesi = SatuanKonversiModel::findOrfail($request->IDSatuanKonversi);

        $SatuanKonvesi->Qty = $request->Qty;
        $SatuanKonvesi->IDSatuanBesar = $request->IDSatuanBesar;

        $SatuanKonvesi->save(); 
        
        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.',
        );

        return json_encode($data);
    }

    public function destroy($id) {
        DB::beginTransaction();

        $data_exist = SatuanKonversiModel::findOrfail($id);

        $data_exist->delete();

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }

    public function get_satuan(Request $request) {
        $queryBuilder = VListSatuanKonversiModel::select('*');

        $queryBuilder->where('IDBarang', $request->IDBarang);

        $response = $queryBuilder->get();

        return json_encode($response);
    }
}