<?php
/*=====Create DEDY @11/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\GroupBarangModel;
use App\Models\BarangModel;

class GroupbarangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $groupbarang        = DB::table('tbl_group_barang')        
                                ->orderBy('tbl_group_barang.Group_Barang', 'asc')
                                ->get();
        
        return view('groupbarang/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'groupbarang'));
    }

    public function datatable(Request $request) {
        $data = GroupBarangModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function pencarian(Request $request)
    {
        $keyword = $request->get('keyword');
        $status = $request->get('status');
        $kolom = $request->get('jenis');
        
        if ($kolom!="" || $status!="" || $keyword!="") {             
            if($kolom){
                $filter = '"'.$kolom.'" ILIKE \'%'.$keyword.'%\' ';
            } else {
                $filter = ' "Kode_Group_Barang" ILIKE \'%'.$keyword.'%\'
                            OR "Group_Barang" ILIKE \'%'.$keyword.'%\' ';
            }
            $filter .=  $status ? 'AND "Aktif" = \''.$status.'\'' : "";
                
            $datapencarian = DB::select('SELECT  * 
                                            FROM tbl_group_barang
                                            WHERE '.$filter.'
                                        ');                                
            return response()->json($datapencarian);                 
                            
        }else {
            $datapencarian = DB::select('SELECT * FROM tbl_group_barang
                                        ');
            return response()->json($datapencarian); 
        }
    }

    public function tambah_data(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupBarang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        return view('groupbarang/create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser'));
    }

    public function simpandata(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'kodegroup'             => 'required|unique:tbl_group_barang,Kode_Group_Barang',
            'namagroup'                  => 'required',
        ])->setAttributeNames([
            'kodegroup'             => 'Kode',
            'namagroup'                  => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = $request->get('data1');

        $nextnumber = DB::table('tbl_group_barang')->selectRaw(DB::raw('MAX("IDGroupBarang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $header = [
            'IDGroupBarang'     => $urutan_id,
            'Kode_Group_Barang' => $request->kodegroup,
            'Group_Barang'      => $request->namagroup,
            'Aktif'             => 'Aktif',
        ];

        $createheader=DB::table('tbl_group_barang')->insert($header);

        return response()->json($createheader);
    }

    public function editdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('GroupBarang')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $edit               = DB::table('tbl_group_barang')
                                ->where('IDGroupBarang', $request->id)
                                ->first();

        $array_kode     = unserialize($edit->Cetak_Kode);
        $array_nama     = unserialize($edit->Cetak_Nama);

        $data_barang    = BarangModel::where('IDGroupBarang', $request->id)->first();
        
        return view('groupbarang/edit', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'edit', 'data_barang', 'array_kode', 'array_nama'));
    }

    public function simpandataedit(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'IDGroupBarang'               => 'required',
            'kodegroup'             => 'required',
            'namagroup'                  => 'required',
        ])->setAttributeNames([
            'IDGroupBarang'               => 'Data',
            'kodegroup'             => 'Kode Group Coa',
            'namagroup'                  => 'Nama Group',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $data = $request->get('data1');

        $cetakkode = array( ($request->kode2) ? 'KODE':'',
                             ($request->nama2) ? 'NAMA':'',
                             ($request->tipe2) ? 'TIPE':'',
                             ($request->ukuran2) ? 'UKURAN':'',
                             ($request->kategori2) ? 'KATEGORI':'');
        $cetakkode2= array( ($request->kode3) ? 'KODE':'',
                            ($request->nama3) ? 'NAMA':'',
                            ($request->tipe3) ? 'TIPE':'',
                            ($request->ukuran3) ? 'UKURAN':'',
                            ($request->kategori3) ? 'KATEGORI':'');
        
        DB::beginTransaction();
        
        $data = GroupBarangModel::findOrfail($request->IDGroupBarang);
        
            $data->Kode_Group_Barang = $request->kodegroup;
            $data->Group_Barang      = $request->namagroup;
            $data->Aktif             = 'aktif';
            if (count($cetakkode) > 0) {
                $data->Panjang_Kode      = $request->panjang;
                $data->Pemisah_Kode      = $request->pemisah;
                $data->Cetak_Kode   = serialize($cetakkode);
            }

            if (count($cetakkode2) > 0) {
                $data->Panjang_Nama      = $request->panjang2;
                $data->Pemisah_Nama      = $request->pemisah2;
                $data->Cetak_Nama       = serialize($cetakkode2);
            }

        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function hapusdata(Request $request)
    {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if(! $akses){
            $response = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($response);
        }

        $cek_group = DB::table('tbl_barang')->where('IDGroupBarang', $request->id)->first();
        if ($cek_group) {
            $response = [
                'status'    => false,
                'message'   => 'Data gagal dihapus'
            ];
            return json_encode($response);
        }

        DB::table('tbl_group_barang')
            ->where('IDGroupBarang', $request->id)
            ->delete();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($response);
    }

    public function simpangroupbaru(Request $request)
    {
        $data1 = $request->get('data1');
        $data2 = $request->get('data2');
        $data3 = $request->get('data3');

        $nextnumber = DB::table('tbl_group_barang')->selectRaw(DB::raw('MAX("IDGroupBarang") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        //simpan cetakan kode 
        $cetakkode = array($data1['kode2']=='false'?'':'KODE',
                            $data1['nama2']=='false'?'':'NAMA',
                            $data1['tipe2']=='false'?'':'TIPE',
                            $data1['ukuran2']=='false'?'':'UKURAN',
                            $data1['kategori2']=='false'?'':'KATEGORI');
        $cetakkode2= array($data2['kode3']=='false'?'':'KODE',
                            $data2['nama3']=='false'?'':'NAMA',
                            $data2['tipe3']=='false'?'':'TIPE',
                            $data2['ukuran3']=='false'?'':'UKURAN',
                            $data2['kategori3']=='false'?'':'KATEGORI');
        $data1['kode2'];
        $header = [
            'IDGroupBarang'     => $urutan_id,
            'Kode_Group_Barang' => $data3['kodegroup'],
            'Group_Barang'      => $data3['namagroup'],
            'Aktif'             => 'aktif',
            'Panjang_Kode'      => $data1['panjang'],
            'Pemisah_Kode'      => $data1['pemisah'],
            'Cetak_Kode'        => serialize($cetakkode),
            'Panjang_Nama'      => $data2['panjang2'],
            'Pemisah_Nama'      => $data2['pemisah2'],
            'Cetak_Nama'        => serialize($cetakkode2),
        ];

        $createheader=DB::table('tbl_group_barang')->insert($header);

        return response()->json($createheader);
        
    }


}