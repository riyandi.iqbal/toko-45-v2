<?php
/*=====Create DEDY @10/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;


class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function cektambah(Request $request)
    {
        $iduser           = Auth::user()->id;
        $cekakses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        return json_encode($cekakses);
    }

    public function cekubah(Request $request)
    {
        $iduser           = Auth::user()->id;
        $cekakses         = DB::table('users_akses')->where('Ubah', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        return json_encode($cekakses);
    }

    public function cekhapus(Request $request)
    {
        $iduser           = Auth::user()->id;
        $cekakses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        return json_encode($cekakses);
    }
}
?>