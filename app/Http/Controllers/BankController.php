<?php
/*=====Create TIAR @06/04/2019====*/
/*=====UPDATED TIAR @13/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\BankModel;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('bank.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = BankModel::select('*');

        $data->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_bank.IDCoa');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Bank')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $coa                = DB::table('tbl_coa')->get();
        
        return view('bank.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'coa'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'no_rek'             => 'required|unique:tbl_bank,Nomor_Rekening',
            'coa'             => 'required',
            'an'                => 'required',
        ])->setAttributeNames([
            'no_rek'             => 'Data',
            'coa'             => 'COA',
            'an'                => 'Atas Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = DB::table('tbl_bank')->selectRaw(DB::raw('MAX("IDBank") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new BankModel;

            $data->IDBank            = $urutan_id;
            $data->Nomor_Rekening    = $request->no_rek;
            $data->IDCoa             = $request->coa;
            $data->Atas_Nama         = $request->an;
            $data->Aktif             = 'Aktif';
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Bank')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $banks               = BankModel::findOrfail($id);
        $coa                = DB::table('tbl_coa')->get();

        return view('bank.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'banks', 'coa'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'idbank'            => 'required',
            'no_rek'             => 'required',
            'coa'             => 'required',
            'an'                => 'required',
        ])->setAttributeNames([
            'idbank'            => 'Data',
            'no_rek'             => 'Data',
            'coa'             => 'COA',
            'an'                => 'Atas Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = BankModel::findOrfail($request->idbank);

            $data->Nomor_Rekening    = $request->no_rek;
            $data->IDCoa             = $request->coa;
            $data->Atas_Nama         = $request->an;
            $data->Aktif             = 'Aktif';
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = BankModel::findOrfail($id)->delete();
        
        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dihapus.'
        );

        return json_encode($data);
    }
}