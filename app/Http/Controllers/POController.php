<?php
/*=====Create DEDY @13/12/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Validator;

use Datatables;
use App\Models\MataUangModel;
use App\Models\BarangModel;
use App\Models\BankModel;
use App\Models\PurchaseOrderModel;
use App\Models\PurchaseOrderDetailModel;
use App\Models\PerusahaanModel;
use App\Models\SettingCOAModel;
use App\Models\CaraBayarModel;
use App\Models\VListSatuanKonversiModel;
use App\Models\HargaJualModel;
use App\Models\UMSupplierModel;
use App\Models\GiroModel;
use App\Models\JurnalModel;

// HELPERS //
use App\Helpers\AppHelper;

// LIB //
use Illuminate\Support\Facades\Input;


class POController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $po                 = PurchaseOrderModel::Getindex()->get();        

        return view('po/index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'po'));
    }

    public function datatable(Request $request) {
        $data = PurchaseOrderModel::Getindex();

        if ($request->get('tanggal_awal')) {
            if ($request->get('tanggal_akhir')) {
                $data->whereBetween('tbl_purchase_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
            } else {
                $data->where('tbl_purchase_order.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_purchase_order.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();
        
        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Po')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $supplier           = DB::table('tbl_supplier')->orderBy('Nama', 'asc')->get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $bank = BankModel::get();

        $cara_bayar     = CaraBayarModel::GetPO();

        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');

        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);
        
        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        $barang = $barangBuilder->get();

        return view('po.create', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'supplier', 'bank', 'mata_uang', 'cara_bayar', 'barang'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Nomor'             => 'required',
            'IDSupplier'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'Grand_total'       => 'required',
            'IDMataUang'        => 'required',
            'Kurs'              => 'required',
            'Status_ppn'        => 'required',
            'Keterangan'        => 'nullable',
            'Jenis_Pembayaran'  => 'nullable',
            'IDCoa'             => 'nullable',
            'pembayaran'        => 'nullable',
        ])->setAttributeNames([
            'Nomor'             => 'Nomor',
            'IDSupplier'        => 'Supplier', 
            'Tanggal'           => 'Tanggal',
            'Total_qty'         => 'Total Qty',
            'Grand_total'       => 'Grand Total',
            'IDMataUang'        => 'Mata Uang',
            'Kurs'              => 'Kurs',
            'Status_ppn'        => 'Jenis PPn',
            'Keterangan'        => 'Keterangan',
            'Jenis_Pembayaran'  => 'Jenis Pembayaran',
            'IDCoa'             => 'IDCoa',
            'pembayaran'        => 'Pembayaran',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        $ceknopo =  DB::table('tbl_purchase_order')
                        ->where('Nomor', $request->Nomor)
                        ->first();    
        if(! $ceknopo){
            $Nomor = $request->Nomor;
        }else{
            $hasilkeluaran = substr($ceknopo->Nomor,2,4) + 1;
            $cekagain = substr($request->Nomor, 0, 2);
            $ambilsetelahpenambahan = substr($request->Nomor, 6);
            $Nomor = $cekagain.str_pad($hasilkeluaran, 4, 0, STR_PAD_LEFT).$ambilsetelahpenambahan;
        }

        DB::beginTransaction();

        $IDPO = uniqid();
        
        $purchase_order = new PurchaseOrderModel;

        $purchase_order->IDPO              = $IDPO;
        $purchase_order->Nomor             = $Nomor;
        $purchase_order->IDSupplier        = $request->IDSupplier;
        $purchase_order->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $purchase_order->Total_qty         = $request->Total_qty;
        $purchase_order->Grand_total       = AppHelper::StrReplace($request->Grand_total);
        $purchase_order->IDMataUang        = $request->IDMataUang;
        $purchase_order->Kurs              = $request->Kurs;
        $purchase_order->Keterangan        = $request->Keterangan;
        $purchase_order->Jenis_PO          = 'TTI';
        $purchase_order->Batal         = 'aktif';
        $purchase_order->Status        = 'Aktif';
        $purchase_order->is_taken      = 'f';
        $purchase_order->approve       = 'Pending';
        $purchase_order->jenis_ppn     = $request->Status_ppn =='include' ? '1':'0';
        $purchase_order->pilihanppn    = $request->Status_ppn;
        $purchase_order->dibuat_pada       = date('Y-m-d H:i:s');
        $purchase_order->diubah_pada       = date('Y-m-d H:i:s');

        $purchase_order->save();

        foreach ($request->IDBarang as $key => $value) {
            $purchase_order_detail = new PurchaseOrderDetailModel;
            
            $DPP = AppHelper::StrReplace($request->Sub_total[$key]) - (10 / 100 * AppHelper::StrReplace($request->Sub_total[$key])); 

            $purchase_order_detail->IDPODetail     = uniqid();
            $purchase_order_detail->IDPO           = $IDPO;
            $purchase_order_detail->IDBarang       = $value;
            $purchase_order_detail->Qty            = $request->Qty[$key];
            $purchase_order_detail->Qty_terima     = $request->Qty[$key];
            $purchase_order_detail->IDSatuan       = $request->IDSatuan[$key];            
            $purchase_order_detail->Harga          = AppHelper::StrReplace($request->harga[$key]);
            $purchase_order_detail->PPN            = $request->ppn[$key];
            $purchase_order_detail->DPP            = $request->Status_ppn == 'include' ? ($DPP) : 0 ;
            $purchase_order_detail->Jenis_ppn      = $request->Status_ppn =='include' ? '1':'0';;
            $purchase_order_detail->Saldo          = AppHelper::StrReplace($request->Sub_total[$key]);

            $purchase_order_detail->save();
        }
        
        if ($request->Jenis_Pembayaran) {
            $um_supplier = new UMSupplierModel;
    
            $um_supplier->IDUMSupplier  = uniqid();
            $um_supplier->IDSupplier    = $request->IDSupplier;
            $um_supplier->Nomor_Faktur  = $Nomor;
            $um_supplier->Tanggal_UM        = AppHelper::DateFormat($request->Tanggal);
            $um_supplier->Jenis_pembayaran  = $request->Jenis_Pembayaran;
            $um_supplier->Nominal           = AppHelper::StrReplace($request->pembayaran);
            $um_supplier->IDFaktur          = $IDPO;
            $um_supplier->IDCOA             = ($request->IDCoa);
            $um_supplier->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
            $um_supplier->Nomor_Giro        = $request->Nomor_giro;
    
            $um_supplier->save();

            if ($request->Jenis_Pembayaran == 'giro') {
                $giro = new GiroModel;

                $giro->IDGiro           = uniqid();
                $giro->IDFaktur         = $IDPO;
                $giro->IDPerusahaan     = $request->IDSupplier;
                $giro->Jenis_faktur     = 'PO';
                $giro->Nomor_faktur     = $Nomor;
                $giro->Nomor_giro       = $request->Nomor_giro;
                $giro->Tanggal_giro     = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $giro->Tanggal_faktur   = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai            = AppHelper::StrReplace($request->pembayaran);

                $giro->save();
            }

            
            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '35')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '35')
                                ->where('Posisi', '=', 'kredit')
                                ->where('Tingkat', '=', 'utama')
                                ->first();

            $jurnal_debet = new JurnalModel;

            $jurnal_debet->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_debet->Nomor        = $Nomor;
            $jurnal_debet->IDFaktur     = $IDPO;
            $jurnal_debet->IDFakturDetail   = '-';
            $jurnal_debet->Jenis_faktur = 'PO';
            $jurnal_debet->IDCOA        = $CoaDebet->IDCoa;
            $jurnal_debet->Debet        = AppHelper::StrReplace($request->pembayaran);
            $jurnal_debet->Kredit       = 0;
            $jurnal_debet->IDMataUang       = $request->IDMataUang;
            $jurnal_debet->Kurs             = $request->Kurs;
            $jurnal_debet->Total_debet      = AppHelper::StrReplace($request->pembayaran);
            $jurnal_debet->Total_kredit     = 0;
            $jurnal_debet->Keterangan       = 'Pesanan Pembelian No ' . $Nomor;
            $jurnal_debet->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_debet->save();

            $jurnal_kredit = new JurnalModel;

            $jurnal_kredit->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_kredit->Nomor        = $Nomor;
            $jurnal_kredit->IDFaktur     = $IDPO;
            $jurnal_kredit->IDFakturDetail   = '-';
            $jurnal_kredit->Jenis_faktur = 'PO';
            $jurnal_kredit->IDCOA        = $request->IDCoa;
            $jurnal_kredit->Debet        = 0;
            $jurnal_kredit->Kredit       = AppHelper::StrReplace($request->pembayaran);
            $jurnal_kredit->IDMataUang       = $request->IDMataUang;
            $jurnal_kredit->Kurs             = $request->Kurs;
            $jurnal_kredit->Total_debet      = 0;
            $jurnal_kredit->Total_kredit     = AppHelper::StrReplace($request->pembayaran);
            $jurnal_kredit->Keterangan       = 'Uang Muka Pembelian Pesanan No ' . $Nomor;
            $jurnal_kredit->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_kredit->save();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.',
            'data'      => [
                'IDPO'     => $IDPO,
            ]
        );

        return json_encode($data);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Po')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $tahun              = date('Y');
        $aktif              = 'aktif';
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $supplier           = DB::table('tbl_supplier')->orderBy('Nama', 'asc')->get();
        $mata_uang = MataUangModel::where('Aktif', $aktif)->OrderBy('Kurs', 'asc')->get();
        $barang = BarangModel::get();
        $bank = BankModel::get();

        $cara_bayar     = CaraBayarModel::GetPO();

        $purchase_order                 = DB::table('tbl_purchase_order')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang','=','tbl_purchase_order.IDMataUang')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_purchase_order.IDSupplier')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_purchase_order.*',  'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang')
                                    ->first();

        $purchase_order_detail           = DB::table('tbl_purchase_order_detail')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
                                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')
                                    ->join('tbl_satuan as satuan_kecil', 'satuan_kecil.IDSatuan', '=', 'tbl_barang.IDSatuan')
                                    ->where('tbl_purchase_order_detail.IDPO', $id)
                                    ->select('tbl_purchase_order_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang', 'tbl_barang.IDSatuan as IDSatuanKecil', 'satuan_kecil.Satuan as Satuan_kecil')
                                    ->get();

        $um_supplier                 = DB::table('tbl_um_supplier')
                                    ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor','=','tbl_um_supplier.Nomor_Faktur')
                                    ->join('tbl_coa', 'tbl_coa.IDCoa','=','tbl_um_supplier.IDCOA')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
                                    ->first();

        $barangBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');
        
        $barangBuilder->whereNotIn('tbl_barang.IDGroupBarang', ['P000004']);
        
        $barangBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        $barang = $barangBuilder->get();

        return view('po.update', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'supplier', 'bank', 'mata_uang', 'cara_bayar', 'purchase_order', 'purchase_order_detail', 'um_supplier', 'barang'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDPO'             => 'required',
            'IDSupplier'        => 'required', 
            'Tanggal'           => 'required||date_format:"d/m/Y"',
            'Total_qty'         => 'required|numeric',
            'Grand_total'       => 'required',
            'IDMataUang'        => 'required',
            'Kurs'              => 'required',
            'Status_ppn'        => 'required',
            'Keterangan'        => 'nullable',
            'Jenis_Pembayaran'  => 'nullable',
            'IDCoa'             => 'nullable',
            'pembayaran'        => 'nullable',
        ])->setAttributeNames([
            'IDPO'             => 'Data',
            'Nomor'             => 'Nomor',
            'IDSupplier'        => 'Customer', 
            'Tanggal'           => 'Tanggal',
            'Total_qty'         => 'Total Qty',
            'Grand_total'       => 'Grand Total',
            'IDMataUang'        => 'Mata Uang',
            'Kurs'              => 'Kurs',
            'Status_ppn'        => 'Jenis PPn',
            'Keterangan'        => 'Keterangan',
            'Jenis_Pembayaran'  => 'Jenis Pembayaran',
            'IDCoa'             => 'IDCoa',
            'pembayaran'        => 'Pembayaran',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $Nomor = $request->Nomor;
        $IDPO = $request->IDPO;

        $purchase_order = PurchaseOrderModel::findOrfail($request->IDPO);

        $purchase_order->Nomor             = $Nomor;
        $purchase_order->IDSupplier        = $request->IDSupplier;
        $purchase_order->Tanggal           = AppHelper::DateFormat($request->Tanggal);
        $purchase_order->Total_qty         = $request->Total_qty;
        $purchase_order->Grand_total       = AppHelper::StrReplace($request->Grand_total);
        $purchase_order->IDMataUang        = $request->IDMataUang;
        $purchase_order->Kurs              = $request->Kurs;
        $purchase_order->Keterangan        = $request->Keterangan;
        $purchase_order->jenis_ppn     = $request->Status_ppn =='include' ? '1':'0';
        $purchase_order->pilihanppn    = $request->Status_ppn;

        $purchase_order->save();

        $purchase_order_detail_exist = PurchaseOrderDetailModel::where('IDPO', $purchase_order->IDPO)->delete();

        foreach ($request->IDBarang as $key => $value) {
            $purchase_order_detail = new PurchaseOrderDetailModel;
            
            $DPP = AppHelper::StrReplace($request->Sub_total[$key]) - (10 / 100 * AppHelper::StrReplace($request->Sub_total[$key])); 

            $purchase_order_detail->IDPODetail     = uniqid();
            $purchase_order_detail->IDPO           = $IDPO;
            $purchase_order_detail->IDBarang       = $value;
            $purchase_order_detail->Qty            = $request->Qty[$key];
            $purchase_order_detail->Qty_terima     = 0;
            $purchase_order_detail->IDSatuan       = $request->IDSatuan[$key];            
            $purchase_order_detail->Harga          = AppHelper::StrReplace($request->harga[$key]);
            $purchase_order_detail->PPN            = $request->ppn[$key];
            $purchase_order_detail->DPP            = $request->Status_ppn == 'include' ? ($DPP) : 0 ;
            $purchase_order_detail->Jenis_ppn      = $request->Status_ppn =='include' ? '1':'0';;
            $purchase_order_detail->Saldo          = AppHelper::StrReplace($request->Sub_total[$key]);

            $purchase_order_detail->save();
        }

        if ($request->Jenis_Pembayaran) {
            if (! $request->IDUMSupplier) {
                $um_supplier = new UMSupplierModel;
        
                $um_supplier->IDUMSupplier  = uniqid();
                $um_supplier->IDSupplier    = $request->IDSupplier;
                $um_supplier->Nomor_Faktur  = $Nomor;
                $um_supplier->Tanggal_UM        = AppHelper::DateFormat($request->Tanggal);
                $um_supplier->Jenis_pembayaran  = $request->Jenis_Pembayaran;
                $um_supplier->Nominal           = AppHelper::StrReplace($request->pembayaran);
                $um_supplier->IDFaktur          = $IDPO;
                $um_supplier->IDCOA             = ($request->IDCoa);
                $um_supplier->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $um_supplier->Nomor_Giro        = $request->Nomor_giro;
        
                $um_supplier->save();
            } else {
                $um_supplier = UMSupplierModel::findOrfail($request->IDUMSupplier);
        
                $um_supplier->IDSupplier    = $request->IDSupplier;
                $um_supplier->Nomor_Faktur  = $Nomor;
                $um_supplier->Tanggal_UM        = AppHelper::DateFormat($request->Tanggal);
                $um_supplier->Jenis_pembayaran  = $request->Jenis_Pembayaran;
                $um_supplier->Nominal           = AppHelper::StrReplace($request->pembayaran);
                $um_supplier->IDFaktur          = $IDPO;
                $um_supplier->IDCOA             = ($request->IDCoa);
                $um_supplier->Tanggal_giro      = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $um_supplier->Nomor_Giro        = $request->Nomor_giro;
        
                $um_supplier->save();
            }

            if ($request->Jenis_Pembayaran == 'giro') {
                $giro_exist = GiroModel::where('IDFaktur', $IDPO)->first();

                if ($giro_exist) {
                    $giro = GiroModel::findOrfail($giro_exist);
                } else {
                    $giro = new GiroModel;
                    $giro->IDGiro           = uniqid();
                }

                $giro->IDFaktur         = $IDPO;
                $giro->IDPerusahaan     = $request->IDSupplier;
                $giro->Jenis_faktur     = 'PO';
                $giro->Nomor_faktur     = $Nomor;
                $giro->Nomor_giro       = $request->Nomor_giro;
                $giro->Tanggal_giro     = $request->Tanggal_giro ? AppHelper::DateFormat($request->Tanggal_giro) : null;
                $giro->Tanggal_faktur   = AppHelper::DateFormat($request->Tanggal);
                $giro->Nilai            = AppHelper::StrReplace($request->pembayaran);

                $giro->save();
            }

            $delete_jurnal_awal = JurnalModel::where('IDFaktur', $IDPO)->delete();
            
            $CoaDebet = SettingCOAModel::where('IDMenu', '=', '35')
                            ->where('Posisi', '=', 'debet')
                            ->where('Tingkat', '=', 'utama')
                            ->first();

            $CoaKredit = SettingCOAModel::where('IDMenu', '=', '35')
                                ->where('Posisi', '=', 'kredit')
                                ->where('Tingkat', '=', 'utama')
                                ->first();

            $jurnal_debet = new JurnalModel;

            $jurnal_debet->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_debet->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_debet->Nomor        = $Nomor;
            $jurnal_debet->IDFaktur     = $IDPO;
            $jurnal_debet->IDFakturDetail   = '-';
            $jurnal_debet->Jenis_faktur = 'PO';
            $jurnal_debet->IDCOA        = $CoaDebet->IDCoa;
            $jurnal_debet->Debet        = AppHelper::StrReplace($request->pembayaran);
            $jurnal_debet->Kredit       = 0;
            $jurnal_debet->IDMataUang       = $request->IDMataUang;
            $jurnal_debet->Kurs             = $request->Kurs;
            $jurnal_debet->Total_debet      = AppHelper::StrReplace($request->pembayaran);
            $jurnal_debet->Total_kredit     = 0;
            $jurnal_debet->Keterangan       = 'Pesanan Pembelian No ' . $Nomor;
            $jurnal_debet->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_debet->save();

            $jurnal_kredit = new JurnalModel;

            $jurnal_kredit->IDJurnal     = AppHelper::NumberJurnal();
            $jurnal_kredit->Tanggal      = AppHelper::DateFormat($request->Tanggal);
            $jurnal_kredit->Nomor        = $Nomor;
            $jurnal_kredit->IDFaktur     = $IDPO;
            $jurnal_kredit->IDFakturDetail   = '-';
            $jurnal_kredit->Jenis_faktur = 'PO';
            $jurnal_kredit->IDCOA        = $request->IDCoa;
            $jurnal_kredit->Debet        = 0;
            $jurnal_kredit->Kredit       = AppHelper::StrReplace($request->pembayaran);
            $jurnal_kredit->IDMataUang       = $request->IDMataUang;
            $jurnal_kredit->Kurs             = $request->Kurs;
            $jurnal_kredit->Total_debet      = 0;
            $jurnal_kredit->Total_kredit     = AppHelper::StrReplace($request->pembayaran);
            $jurnal_kredit->Keterangan       = 'Uang Muka Pembelian Pesanan No ' . $Nomor;
            $jurnal_kredit->Saldo            = AppHelper::StrReplace($request->pembayaran);

            $jurnal_kredit->save();
        } else {
            $delete_um = UMSupplierModel::where('IDFaktur', $IDPO)->delete();
            $delete_jurnal_awal = JurnalModel::where('IDFaktur', $IDPO)->delete();
        }

        DB::commit();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.',
            'data'      => [
                'IDPO'     => $IDPO,
            ]
        );

        return json_encode($data);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akseshapus         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akseshapus==null){
            return redirect('Po')->with('alert', 'Anda Tidak Memiliki Akses Hapus');
        }
       
        
        $purchase_order = PurchaseOrderModel::findOrfail($id);

        $purchase_order->Status = 'Batal';
        $purchase_order->Batal = 'Batal';

        $purchase_order->save();

        $data = array (
            'status'    => true,
            'message'   => 'Data berhasil dibatalkan.'
        );

        return json_encode($data);
    }

    public function detail($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $purchase_order                 = DB::table('tbl_purchase_order')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang','=','tbl_purchase_order.IDMataUang')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_purchase_order.IDSupplier')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_purchase_order.*',  'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang')
                                    ->first();
        $purchase_order_detail           = DB::table('tbl_purchase_order_detail')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
                                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')
                                    ->where('tbl_purchase_order_detail.IDPO', $id)
                                    ->select('tbl_purchase_order_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang')
                                    ->get();
        $um                 = DB::table('tbl_um_supplier')
                                    ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor','=','tbl_um_supplier.Nomor_Faktur')
                                    ->join('tbl_coa', 'tbl_coa.IDCoa','=','tbl_um_supplier.IDCOA')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
                                    ->first();
        
        return view('po.detail', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'purchase_order', 'purchase_order_detail', 'um'));
    }

    public function print($id) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $purchase_order                 = DB::table('tbl_purchase_order')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang','=','tbl_purchase_order.IDMataUang')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_purchase_order.IDSupplier')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_purchase_order.*',  'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang')
                                    ->first();
        $purchase_order_detail           = DB::table('tbl_purchase_order_detail')
                                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_purchase_order_detail.IDBarang')
                                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_purchase_order_detail.IDSatuan')
                                    ->where('tbl_purchase_order_detail.IDPO', $id)
                                    ->select('tbl_purchase_order_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang')
                                    ->get();
        $um                 = DB::table('tbl_um_supplier')
                                    ->join('tbl_purchase_order', 'tbl_purchase_order.Nomor','=','tbl_um_supplier.Nomor_Faktur')
                                    ->join('tbl_coa', 'tbl_coa.IDCoa','=','tbl_um_supplier.IDCOA')
                                    ->where('tbl_purchase_order.IDPO', $id)
                                    ->select('tbl_um_supplier.*', 'tbl_coa.Nama_COA')
                                    ->first();
        
        $kurs = MataUangModel::findOrfail($purchase_order->IDMataUang);
        
        $perusahaan          = PerusahaanModel::Getforprint();
        
        return view('po.print', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'purchase_order', 'purchase_order_detail', 'um', 'perusahaan', 'kurs'));
    }

    public function get_barang() {
        DB::enableQueryLog();
        $queryBuilder = BarangModel::select('tbl_barang.*', 'tbl_satuan.Satuan');
        
        $queryBuilder->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan');

        if (Input::get('q')) {
            $queryBuilder->where('Nama_Barang', 'iLike', '%' . Input::get('q') . '%' );
        }

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_satuan() {
        $queryBuilder = VListSatuanKonversiModel::select('*');
        
        if (Input::get('q')) {
            $queryBuilder->where('Satuan', 'iLike', '%' . Input::get('q') . '%' );
        }
        $queryBuilder->where('IDBarang', Input::get('IDBarang'));

        $response = $queryBuilder->get();

        return json_encode($response);
    }

    public function get_harga_jual() {
        $queryBuilder = HargaJualModel::select('*');
        
        $queryBuilder->where('IDBarang', '=', Input::get('IDBarang'));
        $queryBuilder->where('IDSatuan', '=', Input::get('IDSatuan'));
        $queryBuilder->where('IDMataUang', '=', Input::get('IDMataUang'));

        $response = $queryBuilder->first();

        return json_encode($response);
    }

    public function get_coa(Request $request) {
        $jenis = $request->Jenis_Pembayaran;

        if($jenis=='transfer'){
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '10103.%')
                        ->get();
        } elseif($jenis=='giro') {
            $data = DB::table('tbl_coa')
                        ->where('Kode_COA', 'LIKE', '20102')
                        ->get();
        } elseif($jenis=='cash') {
            $data = DB::table('tbl_coa')
                        ->where('Nama_COA', 'LIKE', 'Kas%')
                        ->get();
        }

        return json_encode($data);
    }

    public function laporan_po(Request $request) {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        $mata_uang          = MataUangModel::where('Aktif', 'aktif')->get();
        return view('po.laporan', compact('coreset','aksesmenu', 'aksesmenudetail', 'aksessetting', 'namauser', 'mata_uang'));
    }

    public function datatable_detail(Request $request) {        
        $data = DB::table('tbl_purchase_order')
                ->join('tbl_purchase_order_detail', 'tbl_purchase_order.IDPO','=','tbl_purchase_order_detail.IDPO')
                ->join('tbl_barang', 'tbl_purchase_order_detail.IDBarang','=','tbl_barang.IDBarang')
                ->join('tbl_supplier', 'tbl_purchase_order.IDSupplier','=','tbl_supplier.IDSupplier')
                ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_purchase_order.IDMataUang')
                // ->select('tbl_purchase_order.*')
                ->select('tbl_barang.Nama_Barang', 'tbl_purchase_order.IDPO', 'tbl_purchase_order.Nomor', 'tbl_purchase_order.Tanggal', 'tbl_purchase_order_detail.Qty', 'tbl_purchase_order_detail.Harga', 'tbl_purchase_order_detail.Saldo', 'tbl_supplier.Nama','tbl_mata_uang.Mata_uang', 'tbl_mata_uang.Kode');
        if ($request->get('tanggal_awal')) {
            //tanggal awal tidak kosong            
            if ($request->get('tanggal_akhir')) {
                if($request->get('mata_uang')){
                    $data->whereBetween('tbl_purchase_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));
                }else{
                    $data->whereBetween('tbl_purchase_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                }
                if($request->get('field')&&$request->get('keyword')) {
                    $data->whereBetween('tbl_purchase_order.Tanggal', [AppHelper::DateFormat($request->get('tanggal_awal')), AppHelper::DateFormat($request->get('tanggal_akhir'))]);
                    $data->where('tbl_mata_uang.Kode', $request->get('mata_uang'));                    
                    $data->where(''.$request->get('field').'', 'ilike', '%' . $request->get('keyword') . '%');
                }
            } else {                
                $data->where('tbl_purchase_order.Tanggal', AppHelper::DateFormat($request->get('tanggal_awal')));
            }
        } else {
            //tanggal awal kosong
            if ($request->get('tanggal_akhir')) {
                $data->where('tbl_purchase_order.Tanggal', '<=', AppHelper::DateFormat($request->get('tanggal_akhir')));
            }
        }

        $data->orderBy('tbl_purchase_order.Tanggal', 'asc');
        
        $data->get();        
        return Datatables::of($data)->make(true);
    }
}
