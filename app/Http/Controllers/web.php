<?php


Route::get('/', function () {
return view('auth/login');
});

Auth::routes();
//===========================Route GET
Route::get('/logout', 'HomeController@logout');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/Bank', 'BankController@index');
Route::get('/Bank/tambah_bank', 'BankController@tambah_bank');
Route::get('/Bank/edit_bank/{id}', 'BankController@edit_bank');
Route::get('/Bank/delete/{id}', 'BankController@delete_bank');

Route::get('/GroupCOA', 'CoaController@indexgroup');
Route::get('/GroupCOA/tambah_group', 'CoaController@tambah_group');
Route::get('/GroupCOA/edit_group_coa/{id}', 'CoaController@edit_group');
Route::get('/GroupCOA/hapusgroup/{id}', 'CoaController@hapusgroup');

Route::get('/Coa', 'CoaController@indexcoa');
Route::get('/coa/tambah_coa', 'CoaController@tambah_coa');
Route::get('/coa/edit/{id}', 'CoaController@edit_coa');
Route::get('/coa/hapus_coa/{id}', 'CoaController@hapuscoa');
Route::get('/coa/exporttoexcel', 'CoaController@exporttoexcel');

Route::get('/Matauang', 'MatauangController@index');
Route::get('/Matauang/tambah_data', 'MatauangController@tambah_data');
Route::get('/Matauang/edit/{id}', 'MatauangController@edit_data');
Route::get('/Matauang/editstatus/{id}', 'MatauangController@editstatus');

Route::get('/Kota', 'KotaController@index');
Route::get('/Kota/tambah_data', 'KotaController@tambah_data');
Route::get('/Kota/edit_kota/{id}', 'KotaController@editkota');
Route::get('/Kota/hapus_kota/{id}', 'KotaController@delete');

Route::get('/Gudang', 'GudangController@index');
Route::get('/Gudang/tambah_data', 'GudangController@tambah_data');
Route::get('/gudang/edit/{id}', 'GudangController@editgudang');
Route::get('/Gudang/hapus_gudang/{id}', 'GudangController@hapusgudang');

Route::get('/GroupSupplier', 'GroupSupplierController@indexgroup');
Route::get('/GroupSupplier/tambah_group', 'GroupSupplierController@tambah_group');
Route::get('/GroupSupplier/edit/{id}', 'GroupSupplierController@editgroup');
Route::get('/GroupSupplier/hapus_groupsupplier/{id}', 'GroupSupplierController@hapusgroup');

Route::get('/Supplier', 'GroupSupplierController@indexsupplier');
Route::get('/Supplier/tambah_supplier', 'GroupSupplierController@tambah_supplier');
Route::get('/Supplier/update/{id}', 'GroupSupplierController@editsupplier');
Route::get('/Supplier/delete/{id}', 'GroupSupplierController@hapussupplier');

Route::get('/GroupCustomer', 'CustomerController@indexgroup');
Route::get('/GroupCustomer/tambah_group', 'CustomerController@tambah_group');
Route::get('/GroupCustomer/edit/{id}', 'CustomerController@editgrup');
Route::get('/GroupCustomer/delete/{id}', 'CustomerController@hapusgroup');

Route::get('/Customer', 'CustomerController@index');
Route::get('/Customer/tambah_data', 'CustomerController@tambah_customer');
Route::get('/Customer/edit_customer/{id}', 'CustomerController@editcustomer');
Route::get('/Customer/hapus_customer/{id}', 'CustomerController@hapus_customer');

Route::get('/GroupBarang', 'GroupbarangController@index');
Route::get('/Groupbarang/tambah_group', 'GroupbarangController@tambah_data');
Route::get('/GroupBarang/edit_group_barang/{id}', 'GroupbarangController@editdata');
Route::get('/GroupBarang/hapus_group_barang/{id}', 'GroupbarangController@hapusdata');

Route::get('/Kategori', 'KategoriController@index');
Route::get('/kategori/create', 'KategoriController@create');
Route::get('/kategori/edit/{id}', 'KategoriController@edit');
Route::get('/kategori/editstatus/{id}', 'KategoriController@status');

Route::get('/Ukuran', 'UkuranController@index');
Route::get('/Ukuran/create', 'UkuranController@tambah_data' );
Route::get('/Ukuran/edit/{id}', 'UkuranController@editdata');
Route::get('/Ukuran/editstatus/{id}', 'UkuranController@editstatus');
Route::get('Ukuran/datatable', 'UkuranController@datatable');

Route::get('/Satuan', 'SatuanController@index');
Route::get('/Satuan/create', 'SatuanController@create');
Route::get('/Satuan/update/{id}', 'SatuanController@update');
Route::get('/Satuan/delete/{id}', 'SatuanController@editstatus');

Route::get('/Tipe', 'TipeController@index');
Route::get('/Tipe/create', 'TipeController@create');
Route::get('/Tipe/update/{id}', 'TipeController@update');
Route::get('/Tipe/delete/{id}', 'TipeController@editstatus');

Route::get('/Barang', 'BarangController@index');
Route::get('/Barang/create', 'BarangController@create');
Route::get('/Barang/edit_barang/{id}', 'BarangController@update');
Route::get('/Barang/hapus_barang/{id}', 'BarangController@editstatus');
Route::get('Barang/pencarian', 'BarangController@pencarian');

Route::get('/HargaJualBarang', 'HargajualController@index');
Route::get('/HargaJualBarang/tambah_data', 'HargajualController@tambah_data');
Route::get('/HargaJualBarang/edit_harga_jual_barang/{id}', 'HargajualController@editdata');
Route::get('/HargaJualBarang/hapus_harga_jual_barang/{id}', 'HargajualController@editstatus');

Route::get('Po', 'POController@index');
Route::get('/Po/printpo/{id}', 'POController@printdetail');
Route::get('/Po/create', 'POController@create');
Route::get('/Po/editnew/{id}', 'POController@edit');
Route::get('/Po/show/{id}', 'POController@showdata');
Route::get('/Po/laporan_po', 'POController@laporanpo');
Route::post('/Po/laporan_po2', 'POController@laporanpo2');

Route::get('/PenerimaanBarang', 'PenerimaanBarangController@index');
Route::get('/PenerimaanBarang/show/{id}', 'PenerimaanBarangController@showdata');
Route::get('/PenerimaanBarang/printnewpb/{id}', 'PenerimaanBarangController@printdata');
Route::get('/PenerimaanBarang/create', 'PenerimaanBarangController@create');
Route::get('/PenerimaanBarang/check_PO/id={id}', 'PenerimaanBarangController@check_po');
Route::get('/PenerimaanBarang/edit/{id}', 'PenerimaanBarangController@editdata');
Route::get('PenerimaanBarang/hapus_data/{id}', 'PenerimaanBarangController@hapusdata');

Route::get('/InvoicePembelian', 'InvoicepembelianController@index');
Route::get('/InvoicePembelian/show/{id}', 'InvoicepembelianController@showdata');
Route::get('/InvoicePembelian/printfb/{id}', 'InvoicepembelianController@printdata');
Route::get('/InvoicePembelian/create', 'InvoicepembelianController@create');
Route::get('/InvoicePembelian/edit/{id}', 'InvoicepembelianController@edit');



Route::get('/ReturPembelian', 'ReturPembelianController@index');
Route::get('/ReturPembelian/tambah_retur_pembelian', 'ReturPembelianController@tambah_data');
Route::get('/ReturPembelian/show/{id}', 'ReturPembelianController@showdata');
Route::get('/ReturPembelian/printretur/{id}', 'ReturPembelianController@printretur');




//=====================================
//===========================Route POST
//=====================================
Route::post('Bank/simpan', 'BankController@simpan');
Route::post('Bank/simpanedit', 'BankController@simpanedit');
Route::post('Bank/pencarian', 'BankController@pencarian');

Route::post('GroupCOA/simpandatagroup', 'CoaController@simpandatagroup');
Route::post('GroupCOA/simpaneditgroup', 'CoaController@simpaneditgroup');
Route::post('GroupCOA/pencariangroup', 'CoaController@pencariangroup');

Route::post('coa/simpancoa', 'CoaController@simpandatacoa');
Route::post('coa/simpandataedit', 'CoaController@simpandataeditcoa');
Route::post('coa/pencariancoa', 'CoaController@pencariancoa');

Route::post('/Matauang/pencarian', 'MatauangController@pencarian');
Route::post('/Matauang/simpandata', 'MatauangController@simpandata');
Route::post('/Matauang/addidr', 'MatauangController@addidr');
Route::post('/Matauang/edit_matauang', 'MatauangController@edit_matauang');

Route::post('/Kota/pencarian', 'KotaController@pencarian');
Route::post('/Kota/simpandata', 'KotaController@simpandata');
Route::post('/Kota/simpandataedit', 'KotaController@simpandataedit');

Route::post('/Gudang/pencarian', 'GudangController@pencarian');
Route::post('/Gudang/simpandata', 'GudangController@simpangudang');
Route::post('/Gudang/simpanedit', 'GudangController@simpaneditgudang');

Route::post('/GroupSupplier/simpandatagroup', 'GroupSupplierController@simpangroup');
Route::post('/GroupSupplier/simpandatagroupedit', 'GroupSupplierController@simpangroupedit');
Route::post('/GroupSupplier/carigroup', 'GroupSupplierController@carigroup');

Route::post('/Supplier/carisupplier', 'GroupSupplierController@carisupplier');
Route::post('/Supplier/simpandatasupplier', 'GroupSupplierController@simpandatasupplier');
Route::post('/Supplier/simpandatagroup', 'GroupSupplierController@simpandatagroupsupplier');
Route::post('/Supplier/simpandatakota', 'GroupSupplierController@simpandatakota');
Route::post('/Supplier/simpandatasupplieredit', 'GroupSupplierController@editsuppliersave');

Route::post('/GroupCustomer/simpandatagroup', 'CustomerController@simpangroup');
Route::post('/GroupCustomer/carigroup', 'CustomerController@carigroup');
Route::post('/GroupCustomer/simpandatagroupedit', 'CustomerController@simpaneditgroup');

Route::post('/Customer/caricustomer', 'CustomerController@pencarian');
Route::post('/Customer/simpangroup', 'CustomerController@groupcreate');
Route::post('/Customer/simpankota', 'CustomerController@kotacreate');
Route::post('/Customer/simpandatacustomer', 'CustomerController@simpancustomer');
Route::post('/Customer/simpanedit', 'CustomerController@editcustomersimpan');

Route::post('/Groupbarang/pencarian', 'GroupbarangController@pencarian');
Route::post('/Groupbarang/simpandata', 'GroupbarangController@simpandata');
Route::post('/GroupBarang/simpandataedit', 'GroupbarangController@simpandataedit');

Route::post('/kategori/pencarian', 'KategoriController@pencarian');
Route::post('/kategori/simpandata', 'KategoriController@simpan_data');
Route::post('/kategori/simpandataedit', 'KategoriController@simpan_dataedit');

Route::post('/Ukuran/pencarian', 'UkuranController@pencarian');
Route::post('/Ukuran/simpandata', 'UkuranController@simpandata');
Route::post('/Ukuran/simpandataedit', 'UkuranController@simpandataedit');

Route::post('/Satuan/pencarian', 'SatuanController@pencarian');
Route::post('/Satuan/simpandata', 'SatuanController@simpandata');
Route::post('/Satuan/simpanedit', 'SatuanController@simpanedit');

Route::post('/Tipe/simpandata', 'TipeController@simpandata');
Route::post('/Tipe/simpanedit', 'TipeController@simpanedit');
Route::post('/Tipe/pencarian', 'TipeController@pencarian');

Route::post('/Barang/pencarian', 'BarangController@pencarian');
Route::post('/Barang/proses_group_barang', 'BarangController@simpangroup');
Route::post('/Barang/ambiltipe', 'BarangController@caritipe');
Route::post('/Barang/ambilukuran', 'BarangController@cariukuran');
Route::post('/Barang/ambilcode', 'BarangController@caricode');
Route::post('/Barang/simpandata', 'BarangController@simpandata');
Route::post('/Barang/simpandataedit', 'BarangController@simpandataedit');

Route::post('/HargaJualBarang/simpandata', 'HargajualController@simpandata');
Route::post('/HargaJualBarang/simpandataedit', 'HargajualController@simpandataedit');
Route::post('/HargaJualBarang/pencarian', 'HargajualController@pencarian');

Route::post('/Po/pencarian', 'POController@pencarian');
Route::post('/Po/get_kurs', 'POController@carikurs');
Route::post('/Po/getsatuan', 'POController@carisatuan');
Route::post('/Po/ambilhargabeli', 'POController@carihargabeli');
Route::post('/Po/simpandatapo', 'POController@simpandata');
Route::post('/Po/getcoa', 'POController@ambilcoa');
Route::post('/Po/simpandatapoedit', 'POController@simpandataedit');

Route::post('/PenerimaanBarang/indexcari', 'PenerimaanBarangController@pencarian');
Route::post('/PenerimaanBarang/tampildatapo', 'PenerimaanBarangController@caridatapo');
Route::post('/PenerimaanBarang/storenew', 'PenerimaanBarangController@simpandata');
Route::post('/PenerimaanBarang/tampildatapoedit', 'PenerimaanBarangController@caridatapoedit');
Route::post('/PenerimaanBarang/storenewedit', 'PenerimaanBarangController@simpandataedit');
Route::get('/Laporan_penerimaan_barang/index', 'PenerimaanBarangController@laporanpb');
Route::post('/Laporan_penerimaan_barang/laporan_pb', 'PenerimaanBarangController@carilaporan');

// INVOICE PEMBELIAN //
Route::post('/InvoicePembelian/pencarian', 'InvoicepembelianController@caridata');
Route::post('/InvoicePembelian/getnomorpb', 'InvoicepembelianController@getnopb');
Route::post('/InvoicePembelian/check_penerimaan_barang', 'InvoicepembelianController@cek_pb');
Route::post('/InvoicePembelian/simpan_invoice_pembelian', 'InvoicepembelianController@simpandata');
Route::post('/InvoicePembelian/ambilinvoicedetail', 'InvoicepembelianController@ambildetailedit');
Route::post('/InvoicePembelian/edit/simpan_invoice_pembelianedit', 'InvoicepembelianController@simpandataedit');
Route::get('InvoicePembelian/laporan_invoice', 'InvoicepembelianController@laporan');

// RETUR PEMBELIAN //
Route::post('/ReturPembelian/pencarian', 'ReturPembelianController@pencarian');
Route::post('/ReturPembelian/check_invoice_pembelian', 'ReturPembelianController@cek_invoice');
Route::post('/ReturPembelian/cek_grand_total', 'ReturPembelianController@cek_gt');
Route::post('/ReturPembelian/simpan_retur_pembelian', 'ReturPembelianController@simpandata');
Route::get('ReturPembelian/laporan_retur_pembelian', 'ReturPembelianController@laporan');

// PEMBAYARAN HUTANG //
Route::get('/PembayaranHutang', 'PembayaranHutangController@index');
Route::get('/PembayaranHutang/create', 'PembayaranHutangController@tambah_data');
Route::get('/PembayaranHutang/show/{id}', 'PembayaranHutangController@showdata');
Route::post('/PembayaranHutang/indexcari', 'PembayaranHutangController@pencarian');
Route::post('/PembayaranHutang/check_hutang', 'PembayaranHutangController@cek_hutang');
Route::post('/PembayaranHutang/COADP', 'PembayaranHutangController@coadp');
Route::post('/PembayaranHutang/getcoa', 'PembayaranHutangController@getcoa');
Route::post('/PembayaranHutang/store', 'PembayaranHutangController@simpandata');
Route::get('PembayaranHutang/pencarian_lap_pembayaran_hutang', 'PembayaranHutangController@laporan');

// ROUTE SALES ORDER //
Route::get('SalesOrder/laporan', 'SalesOrderController@laporan')->name('SalesOrder.laporan');
Route::get('SalesOrder/datatable', 'SalesOrderController@datatable')->name('SalesOrder.datatable');
Route::get('SalesOrder/datatable_detail', 'SalesOrderController@datatable_detail')->name('SalesOrder.datatable_detail');
Route::get('SalesOrder/get_barang', 'SalesOrderController@get_barang')->name('SalesOrder.get_barang');
Route::resource('SalesOrder', 'SalesOrderController');
Route::post('SalesOrder/number_so', 'SalesOrderController@number_so')->name('SalesOrder.number');
Route::post('SalesOrder/get_coa', 'SalesOrderController@get_coa')->name('SalesOrder.get_coa');
Route::post('SalesOrder/update_data', 'SalesOrderController@update_data')->name('SalesOrder.update_data');
Route::get('SalesOrder/detail/{id}', 'SalesOrderController@detail')->name('SalesOrder.detail');
Route::get('SalesOrder/print/{id}', 'SalesOrderController@print')->name('SalesOrder.print');

// SURAT JALAN //
Route::get('SuratJalan/laporan', 'SuratJalanController@laporan')->name('SuratJalan.laporan');
Route::get('SuratJalan/datatable_detail', 'SuratJalanController@datatable_detail')->name('SuratJalan.datatable_detail');
Route::get('SuratJalan/datatable', 'SuratJalanController@datatable')->name('SuratJalan.datatable');
Route::get('SuratJalan/get_sales_order_detail', 'SuratJalanController@get_sales_order_detail')->name('SuratJalan.sales_order_detail');
Route::resource('SuratJalan', 'SuratJalanController');
Route::post('SuratJalan/number_invoice', 'SuratJalanController@number_invoice')->name('SuratJalan.number');
Route::post('SuratJalan/get_sales_order', 'SuratJalanController@get_sales_order')->name('SuratJalan.sales_order');
Route::get('SuratJalan/detail/{id}', 'SuratJalanController@detail')->name('SuratJalan.detail');
Route::get('SuratJalan/print/{id}', 'SuratJalanController@print')->name('SuratJalan.print');

// INVOICE PENJUALAN //
Route::get('InvoicePenjualan/laporan', 'InvoicePenjualanController@laporan')->name('InvoicePenjualan.laporan');
Route::get('InvoicePenjualan/datatable', 'InvoicePenjualanController@datatable')->name('InvoicePenjualan.datatable');
Route::get('InvoicePenjualan/datatable_detail', 'InvoicePenjualanController@datatable_detail')->name('InvoicePenjualan.datatable_detail');
Route::get('InvoicePenjualan/get_surat_jalan_detail', 'InvoicePenjualanController@get_surat_jalan_detail')->name('InvoicePenjualan.surat_jalan_detail');
Route::resource('InvoicePenjualan', 'InvoicePenjualanController');
Route::post('InvoicePenjualan/number_invoice', 'InvoicePenjualanController@number_invoice')->name('InvoicePenjualan.number');
Route::post('InvoicePenjualan/get_surat_jalan', 'InvoicePenjualanController@get_surat_jalan')->name('InvoicePenjualan.surat_jalan');
Route::get('InvoicePenjualan/detail/{id}', 'InvoicePenjualanController@detail')->name('InvoicePenjualan.detail');
Route::get('InvoicePenjualan/print/{id}', 'InvoicePenjualanController@print')->name('InvoicePenjualan.print');
Route::post('InvoicePenjualan/get_coa', 'InvoicePenjualanController@get_coa')->name('InvoicePenjualan.get_coa');


// Bahan Baku //
Route::get('/Bahan_baku', 'BahanbakuController@index');
Route::get('/Bahanbaku', 'BahanbakuController@index');
Route::get('/Bahan_baku/produksi', 'BahanbakuController@index');
Route::get('/Bahan_baku/tambah_produksi', 'BahanbakuController@tambahdata');
Route::get('/Bahan_baku/tambah_produksigudang/{id}', 'BahanbakuController@tambahdata2');
Route::post('/Bahanbaku/ambilsatuan', 'BahanbakuController@ambilsatuan');
Route::post('/Bahan_baku/cekstok', 'BahanbakuController@cekstok');
Route::post('/Bahan_baku/simpandataproduksi', 'BahanbakuController@simpandata');
Route::post('Bahanbaku/cekdatadetailformula', 'BahanbakuController@cekdetailformula');
Route::get('Bahan_baku/produksi_show/{id}', 'BahanbakuController@showdata');
Route::get('Bahan_baku/produksi_edit/{id}', 'BahanbakuController@editdata');
Route::post('Bahan_baku/simpandataproduksiedit', 'BahanbakuController@simpanedit');
Route::get('Bahanbaku/printdata/{id}', 'BahanbakuController@printdata');
Route::post('Bahanbaku/pencarian', 'BahanbakuController@caridata');


// RETUR PENJUALAN //
Route::get('ReturPenjualan/laporan', 'ReturPenjualanController@laporan')->name('ReturPenjualan.laporan');
Route::get('ReturPenjualan/datatable', 'ReturPenjualanController@datatable')->name('ReturPenjualan.datatable');
Route::get('ReturPenjualan/datatable_detail', 'ReturPenjualanController@datatable_detail')->name('ReturPenjualan.datatable_detail');
Route::get('ReturPenjualan/get_penjualan_detail', 'ReturPenjualanController@get_penjualan_detail')->name('ReturPenjualan.penjualan_detail');
Route::resource('ReturPenjualan', 'ReturPenjualanController');
Route::post('ReturPenjualan/number_invoice', 'ReturPenjualanController@number_invoice')->name('ReturPenjualan.number');
Route::post('ReturPenjualan/get_penjualan', 'ReturPenjualanController@get_penjualan')->name('ReturPenjualan.penjualan');
Route::get('ReturPenjualan/detail/{id}', 'ReturPenjualanController@detail')->name('ReturPenjualan.detail');
Route::get('ReturPenjualan/print/{id}', 'ReturPenjualanController@print')->name('ReturPenjualan.print');

// FORMULA //
Route::get('Formula', 'FormulaController@index');
Route::get('/Formula/tambahdata', 'FormulaController@tambah_data');
Route::post('Formula/simpandata', 'FormulaController@simpandata');
Route::get('Formula/show/{id}', 'FormulaController@showdata');
Route::get('Formula/hapus_data/{id}', 'FormulaController@hapusdata');
Route::post('Formula/getsatuan', 'FormulaController@getsatuan');
Route::get('Formula/edit/{id}', 'FormulaController@editdata');
Route::post('Formula/simpandataedit', 'FormulaController@simpandataedit');

// PENERIMAAN BARANG JADI //
Route::get('BarangJadi', 'PenerimaanBarangJadiController@index');
Route::get('BarangJadi/tambahdata', 'PenerimaanBarangJadiController@create');
Route::post('PenerimaanBarangJadi/ambildetail', 'PenerimaanBarangJadiController@ambildetail');
Route::post('BarangJadi/simpandata', 'PenerimaanBarangJadiController@simpandata');
Route::get('BarangJadi/showdata/{id}', 'PenerimaanBarangJadiController@showdata');
Route::get('BarangJadi/printdata/{id}', 'PenerimaanBarangJadiController@printdata');
Route::get('BarangJadi/edit/{id}', 'PenerimaanBarangJadiController@editdata');
Route::post('BarangJadi/simpandataedit', 'PenerimaanBarangJadiController@editdatasimpan');

// PENERIMAAN PIUTANG //
Route::get('PenerimaanPiutang/laporan', 'PenerimaanPiutangController@laporan')->name('PenerimaanPiutang.laporan');
Route::get('PenerimaanPiutang/get_piutang', 'PenerimaanPiutangController@get_piutang')->name('PenerimaanPiutang.piutang');
Route::get('PenerimaanPiutang/datatable', 'PenerimaanPiutangController@datatable')->name('PenerimaanPiutang.datatable');
Route::resource('PenerimaanPiutang', 'PenerimaanPiutangController');
Route::post('PenerimaanPiutang/number_invoice', 'PenerimaanPiutangController@number_invoice')->name('PenerimaanPiutang.number');
Route::post('PenerimaanPiutang/get_coa', 'PenerimaanPiutangController@get_coa')->name('PenerimaanPiutang.get_coa');
Route::get('PenerimaanPiutang/detail/{id}', 'PenerimaanPiutangController@detail')->name('PenerimaanPiutang.detail');

// LAPORAN STOK //
Route::get('Laporan_stok', 'StokController@index');
Route::post('stok/pencarian', 'StokController@caridata');
Route::get('stok/kartustok/{id}', 'StokController@kartustok');
