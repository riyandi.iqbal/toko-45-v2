<?php
/*=====Create TIAR @14/04/2019====*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

use Datatables;
use Validator;

use App\Models\SatuanModel;

class SatuanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        $iduser             = Auth::user()->id;
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();

        return view('satuan.index', compact('aksesmenu', 'aksesmenudetail', 'aksessetting','coreset', 'namauser'));
    }

    public function datatable(Request $request) {
        $data = SatuanModel::select('*');

        if ($request->get('field')) {
            $data->where($request->get('field'), 'iLike', '%'.$request->get('keyword'). '%');
        }

        $data->get();

        return Datatables::of($data)->make(true);
    }

    public function create() {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Satuan')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $kode               = SatuanModel::selectRaw(DB::raw('MAX(SUBSTRING("Kode_Satuan", 4, 4)) as curr_number'))->first();
        
        return view('satuan.create', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'kode'));
    }

    public function store(Request $request) {
        $validate = Validator::make($request->all(), [
            'Kode_Satuan'    => 'required|unique:tbl_satuan,Kode_Satuan',
            'Satuan'             => 'required',
        ])->setAttributeNames([
            'Kode_Satuan'    => 'Kode',
            'Satuan'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();
        $nextnumber = SatuanModel::selectRaw(DB::raw('MAX("IDSatuan") as nonext'))->first();   
        if($nextnumber->nonext==''){
          $urutan_id = 'P000001';
        }else{
          $hasil = substr($nextnumber->nonext,2,6) + 1;
          $urutan_id = 'P'.str_pad($hasil, 6, 0, STR_PAD_LEFT);
        }

        $data = new SatuanModel;

        $data->IDSatuan            = $urutan_id;
        $data->Kode_Satuan         = $request->Kode_Satuan;
        $data->Satuan           = $request->Satuan;
        $data->Aktif            = 'aktif';
        $data->CID               = Auth::user()->id;
        $data->CTime             = date('Y-m-d H:i:s');
        
        $data->save();

        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil disimpan.'
        );

        return json_encode($response);
    }

    public function show($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Tambah', '=', 'yes')->where('IDUser', '=', $iduser)->first();
        if($akses==null){
            return redirect('Satuan')->with('alertakses', 'Anda Tidak Memiliki Akses');
        }
        $namauser           = Auth::user()->name;
        $aksesmenu          = app('App\Http\Controllers\Aksesmenu')->aksesmenu();
        $aksesmenudetail    = app('App\Http\Controllers\Aksesmenu')->aksesmenudetail();
        $aksessetting       = app('App\Http\Controllers\Aksesmenu')->aksessetting();
        $coreset            = app('App\Http\Controllers\Aksesmenu')->coreset();
        
        $satuan               = SatuanModel::findOrfail($id);

        return view('satuan.update', compact('aksesmenu', 'aksesmenudetail', 'aksessetting', 'coreset','namauser', 'satuan'));
    }

    public function update_data(Request $request) {
        $validate = Validator::make($request->all(), [
            'IDSatuan'  => 'required',
            'Kode_Satuan'    => 'required',
            'Satuan'             => 'required',
        ])->setAttributeNames([
            'IDSatuan'  => 'Data',
            'Kode_Satuan'    => 'Kode',
            'Satuan'             => 'Nama',
        ]);
        
        if ($validate->fails()) {
            $data = [
                'status'    => false,
                'message'   => strip_tags($validate->errors()->first())
            ];
            return json_encode($data);
        }

        DB::beginTransaction();

        $data = SatuanModel::findOrfail($request->IDSatuan);

        $data->Kode_Satuan         = $request->Kode_Satuan;
        $data->Satuan         = $request->Satuan;
        $data->Aktif                = 'aktif';
        $data->MID               = Auth::user()->id;
        $data->MTime             = date('Y-m-d H:i:s');
        
        $data->save();
        
        DB::commit();

        $response = array (
            'status'    => true,
            'message'   => 'Data berhasil diubah.'
        );

        return json_encode($response);
    }

    public function destroy($id) {
        $iduser             = Auth::user()->id;
        $akses         = DB::table('users_akses')->where('Hapus', '=', 'yes')->where('IDUser', '=', $iduser)->first();

        if(! $akses){
            $data = [
                'status'    => false,
                'message'   => 'Anda Tidak Memiliki Akses'
            ];
            return json_encode($data);
        }

        $data_exist  = SatuanModel::findOrfail($id);

        if ($data_exist->Aktif == 'aktif') {
            $data_exist->Aktif = 'nonaktif';
        } else {
            $data_exist->Aktif = 'aktif';
        }
        
        $data_exist->save();
        
        $data = array (
            'status'    => true,
            'message'   => 'Aktif berhasil diubah.'
        );

        return json_encode($data);
    }
}