<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListSatuanKonversiModel extends Model
{
    protected $table = "vlistsatuankonversi";
    protected $primaryKey = "IDSatuanKonversi";
    public $incrementing = false;
    public $timestamps = false;
}