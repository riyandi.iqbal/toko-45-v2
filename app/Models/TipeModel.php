<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipeModel extends Model
{
    protected $table = 'tbl_tipe_barang';
    protected $primaryKey = 'IDTipe';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTipe', 'Kode_Tipe', 'Nama_Tipe', 'Status', 'CID', 'CTime', 'MID', 'MTime'
    ];
}