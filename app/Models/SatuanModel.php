<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SatuanModel extends Model
{
    protected $table = 'tbl_satuan';
    protected $primaryKey = 'IDSatuan';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDSatuan', 'Kode_Satuan', 'Satuan', 'Aktif', 'CID', 'CTime', 'MID', 'MTime'
    ];
}
