<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermissionModel extends Model {
    protected $table = 'users_akses';
    protected $primaryKey = 'IDAkses';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDAkses', 'IDUser', 'Tambah', 'Ubah', 'Hapus'
    ];


}
