<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurnalUmumDetailModel extends Model
{
    protected $table = 'tbl_jurnal_umum_detail';
    protected $primaryKey = 'IDJUDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDJUDetail', 'IDJU', 'IDCOA', 'Posisi', 'Debet', 'Kredit',
        'IDMataUang', 'Kurs', 'Keterangan', 'Urutan'
    ];
}
