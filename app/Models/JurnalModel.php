<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JurnalModel extends Model {
    protected $table = 'tbl_jurnal';
    protected $primaryKey = 'IDJurnal';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDJurnal', 'Tanggal', 'Nomor', 'IDFaktur', 'IDFakturDetail', 'Jenis_faktur', 'IDCOA', 'Debet', 'Kredit',
        'IDMataUang', 'Kurs', 'Total_debet', 'Total_kredit', 'Keterangan', 'Saldo', 'IDCOAAnak'
    ];

    public function scopeGetforjurnal()
    {
        return DB::select('select a.*, 
                                b."Kode_COA", 
                                b."Nama_COA",
                                b."Status", 
                                    (select d."Nama_COA"
                                        from tbl_jurnal c, 
                                        tbl_coa d 
                                    where a."IDJurnal"=c."IDJurnal" 
                                    and c."IDCOA"=d."IDCoa") as coa_anak,
                                    (select d."Kode_COA"
                                        from tbl_jurnal c, 
                                        tbl_coa d 
                                    where a."IDJurnal"=c."IDJurnal" 
                                    and c."IDCOA"=d."IDCoa") as kode_anak  
                            from tbl_jurnal a, tbl_coa b
                            where a."IDCOA"=b."IDCoa"
                            ORDER BY a."IDJurnal" ASC
                            ');
                        
    }


}
