<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HargaJualModel extends Model
{
    protected $table = 'tbl_harga_jual_barang';
    protected $primaryKey = 'IDHargaJual';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDHargaJual', 'IDBarang', 'IDSatuan', 'IDGroupCustomer', 'Harga_Jual', 'CID', 'CTime', 'MID', 'MTime',
        'Status', 'IDMataUang'
    ];

    public function scopeGetforindex() {
        return  DB::table('tbl_harga_jual_barang')
        ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_harga_jual_barang.IDBarang')
        ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan','=','tbl_harga_jual_barang.IDSatuan')
        ->leftJoin('tbl_mata_uang', 'tbl_mata_uang.IDMataUang','=','tbl_harga_jual_barang.IDMataUang')
        ->leftJoin('tbl_group_customer', 'tbl_group_customer.IDGroupCustomer','=','tbl_harga_jual_barang.IDGroupCustomer')
        ->select('tbl_barang.Nama_Barang', 'tbl_satuan.Satuan','tbl_harga_jual_barang.*', 'tbl_group_customer.Nama_Group_Customer', 'tbl_mata_uang.Mata_uang')                
        ->orderBy('tbl_barang.Nama_Barang', 'asc');
    }

    public function scopeDatatable() {
        return  DB::table('tbl_harga_jual_barang')
        ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_harga_jual_barang.IDBarang')
        ->select('tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang', 'tbl_barang.IDBarang')
        ->groupBy('tbl_barang.Nama_Barang')
        ->groupBy('tbl_barang.Kode_Barang')
        ->groupBy('tbl_barang.IDBarang');
    }
}
