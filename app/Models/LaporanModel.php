<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LaporanModel extends Model {
    protected $table = 'tbl_menu_detail';
    protected $primaryKey = 'IDMenuDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDMenuDetail', 'IDMenu', 'Menu_Detail', 'Url', 'Urutan', 'Keterangan'
    ];

}
