<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembayaranHutangBayarModel extends Model {
    protected $table = 'tbl_pembayaran_hutang_bayar';
    protected $primaryKey = 'IDBSBayar';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDBSBayar', 'IDBS', 'Jenis_pembayaran', 'IDCOA', 'Nomor_giro', 'Nominal_pembayaran', 'IDMataUang',
        'Kurs', 'Status_giro', 'Tanggal_giro', 'CID', 'CTime', 'MID', 'MTime', 'Nilai_kurs'
    ];
}
