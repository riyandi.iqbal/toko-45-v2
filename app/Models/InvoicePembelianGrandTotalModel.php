<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePembelianGrandTotalModel extends Model {
    protected $table = 'tbl_pembelian_grand_total';
    protected $primaryKey = 'IDFBGrandTotal';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFBGrandTotal', 'IDFB', 'Pembayaran', 'IDMataUang', 'Kurs', 'DPP',
        'Discount', 'PPN', 'Grand_total', 'Sisa', 'Batal', 'total_invoice_diskon'
    ];


}
