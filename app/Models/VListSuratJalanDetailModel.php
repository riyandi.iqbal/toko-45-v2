<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListSuratJalanDetailModel extends Model
{
    protected $table = "vlistsuratjalandetail";
    protected $primaryKey = "idsjcdetail";
    public $incrementing = false;
    public $timestamps = false;
}