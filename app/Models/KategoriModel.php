<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    protected $table = 'tbl_kategori';
    protected $primaryKey = 'IDKategori';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKategori', 'Kode_Kategori', 'Nama_Kategori', 'Status', 'CID', 'CTime', 'MID', 'MTime', 'IDGroupBarang'
    ];
}