<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PenerimaanBarangModel extends Model {
    protected $table = 'tbl_terima_barang_supplier';
    protected $primaryKey = 'IDTBS';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTBS', 'Tanggal', 'Nomor', 'IDSupplier', 'Nomor_sj', 'IDPO', 'NoSO',
        'Total_qty_yard', 'Total_qty_meter', 'Saldo_yard', 'Saldo_meter', 'Keterangan',
        'Jenis_TBS', 'Batal', 'Total_harga', 'Status_pakai', 'create_inv', 'IDGudang', 'IDMataUang'
    ];
}
