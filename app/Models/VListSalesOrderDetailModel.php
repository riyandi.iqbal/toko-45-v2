<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListSalesOrderDetailModel extends Model
{
    protected $table = "vlistsalesorderdetail";
    protected $primaryKey = "IDSOKDetail";
    public $incrementing = false;
    public $timestamps = false;
}