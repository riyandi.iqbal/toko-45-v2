<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanGrandTotalModel extends Model {
    protected $table = 'tbl_penjualan_grand_total';
    protected $primaryKey = 'IDFJGrandTotal';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFJGrandTotal', 'IDFJ', 'Pembayaran', 'IDMataUang', 'Kurs', 'DPP',
        'Discount', 'PPN', 'Grand_total', 'Sisa', 'Batal'
    ];


}
