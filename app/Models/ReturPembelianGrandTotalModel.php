<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPembelianGrandTotalModel extends Model {
    protected $table = 'tbl_retur_pembelian_grand_total';
    protected $primaryKey = 'IDRBGrandTotal';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDRBGrandTotal', 'IDRB', 'Pembayaran', 'Pembayaran', 'Kurs', 'DPP', 'Discount', 'PPN', 'Grand_total', 'Sisa'
    ];


}
