<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPembelianDetailModel extends Model {
    protected $table = 'tbl_retur_pembelian_detail';
    protected $primaryKey = 'IDRBDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDRBDetail', 'IDRB', 'IDBarang', 'Qty_yard', 'Saldo_yard', 'IDSatuan', 'Harga', 
        'Subtotal' 
    ];

    public function scopegetItem() {
        return ReturPembelianDetailModel::join("tbl_retur_pembelian", "tbl_retur_pembelian_detail.IDRB","=","tbl_retur_pembelian.IDRB")
        ->join("tbl_barang", "tbl_barang.IDBarang","=","tbl_retur_pembelian_detail.IDBarang")
        ->join("tbl_satuan", "tbl_retur_pembelian_detail.IDSatuan","=","tbl_satuan.IDSatuan")
        ->select("tbl_retur_pembelian_detail.*", "tbl_barang.Nama_Barang", "tbl_barang.Kode_Barang", 'tbl_satuan.Satuan');
    }
}
