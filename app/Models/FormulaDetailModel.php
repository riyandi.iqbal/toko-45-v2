<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FormulaDetailModel extends Model
{
    protected $table = 'tbl_formula_detail';
    protected $primaryKey = 'IDFormulaDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFormula', 'IDFormulaDetail', 'Nama_Barang', 'Berat', 'IDSatuan'
    ];

    public function scopeGetdetail()
    {
        return DB::table('tbl_formula_detail')
            ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_formula_detail.IDBarang')
            ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_formula_detail.IDSatuan')
            ->select('tbl_formula_detail.*', 'tbl_satuan.Satuan', 'tbl_barang.Nama_Barang');
    }

    
}
