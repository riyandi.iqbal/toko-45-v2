<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListGiroModel extends Model
{
    protected $table = "vlistgiro";
    protected $primaryKey = "IDGiro";
    public $incrementing = false;
    public $timestamps = false;
}