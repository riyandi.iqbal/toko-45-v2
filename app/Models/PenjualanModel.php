<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenjualanModel extends Model {
    protected $table = 'tbl_penjualan';
    protected $primaryKey = 'IDFJ';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFJ', 'Nomor', 'Tanggal', 'IDCustomer', 'Nama_di_faktur', 'IDSJC', 'TOP',
        'Tanggal_jatuh_tempo', 'IDMataUang', 'Kurs', 'Total_qty', 'Saldo_qty',
        'Discount', 'Status_ppn', 'Keterangan', 'Batal', 'Grand_total', 'Status_pakai',
        'DPP', 'PPN',
        'dibuat_pada', 'diubah_pada'
    ];

    public function scopeUMCustomer($id)
    {
        return DB::table('tbl_penjualan')->select(
            'tbl_penjualan.IDFJ',
            'tbl_penjualan.Tanggal',
            'tbl_penjualan.IDSJC',
            'tbl_sales_order.IDSOK',
            'tbl_um_customer.Nilai_UM')->leftJoin('tbl_surat_jalan_customer', 'tbl_penjualan.IDSJC', '=', 'tbl_surat_jalan_customer.idsjc')->leftJoin('tbl_sales_order', 'tbl_sales_order.IDSOK', '=', 'tbl_surat_jalan_customer.idso')->leftJoin('tbl_um_customer', 'tbl_um_customer.IDFaktur', '=', 'tbl_sales_order.IDSOK')->where('tbl_penjualan.IDFJ', $id)->first();
    }
}
