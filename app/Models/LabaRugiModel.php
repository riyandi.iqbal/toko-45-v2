<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LabaRugiModel extends Model {
    protected $table = 'tbl_setting_labarugi';
    protected $primaryKey = 'IDLabaRugi';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDLabaRugi', 'IDCoa', 'Keterangan', 'Kategori', 'Perhitungan', 'Group', 'Urutan'
    ];

    public function childs() {
        return $this->hasMany('App\Models\LabaRugiModel','Group','IDCoa')
                        ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_labarugi.IDCoa')
                        ->orderBy('tbl_setting_labarugi.IDLabaRugi', 'asc') ;
        
    }
}
