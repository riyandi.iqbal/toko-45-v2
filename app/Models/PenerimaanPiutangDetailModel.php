<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenerimaanPiutangDetailModel extends Model {
    protected $table = 'tbl_penerimaan_piutang_detail';
    protected $primaryKey = 'IDTPDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTPDetail', 'IDTP', 'IDFJ', 'Nomor', 'Tanggal_fj', 'Nilai_fj', 'Telah_diterima', 'Diterima',
        'UM', 'Retur', 'Saldo', 'Selisih', 'IDMataUang', 'Kurs', 'IDPiutang'
    ];
}
