<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KoreksiPersediaanDetailModel extends Model {
    protected $table = 'tbl_koreksi_persediaan_detail';
    protected $primaryKey = 'IDKPDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKPDetail', 'IDKP', 'IDBarang', 'IDSatuan', 'Barcode', 'IDMataUang', 'Qty', 'Saldo_qty', 'Harga', 'Kurs'
    ];
}
