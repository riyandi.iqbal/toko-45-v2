<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupplierModel extends Model {
    protected $table = 'tbl_supplier';
    protected $primaryKey = 'IDSupplier';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDSupplier', 'IDGroupSupplier', 'Kode_Supplier', 'Nama'
        ,'Alamat', 'No_Telpon', 'IDKota', 'Fax', 'Email', 'NPWP',
        'No_KTP', 'Aktif'
    ];


}
