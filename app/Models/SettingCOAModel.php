<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class SettingCOAModel extends Model
{
    protected $table = 'tbl_settingcoa';
    protected $primaryKey = 'IDSETCOA';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDSETCOA','IDMenu', 'IDCoa', 'Posisi', 'CID', 'CTime', 'MID', 'MTime', 'Tingkat', 'Cara'
    ];

    public function scopeGetindex()
    {
        return DB::table('tbl_settingcoa')
                    ->join('tbl_menu_detail', 'tbl_settingcoa.IDMenu', '=', 'tbl_menu_detail.IDMenuDetail')
                    ->join('tbl_coa', 'tbl_settingcoa.IDCoa', '=', 'tbl_coa.IDCoa')
                    ->select('tbl_coa.Nama_COA', 'tbl_menu_detail.Menu_Detail', 'tbl_settingcoa.*')
                    ->groupBy('tbl_settingcoa.IDSETCOA', 'tbl_coa.Nama_COA', 'tbl_menu_detail.Menu_Detail', 'tbl_menu_detail.IDMenuDetail')
                    ->orderBy('tbl_menu_detail.IDMenuDetail');
    }
    //================invoice pembelian
    public function scopeGetppnfaktur()
    {
        return DB::table('tbl_settingcoa')
                    ->where('IDMenu', '=', '44')
                    ->where('Tingkat', '=', 'tambahan')
                    ->where('Cara', '=', 'utang')
                    ->where('Posisi', '=', 'debet')
                    ->first();
    }
    public function scopeGetdppfaktur()
    {
        return DB::table('tbl_settingcoa')
                    ->where('IDMenu', '=', '44')
                    ->where('Tingkat', '=', 'utama')
                    ->where('Cara', '=', 'utang')
                    ->where('Posisi', '=', 'debet')
                    ->first();
    }
    public function scopeGetkreditfaktur()
    {
        return DB::table('tbl_settingcoa')
                    ->where('IDMenu', '=', '44')
                    ->where('Tingkat', '=', 'utama')
                    ->where('Cara', '=', 'utang')
                    ->where('Posisi', '=', 'kredit')
                    ->first();
    }

}
