<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetailModel extends Model
{
    protected $table = 'tbl_sales_order_detail';
    protected $primaryKey = 'IDSOKDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDSOKDetail', 'IDSOK', 'IDBarang', 'Qty', 'Saldo_qty', 
        'IDSatuan', 'Harga', 'Sub_total',
        'dibuat_pada'
    ];
}
