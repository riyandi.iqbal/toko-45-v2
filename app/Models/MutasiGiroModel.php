<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MutasiGiroModel extends Model {
    protected $table = 'tbl_mutasi_giro';
    protected $primaryKey = 'IDMG';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDMG', 'Tanggal', 'Nomor', 'Total', 'IDMataUang', 'IDMataUang', 'Keterangan', 'Batal',
        'dibuat_pada', 'dibuat_oleh', 'diubah_pada', 'diubah_oleh'
    ];
}
