<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ControlPanelModel extends Model
{
    protected $table = 'tbl_controlpanel';
    protected $primaryKey = 'IDCP';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDCP', 'Nama', 'Link', 'Urutan'
    ];

    public function scopeGetperusahaan()
    {
        return DB::table('tbl_perusahaan')->where('IDPerusahaan', '=', '1')->first();
    }

    public function scopeGetcoabayar()
    {
        return DB::table('tbl_coa')->whereIn('IDCoa', ['P000093', 'P000003'])->get();
    }

    public function scopeGetformenusetting()
    {
        return DB::table('tbl_menu_detail')->join('tbl_menu', 'tbl_menu.IDMenu', '=', 'tbl_menu_detail.IDMenu')
                    ->whereIn('tbl_menu.IDMenu', ['12', '19'])
                    ->where('tbl_menu_detail.Menu_Detail', 'not like', '%Laporan%')
                    ->orderBy('tbl_menu.IDMenu', 'asc')
                    ->orderBy('tbl_menu_detail.IDMenuDetail', 'asc')
                    ->get();
    }
}
