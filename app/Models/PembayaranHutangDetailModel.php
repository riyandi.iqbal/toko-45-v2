<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembayaranHutangDetailModel extends Model {
    protected $table = 'tbl_pembayaran_hutang_detail';
    protected $primaryKey = 'IDBSDet';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDBSDet', 'IDBS', 'IDFB', 'Nomor', 'Tanggal_fb', 'Nilai_fb', 'Telah_diterima', 'Diterima', 'UM',
        'Retur', 'Saldo', 'Selisih', 'IDMataUang', 'Kurs', 'IDHutang'
    ];
}
