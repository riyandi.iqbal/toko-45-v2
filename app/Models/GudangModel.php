<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GudangModel extends Model
{
    protected $table = 'tbl_gudang';
    protected $primaryKey = 'IDGudang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGudang', 'Kode_Gudang', 'Nama_Gudang', 'Aktif'
    ];
}
