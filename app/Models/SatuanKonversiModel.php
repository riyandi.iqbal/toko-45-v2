<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SatuanKonversiModel extends Model
{
    protected $table = 'tbl_satuan_konversi';
    protected $primaryKey = 'IDSatuanKonversi';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDSatuanKonversi', 'IDSatuanKecil', 'IDSatuanBesar', 'Qty', 'IDBarang',
        'dibuat_pada', 'dibuat_oleh', 'diubah_pada', 'diubah_oleh'
    ];

    public function scopeDatatable() {
        return SatuanKonversiModel::join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_satuan_konversi.IDBarang')
        ->select('tbl_barang.Nama_Barang', 'tbl_barang.Kode_Barang', 'tbl_barang.IDBarang')
        ->groupBy('tbl_barang.Nama_Barang')
        ->groupBy('tbl_barang.Kode_Barang')
        ->groupBy('tbl_barang.IDBarang');
    }
}
