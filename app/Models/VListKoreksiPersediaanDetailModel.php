<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListKoreksiPersediaanDetailModel extends Model
{
    protected $table = "vlistkoreksipersediaandetail";
    protected $primaryKey = "IDKPDetail";
    public $incrementing = false;
    public $timestamps = false;
}