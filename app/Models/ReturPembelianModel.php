<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPembelianModel extends Model {
    protected $table = 'tbl_retur_pembelian';
    protected $primaryKey = 'IDRB';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDRB', 'Tanggal', 'Nomor', 'IDFB', 'IDSupplier', 'Tanggal_fb', 'Total_qty_yard', 'Total_qty_meter',
        'Saldo_yard', 'Saldo_meter', 'Discount', 'Status_ppn', 'Keterangan', 'Batal', 'Status_pakai' 
    ];

    public function scopegetIndex()
    {
        return ReturPembelianModel::join("tbl_supplier", "tbl_supplier.IDSupplier","=","tbl_retur_pembelian.IDSupplier")
                                  ->join("tbl_pembelian", "tbl_pembelian.IDFB","=","tbl_retur_pembelian.IDFB")
                                  ->join("tbl_mata_uang", "tbl_mata_uang.IDMataUang", "=", "tbl_pembelian.IDMataUang")
                                  ->select('tbl_retur_pembelian.*', 
                                            'tbl_supplier.Nama', 
                                            'tbl_pembelian.Nomor AS Nomor_inv',
                                            'tbl_mata_uang.Mata_uang', 'tbl_mata_uang.Kode');
    }
}
