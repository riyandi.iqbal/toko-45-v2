<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenerimaanBarangDetailModel extends Model {
    protected $table = 'tbl_terima_barang_supplier_detail';
    protected $primaryKey = 'IDTBSDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTBSDetail', 'IDTBS', 'IDBarang', 'Qty_yard', 'Qty_meter', 'IDSatuan', 'Harga'
    ];

    public function scopeGetItem() {
        return PenerimaanBarangDetailModel::select('*')
                                            ->leftJoin('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_terima_barang_supplier_detail.IDBarang')
                                            ->leftJoin('tbl_satuan', 'tbl_satuan.IDSatuan', '=','tbl_terima_barang_supplier_detail.IDSatuan');
    } 
}

