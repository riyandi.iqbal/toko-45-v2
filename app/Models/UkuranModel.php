<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UkuranModel extends Model
{
    protected $table = 'tbl_ukuran';
    protected $primaryKey = 'IDUkuran';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDUkuran', 'Kode_Ukuran', 'Nama_Ukuran', 'Status', 'CID', 'CTime', 'MID', 'MTime'
    ];
}