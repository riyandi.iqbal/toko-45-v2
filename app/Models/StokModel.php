<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StokModel extends Model
{
    protected $table = 'tbl_stok';
    protected $primaryKey = 'IDStok';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDStok', 'Tanggal', 'Nomor_faktur', 'Jenis_faktur', 'Barcode',
        'IDBarang', 'IDGudang', 'Qty_yard', 'Qty_meter', 'Saldo_yard', 'Saldo_meter',
        'Grade', 'IDSatuan', 'Harga', 'Total', 'Qty_pcs', 'Saldo_pcs', 'Nama_Barang'
    ];

    public function scopeTampilstok()
    {
        return DB::table('tbl_stok')
                        ->join("tbl_satuan", "tbl_stok.IDSatuan","=","tbl_satuan.IDSatuan")
                        ->join("tbl_barang", "tbl_barang.IDBarang", "=", "tbl_stok.IDBarang")
                        ->join("tbl_kategori", 'tbl_kategori.IDKategori', '=', "tbl_barang.IDKategori")
                        ->join("tbl_tipe_barang", 'tbl_tipe_barang.IDTipe', '=', 'tbl_barang.IDTipe')
                        ->join("tbl_gudang", 'tbl_gudang.IDGudang', '=', 'tbl_stok.IDGudang')
                        ->join('tbl_ukuran', 'tbl_ukuran.IDUkuran', '=', 'tbl_barang.IDUkuran');
    
    }

    public function scopeTampildetailstok()
    {
        return DB::table('tbl_kartu_stok')
                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_kartu_stok.IDBarang')
                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_kartu_stok.IDSatuan')
                    ->select('tbl_kartu_stok.*', 'tbl_barang.Nama_Barang', 'tbl_satuan.Satuan');
    }
}
