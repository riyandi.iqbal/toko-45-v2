<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenerimaanBarangJadiDetailModel extends Model {
    protected $table = 'tbl_penerimaan_barang_jadi_detail';
    protected $primaryKey = 'IDPBJDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPBJDetail', 'IDPBJ', 'IDBarang', 'IDTipe', 'IDUkuran', 'Berat', 'IDSatuan', 'Total', 'Qty'
    ];

    public function scopeGetdetail()
    {
        return DB::table('tbl_penerimaan_barang_jadi_detail')
                    ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_penerimaan_barang_jadi_detail.IDBarang')
                    ->join('tbl_tipe_barang', 'tbl_tipe_barang.IDTipe', '=', 'tbl_penerimaan_barang_jadi_detail.IDTipe')
                    ->join('tbl_ukuran', 'tbl_ukuran.IDUkuran', '=', 'tbl_penerimaan_barang_jadi_detail.IDUkuran')
                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_penerimaan_barang_jadi_detail.IDSatuan');
    }

}

