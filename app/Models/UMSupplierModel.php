<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UMSupplierModel extends Model {
    protected $table = 'tbl_um_supplier';
    protected $primaryKey = 'IDUMSupplier';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDUMSupplier', 'IDSupplier', 'Tanggal_UM', 'Nomor_Faktur', 'Nominal',
        'Saldo_UM', 'ID_Faktur', 'Pembayaran', 'IDCOA', 'Jenis_pembayaran',
        'Nomor_Giro', 'Tanggal_giro', 'IDFB',
        'dibuat_pada', 'diubah_pada'
    ];
}
