<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalesOrderModel extends Model {
    protected $table = 'tbl_sales_order';
    protected $primaryKey = 'IDSOK';
    public $incrementing = false;
    public $timestamps = false;
 
    protected $fillable = [
        'IDSOK', 'Nomor', 'IDCustomer', 'TOP', 'Tanggal_jatuh_tempo',
        'IDMataUang', 'Kurs', 'Total_qty', 'Saldo_qty', 'No_po_customer',
        'Grand_total', 'Keterangan', 'Batal', 'Status_ppn', 'DPP', 'PPN',
        'dibuat_pada', 'diubah_pada'
    ];


}
