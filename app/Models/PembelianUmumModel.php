<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembelianUmumModel extends Model {
    protected $table = 'tbl_pembelian_umum';
    protected $primaryKey = 'IDFBUmum';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFBUmum', 'Tanggal', 'Nomor', 'IDSupplier', 'IDTBSUmum', 'Status_ppn', 'Persen_disc',
        'Disc', 'IDMataUang', 'Kurs', 'Total_qty', 'Grand_total', 'Keterangan', 'Batal', 'Jatuh_tempo',
        'Tanggal_jatuh_tempo', 'Status_pakai'
    ];


}
