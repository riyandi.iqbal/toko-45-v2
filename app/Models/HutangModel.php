<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HutangModel extends Model {
    protected $table = 'tbl_hutang';
    protected $primaryKey = 'IDHutang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'Tanggal_Hutang', 'Jatuh_Tempo', 'No_Faktur', 'IDSupplier', 'Jenis_Faktur', 'Nilai_Hutang',
        'Saldo_Awal', 'Pembayaran', 'Saldo_Akhir', 'IDHutang', 'IDFaktur', 'UM', 'DISC', 'Retur',
        'Selisih', 'Kontra_bon', 'Saldo_kontra_bon', 'Batal', 'is_paid', 'CID', 'CTime', 'MID', 'MTime', 'no_inv'
    ];


}
