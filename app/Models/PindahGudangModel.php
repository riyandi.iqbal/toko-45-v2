<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PindahGudangModel extends Model {
    protected $table = 'tbl_pindah_gudang';
    protected $primaryKey = 'IDPG';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPG', 'Tanggal', 'Nomor', 'Total', 'IDMataUang', 'Kurs', 'Keterangan', 'Jenis', 'Batal', 'IDGudangAwal', 'IDGudangTujuan',
        'dibuat_pada', 'dibuat_oleh', 'diubah_pada', 'diubah_oleh'
    ];
}
