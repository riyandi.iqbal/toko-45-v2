<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PiutangModel extends Model {
    protected $table = 'tbl_piutang';
    protected $primaryKey = 'IDPiutang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPiutang', 'IDFaktur', 'UM', 'DISC', 'Retur', 'Selisih',
        'Kontra_bon', 'Saldo_kontra_bon', 'Tanggal_Piutang', 'Jatuh_Tempo', 'No_Faktur', 'Batal',
        'IDCustomer', 'Jenis_Faktur', 'Nilai_Piutang', 'Saldo_Awal', 'Pembayaran', 'Saldo_Akhir'
    ];
}
