<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListSuratJalanModel extends Model
{
    protected $table = "vlistsuratjalan";
    protected $primaryKey = "idsjc";
    public $incrementing = false;
    public $timestamps = false;
}