<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListJurnalUmumDetailModel extends Model
{
    protected $table = "vlistjurnalumumdetail";
    protected $primaryKey = "IDJUDetail";
    public $incrementing = false;
    public $timestamps = false;
}