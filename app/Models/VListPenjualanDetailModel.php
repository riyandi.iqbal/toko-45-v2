<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPenjualanDetailModel extends Model
{
    protected $table = "vlistpenjualandetail";
    protected $primaryKey = "IDFJDetail";
    public $incrementing = false;
    public $timestamps = false;
}