<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KodeTransaksiModel extends Model {
    protected $table = 'tbl_kode_transaksi';
    protected $primaryKey = 'IDKode';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKode', 'Kode', 'IDMenuDetail', 'Jadi1', 'Jadi2', 'Jadi3', 'Jadi4', 'Pisah1', 'Pisah2', 'Pisah3', 'Pisah4'
    ];


}
