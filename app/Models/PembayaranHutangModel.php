<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembayaranHutangModel extends Model
{
    protected $table = 'tbl_pembayaran_hutang';
    protected $primaryKey = 'IDBS';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDBS', 'Tanggal', 'IDTanggal', 'Nomor', 'IDSupplier', 
        'IDMataUang', 'Kurs', 'Total', 'Uang_muka', 'Selisih', 
        'Kompensasi_um', 'Pembayaran', 'Kelebihan_bayar', 
        'Keterangan', 'Batal', 'CID', 'CTime', 'MID', 'MTime', 'Nilai_kurs'
    ];
}
