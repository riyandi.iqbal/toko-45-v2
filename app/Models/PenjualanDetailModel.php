<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanDetailModel extends Model {
    protected $table = 'tbl_penjualan_detail';
    protected $primaryKey = 'IDFJDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFJDetail', 'IDFJ', 'IDBarang', 'Qty', 'Saldo_qty', 'IDSatuan', 'Harga', 
        'Sub_total' 
    ];


}