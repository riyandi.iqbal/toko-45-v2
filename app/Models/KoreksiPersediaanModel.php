<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KoreksiPersediaanModel extends Model {
    protected $table = 'tbl_koreksi_persediaan';
    protected $primaryKey = 'IDKP';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKP', 'Tanggal', 'Nomor', 'Total', 'IDMataUang', 'Kurs', 'Keterangan', 'Jenis', 'Batal', 'IDGudang',
        'dibuat_pada', 'dibuat_oleh', 'diubah_pada', 'diubah_oleh'
    ];
}
