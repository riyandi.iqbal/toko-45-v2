<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiroModel extends Model
{
    protected $table = 'tbl_giro';
    protected $primaryKey = 'IDGiro';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGiro', 'IDFaktur', 'IDPerusahaan', 'Jenis_faktur', 'Nomor_faktur', 'IDBank',
        'Nomor_giro', 'Tanggal_faktur', 'Tanggal_cair', 'Nomor_rekening', 
        'Nilai', 'Status_giro', 'Jenis_giro', 'Tanggal_giro'
    ];
}
