<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BahanbakuDetailModel extends Model
{
    protected $table = 'tbl_produksibahanbaku_detail';
    protected $primaryKey = 'IDPBBDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPBBDetail', 'IDPBB', 'IDBarang', 'Qty', 'Satuan', 'Total'
    ];

    public function scopeIndexdata()
    {
        return DB::table('tbl_produksibahanbaku')
                ->join('tbl_produksibahanbaku_detail', 'tbl_produksibahanbaku.IDPBB', '=', 'tbl_produksibahanbaku_detail.IDPBB')
                ->join('tbl_formula', 'tbl_formula.IDFormula', '=', 'tbl_produksibahanbaku.IDFormula')
                ->select('tbl_produksibahanbaku.*',
                        'tbl_produksibahanbaku_detail.*',
                        'tbl_formula.Nama_Formula'

                );
    }

    public function scopeGetdetail()
    {
        return DB::table('tbl_produksibahanbaku')
                ->join('tbl_produksibahanbaku_detail', 'tbl_produksibahanbaku.IDPBB', '=', 'tbl_produksibahanbaku_detail.IDPBB')
                ->join('tbl_formula', 'tbl_formula.IDFormula', '=', 'tbl_produksibahanbaku.IDFormula')
                ->join('tbl_barang', 'tbl_produksibahanbaku_detail.IDBarang', '=', 'tbl_barang.IDBarang')
                ->join('tbl_satuan', 'tbl_produksibahanbaku_detail.IDSatuan', '=', 'tbl_satuan.IDSatuan')
                ->select('tbl_produksibahanbaku.*',
                            'tbl_produksibahanbaku_detail.*',  
                            'tbl_formula.Nama_Formula',
                            'tbl_barang.Nama_Barang',
                            'tbl_satuan.Satuan'                
            );
    }
}
