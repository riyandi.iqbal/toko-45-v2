<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembelianModel extends Model {
    protected $table = 'tbl_pembelian';
    protected $primaryKey = 'IDFB';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFB', 'Tanggal', 'Nomor', 'IDTBS', 'IDSupplier', 'No_sj_supplier', 'TOP', 'Tanggal_jatuh_tempo',
        'IDMataUang', 'Kurs', 'Total_qty_yard', 'Total_qty_meter', 'Saldo_yard', 'Saldo_meter', 'Discount',
        'Status_ppn', 'Keterangan', 'Batal', 'Status_pakai', 'is_paid'
    ];


}
