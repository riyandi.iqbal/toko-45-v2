<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    protected $table = 'tbl_customer';
    protected $primaryKey = 'IDCustomer';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDCustomer', 'IDGroupCustomer', 'Kode_Customer', 'Nama',
        'Alamat', 'No_Telpon', 'IDKota', 'Fax', 'Email', 'NPWP', 'No_KTP',
        'Aktif'
    ];
}
