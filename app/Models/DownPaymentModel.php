<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DownPaymentModel extends Model {
    protected $table = 'tbl_down_payment';
    protected $primaryKey = 'IDDownPayment';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDDownPayment', 'NoDP', 'NominalDP', 'Keterangan', 'Aktif', 'Ctime', 'IDSupplier', 'Pembayaran',
        'Jenis_pembayaran', 'IDCOA', 'Saldo_Akhir'
    ];


}
