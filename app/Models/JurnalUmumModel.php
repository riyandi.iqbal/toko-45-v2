<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JurnalUmumModel extends Model {
    protected $table = 'tbl_jurnal_umum';
    protected $primaryKey = 'IDJU';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDJU', 'Tanggal', 'Nomor', 'Batal', 'Total_debit', 'Total_kredit',
        'dibuat_pada', 'dibuat_oleh', 'diubah_pada', 'diubah_oleh'
    ];
}
