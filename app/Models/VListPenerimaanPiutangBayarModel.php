<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPenerimaanPiutangBayarModel extends Model
{
    protected $table = "vlistpenerimaanpiutangbayar";
    protected $primaryKey = "IDTPBayar";
    public $incrementing = false;
    public $timestamps = false;
}