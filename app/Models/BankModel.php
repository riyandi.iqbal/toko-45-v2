<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankModel extends Model
{
    protected $table = 'tbl_bank';
    protected $primaryKey = 'IDBank';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDBank', 'Nomor_Rekening', 'IDCoa', 'Atas_Nama', 'Aktif'
    ];
}
