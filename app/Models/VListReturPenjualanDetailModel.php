<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListReturPenjualanDetailModel extends Model
{
    protected $table = "vlistreturpenjualandetail";
    protected $primaryKey = "IDRPDetail";
    public $incrementing = false;
    public $timestamps = false;
}