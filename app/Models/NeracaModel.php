<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NeracaModel extends Model {
    protected $table = 'tbl_setting_neraca';
    protected $primaryKey = 'IDNeraca';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDNeraca', 'IDCoa', 'Keterangan', 'Kategori', 'Perhitungan', 'Group', 'Urutan', 'Nilai'
    ];

    public function childs() {
        return 
                $this->hasMany('App\Models\NeracaModel','Group','IDCoa')
                ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_setting_neraca.IDCoa')
                ->orderBy('tbl_setting_neraca.IDNeraca', 'asc') ;
    }
}
