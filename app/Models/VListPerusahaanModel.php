<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPerusahaanModel extends Model
{
    protected $table = "vlistperusahaan";
    protected $primaryKey = "ID";
    public $incrementing = false;
    public $timestamps = false;
}