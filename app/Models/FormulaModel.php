<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormulaModel extends Model
{
    protected $table = 'tbl_formula';
    protected $primaryKey = 'IDFormula';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFormula', 'Nama_Formula', 'Total', 'Tanggal', 'CID', 'CTime', 'MID', 'MTime'
    ];
}
