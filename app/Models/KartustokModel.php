<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KartustokModel extends Model {
    protected $table = 'tbl_kartu_stok';
    protected $primaryKey = 'IDKartuStok';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKartuStok', 'Tanggal', 'Nomor_faktur', 'IDFaktur', 'IDFakturDetail', 'IDStok', 'Jenis_faktur', 'Barcode', 'IDBarang',
        'IDCorak', 'IDWarna', 'IDGudang', 'Masuk_yard', 'Masuk_meter', 'Keluar_yard', 'Keluar_meter', 'Grade', 'IDSatuan',
        'Harga', 'IDMataUang', 'Kurs', 'Total', 'Masuk_pcs', 'Keluar_pcs', 'Nama_Barang'
    ];


}
