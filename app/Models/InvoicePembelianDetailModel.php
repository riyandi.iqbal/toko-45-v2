<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePembelianDetailModel extends Model
{
    protected $table = 'tbl_pembelian_detail';
    protected $primaryKey = 'IDFBDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFBDetail', 'IDFB', 'IDTBSDetail', 'IDBarang', 'Qty_yard', 'Saldo_yard', 'Qty_meter', 'Saldo_meter', 'IDSatuan', 'Harga', 'Sub_total', 'diskon', 'IDMataUang',
    ];

    public function scopeGetItem()
    {
        return InvoicePembelianDetailModel::leftJoin("tbl_barang", 'tbl_pembelian_detail.IDBarang', '=', 'tbl_barang.IDBarang')
            ->leftJoin("tbl_satuan", 'tbl_pembelian_detail.IDSatuan', '=', 'tbl_satuan.IDSatuan');
    }

}
