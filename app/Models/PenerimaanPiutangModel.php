<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenerimaanPiutangModel extends Model {
    protected $table = 'tbl_penerimaan_piutang';
    protected $primaryKey = 'IDTP';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTP', 'Tanggal', 'Nomor', 'IDCustomer', 'IDMataUang', 'Kurs', 'Total', 'Uang_muka',
        'Selisih', 'Kompensasi_um', 'Pembayaran', 'Kelebihan_bayar', 'Keterangan', 'Batal'
    ];
}
