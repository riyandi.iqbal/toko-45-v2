<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PindahGudangDetailModel extends Model {
    protected $table = 'tbl_pindah_gudang_detail';
    protected $primaryKey = 'IDPGDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPGDetail', 'IDPG', 'IDBarang', 'IDSatuan', 'Barcode', 'IDMataUang', 'Qty', 'Saldo', 'Harga', 'Kurs'
    ];
}
