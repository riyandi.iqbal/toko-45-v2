<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupUserModel extends Model {
    protected $table = 'tbl_group_user';
    protected $primaryKey = 'IDGroupUser';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGroupUser', 'Kode_Group_User', 'Group_User', 'Tambah', 'Edit', 'Batal'
    ];


}
