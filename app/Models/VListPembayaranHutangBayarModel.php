<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPembayaranHutangBayarModel extends Model
{
    protected $table = "vlistpembayaranhutangbayar";
    protected $primaryKey = "IDBSBayar";
    public $incrementing = false;
    public $timestamps = false;
}