<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPenjualanDetailModel extends Model {
    protected $table = 'tbl_retur_penjualan_detail';
    protected $primaryKey = 'IDRPDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDRPDetail', 'IDRP', 'IDBarang', 'Qty', 'Saldo_qty', 'IDSatuan', 'Harga', 
        'Sub_total' 
    ];


}
