<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrderModel extends Model
{
    protected $table = 'tbl_purchase_order';
    protected $primaryKey = 'IDPO';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPO', 'Tanggal', 'Nomor', 'Tanggal_selesai', 'IDSupplier', 'Jenis_Pesanan', 'IDMataUang', 'Kurs',
        'Total_qty', 'Saldo', 'Keterangan', 'Jenis_PO', 'Batal', 'Grand_total', 'Status', 'is_taken', 'jenis_ppn',
        'pilihanppn', 'approve', 'dibuat_pada', 'diubah_pada'
    ];

    public function scopeGetindex()
    {
        return DB::table('tbl_purchase_order')
                                    ->join('tbl_purchase_order_detail', 'tbl_purchase_order.IDPO', '=', 'tbl_purchase_order_detail.IDPO')
                                    ->join('tbl_supplier', 'tbl_supplier.IDSupplier','=','tbl_purchase_order.IDSupplier')
                                    ->join('tbl_mata_uang', 'tbl_mata_uang.IDMataUang', '=', 'tbl_purchase_order.IDMataUang')
                                    ->select('tbl_purchase_order.*', 'tbl_supplier.Nama', 'tbl_mata_uang.Mata_uang', 'tbl_mata_uang.Kode',  DB::raw('SUM(tbl_purchase_order_detail."Qty") as totqty'))
                                    ->groupBy('tbl_purchase_order.IDPO', 'tbl_purchase_order.Tanggal',
                                            'tbl_purchase_order.Nomor',
                                            'tbl_purchase_order.Tanggal_selesai',
                                            'tbl_purchase_order.IDSupplier',
                                            'tbl_purchase_order.Jenis_Pesanan',
                                            'tbl_purchase_order.IDMataUang',
                                            'tbl_purchase_order.Kurs',
                                            'tbl_purchase_order.Total_qty',
                                            'tbl_purchase_order.Saldo',
                                            'tbl_purchase_order.Keterangan',
                                            'tbl_purchase_order.Jenis_PO',
                                            'tbl_purchase_order.Batal',
                                            'tbl_purchase_order.Grand_total',
                                            'tbl_purchase_order.Status',
                                            'tbl_purchase_order.is_taken',
                                            'tbl_purchase_order.jenis_ppn',
                                            'tbl_purchase_order.pilihanppn',
                                            'tbl_purchase_order.approve',
                                            'tbl_purchase_order.dibuat_pada',
                                            'tbl_purchase_order.diubah_pada',
                                            'tbl_mata_uang.Mata_uang',
                                            'tbl_mata_uang.Kode',
                                            'tbl_supplier.Nama');
    }
}
