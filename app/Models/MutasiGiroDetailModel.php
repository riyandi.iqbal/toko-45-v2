<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MutasiGiroDetailModel extends Model {
    protected $table = 'tbl_mutasi_giro_detail';
    protected $primaryKey = 'IDMGDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDMGDetail', 'IDMG', 'Jenis_giro', 'IDGiro', 'Status', 'Nomor_lama', 'Nomor_baru'
    ];
}
