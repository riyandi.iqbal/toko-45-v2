<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPenerimaanPiutangModel extends Model
{
    protected $table = "vlistpenerimaanpiutang";
    protected $primaryKey = "IDTP";
    public $incrementing = false;
    public $timestamps = false;
}