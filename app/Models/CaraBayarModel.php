<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CaraBayarModel extends Model
{
    protected $table = 'tbl_cara_bayar';
    protected $primaryKey = 'IDCaraBayar';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDCaraBayar', 'Nama', 'IDCoa', 'CID', 'CTime', 'MID', 'MTime', 'PO', 'FB', 'PH',
        'SO', 'FJ', 'PP',
    ];

    public function scopeGetforindex()
    {
        return DB::table('tbl_cara_bayar')
            ->join('tbl_coa', 'tbl_coa.IDCoa', '=', 'tbl_cara_bayar.IDCoa')
            ->select('tbl_cara_bayar.*', 'tbl_coa.Nama_COA')
            ->orderBy('tbl_cara_bayar.IDCaraBayar', 'asc')->get();
    }

    public function scopeGetpo()
    {
        return DB::table('tbl_cara_bayar')
            ->where('PO', '=', 'yes')
            ->get();
    }

    public function scopeGetph()
    {
        return DB::table('tbl_cara_bayar')
            ->where('PH', '=', 'yes')
            ->get();
    }

    public function scopeGetSO()
    {
        return DB::table('tbl_cara_bayar')
            ->where('SO', '=', 'yes')
            ->get();
    }

    public function scopeGetFB()
    {
        return DB::table('tbl_cara_bayar')
            ->where('FB', '=', 'yes')
            ->get();
    }

    public function scopeGetFJ()
    {
        return DB::table('tbl_cara_bayar')
            ->where('FJ', '=', 'yes')
            ->get();
    }

    public function scopeGetPP()
    {
        return DB::table('tbl_cara_bayar')
            ->where('PP', '=', 'yes')
            ->get();
    }

}
