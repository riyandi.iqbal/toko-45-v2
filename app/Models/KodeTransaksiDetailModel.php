<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KodeTransaksiDetailModel extends Model {
    protected $table = 'tbl_kode_transaksi_detail';
    protected $primaryKey = 'IDKodeDetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKode', 'IDKodeDetail', 'Asal1', 'Tampil1', 'Manual1', 'Pemisah1', 'Format1', 'Jenis1',
        'Asal2', 'Tampil2', 'Manual2', 'Pemisah2', 'Format2', 'Jenis2',
        'Asal3', 'Tampil3', 'Manual3', 'Pemisah3', 'Format3', 'Jenis3',
        'Asal4', 'Tampil4', 'Manual4', 'Pemisah4', 'Format4', 'Jenis4'
    ];


}
