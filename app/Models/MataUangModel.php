<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MataUangModel extends Model {
    protected $table = 'tbl_mata_uang';
    protected $primaryKey = 'IDMataUang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDMataUang', 'Kode', 'Mata_uang', 'Negara', 'Default', 'Kurs', 'Aktif', 'Tanggal'
    ];
}
