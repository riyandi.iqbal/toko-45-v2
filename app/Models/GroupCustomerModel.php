<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupCustomerModel extends Model
{
    protected $table = 'tbl_group_customer';
    protected $primaryKey = 'IDGroupCustomer';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGroupCustomer', 'Kode_Group_Customer', 'Nama_Group_Customer', 'Aktif'
    ];
}
