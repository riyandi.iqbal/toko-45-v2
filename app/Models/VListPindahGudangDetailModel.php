<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPindahGudangDetailModel extends Model
{
    protected $table = "vlistpindahgudangdetail";
    protected $primaryKey = "IDPGDetail";
    public $incrementing = false;
    public $timestamps = false;
}