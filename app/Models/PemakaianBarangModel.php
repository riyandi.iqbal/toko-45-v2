<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PemakaianBarangModel extends Model {
    protected $table = 'tbl_pemakaian_barang';
    protected $primaryKey = 'IDPakai';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPakai', 'Keterangan', 'Diambil_oleh', 'CID', 'CTime', 'MID', 'MTime', 'Tanggal'
    ];

    public function scopeGetforindex()
    {
        return DB::table('tbl_pemakaian_barang')->join('users', 'users.id', '=', 'tbl_pemakaian_barang.CID')->where('Delete', '=', 'f');
    }

    public function scopeGetforshow()
    {
        return DB::table('tbl_pemakaian_barang')
                        ->join('tbl_pemakaian_barang_detail', 'tbl_pemakaian_barang.IDPakai', '=', 'tbl_pemakaian_barang_detail.IDPakai')
                        ->join('tbl_barang', 'tbl_barang.IDBarang', '=', 'tbl_pemakaian_barang_detail.IDBarang')
                        ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_pemakaian_barang_detail.IDSatuan')
                        ;
    }

    public function scopeGetforshowheader()
    {
        return DB::table('tbl_pemakaian_barang')
                    ->join('users', 'users.id', '=', 'tbl_pemakaian_barang.CID');
    }
}
