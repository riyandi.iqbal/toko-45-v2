<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPembayaranHutangModel extends Model
{
    protected $table = "vlistpembayaranhutang";
    protected $primaryKey = "IDBS";
    public $incrementing = false;
    public $timestamps = false;
}