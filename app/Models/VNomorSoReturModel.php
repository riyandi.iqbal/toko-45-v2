<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VNomorSoReturModel extends Model
{
    protected $table = "vnomorsoretur";
    protected $primaryKey = "ID";
    public $incrementing = false;
    public $timestamps = false;
}