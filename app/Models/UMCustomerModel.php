<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UMCustomerModel extends Model {
    protected $table = 'tbl_um_customer';
    protected $primaryKey = 'IDUMCustomer';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDUMCustomer', 'IDCustomer', 'Tanggal_UM', 'Nomor_Faktur', 'Nilai_UM',
        'Saldo_UM', 'ID_Faktur', 'IDCoa', 'Jenis_Pembayaran',
        'Nomor_giro', 'Tanggal_giro', 'IDFJ',
        'dibuat_pada', 'diubah_pada'
    ];
}
