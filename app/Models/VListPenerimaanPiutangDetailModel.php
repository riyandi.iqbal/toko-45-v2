<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPenerimaanPiutangDetailModel extends Model
{
    protected $table = "vlistpenerimaanpiutangdetail";
    protected $primaryKey = "IDTPDetail";
    public $incrementing = false;
    public $timestamps = false;
}