<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KotaModel extends Model
{
    protected $table = 'tbl_kota';
    protected $primaryKey = 'IDKota';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDKota', 'Kode_Kota', 'Provinsi', 'Kota', 'Aktif'
    ];
}
