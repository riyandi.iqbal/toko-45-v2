<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class PerusahaanModel extends Model
{
    protected $table = 'tbl_perusahaan';
    protected $primaryKey = 'IDPerusahaan';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPerusahaan', 'Nama', 'Alamat', 'Kecamatan', 'Kota', 'Telp', 'Fax', 'Logo_head', 
        'Kontak', 'Email', 'MID', 'MTime', 'Logo_text', 'Font', 'FontSize', 'FontStyle', 'Angkakoma'
    ];

    public function scopeGetforprint()
    {
        return DB::table('tbl_perusahaan')
                    ->join('tbl_kota', 'tbl_kota.IDKota', '=', 'tbl_perusahaan.Kota')
                    ->select('tbl_perusahaan.*', 'tbl_kota.Kota as Nama_Kota', 'tbl_kota.Provinsi')
                    ->first();
    }

   
}
