<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class InvoicePembelianModel extends Model
{
    protected $table = 'tbl_pembelian';
    protected $primaryKey = 'IDFB';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFB', 'Tanggal', 'Nomor', 'IDTBS', 'IDSupplier', 'No_sj_supplier', 'TOP',
        'Tanggal_jatuh_tempo', 'IDMataUang', 'Kurs', 'Total_qty_yard', 'Total_qty_meter',
        'Saldo_yard', 'Saldo_meter', 'Discount', 'Status_ppn', 'Keterangan', 'Batal',
        'Status', 'Status_pakai', 'is_paid', 'no_pajak', 'jenis_ppn',
    ];

    public function scopeGetindex()
    {
        return DB::table('tbl_pembelian')
            ->join("tbl_supplier", "tbl_pembelian.IDSupplier", "=", "tbl_supplier.IDSupplier")
            ->join("tbl_pembelian_detail", "tbl_pembelian_detail.IDFB", "=", "tbl_pembelian.IDFB")
            ->join("tbl_mata_uang", "tbl_mata_uang.IDMataUang", "=", "tbl_pembelian.IDMataUang")
            ->leftjoin("tbl_terima_barang_supplier", "tbl_terima_barang_supplier.IDTBS", "=", "tbl_pembelian.IDTBS")
        // ->select("tbl_pembelian.*", "tbl_supplier.Nama")
            ->select("tbl_pembelian.*", "tbl_supplier.Nama", "tbl_pembelian_detail.IDMataUang as IDMT", DB::raw('SUM(tbl_pembelian_detail."Sub_total") AS total_harga'), "tbl_mata_uang.Mata_uang", "tbl_mata_uang.Kode", "tbl_terima_barang_supplier.Nomor as Nomor_tbs")
            ->groupBy("tbl_pembelian.IDFB", "tbl_pembelian.Tanggal", "tbl_pembelian.Nomor", "tbl_pembelian.IDTBS", "tbl_pembelian.IDSupplier", "tbl_pembelian.No_sj_supplier",
                "tbl_pembelian.Batal", "tbl_pembelian.TOP", "tbl_pembelian.Discount", "tbl_pembelian.Tanggal_jatuh_tempo", "tbl_pembelian.IDMataUang", "tbl_pembelian.Kurs",
                "tbl_pembelian.Total_qty_yard", "tbl_pembelian.Total_qty_meter", "tbl_pembelian.Saldo_yard", "tbl_pembelian.Saldo_meter", "tbl_pembelian.Status_ppn",
                "tbl_pembelian.Keterangan", "tbl_pembelian.Status", "tbl_pembelian.no_pajak", "tbl_pembelian.Status_pakai", "tbl_pembelian.is_paid", "tbl_pembelian.jenis_ppn", "tbl_supplier.Nama", "tbl_pembelian_detail.IDMataUang", "tbl_mata_uang.Mata_uang", "tbl_mata_uang.Kode", "tbl_terima_barang_supplier.Nomor");

    }
}
