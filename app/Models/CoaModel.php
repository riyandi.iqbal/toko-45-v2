<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CoaModel extends Model
{
    protected $table = 'tbl_coa';
    protected $primaryKey = 'IDCoa';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDCoa', 'Kode_COA', 'Nama_COA', 'Status', 'Aktif', 
        'IDGroupCOA', 'Modal', 'Saldo_Awal', 'Normal_Balance'
    ];
}
