<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListReturPenjualanModel extends Model
{
    protected $table = "vlistreturpenjualan";
    protected $primaryKey = "IDRP";
    public $incrementing = false;
    public $timestamps = false;
}