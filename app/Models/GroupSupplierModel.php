<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupSupplierModel extends Model
{
    protected $table = 'tbl_group_supplier';
    protected $primaryKey = 'IDGroupSupplier';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGroupSupplier', 'Kode_Group_Supplier', 'Group_Supplier', 'Aktif'
    ];
}
