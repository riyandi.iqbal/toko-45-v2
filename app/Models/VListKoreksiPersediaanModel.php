<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListKoreksiPersediaanModel extends Model
{
    protected $table = "vlistkoreksipersediaan";
    protected $primaryKey = "IDKP";
    public $incrementing = false;
    public $timestamps = false;
}