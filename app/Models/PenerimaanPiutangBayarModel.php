<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenerimaanPiutangBayarModel extends Model {
    protected $table = 'tbl_penerimaan_piutang_bayar';
    protected $primaryKey = 'IDTPBayar';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDTPBayar', 'IDTP', 'Jenis_pembayaran', 'Jenis_pembayaran', 'IDCOA', 
        'Nomor_giro', 'Nominal_pembayaran', 'IDMataUang', 'Kurs', 'Status_giro', 'Tanggal_giro'
    ];
}
