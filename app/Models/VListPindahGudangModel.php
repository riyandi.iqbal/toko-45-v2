<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPindahGudangModel extends Model
{
    protected $table = "vlistpindahgudang";
    protected $primaryKey = "IDPG";
    public $incrementing = false;
    public $timestamps = false;
}