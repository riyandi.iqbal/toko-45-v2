<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuratJalanModel extends Model {
    protected $table = 'tbl_surat_jalan_customer';
    protected $primaryKey = 'idsjc';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'idsjc', 
        'nomor', 
        'tanggal', 
        'idcustomer', 
        'idso', 
        'nomorso', 
        'asalfaktur',
        'nomorcustomer', 
        'idgudang', 
        'idmatauang', 
        'kurs', 
        'qty',
        'saldoqty', 
        'top', 
        'jatuhtempo', 
        'disc', 
        'persendisc', 
        'ppn',
        'statusppn', 
        'grantotal',
        'sisa', 
        'saldosisa', 
        'kelebihan', 
        'keterangan', 
        'jenisso', 
        'dpp', 
        'nilaippn', 
        'iduser', 
        'created_at', 
        'updated_at',
        'confirm'
    ];


}
