<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetailModel extends Model
{
    protected $table = 'tbl_purchase_order_detail';
    protected $primaryKey = 'IDPODetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPODetail', 'IDPO', 'IDBarang', 'Qty', 'Saldo', 
        'IDSatuan', 'Harga', 'Qty_terima', 'DPP', 'PPN', 'Jenis_ppn'
    ];
}
