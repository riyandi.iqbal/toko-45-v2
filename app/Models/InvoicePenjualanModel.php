<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoicePenjualanModel extends Model {
    protected $table = 'tbl_penjualan';
    protected $primaryKey = 'IDFJ';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFJ', 'Tanggal', 'Nomor', 'IDCustomer', 'Nama_di_faktur', 'IDSJC', 'TOP',
        'Tanggal_jatuh_tempo', 'IDMataUang', 'Kurs', 'Total_qty', 'Discount', 'Status_ppn', 'Keterangan',
        'Batal', 'Grand_total', 'Status_pakai', 'DPP', 'PPN', 'dibuat_pada', 'diubah_pada'
    ];


}
