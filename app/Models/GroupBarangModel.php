<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupBarangModel extends Model
{
    protected $table = 'tbl_group_barang';
    protected $primaryKey = 'IDGroupBarang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGroupBarang', 'Kode_Group_Barang', 'Group_Barang', 'Aktif', 'Panjang_Kode', 'Cetak_Kode',
        'Pemisah_Kode', 'Panjang_Nama', 'Cetak_Nama', 'Pemisah_Nama'
    ];
}
