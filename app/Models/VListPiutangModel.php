<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPiutangModel extends Model
{
    protected $table = "vlistpiutang";
    protected $primaryKey = "IDPiutang";
    public $incrementing = false;
    public $timestamps = false;
}