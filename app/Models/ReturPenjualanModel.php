<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReturPenjualanModel extends Model {
    protected $table = 'tbl_retur_penjualan';
    protected $primaryKey = 'IDRP';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDRP', 'Nomor', 'Tanggal', 'IDCustomer', 'IDFJ',
        'Tanggal_fj', 'Total_qty', 'Saldo_qty',
        'Discount', 'Status_ppn', 'Keterangan', 'Batal', 'Grand_total', 'Status_pakai', 'Pilihan_retur',
        'dibuat_pada', 'diubah_pada'
    ];


}
