<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostingModel extends Model
{
    protected $table = 'tbl_posting';
    protected $primaryKey = 'IDPosting';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPosting', 'Bulan', 'Tahun', 'Status', 'CID', 'CTime', 'MID', 'MTime'
    ];
}
