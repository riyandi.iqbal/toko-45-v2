<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SUratJalanDetailModel extends Model {
    protected $table = 'tbl_surat_jalan_customer_detail';
    protected $primaryKey = 'idsjcdetail';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'idsjcdetail', 
        'idsjc', 
        'idbarang', 
        'qty', 
        'saldo', 
        'hargasatuan', 
        'idsatuan', 
        'diskon2',
        'idmatauang', 
        'kurs', 
        'expiredate', 
        'dpp', 
        'statusppn', 
        'nilaippn', 
        'subtotal', 
        'idso',
        'idsodetail', 
        'asalfaktur', 
        'idgudang', 
        'nomorsj', 
        'nomorso', 
        'created_at', 
        'updated_at'
    ];


}
