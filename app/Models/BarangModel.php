<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BarangModel extends Model
{
    protected $table = 'tbl_barang';
    protected $primaryKey = 'IDBarang';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDBarang', 'IDGroupBarang', 'Kode_Barang', 'Nama_Barang', 'IDSatuan',
        'Aktif', 'IDKategori', 'IDUkuran', 'IDTipe', 'Berat', 'StokMinimal'
    ];

    public function scopeGetdetail()
    {
        return DB::table('tbl_barang')
            ->join('tbl_ukuran', 'tbl_ukuran.IDUkuran', '=', 'tbl_barang.IDUkuran')
            ->join('tbl_tipe_barang', 'tbl_tipe_barang.IDTipe', '=', 'tbl_barang.IDTipe');
    }

    public function scopeGetforindex()
    {
        return DB::table('tbl_barang')
                    ->join('tbl_group_barang', 'tbl_group_barang.IDGroupBarang', '=', 'tbl_barang.IDGroupBarang')
                    ->join('tbl_kategori', 'tbl_kategori.IDKategori', '=', 'tbl_barang.IDKategori')
                    ->join('tbl_ukuran', 'tbl_ukuran.IDUkuran', '=', 'tbl_barang.IDUkuran')
                    ->join('tbl_satuan', 'tbl_satuan.IDSatuan', '=', 'tbl_barang.IDSatuan')
                    ->select('tbl_group_barang.Group_Barang', 
                            'tbl_barang.Kode_Barang',
                            'tbl_barang.Nama_Barang',
                            'tbl_barang.Aktif',
                            'tbl_barang.IDBarang',
                            'tbl_barang.StokMinimal',
                            'tbl_kategori.Nama_Kategori',
                            'tbl_ukuran.Nama_Ukuran',
                            'tbl_satuan.Satuan',
                            'tbl_barang.IDSatuan'   
                        );
    }
}