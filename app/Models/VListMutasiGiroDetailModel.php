<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListMutasiGiroDetailModel extends Model
{
    protected $table = "vlistmutasigirodetail";
    protected $primaryKey = "IDMGDetail";
    public $incrementing = false;
    public $timestamps = false;
}