<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PenjualanPembayaranModel extends Model {
    protected $table = 'tbl_penjualan_pembayaran';
    protected $primaryKey = 'IDFJPembayaran';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDFJPembayaran', 'IDFJ', 'Jenis_pembayaran', 'IDCOA', 'NominalPembayaran', 'IDMataUang',
        'Kurs', 'Tanggal_giro', 'Batal', 'Nomor_giro',
        'dibuat_pada', 'diubah_pada'
    ];


}
