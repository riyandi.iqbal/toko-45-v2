<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BahanbakuModel extends Model
{
    protected $table = 'tbl_produksibahanbaku';
    protected $primaryKey = 'IDPBB';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPBB', 'Nomor', 'Tanggal', 'IDGudang', 'Keterangan', 'CID', 'CTime', 'MID', 'MTime', 'IDFormula', 'Qty_Header'
    ];

    public function scopeGetheader()
    {
        return DB::table('tbl_produksibahanbaku')
                ->join('tbl_produksibahanbaku_detail', 'tbl_produksibahanbaku.IDPBB', '=', 'tbl_produksibahanbaku_detail.IDPBB')
                ->join('tbl_formula', 'tbl_formula.IDFormula', '=', 'tbl_produksibahanbaku.IDFormula');
    }

    public function scopeGetBahanBaku()
    {
        return DB::table('tbl_produksibahanbaku')
                ->join('tbl_formula', 'tbl_formula.IDFormula', '=', 'tbl_produksibahanbaku.IDFormula')
                ->select('tbl_produksibahanbaku.*', 'tbl_formula.Nama_Formula');
    }
}
