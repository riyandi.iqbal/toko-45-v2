<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupCoaModel extends Model
{
    protected $table = 'tbl_group_coa';
    protected $primaryKey = 'IDGroupCOA';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDGroupCOA', 'Nama_Group', 'Normal_Balance', 'Kode_Group_COA', 'Aktif'
    ];
}
