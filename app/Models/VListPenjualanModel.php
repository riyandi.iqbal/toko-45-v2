<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VListPenjualanModel extends Model
{
    protected $table = "vlistpenjualan";
    protected $primaryKey = "IDFJ";
    public $incrementing = false;
    public $timestamps = false;
}