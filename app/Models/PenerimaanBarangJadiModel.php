<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PenerimaanBarangJadiModel extends Model {
    protected $table = 'tbl_penerimaan_barang_jadi';
    protected $primaryKey = 'IDPBJ';
    public $incrementing = false;
    public $timestamps = false;

    protected $fillable = [
        'IDPBJ', 'Tanggal', 'Nomor', 'Keterangan', 'IDGudang', 'CID', 'CTime', 'MID', 'MTime', 'Status'
    ];


}

