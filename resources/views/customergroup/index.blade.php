<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url ('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="#">Data Group Customer</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="form_grid_3">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url ('GroupCustomer/create')}}"><span style="color: white;">Tambah Data</span></a>
                </div>
            </div>
            <br>
            <div class="col-md-4">
              <label class="field_title mt-dot2">.</label>
              <select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                <option value=""></option>
                <option value="Kode_Group_Customer">Kode Group Customer</option>
                <option value="Nama_Group_Customer">Group Customer</option>
              </select>
            </div>
            <div class="col-md-4">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
      <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
          <th>No</th>
          <th>Kode Group Customer</th>
          <th>Group Customer</th>
          <th>Status</th>
          <th>Aksi</th>
        </thead>
      </table>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/customergroup/index.js') }}"></script>
<script>
    var urlData = '{{ route("GroupCustomer.datatable") }}';
    var url                 = '{{ url("GroupCustomer") }}';
</script>
@endsection 