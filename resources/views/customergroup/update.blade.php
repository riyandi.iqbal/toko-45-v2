<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('GroupCustomer')}}">Data Group Customer</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Group Customer</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Edit Data Group Customer</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="judul">Kode Group Customer</label>
          </div>
          <div class="col-md-9">
            <input type="hidden" name="id" id="id" value="<?php echo $groupcustomer->IDGroupCustomer?>">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="kode" id="kode" required oninvalid="this.setCustomValidity('Kode Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $groupcustomer->Kode_Group_Customer?>">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Nama Group Customer</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="nama" id="nama" required oninvalid="this.setCustomValidity('Nama Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $groupcustomer->Nama_Group_Customer ?>">
            <br><br>
          </div>
        </div>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a href="{{url('GroupCustomer')}}" style="color: white;">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
          </div>
        </div>
      </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("GroupCustomer") }}';
  var urlUpdate           = '{{ route("GroupCustomer.update_data") }}';
  var url                 = '{{ url("GroupCustomer") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/customergroup/update.js') }}"></script>
@endsection 