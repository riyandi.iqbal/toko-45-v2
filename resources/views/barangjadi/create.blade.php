<!-- ===========Create By Dedy 06-01-2020=============== -->
@extends('layouts.app')   
@section('content')

<?php
if ($kode->curr_number == null) {
  $number = 1;
} else {
  $number = $kode->curr_number + 1;
}
$kodemax = str_pad($number, 4, "0", STR_PAD_LEFT);
$array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
$code = $kodemax.'/'.'PBJ'.'/'.$array_bulan[date('n')].'/'.date('y');
?>
<style type="text/css">
	body {
	background: #f1f1f1;
}



@media screen and (max-width:768px) {
	.table-responsive {
		overflow: auto;
	}
}
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{url('Po')}}">Data Penerimaan Barang Jadi</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Penerimaan Barang Jadi</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Penerimaan Barang Jadi</span>
    </div>
	<div class="banner">
		<form class="form-horizontal" action="{{url('BarangJadi/simpandata')}}" method="post" >
			{{ csrf_field() }}
			<table class="table table-responsive">
			<tr>
				<td>
					<label>Tanggal</label>
					<input type="date" class="form-control input-date-padding date-picker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
					<br>
					<label>Nomor Penerimaan Barang Jadi</label>
					<input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No PO" readonly value="<?php echo $code ?>">
					<br>
					<label>Gudang</label>
					<select name="gudang" id="gudang" class="form-control select2" style="width: 100%;" readonly>
						<option value="P000002" selected>Gudang Barang Jadi</option>
					</select>
                </td>
				<td>
					<label>Keterangan</label>
					<textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')"></textarea>
				</td>
			</tr>
		</table>
		<!--<br>-->
		<div class="container" style="width: 100%">
			<div id="content">
		        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
		            <li class="active"><a href="#tab-1" data-toggle="tab">Input Data Penerimaan Barang Jadi</a></li>
		            <!-- <li><a href="#tab-2" data-toggle="tab">Pembayaran</a></li> -->
		        </ul>
		        <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active" id="tab-1">
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
		            	</div> 
		            	<div class="container" style="width: 100%">
		            		<br>
		                    	<div class="autocomplete col-md-12">
								    <input id="cariwarna" type="hidden" class="form-control" name="cariwarna" placeholder="Cari Warna" onchange="return autofill();">
								    <input type="hidden" name="qty" id="qty" value="0">
								    <input type="hidden" name="harga" id="harga" value="0">
								</div>
								<table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;">
		                        
			                        <thead style="background-color: #16305d; color: white">
			                          <tr>
			                            <th style="width: 25%">Kode Barang</th>
			                            <th style="width: 10%">Type</th>
			                            <th style="width: 15%">Ukuran</th>
			                            <th style="width: 10%">Berat</th>
			                            <th style="width: 10%">Qty</th>
			                            <th style="width: 10%">Satuan</th>
                                        <th style="width: 10%">Total</th>
			                            <th style="width: 2%">Action</th>
			                          </tr>
			                        </thead>
			                        <tbody id="tabel-cart-po">

			                        </tbody>
			                    </table>
			                    <a class="btn col-11" id="buttonadd" onclick="addmore()" /><i class="col-11 hvr-icon-float-away">Tambah</i></a>
		                    <br> 
		            	</div>
		            </div>
		            <div class="tab-pane" id="tab-2">
		            	<br>
		            	<br>
		            	<br>
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Pembayaran</span>	
		            	</div> 
		            	<div class="container" style="width: 90%">
		            		<br>
		            		<div class="col-md-6">
		            			<label>Jenis Pembayaran</label>
		            			<select name="jenis" id="jenis" class="form-control" onchange="jenis_pembayaran()">
		            				<option value=""><-- Pilih Jenis Pembayaran --></option>
                                    <option value="transfer">Transfer</option>
                                    <option value="giro">Giro</option>
                                    <option value="cash">Cash</option>
                                </select>
		            		</div>
		            		<div class="col-md-6">
		            			<label>Nama COA</label>
		            			<select name="namacoa" id="namacoa" class="form-control">
                                    <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                </select>
		            		</div>
		            		<div class="col-md-6">
		            			<label>No Giro</label>
		            			<input type="text" class="form-control" name="no_giro" id="no_giro" placeholder="Nomor Giro">
		            		</div>
		            		<div class="col-md-6">
		            			<label>Jumlah Pembayaran</label>
		            			<input type="text" class="input-date-padding-3-2 form-control harga-123" name="pembayaran" id="pembayaran" >
		            		</div>
		            	</div>
		            	<br>
		                <br>
		                	<!-- <button onclick="val_submit()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button> -->
		                <br>
		            </div>
		    	</div>
				<button onclick="val_submit()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button>
			</div>
		</div>
		</form>
	</div>
	</div>
    <br><br><br>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script type="text/javascript">
	$(document).ready(function() {

			$('#Baranga').editableSelect({
				effects: 'fade'
			});

			addmore();
        
        var rupiah = document.getElementById('pembayaran');
		rupiah.addEventListener('keyup', function(e){
			rupiah.value = formatRupiah(this.value);
		});

        
	})
    function autofill(){
        console.log('proses input ke tabel');
        var field = initialization();
	    PM.push(field);
	    setTimeout(function() {
	        renderJSON(PM);
	        field = null;
	    }, 500);	                 
    }

    //=================================ADDMORE ROW TABLE
    
    var i=1;

    function addmore()
	{
		i = i + 1;
		var htm_option_barang = "";
		var get_data_barang = JSON.parse(`<?php echo json_encode($barang); ?>`);

		var htm_option_satuan = "";
		var get_data_satuan = JSON.parse(`<?php echo json_encode($satuan); ?>`);


		for(var a in get_data_barang){
			var row_barang = get_data_barang[a];
			htm_option_barang+="<option value="+row_barang['IDBarang']+">"+row_barang['Nama_Barang']+"</option>";
		}
		
		for(var b in get_data_satuan){
			var row_satuan = get_data_satuan[b];
			htm_option_satuan += "<option value="+row_satuan['IDSatuan']+">"+row_satuan['Satuan']+"</option>";
			
		}		
		

		$('#tabelfsdfsf').append('<tr id="rowvar'+i+'" style="width:100%">'

			+'<td style="width:25%"><select  placeholder="Cari Barang" class="cari form-control" name="barang_array[]" id="barang_array'+i+'" onchange="gantisatuan(this.value, i); ambildetail(this.value, i)" style="width:100%;"><option value="" label="Pilih Barang"></option>'+htm_option_barang+'</select><input type="hidden" name="kode_barang_array[]" class="form-control"></td>'

			+'<td style="width:10%"><input type="tex" id="tipe_array'+i+'" name="tipe_array[]" readonly class="form-control"><input type="hidden" id="kode_tipe_array'+i+'" name="kode_tipe_array[]" readonly class="form-control"></td>'

			+'<td style="width:15%"><input type="tex" name="ukuran_array[]" id="ukuran_array'+i+'" readonly class="form-control"><input type="hidden" id="kode_ukuran_array'+i+'" name="kode_ukuran_array[]" readonly class="form-control"></td>'

			+'<td style="width:15%"><input type="text" id="berat_array'+i+'" name="berat_array[]" class="form-control"></td>'

			+'<td style="width:5%"><input type="tex" name="qty_array[]" id="qty_array'+i+'" onkeyup="hitungtotal(this.value, i)" class="form-control"></td>'

            +'<td style="width:5%"><input type="tex" name="satuan_array[]" id="satuan_array'+i+'" class="form-control"><input type="hidden" id="kode_satuan_array'+i+'" name="kode_satuan_array[]" readonly class="form-control"></td>'

			+'<td style="width:15%"><input type="tex" id="total_array'+i+'" name="total_array" class="form-control" readonly></td><td style="width:2%"><a onclick="deleteitem('+i+')"><i class="fa fa-times-circle fa-lg" style="color:red; padding: 10px;"></i></a></td>'

			+'</tr>');

		$('.cari').select2(); 
       	
	}

	

	function deleteitem(i)
	{
		var result = confirm("Apakah anda yakin?");
		if (result) {
			
			$('#rowvar'+i).remove();
		}

	}

	

	function gantisatuan(nilai, urutan)
	{
		$("#modal-loading").fadeIn();
		console.log('IDBARANG');
		console.log($('#barang_array'+urutan).val());

		var idbrg 	= $('#barang_array'+urutan).val();

		$.ajax({
				url : "{{url('Po/getsatuan')}}",
                  type: "GET",
                  data:{"_token": "{{ csrf_token() }}",data:idbrg},
                  dataType:'json',
                  success: function(data)
                  { 
					$("#modal-loading").fadeOut();
                    console.log(data[0].Satuan);
                    $('#satuan_array'+urutan).val(data[0].Satuan);
                    $('#kode_satuan_array'+urutan).val(data[0].IDSatuan);
                  }
		});
	}

	function ambildetail(nilai, urutan)
	{
		console.log('IDBARANG');
		console.log($('#barang_array'+urutan).val());

		var idbrg 	= $('#barang_array'+urutan).val();
		$.ajax({
				url : "{{url('PenerimaanBarangJadi/ambildetail')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",data:idbrg},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log(data.Nama_Tipe);
                    $('#tipe_array'+urutan).val(data.Nama_Tipe);
					$('#kode_tipe_array'+urutan).val(data.IDTipe)
                    $('#ukuran_array'+urutan).val(data.Nama_Ukuran);
					$('#kode_ukuran_array'+urutan).val(data.IDUkuran);
					$('#kode_barang_array'+urutan).val(data.IDBarang);
                  }
		});
	}

	function val_submit(){
		
		var ket = document.getElementById("Keterangan").value;
		
		if (ket !="") {
			$("#modal-loading").fadeIn();
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}

	var val_qty;
	var array_qty = new Array(100);
	for (var q = 0; q < array_qty.length; q++) {
		array_qty[q] = new Array(100);
	}

	function hitungtotal(nilai, urutan)
	{
		$('#qty_array'+urutan).val(nilai.replace(/[^\d]/,''));

		total_qty = 0;
		array_qty[urutan][0]=urutan;
		array_qty[urutan][1]=parseInt(nilai);

		for (var y = 0; y < array_qty.length; y++) {
			if(array_qty[y][1]!=null){
				total_qty = parseInt(total_qty) + parseInt(array_qty[y][1]);
			}
		}

		var harga = $('#berat_array'+urutan).val().replace(/[^\d]/,'');
		console.log('CEK BERAT');
		console.log(harga);

		var qty = $('#qty_array'+urutan).val();
		console.log('CEK QTY');
		console.log(qty);

		if(harga==''){
			console.log('GAK DIHITUNG');
		}else{
			console.log('HITUNG2 PPN');
			var a = 0;
			a = harga*qty;
			console.log(a);
			$('#total_array'+urutan).val(a);
		}

		// 	tot2 = 0;
		// 	array_harga2[urutan][0]=urutan;
		// 	array_harga2[urutan][1]=parseInt(a);

		// 	for (var z = 0; z < array_harga2.length; z++){
		// 		if(array_harga2[z][1]!=null){
		// 			tot2 = parseInt(tot2) + parseInt(array_harga2[z][1]);
		// 		}
		// 	}
		// $('#grandtotal').val(rupiah(tot2));
		// hitung2(harga, urutan);
	}
	
	function number_format(n, c, d, t) {
		var c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "," : d,
		t = t == undefined ? "." : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;

		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
	
</script>
@endsection