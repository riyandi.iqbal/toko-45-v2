<!-- /*=====Create TIAR 2020-06-12====*/ -->
@extends('layouts.app')   
@section('content')
<?php $currentMonth = date('m') - 1; ?>
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Posting')}}">Data Posting</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Posting</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Tambah Data Posting</span>
    </div>
    <div class="bannerbody">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="Bulan">Bulan</label>
          </div>
          <div class="col-md-9">
            <select name="Bulan" id="Bulan" class="form-control select2" style="width: 70%; margin-right:5px" required>
                <option value="">Pilih Bulan</option>
                <option value="1" {{ $currentMonth == '1' ? 'selected' : ''  }} >Januari</option>
                <option value="2" {{ $currentMonth == '2' ? 'selected' : ''  }} >Februari</option>
                <option value="3" {{ $currentMonth == '3' ? 'selected' : ''  }} >Maret</option>
                <option value="4" {{ $currentMonth == '4' ? 'selected' : ''  }} >April</option>
                <option value="5" {{ $currentMonth == '5' ? 'selected' : ''  }} >Mei</option>
                <option value="6" {{ $currentMonth == '6' ? 'selected' : ''  }} >Juni</option>
                <option value="7" {{ $currentMonth == '7' ? 'selected' : ''  }} >Juli</option>
                <option value="8" {{ $currentMonth == '8' ? 'selected' : ''  }} >Agustus</option>
                <option value="9" {{ $currentMonth == '9' ? 'selected' : ''  }} >September</option>
                <option value="10" {{ $currentMonth == '10' ? 'selected' : ''  }} >Oktober</option>
                <option value="11" {{ $currentMonth == '11' ? 'selected' : ''  }} >November</option>
                <option value="12" {{ $currentMonth == '12' ? 'selected' : ''  }} >Desember</option>
            </select>
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="Tahun">Tahun</label>
          </div>
          <div class="col-md-9">
            <input type="number" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Tahun" id="Tahun" required oninvalid="this.setCustomValidity('Nama Posting Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width: 70%;" value="{{ date('Y') }}">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="Saldo_laba_rugi">Nilai Laba Rugi</label>
          </div>
          <div class="col-md-9">
            <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Saldo_laba_rugi" id="Saldo_laba_rugi" required readonly style="width: 70%;">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="Saldo_neraca">Nilai Neraca</label>
          </div>
          <div class="col-md-9">
            <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Saldo_neraca" id="Saldo_neraca" required readonly style="width: 70%;">
            <br><br>
          </div>
        </div>
        <br><br><br>
        <div class="banner">
          <div class="row">
              <div class="col-lg-12">
                  <table class="table table-bordered table-responsive" id="table-detail" style="font-size: 12px; margin-top: 10px; width: 100%;">
                      <thead style="background-color: #16305d; color: white">
                          <tr>
                              <th>No</th>
                              <th>Charts of Account</th>
                              <th style="width: 15%">Debit</th>
                              <th style="width: 15%">Kredit</th>
                          </tr>
                      </thead>
                      <tbody id="list-saldo">

                      </tbody>
                  </table>
              </div>
          </div>
        </div>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('Posting')}}" name="simpan">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Posting") }}';
  var urlInsert           = '{{ route("Posting.store") }}';
  var urlSaldoAkhir           = '{{ route("Posting.get_saldo_akhir") }}';
  var url                 = '{{ url("Posting") }}';
  var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/posting/create.js') }}"></script>
@endsection