<!-- 
    ===========Create By TIAR 04-06-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanLabaRugi')}}">Laporan Laba / Rugi</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan Laba / Rugi</h2>
    <br>
    <?php 
    $currentYear = date('Y');
    $currentMonth = date('m');
    ?>
        <br>
        <select name="bulan" id="bulan" class="form-control select2" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" {{ $currentMonth == '1' ? 'selected' : ''  }} >Januari</option>
            <option value="2" {{ $currentMonth == '2' ? 'selected' : ''  }} >Februari</option>
            <option value="3" {{ $currentMonth == '3' ? 'selected' : ''  }} >Maret</option>
            <option value="4" {{ $currentMonth == '4' ? 'selected' : ''  }} >April</option>
            <option value="5" {{ $currentMonth == '5' ? 'selected' : ''  }} >Mei</option>
            <option value="6" {{ $currentMonth == '6' ? 'selected' : ''  }} >Juni</option>
            <option value="7" {{ $currentMonth == '7' ? 'selected' : ''  }} >Juli</option>
            <option value="8" {{ $currentMonth == '8' ? 'selected' : ''  }} >Agustus</option>
            <option value="9" {{ $currentMonth == '9' ? 'selected' : ''  }} >September</option>
            <option value="10" {{ $currentMonth == '10' ? 'selected' : ''  }} >Oktober</option>
            <option value="11" {{ $currentMonth == '11' ? 'selected' : ''  }} >November</option>
            <option value="12" {{ $currentMonth == '12' ? 'selected' : ''  }} >Desember</option>
        </select>
        <select name="tahun" id="tahun" class="form-control select2" style="width: 20%; float: left; margin-right:20px;">
            @for ($i = 2019;$i <= $currentYear; $i++)
                <option value="{{ $i }}" {{ $i == $currentYear ? 'selected' : '' }}>{{ $i }}</option>
            @endfor
        </select>

        <input type="button" id="btn-search" value="Cari" class="btn btn-primary">
        <br><br><br>
        <table class="table table-bordered" id="table-preview">

        </table>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("labarugi.laporan") }}';
    var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/keuangan/labarugi_laporan.js') }}"></script> 
@endsection 