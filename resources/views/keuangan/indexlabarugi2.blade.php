<!-- 
    ===========Create By Dedy 06-03-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanLabaRugi')}}">Laporan Laba / Rugi</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan Laba / Rugi</h2>
    <br>
    <?php 
    
    $currentYear = date('Y');
    ?>
    <form action="{{url('LaporanLabaRugi/cari_bulan_pl')}}" method="post">
    {{ csrf_field() }}
        <br>
        <?php $currentMonth = date('n') ; ?>
        <select name="bulan" id="bulan" class="form-control" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
        <?php $now = date('Y');         
        ?>
        <select name="tahun" id="tahun" class="form-control" style="width: 20%; float: left; margin-right:20px;">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>

        <input type="submit" value="Cari" class="btn btn-primary" onclick="val_submit()">
    </form>
    <br><br><br>
    <?php
    if($data['bulan'] != ""){

      if($data['bulan_ini']=="1"){
        $data['bulan_sekarang']="Januari";
      }elseif($data['bulan']=="2"){
        $data['bulan_sekarang']="Februari";
      }elseif($data['bulan']=="3"){
        $data['bulan_sekarang']="Maret";
      }elseif($data['bulan']=="4"){
        $data['bulan_sekarang']="April";
      }elseif($data['bulan']=="5"){
        $data['bulan_sekarang']="Mei";
      }elseif($data['bulan']=="6"){
        $data['bulan_sekarang']="Juni";
      }elseif($data['bulan']=="7"){
        $data['bulan_sekarang']="Juli";
      }elseif($data['bulan']=="8"){
        $data['bulan_sekarang']="Agustus";
      }elseif($data['bulan']=="9"){
        $data['bulan_sekarang']="September";
      }elseif($data['bulan']=="10"){
        $data['bulan_sekarang']="Oktober";
      }elseif($data['bulan']=="11"){
        $data['bulan_sekarang']="November";
      }elseif($data['bulan']=="12"){
        $data['bulan_sekarang']="Desember";
      }
    }
      ?>
    <table class="table">
            <thead style="font-size: 13px;">
                <tr>
                    <th>Keterangan</th>
                    <th><?php echo $data['bulan_sekarang']." ".$data['tahun'] ?></th>
                    <th>s/d <?php echo $data['bulan_sekarang']." ".$data['tahun'] ?></th>
                </tr>
            </thead>
            <tbody style="font-size: 13px">
            <?php foreach ($set_pl as $pl) { ?>
                <tr>
                    <td><?php
                            if($pl->kategori=="judul"){
                                echo "<b>- ".$pl->keterangan."<b>";
                            }else if($pl->kategori=="subjudul"){
                                echo "-- ".$pl->keterangan;
                            }else if($pl->kategori=="data"){
                                echo "&nbsp;&nbsp;&nbsp; ".$pl->keterangan;
                            }else if($pl->kategori=="hasil"){
                                echo "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$pl->keterangan."</b>";
                            }else if($pl->kategori=="spasi"){
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$pl->keterangan;
                            }else if($pl->kategori=="total"){
                                echo "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$pl->keterangan."</b>";
                            }
                        ?>
                    </td>
                    <td style="text-align: right;">
                      <?php
                            if($pl->kategori!="judul" && $pl->kategori!="subjudul"){
                              echo $pl->total_bulan_ini==''?'Rp. '.'0':'Rp. '.$pl->total_bulan_ini;
                      } ?>
                    </td>
                    <td style="text-align: right;">
                      <?php
                            if($pl->kategori!="judul" && $pl->kategori!="subjudul"){
                              echo $pl->bulan_sd_bulan==''?'Rp. '.'0':'Rp. '.$pl->bulan_sd_bulan ;
                      } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
@endsection 