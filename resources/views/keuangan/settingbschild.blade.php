<ul>
@foreach($childs as $child)
	<li>
        
        {{ $child->Keterangan }} 
        &nbsp;&nbsp;<a href="{{url('BalanceSheet/editdata')}}/<?php echo $child->IDNeraca ?>"><i class="fa fa-pencil" onclick="editneraca()" style="color:green"></i></a>
        <i class="fa fa-trash" onclick="hapusneraca()" style="color:red"></i>
	@if(count($child->childs))
            @include('keuangan.settingbschild',['childs' => $child->childs])
        @endif
	</li>
@endforeach
</ul>