@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanGeneralLedger')}}">Laporan General Ledger</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan General Ledger</h2>
    <br>
    <?php 
    $currentYear = date('Y');
    $currentMonth = date('m');
    ?>


        <select data-placeholder="Cari Berdasarkan Group" name="field" class="chosen-select form-control" id="field" style="width: 0%">
            <option value="tbl_jurnal.IDCOA">COA</option>
        </select>   


    <div class="col-md-4">
        <div id="select">
            <select data-placeholder="Semua COA" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100%">
                <option value="">- Semua COA -</option>
                <?php foreach ($coa as $row) {
                    echo "<option value='$row->IDCoa'>$row->Nama_COA</option>";
                }
            ?>
          </select>
        </div>
    </div>

        <div class="col-md-4">
        <select name="bulan" id="bulan" class="form-control select2" style="width: 40%; float:left; margin-right:5px" required>
            <option value="1" {{ $currentMonth == '1' ? 'selected' : ''  }} >Januari</option>
            <option value="2" {{ $currentMonth == '2' ? 'selected' : ''  }} >Februari</option>
            <option value="3" {{ $currentMonth == '3' ? 'selected' : ''  }} >Maret</option>
            <option value="4" {{ $currentMonth == '4' ? 'selected' : ''  }} >April</option>
            <option value="5" {{ $currentMonth == '5' ? 'selected' : ''  }} >Mei</option>
            <option value="6" {{ $currentMonth == '6' ? 'selected' : ''  }} >Juni</option>
            <option value="7" {{ $currentMonth == '7' ? 'selected' : ''  }} >Juli</option>
            <option value="8" {{ $currentMonth == '8' ? 'selected' : ''  }} >Agustus</option>
            <option value="9" {{ $currentMonth == '9' ? 'selected' : ''  }} >September</option>
            <option value="10" {{ $currentMonth == '10' ? 'selected' : ''  }} >Oktober</option>
            <option value="11" {{ $currentMonth == '11' ? 'selected' : ''  }} >November</option>
            <option value="12" {{ $currentMonth == '12' ? 'selected' : ''  }} >Desember</option>
        </select>
        <select name="tahun" id="tahun" class="form-control select2" style="width: 40%; float: left; margin-right:20px;">
            @for ($i = 2019;$i <= $currentYear; $i++)
                <option value="{{ $i }}" {{ $i == $currentYear ? 'selected' : '' }}>{{ $i }}</option>
            @endfor
        </select>
        </div>

        <div class="col-md-3">
            <div class="form_grid_2">
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>                
        </div>
        <br><br><br>
        
        <div class="banner">
            <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor Transaksi</th>
                        <th>Kode COA</th>
                        <th>Nama COA</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Saldo Akhir</th>
                    </tr>
                </thead>
            </table>
        </div>

    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("GeneralLedger.datatable") }}';
    var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/keuangan/gl_laporan.js') }}"></script> 
@endsection 