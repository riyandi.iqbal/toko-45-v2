<!-- 
    ===========Create By Dedy 06-03-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelKeuangan')}}">Master Control Panel Keuangan</a>
		</h2>
	</div>
	<br>
    <div class="bannerbody container">
        <a href="{{url('LabaRugi')}}" style="color: #fff">
            <div class="col-md-3" style="padding: 10px; margin-right:10px; margin-bottom:10px; background-color: #254283">
                <label>Setting Laba/Rugi</label>
            </div>
        </a>    

        <a href="{{url('BalanceSheet')}}" style="color: #fff">
            <div class="col-md-3" style="padding: 10px; margin-right:10px; margin-bottom:10px; background-color: #254283">
                <label>Setting Neraca</label>
            </div>
        </a>  
    </div>
</div>
@endsection 