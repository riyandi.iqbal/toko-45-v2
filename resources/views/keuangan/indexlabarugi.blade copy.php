<!-- 
    ===========Create By TIAR 04-06-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanLabaRugi')}}">Laporan Laba / Rugi</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan Laba / Rugi</h2>
    <br>
    <?php 
    
    $currentYear = date('Y');
    ?>
    <form action="{{url('LaporanLabaRugi/cari_bulan_pl')}}" method="post">
    {{ csrf_field() }}
        <br>
        <?php $currentMonth = date('n') ; ?>
        <select name="bulan" id="bulan" class="form-control" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
        <?php $now = date('Y');         
        ?>
        <select name="tahun" id="tahun" class="form-control" style="width: 20%; float: left; margin-right:20px;">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>

        <input type="submit" value="Cari" class="btn btn-primary" onclick="val_submit()">
    </form>
    <br><br><br>
    <table class="table">
            <thead style="font-size: 13px;">
                <tr>
                    <th>Keterangan</th>
                    <th>Bulan Berjalan</th>
                    <th>Bulan Sekarang</th></th>
                </tr>
            </thead>
            <tbody style="font-size: 13px">
            
            </tbody>
        </table>
    </div>
</div>
@endsection 