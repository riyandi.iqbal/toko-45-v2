@extends('layouts.app')   
@section('content') 
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('BalanceSheet')}}">Setting Neraca</a>
		</h2>
	</div>
    <div class="banner text-center" style="font-size: 16px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Setting Neraca</span>
    </div>
    <div class="bannerbody container">
        <div  style="float:left; width:45%; margin-right: 5%;">
            <label class="judul">INPUT DATA NERACA</label>
            <hr>
            <div class="col-md-3">
                <label class="judul">COA</label>
            </div>
            <div class="col-md-9">
                <select class="chosen-select" id="idcoa" name="idcoa" onchange="gantiketerangan()">
                <?php foreach ($coa as $keycoa) { 
                    if($keycoa->IDCoa == $dataedit['IDCoa']){ ?>
                        <option value="<?php $keycoa->IDCoa ?>" selected><?php echo $keycoa->Kode_COA ?> -- <?php echo $keycoa->Nama_COA ?></option>
                    <?php }else{ ?>
                    <option value="<?php echo $keycoa->IDCoa ?>"><?php echo $keycoa->Kode_COA ?> -- <?php echo $keycoa->Nama_COA ?></option>
                    <?php } ?>
                <?php } ?>
                </select>
            <br><br>
            </div>
            
            <div class="col-md-3">
                <label class="judul">Keterangan</label>        
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" value="<?php echo $dataedit['Keterangan'] ?>" id="keterangan" name="keterangan" onkeyup="javascript:this.value=this.value.toUpperCase();">
            <br><br>
            </div>

            <div class="col-md-3">
                <label class="judul">Kategori</label>
            </div>
            <div class="col-md-9">
                <select class="chosen-select" name="kategori" id="kategori" onchange="cekkategori()">
                    <option value="">Pilih Kategori</option>
                    <option value="judul">JUDUL</option>
                    <option value="subjudul">SUB JUDUL</option>
                    <option value="data">DATA</option>
                    <option value="hasil">HASIL</option>
                    <option value="spasi">SPASI</option>
                    <option value="total">TOTAL</option>
                </select>
            <br><br>
            </div>

            <div class="col-md-3">
                <label class="judul">Perhitungan</label>
            </div>
            <div class="col-md-9">
                <select class="chosen-select" name="perhitungan" id="perhitungan">
                    <option value="tidakada">TIDAK ADA</option>
                    <option value="debet">DEBET</option>
                    <option value="kredit">KREDIT</option>
                </select>
            <br><br>
            </div>
            <div class="group">
                <div class="col-md-3">
                    <label class="judul">GROUP</label>
                </div>
                <div class="col-md-9">
                    <select class="chosen-select" id="idcoa2" name="idcoa2" >
                    <option value="0"></option>
                    <?php foreach ($coa as $keycoa2) { ?>
                        <option value="<?php echo $keycoa2->IDCoa ?>"><?php echo $keycoa2->Kode_COA ?> -- <?php echo $keycoa2->Nama_COA ?></option>
                    <?php } ?>
                    </select>
                <br><br>
                </div>
            </div>
            

            <div class="col-md-12"><button class="btn btn-primary" onclick="simpandata()">SIMPAN</button></div>
        </div>

        <div style="width: 40%; float: left; margin-right: 5%;">
        <label class="judul">SAMPLE KELUARAN NERACA</label>
        <hr>
            <div class="col-md-12">
				<ul id="tree1">
				    @foreach($headergroup as $header)
				        <li>
                           
                            {{ $header->Keterangan }}   
                            &nbsp;&nbsp;<i class="fa fa-pencil" onclick="editneraca()" style="color:green">
                                                    <input type="hidden" name="editneraca" id="editneraca" value="<?php echo $header->IDNeraca ?>">
                                              </i>
                            <i class="fa fa-trash" onclick="hapusneraca()" style="color:red">
                                        <input type="hidden" name="hapusneraca" id="hapusneraca" value="<?php echo $header->IDNeraca ?>">
                                  </i>
                            
				            
                            @if(count($header->childs))
				                @include('keuangan.settingbschild',['childs' => $header->childs])
				            @endif
				        </li><br>
				    @endforeach
				</ul>
            </div>
        </div>
        
    </div>
</div>
<script src="{{ asset('js/keuangan/neraca_create.js') }}"></script> 
<script>
    var urlSimpan = "{{url('BalanceSheet/simpandata')}}";
    var urlEdit = "{{url('BalanceSheet/editdata')}}";
    var urlAmbilkode = "{{url('BalanceSheet/ambilkode')}}";
    var token = "{{ csrf_token() }}";
    var urlReload = "{{url('BalanceSheet')}}";
</script>
@endsection 

