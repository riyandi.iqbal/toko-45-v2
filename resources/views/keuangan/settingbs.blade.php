@extends('layouts.app')   
@section('content') 
<style>
    .btn-add, .btn-remove {
        padding: 8px;
    }
</style>
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('BalanceSheet')}}">Setting Neraca</a>
		</h2>
	</div>
    <div class="banner text-center" style="font-size: 16px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Setting Neraca</span>
    </div>
    <div class="bannerbody container">
        <form id="form-data">
            <div  style="float:left; width:70%;">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th style="width: 10%">Aksi <button type="button" class="btn col-20 btn-sm btn-add-head"> + </button> </th>
                            <th style="width: 35%">COA</th>
                            <th style="width: 25%">Keterangan / label</th>
                            <th style="width: 15%">Kategori</th>
                            <th style="width: 15%">Group</th>
                        </tr>
                    </thead>
                    <tbody id="tbody-table-data">
                        @foreach ($data1 as $item)
                            <tr>
                                <td style="width: 10%"> 
                                    <button type="button" class="btn col-20 btn-sm btn-add"> + </button> <button type="button" class="btn col-18 btn-sm btn-remove"> - </button>
                                </td>
                                <td  style="width: 35%">
                                    <select name="IDCoa[]" class="select2 idcoa" style="width: 100%">
                                        <option value="">-</option>
                                        @foreach ($coa as $val)
                                            <option value="{{ $val->IDCoa }}" data-kode="{{ $val->Kode_COA }}" data-nama="{{ $val->Nama_COA }}" {{ $item->IDCoa == $val->IDCoa ? 'selected' : '' }}>{{ $val->Kode_COA . ' - ' . $val->Nama_COA }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td style="width: 25%">
                                    <textarea name="Keterangan[]" class="keterangan">{{ $item->Keterangan }}</textarea>
                                </td>
                                <td  style="width: 15%"> 
                                    <select name="Kategori[]" class="select2 kategori" style="width: 100%">     
                                        <option value="judul" {{ $item->Kategori == 'judul' ? 'selected' : '' }}> Judul </option>    
                                        <option value="data" {{ $item->Kategori == 'data' ? 'selected' : '' }}> Data </option>
                                        <option value="hasil" {{ $item->Kategori == 'hasil' ? 'selected' : '' }}> Hasil </option>
                                        <option value="spasi" {{ $item->Kategori == 'spasi' ? 'selected' : '' }}> Spasi </option>
                                        <option value="total" {{ $item->Kategori == 'total' ? 'selected' : '' }}> Total </option>
                                    </select> 
                                </td>
                                <td  style="width: 15%">
                                    <input type="text" name="Group[]" value="{{ $item->Group }}"> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div  style="float:right; width:25%; margin-left:2%;">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody id="table-preview-i">
                        
                    </tbody>
                </table>
            </div>
            <div class="text-center"  style="width:100%; margin-left:2%;">
                <br><br>
                <div class="btn">
                    <button type="button" class="btn col-16" style="width: 100px;" id="preview">Preview</button>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-success" style="width: 100px;">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>


<div id="modal-preview" class="modal fade" role="dialog">
    <div class="modal-dialog">
            {{ csrf_field() }}
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Preview</h4>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody id="table-preview">
                            
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="close" data-dismiss="modal">Tutup</button>
                </div>
            </div>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlSimpan = "{{url('BalanceSheet/simpandata')}}";
    var urlAmbilkode = "{{url('BalanceSheet/ambilkode')}}";
    var token = "{{ csrf_token() }}";
    var urlReload = "{{url('BalanceSheet')}}";
    var data_coa = <?php echo json_encode($coa); ?>;
    
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/keuangan/labarugi_create.js') }}"></script> 
@endsection 
