<!-- 
    ===========Create By Dedy 06-03-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanNeraca')}}">Laporan Neraca</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan Neraca</h2>
    <br>
    <?php 
    
    $currentYear = date('Y');
    ?>
    <form action="{{url('LaporanNeraca/cari_bulan_lr')}}" method="post">
    {{ csrf_field() }}
        <br>
        <?php $currentMonth = $data['bulan'] ? $data['bulan'] : date('n') ; ?>
        <select name="bulan" id="bulan" class="form-control" style="width: 20%; float:left; margin-right:5px " required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
        <?php $now = date('Y');?>
        <select name="tahun" id="tahun" class="form-control" style="width: 20%; float: left">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>
        <label class="judul" style="float: left;">S.D.</label>
        <?php  $currentMonth = $data['bulan1'] ? $data['bulan1'] : date('n', strtotime("-1 month")); ?>
        <select name="bulan1" id="bulan1" class="form-control" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
    
        <?php $now = date('Y'); ?>
        <select name="tahun1" id="tahun1" class="form-control" style="width: 20%; float: left; margin-right: 30px">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>
        
        <input type="submit" value="Cari" class="btn btn-primary" onclick="val_submit()">
    </form>
    <br><br><br>
    
    <table class="table">
    <?php
      if($data['bulan'] != ""){

        if($data['bulan_ini']=="1"){
          $data['bulan_sekarang_ini']="Januari";
        }elseif($data['bulan_ini']=="2"){
            $data['bulan_sekarang_ini']="Februari";
        }elseif($data['bulan_ini']=="3"){
            $data['bulan_sekarang_ini']="Maret";
        }elseif($data['bulan_ini']=="4"){
            $data['bulan_sekarang_ini']="April";
        }elseif($data['bulan_ini']=="5"){
            $data['bulan_sekarang_ini']="Mei";
        }elseif($data['bulan_ini']=="6"){
            $data['bulan_sekarang_ini']="Juni";
        }elseif($data['bulan_ini']=="7"){
            $data['bulan_sekarang_ini']="Juli";
        }elseif($data['bulan_ini']=="8"){
            $data['bulan_sekarang_ini']="Agustus";
        }elseif($data['bulan_ini']=="9"){
            $data['bulan_sekarang_ini']="September";
        }elseif($data['bulan_ini']=="10"){
            $data['bulan_sekarang_ini']="Oktober";
        }elseif($data['bulan_ini']=="11"){
            $data['bulan_sekarang_ini']="November";
        }elseif($data['bulan_ini']=="12"){
            $data['bulan_sekarang_ini']="Desember";
        }
        
        if($data['bulan_ini_dikurang']=="1"){
            $data['bulan_dikurang_sekarang']="Januari";
        }elseif($data['bulan_ini_dikurang']=="2"){
            $data['bulan_dikurang_sekarang']="Februari";
        }elseif($data['bulan_ini_dikurang']=="3"){
            $data['bulan_dikurang_sekarang']="Maret";
        }elseif($data['bulan_ini_dikurang']=="4"){
            $data['bulan_dikurang_sekarang']="April";
        }elseif($data['bulan_ini_dikurang']=="5"){
            $data['bulan_dikurang_sekarang']="Mei";
        }elseif($data['bulan_ini_dikurang']=="6"){
            $data['bulan_dikurang_sekarang']="Juni";
        }elseif($data['bulan_ini_dikurang']=="7"){
            $data['bulan_dikurang_sekarang']="Juli";
        }elseif($data['bulan_ini_dikurang']=="8"){
            $data['bulan_dikurang_sekarang']="Agustus";
        }elseif($data['bulan_ini_dikurang']=="9"){
            $data['bulan_dikurang_sekarang']="September";
        }elseif($data['bulan_ini_dikurang']=="10"){
            $data['bulan_dikurang_sekarang']="Oktober";
        }elseif($data['bulan_ini_dikurang']=="11"){
            $data['bulan_dikurang_sekarang']="November";
        }elseif($data['bulan_ini_dikurang']=="12"){
          $data['bulan_dikurang_sekarang']="Desember";
        }else{
            $data['bulan_dikurang_sekarang']="Januari";
        }
    }
        ?>
            <thead style="font-size: 13px;">
                <tr>
                    <th>Keterangan</th>
                    <th><?php echo $data['bulan_sekarang_ini']." ".$data['tahun'] ?></th>
                    <th><?php echo $data['bulan_dikurang_sekarang']." ".$data['tahun'] ?></th>
                    <th>+/-</th>
                </tr>
            </thead>
            <tbody style="font-size: 13px">
            <?php foreach ($set_lr as $lr) { ?>
                <tr>
                    <td><?php
                            if($lr->kategori=="judul"){
                                echo "<b>- ".$lr->keterangan."<b>";
                            }else if($lr->kategori=="subjudul"){
                                echo "-- ".$lr->keterangan;
                            }else if($lr->kategori=="data"){
                                echo "&nbsp;&nbsp;&nbsp; ".$lr->keterangan;
                            }else if($lr->kategori=="hasil"){
                                echo "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$lr->keterangan."</b>";
                            }else if($lr->kategori=="spasi"){
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$lr->keterangan;
                            }else if($lr->kategori=="total"){
                                echo "<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ".$lr->keterangan."</b>";
                            }
                        ?>
                    </td>
                    
                    <td style="text-align: right;">
                        <?php
                            if($lr->kategori!="judul" && $lr->kategori!="subjudul" && $lr->nilai!='yes'){
                                echo $lr->total_bulan_ini==''?'Rp. '.'0':'Rp. '.number_format($lr->total_bulan_ini);
                            }else if($lr->kategori=="subjudul" && $lr->nilai=='yes'){ 
                                    echo $lr->total_bulan_ini==''?'Rp. '.'0':'Rp. '.number_format($lr->total_bulan_ini);
                            }
                            ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                            if($lr->kategori!="judul" && $lr->kategori!="subjudul" && $lr->nilai!='yes'){
                                echo $lr->total_bulan_lalu==''?'Rp. '.'0':'Rp. '.number_format($lr->total_bulan_lalu);
                            }else if($lr->kategori=="subjudul" && $lr->nilai=='yes'){ 
                                    echo $lr->total_bulan_lalu==''?'Rp. '.'0':'Rp. '.number_format($lr->total_bulan_lalu);
                            } ?>
                    </td>
                    <td style="text-align: right;">
                        <?php
                        if($lr->kategori!="judul" && $lr->kategori!="subjudul" && $lr->nilai!='yes'){
                            echo 'Rp. '.number_format($lr->total_bulan_lalu-$lr->total_bulan_ini);
                        }else if($lr->kategori=="subjudul" && $lr->nilai=='yes'){ 
                            echo 'Rp. '.number_format($lr->total_bulan_lalu-$lr->total_bulan_ini);
                        }
                         ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function val_submit()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>
@endsection 