<!-- 
    ===========Create By Dedy 06-03-2020=============== 
-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('LaporanNeraca')}}">Laporan Neraca</a>
		</h2>
    </div>  
    <br>
    <div class="bannerbody container">
    <h2>Laporan Neraca</h2>
    <br>
    <?php 
    
    $currentYear = date('Y');
    ?>
    <form action="{{url('LaporanNeraca/cari_bulan_lr')}}" method="post">
    {{ csrf_field() }}
        <br>
        <?php $currentMonth = date('n') ; ?>
        <select name="bulan" id="bulan" class="form-control" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
        <?php $now = date('Y');         
        ?>
        <select name="tahun" id="tahun" class="form-control" style="width: 20%; float: left">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>
        <label class="judul" style="float: left;">S.D.</label>
        <?php  $currentMonth = date('n', strtotime("-1 month")); ?>
        <select name="bulan1" id="bulan1" class="form-control" style="width: 20%; float:left; margin-right:5px" required>
            <option value="">Pilih Bulan</option>
            <option value="1" <?php if($currentMonth=="1"){echo 'selected';}else{ } ?>>Januari</option>
            <option value="2" <?php if($currentMonth=="2"){echo 'selected';}else{ } ?>>Februari</option>
            <option value="3" <?php if($currentMonth=="3"){echo 'selected';}else{ } ?>>Maret</option>
            <option value="4" <?php if($currentMonth=="4"){echo 'selected';}else{ } ?>>April</option>
            <option value="5" <?php if($currentMonth=="5"){echo 'selected';}else{ } ?>>Mei</option>
            <option value="6" <?php if($currentMonth=="6"){echo 'selected';}else{ } ?>>Juni</option>
            <option value="7" <?php if($currentMonth=="7"){echo 'selected';}else{ } ?>>Juli</option>
            <option value="8" <?php if($currentMonth=="8"){echo 'selected';}else{ } ?>>Agustus</option>
            <option value="9" <?php if($currentMonth=="9"){echo 'selected';}else{ } ?>>September</option>
            <option value="10" <?php if($currentMonth=="10"){echo 'selected';}else{ } ?>>Oktober</option>
            <option value="11" <?php if($currentMonth=="11"){echo 'selected';}else{ } ?>>November</option>
            <option value="12" <?php if($currentMonth=="12"){echo 'selected';}else{ } ?>>Desember</option>
        </select>
    
        <?php $now = date('Y'); ?>
        <select name="tahun1" id="tahun1" class="form-control" style="width: 20%; float: left; margin-right: 30px">
            <?php for ($a=2019;$a<=$now;$a++) { ?>
                <option value="<?php echo $a ?>" <?php if($currentYear==$a){echo 'selected';}else{ } ?>><?php echo $a ?></option>
            <?php } ?>
        </select>
        
        <input type="submit" value="Cari" class="btn btn-primary" onclick="val_submit()">
    </form>
    <br><br><br>
    
   
    </div>
</div>
<script>
    function val_submit()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>
@endsection 