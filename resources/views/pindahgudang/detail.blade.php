<!-- ===========Create By Tiar 18-02-2020 =============== -->
@extends('layouts.app')
@section('content')
    <!-- body data -->
    <?php use App\Helpers\AppHelper; ?>
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('PindahGudang')}}">Pindah Gudang</a>
              <i class="fa fa-angle-right"></i>
              Detail Pindah Gudang - {{ $PindahGudang->Nomor }}
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table table-bordered" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" >Tanggal</td>
                            <td width="65%"> {{ AppHelper::DateIndo($PindahGudang->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td> {{ $PindahGudang->Nomor }} </td>
                        </tr>
                        <tr>
                            <td>Gudang</td>
                            <td> {{ $PindahGudang->Nama_gudang_asal }} </td>
                        </tr>
                        <tr>
                            <td>Gudang Tujuan</td>
                            <td> {{ $PindahGudang->Nama_gudang_tujuan }} </td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td> {{ AppHelper::NumberFormat($PindahGudang->Total) }} </td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td> {{ $PindahGudang->Keterangan }} </td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td> {{ ($PindahGudang->Batal == 0) ? 'Aktif' : 'Dibatalkan' }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($PindahGudangDetail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga) }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga * $item->Qty) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot style="background-color: #16305d; color: #fff">
                        <tr>
                            <th colspan="2">Total</th>
                            <th style="text-align: right;"> {{ AppHelper::NumberFormat($PindahGudang->Total) }} </th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <a style="color: white;" href="{{ route('PindahGudang.index') }}">
                        <div class="btn col-11">
                            <span> Kembali </span>
                        </div>
                    </a>
                    @if ($PindahGudang->Batal == 0)
                        {{-- <a style="color: white;" href="{{ route('PindahGudang.show', $PindahGudang->IDPG) }}">
                            <div class="btn col-1">
                                <span>Edit Data</span>
                            </div>
                        </a> --}}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection