<!-- CREATE BY DEDY 27 MARET 2020 -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('KodeTransaksi')}}">Data Kode Transaksi</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Ubah Data Kode Transaksi</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Ubah Data Kode Transaksi</span>
    </div>
    <div class="bannerbody">
        <label class="judul">Menu</label>
        <select data-placeholder="Cari COA" class="chosen-select" name="idmenu" id="idmenu" required>
        <option value='0'>--==Pilih Menu Transaksi==--</option>;
            <?php foreach ($IDMenu as $menu) {
                 if ($menu->IDMenuDetail == $dataedit->IDMenuDetail) { ?>
                    <option value="<?= $menu->IDMenuDetail ?>" selected> <?= $menu->Menu_Detail ?></option> 
                    <!-- Bila Data Sudah isi sebelumnya -->
                    <?php }else{
                        echo "<option value='$menu->IDMenuDetail'>$menu->Menu_Detail</option>";
                    }}
                    ?>
                    
        </select>
        <br>
        <label class="judul">Preview Kode</label>
        <input type="tex" class="form-control" id="kodeview" name="kodeview" readonly value="<?php echo $dataedit->Kode ?>">

        <input type="hidden" id="jadi1" name="jadi1">
        <input type="hidden" id="jadi2" name="jadi2">
        <input type="hidden" id="jadi3" name="jadi3">
        <input type="hidden" id="jadi4" name="jadi4">

        <input type="hidden" id="pisah1" name="pisah1">
        <input type="hidden" id="pisah2" name="pisah2">
        <input type="hidden" id="pisah3" name="pisah3">
        <input type="hidden" id="pisah4" name="pisah4">
        <br><br>
        <table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;">         
            <thead style="background-color: #16305d; color: white">
                <tr>
                <th style="width: 20%">Asal Format</th>
                <th style="width: 10%">Tampil</th>
                <th style="width: 15%">Manual Kode</th>
                <th style="width: 10%">Pemisah</th>
                <th style="width: 15%">Format</th>
                <th style="width: 15%">Jenis Format</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <select class="chosen-select" style="width: 200px" id="asal1" name="asal1" onchange="gantiasal()" data-placeholder="Pilih Asal Format">
                            <?php if($dataedit->Asal1=='1'){?>
                                <option value="1" selected>No Urut</option>
                                <option value="2">Manual</option>
                                <option value="3">Bulan</option>
                                <option value="4">Tahun</option>
                            <?php }else if($dataedit->Asal1=='2') {?>
                                <option value="1">No Urut</option>
                                <option value="2" selected>Manual</option>
                                <option value="3">Bulan</option>
                                <option value="4">Tahun</option>
                            <?php }else if($dataedit->Asal1=='3') {?>
                                <option value="1">No Urut</option>
                                <option value="2">Manual</option>
                                <option value="3" selected>Bulan</option>
                                <option value="4">Tahun</option>
                            <?php }else if($dataedit->Asal1=='4') {?>
                                <option value="1">No Urut</option>
                                <option value="2">Manual</option>
                                <option value="3">Bulan</option>
                                <option value="4" selected>Tahun</option>
                            <?php }else {?>
                                <option value="1">No Urut</option>
                                <option value="2">Manual</option>
                                <option value="3">Bulan</option>
                                <option value="4">Tahun</option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 100px" id="tampil1" name="tampil1" data-placeholder="Pilih Tampil" onchange="tampil()">
                            <?php if($dataedit->Tampil1=='1'){?>
                                <option value="1" selected>Ya</option>
                                <option value="2">Tidak</option>
                            <?}else if($dataedit->Tampil1=='2'){?>
                                <option value="1">Ya</option>
                                <option value="2" selected>Tidak</option>
                            <?}else{?>
                                <option value="1">Ya</option>
                                <option value="2">Tidak</option>
                            <?}?>
                        </select>
                    </td>
                    <td>
                    <?php if($dataedit->Manual1==''){?>
                        <input type="tex" class="form-control" id="manual1" name="manual1">
                    <?}else{?>
                        <input type="tex" class="form-control" id="manual1" name="manual1" value="<?php echo $dataedit->Manual1 ?>">
                    <?}?>
                    </td>
                    <td>
                    <?php if($dataedit->Pemisah1==''){?>
                        <input type="tex" class="form-control" id="pemisah1" name="pemisah1" onchange="pemisah1()">
                    <?php }else{?>
                        <input type="tex" class="form-control" id="pemisah1" name="pemisah1" value="<?php echo $dataedit->Pemisah1 ?>" onchange="pemisah1()">
                    <?php }?>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="format1" name="format1" data-placeholder="Pilih Format" onchange="tampil()">
                            <?php if($dataedit->Format1=='1'){?>
                                <option value="0"></option>
                                <option value="1" selected>0000</option>
                                <option value="2">MM</option>
                                <option value="3">YY</option>
                            <?php }else if($dataedit->Format1=='2'){?>
                                <option value="0"></option>
                                <option value="1">0000</option>
                                <option value="2" selected>MM</option>
                                <option value="3">YY</option>
                            <?php }else if($dataedit->Format1='3'){?>
                                <option value="0"></option>
                                <option value="1">0000</option>
                                <option value="2">MM</option>
                                <option value="3" selected>YY</option>
                            <?php }else{?>
                                <option value="0"></option>
                                <option value="1">0000</option>
                                <option value="2">MM</option>
                                <option value="3">YY</option>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="jenis1" name="jenis1" data-placeholder="Pilih Jenis" onchange="tampil()">
                            <?php if($dataedit->Jenis1=='1'){?>
                                <option value="0"></option>
                                <option value="1" selected>Angka</option>
                                <option value="2">Romawi</option>
                            <?php }else if($dataedit->Jenis1=='2'){?>
                                <option value="0"></option>
                                <option value="1">Angka</option>
                                <option value="2" selected>Romawi</option>
                            <?php }else{?>
                                <option value="0"></option>
                                <option value="1">Angka</option>
                                <option value="2">Romawi</option>
                            <?php } ?>
                            
                        </select>
                    </td>
                </tr>
                <!-- BARIS 2 START -->
                <tr>
                    <td>
                        <select class="chosen-select" style="width: 200px" id="asal2" name="asal2" onchange="gantiasal2()" data-placeholder="Pilih Asal Format">
                            <option value=""></option>
                            <option value="1">No Urut</option>
                            <option value="2">Manual</option>
                            <option value="3">Bulan</option>
                            <option value="4">Tahun</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 100px" id="tampil2" name="tampil2" data-placeholder="Pilih Tampil" onchange="tampil2()">
                            <option value="0"></option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="manual2" name="manual2" onchange="tampil2()">
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="pemisah2" name="pemisah2" onchange="pemisah1()">
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="format2" name="format2" data-placeholder="Pilih Format" onchange="tampil2()">
                            <option value="0"></option>
                            <option value="1">0000</option>
                            <option value="2">MM</option>
                            <option value="3">YY</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="jenis2" name="jenis2" data-placeholder="Pilih Jenis" onchange="tampil2()">
                            <option value="0"></option>
                            <option value="1">Angka</option>
                            <option value="2">Romawi</option>
                        </select>
                    </td>
                </tr>
                <!-- BARIS 3 START -->
                <tr>
                    <td>
                        <select class="chosen-select" style="width: 200px" id="asal3" name="asal3" onchange="gantiasal3()" data-placeholder="Pilih Asal Format">
                            <option value=""></option>
                            <option value="1">No Urut</option>
                            <option value="2">Manual</option>
                            <option value="3">Bulan</option>
                            <option value="4">Tahun</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 100px" id="tampil3" name="tampil3" data-placeholder="Pilih Tampil" onchange="tampil3()">
                            <option value="0"></option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="manual3" name="manual3" onchange="tampil3()">
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="pemisah3" name="pemisah3" onchange="pemisah1()">
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="format3" name="format3" data-placeholder="Pilih Format" onchange="tampil3()">
                            <option value="0"></option>
                            <option value="1">0000</option>
                            <option value="2">MM</option>
                            <option value="3">YY</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="jenis3" name="jenis3" data-placeholder="Pilih Jenis" onchange="tampil3()">
                            <option value="0"></option>
                            <option value="1">Angka</option>
                            <option value="2">Romawi</option>
                        </select>
                    </td>
                </tr>
                <!-- BARIS 4 START -->
                <tr>
                    <td>
                        <select class="chosen-select" style="width: 200px" id="asal4" name="asal4" onchange="gantiasal4()" data-placeholder="Pilih Asal Format">
                            <option value=""></option>
                            <option value="1">No Urut</option>
                            <option value="2">Manual</option>
                            <option value="3">Bulan</option>
                            <option value="4">Tahun</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 100px" id="tampil4" name="tampil4" data-placeholder="Pilih Tampil" onchange="tampil4()">
                            <option value="0"></option>
                            <option value="1">Ya</option>
                            <option value="2">Tidak</option>
                        </select>
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="manual4" name="manual4" onchange="tampil4()">
                    </td>
                    <td>
                        <input type="tex" class="form-control" id="pemisah4" name="pemisah4" onchange="pemisah1()">
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="format4" name="format4" data-placeholder="Pilih Format" onchange="tampil4()">
                            <option value="0"></option>
                            <option value="1">0000</option>
                            <option value="2">MM</option>
                            <option value="3">YY</option>
                        </select>
                    </td>
                    <td>
                        <select class="chosen-select" style="width: 150px" id="jenis4" name="jenis4" data-placeholder="Pilih Jenis" onchange="tampil4()">
                            <option value="0"></option>
                            <option value="1">Angka</option>
                            <option value="2">Romawi</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <div class="text-center">
    		<div class="btn btn-success hvr-icon-float-away">
                <span style="color: white;" onclick="simpan()">Simpan&nbsp;&nbsp;&nbsp;</span>
            </div>
            
    	</div>
    </div>
</div>
<script src="{{ asset('js/kodetransaksi/kodetransaksi_create.js') }}"></script>  
<script>
    $(document).ready(function() {
        document.getElementById("manual1").disabled = true;
        document.getElementById("manual2").disabled = true;
        document.getElementById("manual3").disabled = true;
        document.getElementById("manual4").disabled = true;

        document.getElementById("pemisah1").disabled = true;
        document.getElementById("pemisah2").disabled = true;
        document.getElementById("pemisah3").disabled = true;
        document.getElementById("pemisah4").disabled = true;
              
    })

    var tokendata = "{{ csrf_token() }}";
    var urlBack     = "{{url('KodeTransaksi')}}";
    
</script>
@endsection