<!-- CREATE BY DEDY 27 MARET 2020 -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('KodeTransaksi')}}">Data Kode Transaksi</a>
        </h2>
    </div>
    <br>
    <div class="bannerbody">
        <div class="form_grid_3">
            <div class="btn btn-primary hvr-icon-float-away">
                <a href="{{url('KodeTransaksi/tambah_data')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;&nbsp;</span></a>
            </div>
        </div>
        <br>
           
            <table id="tblkode" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Nama Menu</th>
                        <th>Preview Kode</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody id="previewdata"> 
                <?php
                    $i = 1; 
                    foreach ($kode as $dk) { ?>
                    <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $dk->Menu_Detail ?></td>
                        <td><?php echo $dk->Kode ?></td>
                        <td>
                            <a href="{{url('Editkodetransaksi')}}/<?php echo $dk->IDKode ?>"><span style="color: blue;"><i class="fa fa-pencil fa-lg"></i></span></a>
                        </td>
                    </tr>
                <?php $i++; } ?>
                </tbody>
            </table>
        
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblkode').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false,
            'destroy'   : true,          
        }); 
        @if (session('alertakses'))
          swal("Perhatian", "{{ session('alertakses') }}", "warning");
        @endif          
    })
</script>
@endsection