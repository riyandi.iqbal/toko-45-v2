@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="#">Data Customer</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{route('Customer.create')}}"><span style="color: white;">Tambah Data</span></a>
                </div>
            </div>
            <br><br>
            <div class="col-md-3">
              <label class="field_title mt-dot2">.</label>
              <select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                <option value=""></option>
                <option value="Nama_Group_Customer">Nama Group Customer</option>
                <option value="Kode_Customer">Kode Customer</option>
                <option value="Nama">Nama</option>
                <option value="Alamat">Alamat</option>
                <option value="No_Telpon">No Telpon</option>
                <option value="Kota">Kota</option>
                <option value="Fax">Fax</option>
                <option value="Email">Email</option>
                <option value="NPWP">NPWP</option>
                <option value="No_KTP">No KTP</option>
              </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
      <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
        <thead style="color: #fff">
            <th>No</th>
            <th>Nama Group Customer</th>
            <th>Kode Customer</th>
            <th>Nama</th>
            <th>Aktif</th>
            <th>Aksi</th>
        </thead>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/customer/index.js') }}"></script>
<script>
    var urlData = '{{ route("Customer.datatable") }}';
    var url                 = '{{ url("Customer") }}';
</script>
@endsection 