@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Customer')}}">Data Customer</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Customer</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Customer</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Group Customer</label>
            </div>
            <div class="col-md-9">
            <select data-placeholder="Pilih Group Customer"  name="IDGroupCustomer" id="IDGroupCustomer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:75%;" class="form-control select2" tabindex="13">
                <option value=""></option> 
                <?php foreach($groupcustomer as $group)
                {
                    echo "<option value='$group->IDGroupCustomer'>$group->Nama_Group_Customer</option>";
                } 
                ?>
            </select>
            <div class="flot-right btn btn-primary hvr-icon-float-away">
                <a data-toggle="modal" data-target="#modalgroup"><span style="color: white;">Group Customer </span></a>
            </div>
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Kode Customer</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Customer" name="Kode_Customer" id="Kode_Customer" required oninvalid="this.setCustomValidity('Kode Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="{{ $kode_customer }}" readonly>
                    <br><br>
                </div>

                <div class="col-md-3">
                    <label class="judul">Nama Customer</label>
                </div>
                <div class="col-md-9">
                <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Nama" name="Nama" id="Nama" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Alamat</label> 
                </div>
                <div class="col-md-9">
                <textarea class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Alamat" name="Alamat" id="Alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="6"></textarea>
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Kota</label>        
                </div>
                <div class="col-md-9">
                <select data-placeholder="Pilih Kota"  name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:75%;" class="chosen-select" tabindex="13">
                    <option value=""></option> 
                    <?php foreach($kota as $kot)
                    {
                        echo "<option value='$kot->IDKota'>$kot->Kota</option>";
                    } 
                    ?>
                </select>
                <div class="flot-right btn btn-primary hvr-icon-float-away">
                    <a data-toggle="modal" data-target="#modalkota"><span style="color: white;">Tambah Kota</span></a>
                </div>
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">No Telepon</label>
                </div>
                <div class="col-md-9">              
                    <input type="tex" class="form-control" placeholder="+62" name="No_Telpon" id="No_Telpon" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Fax</label>               
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" name="Fax" id="Fax" placeholder=" Fax" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Email</label>
                </div>
                <div class="col-md-9">
                <input type="email" class="form-control" name="Email" id="Email" placeholder=" Email"  required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">NPWP</label>
                </div>
                <div class="col-md-9">
                <input type="tex" class="form-control" name="NPWP" id="NPWP" placeholder=" NPWP" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>
                
                <div class="col-md-3">
                <label class="judul">No KTP</label>
                </div> 
                <div class="col-md-9">
                <input type="tex" class="form-control" name="No_KTP" id="No_KTP" placeholder=" No_KTP" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                </div>
                
            </div>
            <div class="text-center">
                <br><br>
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url('Customer')}}" style="color: white;" name="simpan">Kembali</a></span>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>



      <div id="modalgroup" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <form method="post" action="simpangroup" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Data Group Customer</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                      <label class="judul">Kode Group Customer</label>             
                    </div>
                    <div class="col-md-9">
                      <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Group Customer" name="kode_group" id="kode_group" required oninvalid="this.setCustomValidity('Kode Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                      <label class="judul">Group Customer</label>
                </div>
                    <div class="col-md-9">
                      <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Nama Group Customer" name="groupcustomer" id="groupcustomer" required oninvalid="this.setCustomValidity('Nama Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
              </form>
              </div>
            </div>


            <div id="modalkota" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <form method="post" action="simpankota" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kota</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                      <label class="judul">Kode Kota</label>             
                    </div>
                    <div class="col-md-9">
                      <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Kota" name="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                      <label class="judul">Provinsi</label>
                </div>
                    <div class="col-md-9">
                      <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Provinsi" name="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                      <label class="judul">Kota</label>
                </div>
                    <div class="col-md-9">
                      <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kota" name="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
              </form>
              </div>
            </div>

            
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Customer") }}';
  var urlInsert           = '{{ route("Customer.store") }}';
  var url                 = '{{ url("Customer") }}';
  var urlKodeCustomer    = '{{ route("Customer.kode") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/customer/create.js') }}"></script>
@endsection 
