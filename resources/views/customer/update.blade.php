@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Customer')}}">Data Customer</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Customer</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Customer</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Group Customer</label>
            </div>
            <div class="col-md-9">
                <input type="hidden" name="IDCustomer" id="IDCustomer" value="<?php echo $edit->IDCustomer?>">
                <select data-placeholder="Pilih Group Customer"  name="IDGroupCustomer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                    <option value=""></option> 
                    <?php
                        foreach ($groupcustomer as $group) {
                        if ($edit->IDGroupCustomer==$group->IDGroupCustomer) {
                    ?>
                    <option value="<?php echo $group->IDGroupCustomer ?>" selected><?php echo $group->Nama_Group_Customer; ?></option>
                    <?php
                        } else {
                    ?>
                    <option value="<?php echo $group->IDGroupCustomer ?>"><?php echo $group->Nama_Group_Customer; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Kode Customer</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Customer" name="Kode_Customer" id="Kode_Customer" value="<?php echo $edit->Kode_Customer ?>" required oninvalid="this.setCustomValidity('Kode Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>

                <div class="col-md-3">
                    <label class="judul">Nama Customer</label>
                </div>
                <div class="col-md-9">
                <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Nama" name="Nama" id="Nama" value="<?php echo $edit->Nama ?>" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Alamat</label> 
                </div>
                <div class="col-md-9">
                <textarea class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Alamat" name="Alamat" id="Alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="resize: none;" rows="6"><?php echo $edit->Alamat ?></textarea>
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Kota</label>        
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Kota"  name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option> 
                        <?php  foreach ($kota as $kot) {
                            if ($edit->IDKota==$kot->IDKota) {
                        ?>
                        <option value="<?php echo $kot->IDKota ?>" selected><?php echo $kot->Kota; ?></option>
                        <?php
                            } else {
                        ?>
                        <option value="<?php echo $kot->IDKota ?>"><?php echo $kot->Kota; ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">No Telepon</label>
                </div>
                <div class="col-md-9">              
                    <input type="tex" class="form-control" placeholder="+62" name="No_Telpon" id="No_Telpon" value="<?php echo $edit->No_Telpon ?>" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Fax</label>               
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" name="Fax" id="Fax" placeholder=" Fax" value="<?php echo $edit->Fax ?>" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">Email</label>
                </div>
                <div class="col-md-9">
                <input type="email" class="form-control" name="Email" id="Email" placeholder=" Email" value="<?php echo $edit->Email ?>" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>

                <div class="col-md-3">
                <label class="judul">NPWP</label>
                </div>
                <div class="col-md-9">
                <input type="tex" class="form-control" name="NPWP" id="NPWP" placeholder=" NPWP" value="<?php echo $edit->NPWP ?>" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
                </div>
                
                <div class="col-md-3">
                <label class="judul">No KTP</label>
                </div> 
                <div class="col-md-9">
                <input type="tex" class="form-control" name="No_KTP" id="No_KTP" placeholder=" No_KTP" value="<?php echo $edit->No_KTP ?>" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <input type="hidden" name="idcustomer" id="idcustomer" value="<?php echo $edit->IDCustomer; ?>">
                </div>
                
            </div>
            <div class="text-center">
                <br><br>
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url('Customer')}}" style="color: white;" name="simpan">Kembali</a></span>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Customer") }}';
  var urlUpdate           = '{{ route("Customer.update_data") }}';
  var url                 = '{{ url("Customer") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/customer/update.js') }}"></script>
@endsection 
