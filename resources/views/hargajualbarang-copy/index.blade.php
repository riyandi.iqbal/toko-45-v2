<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('HargaJualBarang')}}">Data Harga Jual</a>
                <i class="fa fa-angle-right"></i>
                <a> Tambah Harga Jual Barang </a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
               <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url('HargaJualBarang/tambah_data')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div> 
            
            </div>
            <br><br><br>
            <div class="col-md-12 text-center">
                <div class="col-md-4">
                    <select name="field" id="field" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="form-control">
                        <option value=""></option>
                        <option value="Kode_Barang">Kode Barang</option>
                        <option value="Nama_Barang">Nama Barang</option>
                        <option value="Satuan">Satuan</option>
                        <option value="Modal">Modal</option>
                        <option value="Harga_Jual">Harga Jual</option>
                        <option value="Kode_Group_Customer">Kode Group Customer</option>
                        <option value="Nama_Group_Customer">Nama Group Customer</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <input name="keyword" id="keyword" type="tex" class="form-control" placeholder="Masukkan Keyword">
                </div>
                <div class="col-md-4">
                    <button class="btn btn-primary" id="cari-data" type="submit">Search</button>
                </div>
            </div>
    </div>
    <br>
    <div class="banner">
        <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Satuan</th>
                    <th>Modal</th>
                    <th>Harga Jual</th>
                    <th>Group Customer</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/hargajual/index.js') }}"></script>
<script>
    var urlData = '{{ route("HargaJualBarang.datatable") }}';
    var url                 = '{{ url("HargaJualBarang") }}';
</script>
@endsection