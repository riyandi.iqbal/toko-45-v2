<!-- ===========Create By Dedy 13-12-2019=============== -->
<!-- ===========Updated By Tiar 21-04-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('HargaJualBarang')}}">Data Harga Jual</a>
                <i class="fa fa-angle-right"></i>
                <a> Tambah Harga Jual Barang </a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Harga Jual Barang</span>
    </div>
    <br>
    <div class="banner container">
        <div class="container" style="width: 100%">
            <div class="col-md-3">
                <label class="field_title">Barang</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Cari Berdasarkan Barang" id="barang" name="barang" class="chosen-select form-control">
                    <option value=""></option>
                    <?php foreach($barang as $bar)
                        {
                            echo "<option value='$bar->IDBarang'>$bar->Nama_Barang</option>";
                        } 
                    ?>
                </select>
            <br><br>
            </div>
            
            <div class="col-md-3">
                <label class="field_title">Satuan</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Cari Berdasarkan Satuan" class="chosen-select form-control" name="satuan" id="satuan" required oninvalid="this.setCustomValidity('Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php foreach($satuan as $sat)
                        {
                            echo "<option value='$sat->IDSatuan'>$sat->Satuan</option>";
                        } 
                    ?>
                </select>
            <br><br>
            </div>
            
            <div class="col-md-3">
                <label class="field_title">Modal</label>
            </div>
            <div class="col-md-9">
                <input name="modal" placeholder="Rp." type="tex" id="modal" class="form-control price-date" required oninvalid="this.setCustomValidity('Modal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>
            
            <div class="col-md-3">
                <label class="field_title">Harga Jual</label>
            </div>
            <div class="col-md-9">
                <input name="harga_jual" placeholder="Rp." id="harga_jual" class="form-control price-date" type="tex" required oninvalid="this.setCustomValidity('Harga Jual Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>
            
            <div class="col-md-3">
                <label class="field_title">Group Customer</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Cari Berdasarkan Group Customer" class="chosen-select form-control"  name="groupcustomer" id="groupcustomer" required oninvalid="this.setCustomValidity('Group Customer Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                        <?php foreach($groupcustomer as $group)
                            {
                                echo "<option value='$group->IDGroupCustomer'>$group->Nama_Group_Customer</option>";
                            } 
                        ?>
                </select>
            <br><br>
            </div>
            
            <div class="col-md-12 text-center">
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url('HargaJualBarang')}}" style="color: white;">Kembali</a></span>
                </div>
                <input onclick="simpan()" name="simtam" class="btn btn-primary" value="Simpan Data">
            </div>
        </div> 
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var rupiah = document.getElementById('modal');
            rupiah.addEventListener('keyup', function(e){
                rupiah.value = formatRupiah(this.value);
            });
        var rupiah2 = document.getElementById('harga_jual');
            rupiah2.addEventListener('keyup', function(e){
                rupiah2.value = formatRupiah(this.value);
            });
    })
    
  function field()
    {
        var data1 = {
			"Barang" 		:$('#barang').val(),
			"Satuan"		:$('#satuan').val(),
			"Modal"			:$('#modal').val().replace('.',''),
			"Hargajual"		:$('#harga_jual').val().replace('.',''),
			"Group"			:$('#groupcustomer').val(),
        }
        return data1;
    }

    function simpan()
    {
        var data = field();
        
            console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{url('HargaJualBarang/simpandata')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data1:data},
                  dataType:'json',
                  beforeSend: function(data) {
                    $('#modal-loading').fadeIn('fast');
                  },
                  success: function(data)
                  { 
                    $("#modal-loading").fadeOut();
                    if (data.status === false) {
                        swal({
                            title: "",
                            text: data.message,
                            icon: "error",
                            confirmButtonColor: "#254283"
                        });
                    } else {
                        swal({
                          title: "Berhasil",
                          text: "Berhasil Simpan Data",
                          icon: "success"
                      }).then(function() {
                          window.location = "{{url('HargaJualBarang')}}";
                      });
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
              
    }
</script>
@endsection