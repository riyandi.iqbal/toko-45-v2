<!-- create @2019-01-02
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<style>
  span a .fa {
      cursor: pointer;
      padding: 5px;
  }
</style>

<!-- tittle data -->
<div class="main-grid">
<div class="banner">
    <h2>
        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
            <i class="fa fa-angle-right"></i>
            <a href="{{url('Bahanbaku')}}">Data Produksi Bahan Baku</a>
    </h2>
</div>
<!-- ======================= -->
<?php $hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)); ?>
<br>
<div class="banner container">
    <div class="form_grid_3">
            <div class="btn btn-primary hvr-icon-float-away">
                <a href="{{url('Bahan_baku/tambah_produksi')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;&nbsp;</span></a>
            </div>
        </div>
        <br>
        <div class="col-md-2">
            <label class="field_title mt-dot2">Periode</label>
            <div class="form_input">
                <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
            </div>
        </div>
        <div class="col-md-2">
            <label class="field_title mt-dot2"> S/D </label>
            <div class="form_input">
                <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
            </div>
        </div>
        <div class="col-md-3">
            <label class="field_title mt-dot2">.</label>
            <select name="field" id="field" data-placeholder="" style="width: 100%!important" class="form-control" tabindex="13">
                <option value="">Cari Berdasarkan</option>
                <option value="Nomor">No Produksi</option>
                <option value="Nama_Formula">Nama Formula</option>
            </select>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">.</label>
                <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form_grid_2">
                <label>&nbsp;.</label>
                <br>
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>    
            </div>
        </div>
    <!-- </form> -->
</div>
<br>
<div class="banner">
        <div class="widget_content">
                    <!-- <table class="display" id="action_tbl"> -->
            <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor Produksi</th>
                        <th>Nama Formula</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
</div>
</div>  

<script type="text/javascript">
$(document).ready(function() {
        @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif 

        @if (session('alert2'))
            swal("Perhatian", "{{ session('alert2') }}", "warning");
        @endif           
})

</script>

<script>
    var urlData = '{{ route("Bahan_baku.datatable") }}';
    var url                 = '{{ url("Bahan_baku") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/bahanbaku/index.js') }}"></script>
@endsection