<!-- create @2019-01-06
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('Po')}}">Data Produksi Bahan Baku</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Produksi Bahan Baku - <b><?php echo $formula->Nama_Formula ?></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="table cell-border" width="100%" style="font-size: 12px;">
                            <thead style="display: none;">
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo date("d/m/Y", strtotime( $formula->Tanggal) )?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Nama Formula</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo $formula->Nama_Formula?></td>
                                </tr>
                                <tr>
                                    <td  style="background-color: #e5eff0; border: 1px solid;">Total</td>
                                    <td  style="background-color: #e5eff0; border: 1px solid;"><?php echo $formula->Qty_Header ?></td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <!-- <table class="display data_tbl">
                 --><table id="tblshowpo" class="table cell-border" width="100%" style="font-size: 12px;">
                  <thead style="background-color: #16305d; color: #fff">
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Berat</th>
                      <th>Satuan</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($formuladetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($formuladetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Nama_Barang; ?></td>
                          <td class="text-center"><?php echo $data2->Qty; ?></td>
                          <td style="text-align: right;"><?php echo $data2->Satuan ?></td>
                          <td style="text-align: right;"><?php echo $data2->Total; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn col-11 hvr-icon-back">
                                    <span> <a style="color: white;" href="{{url('Bahanbaku')}}" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn col-3 hvr-icon-spin">
                                  <span><a style="color: white;" href="../../Bahanbaku/printdata/<?php echo $formula->IDFormula ?>" target="__blank">Print Data</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblshowpo').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false
        });


    })
</script>
@endsection