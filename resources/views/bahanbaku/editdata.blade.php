<!-- create @2019-01-06
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')

<style type="text/css">
	body {
	background: #f1f1f1;
}



@media screen and (max-width:768px) {
	.table-responsive {
		overflow: auto;
	}
}


.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
  color: red;
  font-weight: bold;
  font-style: italic;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}

</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{url('Bahanbaku')}}">Data Produksi Bahan Baku</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Edit Produksi Bahan Baku</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Produksi Bahan Baku</span>
    </div>
	<div class="banner">
		<form class="form-horizontal" action="{{url('Bahan_baku/simpandataproduksiedit')}}" method="post" >
		{{ csrf_field() }}
			<table class="table table-responsive">
			<tr>
				<td width="50%">
					<label>Tanggal</label>
					<input type="date" class="form-control input-date-padding date-picker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo $formula->Tanggal; ?>">
					<input type="hidden" id="idpbb" name="idpbb" value="<?php echo $formula->IDPBB ?>">
                    <br>
					<label>Nomor Produksi</label>
					<input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Produksi" readonly value="<?php echo $formula->Nomor?>">
					<br>
					<label>Formula</label><br>
                    <input type="text" name="formula" id="formula" class="form-control" value="<?php echo $formula->Nama_Formula?>" readonly>
                    <input type="hidden" name="formulaid" id="formulaid" class="form-control" value="<?php echo $formula->IDFormula?>" readonly>
				</td>
				<td>
					<label>Qty</label>
					<input type="number" class="form-control" id="qty" name="qty" onkeyup="hitungtotal()" value="<?php echo $formula->Qty_Header?>">
					<br>
					<label>Keterangan</label>
					<textarea name="Keterangan" id="Keterangan"  rows="6" tabindex="3" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')"><?php echo $formula->Keterangan?></textarea>
				</td>
			</tr>
		</table>
		<form method="post" action="" >
		<!--<br>-->
		<div class="container" style="width: 100%">
			<div id="content">
		        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
		            <li class="active"><a href="#tab-1" data-toggle="tab">Input Data Produksi Bahan Baku</a></li>
		        </ul>
		        <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active" id="tab-1">
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Produksi Bahan Baku </span>	
		            	</div> 
		            	<div class="container" style="width: 100%">
		            		<br>
		                    	<div class="autocomplete col-md-12">

								    <input type="hidden" name="harga" id="harga" value="0">
								</div>
								
								<table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;">
		                        
			                        <thead style="background-color: #16305d; color: white">
			                          <tr>
			                            <th style="width: 25%">Nama Barang</th>
			                            <th style="width: 20%">Qty</th>
			                            <th style="width: 15%">Satuan</th>
			                            <th style="width: 25%">Total</th>
			                          </tr>
			                        </thead>
			                        <tbody id="tabel-cart-formula">
			                        	
			                        </tbody>
			                    </table>
			                <br>
		                    <br>
			                <br>
								
			                	<button onclick="val_submit()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button>
			                <br> 
		            	</div>
		            </div>
		            
		    	</div>
			</div>
		</div>
		</form>
	</div>
	</div>
    <br><br><br>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

  <script type="text/javascript">
	$(document).ready(function() {

			$('#Barang').editableSelect({
				effects: 'fade'
			});    

            hitungtotal();   
	})

	function gantigudang()
	{
		var gudang = $('#gudang').val();
		window.location.href = "{{url('Bahan_baku/tambah_produksigudang')}}/"+gudang+"";
	}
	 
    function autofill(){
        console.log('proses input ke tabel');
        var field = initialization();
	    PM.push(field);
	    setTimeout(function() {
	        renderJSON(PM);
	        field = null;
	    }, 500);	                 
    }

    

	function val_submit(){
		
		var ket = document.getElementById("Keterangan").value;
		
		if (ket !="") {
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}

	var val_qty;
	var array_qty = new Array(100);
	for (var q = 0; q < array_qty.length; q++) {
		array_qty[q] = new Array(100);
	}

	function hitung(nilai, urutan)
	{
		$('#qty_array'+urutan).val(nilai.replace(/[^\d]/,''));

		total_qty = 0;
		array_qty[urutan][0]=urutan;
		array_qty[urutan][1]=parseInt(nilai);

		for (var y = 0; y < array_qty.length; y++) {
			if(array_qty[y][1]!=null){
				total_qty = parseInt(total_qty) + parseInt(array_qty[y][1]);
			}
		}
		if(total_qty>$('#Total_qty').val()){
			val_qty = false;
			$('#span_qty'+urutan).show();
		}else{
			val_qty = true;
			$('#span_qty'+urutan).hide();
		}

		var harga = $('#hargaa_array'+urutan).val();
		console.log('CEK HARGA');
		console.log(harga);

		var qty = $('#qtya_array'+urutan).val();
		console.log('CEK QTY');
		console.log(qty);
	
	}

	var val_qtyb;
	var array_qtyb = new Array(100);
	for (var q = 0; q < array_qtyb.length; q++) {
		array_qtyb[q] = new Array(100);
	}

	

	function number_format(n, c, d, t) {
		var c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "," : d,
		t = t == undefined ? "." : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;

		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}

	var RS = new Array();
	function masukindetailformula()
	{
		console.log($('#formula').val());
		var Nomor 	= $('#formula').val()
		$.ajax({
                  url : "{{url('Bahanbaku/cekdatadetailformula')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",Nomor:Nomor},
                  dataType:'json',
                success: function(data){
					console.log(data)
					var html = '';
						if (data <= 0) {
						console.log('-----------data kosong------------');
						$("#tabel-cart-formula").html("");
						var value =
							"<tr>" +
							"<td colspan=3 class='text-center'>DATA KOSONG</td>"+
							"</tr>";
						$("#tabel-cart-formula").append(value);
						swal('Perhatian', 'Data Detail Tidak Ada', 'warning');
						} else {
						console.log("BERHASIL AMBIL DATA");
						console.log(data);
							$("#tabel-cart-formula").html("");
							for(i = 0; i<data.length;i++){

								RS[i] = data[i];
								var value =
								"<tr class=list-cart id='rowvar"+i+"'>" +               
								"<td style='padding: 20px;'>"+RS[i].Nama_Barang+"<input type='hidden' readonly id='idbarang"+i+"' name='idbarang"+i+"' value='"+RS[i].IDBarang+"'>"+"</td>"+
								"<td><input style='text-align:right;' type='text' readonly id='berat"+i+"' name='berat"+i+"' value='"+RS[i].Berat+"'>"+"</td>"+
								"<td style='padding: 20px;'>"+RS[i].Satuan+"<input type='hidden' readonly id='idsatuan"+i+"' name='idsatuan"+i+"' value='"+RS[i].IDSatuan+"'>"+"</td>"+
								"<td><input style='text-align:right;' type='text' readonly id='total"+i+"' name='total"+i+"' value='0'>"+"</td>"+
								"</tr>";
							$("#tabel-cart-formula").append(value);
							}
						}
				}
		});
	}
	var RS2 = new Array();
	function hitungtotal()
	{
		console.log($('#qty').val());
		console.log($('#formula').val());
		var Nomor 	= $('#formulaid').val()
		$.ajax({
                  url : "{{url('Bahanbaku/cekdatadetailformula')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",Nomor:Nomor},
                  dataType:'json',
                success: function(data){
					console.log(data)
					var html = '';
						if (data <= 0) {
						console.log('-----------data kosong------------');
						$("#tabel-cart-formula").html("");
						var value =
							"<tr>" +
							"<td colspan=3 class='text-center'>DATA KOSONG</td>"+
							"</tr>";
						$("#tabel-cart-formula").append(value);
						swal('Perhatian', 'Data Detail Tidak Ada', 'warning');
						} else {
						console.log("BERHASIL AMBIL DATA");
						console.log(data);
							$("#tabel-cart-formula").html("");
							for(i = 0; i<data.length;i++){
								
								RS2[i] = data[i];

								var total = RS2[i].Berat*$('#qty').val();

								var value =
								"<tr class=list-cart id='rowvar"+i+"'>" +               
								"<td style='padding: 20px;'>"+RS2[i].Nama_Barang+"<input type='hidden' readonly id='idbarang_array"+i+"' name='idbarang_array[]' value='"+RS2[i].IDBarang+"'>"+"</td>"+
								"<td><input style='text-align:right;' type='text' readonly id='berat_array"+i+"' name='berat_array[]' value='"+RS2[i].Berat+"'>"+"</td>"+
								"<td style='padding: 20px;'>"+RS2[i].Satuan+"<input type='hidden' readonly id='idsatuan_array"+i+"' name='idsatuan_array[]' value='"+RS2[i].IDSatuan+"'>"+"</td>"+
								"<td><input style='text-align:right;' type='text' readonly id='total_array"+i+"' name='total_array[]' value='"+total+"'>"+"</td>"+
								"</tr>";
							$("#tabel-cart-formula").append(value);
							}
						}
				}
		});
	}
  

</script>
@endsection