<!-- create @2019-01-02
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')

<?php
if ($kodepbb->curr_number == null) {
  $number = 1;
} else {
  $number = $kodepbb->curr_number + 1;
}
$kodemax = str_pad($number, 4, "0", STR_PAD_LEFT);
  
$array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
$code_pbb = 'PBB-'.$kodemax.'/'.$array_bulan[date('n')].'/'.date('y');
?>
<style type="text/css">
	body {
	background: #f1f1f1;
}



@media screen and (max-width:768px) {
	.table-responsive {
		overflow: auto;
	}
}


.blink {
  animation: blink-animation 1s steps(5, start) infinite;
  -webkit-animation: blink-animation 1s steps(5, start) infinite;
  color: red;
  font-weight: bold;
  font-style: italic;
}
@keyframes blink-animation {
  to {
    visibility: hidden;
  }
}
@-webkit-keyframes blink-animation {
  to {
    visibility: hidden;
  }
}

</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{url('Bahanbaku')}}">Data Produksi Bahan Baku</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Produksi Bahan Baku</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Produksi Bahan Baku</span>
    </div>
	<div class="banner">
		<form class="form-horizontal" action="{{url('Bahan_baku/simpandataproduksi')}}" method="post" >
		{{ csrf_field() }}
			<table class="table table-responsive">
			<tr>
				<td width="50%">
					<label>Tanggal</label>
					<input type="date" class="form-control input-date-padding date-picker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
					<br>
					<label>Nomor Produksi</label>
					<input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Produksi" value="<? echo $code_pbb; ?>" readonly>
					<br>
					<label>Gudang</label><br>
                    <select data-placeholder="Gudang Penerima" class="chosen-select" id="gudang" name="gudang" onchange="gantigudang()">
                    	
                    <?php if ($idgudang==''){ ?>
                    		<option value=""><-=Silahkan Pilih Gudang=-></option>
                            <?php foreach ($gudang as $gd) : ?>
                            <option value="<?php echo $gd->IDGudang ?>">
                            <?php echo $gd->Nama_Gudang ?></option>
                            <?php endforeach ?>
                    	<?php } else { ?>
                    		<option value="<?php echo $idgudang['IDGudang'] ?>"><?php echo $idgudang['Nama_Gudang'] ?></option>
                            <option value=""></option>
                      		<option value=""><-=xxxxxxxxxxxxxxxxxxxx=-></option>
                            <?php foreach ($gudang as $gd) : ?>
                            <option value="<?php echo $gd->IDGudang ?>">
                            <?php echo $gd->Nama_Gudang ?></option>
                            <?php endforeach ?>
                    	<?php } ?>
				</td>
				<td>
					<label>Keterangan</label>
					<textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')"></textarea>
				</td>
			</tr>
		</table>
		<div class="container" style="width: 100%">
			<div id="content">
		        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
		            <li class="active"><a href="#tab-1" data-toggle="tab">Input Data Produksi Bahan Baku</a></li>
		        </ul>
		        <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active" id="tab-1">
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Produksi Bahan Baku </span>	
		            	</div> 
		            	<div class="container" style="width: 100%">
		            		<br>
		                    	<div class="autocomplete col-md-12">
								    <input id="cariwarna" type="hidden" class="form-control" name="cariwarna" placeholder="Cari Warna" onchange="return autofill();">
								    <input type="hidden" name="qty" id="qty" value="0">
								    <input type="hidden" name="harga" id="harga" value="0">
								</div>
								
								<table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;width:100%">
		                        
			                        <thead style="background-color: #16305d; color: white">
			                          <tr>
			                            <th style="width: 25%">Nama Barang</th>
			                            <th style="width: 10%">Qty</th>
			                            <th style="width: 15%">Satuan</th>
			                            <th style="width: 2%">Action</th>
			                          </tr>
			                        </thead>
			                        <tbody id="tabel-cart-po">
			                        	
			                        </tbody>
			                    </table>
			                    <a class="btn col-11" id="buttonadd" onclick="addmore()" /><i class="col-11 hvr-icon-float-away">Tambah</i></a>
		                    <br>
		                    <br>
			                <br>
			                	<button onclick="val_submit()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button>
			                <br> 
		            	</div>
		            </div>
		            
		    	</div>
			</div>
		</div>
		</form>
	</div>
	</div>
    <br><br><br>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <script type="text/javascript">
	$(document).ready(function() {

			$('#Barang').editableSelect({
				effects: 'fade'
			});       
		addmore();        
	})

	function gantigudang()
	{
		var gudang = $('#gudang').val();
		window.location.href = "{{url('Bahan_baku/tambah_produksigudang')}}/"+gudang+"";
	}
	 
    function autofill(){
        console.log('proses input ke tabel');
        var field = initialization();
	    PM.push(field);
	    setTimeout(function() {
	        renderJSON(PM);
	        field = null;
	    }, 500);	                 
    }

    //=================================ADDMORE ROW TABLE
    
    var i=1;
    function addmore()
	{
		i = i + 1;
		var htm_option_barang = "";
		var get_data_barang = JSON.parse(`<?php echo json_encode($barang); ?>`);

		for(var a in get_data_barang){
			var row_barang = get_data_barang[a];
			htm_option_barang+="<option value="+row_barang['IDBarang']+">"+row_barang['Nama_Barang']+"</option>";
		}

		var htm_option_satuan = "";
		var get_data_satuan = JSON.parse(`<?php echo json_encode($satuan); ?>`);
		for(var b in get_data_satuan){
			var row_satuan = get_data_satuan[b];
			htm_option_satuan += "<option value="+row_satuan['IDSatuan']+">"+row_satuan['Satuan']+"</option>";
		}

		$('#tabelfsdfsf').append('<tr id="rowvar'+i+'" style="width:100%"><td><select  placeholder="Cari Barang" class="cari form-control" name="Baranga_array[]" id="Baranga'+i+'" onchange="gantisatuan(this.value, i);" style="width:75%"><option value="" label="Pilih..."></option>'+htm_option_barang+'</select><input type="hidden" name="kode_baranga_array[]" class="form-control"></td><td><input placeholder="Quantity" type="number" id="qtya_array'+i+'" name="qtya_array[]" onkeyup="hitung(this.value, i);" onchange="cekstok(this.value, i)" class="form-control"><span id="span_qty'+i+'" style="display: none;"></span></td><td><input type="text" id="Satuana_array'+i+'" name="Satuana_array[]" readonly class="form-control"><input type="hidden" id="kode_satuana_array'+i+'" name="kode_satuana_array[]" readonly class="form-control"><!--select  placeholder="Cari Satuan" class="chosen-select form-control" name="Satuana_array[]" id="Satuana'+i+'" ><option value="" label="Pilih..."></option>'+htm_option_satuan+'</select><input type="hidden" name="kode_satuana_array[]" class="form-control"--></td><td><a onclick="deleteitem('+i+')"><i class="fa fa-times-circle fa-lg" style="color:red; padding: 10px;"></i></a></td></tr>');
        $('.cari').select2(); 
    }

	
	function deleteitem(i)
	{
		var result = confirm("Apakah anda yakin?");
		if (result) {
			$('#rowvar'+i).remove();
		}
	}

	function gantisatuan(nilai, urutan)
	{
		console.log('IDBARANG');
		console.log($('#Baranga'+urutan).val());

		var idbrg 	= $('#Baranga'+urutan).val();

		$.ajax({
				url : "{{url('Bahanbaku/ambilsatuan')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",idbrg:idbrg},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log(data[0].Satuan);
                    $('#Satuana_array'+urutan).val(data[0].Satuan);
                    $('#kode_satuana_array'+urutan).val(data[0].IDSatuan);
                    //hitungkurs(urutan);
                  }
		});
		
	}

	function val_submit(){
		
		var ket = document.getElementById("Keterangan").value;
		
		if (ket !="") {
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}

	var val_qty;
	var array_qty = new Array(100);
	for (var q = 0; q < array_qty.length; q++) {
		array_qty[q] = new Array(100);
	}

	function hitung(nilai, urutan)
	{
		$('#qty_array'+urutan).val(nilai.replace(/[^\d]/,''));

		total_qty = 0;
		array_qty[urutan][0]=urutan;
		array_qty[urutan][1]=parseInt(nilai);

		for (var y = 0; y < array_qty.length; y++) {
			if(array_qty[y][1]!=null){
				total_qty = parseInt(total_qty) + parseInt(array_qty[y][1]);
			}
		}
		if(total_qty>$('#Total_qty').val()){
			val_qty = false;
			$('#span_qty'+urutan).show();
		}else{
			val_qty = true;
			$('#span_qty'+urutan).hide();
		}

		var harga = $('#hargaa_array'+urutan).val();
		console.log('CEK HARGA');
		console.log(harga);

		var qty = $('#qtya_array'+urutan).val();
		console.log('CEK QTY');
		console.log(qty);
	
	}

	var val_qtyb;
	var array_qtyb = new Array(100);
	for (var q = 0; q < array_qtyb.length; q++) {
		array_qtyb[q] = new Array(100);
	}

	

	function number_format(n, c, d, t) {
		var c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "," : d,
		t = t == undefined ? "." : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;

		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}

	function cekstok(nilai, urutan)
	{
		$('#qty_array'+urutan).val(nilai.replace(/[^\d]/,''));
		console.log("CEK MASUKAN");
		var qty 	= $('#qtya_array'+urutan).val();
		var nama 	= $('#Baranga'+urutan).val();
		console.log(qty);
		console.log(nama);
		$.ajax({
				url : "{{url('Bahan_baku/cekstok')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",idbrg:nama, qty:qty},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log(data);
                    if(data < 0){
	                  swal({
                        title: "Perhatian!",
                        text: "Stok Tidak Tersedia",
                        icon: "warning"
                    }).then(function() {
                        $('#qtya_array'+urutan).focus();
                    });
	                  
	                }
                    
                  }
		});
	}
  

</script>
@endsection