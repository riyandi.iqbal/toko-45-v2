<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    table tr td {
        padding: 5px;
    }

    hr {
        margin : 5px;
        border-top : 1px solid #8f8f8f;
    }

    .pagination {
        margin: unset !important;
    }

    
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('MutasiGiro.index')}}">Mutasi Giro</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Mutasi Giro</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Mutasi Giro</span>
    </div>

    <form id="form-data">
        <div class="banner">
            {{ csrf_field() }}
            <table class="table-responsive" style="width: 100%;">
                <tr>
                    <td style="width: 50%">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="<?php echo date('d/m/Y'); ?>">
                    </td>
                    <td>
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan" class="form-control"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <label for="Nomor">Nomor</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="" readonly>
                    </td>
                </tr>
            </table>
            <hr>
            <table class="table-responsive" style="width: 100%;">
                <tr>
                    <td style="width: 50%">
                        <label for="Jenis_giro">Jenis Giro</label><br>
                        <select class="form-control select2 form-detail" style="width: 100%" name="Jenis_giro" id="Jenis_giro">
                            <option value="">-- Pilih Jenis Giro --</option>
                            <option value="Masuk">Giro Masuk</option>
                            <option value="Keluar">Giro Keluar</option>
                        </select>
                    </td>
                    <td style="width: 50%">
                        <label for="Status">Status</label><br>
                        <select class="form-control select2 form-detail" style="width: 100%" name="Status" id="Status">
                            <option value="">-- Pilih Status --</option>
                            <option value="Cair">Cair</option>
                            <option value="Batal">Batal</option>
                            <option value="Nomor_baru">Nomor Baru</option>
                        </select>
                    </td>   
                </tr>
                <tr>
                    <td>
                        <label for="IDPerusahaan">Nama</label>
                        <select class="form-control select2 form-detail" style="width: 100%" name="IDPerusahaan" id="IDPerusahaan">
                            <option value="">-- Pilih Perusahaan --</option>
                            {{-- @foreach ($perusahaan as $item)
                                <option value="{{ $item->ID }}" data-kategori={{ $item->Kategori }}> {{ $item->Nama . ' - ' . $item->Kategori }} </option>
                            @endforeach --}}
                        </select>
                    </td>
                    <td>
                        <label for="Nomor_baru">Nomor Baru</label>
                        <input type="text" name="Nomor_baru" id="Nomor_baru" class="form-control form-detail" disabled>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="IDGiro">Nomor Giro</label>
                        <select class="form-control form-detail select2" style="width: 100%" name="IDGiro" id="IDGiro">
                            <option value="">-- Pilih Giro --</option>
                        </select>
                    </td>
                    <td>
                        <label for="IDBank">Nama</label>
                        <select class="form-control form-detail select2" style="width: 100%" name="IDBank" id="IDBank">
                            <option value="">-- Pilih Bank --</option>
                            @foreach ($bank as $item)
                                <option value="{{ $item->IDCoa }}" data-rekening="{{ $item->Nomor_Rekening }}"> {{ $item->Atas_Nama . ' - ' .$item->Nama_COA }} </option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Tanggal_giro">Tanggal Giro</label>
                        <input type="text" name="Tanggal_giro" id="Tanggal_giro" class="form-control form-detail form-auto" readonly>
                    </td>
                    <td>
                        <label for="Nomor_rekening">Nomor Rekening</label>
                        <input type="text" name="Nomor_rekening" id="Nomor_rekening" class="form-control form-detail" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Nomor_faktur">Nomor Faktur</label>
                        <input type="text" name="Nomor_faktur" id="Nomor_faktur" class="form-control form-detail form-auto" readonly>
                    </td>
                    <td>
                        <label for="Nominal_giro">Nominal Giro</label>
                        <input type="text" name="Nominal_giro" id="Nominal_giro" class="form-control form-detail form-auto" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Tanggal_faktur">Tanggal Faktur</label>
                        <input type="text" name="Tanggal_faktur" id="Tanggal_faktur" class="form-control form-detail form-auto" readonly>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-primary pull-right" type="button" id="btn-add-detail" name="btn-add-detail" style="float: center; margin-top: 10px;">Tambah</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="banner">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-responsive" id="table-detail" style="font-size: 12px; margin-top: 10px; width: 100%;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th>No</th>
                                <th>Jenis Giro</th>
                                <th>Perusahaan</th>
                                <th>Nomor Giro</th>
                                <th>Tanggal Giro</th>
                                <th>Nomor Faktur</th>
                                <th>Tanggal Faktur</th>
                                <th>Status</th>
                                <th>Nomor Baru</th>
                                <th>Nama Bank</th>
                                <th>Nomor Rekening</th>
                                <th>Nominal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tfoot style="background-color: #16305d; color: white">
                            <tr>
                                <th colspan="11" style="text-align:right">Total Nominal</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="col-lg-12" style="margin-bottom: 40px;" >
                    <button class="btn btn-primary pull-right" type="submit" id="Simpan" name="Simpan" style="float: center; margin-top: 10px;">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("MutasiGiro.index") }}';
    var urlNumberInvoice    = '{{ route("MutasiGiro.number") }}';
    var urlInsert           = '{{ route("MutasiGiro.store") }}';
    var url                 = '{{ url("MutasiGiro") }}';
    var urlNomorGiro        = '{{ route("MutasiGiro.nomor_giro") }}';
    var urlPerusahaan        = '{{ route("MutasiGiro.perusahaan") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/mutasigiro/mutasigiro_create.js') }}"></script>

@endsection