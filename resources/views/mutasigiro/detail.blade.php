<!-- ===========Create By Tiar 18-02-2020 =============== -->
@extends('layouts.app')
@section('content')
    <!-- body data -->
    <?php use App\Helpers\AppHelper; ?>
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('MutasiGiro')}}">Mutasi Giro</a>
              <i class="fa fa-angle-right"></i>
              Detail Mutasi Giro - {{ $MutasiGiro->Nomor }}
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table table-bordered" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" >Tanggal</td>
                            <td width="65%"> {{ AppHelper::DateIndo($MutasiGiro->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td> {{ $MutasiGiro->Nomor }} </td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td> {{ AppHelper::NumberFormat($MutasiGiro->Total) }} </td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td> {{ $MutasiGiro->Keterangan }} </td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td> {{ ($MutasiGiro->Batal == 0) ? 'Aktif' : 'Dibatalkan' }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Jenis Giro</th>
                            <th>Perusahaan</th>
                            <th>Nomor Giro</th>
                            <th>Tanggal Giro</th>
                            <th>Nomor Faktur</th>
                            <th>Status</th>
                            <th>Nomor Baru</th>
                            <th>Nama Bank</th>
                            <th>Nomor Rekening</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($MutasiGiroDetail as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->Jenis_giro }} </td>
                                <td> {{ $item->Nama }} </td>
                                <td> {{ $item->Nomor_giro }} </td>
                                <td> {{ $item->Tanggal_giro }} </td>
                                <td> {{ $item->Nomor_faktur }} </td>
                                <td> {{ $item->Status }} </td>
                                <td> {{ $item->Nomor_baru }} </td>
                                <td> {{ $item->Nama_bank }} </td>
                                <td> {{ $item->Nomor_rekening }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Nilai) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot style="background-color: #16305d; color: #fff">
                        <tr>
                            <th colspan="10">Total</th>
                            <th style="text-align: right;"> {{ AppHelper::NumberFormat($MutasiGiro->Total) }} </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <a style="color: white;" href="{{ route('MutasiGiro.index') }}">
                        <div class="btn col-11">
                            <span> Kembali </span>
                        </div>
                    </a>
                    @if ($MutasiGiro->Batal == 0)
                        <a style="color: white;" href="{{ route('MutasiGiro.show', $MutasiGiro->IDJU) }}">
                            <div class="btn col-1">
                                <span>Edit Data</span>
                            </div>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection