<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Tipe')}}">Data Tipe</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Tipe</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">     

      <span>Edit Data Tipe</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="judul">Kode Tipe</label>
          </div>
          <div class="col-md-9">
            <input type="hidden" name="IDTipe" id="IDTipe" value="<?php echo $tipe->IDTipe; ?>" /> 
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Kode_Tipe" id="Kode_Tipe" value="<?php echo $tipe->Kode_Tipe; ?>" required oninvalid="this.setCustomValidity('Kode Type Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Nama Tipe</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama_Tipe" id="Nama_Tipe" value="<?php echo $tipe->Nama_Tipe; ?>" required oninvalid="this.setCustomValidity('Nama Tipe Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
          </div>
          
        </div>
        <br><br><br>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('Tipe')}}" name="simpan">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Tipe") }}';
  var urlUpdate           = '{{ route("Tipe.update_data") }}';
  var url                 = '{{ url("Tipe") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/tipe/update.js') }}"></script>
@endsection