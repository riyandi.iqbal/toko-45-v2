@extends('layouts.app')
@section('content')
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<style>
    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 3px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    }

    .box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {
        display: inline-block;
        font-size: 18px;
        margin: 0;
        line-height: 1;
    }

    .box-header>.box-tools {
        position: absolute;
        right: 10px;
        top: 5px;
    }

    .pull-right {
        float: right;
    }

    .box-header.with-border {
        border-bottom: 1px solid #f4f4f4;
    }

    .box-header {
        color: #444;
        display: block;
        padding: 10px;
        position: relative;
    }

    .box-body {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        padding: 10px;
    }

    .box-footer {
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
        border-top: 1px solid #f4f4f4;
        padding: 10px;
        background-color: #fff;
    }

    .small-box {
        border-radius: 2px;
        position: relative;
        display: block;
        margin-bottom: 20px;
        box-shadow: 0 1px 1px rgba(0,0,0,0.1);
    }

    .small-box>.inner {
        padding: 10px;
    }

    .small-box>.small-box-footer {
        position: relative;
        text-align: center;
        padding: 3px 0;
        color: #fff;
        color: rgba(255,255,255,0.8);
        display: block;
        z-index: 10;
        background: rgba(0,0,0,0.1);
        text-decoration: none;
    }

    .bg-aqua, .callout.callout-info, .alert-info, .label-info, .modal-info .modal-body {
        background-color: #00c0ef !important;
    }

    .bg-green, .callout.callout-success, .alert-success, .label-success, .modal-success .modal-body {
        background-color: #00a65a !important;
    }

    .bg-red, .bg-yellow, .bg-aqua, .bg-blue, .bg-light-blue, .bg-green, .bg-navy, .bg-teal, .bg-olive, .bg-lime, .bg-orange, .bg-fuchsia, .bg-purple, .bg-maroon, .bg-black, .bg-red-active, .bg-yellow-active, .bg-aqua-active, .bg-blue-active, .bg-light-blue-active, .bg-green-active, .bg-navy-active, .bg-teal-active, .bg-olive-active, .bg-lime-active, .bg-orange-active, .bg-fuchsia-active, .bg-purple-active, .bg-maroon-active, .bg-black-active, .callout.callout-danger, .callout.callout-warning, .callout.callout-info, .callout.callout-success, .alert-success, .alert-danger, .alert-error, .alert-warning, .alert-info, .label-danger, .label-info, .label-warning, .label-primary, .label-success, .modal-primary .modal-body, .modal-primary .modal-header, .modal-primary .modal-footer, .modal-warning .modal-body, .modal-warning .modal-header, .modal-warning .modal-footer, .modal-info .modal-body, .modal-info .modal-header, .modal-info .modal-footer, .modal-success .modal-body, .modal-success .modal-header, .modal-success .modal-footer, .modal-danger .modal-body, .modal-danger .modal-header, .modal-danger .modal-footer {
        color: #fff !important;
    }
</style>
<div class="main-grid">
    <div class="bannerbody" style="height: 100%;">

        <div class="row">
            <div class="col-lg-4 col-xs-6">
                <div class="box box-info">
                    <div class="box-header">
                      <h3 class="box-title">Income</h3>

                      <div class="box-tools pull-right">
                        <select name="bulan" id="bulan" class="select2">
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select name="tahun" id="tahun" class="select2" style="width: 100px;">
                            <option value="2020">2020</option>
                        </select>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="pendapatan"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="box box-info">
                    <div class="box-header">
                      <h3 class="box-title">Sales</h3>

                      <div class="box-tools pull-right">
                        <select name="bulan-sales" id="bulan-sales" class="select2">
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select name="tahun-sales" id="tahun-sales" class="select2" style="width: 100px;">
                            <option value="2020">2020</option>
                        </select>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="highchart-sales"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-lg-4 col-xs-6">
                <div class="box box-info">
                    <div class="box-header">
                      <h3 class="box-title">Purchase</h3>

                      <div class="box-tools pull-right">
                        <select name="bulan-purchase" id="bulan-purchase" class="select2">
                            <option value="01">Januari</option>
                            <option value="02">Februari</option>
                            <option value="03">Maret</option>
                            <option value="04">April</option>
                            <option value="05">Mei</option>
                            <option value="06">Juni</option>
                            <option value="07">Juli</option>
                            <option value="08">Agustus</option>
                            <option value="09">September</option>
                            <option value="10">Oktober</option>
                            <option value="11">November</option>
                            <option value="12">Desember</option>
                        </select>
                        <select name="tahun-purchase" id="tahun-purchase" class="select2" style="width: 100px;">
                            <option value="2020">2020</option>
                        </select>
                      </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="highchart-purchase"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <div class="row">

        </div>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script>
    var urlPendapatan = '{{ route("pendapatan") }}';
    var urlSales = '{{ route("sales") }}';
    var urlPurchase = '{{ route("purchase") }}';
    var bulan = '{{ date("m") }}';

    function thisHighchart(divId, textTitle, series, categories, textSource = 'Toko 45') {
        Highcharts.chart(divId, {
            title: {
                text: textTitle
            },

            subtitle: {
                text: textSource
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: 'Jumlah'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },

            series: series,

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

            });
    }

    function highcart(divId, type, text, categories, series) {
        Highcharts.chart(divId, {
            chart: {
                type: type
            },
            title: {
                text: text
            },
            xAxis: {
                categories: categories
            },
            yAxis: {
                title: {
                    text: ''
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: series
        });
    }

    function set_pendapatan(result) {
        highcart('pendapatan', 'line', 'Income', result.categories, result.series);
    }

    function set_sales(result) {
        thisHighchart('highchart-sales', 'Sales', result.series, result.categories);
    }

    function set_purchase(result) {
        thisHighchart('highchart-purchase', 'Purchase', result.series, result.categories);
    }

    $(document).ready(function() {
        $('.select2').select2();

        // thisHighchart('highcharts', 'Company Sales', data);
        // thisHighchart('highchart-order', 'Company Purchase', data_purchase);

        $('#bulan').on('change', function() {
            var reqData = {
                bulan : $('#bulan').val(),
                tahun : $('#tahun').val(),
            }

            ajaxData(urlPendapatan, reqData, set_pendapatan)
        });

        $('#tahun').on('change', function() {
            var reqData = {
                bulan : $('#bulan').val(),
                tahun : $('#tahun').val(),
            }

            ajaxData(urlPendapatan, reqData, set_pendapatan)
        });

        $('#bulan-sales').on('change', function() {
            var reqData = {
                bulan : $('#bulan-sales').val(),
                tahun : $('#tahun-sales').val(),
            }

            ajaxData(urlSales, reqData, set_sales)
        });

        $('#tahun-sales').on('change', function() {
            var reqData = {
                bulan : $('#bulan-sales').val(),
                tahun : $('#tahun-sales').val(),
            }

            ajaxData(urlSales, reqData, set_sales)
        });

        $('#bulan-purchase').on('change', function() {
            var reqData = {
                bulan : $('#bulan-purchase').val(),
                tahun : $('#tahun-purchase').val(),
            }

            ajaxData(urlPurchase, reqData, set_purchase)
        });

        $('#tahun-purchase').on('change', function() {
            var reqData = {
                bulan : $('#bulan-purchase').val(),
                tahun : $('#tahun-purchase').val(),
            }

            ajaxData(urlPurchase, reqData, set_purchase)
        });

        $('#bulan').val(bulan).trigger('change');
        $('#bulan-sales').val(bulan).trigger('change');
        $('#bulan-purchase').val(bulan).trigger('change');
    })
</script>
@endsection
@extends('footer')
