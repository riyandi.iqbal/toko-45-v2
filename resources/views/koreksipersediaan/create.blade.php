<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    table tr td {
        padding: 5px;
    }

    hr {
        margin : 5px;
        border-top : 1px solid #8f8f8f;
    }

    .pagination {
        margin: unset !important;
    }

    
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('KoreksiPersediaan.index')}}">Koreksi Persediaan</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Koreksi Persediaan</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Koreksi Persediaan</span>
    </div>

    <form id="form-data">
        <div class="banner">
            {{ csrf_field() }}
            <table class="table-responsive" style="width: 100%;">
                <tr>
                    <td style="width: 50%">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="<?php echo date('d/m/Y'); ?>">
                    </td>
                    <td>
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan" class="form-control"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <label for="Nomor">Nomor</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="" readonly>
                    </td>
                    <td>
                        <div style="column-count: 2">
                            <label for="IDGudang">Gudang</label>
                            <select name="IDGudang" id="IDGudang" class="form-control select2">
                                <option value="">-- Pilih Gudang --</option>
                                @foreach ($gudang as $item)
                                    <option value="{{ $item->IDGudang }}"> {{ $item->Nama_Gudang }} </option>
                                @endforeach
                            </select>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="banner">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-responsive" id="table-detail" style="font-size: 12px; margin-top: 10px; width: 100%;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th>Nama Barang</th>
                                <th style="width: 10%">Satuan</th>
                                <th style="width: 10%">Qty</th>
                                <th style="width: 15%">Harga Modal</th>
                                <th style="width: 15%">Sub Total</th>
                                <th style="width: 5%"></th>
                            </tr>
                        </thead>
                        <tbody id="list-barang">

                        </tbody>
                        <tfoot style="background-color: #16305d; color: white">
                            <tr>
                                <th colspan="2">Total</th>
                                <th> <input type="text" class="form-control" name="Total_qty" id="Total_qty" readonly> </th>
                                <th>  </th>
                                <th> <input type="text" class="form-control" name="Grand_total" id="Grand_total" readonly> </th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                    <a class="btn col-11" id="btn-add-barang" ><i class="col-11 hvr-icon-float-away">Tambah</i></a>
                </div>
                <div class="col-lg-12" style="margin-bottom: 40px;" >
                    <button class="btn btn-primary pull-right" type="submit" id="Simpan" name="Simpan" style="float: center; margin-top: 10px;">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("KoreksiPersediaan.index") }}';
    var urlNumberInvoice    = '{{ route("KoreksiPersediaan.number") }}';
    var urlInsert           = '{{ route("KoreksiPersediaan.store") }}';
    var url                 = '{{ url("KoreksiPersediaan") }}';
    var urlBarang           = '{{ route("KoreksiPersediaan.get_barang") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/koreksipersediaan/koreksipersediaan_create.js') }}"></script>

@endsection