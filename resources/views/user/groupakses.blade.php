<!-- ===========Create By Dedy 24-02-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelGroupUser')}}">Data Akses Group</a>
        </h2>
    </div>
    <div class="bannerbody container">
    <form action="{{url('GroupAksesSimpan')}}" method="POST">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="idgroup" name="idgroup" value="<?php echo $group->IDGroupUser; ?>">
        <label><?php echo $group->Group_User; ?></label><br>
        <hr>
        <table id="tblgroup" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 10px;">
            <thead style="color: #fff">
                <th>Menu</th>
                <th>Akses Group</th>
            </thead>
            <tbody>
                <tr>
                    <td>
                    <?php foreach ($menu as $k) { ?>
                                <input type="checkbox" id="idmenu_array" name="idmenu_array[]" value="<?php echo $k->IDMenuDetail; ?>">    
                                <label>Data&nbsp;<?php echo $k->Menu_Detail; ?></label><br>
                    <?php }?>
                    </td>
                    <td>
                    <?php foreach ($menurole as $l) { ?>
                        <input type="checkbox" checked id="idmenu2_array" name="idmenu2_array[]" value="<?php echo $l->IDMenuDetail; ?>">    
                        <label>Data&nbsp;<?php echo $l->Menu_Detail; ?></label><br>
                    <?php }?>
                    </td>
                </tr>
            </tbody>
        </table>
        <!-- <?php foreach ($menu as $k) { ?>
                <div class="container" style="width: 45%; float: left;">
                    
                    <input type="checkbox" id="idmenu_array" name="idmenu_array[]" value="<?php echo $k->IDMenuDetail; ?>">    
                    
                    <label>Data&nbsp;<?php echo $k->Menu_Detail; ?></label>
                </div>
        <?php }?> -->
        
        <div class="text-center" style="margin-top:20px;float:left;">
            <button class="btn btn-primary" onclick="val_submit()">Simpan</button>
            <br>
    	</div>
    </form>
        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tblgroup').DataTable({
            "searching": false,
            "info": false,
            "ordering": false,
            "lengthChange": false,
            "paging": false
        }); 

        @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif     
    })

    function val_submit()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>
@endsection