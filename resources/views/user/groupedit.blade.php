<!-- ===========Create By Dedy 14-02-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Group User</a>
        </h2>
    </div>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Group User</span>
    </div>
    <div class="banner container">
        <div class="col-md-3">
            <label class="judul">Nama Group User</label>
        </div>
        <div class="col-md-9">
            <input type="hidden" class="form-control" id="idgroup" name="idgroup" value="<?php echo $group->IDGroupUser; ?>">
            <input type="tex" class="form-control" name="nama" id="nama" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo $group->Group_User; ?>">
        <br>
        </div>
        
        <div class="text-center">
    		<div class="btn btn-success hvr-icon-float-away">
                <span style="color: white;" onclick="simpandata()">Simpan</span>
            </div>
    	</div>
    </div>
</div>
<script>
    function field()
	{
		var data1 = {
          "IDGroup"     :$('#idgroup').val(),
          "Nama"        :$('#nama').val(),       
        }
        return data1;
	}
    function simpandata()
    {
        console.log('SIMPAN DATA BEGIN');
        $("#modal-loading").fadeIn();
        var data = field();
        console.log($('#idgroup').val());
		console.log($('#nama').val());
		if($('#nama').val()==''){
            swal('Perhatian', 'Nama Masih Kosong', 'warning');
            $("#modal-loading").fadeOut();
		}else{
			console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{url('GroupUser/simpandataedit')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",  data1:data},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    $("#modal-loading").fadeOut();
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Simpan Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "{{url('ControlPanelGroupUser')}}";
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
		}	
    }
</script>
@endsection