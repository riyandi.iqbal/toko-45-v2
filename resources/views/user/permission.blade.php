<!--======================== Create by Dedy 10 Maret 2020 ========================-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Akses Data User</a>
        </h2>
    </div>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Akses Data User</span>
    </div>
    <div class="banner">
    <h3><?php echo $namauser ?></h3>
    <hr>
    <form action="{{url('PermissionSimpan')}}" method="POST">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="user" name="user" value="<?php echo $iduser ?>">
        <table style="width: 100%">
            <?php if($userakses!='') { ?>
            <tr>
                <td style="width: 30%"><label class="judul">Tambah Data</label></td>
                <td><?php if($userakses->Tambah=='yes') {?>
                    <input type="checkbox" checked id="tambah" name="tambah">
                <?php }else{ ?>
                    <input type="checkbox" id="tambah" name="tambah">
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><label class="judul">Ubah Data</label></td>
                <td><?php if($userakses->Ubah=='yes') {?>
                    <input type="checkbox" checked id="ubah" name="ubah">
                <?php }else{ ?>
                    <input type="checkbox" id="ubah" name="ubah">
                <?php } ?>
                </td>
            </tr>
            <tr>
                <td><label class="judul">Hapus Data</label></td>
                <td><?php if($userakses->Hapus=='yes') {?>
                    <input type="checkbox" checked id="hapus" name="hapus">
                <?php }else{ ?>
                    <input type="checkbox" id="hapus" name="hapus">
                <?php } ?>
                </td>
            </tr>

            <?php }else{ ?>
            <tr>
                <td style="width: 30%"><label class="judul">Tambah Data</label></td>
                <td>
                    <input type="checkbox" id="tambah" name="tambah">
                </td>
            </tr>
            <tr>
                <td><label class="judul">Ubah Data</label></td>
                <td>
                    <input type="checkbox" id="ubah" name="ubah">
                </td>
            </tr>
            <tr>
                <td><label class="judul">Hapus Data</label></td>
                <td>
                    <input type="checkbox" id="hapus" name="hapus">
                </td>
            </tr>
            <?php } ?>
        </table>
            <div class="text-center">
                <button class="btn btn-primary" onclick="val_submit()">Simpan</button>
            </div>
        </div>
    </form>
   
</div>
<script>
    function simpan()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>
@endsection