<!-- ===========Create By Dedy 24-02-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User Group</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
    <form action="{{url('GroupingUserSimpan')}}" method="POST">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="iduser" name="iduser" value="<?php echo $user->id; ?>">
        <label><?php echo strtoupper($user->name); ?></label><br>
        <hr>
        <label class="judul">Group Akses</label>
        <select data-placeholder="Pilih Group" style=" width:75%;" class="chosen-select" name="idgroup" id="idgroup" required oninvalid="this.setCustomValidity('Group Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <option value=""></option>                    
                <?php foreach ($group as $key ) { ?>
                    <option value="<?= $key->IDGroupUser ?>"> <?= $key->Group_User ?></option> 
                <?php } ?>
        </select>
        <div class="text-center" style="margin-top:20px;float:left;">
            <button class="btn btn-primary" onclick="val_submit()">Simpan</button>
            <br>
    	</div>
    </form>
    </div>
</div>
<script>
    function val_submit()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>

@endsection