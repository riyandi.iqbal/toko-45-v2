<!-- ===========Create By Dedy 14-02-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data User</a>
        </h2>
    </div>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data User</span>
    </div>
    <div class="banner container">
        <div class="col-md-3">
            <label class="judul">Nama User</label>
        </div>
        <div class="col-md-9">
            <input type="tex" class="form-control" name="nama" id="nama" onkeyup="javascript:this.value=this.value.toUpperCase();">
        <br>
        </div>
        
        <div class="col-md-3">
            <label class="judul">Username</label>
        </div>
        <div class="col-md-9">
            <input type="tex" class="form-control" name="user" id="user" onkeyup="javascript:this.value=this.value.toUpperCase()">
        <br>
        </div>

        <div class="col-md-3">
            <label class="judul">Email</label>
        </div>
        <div class="col-md-9">
            <input type="tex" class="form-control" name="email" id="email">
        <br>
        </div>

        <div class="col-md-3">
            <label class="judul">Password Default</label>
        </div>
        <div class="col-md-9">
            <input type="password" class="form-control" name="password" id="password">
        <br>
        </div>

        <div class="text-center">
    		<div class="btn btn-success hvr-icon-float-away">
                <span style="color: white;" onclick="simpan()">Simpan</span>
            </div>
    	</div>
    </div>
</div>
@endsection