<!-- ===========Create By Dedy 14-02-2020=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User</a>
        </h2>
    </div>
    
    <div class="banner">
        <div class="form_grid_3">
            <div class="btn btn-primary hvr-icon-float-away">
                <a href="{{url('RegisterUser')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
            </div>
        </div>
        <br><br>
        <table id="tblabc" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 10px;">
            <thead style="color: #fff">
    			<th>No</th>
                <th>Nama User</th>
    			<th>Group</th>
                <th>Email</th>
    			<th>Aksi</th>
    		</thead>
    		<tbody id="previewdata">
                    <?php 
                    $i=1;
                    foreach ($user as $data) { ?>
                        <tr class="odd gradeA">
                            <td><?php echo $i; ?></td>
                            <td><?php echo strtoupper($data->name); ?></td>
                            <td><?php echo $data->email; ?></td>
                            <td><?php echo $data->email; ?></td>
                            <td>
                                <a href="User/edit/<?php echo $data->id ?>" title="Edit Data">&nbsp;<i class="fa fa-pencil fa-lg"></i></a>
                                <a href="User/grouping/<?php echo $data->id ?>" title="Group User" style="color:green">&nbsp;<i class="fa fa-users fa-lg"></i></a>
                                <a href="User/permission/<?php echo $data->id ?>" title="Akses Permision" style="color:grey">&nbsp;<i class="fa fa-lock fa-lg"></i></a>
                            </td>
                        </tr>
                    <?php 
                    $i++;
                    } ?>
    		</tbody>
    	</table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tblabc').DataTable(); 

        @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif  

         @if (session('alert2'))
            swal("Berhasil", "{{ session('alert2') }}", "success");
        @endif     
    })
</script>
@endsection