<!--======================== Create by Dedy 10 Maret 2020 ========================-->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanelUser')}}">Data User</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Akses Data User</a>
        </h2>
    </div>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Akses Data User</span>
    </div>
    <div class="bannerbody">
    <h3><?php echo $namauser ?></h3>
    <hr>
    <form action="{{url('PermissionSimpan')}}" method="POST">
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <input type="hidden" id="user" name="user" value="<?php echo $iduser ?>">
        <table id="tblakses" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 10px;">
            <thead style="color: #fff">
                <th>Menu</th>
                <th>Tambah Data</th>
                <th>Ubah Data</th>
                <th>Hapus Data</th>
            </thead>
            <tbody>
                <?php foreach ($menushow as $key) { ?>
                    <tr>
                        <td><?php echo $key->Menu_Detail ?></td>
                        <td><input type="checkbox" id="tambah_<?php echo $key->IDMenuDetail ?>" name="tambah_<?php echo $key->IDMenuDetail ?>"></td>
                        <td><input type="checkbox" id="ubah_<?php echo $key->IDMenuDetail ?>" name="ubah_<?php echo $key->IDMenuDetail ?>"></td>
                        <td><input type="checkbox" id="hapus_<?php echo $key->IDMenuDetail ?>" name="hapus_<?php echo $key->IDMenuDetail ?>"></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
            <div class="text-center">
                <button class="btn btn-primary" onclick="val_submit()">Simpan</button>
            </div>
        </div>
    </form>
   
</div>
<script>
     $(document).ready(function() {
        $('#tblakses').DataTable({
            searching: false,
            ordering:false,
            paging:false,
            info:false,
        });     
    })
    function simpan()
    {
        $("#modal-loading").fadeIn();
        return true;
    }
</script>
@endsection