<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Barang')}}">Data Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url('Barang/create')}}"><span style="color: white;">Tambah Data</span></a>
                </div>
            <br>
            <br>
            </div>
            <form action="{{url('Barang/pencarian')}}" method="GET">
            <div class="col-md-3">
                <select data-placeholder="Cari Berdasarkan Group" name="jenispencarian" id="jenispencarian" class="chosen-select form-control">
                    <option value=""></option>
                    <option value="Group_Barang">Nama Group Barang</option>
                    <option value="Kode_Barang">Kode Barang</option>
                    <option value="Nama_Barang">Nama Barang</option>
                </select>
            </div>
            <div class="col-md-3">
                <select data-placeholder="Cari Berdasarkan Status" name="statuspencarian" class="chosen-select form-control">
                    <option value="aktif">Aktif</option>
                    <option value="tidak aktif">Tidak Aktif</option>
                </select>
            </div>
            <div class="col-md-3">
              <div id="pilihan">
                <input name="keyword" id="keyword" type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Cari Nama Ukuran" class="form-control" value="{{ $keyword }}">
              </div>
              <div id="select">
                <select data-placeholder="Pilih Group Barang" class="chosen-select form-control" name="keyword" id="keyword" required style="width: 100% !important">
                  <option value="">- Pilih Group Barang -</option>
                  @foreach ($groupbarang as $item)
                    <option value="{{$item->IDGroupBarang}}">{{ $item->Group_Barang }}</option>
                  @endforeach
              </select>
              </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div> 
          </form>          
    </div>
    <br>
    <div class="banner">
        <table id="tblbarang" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Group Barang</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Ukuran</th>
                    <th>Satuan</th>
                    <th>Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody id="previewdata">
            <?php if(empty($barang)){
            ?>
            <?php
            }else{
                $i=1;
                foreach ($barang as $data) {
                ?>
                <tr class="odd gradeA">
                    <td><center><?php echo $i; ?></center></td>
                    <td><?php echo $data->Group_Barang; ?></td>
                    <td><?php echo $data->Kode_Barang; ?></td>
                    <td><?php echo $data->Nama_Barang; ?></td>
                    <td><?php echo $data->Nama_Kategori; ?></td>
                    <td><?php echo $data->Nama_Ukuran; ?></td>
                    <td><?php echo $data->Satuan; ?></td>                    
                    <td><center><?php echo $data->Aktif; ?></center></td>
                    <td class="text-center ukuran-logo">
                        <?php if($data->Aktif=='aktif') { ?>
                          <span><a class="action-icons c-edit" href="edit_barang/<?php echo $data->IDBarang ?>" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>
                        <span><a data-toggle="modal" data-target="#modalHapus<?php echo $data->IDBarang ?>" title="Non Aktifkan..??"><i class="fa fa-trash-o fa-lg" style="color: red"></i></a></span>
                        <?php }else{ ?>
                          <span><a class="action-icons c-edit" title="Ubah Data" style="cursor: no-drop;"><i class="fa fa-pencil fa-lg"></i></a></span>
                        <span><a data-toggle="modal" data-target="#modalHapus2<?php echo $data->IDBarang ?>" title="Aktifkan..??"><i class="fa fa-check fa-lg" style="color: green"></i></a></span>
                        <?php } ?>  
                    </td>
                </tr>
                <?php
                $i++;
                } 
            }
            ?>
            </tbody>
        </table>
        <div style="float:right;">{{ $barang->links() }}</div>
    </div>
</div>

</div>


<!-- ===================== -->



<?php
        if (isset($barang)){ foreach($barang as $data){ ?>
            <div id="modalHapus<?php echo $data->IDBarang?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Peringatan!</h4>
                  </div>
                  <div class="modal-body">
                    <p>Yakin Akan Menonaktifkan Data ?!</p>
                    <input type="hidden" name="idcari" value="<?php echo $data->IDBarang?>">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <div class="btn hvr-icon-shrink col-10">
                        <a style="color: white;" href="hapus_barang/<?php echo $data->IDBarang ?>"><span>Non Aktifkan</span></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>

    <?php } } ?>
<?php
        if (isset($barang)){ foreach($barang as $data){ ?>
            <div id="modalHapus2<?php echo $data->IDBarang?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Peringatan!</h4>
                  </div>
                  <div class="modal-body">
                    <p>Yakin Akan Mengaktifkan Data ?!</p>
                    <input type="hidden" name="idcari" value="<?php echo $data->IDBarang?>">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <div class="btn hvr-icon-shrink col-10">
                        <a style="color: white;" href="hapus_barang/<?php echo $data->IDBarang ?>"><span>Aktifkan</span></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>

    <?php } } ?>


<script type="text/javascript">
    $(document).ready(function() {
        $('#tblbarang').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false,
            "paging" :false           
        });          
    })

    var PM  = [];
    function caridata()
    {
       
        var keyword = $('#keyword').val();
        var jenis   = $('#jenispencarian').val();
        var status  = $('#statuspencarian').val();
        $.ajax({
                  url : "{{url('Barang/pencarian')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", jenis:jenis, status:status, keyword:keyword},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        swal('Perhatian', 'Data Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];

                      if(PM[i].Aktif=='aktif'){
                        var $status  = "<a href='edit_barang/"+PM[i].IDBarang+"' data-toggle='modal'><i class='fa fa-pencil fa-lg'></i></a>&nbsp;<a data-toggle='modal' data-target='#modalHapus"+PM[i].IDBarang+"' title='Non Aktifkan..??'><i class='fa fa-trash-o fa-lg' style='color: red'></i></a>"
                      }else{
                        var $status  = "<a style='cursor: no-drop' data-toggle='modal'><i class='fa fa-pencil fa-lg'></i></a>&nbsp;<a data-toggle='modal' data-target='#modalHapus2"+PM[i].IDBarang+"' title='Aktifkan..??'><i class='fa fa-check fa-lg' style='color: green'></i></a>"  
                      }
                                        

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+PM[i].Group_Barang+"</td>"+
                      "<td>"+PM[i].Kode_Barang+"</td>"+
                      "<td>"+PM[i].Nama_Barang+"</td>"+
                      "<td>"+PM[i].Nama_Kategori+"</td>"+
                      "<td>"+PM[i].Nama_Ukuran+"</td>"+
                      "<td>"+PM[i].Satuan+"</td>"+
                      "<td>"+PM[i].Aktif+"</td>"+
                      "<td>"+$status+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }
                      
                     }
                }
            });

            
    
    }

    $(document).ready(function() {
      $('#select').hide();
      $('#jenispencarian').on('change', function() {
            if ($(this).val() == 'Group_Barang') {
                $('#pilihan').hide();
                $('#select').show();
                $('#keyword_chosen').removeAttr('style');
            } else {
              $('#pilihan').show();
                $('#select').hide();
            }
      })
    })
</script>
@endsection
