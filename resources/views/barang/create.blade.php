@extends('layouts.app')
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Barang')}}">Data Barang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Barang</span>
    </div>
    <div class="bannerbody">
        <form id="form-data">
        <div class="container">
        <br><br>
            <div class="col-md-3">
            <label class="judul">Group Barang</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Pilih Group Barang" name="IDGroupBarang" id="IDGroupBarang" style=" width:70%;" class="form-control select2" tabindex="13">
                    <option value=""></option>
                    @foreach ($groupbarang as $item)
                        <option value="{{ $item->IDGroupBarang }}" data-kode="{{ $item->Kode_Group_Barang }}" data-namagroup="{{ $item->Group_Barang }}" data-group='<?php echo json_encode(unserialize($item->Cetak_Kode)); ?>' data-pemisah_kode="{{ $item->Pemisah_Kode }}" data-panjang_kode="{{ $item->Panjang_Kode }}" data-nama='<?php echo json_encode(unserialize($item->Cetak_Nama)); ?>' data-pemisah_nama="{{ $item->Pemisah_Nama }}" data-panjang_nama="{{ $item->Panjang_Nama }}">{{ $item->Group_Barang }}</option>
                    @endforeach
                </select>
                {{-- <div class="flot-right btn btn-primary hvr-icon-float-away">
                    <a data-toggle="modal" data-target="#modaladd"><span style="color: white;">Tambah Group</span></a>
                </div> --}}
            <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Kode Barang</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Kode Barang" name="Kode_Barang" id="Kode_Barang" onkeyup="javascript:this.value=this.value.toUpperCase();" required oninvalid="this.setCustomValidity('Kode Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Nama Barang</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Nama Barang" name="Nama_Barang" id="Nama_Barang" onkeyup="javascript:this.value=this.value.toUpperCase();" required oninvalid="this.setCustomValidity('Nama Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>
                <br><br>
            </div>
            <div class="tipe col-md-3">
                <label class="judul">Type Barang</label>
            </div>
            <div class="tipe col-md-9"> 
                <select data-placeholder="Pilih Type Barang" name="IDTipe" id="IDTipe" oninvalid="this.setCustomValidity('Type Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="select2 form-control">
                    <option value=""></option>
                    @foreach ($tipe as $item)
                        <option value="{{ $item->IDTipe }}" data-kode="{{ $item->Kode_Tipe }}" data-nama="{{ $item->Nama_Tipe }}" >{{ $item->Nama_Tipe }}</option>
                    @endforeach
                </select>
                <br><br>
            </div>
            <div class="ukuran col-md-3">
                <label class="judul">Ukuran Barang</label>
            </div>
            <div class="ukuran col-md-9">
                <select data-placeholder="Pilih Ukuran Barang" name="IDUkuran" id="IDUkuran"  oninvalid="this.setCustomValidity('Ukuran Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="select2 form-control">
                    <option value=""></option>
                    @foreach ($ukuran as $item)
                        <option value="{{ $item->IDUkuran }}" data-kode="{{ $item->Nama_Ukuran }}"> {{ $item->Nama_Ukuran }} </option>
                    @endforeach
                </select>
                <br><br>
            </div>
            <div class="kategori col-md-3">
                <label class="judul">Kategori Barang</label>
            </div>
            <div class="kategori col-md-9"> 
                <select data-placeholder="Pilih Kategori Barang" name="IDKategori" id="IDKategori" oninvalid="this.setCustomValidity('Kategori Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="select2 form-control">

                </select>
                <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Satuan Barang</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Pilih Satuan Barang" name="IDSatuan" id="IDSatuan" required oninvalid="this.setCustomValidity('Satuan Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="select2 form-control">
                    <option value=""></option>
                    @foreach ($satuan as $item)
                        <option value="{{ $item->IDSatuan }}"> {{ $item->Satuan }} </option>
                    @endforeach
                </select>
                <br><br>
            </div>

            <div class="berat col-md-3">
                <label class="judul">Berat</label>
            </div>
            <div class="berat col-md-9">
                <input type="number" name="Berat" id="Berat" required oninvalid="this.setCustomValidity('Berat Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="form-control">
                <br><br>
            </div>

            <div class="berat col-md-3 stok">
                <label class="judul">Stok Minimal</label>
            </div>
            <div class="berat col-md-9 stok">
                <input type="number" name="StokMinimal" id="StokMinimal" required oninvalid="this.setCustomValidity('Berat Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="form-control">
            </div>
            <br><br><br>            
        </div>
        <div class="text-center">
            <br><br>
            <div class="btn col-11 hvr-icon-back">
                <span> <a href="{{url('Barang')}}" style="color: white;" name="simpan">Kembali</a></span>
            </div>
            <div class="btn">
                <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
            </div>
        </div>
    </form>
    </div>
</div>

</div>

<div id="modaladd" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <form method="post" action="proses_group_barang" enctype="multipart/form-data" class="form_container left_label">
        {{ csrf_field() }}
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Group Barang</h4>
        </div>
        <div class="modal-body">
        <!-- Kode Barang -->
            <label class="field_title">Kode Group Barang</label>
            <div class="form_input">
                <input type="text" class="form-control" placeholder="Kode Group Barang" name="kode" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            </div>

            <!-- Nama Barang -->
            <label class="field_title">Group Barang</label>
            <div class="form_input">
                <input type="text" class="form-control" placeholder="Nama Group Barang" name="nama" required oninvalid="this.setCustomValidity('Nama Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            </div>
        </div>
        <div class="modal-footer">
        <button type="submit" class="btn btn-success">Simpan</button>
        </div>
    </div>
    </form>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>  
    var urlKategori    = '{{ route("Barang.kategori") }}';
    var urlInsert = "{{url('Barang/simpandata')}}";
    var urlIndex = "{{url('Barang')}}";
    var format_kode = new Array();
    var format_nama = new Array();
    var urutan, urutan_nama;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/barang/create.js') }}"></script>

@endsection