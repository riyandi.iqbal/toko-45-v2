<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Barang')}}">Data Barang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Barang</span>
    </div>
    <div class="banner">
        <div class="container">
        <br><br>
        <div class="col-md-3">
          <label class="judul">Group Barang</label>
        </div>
        <div class="col-md-9">
            <select data-placeholder="Pilih Group Barang" name="group" id="group" onchange="cekgroup()" required oninvalid="this.setCustomValidity('Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:70%;" class="chosen-select" tabindex="13">
                <option value=""></option>
                <?php
                foreach ($groupbarang as $group) {
                    if ($edit->IDGroupBarang==$group->IDGroupBarang) {
                ?>
                <option value="<?php echo $group->IDGroupBarang ?>" selected><?php echo $group->Group_Barang; ?></option>
                <?php
                    } else {
                ?>
                <option value="<?php echo $group->IDGroupBarang ?>"><?php echo $group->Group_Barang; ?></option>
                <?php
                    }
                }
                ?>
            </select>
            <div class="flot-right btn btn-primary hvr-icon-float-away">
                <a data-toggle="modal" data-target="#modaladd"><span style="color: white;">Tambah Group</span></a>
            </div>
          <br><br>
        </div>
            <div class="col-md-3">
                <label class="judul">Kode Barang</label>
            </div>
            <div class="col-md-9">
                <input type="hidden" name="idbarang" id="idbarang" value="<?php echo $edit->IDBarang; ?>">
                <input type="text" class="form-control" placeholder="Kode Barang" name="kode" value="<?php echo $edit->Kode_Barang; ?>" id="kode" onkeyup="javascript:this.value=this.value.toUpperCase();"  required oninvalid="this.setCustomValidity('Kode Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <input type="hidden" name="kodebayangan" id="kodebayangan">
                <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Nama Barang</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" placeholder="Nama Barang" name="nama" id="nama" onkeyup="javascript:this.value=this.value.toUpperCase();" value="<?php echo $edit->Nama_Barang; ?>" required oninvalid="this.setCustomValidity('Nama Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" >
                <br><br>
            </div>
            <div class="tipe col-md-3">
                <label class="judul">Type Barang</label>
            </div>
            <div class="tipe col-md-9"> 
                <select data-placeholder="Pilih Type Barang" name="tipe" id="tipe" onchange="gantikode()" required oninvalid="this.setCustomValidity('Type Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="chosen-select form-control">
                    <option value=""></option>
                    <?php
                        foreach ($tipe as $tp) {
                        if ($edit->IDTipe==$tp->IDTipe) {
                    ?>
                    <option value="<?php echo $tp->IDTipe ?>" selected><?php echo $tp->Nama_Tipe; ?></option>
                    <?php
                        } else {
                    ?>
                    <option value="<?php echo $tp->IDTipe ?>"><?php echo $tp->Nama_Tipe; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
                <br><br>
            </div>
            <div class="kategori col-md-3">
                <label class="judul">Kategori Barang</label>
            </div>
            <div class="kategori col-md-9"> 
                <select data-placeholder="Pilih Kategori Barang" name="kategori" id="kategori" required oninvalid="this.setCustomValidity('Kategori Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="chosen-select form-control">
                    <option value=""></option>
                    <?php
                        foreach ($kategori as $ktg) {
                        if ($edit->IDKategori==$ktg->IDKategori) {
                    ?>
                    <option value="<?php echo $ktg->IDKategori ?>" selected><?php echo $ktg->Nama_Kategori; ?></option>
                    <?php
                        } else {
                    ?>
                    <option value="<?php echo $ktg->IDKategori ?>"><?php echo $ktg->Nama_Kategori; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
                <br><br>
            </div>
            <div class="ukuran col-md-3">
                <label class="judul">Ukuran Barang</label>
            </div>
            <div class="ukuran col-md-9">
                <select data-placeholder="Pilih Ukuran Barang" name="ukuran" id="ukuran" onchange="gantinama()" required oninvalid="this.setCustomValidity('Ukuran Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="chosen-select form-control">
                    <option value=""></option>
                    <?php
                        foreach ($ukuran as $ukr) {
                        if ($edit->IDUkuran==$ukr->IDUkuran) {
                    ?>
                    <option value="<?php echo $ukr->IDUkuran ?>" selected><?php echo $ukr->Nama_Ukuran; ?></option>
                    <?php
                        } else {
                    ?>
                    <option value="<?php echo $ukr->IDUkuran ?>"><?php echo $ukr->Nama_Ukuran; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
                <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Satuan Barang</label>
            </div>
            <div class="col-md-9">
                <select data-placeholder="Pilih Satuan Barang" name="satuan" id="satuan" required oninvalid="this.setCustomValidity('Satuan Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="chosen-select form-control">
                    <option value=""></option>
                    <?php
                        foreach ($satuan as $stn) {
                        if ($edit->IDSatuan==$stn->IDSatuan) {
                    ?>
                    <option value="<?php echo $stn->IDSatuan ?>" selected><?php echo $stn->Satuan; ?></option>
                    <?php
                        } else {
                    ?>
                    <option value="<?php echo $stn->IDSatuan ?>"><?php echo $stn->Satuan; ?></option>
                    <?php
                        }
                    }
                    ?>
                </select>
                <br><br>
            </div>
            <div class="berat col-md-3">
                <label class="judul">Berat</label>
            </div>
            <div class="berat col-md-9">
                <input type="text" class="form-control" placeholder="Berat Barang" name="berat" id="berat" value="<?php echo $edit->Berat; ?>" required oninvalid="this.setCustomValidity('Berat Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
            </div>

            <div class="col-md-3">
                <label class="judul">Stok Minimal</label>
            </div>
            <div class="col-md-9">
                <input type="number" name="stokminimal" id="stokminimal" value="<?php echo $edit->StokMinimal; ?>"  required oninvalid="this.setCustomValidity('Berat Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" class="form-control">
            </div>

            
        </div>
        <div class="text-center">
            <br><br>
            <div class="btn col-11 hvr-icon-back">
                <span> <a href="{{url('Barang')}}" style="color: white;" name="simpan">Kembali</a></span>
            </div>
            <div class="btn btn-success hvr-icon-float-away">
                <span style="color: white;" onclick="simpan()">Simpan</span>
            </div>
        </div>
    </div>
</div>

</div>

            <div id="modaladd" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <form method="post" action="proses_group_barang" enctype="multipart/form-data" class="form_container left_label">
                    {{ csrf_field() }}
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Group Barang</h4>
                  </div>
                  <div class="modal-body">
                    <!-- Kode Barang -->
                        <label class="field_title">Kode Group Barang</label>
                        <div class="form_input">
                            <input type="text" class="form-control" placeholder="Kode Group Barang" name="kode" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        </div>

                        <!-- Nama Barang -->
                        <label class="field_title">Group Barang</label>
                        <div class="form_input">
                            <input type="text" class="form-control" placeholder="Nama Group Barang" name="nama" required oninvalid="this.setCustomValidity('Nama Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
                </form>
              </div>
            </div>


<script type="text/javascript">
    $(document).ready(function() {

		cekgroup();
		
	})
    function cekgroup()
    {
      console.log($('#group').val());
      if($('#group').val()=='P000004'){
        //document.getElementById("kode").value = '';
        //document.getElementById("nama").value = '';
        document.getElementById("kode").readOnly = true;
        document.getElementById("nama").readOnly = true;
        $(".tipe").show();
        $(".kategori").show();
        $(".ukuran").show();
        $(".berat").show();
      }else if($('#group').val()=='P000003'){
        document.getElementById("kode").readOnly = true;
        document.getElementById("nama").readOnly = false;
        $(".tipe").hide();
        $(".kategori").hide();
        $(".ukuran").hide();
        $(".berat").hide();
      }else if($('#group').val()=='P000005'){
        document.getElementById("kode").readOnly = true;
        document.getElementById("nama").readOnly = false;
        $(".tipe").hide();
        $(".ukuran").hide();
        $(".berat").hide();
        $(".stok").show();
      }else{
        document.getElementById("kode").readOnly = false;
        document.getElementById("nama").readOnly = false;
        $(".tipe").hide();
        $(".kategori").hide();
        $(".ukuran").hide();
        $(".berat").hide();
      }
    }
    function gantikode()
    {
        console.log($('#tipe').val());
        var datatipe = $('#tipe').val();
                $.ajax({
                  url : "{{url('Barang/ambiltipe')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data:datatipe},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    console.log(data[0].Nama_Tipe);
                    var kode = data[0].Kode_Tipe;
                    var spara = '-';
                    $('#kode').val(kode+spara);
                    $('#kodebayangan').val(data[0].IDTipe);
                    $('#nama').val(data[0].Nama_Tipe);
                    gantinama();
                    caricode();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
    }

    function caricode()
    {       
        var datacode = $('#kodebayangan').val();
            $.ajax({
                  url : "{{url('Barang/ambilcode')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data:datacode},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    console.log('===========CODE HITUNGAN===========');
                    console.log(data[0].code);

                        if(data[0].code==''){
                          var urutan_id = '01';
                        }else{
                          if(data[0].code <= 9){
                                $hasil = data[0].code + 1;
                                var urutan_id = '0'+$hasil;
                          }else{
                                $hasil = data[0].code + 1;
                                var urutan_id = $hasil;
                          }  
                          
                        }
                    console.log('===========HASIL HITUNGAN===========');
                    console.log(urutan_id);
                    var kode1 = $('#kode').val();
                    $('#kode').val(kode1+urutan_id);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal('Gagal', 'Error Code', 'warning');
                 }
            });
    }

    function gantinama()
    {
        $("#modal-loading").fadeIn();
        console.log($('#ukuran').val());
        var dataukuran = $('#ukuran').val();
            $.ajax({
                  url : "{{url('Barang/ambilukuran')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data:dataukuran},
                  dataType:'json',
                  success: function(data)
                  { 
                    $("#modal-loading").fadeOut();
                    console.log('----------Ajax berhasil-----');
                    console.log(data[0].Nama_Ukuran);
                    var nama = data[0].Nama_Ukuran;
                    var bfore = $('#nama').val();
                    var filter = bfore.slice(0, 5);
                   console.log(filter);
                    var spara = ' ';
                    $('#nama').val(filter+spara+nama);
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
            });
    }


    function field()
    {
        var data1 = {
          "Groupbarang"         :$('#group').val(),
          "Kodebarang"          :$('#kode').val(), 
          "Namabarang"          :$('#nama').val(),
          "Kategori"            :$('#kategori').val(),
          "Ukuran"              :$('#ukuran').val(),
          "Satuan"              :$('#satuan').val(),
          "Tipe"                :$('#tipe').val(),
          "IDBarang"            :$('#idbarang').val(), 
          "Berat"               :$('#berat').val(), 
          "StokMinimal"         :$('#stokminimal').val(),    
        }
        return data1;
    }

    function simpan()
    {
        $("#modal-loading").fadeIn();
        var data = field();
        
            console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{url('Barang/simpandataedit')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data1:data},
                  dataType:'json',
                  beforeSend: function(data) {
                    $('#modal-loading').fadeIn('fast');
                  },
                  success: function(data)
                  { 
                   console.log(data);
                   $("#modal-loading").fadeOut();
                   if (data.status === false) {
                        swal({
                            title: "",
                            text: data.message,
                            icon: "error",
                            confirmButtonColor: "#254283"
                        });
                    } else {
                        swal({
                          title: "Berhasil",
                          text: "Berhasil Simpan Data",
                          icon: "success"
                      }).then(function() {
                          window.location = "{{url('Barang')}}";
                      });
                    }
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    $("#modal-loading").fadeOut();
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
              
    }
</script>
@endsection
