<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<style>
  span a .fa {
      cursor: pointer;
      padding: 5px;
  }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Barang')}}">Data Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url('Barang/create')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div>
            <br>
            <br>
            </div>
            
            <div class="col-md-3">
                <select data-placeholder="Cari Berdasarkan Group" name="field" class="chosen-select form-control" id="field">
                    <option value=""></option>
                    <option value="tbl_barang.IDGroupBarang">Nama Group Barang</option>
                    <option value="Kode_Barang">Kode Barang</option>
                    <option value="Nama_Barang">Nama Barang</option>
                </select>
            </div>
            <div class="col-md-3">
              <div id="pilihan">
                    <input name="keyword" id="keyword" type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Cari Nama Ukuran" class="form-control" >
              </div>
              <div id="select">
                <select data-placeholder="Pilih Group Barang" class="chosen-select form-control" name="keyword" id="keyword1" required style="width: 100% !important">
                  <option value="">- Pilih Group Barang -</option>
                  @foreach ($groupbarang as $item)
                    <option value="{{$item->IDGroupBarang}}">{{ $item->Group_Barang }}</option>
                  @endforeach
              </select>
              </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>   
    </div>
    <br>
    <div class="banner">
        <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Group Barang</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Kategori</th>
                    <th>Ukuran</th>
                    <th>Satuan</th>
                    <th>Stok Minimal</th>
                    <th>Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/barang/index.js') }}"></script>
<script>
    var urlData = '{{ route("Barang.datatable") }}';
    var url                 = '{{ url("Barang") }}';
</script>
@endsection
