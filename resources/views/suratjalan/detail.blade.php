@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('suratjalan')}}">Data Surat Jalan</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Surat Jalan - {{ $surat_jalan->nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table table-bordered" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" >Tanggal</td>
                            <td width="65%"> {{ AppHelper::DateIndo($surat_jalan->tanggal) }} </td>
                        </tr>
                        <tr>
                            <td>Nomor Surat Jalan</td>
                            <td> {{ $surat_jalan->nomor }} </td>
                        </tr>
                        <tr>
                            <td>Nomor Sales Order</td>
                            <td> {{ $surat_jalan->nomorso }} </td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td> {{ $surat_jalan->IDCustomer . ' - ' . $surat_jalan->Nama }} </td>
                        </tr>
                        {{-- <tr>
                            <td width="35%">Tanggal Jatuh Tempo</td>
                            <td width="65%"> {{ AppHelper::DateIndo($surat_jalan->jatuhtempo) }} </td>
                        </tr> --}}
                        <tr>
                            <td>Keterangan</td>
                            <td> {{ $surat_jalan->keterangan }} </td>
                        </tr>
                        @if ($surat_jalan->statusppn == 'exclude')
                        <tr>
                            <td>Status PPN</td>
                            <td> {{ $surat_jalan->statusppn }} </td>
                        </tr>
                        @endif
                        <tr>
                            <td>Status</td>
                            <td> {{ ($surat_jalan->Batal == 0 || $surat_jalan->Batal == null ) ? 'Aktif' : 'Batal'  }} </td>
                        </tr>
                        {{-- <tr>
                            <td>Total Qty</td>
                            <td> {{ $surat_jalan->qty }} </td>
                        </tr>
                        <tr>
                            <td>Total Harga</td>
                            <td> {{ AppHelper::NumberFormat($surat_jalan->grantotal) }} </td>
                        </tr>
                        <tr>
                            <td>PPN</td>
                            <td> {{ AppHelper::NumberFormat($surat_jalan->nilaippn, 2) }} </td>
                        </tr> --}}
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            {{-- <th>Harga</th> --}}
                            <th>Satuan</th>
                            {{-- <th>PPN % </th>
                            <th>Total</th> --}}
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($surat_jalan_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->qty }} </td>
                                {{-- <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->hargasatuan) }} </td> --}}
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                {{-- <td style="text-align: center;"> {{ ($surat_jalan->statusppn == 'include') ? 0 : 10 }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->subtotal) }} </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('SuratJalan.index') }}" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('SuratJalan.print', $surat_jalan->idsjc) }}" target="__blank">Print Data</a></span>
                    </div>
                    <div class="btn col-1">
                      <span><a style="color: white;" href="{{ route('SuratJalan.show', $surat_jalan->idsjc) }}">Edit Data</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection