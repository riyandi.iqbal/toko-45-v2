<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('SuratJalan')}}">Data Surat Jalan</a>
        </h2>
    </div>
    <div class="banner container" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="form_grid_3">
            <a href="{{url('SuratJalan/create')}}">
                <div class="btn btn-primary hvr-icon-float-away">
                    <span style="color: white;">Tambah Data</span>
                </div>
            </a>
        </div>
        <br>
        <div class="col-md-2">
            <label class="field_title mt-dot2">Periode</label>
                <div class="form_input">
                    <input type="date" class="form-control" name="date_from" id="date_from" value="">
                </div>
        </div>
        <div class="col-md-2">
            <label class="field_title mt-dot2"> S/D </label>
            <div class="form_input">
                <input type="date" class="form-control" name="date_until" id="date_until" value="{{ date('Y-m-t', strtotime(date('Y-m-d'))) }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">.</label>
                <input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form_grid_2">
                <label>&nbsp;.</label>
                <br>
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>
        </div>
    </div>
    <div class="banner">
        <div class="widget_content">
        <button onclick="exlin()" class="btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;EXCEL</button>
        <br><br>
            <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor Surat Jalan</th>
                        <th>Nama Customer</th>
                        <th>Nama Barang</th>
                        <th>Qty</th>
                        <th>Harga</th>
                        <th>Total Harga</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("SuratJalan.datatable_detail") }}';
    var url = '{{ url("SuratJalan") }}';
    function exlin()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#field').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

       window.location.replace("{{url('LaporanSuratjalan/toexcel/?date1=')}}"+date1+'&date2='+date2+'&jenis='+jenis+'&keyword='+keyword);
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/suratjalan/suratjalan_laporan.js') }}"></script>

@endsection