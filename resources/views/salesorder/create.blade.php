<!-- ===========Create By Tiar 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
        font-size: 14px;
    }
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('SalesOrder.index')}}">Data Sales Order</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Sales Order</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Sales Order</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td>
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="<?php echo date('d/m/Y'); ?>">
                        <br>
                        <label for="Nomor">Nomor SO</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No PO">
                        <br>
                        <label for="Grand_total">Grand Total</label>
                        <input type="text" name="Grand_total" id="Grand_total" class="form-control" placeholder="Grand Total Harga" readonly required>
                    </td>
                    <td>
                        
                        <label for="IDCustomer">Customer</label><br>
                        <select data-placeholder="Cari Customer" class="form-control select2" name="IDCustomer" id="IDCustomer" required style="width: 55%">
                            <option value="">- Pilih -</option>
                            <?php foreach ($customer as $row) {
                                echo "<option value='$row->IDCustomer' data-idgroup='$row->IDGroupCustomer'>$row->Nama</option>";
                            }
                            ?>
                        </select>
                        <div class="group">
                            <select data-placeholder="Group Customer" class="form-control select2" name="IDGroupCustomer" id="IDGroupCustomer" required style="width: 40%; float: right; display: none;">
                                <option value="">- Group Customer -</option>
                                <?php foreach ($groupcustomer as $row) {
                                    echo "<option value='$row->IDGroupCustomer'>$row->Nama_Group_Customer</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <br><br>
                        <label for="IDMataUang">Mata Uang</label><br>
                        <select data-placeholder="Cari Kurs" class="chosen-select" style="width: 55%" name="IDMataUang" id="IDMataUang">
                            <option value="">-- Pilih Mata Uang --</option>
                            @foreach ($mata_uang as $item)
                                <option value="{{ $item->IDMataUang }}" data-kurs="{{ $item->Kurs }}" > {{ $item->Mata_uang }} </option>
                            @endforeach
                        </select>
                        <input type="text" name="Kurs" id="Kurs" style="width: 40%; float: right;" readonly class="form-control" placeholder="Kurs">   
                        <br><br>
                        <label for="Total_qty">Total</label>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" style="width: 25%" placeholder="Total Qty" readonly required>
                        <br>
                        <label for="Status_ppn">Jenis PPN</label><br>
                        <input type="radio" name="Status_ppn" id="ppn1" value="include" checked> Include PPN&nbsp;
                        <input type="radio" name="Status_ppn" id="ppn0" value="exclude"> Exclude PPN                 
                    </td>
                    <td>
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"></textarea>
                        <br>
                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 50px;">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                        <li class="active"><a href="#tab-barang" data-toggle="tab">Input Barang</a></li>
                        <li><a href="#tab-pembayaran" data-toggle="tab">Pembayaran</a></li>
                    </ul>
                </div>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="tab-barang">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <table class="table table-bordered" id="table-barang" style="font-size: 12px;">
                                <thead style="background-color: #16305d; color: white">
                                <tr>
                                    <th style="width: 25%">Nama Barang</th>
                                    <th style="width: 10%">Qty</th>
                                    <th style="width: 15%">Satuan</th>
                                    <th style="width: 15%">Harga</th>
                                    <th style="width: 5%">PPN %</th>
                                    <th style="width: 15%">Subtotal</th>
                                    <th style="width: 2%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="list-barang">

                                </tbody>
                            </table>
                            <a class="btn col-11" id="btn-add-barang" ><i class="col-11 hvr-icon-float-away">Tambah</i></a>
                            <div class="btn col-16">
                                <span> <a style="color: white; float: right;" href="{{ route('SalesOrder.index') }}">Kembali</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-pembayaran">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Pembayaran</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <div class="col-md-6">
                                <label>Jenis Pembayaran</label>
                                <select name="Jenis_Pembayaran" id="Jenis_Pembayaran" class="form-control">
                                    <option value="">-- Pilih Jenis Pembayaran --</option>
                                    <?php foreach ($cara_bayar as $cb) { ?>
										<option value="<?php echo strtolower($cb->Nama) ?>"><?php echo $cb->Nama ?></option>
									<?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nama COA</label>
                                <select name="IDCoa" id="IDCoa" class="form-control">
                                    <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nomor Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control" name="Nomor_giro" id="Nomor_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Tanggal Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control datepicker" name="Tanggal_giro" id="Tanggal_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Pembayaran</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="pembayaran" id="pembayaran" >
                            </div>
                            <div class="col-md-12">
                            <button class="btn btn-info" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex    = '{{ route("SalesOrder.index") }}';
    var urlNumberSO = '{{ route("SalesOrder.number") }}';
    var urlInsert = '{{ route("SalesOrder.store") }}';
    var urlBarang = '{{ route("SalesOrder.get_barang") }}';
    var urlSatuan = '{{ route("SalesOrder.get_satuan") }}';
    var urlHargaJual = '{{ route("SalesOrder.get_harga_jual") }}';
    var urlCoa = '{{ route("SalesOrder.get_coa") }}';
    var url     = '{{ url("SalesOrder") }}';
    var data_barang = <?php echo json_encode($barang); ?>;
    function ambilnomorsononppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR SO');
            $.ajax({
                    url : "{{url('ambilnomorsononppn')}}",
                    type: "GET",
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU SO');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }

    function ambilnomorsoppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR SO PPN');
            $.ajax({
                    url : "{{url('ambilnomorsoppn')}}",
                    type: "GET",
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU SO PPN');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/salesorder/salesorder_create.js') }}"></script>

@endsection