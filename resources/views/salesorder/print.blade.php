
<style type = "text/css">
    .printkertas {
       size: 8.5in 11in;  /* width height */
    }

</style>
<?php
    use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
    <div class="banner">
        <div class="headerprint">
            <button class="btn btn-primary" id="printgo" onclick="printgo()">Print</button>
            <a href="{{route('SalesOrder.index')}}"><button class="btn btn-primary">Close</button></a>
        </div>
        <div class="printkertas" id="printkertas">
            <div class="bodyprint" id="bodyprint">
                <br>
                <table width="90%">
                    <tr>
                        <td>
                            <div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
                      </td>
                    </tr>
                </table>
                <hr>
                <br>
                <table width="90%">
                    <tr>
                        <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="7">SALES ORDER</td>
                    </tr>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><?php echo $sales_order->Nomor; ?></td>
                        <td>&nbsp;</td>
                        <td>Grand Total</td>
                        <td>:</td>
                        <td style="text-align: right;"><?php echo $kurs->Kode . AppHelper::NumberFormat($sales_order->Grand_total, $angkakoma); ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo date("d M Y", strtotime($sales_order->Tanggal)); ?></td>
                        <td>&nbsp;</td>
                        <td>Total Qty</td>
                        <td>:</td>
                        <td style="text-align: right;"><?php echo $sales_order->Total_qty; ?></td>
                    </tr>
                    <tr>
                        <td>Mata Uang</td>
                        <td>:</td>
                        <td><?php echo $kurs->Mata_uang; ?></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br>
                <table width="90%" class="table table-bordered">
                        <thead>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>Subtotal</th>
                        </thead>
                        <tbody>
                            @foreach ($sales_order_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                    <td style="text-align: center;"> {{ $item->Qty }} </td>
                                    <td style="text-align: right;"> {{ $kurs->Kode . AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                    <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                    <td style="text-align: right;"> {{ $kurs->Kode . AppHelper::NumberFormat($item->Sub_total, $angkakoma) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                <table width="90%">
                    <tr>
                        <td width="50%">Keterangan : </td>						
                        <td>&nbsp;</td>
                        <td>Jenis Pembayaran</td>
                        <td>:</td>
                        <td>{{ $um_customer ?  strtoupper($um_customer->Jenis_Pembayaran ) : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">{{ $sales_order->Keterangan }}</td>
                        <td>Nama COA</td>
                        <td>:</td>
                        <td>{{ $um_customer ? strtoupper($um_customer->Nama_COA ) : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">&nbsp;</td>
                        <td>Jumlah Pembayaran</td>
                        <td>:</td>
                        <td>{{ $um_customer ? $kurs->Kode . AppHelper::NumberFormat($um_customer->Pembayaran, $angkakoma) : '' }}</td>
                    </tr>
                </table>
            </div>
        </div>		
    </div>	
</div>

<script>
    window.load = print_d('printkertas')
    function print_d(elem){
        var mywindow = window.open('', 'PRINT', 'height=600,width=800');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>