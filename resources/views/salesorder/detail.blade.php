<!-- ===========Create By Tiar 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('SalesOrder')}}">Data Sales Order</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Sales Order - {{ $sales_order->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                            <td width="65%" style="background-color: #e5eff0; border: 1px solid; "> {{  AppHelper::DateIndo($sales_order->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $sales_order->Nomor }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Customer</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ $customer->Nama }} </td>
                        </tr>

                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $sales_order->Keterangan }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Status</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($sales_order->Batal == 0 || $sales_order->Batal == null ) ? 'Aktif' : 'Batal'  }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Total Qty</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $sales_order->Total_qty }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Total Harga</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($sales_order->Grand_total, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Mata Uang</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $kurs->Mata_uang }} </td>
                        </tr>
                        @if ($sales_order->Status_ppn == 'exclude')
                            {{-- <tr>
                                <td style="background-color: #e5eff0; border: 1px solid;">Status PPN</td>
                                <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($sales_order->Status_ppn) }} </td>
                            </tr> --}}
                            <tr>
                                <td style="background-color: #e5eff0; border: 1px solid;">Nilai PPN</td>
                                <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($sales_order->PPN, $angkakoma) }} </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>PPN % </th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($sales_order_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                <td style="text-align: center;"> {{ ($sales_order->Status_ppn == 'include') ? 0 : 10 }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Sub_total, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                    @if ($um_customer)
                        
                    @endif
                    <tfoot style="background-color: #16305d; color: #fff">
                        <tr>
                            <th colspan="6" style="text-align: right;">Jenis Pembayaran</th>
                            <th>{{ $um_customer ?  strtoupper($um_customer->Jenis_Pembayaran ) : '-' }}</th>
                        </tr>
                        <tr>
                            <th colspan="6" style="text-align: right;">Pembayaran Via</th>
                            <th>{{ $um_customer ?  strtoupper($um_customer->Nama_COA ) : '-' }}</th>
                        </tr>
                        <tr>
                            <th colspan="6" style="text-align: right;">Nilai UM</th>
                            <th>{{ $um_customer ? AppHelper::NumberFormat($um_customer->Pembayaran) : '-' }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('SalesOrder.index') }}" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('SalesOrder.print', $sales_order->IDSOK) }}" target="__blank">Print Data</a></span>
                    </div>
                    <div class="btn col-1">
                      <span><a style="color: white;" href="{{ route('SalesOrder.show', $sales_order->IDSOK) }}">Edit Data</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection