<!-- ===========Create By Tiar 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('SalesOrder')}}">Data Sales Order</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Sales Order - {{ $sales_order->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <form id="form-data">
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <input type="hidden" name="IDSOK" id="IDSOK" value="{{ $sales_order->IDSOK }}">
                    <tbody>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                            <td width="65%" style="background-color: #e5eff0; border: 1px solid; "> {{ $sales_order->Tanggal }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $sales_order->Nomor }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Customer</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ $customer->Nama }} </td>
                        </tr>

                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $sales_order->Keterangan }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Status</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($sales_order->Batal == 0 || $sales_order->Batal == null ) ? 'Aktif' : 'Batal'  }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Total Qty</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> 
                                <input placeholder="" type="text" name="Total_qty" id="Total_qty" class="form-control" value="{{ $sales_order->Total_qty }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Total Harga</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> 
                                <input placeholder="" type="text" name="Grand_total" id="Grand_total" class="form-control" value="{{ AppHelper::NumberFormat($sales_order->Grand_total, $angkakoma) }}" readonly>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Mata Uang</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $kurs->Mata_uang }} </td>
                        </tr>
                        @if ($sales_order->Status_ppn == 'exclude')
                            <tr>
                                <td style="background-color: #e5eff0; border: 1px solid;">Status PPN</td>
                                <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($sales_order->Status_ppn) }} </td>
                            </tr>
                            <tr>
                                <td style="background-color: #ffffff; border: 1px solid;">Nilai PPN</td>
                                <td style="background-color: #ffffff; border: 1px solid;"> {{ AppHelper::NumberFormat($sales_order->PPN, $angkakoma) }} </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th style="width: 8%;">Qty Disetujui</th>
                            <th style="width: 10%;">Harga</th>
                            <th>Satuan</th>
                            <th style="width: 8%;">PPN % </th>
                            <th style="width: 15%;">Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($sales_order_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} 
                                    <input type="hidden" class="form-control" value="{{ ($item->IDSOKDetail) }}" name="IDSOKDetail[]">
                                </td>
                                <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty }} </td>
                                <td style="text-align: center;"> 
                                    <input type="text" class="form-control qty" value="{{ $item->Qty }}" name="Qty_disetujui[]">
                                </td>
                                <td style="text-align: right;"> <input type="text" class="form-control price text-right" value="{{ AppHelper::NumberFormat($item->Harga) }}" name="Harga[]"> </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                <td style="text-align: center;"> 
                                    <input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="{{ ($sales_order->Status_ppn == 'include') ? 0 : 10 }}" readonly>
                                </td>
                                <td style="text-align: right;"> 
                                    <input type="text" class="form-control subtotal text-right" value="{{ AppHelper::NumberFormat($item->Sub_total) }}" name="Sub_total[]" readonly>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    @if (is_null($sales_order->Persetujuan))
                            <div class="btn col-5 hvr-icon fa fa-refresh">
                                <span><a style="color: white;" href="{{ route('PersetujuanSalesOrder.rejected', $sales_order->IDSOK) }}">Tolak</a></span>
                            </div>
                            <div class="btn">
                                <button type="submit" class="btn col-1 hvr-icon fa fa-check" style="color: white;">Setujui</button>
                            </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </form>
    <br><br><br>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script type="text/javascript">    
    var urlApproved = '{{ route("PersetujuanSalesOrder.approved") }}';
    var urlSalesOrder = '{{ url("SalesOrder") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/salesorder/salesorder_persetujuan_detail.js') }}"></script>
@endsection