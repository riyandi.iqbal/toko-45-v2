@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
        <div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="home" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="Bank">Data Bank</a>
		</h2>
	</div>
	<br>
	<div class="banner container">
		{{csrf_field()}}
		<div class="col-md-12">
			<div class="btn btn-primary hvr-icon-float-away">
                <a href="Bank/create"><span style="color: white;">Tambah&nbsp;&nbsp;</span></a>
        	</div>
		</div>
		<div class="col-md-3">
			<br>
			<select data-placeholder="Cari Berdasarkan" name="field" id="field" class="chosen-select form-control" style="width: 100%!important" tabindex="13">
                <option value=""></option>
				        <option value="Atas_Nama">Atas Nama</option>
                <option value="Nomor_Rekening">Nomor Rekening</option>
                <option value="tbl_coa.Nama_COA">Nama COA</option>
            </select>
		</div>
		<div class="col-md-3">
			<br>
			<input name="keyword" id="keyword" type="tex" class="form-control">
		</div>
		<div class="col-md-3">
			<br>
			<button class="btn btn-primary"  id="cari-data" name="" value="search">Search</button> 
		</div>
	
	</div>
	<br>
	<div class="banner">
		<table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
        <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Atas Nama</th>
                    <th>No Rekening</th>
                    <th>Nama COA</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
		    </table>
	</div>
</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/bank/index.js') }}"></script>
<script>
    var urlData = '{{ route("Bank.datatable") }}';
    var url                 = '{{ url("Bank") }}';
</script>
@endsection 