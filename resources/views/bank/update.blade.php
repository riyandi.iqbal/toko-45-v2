@extends('layouts.app')   
@section('content')

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url ('home') }}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url ('Bank') }}">Data Bank</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Bank</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Bank</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Nomor Rekening</label>
            </div>
            <div class="col-md-9">
            <input type="text" class="form-control" id="no_rek" name="no_rek" placeholder="Nomor Rekening" value="<?php echo $banks->Nomor_Rekening ?>" required oninvalid="this.setCustomValidity('Nomor Rekening Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
            <input type="hidden" id="idbank" name="idbank" value="<?php echo $banks->IDBank ?>">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Atas Nama</label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="an" id="an" placeholder="Atas Nama" value="<?php echo $banks->Atas_Nama ?>" required oninvalid="this.setCustomValidity('Atas Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih COA" name="coa" id="coa" class="form-control" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                                <option value=""> </option>
                                                <?php foreach ($coa as $row) : ?>
                                                    <option value="<?php echo $row->IDCoa ?>" <?php echo $row->IDCoa == $banks->IDCoa ? 'selected' : '' ?>><?php echo $row->Kode_COA . '-' . $row->Nama_COA ?></option>
                                                <?php endforeach; ?>
                                            </select>
                </div>

            </div>
            <br><br><br>
            <div class="text-center">
                <div class="btn col-11 hvr-icon-back">
                        <span> <a style="color: white;" href="{{ url ('Bank') }}" name="simpan">Kembali</a></span>
                    </div>

                <input type="submit" name="simtam" class="btn btn-primary" value="Simpan Data">
            </div>
        </form>
    </div>
</div>

</div>

<script type="text/javascript">
    function field()
    {
        var data1 = {
          "IDBank"          :$('#idbank').val(),
          "Norek"           :$('#no_rek').val(),
          "An"              :$('#an').val(),
          "COA"             :$("#coa").val(),        
        }
        return data1;
    }

    function simpan()
    {
        var data = field();
            console.log('----------- Prosessimpanedit---------------')
            $.ajax({

                  url : "{{url ('Bank/simpanedit') }}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data1:data },
                  dataType:'json',
                  beforeSend: function(data) {
                    $('#modal-loading').fadeIn('fast');
                  },
                  

                  success: function(response)
                  { 
                    console.log('----------Ajax berhasil-----');
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Edit Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "{{url ('Bank') }}";
                    });
                  },
                  error: function ()
                  {
                    swal('Gagal', 'Data Gagal Di Edit', 'warning');
                 }
               });
             
    }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Bank") }}';
  var urlUpdate           = '{{ route("Bank.update_data") }}';
  var url                 = '{{ url("Bank") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/bank/update.js') }}"></script>
@endsection  