@extends('layouts.app')   
@section('content')

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{ url ('home') }}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url ('Bank') }}">Data Bank</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Bank</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Bank</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            {{csrf_field()}}
            <br><br>
            <div class="col-md-3">
            <label class="judul">Nomor Rekening</label>
            </div>
            <div class="col-md-9">
            <input type="text" class="form-control" name="no_rek" id="no_rek" placeholder="Nomor Rekening" required oninvalid="this.setCustomValidity('Nomor Rekening Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Atas Nama</label>
                </div>
                <div class="col-md-9">
                    <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="an" id="an" placeholder="Atas Nama" required oninvalid="this.setCustomValidity('Atas Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih COA" name="coa" id="coa" class="form-control" required oninvalid="this.setCustomValidity('COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chzn-select" tabindex="13">
                                                <option value=""> </option>
                                                <?php foreach ($coa as $row) : ?>
                                                    <option value="<?php echo $row->IDCoa ?>"><?php echo $row->Kode_COA . '-' . $row->Nama_COA ?></option>
                                                <?php endforeach; ?>
                                            </select>
                </div>

            </div>
            <br><br><br>
            <div class="text-center">
                <div class="btn col-11 hvr-icon-back">
                    <span> <a style="color: white;" href="{{ url ('Bank') }}" name="simpan">Kembali</a></span>
                </div>
            <input type="submit" name="simtam" class="btn btn-primary" value="Simpan Data">
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Bank") }}';
  var urlInsert           = '{{ route("Bank.store") }}';
  var url                 = '{{ url("Bank") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/bank/create.js') }}"></script>
@endsection  
