<!-- ===========Create By Dedy 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset->Angkakoma) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{route('PenerimaanPiutang.index')}}">Data Penerimaan Piutang</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Penerimaan Piutang - {{ $penerimaan_piutang->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="bannerbody">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                            <td width="65%" style="background-color: #e5eff0; border: 1px solid; "> {{ AppHelper::DateIndo($penerimaan_piutang->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $penerimaan_piutang->Nomor }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Customer</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ $penerimaan_piutang->Nama }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nominal Pembayaran</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ AppHelper::NumberFormat($penerimaan_piutang->Pembayaran, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Kelebihan Bayar</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($penerimaan_piutang->Kelebihan_bayar, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $penerimaan_piutang->Keterangan }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th style="">No. Faktur</th>
                            <th style="">Tanggal Faktur</th>
                            <th style="">JT Faktur</th>
                            <th>Nilai Faktur</th>
                            <th style="">Telah Dibayar</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($penerimaan_piutang_detail as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->No_Faktur }} </td>
                                <td> {{ AppHelper::DateIndo($item->Tanggal_Piutang) }} </td>
                                <td> {{ $item->Jatuh_Tempo ? AppHelper::DateIndo($item->Jatuh_Tempo) : '-' }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Nilai_fj, $angkakoma) }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Pembayaran, $angkakoma) }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Saldo_Akhir, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th style="">Jenis Pembayaran</th>
                            <th style="">COA</th>
                            <th style="">Tanggal Giro</th>
                            <th style="">Nomor Giro</th>
                            <th style="">Nominal</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($penerimaan_piutang_bayar as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->Jenis_pembayaran }} </td>
                                <td> {{ $item->Nama_COA }} </td>
                                <td> {{ $item->Nomor_giro }} </td>
                                <td> {{ $item->Tanggal_giro }} </td>
                                <td style="text-align: right;"> {{ $item->Kode . AppHelper::NumberFormat($item->Nominal_pembayaran, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('PenerimaanPiutang.index') }}" name="simpan">Kembali</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection