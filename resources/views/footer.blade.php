<style type="text/css">
	*{margin:0px auto;}
	#container{
	    position:relative;
	}
	#footer{
	    height:40px;
	    line-height:50px;
	    background:#254283;
	    color:#fff;
	    position:fixed;
    	bottom:0px;
    	width:95%;
    	margin-left: 60px;
		text-align: center;
		font-size: <?php echo $coreset->FontSize?>px;
		font-weight: <?php echo $coreset->FontStyle?>;
		font-family: <?php echo $coreset->Font?>;
	}
</style>
<div id="container">
    <div id="footer">
        Version 2.0
    </div>
</div>
