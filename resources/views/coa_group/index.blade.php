@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url ('home') }}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url ('GroupCOA') }}">Data Group Coa</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="form_grid_3">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{route('GroupCOA.create') }}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div>
            </div>
            <br>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label>.</label><br>
                    <select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="Kode_Group_COA">Kode Group COA</option>
                        <option value="Nama_Group">Group COA</option>
                        <option value="Normal_Balance">Normal Balance</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" onkeyup="javascript:this.value=this.value.toUpperCase();" type="tex" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button class="btn btn-primary" id="cari-data"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
        <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
            <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Kode Group COA</th>
                    <th>Group COA</th>
                    <th>Normal Balance</th>
                    <th>Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
 
        </table>
    </div>
</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/coa_group/index.js') }}"></script>
<script>
    var urlData = '{{ route("GroupCoa.datatable") }}';
    var url                 = '{{ url("GroupCOA") }}';
</script>

@endsection 
