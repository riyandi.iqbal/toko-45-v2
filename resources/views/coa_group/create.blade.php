@extends('layouts.app')   
@section('content')
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('GroupCOA')}}">Data Group Coa</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Group COA</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Tambah Data Group COA</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="judul">Kode Group COA</label>
          </div>
          <div class="col-md-9">
            <input type="number" class="form-control" name="Kode_Group_COA" id="Kode_Group_COA" required oninvalid="this.setCustomValidity('Kode Group COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Nama Group COA</label>
          </div>
          <div class="col-md-9">
            <input type="text" class="form-control" name="Nama_Group" id="Nama_Group" required oninvalid="this.setCustomValidity('Nama Group COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
            <br><br>
          </div>
          <div class="col-md-3">
              <label class="judul">Normal Balance</label>
          </div>
          <div class="col-md-9">
              <select data-placeholder="Pilih Normal Balance" name="Normal_Balance" id="Normal_Balance" required oninvalid="this.setCustomValidity('Normal Balanc Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width:100%;" class="chosen-select" tabindex="13">
                  <option value=""></option>
                  <option value="debet">Debet</option>
                  <option value="kredit">Kredit</option>
              </select>
          </div>
        </div>
        <div class="col-md-9">
        </div>
        <br><br><br>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('GroupCOA')}}" name="simpan">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away"> Simpan </button>
          </div>
        </div>  
      </form>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
  var urlIndex            = '{{ url("GroupCOA") }}';
  var urlInsert           = '{{ route("GroupCOA.store") }}';
  var url                 = '{{ url("GroupCOA") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/coa_group/create.js') }}"></script>
@endsection 
