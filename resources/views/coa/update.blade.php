<!-- create @2019-12-09
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url ('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url ('Coa') }}">Data COA</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit COA (Charts Of Accounts)</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit COA (Charts Of Accounts)</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Kode COA</label>
            </div>
            <div class="col-md-9">
            <input type="hidden" name="idcoa" id="idcoa" value="<?php echo $coa->IDCoa ?>">
            <input type="text" class="form-control" name="kodecoa" id="kodecoa" value="<?php echo $coa->Kode_COA ?>" required oninvalid="this.setCustomValidity('Kode COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Nama COA</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" name="namacoa" id="namacoa" value="<?php echo $coa->Nama_COA ?>" required oninvalid="this.setCustomValidity('Kode COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Status COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Status COA" name="statuscoa" id="statuscoa" required oninvalid="this.setCustomValidity('Status COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <?php if ($coa->Status == 'anak') {
                            echo '<option selected value="anak">Anak</option><option value="kepala">Kepala</option>';
                        }else{
                            echo '<option value="anak">Anak</option><option selected value="kepala">Kepala</option>';
                        } ?>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Normal Balance</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Normal Balance" name="Normal_Balance" id="Normal_Balance" required oninvalid="this.setCustomValidity('Normal Balanc Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="debet" {{ $coa->Normal_Balance == 'debet' ? 'selected' : '' }} >Debet</option>
                        <option value="kredit" {{ $coa->Normal_Balance == 'kredit' ? 'selected' : '' }} >Kredit</option>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Nama Group COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Nama Group" name="namagroup" id="namagroup" required oninvalid="this.setCustomValidity('Nama Group Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                            <?php
                            foreach ($groupcoa as $co) {
                                if ($coa->IDGroupCOA==$co->IDGroupCOA) {
                            ?>
                        <option value="<?php echo $co->IDGroupCOA ?>" selected><?php echo $co->Nama_Group; ?></option>
                            <?php
                                } else {
                            ?>
                        <option value="<?php echo $co->IDGroupCOA ?>"><?php echo $co->Nama_Group; ?></option>
                            <?php
                                    }
                                }
                            ?>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Update Saldo Awal</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Status" name="updatesaldo" id="updatesaldo" required oninvalid="this.setCustomValidity('Saldo Awal Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <?php if ($coa->Saldo_Awal == 't') {
                            echo '<option selected value="t">Ya</option><option value="f">Tidak</option>';
                        }else{
                            echo '<option value="t">Ya</option><option selected value="f">Tidak</option>';
                        } ?>
                    </select>
                    <input type="hidden" class="form-control price-date" name="Modal" id="tanpa-rupiah-kredit" placeholder="Debit" required oninvalid="this.setCustomValidity('Debit Tidak Boleh Kosong')" value="0" oninput="setCustomValidity('')"/>
                </div>
            </div>
            <div class="text-center">
                <br><br>
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url ('Coa') }}" style="color: white;" name="simpan">Kembali</a></span>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Coa") }}';
  var urlUpdate           = '{{ route("Coa.update_data") }}';
  var url                 = '{{ url("Coa") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/coa/update.js') }}"></script>
@endsection 
