<!-- create @2019-12-09
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="#">Data COA (Charts Of Accounts)</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url ('Coa/create') }}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div>
            </div>
            <br><br><br>
            <div class="col-md-3">
          		<label class="field_title mt-dot2">.</label>
           		<select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                    <option value=""></option>
                    <option value="Kode_COA">Kode COA</option>
                    <option value="Nama_COA">Nama COA</option>
                    <option value="Normal_Balance">Normal Balance</option>
                    <option value="Status">Status</option>
                </select>
          	</div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <input name="keyword" id="keyword" type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Masukka kata kunci" class="form-control">
            </div>
            <div class="col-md-3">
                <label>&nbsp;.</label>
                <br>
                <button class="btn btn-primary" id="cari-data"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>
    </div>
    <br>
    <div class="banner">
    	<table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
            <thead style="color: #fff">
                <tr>
                  <th>No</th>
                  <th>Kode COA</th>
                  <th>Nama COA</th>
                  <th>Group COA</th>
                  <th>Status COA</th>
                  <th>Normal Balance</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
    		    </thead>
    	</table>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/coa/index.js') }}"></script>
<script>
    var urlData = '{{ route("Coa.datatable") }}';
    var url                 = '{{ url("Coa") }}';
</script>
@endsection 
