<!-- create @2019-12-09
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url ('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url ('Coa')}}">Data COA</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data COA</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data COA</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Kode COA</label>
            </div>
            <div class="col-md-9">
            <input type="number" class="form-control" name="kodecoa" id="kodecoa" required oninvalid="this.setCustomValidity('Kode COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Nama COA</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" name="namacoa" id="namacoa" required oninvalid="this.setCustomValidity('Kode COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" onkeyup="javascript:this.value=this.value.toUpperCase();">
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Status COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Status COA" name="statuscoa" id="statuscoa" required oninvalid="this.setCustomValidity('Status COA Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="anak">Anak</option>
                        <option value="kepala">Kepala</option>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Normal Balance</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Normal Balance" name="Normal_Balance" id="Normal_Balance" required oninvalid="this.setCustomValidity('Normal Balanc Tidak Boleh Kosong')" oninput="setCustomValidity('')" style="width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="debet">Debet</option>
                        <option value="kredit">Kredit</option>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Nama Group COA</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Nama Group" name="namagroup" id="namagroup" required oninvalid="this.setCustomValidity('Nama Group Tidak Boleh Kosong')" oninput="setCustomValidity('')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                            <?php 
                            foreach ($groupcoa as $data) {
                            ?>
                            <option value="<?php echo $data->IDGroupCOA ?>"><?php echo $data->Nama_Group ?></option>
                            <?php
                                }
                            ?>
                    </select>
                    <br><br>
                </div>
                <div class="col-md-3">
                    <label class="judul">Update Saldo Awal</label>
                </div>
                <div class="col-md-9">
                    <select data-placeholder="Pilih Status" name="updatesaldo" id="updatesaldo" required oninvalid="this.setCustomValidity('Saldo Awal Tidak Boleh Kosong')" oninput="setCustomValidity('')" style=" width:100%;" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="t">Ya</option>
                        <option value="f">Tidak</option>
                    </select>
                    <input type="hidden" class="form-control price-date" name="Modal" id="Modal" placeholder="Debit" required oninvalid="this.setCustomValidity('Debit Tidak Boleh Kosong')" value="0" oninput="setCustomValidity('')"/>
                </div>
            </div>
            <div class="text-center">
                <br><br>
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url ('Coa')}}" style="color: white;" name="simpan">Kembali</a></span>
                </div>
                {{-- <div class="btn btn-success hvr-icon-float-away">
                    <span style="color: white;" onclick="simpan()">Simpan</span>
                </div> --}}
                <div class="btn">
                    <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Coa") }}';
  var urlInsert           = '{{ route("Coa.store") }}';
  var url                 = '{{ url("Coa") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/coa/create.js') }}"></script>
@endsection 
