<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Satuan')}}">Data Satuan</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Satuan</a>                
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">     
      <span>Tambah Data Satuan</span>
    </div>
    <div class="banner">
      <form id="form-data">
      <div class="container">
      <br><br>
        <div class="col-md-3">
          <label class="judul">Kode Satuan</label>
        </div>
        <div class="col-md-9">
          <input type="tex" class="form-control" name="Kode_Satuan" id="Kode_Satuan" onkeyup="javascript:this.value=this.value.toUpperCase();" required oninvalid="this.setCustomValidity('Kode Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
          <br><br>
        </div>
        <div class="col-md-3">
          <label class="judul">Nama Satuan</label>
        </div>
        <div class="col-md-9">
          <input type="tex" class="form-control" name="Satuan" id="Satuan" onkeyup="javascript:this.value=this.value.toUpperCase();" required oninvalid="this.setCustomValidity('Nama Satuan Tidak Boleh Kosong')" oninput="setCustomValidity('')">
          <br><br>
        </div>
        
      </div>
      <br><br><br>
      <div class="text-center">
        <div class="btn col-11 hvr-icon-back">
          <span> <a style="color: white;" href="{{url('Satuan')}}" name="simpan">Kembali</a></span>
        </div>
        <div class="btn">
            <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
        </div>
      </div>
    </form>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Satuan") }}';
  var urlInsert           = '{{ route("Satuan.store") }}';
  var url                 = '{{ url("Satuan") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/satuan/create.js') }}"></script>
@endsection
