<!-- /*=====Create DEDY @10/12/2019====*/ -->
@extends('layouts.app')   
@section('content')
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Gudang')}}">Data Gudang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Gudang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Edit Data Gudang</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="judul">Kode Gudang</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="kodegudang" id="kodegudang" value="<?php echo $gudang->Kode_Gudang; ?>" required oninvalid="this.setCustomValidity('Kode Gudang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <input type="hidden" name="idgudang" id="idgudang" value="<?php echo $gudang->IDGudang ?>">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Nama Gudang</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="nama" id="nama" value="<?php echo $gudang->Nama_Gudang ?>" required oninvalid="this.setCustomValidity('Nama Gudang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
          </div>
          
        </div>
        <br><br><br>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('Gudang')}}" name="simpan">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>


<script type="text/javascript">
  function field()
  {
    var data1 = {
          "Kodegudang"        :$('#kodegudang').val(),
          "Nama"              :$('#nama').val(),
          "IDGudang"          :$('#idgudang').val(),        
        }
        return data1;
  }

  function simpan()
  {
    var data = field();
    console.log($('#Kodegudang').val());

      console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{url('Gudang/simpanedit')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data:data},
                  dataType:'json',
                  success: function(data)
                  { 
                    //zeroValue;
                    console.log('----------Ajax berhasil-----');
                    //swal('Berhasil', 'Berhasil Simpan Data', 'success');
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Simpan Edit Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "{{url('Gudang')}}";
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal('Gagal', 'Data Gagal Simpan', 'warning');
                 }
               });
     
  }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Gudang") }}';
  var urlUpdate           = '{{ route("Gudang.update_data") }}';
  var url                 = '{{ url("Gudang") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/gudang/update.js') }}"></script>

@endsection