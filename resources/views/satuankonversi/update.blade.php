@extends('layouts.app')   
@section('content')
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('SatuanKonversi')}}">Data Konversi Satuan</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Konversi Satuan</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">     

      <span>Edit Data Satuan</span>
    </div>
    <div class="bannerbody">
      <table class="table table-responsive">
        <tr>
            <td style="width: 20%;">
                Nama Barang
            </td>
            <td style="width: 5%;"> : </td>
            <td>
                {{ $data_barang->Nama_Barang }}
            </td>
        </tr>
        <tr>
            <td style="width: 20%;">
                Satuan Kecil
            </td>
            <td style="width: 5%;"> : </td>
            <td>
                {{ $data_barang->Satuan }}
            </td>
        </tr>
    </table>
    
      <div class="container" style="width: 100%">
        <br>
        <table class="table table-bordered" id="table-satuan" style="font-size: 12px;">
            <thead style="background-color: #16305d; color: white">
              <tr>
                  <th style="width: 5%;">No</th>
                  <th >Satuan Besar</th>
                  <th style="width: 15%;">Qty</th>
                  <th style="width: 15%;">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($satuankonversi as $key => $item)
                  <tr>
                    <td> {{ ++$key }}
                      <input type="hidden" class="IDSatuanBesar" value="{{ $item->IDSatuanBesar }}">
                      <input type="hidden" class="Qty" value="{{ $item->Qty }}">
                      <input type="hidden" class="IDSatuanKonversi" value="{{ $item->IDSatuanKonversi }}">
                    </td>
                    <td> {{ $item->Satuan_besar }} </td>
                    <td> {{ $item->Qty }} </td>
                    <td> 
                      <span class="action-icons edit"><i class="fa fa-pencil fa-lg"></i></span> &nbsp;
                      <span><a title="Hapus" onclick="deleteData('{{ $item->IDSatuanKonversi }}', '{{ url('SatuanKonversi') . '/' . $item->IDSatuanKonversi }}')">  <i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>
                    </td>
                  </tr>
              @endforeach
            </tbody>
        </table>
        <div class="btn col-16">
            <span> <a style="color: white; float: right;" href="{{ route('SatuanKonversi.index') }}">Kembali</a></span>
        </div>
    </div>
    </div>
  </div>
</div>

<div id="modal-data" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form id="form-data">
      {{ csrf_field() }}
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Harga Jual</h4>
          </div>
          <div class="modal-body">
            <div class="row" style="padding: 10px;">
              <input type="hidden" name="IDSatuanKonversi" id="IDSatuanKonversi">
              <label class="field_title">Satuan Besar</label>
              <select name="IDSatuanBesar" id="IDSatuanBesar" class="form-control select2" style="width: 100%">
                  <option value="">- Pilih Satuan Besar -</option>
                  @foreach ($data_satuan as $item)
                      <option value="{{ $item->IDSatuan }}" {{ $data_barang->IDSatuan == $item->IDSatuan ? 'disabled' : '' }} > {{ $item->Satuan }} </option>
                  @endforeach
              </select>
              <label class="field_title">Qty</label>
              <div class="form_input">
                <input type="text" name="Qty" id="Qty" class="form-control">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn col-16" class="close" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </div>
    </form>
  </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
  var IDBarang = '{{ $data_barang->IDBarang }}';
  var urlInsert = '{{ route("SatuanKonversi.update_data") }}';
  var url     = '{{ url("SatuanKonversi") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/satuankonversi/satuankonversi_update.js') }}"></script>
@endsection
