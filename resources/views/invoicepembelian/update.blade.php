<!-- ===========Create By Tiar 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
        font-size: 14px;
    }
</style>
<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('InvoicePembelian.index')}}">Data Invoice Pembelian</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Edit Invoice Pembelian</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Invoice Pembelian</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td>
                        <input type="hidden" name="IDFB" id="IDFB" value="{{ $purchase_order->IDFB }}">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date_format(date_create($purchase_order->Tanggal), 'd/m/Y') }}">
                        <br>
                        <label for="Nomor">Nomor Invoice</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No PO" value="{{ $purchase_order->Nomor }}">

                        <input type="hidden" name="Nomor_exist" id="Nomor_exist" class="form-control" placeholder="No PO" value="{{ $purchase_order->Nomor }}">
                        <br>
                        <label for="IDSupplier">Supplier</label><br>
                        <select data-placeholder="Cari Supplier" class="form-control select2" name="IDSupplier" id="IDSupplier" required style="width: 100%;" >
                            <option value="">- Pilih -</option>
                            <?php foreach ($supplier as $row) {
    $selected = ($row->IDSupplier == $purchase_order->IDSupplier) ? 'selected' : '';
    echo "<option value='$row->IDSupplier' $selected >$row->Nama</option>";
}
?>
                        </select>

                    </td>
                    <td>
                    <label for="IDTBS">Nomor Pajak </label>
                        <input type="text" name="Nomor_pajak" id="Nomor_pajak" class="form-control form-white" placeholder="No Pajak" value="{{ $purchase_order->no_pajak }}">
                        <br>
                        <label for="Nomor_sj">Nomor Invoice Supplier</label><br>
                        <input type="text" name="Nomor_sj" id="Nomor_sj" class="form-control form-white" placeholder="Nomor Invoice Supplier" value="{{ $purchase_order->No_sj_supplier }}">
                        <br>
                        <label for="Status_ppn">Jenis PPN</label><br>
                        <input type="radio" name="Status_ppn" id="ppn1" value="include" {{ ('1' == $purchase_order->jenis_ppn) ? 'checked' : '' }}> Include PPN&nbsp;
                        <input type="radio" name="Status_ppn" id="ppn0" value="exclude" {{ ('0' == $purchase_order->jenis_ppn) ? 'checked' : '' }}> Exclude PPN
                    </td>
                    <td>
                    <label for="Total_qty">Total Qty</label>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control"  placeholder="Total Qty" readonly required value="{{ $purchase_order->Total_qty_yard }}" >
                        <br>
                    <label for="Grand_total">Grand Total</label>
                        <input type="text" name="Grand_total" id="Grand_total" class="form-control" placeholder="Grand Total Harga" readonly required  value="{{ AppHelper::NumberFormat($purchase_order->total_harga) }}">
                        <!-- <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"> {{ $purchase_order->Keterangan }} </textarea> -->
                    </td>
                </tr>
            <tr>
            <td hidden>
            <label for="IDMataUang">Mata Uang</label><br>
                        <select data-placeholder="Cari Kurs" class="form-control select2" style="width: 55%" name="IDMataUang" id="IDMataUang">
                            <option value="">-- Pilih Mata Uang --</option>
                            @foreach ($mata_uang as $item)
                                <?php $selected = ($item->IDMataUang == $purchase_order->IDMataUang) ? 'selected' : '';?>
                                <option value="{{ $item->IDMataUang }}" data-kurs="{{ $item->Kurs }}" {{ $selected }} > {{ $item->Mata_uang }} </option>
                            @endforeach
                        </select>
                        <input type="text" name="Kurs" id="Kurs" style="width: 40%; float: right;" readonly class="form-control" value="{{ $purchase_order->Kurs }}">
                        </td>
                        <br><br>
                        <td hidden>   <label for="IDGudang">Gudang</label>
                        <select name="IDGudang" id="IDGudang" class="form-control select2">
                            @foreach ($gudang as $item)
                                <option value="{{ $item->IDGudang }}"> {{ $item->Nama_Gudang }} </option>
                            @endforeach
                        </select>
                        </td>
                        </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 50px;">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                        <li class="active"><a href="#tab-barang" data-toggle="tab">Input Barang</a></li>
                        <li><a href="#tab-pembayaran" data-toggle="tab">Pembayaran</a></li>
                    </ul>
                </div>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="tab-barang">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>
                        </div>
                        <div class="container" style="width: 100%">
                            <br>
                            <table class="table table-bordered" id="table-barang" style="font-size: 12px;">
                                <thead style="background-color: #16305d; color: white">
                                <tr>
                                    <th style="width: 25%">Nama Barang</th>
                                    <th style="width: 10%">Qty</th>
                                    <th style="width: 13%">Satuan</th>
                                    <th style="width: 15%">Harga</th>
                                    <th style="width: 7%">PPN %</th>
                                    <th style="width: 15%">Diskon</th>
                                    <th style="width: 15%">Subtotal</th>
                                    <th style="width: 2%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="list-barang">
                                    {{-- @foreach ($purchase_order_detail as $item)
                                        <tr>
                                            <td>
                                                <select name="IDBarang[]" id="" class="form-control select-barang">
                                                    <option value="{{ $item->IDBarang }}"> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </option>
                                                </select>
                                            </td>
                                            <td> <input placeholder="Quantity" type="number" name="Qty[]" class="form-control qty" value="{{ $item->Qty_yard }}"> </td>
                                            <td> <select name="IDSatuan[]" class="form-control select-satuan select2" style="width: 100%">
                                                <option value="{{ $item->IDSatuan }}"> {{ $item->Satuan }} </option>
                                            </select> </td>
                                            <td> <input placeholder="" type="text" name="harga[]" class="form-control price" value="{{ AppHelper::NumberFormat($item->Harga) }}"> </td>
                                            <td> <input placeholder="" type="text" name="ppn[]" class="form-control ppn" value="{{ ($purchase_order->jenis_ppn == '1') ? 0 : 10 }}" readonly> </td>
                                            <td> <input placeholder="" type="text" name="diskon[]" class="form-control diskon" value="{{ AppHelper::NumberFormat($item->diskon) }}" readonly> </td>
                                            <td> <input placeholder="" type="text" name="Sub_total[]" class="form-control subtotal" value="{{ AppHelper::NumberFormat($item->sub_total) }}"> </td>
                                            <td><i class="fa fa-times-circle fa-lg hapus" style="color:red; padding: 10px;"></i></td>
                                        </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                            <a class="btn col-11" id="btn-add-barang" ><i class="col-11 hvr-icon-float-away">Tambah</i></a>
                        <table style="width: 100%;">
                                <tr>
                                    <td style="width: 50%; vertical-align: top;">
                                        <label for="Keterangan">Keterangan</label>
                                            <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5">{{ $purchase_order->Keterangan }}</textarea>
                                    </td>
                                    <td style="padding-left: 10px;">
                                        <label style="float: left;" id="juduldpp">DPP</label>
                                        <input type="text" style="width: 70%" id="DPP" name="DPP" class="form-control text-right total-add" placeholder="DPP" value="{{ AppHelper::NumberFormat($purchase_order->DPP, $angkakoma) }}" readonly>
                                       
                                        <br class="ppn1"><br class="ppn1"><br class="ppn1">
                                        <label style="float: left;" id="judulppn" class="ppn1">PPN</label>
                                        <input type="text" style="width: 70%" id="PPN1" name="PPN1" class="form-control text-right ppn1 total-add" placeholder="PPN"  value="{{ $purchase_order->PPN }}"readonly>

                                        <br><br><br>
                                        <label style="float: left;" for="total_invoice">Sub Total Invoice</label>
                                        <input type="text" style="width: 70%" id="total_invoice" name="total_invoice" class="form-control text-right" placeholder="Sub Total Invoice" value="{{AppHelper::NumberFormat($purchase_order->total_harga, $angkakoma) }}" readonly>

                                        <br><br><br>
                                            <label style="float: left;">Diskon %</label>
                                            <input type="text" style="width: 70%" id="diskonpersen" name="diskonpersen" class="form-control text-right form-white" readonly>
                                            <br><br><br>
                                            <label style="float: left;">Diskon Rupiah</label>
                                            <input type="text" style="width: 70%" id="diskonrupiah" name="diskonrupiah" class="form-control text-right form-white harga-123 total-min" value="{{ $purchase_order->Diskon }}">
                                            <br><br><br>
                                        <!-- <label style="float: left;" for="UMCustomer">DP</label>
                                        <input type="text" style="width: 70%" id="UMCustomer" name="UMCustomer" class="form-control text-right" placeholder="DP" readonly>
                                        <br><br><br> -->
                                        <label style="float: left;" for="total_invoice_diskon_view">Total Invoice</label>
                                        <input type="text" style="width: 70%" id="total_invoice_diskon_view" name="total_invoice_diskon_view" class="form-control text-right sisa-total-invoice" placeholder="Total Invoice"value="{{AppHelper::NumberFormat($purchase_order->total_harga, $angkakoma) }}"readonly>

                                        <input type="hidden" style="width: 70%" id="total_invoice_diskon" name="total_invoice_diskon" class="form-control text-right total-invoice" placeholder="Total Invoice" readonly>                                    </td>
                                </tr>
                            </table>

                            <div class="btn col-16">
                                <span> <a style="color: white; float: right;" href="{{ route('InvoicePenjualan.index') }}">Kembali</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-pembayaran">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Pembayaran</span>
                        </div>
                        <div class="container" style="width: 100%">
                            <br>
                            <div class="col-md-6"><input type="hidden" name="IDUMSupplier" id="IDUMSupplier" value="{{ $um_supplier ?  $um_supplier->IDUMSupplier : '' }}">
                                <label>Jenis Pembayaran</label>
                                <select name="Jenis_Pembayaran" id="Jenis_Pembayaran" class="form-control">
                                    <option value="">-- Pilih Jenis Pembayaran --</option>
                                    <?php foreach ($cara_bayar as $cb) {?>
										<option value="<?php echo strtolower($cb->Nama) ?>"><?php echo $cb->Nama ?></option>
									<?php }?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nama COA</label>
                                <select name="IDCoa" id="IDCoa" class="form-control">
                                    <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nomor Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control" name="Nomor_giro" id="Nomor_giro" value="{{ $um_supplier ? ($um_supplier->Nomor_Giro) : '' }}">
                            </div>
                            <div class="col-md-6">
                                <label>Tanggal Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control datepicker" name="Tanggal_giro" id="Tanggal_giro" value="{{ $um_supplier ? $um_supplier->Tanggal_giro ? date_format(date_create($um_supplier->Tanggal_giro), 'd/m/Y') : '' : '' }}">
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Pembayaran</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="pembayaran" id="pembayaran" value="{{ $um_supplier ? AppHelper::NumberFormat($um_supplier->Nominal) : '' }}">
                            </div>
                            <div class="col-md-12">
                            <button class="btn btn-info" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex    = '{{ route("InvoicePembelian.index") }}';
    var urlUpdate = '{{ route("InvoicePembelian.update_data") }}';
    var urlBarang = '{{ route("InvoicePembelian.get_barang") }}';
    var urlCoa = '{{ route("InvoicePembelian.get_coa") }}';
    var url     = '{{ url("InvoicePembelian") }}';
    var urlSatuan = '{{ route("Po.get_satuan") }}';
    var urlHargaJual = '{{ route("Po.get_harga_jual") }}';

    var Jenis_Pembayaran = '{{ $um_supplier ? $um_supplier->Jenis_pembayaran : "" }}';
    var IDCoa = '{{ $um_supplier ? $um_supplier->IDCOA : "" }}';
    var Pembayaran = '{{ $um_supplier ? $um_supplier->Nominal : "" }}';
    let Batal   = {{ ($purchase_order->Batal != 'aktif') ? "true" : "false" }};
    var jenis_ppn = '{{ $purchase_order->jenis_ppn == 1 ? "include" : "exclude" }}';
    var purchase_order_detail = <?php echo json_encode($purchase_order_detail, JSON_PRETTY_PRINT) ?>;
    var data_barang = <?php echo json_encode($barang); ?>;

    function ambilnomorpononppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR SO');
            $.ajax({
                    url : "{{url('ambilnomorinvoice')}}",
                    type: "GET",
                    data:{status:$('#ppn1').val()},
                    dataType:'json',
                    beforeSend: function(data) {
                        $('#modal-loading').fadeIn('fast');
                    },
                    success: function(data)
                    {
                        console.log('KODE BARU SO');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    },
                    complete: function(data) {
                        $('#modal-loading').fadeOut('fast');
                    }
            });
    }

    function ambilnomorpoppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR SO PPN');
            $.ajax({
                    url : "{{url('ambilnomorinvoice')}}",
                    type: "GET",
                    data:{status:$('#Status_ppn').val()},
                    dataType:'json',
                    beforeSend: function(data) {
                        $('#modal-loading').fadeIn('fast');
                    },
                    success: function(data)
                    {
                        console.log('KODE BARU SO PPN');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    },
                    complete: function(data) {
                        $('#modal-loading').fadeOut('fast');
                    }
            });
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/invoicepembelian/invoicepembelian_update.js') }}"></script>

@endsection