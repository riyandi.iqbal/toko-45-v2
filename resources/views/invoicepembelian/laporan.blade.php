<!-- ===========Create By Dedy 20-12-2019=============== -->
@extends('layouts.app')   
@section('content')

<div class="main-grid">
  <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('InvoicePembelian')}}">Data Invoice Pembelian</a>
        </h2>
  </div><!-- banner -->
  <br>
  <?php 
    $hari_ini = date("Y-m-d");
    $tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
  ?>
  <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Data Invoice Pembelian</span>
    </div>
    <br>
  <div class="banner">
    
      <div class="col-md-2">
        <label class="field_title mt-dot2">Periode</label>
          <div class="form_input">
            <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
          </div>
      </div>
      <div class="col-md-2">
        <label class="field_title mt-dot2"> S/D </label>
          <div class="form_input">
            <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
          </div>
      </div>
      <div class="col-md-3">
        <label class="field_title mt-dot2">.</label>
        <select name="jenispencarian" id="jenispencarian" data-placeholder="Cari Berdasarkan" style="width: 100%!important" class="form-control" tabindex="13">
          <option value=""></option>
          <option value="Nomor">No Invoice</option>
          <option value="Nama">Nama Supplier</option>
          <option value="SJ">SJ Supplier</option>
          <option value="Barang">Nama Barang</option>
          <option value="PB">Nomor PB</option>
        </select>
      </div>
      <div class="col-md-3">
        <div class="form_grid_2">
          <label class="field_title mt-dot2">.</label>
          <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
        </div>
      </div>
      <div class="col-md-2">
        <div class="form_grid_2">
          <label>&nbsp;.</label>
          <br>
          <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
        </div>                
      </div>
    <!-- </form> -->
    <br><br><br>
  </div>
  <br>
  <div class="banner">
  <button onclick="exlin()" class="btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;EXCEL</button>
  <br><br>
    <form action="{{url('InvoicePembelian/ubahstatus')}}" method="post" id="ubahstatus">
      <div class="widget_content">
      
        <table id="tablefs" class="display" width="100%" style="background-color: #254283; font-size: 12px;">
          <thead style="color: #fff">
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>No Invoice</th>
              <th>Nama Supplier</th>
              <th>Total QTY</th>
              <th>Jatuh Tempo</th>
              <th>Total Harga</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody id="previewdata">
            <?php if(empty($invoice)){?>
            <?php }else{
              $i=1;
                foreach ($invoice as $data) {
            ?>
            <tr class="odd gradeA">
              <td><center><?php echo $i; ?></center></td>
              <td><?php echo date("d M Y", strtotime($data->Tanggal)); ?></td>
              <td><?php echo $data->Nomor; ?></td>
              <td><?php echo $data->Nama; ?></td>
              <td><?php echo $data->Total_qty_yard; ?></td>
              <td><?php echo date("d M Y", strtotime($data->Tanggal_jatuh_tempo)); ?></td>
              <td align=right><?php echo 'Rp.' .number_format($data->Grand_total,0,',','.'); ?></td>
              <td><center><?php echo $data->Batal; ?></center></td>
            </tr>
            <?php
              $i++;
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </form>
  </div> 
</div>

    <script type="text/javascript">
      $(document).ready(function() {
                //====================================datatables
                table = $('#tablefs').DataTable({ 
                    "searching": false,
                    "lengthChange": false
                });
            });

      var PM = [];
    function caridata()
    {
        var jenis       = $('#jenispencarian').val();
        var keyword     = $('#keyword').val();
        var date_from   = $('#date_from').val();
        var date_until  = $('#date_until').val();
            $.ajax({
                  url : "{{url('InvoicePembelian/pencarian')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+

                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td>"+PM[i].Saldo_yard+"</td>"+
                      "<td>"+formatDate(PM[i].Tanggal_jatuh_tempo)+"</td>"+
                      "<td>Rp. "+rupiah(PM[i].Grand_total)+"</td>"+
                      "<td>"+PM[i].Batal+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }
                      
                     }
                }
            });
    }

    function exlin()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

       window.location.replace("{{url('LaporanInvoice/toexcel/?date1=')}}"+date1+'&date2='+date2+'&jenis='+jenis+'&keyword='+keyword);
    }
    </script>
@endsection