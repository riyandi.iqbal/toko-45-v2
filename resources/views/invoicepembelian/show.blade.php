<!-- ===========Create By Dedy 20-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
	<div class="banner">
		<h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('InvoicePembelian')}}">Data Faktur Pembelian</a>
                <i class="fa fa-angle-right"></i>
                <a href="">Detail Faktur Pembelian</a>
        </h2>
	</div>
	<br>
	<div class="banner">
		<table class="table cell-border" width="100%"  style="background-color: #254283; font-size: 12px;">
            <thead style="display: none;">
            </thead>
            <tbody>
                <tr>
                    <td style="background-color: #ffffff;">Tanggal</td>
	                <td style="background-color: #ffffff;"><?php echo date("d M Y", strtotime($pembelian->Tanggal)); ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #e5eff0;">No Invoice</td>
                    <td width="65%" style="background-color: #e5eff0;"><?php echo $pembelian->No_INV ?></td>
                </tr>
                <tr>
                    <td  style="background-color: #ffffff;">Nama Supplier</td>
                    <td  style="background-color: #ffffff;"><?php echo $pembelian->Nama ?></td>
                </tr>
                <tr>
                    <td style="background-color: #e5eff0;">No Pajak</td>
                    <td style="background-color: #e5eff0;"><?php echo $pembelian->no_pajak ?></td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff;">No Invoice Supplier</td>
                    <td style="background-color: #ffffff;"><?php echo $pembelian->No_sj_supplier ?></td>
                </tr>
                <tr>
                    <td  style="background-color: #e5eff0;">Top</td>
                    <td  style="background-color: #e5eff0;"><?php echo $pembelian->TOP ?></td>
                </tr>
                <!-- <tr>
                    <td style="background-color: #ffffff;">Tanggal Jatuh Tempo</td>
                    <td style="background-color: #ffffff;"><?php echo date("d M Y", strtotime($pembelian->Tanggal_jatuh_tempo)); ?></td>
                </tr> -->
                <tr>
                    <td  style="background-color: #ffffff;">Discount</td>
                    <td  style="background-color: #ffffff;">{{ AppHelper::NumberFormat($pembelian->Diskon, $angkakoma) }}</td>
                </tr>
                <tr>
                    <td style="background-color: #e5eff0;">Status PPN</td>
    	            <td style="background-color: #e5eff0;"><?php echo $pembelian->Status_ppn ?></td>
                </tr>
                <tr>
                    <td  style="background-color: #ffffff;">Keterangan</td>
                    <td  style="background-color: #ffffff;"><?php echo $pembelian->Keterangan ?></td>
                </tr>
                <tr>
                    <td  style="background-color: #e5eff0;">Total Qty</td>
                    <td  style="background-color: #e5eff0;"><?php echo $pembelian->Total_qty_yard ?></td>
                </tr>
                <tr>
                    <td  style="background-color: #ffffff;">DPP</td>
                    <td  style="background-color: #ffffff;"><?php if ($pembelian->IDMataUang == 'P000002') {echo 'Rp.';} else {echo '$.';}?>
                    <?php
if ($pembelian->Status_ppn == "include") {
    echo number_format($pembelian->DPP, 0, ',', '.');
} else {
    echo number_format($total_harga_inv->total_harga, 0, ',', '.');
}
?>
                    </td>
                </tr>
                @if ($pembelian->Status_ppn == 'exclude')
                    <tr>
                        <td  style="background-color: #e5eff0;">PPN</td>
                        <td  style="background-color: #e5eff0;"><?php if ($pembelian->IDMataUang == 'P000002') {echo 'Rp.';} else {echo '$.';}?>
                        <?php
echo number_format($pembelian->PPN, 0, ',', '.');
?>
                        </td>
                    </tr>
                @endif
                <tr>
                    <td style="background-color:#e5eff0;">Grand Total</td>
                    <td style="background-color: #e5eff0;"><?php if ($pembelian->IDMataUang == 'P000002') {echo 'Rp.';} else {echo '$.';}?>
                    <?php
if ($pembelian->Status_ppn == "exclude") {
    echo number_format(($total_harga_inv->total_harga * 1.1), 0, ',', '.');

} else {
    echo number_format(($total_harga_inv->total_harga), 0, ',', '.');

}
?>
                    </td>
                </tr>
            </tbody>
            <tfoot></tfoot>
        </table>
	</div>
	<br>
	<div class="banner">
		<table id="tblinv" class="table cell-border" width="100%">
            <thead   style="background-color: #254283; font-size: 12px; color: #fff;">
            	<tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Qty</th>
                    <th>Harga</th>
                    <th>Satuan</th>
                    <th>PPN % </th>
                    <th>Diskon </th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pembelian_detail as $key => $item)
                    <tr>
                        <td> {{ ++$key }} </td>
                        <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                        <td style="text-align: center;"> {{ $item->Qty_yard }} </td>
                        <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                        <td style="text-align: center;"> {{ $item->Satuan }} </td>
                        <td style="text-align: center;"> {{ ($pembelian->Status_ppn == 'include') ? 0 : 10 }} </td>
                        <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->diskon, $angkakoma) }} </td>
                        <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Sub_total, $angkakoma) }} </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot style="background-color: #254283; color: #fff">
                <tr>
                    <th colspan="2" rowspan="<?php echo ($pembelian->Status_ppn == 'exclude') ? 7 : 6; ?>" style="text-align: left; width: 60%; vertical-align: top;">
                        Keterangan : <br>
                        {{ $pembelian->Keterangan }}
                    </th>
                </tr>
                <tr>
                    <th colspan="5" style="text-align: right;">Sub Total</th>
                    <th style="text-align: right;"> {{ AppHelper::NumberFormat($total_harga_inv->total_harga, $angkakoma) }} </th>
                </tr>
                @if ($pembelian->Status_ppn == 'exclude')
                    <tr>
                        <th colspan="5" style="text-align: right;">PPN</th>
                        <th style="text-align: right;"> {{ AppHelper::NumberFormat($pembelian->PPN, $angkakoma) }} </th>
                    </tr>
                @endif
                <tr>
                    <th colspan="5" style="text-align: right;">Diskon</th>
                    <th style="text-align: right;"> {{ AppHelper::NumberFormat($pembelian->Diskon, $angkakoma) }} </th>
                </tr>
                <tr>
                    <th colspan="5" style="text-align: right;">Grand Total</th>
                    <th style="text-align: right;"> {{ AppHelper::NumberFormat($pembelian->DPP + $pembelian->PPN - $pembelian->Diskon, $angkakoma) }} </th>
                </tr>
                <!-- <tr>
                    <th colspan="5" style="text-align: right;">DP</th>
                    <th style="text-align: right;"> {{ $um ? AppHelper::NumberFormat($um->Nominal, $angkakoma) : 0 }} </th>
                </tr> -->
                <tr>
                    <th colspan="5" style="text-align: right;">Sisa Pembayaran</th>
                    <th style="text-align: right;"> {{ $um ? AppHelper::NumberFormat($pembelian->DPP - $um->Nominal + $pembelian->PPN - $pembelian->Diskon, $angkakoma ) : AppHelper::NumberFormat($pembelian->DPP + $pembelian->PPN - $pembelian->Diskon, $angkakoma ) }} </th>
                </tr>
            </tfoot>
        </table>
        <br><br>
        <div class="text-center">
        	<div class="btn col-11 hvr-icon-back">
                <span> <a style="color: white;" href="{{url('InvoicePembelian')}}" name="simpan">&nbsp;&nbsp;&nbsp;&nbsp;Kembali</a></span>
            </div>
            <div class="btn btn-primary hvr-icon-spin">
                <span><a style="color: white;" href="{{ url('InvoicePembelian/print/' . $pembelian->IDFB) }}">Print Data&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
            </div>
        </div>
        <br><br><br>
	</div>
</div>

<script type="text/javascript">

</script>
@endsection
