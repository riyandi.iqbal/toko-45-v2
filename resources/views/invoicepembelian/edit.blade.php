<!-- ===========Create By Dedy 20-12-2019=============== -->
@extends('layouts.app')   
@section('content')

<style type="text/css">
.chzn-container {
  width: 102.5% !important;
}
</style>

<div class="main-grid">
  <div class="banner">
    <h2>
      <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" style="border: none;">Home</a></span>
      <i class="fa fa-angle-right"></i>
      <a href="{{url('InvoicePembelian')}}">Data Faktur Pembelian</a>
      <i class="fa fa-angle-right"></i>
      <a href="">Edit Faktur Pembelian</a>
    </h2>
  </div>
  <br>
  <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Edit Data Faktur Pembelian</span>
    </div>
  <form class="form-horizontal"  action="{{url('InvoicePembelian/simpandata')}}" method="post">

  <div class="banner container">
    <div class="col-md-4">
      <label>Tgl Invoice</label>
      <input id="Tanggal" type="date" name="Tanggal" class="input-date-padding-3-2 form-control" placeholder="Tanggal" value="<?php echo $header->Tanggal ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
      <br>
      <label>No Invoice</label>
      <input type="text" name="Nomor_invoice" id="Nomor_invoice" class="form-control" placeholder="No Asset" readonly value="<?php echo $header->Nomor ?>" >
      <input type="hidden" name="IDFB" id="IDFB" value="<?php echo $header->IDFB ?>">
      <br>
      <input type="hidden" name="no_penerimaan" id="no_penerimaan" value="<?php echo $header->IDTBS ?>">
      <label>Nomor Penerimaan</label>
      <input name="Nomor" id="Nomor" class="form-control" value="<?php echo $header->Nomortbs ?>" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')">
    </div>
    <div class="col-md-4">
      <label>Jatuh Tempo</label><br>
      <input id="Jatuh_tempo" type="text" onkeyup="hitung();" value="<?php echo $header->TOP ?>" name="Jatuh_tempo" class="form-control tempo" placeholder="Jatuh tempo" value="75" style="width: 30%; float: left; margin-right: 10px">
      <!-- <input type="checkbox" class="checkbox" id="customize" onchange="customize_tempo()">
      <label class="choice pr" style="margin-bottom: 5px;">Customize</label> -->
      <br><br><br>
      <label>Status PPN</label>
      <input type="tex" class="form-control" name="Status_ppn" id="Status_ppn" value="<?php echo $header->Status_ppn ?>" readonly onchange="ubah_status_ppn()">
      <br>
      <label class="field_title mb" style="padding: 0!important">No SJ Supplier</label>
      <input id="No_supplier" type="text" name="No_supplier" class="form-control" placeholder="No Supplier" value="<?php echo $header->No_sj_supplier ?>">
           
    </div>
    <div class="col-md-4">
      <input id="Discount" type="hidden" name="Discount" value="0" class="form-control" placeholder="Discount" onkeyup="ubah_discount()">
      <label>Supplier</label>
      <input type="hidden" class="form-control" name="id_supplier" id="id_supplier" value="<?php echo $header->IDSupplier ?>" readonly>
      <input type="text" class="form-control" name="nama_supplier" id="nama_supplier" value="<?php echo $header->Nama ?>" readonly>
      <br><br>
      <?php
        $tgl1 = date('Y/m/d');
        $tgl2 = date('Y-m-d', strtotime('+75 days', strtotime($tgl1)));
      ?>
      <label>Tgl Jatuh Tempo</label>
      <input id="Tanggal_jatuh_tempo" type="date" value="<?php echo $header->Tanggal_jatuh_tempo ?>" name="Tanggal_jatuh_tempo" class="input-date-padding-3-2 form-control" placeholder="Tanggal Jatuh tempo">
      <br>
      <label>Total QTY</label>
      <input id="Total_qty_yard" type="text" name="Total_qty_yard" class="form-control" placeholder="Total Qty" value="<?php echo $header->Total_qty_yard ?>" readonly>
      <br>
    </div>
  </div>
  <br><br>
  <div class="banner">
    <h2>List Barang</h2>
    <br><br>
    <table class="display" id="tabelfsdfsf" width="100%" style="background-color: #254283; font-size: 12px;">
      <thead style="color: #fff">
        <tr>
          <th class="center" style="width: 5%">No</th>
          <th style="width: 35%">Nama Barang</th>
          <th style="width: 5%">Qty Pesanan</th>
          <th style="width: 5%">Qty Terima</th>
          <th style="width: 25%">Harga Satuan</th>
          <th style="width: 25%">Total Harga</th>
        </tr>
      </thead>
      <tbody id="previewdata">
      	
      </tbody>
    </table>

    <table id="sementara" width="100%" style="background-color: #254283; font-size: 12px; display: none;">
      <thead style="color: #fff">
        <tr>
          <th>Harga</th>
          <th>Nama Barang</th>
        </tr>
      </thead>
      <tbody id="tablesementara">
      </tbody>
    </table>

  </div>
  <div class="banner">
    <div class="container">
      <div class="col-md-6">
        <label>Keterangan</label>
        <textarea id="Keterangan" name="Keterangan" class="form-control" placeholder="Keterangan" rows="9"  style="resize: none;"><?php echo $header->Keterangan ?></textarea>
      </div>
      <div class="col-md-6">
        <?php  ?>
        <label style="float: left;" id="juduldpp">DPP</label>
        <input type="text" style="width: 70%" id="DPP" name="DPP" value="<?php echo number_format($grand->DPP,0,',','.') ?>" class="form-control text-right" placeholder="DPP" readonly>
        <input type="hidden" id="Discount_total" name="Discount_total" class="form-control" placeholder="Discount" readonly>
        <br><br><br>
        <?php ?>
        <label style="float: left;" id="judulppn">PPN</label>
        <input type="text" style="width: 70%" id="PPN" name="PPN" value="<?php echo number_format($grand->PPN,0,',','.') ?>" class="form-control text-right" placeholder="PPN" readonly>
        <?php if($namauser=='Administrator'){ ?>
        <br><br><br>
        <label style="float: left;">Diskon %</label>
        <input type="text" style="width: 70%" id="diskonpersen" name="diskonpersen" value="<?php echo number_format($grand->diskonpersen,0,',','.') ?>" class="form-control text-right" onkeyup="hitungpersen()">
        <br><br><br>
        <label style="float: left;">Diskon Rupiah</label>
        <input type="text" style="width: 70%" id="diskonrupiah" name="diskonrupiah" value="<?php echo number_format($grand->diskonrupiah,0,',','.') ?>" class="form-control text-right" onkeyup="hitungrupiah()">
        <br><br><br>
        <?php } ?>
        <input type="hidden" style="width: 70%" id="total_invoice" name="total_invoice" value="<?php echo number_format($grand->Grand_total,0,',','.') ?>" class="form-control text-right" placeholder="Total Invoice" readonly>
        <label style="float: left;">Total Invoice</label>
        <input type="text" style="width: 70%" id="total_invoice_diskon" name="total_invoice_diskon" value="<?php echo number_format($grand->Grand_total,0,',','.') ?>" class="form-control text-right" placeholder="Total Invoice Setelah Diskon" readonly>
      </div>
    </div>
  <br>
  <div class="banner text-center">
    <a class="btn btn-primary" id="simpan" onclick="saveInvoice()" style="cursor: pointer;">Simpan</a>
  </div>   
  </div>
  <br><br>
</form>
</div>
<script type="text/javascript">
  $(document).ready(function() {
        $('#tabelfsdfsf').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });

        $('#sementara').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false,
            "paging": false            
        });

        cek_nomor_pb();
        insertdata();
                
            
    })


  var base_url = window.location.pathname.split('/');
  $("#Jatuh_tempo").prop("disabled", true);
  $("#Discount").prop("disabled", true);
  $("#namacoa").prop("disabled", true);

  function customize_tempo(){
    if (document.getElementById('customize').checked)
    {
      $("#Jatuh_tempo").prop("disabled", false);
    } else {
      $("#Jatuh_tempo").prop("disabled", true);
    }
  }

  function discount_inv(){
    if (document.getElementById('disc').checked)
    {
      $("#Discount").prop("disabled", false);
    } else {
      $("#Discount").prop("disabled", true);
    }
  }
  function jenis_pembayaran(){
    if (document.getElementById('jenis2').checked)
    {
      $("#namacoa").prop("disabled", false);
    } else if (document.getElementById('jenis3').checked){
      $("#namacoa").prop("disabled", false);
    } else if (document.getElementById('jenis').checked){
      $("#namacoa").prop("disabled", true);
    }
  }
  function hitung() {
    var a = new Date($("#Tanggal").val());
    var b = $("#Jatuh_tempo").val();

    var today = new Date(a.getTime()+(b*24*60*60*1000));
    var dd = today.getDate();
    var mm = today.getMonth()+1;

    var yyyy = today.getFullYear();
    if(dd<10){
      dd='0'+dd;
    } 
    if(mm<10){
      mm='0'+mm;
    } 
    var today = yyyy+'-'+mm+'-'+dd;
    $("#Tanggal_jatuh_tempo").val(today);
  }
</script>
<script type="text/javascript">
  function Penerimaan(){
    //this.Barcode;
    this.Nama_Barang;
    this.Corak;
    this.Warna;
    this.Merk;
    this.Qty_yard;
    this.Qty_meter;
    this.Grade;
    this.Satuan;
    this.NoPO;
    this.NoSO;
    this.Party;
    this.Indent;
    this.Lebar;
    this.Tanggal;
    this.NoPB;
    this.ID_supplier;
    this.NoSJ;
    this.Remark;
    this.Keterangan;
    this.Total_yard;
    this.Total_meter;
    this.Harga;
    this.Sub_total;
    this.IDSupplier;
    this.Nama;
  }

  var PM = new Array();
  index = 0;
  totaldpp=0;

  function cek_nomor_pb() {
    var penerimaan= $("#no_penerimaan").val();
    console.log(penerimaan);

    $.ajax({
      url : '/'+base_url[1]+'/getnomorpb',
      type: "POST",
      data: {"_token": "{{ csrf_token() }}",jenis_tbs:penerimaan},
      dataType:'json',
      success: function(data)
      { 
        var html = '';
        if (data <= 0 || data == null || data =="") {
          html = '<option value="">--Data Kosong--</option>';
        } else {
          html += '<option value="">--Silahkan Pilih PB--</option>';
          for (var i = 0 ; i < data.length; i++) {
            html +='<option value="'+data[i]["Nomor"]+'"> '+data[i]["Nomor"]+' </option>';
          }

        }
        $("#Nomor").html(html);
        
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  }

  var PM2 = [];
  function insertdata()
  {

      var Nomor = $('#IDFB').val();
      console.log(Nomor);
             
        $.ajax({
                  url : "{{url('InvoicePembelian/ambilinvoicedetail')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",Nomor:Nomor},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=6 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Detail Tidak Ada', 'warning');
                      } else {
                        console.log("BERHASIL AMBIL DATA");
                        console.log(data);
                        totalqtyyard=0;
                        totalqtymeter=0;
                        totaldpp=0;
                         $("#previewdata").html("");
                         for(i = 0; i<data.length;i++){

                          PM2[i] = data[i];
                          if(PM2[i].IDMataUang=='P000005'){
                            var isian1 = "<input type=text style='width: 20%; margin-top: 12px;' value='$.' readonly><input style='width: 80%; text-align: right;' type=text name=harga_satuan value="+rupiah(PM2[i].Harga)+" id=harga_satuan"+i+" onkeyup=hitung_total("+i+") onchange=insertin("+i+") readonly>&nbsp;<input type=hidden name=nama_brg value='"+PM2[i].Nama_Barang+"' id=nama_brg"+i+">";
                            var isian2 = "<input type=text style='width: 20%' value='$.' readonly><input style='width: 80%; text-align: right;' type=text name=harga_total_b id=harga_total_b"+i+" value="+rupiah(PM2[i].Harga*PM2[i].Qty_meter)+" readonly> <input type=hidden name=harga_total id=harga_total"+i+"  value="+PM2[i].Harga*PM2[i].Qty_meter+">";
                          }else{
                            var isian1 = "<input type=text style='width: 20%; margin-top: 12px;' value='Rp.' readonly><input style='width: 80%; text-align: right;' type=text name=harga_satuan value="+rupiah(PM2[i].Harga)+" id=harga_satuan"+i+" onkeyup=hitung_total("+i+") onchange=insertin("+i+") readonly>&nbsp;<input type=hidden name=nama_brg value='"+PM2[i].Nama_Barang+"' id=nama_brg"+i+">";
                            var isian2 = "<input type=text style='width: 20%' value='Rp.' readonly><input style='width: 80%; text-align: right;' type=text name=harga_total_b id=harga_total_b"+i+" value="+rupiah(PM2[i].Harga*PM2[i].Qty_meter)+" readonly> <input type=hidden name=harga_total id=harga_total"+i+"  value="+PM2[i].Harga*PM2[i].Qty_meter+">";
                          }
                          var value =
                          "<tr>" +
                          "<td>"+(i+1)+"</td>"+
                          "<td>"+PM2[i].Nama_Barang+"</td>"+
                          "<td>"+PM2[i].Qty_yard+"</td>"+
                          "<td>"+PM2[i].Qty_meter+"</td>"+
                          "<td>"+isian1+"</td>"+
                          "<td>"+isian2+"</td>"+
                          "</tr>";
                          $("#previewdata").append(value);

                          $('#harga_satuan'+i).val(rupiah(PM2[i].Harga));
                          namasupplier= PM2[i].Nama;
                          ppnchosen=PM2[i].pilihanppn;
                          idsupplier= PM2[i].IDSupplier;
                          totalqtyyard+= parseFloat(PM2[i].Qty_yard);
                          totalqtymeter+= parseFloat(PM2[i].Qty_meter);

                            totaldpp += parseFloat(PM2[i].Harga*PM2[i].Qty_meter);
                            console.log(i+'=-=-=-= '+ parseFloat(PM2[i].Harga*PM2[i].Qty_meter));
                          // if(!isNaN(parseFloat(PM2[i].Harga*PM2[i].Qty_meter))){
                          //    totaldpp += parseFloat(PM2[i].Harga*PM2[i].Qty_meter);
                          //    console.log(i+'=-=-=-= '+ parseFloat(PM2[i].Harga*PM2[i].Qty_meter));
                          // }
                          console.log('==============Hitung Ini===============');
                          console.log(PM2[i].pilihanppn);
                           
                          
                        }
                     }
                }
            });
  }

 var HJB = [];
 function insertin(i)
 {
 	var harga = $('#harga_satuan'+i).val();
 	var nama_brg = $('#nama_brg'+i).val();

 	field = {
          "harga"      : $('#harga_satuan'+i).val(),
          "nama_brg"    : $('#nama_brg'+i).val(),
        }

 	HJB.push(field);
        setTimeout(function(){
          renderJSONS(HJB);
          field = null;
        },500);
 }



 	function renderJSONS(data){
        console.log('SUKSES KIRIM');
                
        if(data != null){

         PM2 = [];           
           var table = $('#sementara').DataTable();
           table.clear().draw();
           for(i = 0; i<data.length;i++){

            PM2[i] = data[i];
            console.log(PM2[i].nama_brg); 

            table.row.add([
              PM2[i].harga,
              PM2[i].nama_brg,

              ]).draw().nodes().to$().addClass('rowarray'+i);
        }      
         
        }
      }

 function ubah_status_ppn()
 {
  disc = $("#Discount").val();
  if($("#Status_ppn").val()=="exclude"){
    // ppn= $("#Total").val()*0.1;
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(Math.round(dpp)));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="include"){

    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah(Math.round($("#Total").val())));
      $("#PPN").val(rupiah(Math.round(ppn)));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }

    
  }else{
    ppn= 0;
    $("#DPP").val(rupiah(Math.round($("#Total").val())));
    $("#PPN").val(rupiah(Math.round(ppn)));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }
}

function ubah_discount()
{
  disc = $("#Discount").val();
  if($("#Status_ppn").val()=="exclude"){
    if(disc==0){
      dpp=  100/110*$("#Total").val();
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(dpp));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      dpp=  100/110*($("#Total").val()-disc);
      ppn= dpp*0.1;
      $("#DPP").val(rupiah(dpp));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      total= $("#Total").val()-ppn;
      $("#total_invoice").val(rupiah(total-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }else if($("#Status_ppn").val()=="NON PPN"){
    ppn= 0;
    $("#DPP").val(rupiah($("#Total").val()));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= $("#Total").val()-ppn;
    $("#total_invoice").val(rupiah(total-disc+ppn));
    $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
  }else{
    if(disc == 0){
      ppn= $("#Total").val()*0.1;
      $("#DPP").val(rupiah($("#Total").val()));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }else{
      ppn= ($("#Total").val()-disc)*0.1;
      $("#DPP").val(rupiah($("#Total").val()));
      $("#PPN").val(rupiah(ppn));
      $("#Discount_total").val(rupiah(disc));
      $("#total_invoice").val(rupiah($("#Total").val()-disc+ppn));
      $("#total_invoice_pembayaran").val(rupiah($("#Total").val()-disc+ppn));
    }
  }
}

function hitung_total(i) {
  var tanpa_rupiah_debit_5 = document.getElementById('harga_satuan'+i);
  tanpa_rupiah_debit_5.addEventListener('keyup', function(e)
  {
    tanpa_rupiah_debit_5.value = formatRupiah2(this.value);
  });

  tanpa_rupiah_debit_5.addEventListener('keydown', function(event)
  {
    limitCharacter(event);
  });

  var harga_satuan1= $("#harga_satuan"+i).val();
  var a = PM[i].Qty_yard;
  var b = harga_satuan1.replace(".","");
  var c=a*b;
  $("#harga_total_b"+i).val(rupiah(c));
  $("#harga_total"+i).val(c);

  totaldpp=0;
  for(l=0; l < index; l++){

    totaldpp += parseFloat($("#harga_total"+l).val());
  }
  disc=  $("#Discount").val();
  if($("#Status_ppn").val()=="exclude"){

    ppn= totaldpp*0.1;
    $("#DPP").val(rupiah(totaldpp));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= totaldpp-ppn;
    $("#total_invoice").val(total-disc+ppn);
    $("#total_invoice_pembayaran").val(rupiah(total-disc+ppn));
  }else if($("#Status_ppn").val()=="include"){

    ppn= totaldpp*0.1;
    $("#DPP").val(rupiah(totaldpp));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    $("#total_invoice").val(totaldpp-disc+ppn);
    $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
    
  }else{
    ppn= 0;
    $("#DPP").val(rupiah(totaldpp));
    $("#PPN").val(rupiah(ppn));
    $("#Discount_total").val(rupiah(disc));
    total= totaldpp-ppn;
    $("#total_invoice").val(total-disc+ppn);
    $("#total_invoice_pembayaran").val(rupiah(totaldpp-disc+ppn));
  }

  console.log('CEK INPUTAN');

  var z = $('#harga_satuan'+i).val();
  console.log(z);
  $('#harga_satuan2'+i).val(z);


  

}
function fieldInvoice(){
  var data1 = {
    "id_invoice" : $("#id_invoice").val(),
    "Tanggal" :$("#Tanggal").val(),
    "Nomor_invoice":$("#Nomor_invoice").val(),
    "IDTBS" : $("#no_penerimaan").val(),
    "Nomor":$("#Nomor").val(),
    "IDSupplier":$("#id_supplier").val(),
    "Jatuh_tempo":$("#Jatuh_tempo").val(),
    "Tanggal_jatuh_tempo":$("#Tanggal_jatuh_tempo").val(),
    "No_supplier":$("#No_supplier").val(),
    "Status_ppn":$("#Status_ppn").val(),
    "Discount":$("#Discount").val(),
    "Keterangan":$("#Keterangan").val(),
    "Total_qty_yard":$("#Total_qty_yard").val(),
    "Total_qty_meter":$("#Total_qty_meter").val(),
    "IDFB":$("#IDFB").val(),
  }
  return data1;
}
function fieldgrandtotal(){
  var data3 = {
    "id_invoice_grand_total" : $("#id_invoice_grand_total").val(),
    "id_pembayaran" : $("#id_pembayaran").val(),
    "jenis" :$("#jenis").val(),
    "jenis" :$("#jenis2").val(),
    "jenis" :$("#jenis3").val(),
    "DPP" :$("#DPP").val(),
    "Discount" :$("#Discount").val(),
    "PPN" :$("#PPN").val(),
    "total_invoice_pembayaran" :$("#total_invoice").val(),
    "nominal" :$("#nominal").val(),
    "tgl_giro" :$("#tgl_giro").val(),
    "namacoa" : $("#namacoa").val(),
    "diskonrupiah" : $("#diskonrupiah").val(),
    "diskonpersen" : $("#diskonpersen").val(),
    "total_invoice_diskon" : $("total_invoice_diskon").val(),
  }
  return data3;
}

function harga_satuan_total(i)
{
  for(i=0; i < index; i++){
    PM[i].harga_satuan = PM[i].Harga;
    PM[i].harga_total = parseFloat(PM[i].Harga)*parseFloat(PM[i].Qty_yard);
}
}

function saveInvoice(){
  


  var data1 = fieldInvoice();
  var data3 = fieldgrandtotal();
  var data6 = harga_satuan_total();

 $("#Nomor").css('border', '');
 $("#No_Supplier").css('border', '');

 if($("#Nomor").val()==""){
   $('#alert').html('<div class="pesan sukses">Data PB Tidak Boleh Kosong</div>');
   $("#Nomor").css('border', '1px #C33 solid').focus();
 }else if($("#No_supplier").val()==""){
   $('#alert').html('<div class="pesan sukses">No Supplier Tidak Boleh Kosong</div>');
   $("#No_supplier").css('border', '1px #C33 solid').focus();
 }else{


   $.ajax({

     url: "simpan_invoice_pembelianedit",
     type: "POST",
     data: {

       "_token": "{{ csrf_token() }}",
       "data1" : data1,
       "data2" : PM2,
       "data3" : data3
     },

     dataType: 'json',

     success: function (data) {
       swal({
            title: "Berhasil!",
            text: "Data Berhasil Disimpan!",
            icon: "success"
        }).then(function() {
            window.location = "{{url('InvoicePembelian')}}";
        });

     },
     error: function(msg, status){
       console.log(msg);
       swal('Perhatian', 'Isi Data Dengan Lengkap', 'warning');

     }

   });
 }
}

  function delay(ms) {
        var cur_d = new Date();
        var cur_ticks = cur_d.getTime();
        var ms_passed = 0;
        while(ms_passed < ms) {
            var d = new Date();  // Possible memory leak?
            var ticks = d.getTime();
            ms_passed = ticks - cur_ticks;
            // d = null;  // Prevent memory leak?
        }
    }

function cek_supplier()
{
  var totalinv=0;
  var Nomor = $("#NomorB").val();
  $.ajax({
    url : '/'+base_url[1]+'/cek_supplier',
    type: "POST",
    data:{Nomor:Nomor},
    dataType:'json',
    success: function(data)
    { 
      var html = '';
      if (data <= 0) {
        console.log('-----------data kosong------------');
      } else {
        $("#NamaSupplier").val(data[0].Nama);
        $("#IDSupplier").val(data[0].IDSupplier);
      }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
}



function formatRupiah2(bilangan, prefix)
{
  var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
  split   = number_string.split(','),
  sisa    = split[0].length % 3,
  rupiah  = split[0].substr(0, sisa),
  ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? '.' : '';
    rupiah += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function isidiskon()
{
  $('#diskonpersen').val('0');
  $('#diskonrupiah').val('0'); 
}



function hitungpersen()
{
  var a = $('#diskonpersen').val();
  console.log(a);
  var b = $('#total_invoice').val().replace('.','');
  console.log(b);

  var c = 0;
  c = (b*a) / 100;
  console.log(c);

  $('#diskonrupiah').val(rupiah(c));

  var d = 0;
  d = parseInt(b)-parseInt(c);

  console.log(d);
  $('#total_invoice_diskon').val(rupiah(d));


}

function hitungrupiah()
{
  var a = $('#diskonrupiah').val();
  console.log(a);
  var b = $('#total_invoice').val().replace('.','');
  console.log(b);

  var c = 0;
  c = (a/b)*100;
  console.log(c);

  $('#diskonpersen').val(rupiah(c));

  var d = 0;
  d = b-a;
  $('#total_invoice_diskon').val(rupiah(d));
}

</script>
@endsection
