<!-- ===========Create By Dedy 20-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>

<div class="main-grid">
  <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('UpdatePajakPembelian')}}">Data Update No Pajak</a>
        </h2>
  </div><!-- banner -->
  <br>
  <?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
  <div class="banner">
      <br>
      <div class="col-md-2">
          <label class="field_title mt-dot2">Periode</label>
              <div class="form_input">
                  <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
              </div>
      </div>
      <div class="col-md-2">
          <label class="field_title mt-dot2"> S/D </label>
          <div class="form_input">
              <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
          </div>
      </div>
      <div class="col-md-3">
          <div class="form_grid_2">
              <label class="field_title mt-dot2">Cari Berdasarkan</label>
              <select data-placeholder="Cari Customer" class="chosen-select" name="field" id="field" required style="width: 100%">
                  <option value="">- Pilih -</option>
                  <option value="tbl_pembelian.Nomor">Nomor PO</option>
                  <option value="tbl_supplier.Nama">Customer</option>
                  <option value="tbl_pembelian.Batal">Status</option>
              </select>
          </div>
      </div>
      <div class="col-md-3">
          <div class="form_grid_2">
              <label class="field_title mt-dot2">Keyword</label>
              <div id="pilihan">
                  <input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">
              </div>
          </div>
      </div>
      <div class="col-md-2">
          <div class="form_grid_2">
              <label>&nbsp;.</label>
              <br>
              <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
          </div>
      </div>
    <!-- </form> -->
    <br><br><br>
  </div>
  <br>
  <div class="banner">
    <form action="{{url('InvoicePembelian/ubahstatus')}}" method="post" id="ubahstatus">
      <div class="widget_content">
        <table id="table-data" class="display" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
          <thead style="color: #fff">
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>No Faktur</th>
              <th>No Invoice Supplier</th>
              <th>No Pajak</th>
              <!-- <th>No Penerimaan</th> -->
              <th>Nama Supplier</th>
              <!-- <th>Jatuh Tempo</th> -->
              <th>Total QTY</th>
              <th>Total Harga</th>
              <th>Status</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot style="color: #fff">
              <tr>
                  <th colspan="6" style="text-align: right;">
                      Total
                  </th>
                  <th style="text-align: center;"></th>
                  <th></th>
                  <th></th>
              </tr>
          </tfoot>
        </table>
      </div>
    </form>
  </div>
</div>

<div id="modal-data" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <form id="form-data">
        {{ csrf_field() }}
        <!-- Modal content-->
        
      </form>
    </div>
  </div>

    <script type="text/javascript">
      $(document).ready(function() {
                //====================================datatables
                table = $('#tablefs').DataTable({
                    "searching": false,
                    "lengthChange": false
                });
                @if (session('alertakses'))
                    swal("Berhasil", "{{ session('alertakses') }}", "success");
                @endif
            });

      var PM = [];
    function caridata()
    {
        var jenis       = $('#jenispencarian').val();
        var keyword     = $('#keyword').val();
        var date_from   = $('#date_from').val();
        var date_until  = $('#date_until').val();
            $.ajax({
                  url : "{{url('InvoicePembelian/pencarian')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];


                      if(PM[i].Batal=='aktif'){
                         var $status  = "<span><a href='InvoicePembelian/show/"+PM[i].IDFB+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='InvoicePembelian/printfb/"+PM[i].IDFB+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>&nbsp;<a href='InvoicePembelian/edit/"+PM[i].IDFB+"'title='Edit Data'>&nbsp;<i class='fa fa-pencil fa-lg'></i></a></span>&nbsp;<span><a data-toggle='modal' data-target='#modalBatal"+PM[i].IDFB+"' title='Batalkan.?'><i class='fa fa-times-circle fa-lg' style='color: red'></i></a></span>";
                      }else{
                         var $status  = "<span><a href='InvoicePembelian/show/"+PM[i].IDFB+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='InvoicePembelian/printfb/"+PM[i].IDFB+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>";
                      }

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+

                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td>"+PM[i].Saldo_yard+"</td>"+
                      "<td>"+formatDate(PM[i].Tanggal_jatuh_tempo)+"</td>"+
                      "<td>Rp. "+rupiah(PM[i].Sub_total)+"</td>"+
                      "<td>"+PM[i].Batal+"</td>"+
                      "<td style='text-align:center;'>"+$status+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }

                     }
                }
            });
    }
    </script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("InvoicePembelian.datatable") }}';
    var url = '{{ url("InvoicePembelian") }}';
    var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/invoicepembelian/invoicepembelian_pajak.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('alertakses'))
            swal("Perhatian", "{{ session('alertakses') }}", "warning");
        @endif
    })
</script>
@endsection
