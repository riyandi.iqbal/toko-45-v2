<!-- ===========Create By Dedy 11-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<?php
if ($kode->curr_number == null) {
  $number = 1;
} else {
  $number = $kode->curr_number + 1;
}

$kodemax = str_pad($number, 2, "0", STR_PAD_LEFT);
$code = 'KT-'.$kodemax;
?>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Kategori')}}">Data Kategori</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Kategori</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Kategori</span>
    </div>
    <div class="banner">
      <form id="form-data">
    	<div class="container">
      <br><br>
        <div class="col-md-3">
          <label class="judul">Group Barang</label>
        </div>
        <div class="col-md-9">
            <select data-placeholder="Pilih Group Barang" name="IDGroupBarang" id="IDGroupBarang" onchange="cekgroup()" style=" width:70%;" class="chosen-select" tabindex="13">
                <option value=""></option>
                <?php foreach($groupbarang as $group)
                    {
                        echo "<option value='$group->IDGroupBarang' data-kode='$group->Kode_Group_Barang'>$group->Group_Barang</option>";
                    } 
                ?>
            </select>
            <div class="flot-right btn btn-primary hvr-icon-float-away">
                <a data-toggle="modal" data-target="#modaladd"><span style="color: white;">Tambah Group</span></a>
            </div>
          <br><br>
        </div>
        <div class="col-md-3">
          <label class="judul">Kode Kategori</label>
        </div>
        <div class="col-md-9">
          <input type="text" class="form-control" name="Kode_Kategori" id="Kode_Kategori" value="<?php echo $code ?>" readonly>
          <br><br>
        </div>
    		<div class="col-md-3">
    			<label class="judul">Nama Kategori</label>
    		</div>
    		<div class="col-md-9">
    			<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama_Kategori" id="Nama_Kategori">
    			<br><br>
    		</div>
    		<div class="col-md-3">
    			<label class="judul">Status</label>
    		</div>
    		<div class="col-md-9">
    			<input type="radio" name="Status" id="Status1" value="aktif" checked>Aktif &nbsp;
    			<input type="radio" name="Status" id="Status0" value="nonaktif">Non Aktif
    		</div>
    	</div>
      <div class="text-center">
        <div class="btn col-11 hvr-icon-back">
          <span> <a style="color: white;" href="{{url('Kategori')}}" name="simpan">Kembali</a></span>
        </div>
        <div class="btn">
            <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
        </div>
    </div>
  </form>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Kategori") }}';
  var urlInsert           = '{{ route("Kategori.store") }}';
  var url                 = '{{ url("Kategori") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/kategori/create.js') }}"></script>
@endsection 