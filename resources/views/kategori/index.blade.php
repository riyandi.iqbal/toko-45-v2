<!-- ===========Create By Dedy 11-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Kategori')}}">Data Kategori</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="form_grid_3">
            <a href="{{url('Kategori/create')}}">
                <div class="btn btn-primary hvr-icon-float-away">
                    <span style="color: white;">Tambah Data&nbsp;&nbsp;</span>
                </div>
            </a>
            </div>
            <br>
            <div class="col-md-4">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Nama Kategori" class="form-control">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
    	<table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
    			<th>No</th>
          <th>Kode Kategori</th>
    			<th>Nama Kategori</th>
    			<th>Status</th>
    			<th>Aksi</th>
    		</thead>
    	</table>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/kategori/index.js') }}"></script>
<script>
    var urlData = '{{ route("Kategori.datatable") }}';
    var url                 = '{{ url("Kategori") }}';
</script>
@endsection 