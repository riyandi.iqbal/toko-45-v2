<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<?php 
    use App\Helpers\AppHelper;
?>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('ReturPenjualan.index')}}">Data Retur Penjualan</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Edit Retur Penjualan</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Retur Penjualan</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td style="width: 35%;">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control form-white datepicker" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date_format(date_create($penjualan->Tanggal), 'd/m/Y') }}">
                        <br>
                        <label for="Nomor">Nomor Invoice</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Invoice Penjualan" readonly value="{{ $penjualan->Nomor }}">
                        <br>
                        <label for="IDFJ">Nomor Invoice Penjualan</label>
                        <select name="IDFJ" id="IDFJ" class="form-control select2" style="width: 100%;">
                            <option value="{{ $penjualan->IDFJ }}"> {{ $penjualan->Fj_nomor }} </option>
                        </select>
                    </td>
                    <td style="width: 35%;">
                        <label for="Status_ppn">Status PPN</label><br>
                        <input type="text" name="Status_ppn" id="Status_ppn" class="form-control" readonly value="{{ $penjualan->Status_ppn }}">
                        <br>
                        <label for="IDCustomer">Customer</label><br>
                        <input type="text" name="NamaCustomer" id="NamaCustomer" class="form-control" readonly value="{{ $penjualan->Nama }}" >
                        <input type="hidden" name="IDCustomer" id="IDCustomer" value="{{ $penjualan->IDCustomer }}">
                        <br>
                        <label for="Total_qty">Total</label>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly value="{{ $penjualan->Total_qty }}">
                        <br>   
                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 50px;">
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                </div> 
                <div class="container" style="width: 100%">
                    <br>
                    <table class="table table-bordered" id="table-barang" style="font-size: 12px; margin-top: 10px;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th>No</th>
                                <th style="width: 45%">Nama Barang</th>
                                <th style="width: 10%">Qty</th>
                                <th style="width: 15%">Harga</th>
                                <th style="width: 15%">Satuan</th>
                                <th style="width: 15%">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($penjualan_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Nama_Barang }} </td>
                                    <td align="right"> {{ $item->Qty }} </td>
                                    <td align="right"> {{ AppHelper::NumberFormat($item->Harga) }} </td>
                                    <td align="center"> {{ $item->Satuan }} </td>
                                    <td align="right"> {{ AppHelper::NumberFormat($item->Sub_total) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 50%; vertical-align: top;">
                                <label for="Keterangan">Keterangan</label>
                                    <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5" required>{{ $penjualan->Keterangan }}</textarea>
                            </td>
                            <td style="padding-left: 10px;">
                                <label style="float: left;" id="juduldpp">DPP</label>
                                <input type="text" style="width: 70%" id="DPP" name="DPP" class="form-control text-right total-add" placeholder="DPP" readonly value="{{ AppHelper::NumberFormat($penjualan->DPP, 2) }}">
                                <br><br><br>
                                <?php ?>
                                <label style="float: left;" id="judulppn">PPN</label>
                                <input type="text" style="width: 70%" id="PPN" name="PPN" class="form-control text-right total-add" placeholder="PPN" readonly value="{{ AppHelper::NumberFormat($penjualan->PPN, 2) }}">
                                <br><br><br>
                                <label style="float: left;" for="total_invoice_diskon">Total Invoice</label>
                                <input type="text" style="width: 70%" id="total_invoice_diskon" name="total_invoice_diskon" class="form-control text-right" placeholder="Total Invoice" readonly value="{{ AppHelper::NumberFormat($penjualan->Grand_total) }}">
                            </td>
                        </tr>
                    </table>
                    
                    <button class="btn btn-primary" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                    <div class="btn col-16">
                        <span> <a style="color: white; float: right;" href="{{ route('ReturPenjualan.index') }}">Kembali</a></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("ReturPenjualan.index") }}';
    var urlNumberInvoice    = '{{ route("ReturPenjualan.number") }}';
    var urlInsert           = '{{ route("ReturPenjualan.store") }}';
    var urlPenjualan       = '{{ route("ReturPenjualan.penjualan") }}';
    var urlPenjualanDetail       = '{{ route("ReturPenjualan.penjualan_detail") }}';
    var url                 = '{{ url("ReturPenjualan") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/returpenjualan/returpenjualan_update.js') }}"></script>
<script>
    
</script>
@endsection