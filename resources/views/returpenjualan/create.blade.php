<!-- ===========Create By Tiar 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
        font-size: 14px;
    }
</style>

<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('ReturPenjualan.index')}}">Data Retur Penjualan</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Retur Penjualan</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Retur Penjualan</span>
    </div>

    <div class="bannerbody">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td style="width: 35%;">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control form-white datepicker" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date('d/m/Y') }}">
                    </td>
                    <td style="width: 35%;">
                        <label for="Nomor">Nomor Retur</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Retur" readonly>
                    </td>
                    <td style="width: 30%;">
                        <label for="IDCustomer">Customer</label>
                        <select name="IDCustomer" id="IDCustomer" class="form-control select2" style="width: 100%;" reaquired>
                            <option value="">-- Pilih Customer --</option>
                            @foreach ($customer as $item)
                                <option value="{{ $item->IDCustomer }}" data-group="{{ $item->IDGroupCustomer }}"> {{ $item->Nama }} </option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Status_ppn">Jenis PPN</label><br>
                        <input type="radio" name="Status_ppn" id="ppn1" value="include" checked onclick="ambilnomorrjnonppn()"> <label for="ppn1"> Include PPN </label> &nbsp;
                        <input type="radio" name="Status_ppn" id="ppn0" value="exclude" onclick="ambilnomorrjppn()"> <label for="ppn0"> Exclude PPN </label>
                    </td>
                    <td style="vertical-align: middle;">
                        <input type="checkbox" name="Pilih_invoice" class="checked" id="Pilih_invoice">
                        <label for="Pilih_invoice"> Pilih No. Invoice </label>
                    </td>
                    <td>
                        <label for="IDFJ">Nomor Invoice Penjualan</label>
                        <select name="IDFJ" id="IDFJ" class="form-control select2" style="width: 100%;" disabled>
                            <option value="">- Pilih Invoice Penjualan -</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="Pilihan_retur">Pilihan Retur</label>
                        <select name="Pilihan_retur" id="Pilihan_retur" class="form-control select2" style="width: 100%;" required>
                            <option value="">- Pilih -</option>
                            <option value="TM">Tukar Masuk</option>
                            <option value="RETUR">Retur</option>
                        </select>
                    </td>
                    <td>
                        <label for="IDGudang">Gudang Retur</label>
                        <select name="IDGudang" id="IDGudang" class="form-control select2" style="width: 100%;" readonly>
                            <option value="P000004" selected>Gudang Retur</option>
                        </select>
                    </td>
                    <td>

                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 20px;">
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                </div> 
                <div class="container" style="width: 100%">
                    <br>
                    <table class="table table-bordered" id="table-barang" style="font-size: 12px; margin-top: 10px; margin-bottom: 10px;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th style="width: 25%">Nama Barang</th>
                                <th style="width: 10%">Qty Beli</th>
                                <th style="width: 10%">Qty Retur</th>
                                <th style="width: 15%">Harga</th>
                                <th style="width: 15%">Satuan</th>
                                <th style="width: 5%">PPN</th>
                                <th style="width: 15%">Subtotal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="list-barang">

                        </tbody>
                    </table>
                    <a class="btn col-11" id="btn-add-barang" ><i class="col-11 hvr-icon-float-away">Tambah</i></a>

                    <table style="width: 100%; margin-top:10px;">
                        <tr>
                            <td style="width: 50%; vertical-align: top;">
                                <label for="Keterangan">Keterangan</label>
                                    <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"></textarea>
                            </td>
                            <td style="padding-left: 10px; vertical-align: top;">
                                <label style="float: left;" for="Total_qty">Total Qty</label>
                                <input type="text" style="width: 70%" id="Total_qty" name="Total_qty" class="form-control text-right" placeholder="Total Qty" readonly>
                                <br> <br> <br>
                                <div style="display: none;">
                                    <label style="float: left;" id="juduldpp">DPP</label>
                                    <input type="text" style="width: 70%" id="DPP" name="DPP" class="form-control text-right total-add" placeholder="DPP" readonly>
                                    <br><br><br>
                                    <?php ?>
                                    <label style="float: left;" id="judulppn">PPN</label>
                                    <input type="text" style="width: 70%" id="PPN" name="PPN" class="form-control text-right total-add" placeholder="PPN" readonly>
                                    <br><br><br>
                                </div>
                                <label style="float: left;" for="Grand_total">Total Retur</label>
                                <input type="text" style="width: 70%" id="Grand_total" name="Grand_total" class="form-control text-right" placeholder="Total Retur" readonly>
                            </td>
                        </tr>
                    </table>
                    
                    <button class="btn btn-primary" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                    <div class="btn col-16">
                        <span> <a style="color: white; float: right;" href="{{ route('ReturPenjualan.index') }}">Kembali</a></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("ReturPenjualan.index") }}';
    var urlNumberInvoice    = '{{ route("ReturPenjualan.number") }}';
    var urlInsert           = '{{ route("ReturPenjualan.store") }}';
    var urlPenjualan       = '{{ route("ReturPenjualan.penjualan") }}';
    var urlPenjualanDetail       = '{{ route("ReturPenjualan.penjualan_detail") }}';
    var urlPenjualanCustomer       = '{{ route("ReturPenjualan.get_penjualan_customer") }}';
    var url                 = '{{ url("ReturPenjualan") }}';
    var urlBarang = '{{ route("SalesOrder.get_barang") }}';
    function ambilnomorrjnonppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR RJ');
            $.ajax({
                    url : "{{url('ambilnomorrjnonppn')}}",
                    type: "GET",
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU RJ');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }

    function ambilnomorrjppn()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR RJ PPN');
            $.ajax({
                    url : "{{url('ambilnomorrjppn')}}",
                    type: "GET",
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU RJ PPN');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/returpenjualan/returpenjualan_create.js') }}"></script>
<script>
    
</script>
@endsection