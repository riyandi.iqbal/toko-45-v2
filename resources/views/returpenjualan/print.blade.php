<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Invoice Penjualan</title>
</head>
<style>
    address {
        font-style: normal;
    }
    h2, h3 {
        margin: 0px;
    }
    
    .table-print th {
        padding: .4rem;
        font-weight : bold;
        vertical-align: top;
        border-top: 1px solid #0a0a0a;
        border-bottom: 1px solid #0a0a0a;
        border-left: 1px solid #0a0a0a;
        border-right: 1px solid #0a0a0a;
    }

    .table-print td:first-child {
        border-left: 1px solid #0a0a0a;
    }

    .table-print td:last-child {
        border-right: 1px solid #0a0a0a;
    }

    .table-print { padding: .4rem; border-collapse: collapse }
    .table-print td { padding: .4rem }
</style>
<?php 
    use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<body>
    <table style="width: 100%;">
        <tr>
            <td colspan="2"> 
                <table>
                    <tr>
                            <div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
                    </tr>                
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <h2> Retur Penjualan </h2>
            </td>
        </tr>
        <tr>
            <td style="width: 50%; vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 30%">Nomor</td>
                        <td style="width: 1%"> : </td>
                        <td> {{ $penjualan->Nomor }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">Nomor Invoice Penjualan</td>
                        <td style="width: 1%"> : </td>
                        <td> {{ $penjualan->Fj_nomor }} </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td> : </td>
                        <td> {{ AppHelper::DateIndo($penjualan->Tanggal) }} </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td>Customer</td>
                        <td> : </td>
                        <td> {{ $penjualan->Nama }} </td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td> : </td>
                        <td> {{ $penjualan->Alamat }} </td>
                    </tr>
                    <tr>
                        <td>Telp</td>
                        <td> : </td>
                        <td> {{ $penjualan->No_Telpon }} </td>
                    </tr>
                    {{-- <tr>
                        <td style="width: 30%">PPN</td>
                        <td style="width: 1%"> : </td>
                        <td style="text-transform: capitalize;"> {{ $penjualan->Status_ppn }} </td>
                    </tr> --}}
                </table>
            </td>
        </tr>
        <td>
            <tr>
                <td colspan="2">
                    <table style="width: 100%" class="table-print">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Harga</th>
                                <th>Satuan</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($penjualan_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                    <td style="text-align: center;"> {{ $item->Qty }} </td>
                                    <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                    <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                    <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Sub_total, $angkakoma) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="3" rowspan="3" style="text-align: left; width: 60%;">
                                    Keterangan : <br>
                                    {{ $penjualan->Keterangan }}
                                </th>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: right;">Sub Total</th>
                                <th style="text-align: right;"> {{ AppHelper::NumberFormat($penjualan->Grand_total + $penjualan->Discount) }} </th>
                            </tr>
                            <tr>
                                <th colspan="2" style="text-align: right;">Grand Total</th>
                                <th style="text-align: right;"> {{ AppHelper::NumberFormat($penjualan->Grand_total) }} </th>
                            </tr>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </td>
    </table>
</body>
</html>

<script>
    window.onload = print();
</script>