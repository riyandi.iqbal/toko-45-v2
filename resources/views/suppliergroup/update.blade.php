<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('GroupSupplier')}}">Data Group Supplier</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Group Supplier</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Group Supplier</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Kode Group Supplier</label>
            </div>
            <div class="col-md-9">
            <input type="hidden" name="idgroup" id="idgroup" value="<?php echo $groupsupplier->IDGroupSupplier ?>">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="kodegroup" id="kodegroup" value="<?php echo $groupsupplier->Kode_Group_Supplier; ?>" required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Nama Group Supplier</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="namagroup" id="namagroup" value="<?php echo $groupsupplier->Group_Supplier; ?>" required oninvalid="this.setCustomValidity('Nama Group Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>
                
            </div>
            <div class="text-center">
                <br><br>
                <div class="btn col-11 hvr-icon-back">
                    <span> <a href="{{url('GroupSupplier')}}" style="color: white;" name="simpan">Kembali</a></span>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("GroupSupplier") }}';
  var urlUpdate           = '{{ route("GroupSupplier.update_data") }}';
  var url                 = '{{ url("GroupSupplier") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/suppliergroup/update.js') }}"></script>
@endsection 