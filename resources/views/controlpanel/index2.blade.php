@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanel')}}">Master Control Panel</a>
		</h2>
	</div>
	<br>
    <div class="bannerbody">
    <div id="content">
		        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
		            <li class="active"><a href="#tab-1" data-toggle="tab">Data Perusahaan</a></li>
		            <li><a href="#tab-2" data-toggle="tab">Logo</a></li>
                    <li><a href="#tab-3" data-toggle="tab">Kode Transaksi</a></li>
		        </ul>

		        <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active" id="tab-1">
						<br>
		            	<br>
		            	<br>
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Setting Data Perusahaan</span>	
		            	</div> 
		            	<div class="container" style="width: 100%">
		            		<br>
                            <table style="width: 100%">
                                <tr> 
                                    <td style="width: 45%"><label class="judul">Nama Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><label class="judul">Font Family</label></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="nama" name="nama" value="<?php echo $perusahaan->Nama; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><select data-placeholder="Pilih Font" style="width: 100%" class="chosen-select" name="font1" id="font1" required oninvalid="this.setCustomValidity('Font Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                            <?php 
                                                            foreach ($DataFont as $font ) { 
                                                                if ($font->Nama == $perusahaan->Font) { ?>
                                                                <option value="<?= $font->Nama ?>" selected style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                                                                <?php }else{
                                                                ?>
                                                                <option value="<?= $font->Nama ?>" style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                                                                <?php }}
                                                                ?>
                                                            </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Alamat Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><label class="judul">Font Size</label></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="alamat" name="alamat" value="<?php echo $perusahaan->Alamat; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><select data-placeholder="Pilih Font Size" style="width: 25%" class="chosen-select" name="font2size" id="font2size" required oninvalid="this.setCustomValidity('Font Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                            <?php 
                                                            foreach ($DataFontSize as $fonts ) { 
                                                                if ($fonts->Angka == $perusahaan->FontSize) { ?>
                                                                <option value="<?= $fonts->Angka ?>" selected style="font-size: <?= $fonts->Angka ?>px"> <?= $fonts->Angka ?></option> 
                                                                <?php }else{
                                                                ?>
                                                                <option value="<?= $fonts->Angka ?>" style="font-size: <?= $fonts->Angka ?>px"> <?= $fonts->Angka ?></option> 
                                                                <?php }}
                                                                ?>
                                                            </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Kecamatan Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><label class="judul">Font Style</label></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="kec" name="kec" value="<?php echo $perusahaan->Kecamatan; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><select data-placeholder="Pilih Font Style" style="width: 50%" class="chosen-select" name="font3style" id="font3style" required oninvalid="this.setCustomValidity('Font Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                            <?php 
                                                            foreach ($DataFontStyle as $font2 ) { 
                                                                if ($font2->Style == $perusahaan->FontStyle) { ?>
                                                                    <option value="<?php echo $font2->Style ?>" selected style="font-style: <?= $font2->Style ?>"> <?= $font2->Style ?></option> 
                                                                <?php }else{
                                                                ?>
                                                                    <option value="<?php echo $font2->Style ?>" style="font-style: <?= $font2->Style ?>"> <?= $font2->Style ?></option> 
                                                                <?php }}
                                                                ?>
                                                            </select>
                                    
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Kota</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><select data-placeholder="Pilih Kota" style=" width:60%;" class="chosen-select" name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                                                            <option value=""></option>
                                                            <?php 
                                                            foreach ($DataKota as $key ) { 
                                                            if ($key->IDKota == $perusahaan->Kota) { ?>
                                                            <option value="<?= $key->IDKota ?>" selected> <?= $key->Kota ?></option> 
                                                            <?php }
                                                            ?>
                                                            <option value="<?= $key->IDKota ?>"> <?= $key->Kota ?></option> 
                                                            <?php }
                                                            ?>
                                                        </select>
                                                    
                                                            <div class="flot-right btn btn-primary hvr-icon-float-away">
                                                                <a data-toggle="modal" data-target="#modalkota"><span style="color: white;">Tambah Kota&nbsp;&nbsp;&nbsp;</span></a>
                                                            </div>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Telepon Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%">Jumlah Koma Dibelakang Angka</td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" class="form-control" id="tel" name="tel" value="<?php echo $perusahaan->Telp; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"><input type="text" class="form-control" id="angkakoma" name="angkakoma" value="<?php echo $perusahaan->Angkakoma; ?>"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Fax Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" class="form-control" id="fax" name="fax" value="<?php echo $perusahaan->Fax; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Kontak Person Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="kontak" name="kontak" value="<?php echo $perusahaan->Kontak; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><label class="judul">Email Perusahaan</label></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 45%"><input type="text" class="form-control" id="email" name="email" value="<?php echo $perusahaan->Email; ?>"></td>
                                    <td>&nbsp;</td>
                                    <td style="width: 45%"></td>
                                </tr>
                            </table>
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <input type="hidden" name="IDPerusahaan" id="IDPerusahaan" value="<?php echo $perusahaan->IDPerusahaan; ?>">

                            <div class="col-md-3">
                                
                            </div>
                            <div class="col-md-9">
                            
                                    <!-- <div class="flot-right btn btn-primary hvr-icon-float-away">
                                        <a data-toggle="modal" data-target="#modalfont"><span style="color: white;">Ganti Font</span></a>
                                    </div> -->
                            </br></br>
                                    
                            </div>                          
                            <div class="col-md-3">
                                <label>&nbsp;</label></br>
                                <button onclick="simpandata()" class="btn btn-info" id="simpan" name="simpan">Simpan</button>
                            </div>
		                    
		            	</div>
		            </div>
                    <!--===========END TAB 1=============-->
		            <div class="tab-pane" id="tab-2">
		            	<br>
		            	<br>
		            	<br>
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Logo Perusahaan</span>	
		            	</div> 
		            	<div class="container" style="width: 100%">
		            		<br>
		            		<div class="col-md-6">
                                <label class="judul">Logo Sebelumnya</label><br>
                                <img style="width:100%" src="{{URL::to('/')}}/newtemp/<?php echo $logohead->Logo_head ?>">
                            </div>
                            <div class="col-md-6">
                                    <label class="judul">Upload Logo</label>
                                    <form method="POST" action="{{url('ControlPanel/Simpangambarhead')}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                        <input type="file" class="form-control" name="image" id="image" accept="image/x-png">
                                        <br>
                                        <button type="submit"  class="btn btn-primary">UPLOAD</button>
                                    </form>
                            </div>
		            	</div>
		            </div>
                    <!--==============END TAB 2================-->
                    <div class="tab-pane" id="tab-3">
		            	<br>
		            	<br>
		            	<br>
		            	<div class="form-title">
		            		<span><i class="fa fa-th-list"></i>&nbsp;Kode Transaksi</span>	
		            	</div> 
		            	<div class="bannerbody container" style="width: 100%">
		            		<br>
                                <select data-placeholder="Cari COA" class="form-control" name="idmenu" id="idmenu" required style="width: 50%; float: left">
                                <option value='0'>--==Pilih Menu Transaksi==--</option>;
                                    <?php foreach ($IDMenu as $menu) {
                                        echo "<option value='$menu->IDMenuDetail'>$menu->Menu_Detail</option>";
                                    }
                                    ?>
                                </select>
                            
                                <input type="text" class="form-control" id="namakode" name="namakode" style="width: 50%" placeholder="Kode Transaksi Yang Akan Digunakan">
                                <br>
                                <button onclick="simpankodetransaksi()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button>
                            
		            	</div>
		            	<br>
		                <div class="bannerbody">
                            <table id="tblkodetrans" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                                <thead style="color: #fff">
                                    <th>No</th>
                                    <th>Nama Menu</th>
                                    <th>Kode Transaksi Yang Digunakan</th>
                                    <th>Aksi</th>
                                </thead>
                                <tbody>
                                <?php
                                    $i = 1; 
                                    foreach ($datakode as $dk) { ?>
                                    <tr>
                                        <td><?php echo $i ?></td>
                                        <td><?php echo $dk->Menu_Detail ?></td>
                                        <td><?php echo $dk->Kode ?></td>
                                        <td>
                                            <a data-toggle="modal" data-target="#modaleditkode"><span style="color: white;"><i class="fa fa-pencil fa-lg"></i></span></a>
                                        </td>
                                    </tr>
                                <?php $i++; } ?>
                                </tbody>
                            </table>
                        </div>
		                
		            </div>
		    	</div>
			</div>
    </div>
</div>
<!--=====================MODAL KOTA=========================-->
            <div id="modalkota" class="modal fade" role="dialog">
              <div class="modal-dialog">
              	<form method="post" action="{{url('Supplier/simpandatakota')}}" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kota</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                    	<label class="judul">Kode Kota</label>             
                    </div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Kota" name="kode" id="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Provinsi</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" placeholder="Provinsi" name="provinsi" id="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Kota</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" placeholder="Kota" name="kota" id="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            	</form>
              </div>
            </div>

<!--======================MODAL FONT=========================-->
            <div id="modalfont" class="modal fade" role="dialog">
              <div class="modal-dialog">
              	<form method="post" action="{{url('#')}}" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Font</h4>
                  </div>
                  <div class="modal-body">
                                <select data-placeholder="Pilih Font" style="width: 75%" class="form-control" name="font" id="font" >
                                    <?php 
                                    foreach ($DataFont as $font ) { 
                                    if ($font->Nama == $perusahaan->Font) { ?>
                                    <option value="<?= $font->IDFont ?>" selected style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                                    <?php }else{
                                    ?>
                                    <option value="<?= $font->IDFont ?>" style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                                    <?php }}
                                    ?>
                                </select>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            	</form>
              </div>
            </div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/controlpanel/controlpanelperusahaan_create.js') }}"></script>
<script>
    $(document).ready(function() {
        $('#tblkodetrans').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        }); 
            @if (session('alert'))
                swal("Berhasil", "{{ session('alert') }}", "success");
            @endif 

            @if (session('alert2'))
                swal("Perhatian", "{{ session('alert2') }}", "warning");
            @endif           
    })
</script>
@endsection 