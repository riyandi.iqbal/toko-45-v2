@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="home" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Master Control Panel</a>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Setting Cara Bayar</a>
		</h2>
	</div>
    <div class="text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Setting Cara Bayar</span>
    </div>
    </br>
    <div class="banner">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="col-md-3">
                <label>Cara Bayar</label></br>
                <input type="text" id="carabayar" name="carabayar" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" require>
            </div>

            <div class="col-md-3">
                <label>COA</label></br>
                <select data-placeholder="Cari COA" class="chosen-select" name="idcoa" id="idcoa" required>
                    <?php foreach ($coa as $row2) {
                        echo "<option value='$row2->IDCoa'>$row2->Kode_COA -- $row2->Nama_COA</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-3">
                <label>&nbsp;</label></br>
                <button onclick="simpandata()" class="btn btn-info" id="simpan" name="simpan">Simpan</button>
            </div>
        </br></br></br>
    </div>
    <div class="banner">
    <table id="tblsetbayar" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
    			<th>No</th>
                <th>Nama</th>
    			<th>Nama COA</th>
    			<th>Aksi</th>
    		</thead>
    		<tbody id="previewdata">
            <?php 
                    $i = 1;
                    foreach ($carabayar as $data) { ?>
                    <tr class="odd gradeA">
                            <td><center><?php echo $i; ?></center></td>
                            <td><?php echo $data->Nama; ?></td>
                            <td><?php echo $data->Nama_COA; ?></td>
                            <td class="text-center ukuran-logo">
                                <span><a class="action-icons c-edit" href="settingcarabayar/edit/<?php echo $data->IDCaraBayar;?>" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>
                            </td>
                        </tr>
                                <?php
                                        $i++;
                                    }
                                
                                ?>   
            
    		</tbody>
    	</table>
    </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/controlpanel/controlpanelcarabayar_index.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tblsetbayar').DataTable({
            "searching": true,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });       
    })
    var Backsettingcarabayar = "{{url('settingcarabayar')}}";
</script>

@endsection 