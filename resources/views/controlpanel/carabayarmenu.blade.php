@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanel')}}">Master Control Panel</a>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanel')}}">Setting Cara Bayar -> Menu</a>
		</h2>
	</div>
    <div class="text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Setting Cara Bayar -> Menu</span>
    </div>
    </br>
    <div class="banner">
        <form action="{{url('ControlPanel/simpanbayarmenu')}}" method="post">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <table id="tblcbm" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
                    <th>No</th>
                    <th>Cara Bayar</th>
                    <th>Pesanan Pembelian</th>
                    <th>Faktur Pembelian</th>
                    <th>Pembayaran Hutang</th>
                    <th>Sales Order</th>
                    <th>Faktur Penjualan</th>
                    <th>Pembayaran Piutang</th>
                </thead>
                <tbody>
                <?php 
                    $i = 1;
                    foreach ($carabayar as $data) { ?>
                    <tr class="odd gradeA">
                            <td><center><?php echo $i; ?></center></td>
                            <td><?php echo $data->Nama; ?></td>
                            
                            <?php if($data->PO=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="PO_array" name="PO_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="PO_array" name="PO_array[]"></td>
                            <?php } ?>

                            <?php if($data->FB=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="FB_array" name="FB_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="FB_array" name="FB_array[]"></td>
                            <?php } ?>
                            
                            <?php if($data->PH=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="PH_array" name="PH_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="PH_array" name="PH_array[]"></td>
                            <?php } ?>

                            <?php if($data->SO=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="SO_array" name="SO_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="SO_array" name="SO_array[]"></td>
                            <?php } ?>

                            <?php if($data->FJ=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="FJ_array" name="FJ_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="FJ_array" name="FJ_array[]"></td>
                            <?php } ?>

                            <?php if($data->PP=='yes') {?>
                                <td><input type="checkbox" checked value="<?php echo $data->Nama?>" id="PP_array" name="PP_array[]"></td>
                            <?php }else{ ?>
                                <td><input type="checkbox" value="<?php echo $data->Nama?>" id="PP_array" name="PP_array[]"></td>
                            <?php } ?>
                        </tr>
                        <?php
                            $i++;
                                }
                        ?>  
                </tbody>
            </table>
            <div class="col-md-3">
                <label>&nbsp;</label></br>
                <button onclick="simpandata()" class="btn btn-info">Simpan</button>
            </div>
        </br></br></br>
    </div>
    </form>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<!-- <script src="{{ asset('js/controlpanel/controlpanelcarabayarmenu_index.js') }}"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        $('#tblcbm').DataTable({
            "searching": false,
            "info": false,
            "ordering": false,
            "lengthChange": false            
        }); 
        @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif      
    })
    
    var Backsettingcarabayar = "{{url('settingcarabayar')}}";
    var urlSimpanedit   = "{{url('ControlPanel/simpancarabayaredit')}}";
</script>

@endsection 