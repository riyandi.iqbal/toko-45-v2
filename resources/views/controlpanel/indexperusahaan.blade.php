@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="home" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Master Control Panel</a>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Setting COA</a>
		</h2>
	</div>
    <div class="text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Setting Data Perusahaan</span>
    </div>
    </br>
    <div class="bannerbody container">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <input type="hidden" name="IDPerusahaan" id="IDPerusahaan" value="<?php echo $perusahaan->IDPerusahaan; ?>">
            <div class="col-md-3">
                <label class="judul">Nama Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="nama" name="nama" value="<?php echo $perusahaan->Nama; ?>">
            </br></br>
            </div>
            
            <div class="col-md-3">
                <label class="judul">Alamat Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="alamat" name="alamat" value="<?php echo $perusahaan->Alamat; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
                <label class="judul">Kecamatan Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="kec" name="kec" value="<?php echo $perusahaan->Kecamatan; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Kota</label>
            </div>
            <div class="col-md-9">            	
                <select data-placeholder="Pilih Kota" style=" width:75%;" class="chosen-select" name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php 
                       foreach ($DataKota as $key ) { 
                       if ($key->IDKota == $perusahaan->Kota) { ?>
                    <option value="<?= $key->IDKota ?>" selected> <?= $key->Kota ?></option> 
                    <?php }
                    ?>
                    <option value="<?= $key->IDKota ?>"> <?= $key->Kota ?></option> 
                    <?php }
                    ?>
                </select>
              
                    <div class="flot-right btn btn-primary hvr-icon-float-away">
                    	<a data-toggle="modal" data-target="#modalkota"><span style="color: white;">Tambah Kota</span></a>
                    </div>
               
                <br><br>
            </div>

            <div class="col-md-3">
                <label class="judul">Telepon Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" id="tel" name="tel" value="<?php echo $perusahaan->Telp; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
                <label class="judul">Fax Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" id="fax" name="fax" value="<?php echo $perusahaan->Fax; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
                <label class="judul">Kontak Person Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" id="kontak" name="kontak" value="<?php echo $perusahaan->Kontak; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
                <label class="judul">Email Perusahaan</label>
            </div>
            <div class="col-md-9">
                <input type="text" class="form-control" id="email" name="email" value="<?php echo $perusahaan->Email; ?>">
            </br></br>
            </div>

            <div class="col-md-3">
                <label class="judul">Font Family</label>
            </div>
            <div class="col-md-9">
            <select data-placeholder="Pilih Font" style=" width:75%;" class="chosen-select" name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Font Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php 
                       foreach ($DataFont as $font ) { 
                       if ($font->IDFont == $perusahaan->Font) { ?>
                    <option value="<?= $font->IDFont ?>" selected style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                    <?php }
                    ?>
                    <option value="<?= $font->IDFont ?>" style="font-family: <?= $font->Nama ?>"> <?= $font->Nama ?></option> 
                    <?php }
                    ?>
                </select>
            </br></br>
            </div>
            
            <div class="col-md-3">
                <label>&nbsp;</label></br>
                <button onclick="simpandata()" class="btn btn-info" id="simpan" name="simpan">Simpan</button>
            </div>

        </br></br></br>
    </div>
    
</div>

            <div id="modalkota" class="modal fade" role="dialog">
              <div class="modal-dialog">
              	<form method="post" action="{{url('Supplier/simpandatakota')}}" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kota</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                    	<label class="judul">Kode Kota</label>             
                    </div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Kota" name="kode" id="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Provinsi</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" placeholder="Provinsi" name="provinsi" id="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Kota</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" onkeyup="javascript:this.value=this.value.toUpperCase();" class="form-control" placeholder="Kota" name="kota" id="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            	</form>
            </div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/controlpanel/controlpanelperusahaan_create.js') }}"></script>
@endsection 