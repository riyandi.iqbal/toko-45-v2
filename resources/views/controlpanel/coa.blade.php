@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="home" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Master Control Panel</a>
                <i class="fa fa-angle-right"></i>
                <a href="ControlPanel">Setting COA</a>
		</h2>
	</div>
    <div class="text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Setting Pemakaian COA</span>
    </div>
    </br>
    <div class="bannerbody container">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <div class="col-md-4">
                <label>Menu Pemakaian</label></br>
                <select data-placeholder="Cari Menu Pemakaian" class="chosen-select" name="idmenu" id="idmenu" required>
                    <?php foreach ($menu as $row) {
                        echo "<option value='$row->IDMenuDetail'>$row->Menu_Detail</option>";
                    }
                    ?>
                </select>
            </div>

            <div class="col-md-4">
                <label>COA</label></br>
                <select data-placeholder="Cari COA" class="chosen-select" name="idcoa" id="idcoa" required>
                    <?php foreach ($coa as $row2) {
                        echo "<option value='$row2->IDCoa'>$row2->Kode_COA -- $row2->Nama_COA</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                <label>Posisi</label></br>
                <select data-placeholder="Cari COA" class="chosen-select" name="idposisi" id="idposisi" required>
                    <option value="debet">DEBET</option>
                    <option value="kredit">KREDIT</option>
                </select>
                <br/><br/><br/>
            </div>
            
            <div class="col-md-4">
                <label>Tingkat</label></br>
                <select data-placeholder="Cari COA" class="chosen-select" name="tingkat" id="tingkat" required>
                    <option value="utama">UTAMA</option>
                    <option value="tambahan">TAMBAHAN</option>
                </select>
            </div>
            <div class="col-md-4">
                <label>Lunas/Utang</label></br>
                <select data-placeholder="Cari COA" class="chosen-select" name="lunas" id="lunas" required>
                    <option value="lunas">LUNAS</option>
                    <option value="utang">UTANG</option>
                </select>
            </div>
            <div class="col-md-2">
                <label>&nbsp;</label></br>
                <button onclick="simpandata()" class="btn btn-info" id="simpan" name="simpan">Simpan</button>
            </div>
        </br></br></br>
    </div>
    <div class="banner">
    <table id="tblsetcoa" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
    			<th>No</th>
                <th>Nama Menu</th>
    			<th colspan='2'>Nama COA</th>
                <th>Posisi</th>
                <th>Tingkat</th>
                <th>Secara</th>
    			<th>Aksi</th>
    		</thead>
    		<tbody id="previewdata">
            <?php 
                    $i = 1;
                    foreach ($settingcoa as $data) { ?>
                    <tr class="odd gradeA">
                            <td><center><?php echo $i; ?></center></td>
                            <td><?php echo $data->Menu_Detail; ?></td>
                            <?php if($data->Posisi=='kredit') { ?> 
                            <td>&nbsp;</td>
                            <td><?php echo $data->Nama_COA; ?></td>
                            <?php } else { ?>
                            <td colspan='2'><?php echo $data->Nama_COA; ?></td>
                            <?php } ?>
                            <td><center><?php echo strtoupper($data->Posisi); ?></center></td>
                            <td><center><?php echo strtoupper($data->Tingkat); ?></center></td>
                            <td><center><?php echo strtoupper($data->Cara); ?></center></td>
                            <td class="text-center ukuran-logo">
                                <span><a class="action-icons c-edit" href="settingcoa/edit/<?php echo $data->IDSETCOA;?>" title="Ubah Data"><i class="fa fa-pencil fa-lg"></i></a></span>
                                <span><a data-toggle="modal" data-target="#modalBatal<?php echo $data->IDSETCOA ?>" title="Hapus Data"><i class="fa fa-trash-o fa-lg" style="color: red"></i></a></span>
                            </td>
                        </tr>
                                <?php
                                        $i++;
                                    }
                                
                                ?>   
            
    		</tbody>
    	</table>
    </div>
</div>


    <?php
        if (isset($settingcoa)){ foreach($settingcoa as $data){ ?>
            <div id="modalBatal<?php echo $data->IDSETCOA?>" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Peringatan!</h4>
                  </div>
                  <div class="modal-body">
                    <p>Yakin Akan Menghapus Data <?php echo $data->Nama_COA?> ?!</p>
                    <input type="hidden" name="idcari" value="<?php echo $data->IDSETCOA?>">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <div class="btn hvr-icon-shrink col-10">
	                    <a style="color: white;" href="settingcoa/hapus/<?php echo $data->IDSETCOA ?>"><span>Hapus&nbsp;&nbsp;&nbsp;</span></a>
	                </div>
                  </div>
                </div>

              </div>
            </div>
       <!--  </form> -->
    <?php } } ?>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/controlpanel/controlpanelcoa_create.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tblsetcoa').DataTable({
            "searching": true,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });       
                @if (session('alertakses'))
                    swal("Perhatian", "{{ session('alertakses') }}", "warning");
                @endif  

                @if (session('alert'))
                    swal("Berhasil", "{{ session('alertakses') }}", "success");
                @endif    
    })
    var Backsettingcoa = "{{url('settingcoa')}}";
</script>

@endsection 