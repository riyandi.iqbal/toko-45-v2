@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanel')}}">Master Control Panel</a>
		</h2>
	</div>
	<br>
    <div class="banner container">
        <div class="col-md-6">
            <label class="judul">Logo Sebelumnya</label><br>
            <img style="width:100%" src="{{URL::to('/')}}/newtemp/<?php echo $logohead->Logo_head ?>">
        </div>
        <div class="col-md-6">
                <label class="judul">Upload Logo</label>
                <form method="POST" action="{{url('ControlPanel/Simpangambarhead')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                    <input type="file" class="form-control" name="image" id="image" accept="image/x-png">
                    <br>
                    <button type="submit"  class="btn btn-primary">UPLOAD</button>
                </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('success'))
            swal("Berhasil", "{{ session('success') }}", "success");
        @endif
    })
</script>
@endsection 