@extends('layouts.app')   
@section('content')
<div class="main-grid">
	<div class="banner">
		<h2>
			<span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ControlPanel')}}">Master Control Panel</a>
		</h2>
	</div>
	<br>
    <div class="banner container">
        <?php foreach ($controlpanel as $data) { ?>
            <a href="<?php echo $data->Link ?>" style="color: #fff">
            <div class="col-md-3" style="padding: 10px; margin-right:10px; margin-bottom:10px; background-color: #254283">
                <?php echo $data->Nama ?>
            </div>
            </a>     
            
        <?php } ?>
    </div>
</div>
@endsection 