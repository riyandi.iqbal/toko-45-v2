<!-- ===========Create By Dedy 25-12-2019=============== -->
@extends('layouts.app')
@section('content')

<div class="main-grid">
<div class="banner">
<h2>
    <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
        <i class="fa fa-angle-right"></i>
        <a>Data Pembayaran Hutang</a>
</h2>
</div>
<br>
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<div class="banner container"> 
    <a href="{{url('PembayaranHutang/create') }}">
    <div class="btn btn-primary hvr-icon-float-away">
        <span style="color: white;">Tambah Data&nbsp;&nbsp;&nbsp;</span>
    </div>
    </a>
    <br><br><br>
    <div class="col-md-2">
        <label class="field_title mt-dot2">Periode</label>
        <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
    </div>
    <div class="col-md-2">
        <label class="field_title mt-dot2"> S/D </label>
        <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
    </div>
    <div class="col-md-2">
        <label>.</label>
        <select name="jenispencarian" id="jenispencarian" data-placeholder="No Retur Pembelian" style="width: 100%!important" class="form-control" tabindex="13">
            <option value="Nomor">No PH</option>
            <option value="IDFB">No Invoice</option>
            <option value="Nama">Supplier</option>
        </select>
    </div>
    <div class="col-md-4">
        <label>.</label>
        <input name="keyword" id="keyword" type="text" placeholder="Cari Berdasarkan" class="form-control">
    </div>
    <div class="col-md-2">
        <label>&nbsp;</label><br>
        <button onclick="caridata()" class="btn btn-primary">Search</button>
    </div>                                       
</div>
<br><br>
<div class="bannerbody">
<div class="widget_content">
                <table id="tableas" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                    <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor PH</th>
                        <th>Nama Supplier</th>
                        <th>Pembayaran</th>
                        <th>Cara Pembayaran</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody id="previewdata">
                    <?php if(!empty($datas)){
                        $i=1;
                        foreach ($datas as $data) {
                            ?>
                            <tr class="odd gradeA">
                                <td class="center"><?php echo $i; ?></td>
                                <td><?php echo date("d/m/Y", strtotime($data->Tanggal)); ?></td>
                                <td><?php echo $data->Nomor; ?></td>
                                <td><?php echo $data->Nama; ?></td>
                                
                                <td align=right><?php echo $data->Mata_uang?><?php echo number_format($data->Nominal_pembayaran, $coreset->Angkakoma, '.',','); ?></td>
                                
                                
                                <td><?php echo $data->Jenis_pembayaran; ?></td>
                                <td><?php echo $data->Keterangan; ?></td>
                                <td class="text-center ukuran-logo">
                                    <span><a class="action-icons c-Detail" href="{{url('PembayaranHutang/show/'.$data->IDBS)}}" title="Detail Data"><i class="fa fa-search fa-lg" style="color: green"></i></a></span>
                                   
                                    <?php if($data->Batal=="aktif"){ ?>
                                        <span><a class="action-icons c-sa" href="{{url('PembayaranHutang/cekallpostsgagal')}}/<?php echo $data->IDBS;?>" title="Status Tidak Aktif"><i class="fa fa-times-circle fa-lg" style="color: red"></i></a></span>
                                    <?php }elseif($data->Batal=="tidak aktif"){ ?>
                                        <span><a class="action-icons c-approve" href="{{url('PembayaranHutang/cekallpostsberhasil')}}/<?php echo $data->IDBS;?>" title="Status Aktif">&nbsp;<i class="fa fa-check fa-lg" style="color: #999"></i></a></span>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
    
</div>
</div>

<!-- ======================= -->


<script type="text/javascript">
$(document).ready(function() {
$('#tableas').DataTable({
    "searching": false,
    "info": false,
    "ordering": true,
    "lengthChange": false            
});            
})

var PM = [];
function caridata()
{
var jenis       = $('#jenispencarian').val();
var keyword     = $('#keyword').val();
var date_from   = $('#date_from').val();
var date_until  = $('#date_until').val();
    $.ajax({
            url : "{{url('PembayaranHutang/indexcari')}}",
            type: "POST",
            data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
            dataType:'json',
        success: function(data){
            var html = '';
                if (data <= 0) {
                console.log('-----------data kosong------------');
                $("#previewdata").html("");
                var value =
                    "<tr>" +
                    "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                    "</tr>";
                $("#previewdata").append(value);
                swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                } else {
                console.log(data);
                $("#previewdata").html("");
                for(i = 0; i<data.length;i++){
                PM[i] = data[i];


                if(PM[i].Batal=='aktif'){
                    var $status  = "<span><a href='{{url('PenerimaanBarang/show/')}}"+PM[i].IDTBS+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='{{url('PenerimaanBarang/printnewpb/')}}"+PM[i].IDTBS+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>&nbsp;<span><a data-toggle='modal' data-target='#modalBatal"+PM[i].IDTBS+"' title='Batalkan.?'><i class='fa fa-times-circle fa-lg' style='color: red'></i></a></span>";
                }else{
                    var $status  = "<span><a href='{{url('PenerimaanBarang/show/')}}"+PM[i].IDTBS+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='{{url('PenerimaanBarang/printnewpb/')}}"+PM[i].IDTBS+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>";
                }

                var value =
                "<tr>" +
                "<td>"+(i+1)+"</td>"+
                "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                "<td>"+PM[i].Nomor+"</td>"+
                "<td>"+PM[i].Nama+"</td>"+
                "<td style='text-align:right;'>Rp. "+rupiah(PM[i].Nominal_pembayaran)+"</td>"+
                "<td>"+PM[i].Jenis_pembayaran+"</td>"+
                "<td>"+PM[i].Keterangan+"</td>"+
                "<td style='text-align:center;'>"+$status+"</td>"
                "</tr>";
                $("#previewdata").append(value);
                }
                
                }
        }
    });
}
</script>
@endsection