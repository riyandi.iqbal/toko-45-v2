<!-- ===========Create By Dedy 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset->Angkakoma) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('PembayaranHutang')}}">Data Pembayaran Hutang</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Pembayaran Hutang - {{ $pembayaran_hutang->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="bannerbody">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                            <td width="65%" style="background-color: #e5eff0; border: 1px solid; "> {{ AppHelper::DateIndo($pembayaran_hutang->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $pembayaran_hutang->Nomor }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Customer</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ $pembayaran_hutang->Nama }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nominal Pembayaran</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ AppHelper::NumberFormat($pembayaran_hutang->Pembayaran, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Kelebihan Bayar</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($pembayaran_hutang->Kelebihan_bayar, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $pembayaran_hutang->Keterangan }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th class="center">No</th>
                            <th>No Invoice</th>
                            <th>Tanggal Faktur</th>
                            <th>Nilai Faktur</th>
                            <th>Telah dibayar</th>
                            <th>Saldo</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($pembayaran_hutang_detail as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->Nomor }} </td>
                                <td> {{ AppHelper::DateIndo($item->Tanggal_fb) }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Nilai_Hutang, $angkakoma) }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Pembayaran, $angkakoma) }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Saldo_Akhir, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th style="">Jenis Pembayaran</th>
                            <th style="">COA</th>
                            <th style="">Tanggal Giro</th>
                            <th style="">Nomor Giro</th>
                            <th style="">Nominal</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($pembayaran_hutang_bayar as $key => $item)
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->Jenis_pembayaran }} </td>
                                <td> {{ $item->Nama_COA }} </td>
                                <td> {{ $item->Nomor_giro }} </td>
                                <td> {{ $item->Tanggal_giro }} </td>
                                <td style="text-align: right;"> {{ $item->Kode . AppHelper::NumberFormat($item->Nominal_pembayaran, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ url('PembayaranHutang') }}" name="simpan">Kembali</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection