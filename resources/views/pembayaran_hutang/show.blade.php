<!-- ===========Create By Dedy 01-01-2019=============== -->
@extends('layouts.app')
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PembayaranHutang')}}">Data Pembayaran Hutang</a>
                <i class="fa fa-angle-right"></i>
                <a>Detail Pembayaran Hutang - <b><?php echo $data->Nomor ?></b></a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <table class="table cell-border" width="100%">
            <thead style="display: none;">
            </thead>
            <tbody>
                <tr>
                    <td width="35%" style="background-color: #ffffff;">Tanggal</td>
                    <td width="65%" style="background-color: #ffffff;"><?php echo date('d/m/Y',strtotime($data->Tanggal)) ?></td>
                </tr>
                <tr>
                    <td style="background-color: #e5eff0;">Nomor</td>
                    <td style="background-color: #e5eff0;"><?php echo $data->Nomor?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #ffffff;">Supplier</td>
                    <td width="65%" style="background-color: #ffffff;"><?php echo $data->Nama ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #e5eff0;">Keterangan</td>
                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                </tr>
                <!--=========================DEDY 03-10-2019 -->
                <tr>
                    <td style="background-color: #ffffff;">Nomor Invoice</td>
                    
                </tr>
                <tr>
                    <td width="35%" style="background-color: #e5eff0;">Uang Muka</td>
                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php  echo number_format($data->Uang_muka, $coreset->Angkakoma, '.',','); ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #ffffff;">Pembayaran</td>
                    <td width="65%" style="background-color: #ffffff;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php  echo  number_format($data->Pembayaran, $coreset->Angkakoma, '.',','); ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #e5eff0;">Selisih</td>
                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php  echo number_format($data->Selisih, $coreset->Angkakoma, '.',','); ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #ffffff;">Total</td>
                    <td width="65%" style="background-color: #ffffff;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php echo number_format($data->Total, $coreset->Angkakoma, '.',','); ?></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="banner">
        <center>
            <span class="btn col-11 hvr-icon-back" > <a style="color: #fff;" href="{{url('PembayaranHutang')}}" name="simpan">Kembali</a></span>
            <span class="btn col-3 hvr-icon-spin" ><a style="color: #fff;" href="{{url('PembayaranHutang/printdata/')}}<?php echo $data->Nomor ?>">Print Data</a></span>
        </center>
    </div>
</div>
@endsection