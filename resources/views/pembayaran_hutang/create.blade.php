@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{route('PembayaranHutang.index')}}">Data Pembayaran Hutang</a>
                <i class="fa fa-angle-right"></i>
            <span>Tambah Pembayaran Hutang</span>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Pembayaran Hutang</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td>
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="<?php echo date('d/m/Y'); ?>">
                        <br>
                        <label for="Nomor">Nomor</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="Nomor Pembayaran Hutang" readonly>
                    </td>
                    <td>
                        <label for="IDSupplier">Supplier</label><br>
                        <select data-placeholder="Cari Supplier" class="select2 form-control" name="IDSupplier" id="IDSupplier" required>
                            <option value="">-- Pilih Supplier --</option>
                            <?php foreach ($supplier as $row) {
                                echo "<option value='$row->IDSupplier'>$row->Nama</option>";
                            }
                            ?>
                        </select>         
                    </td>
                    <td>
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"></textarea>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <label for="Total_hutang">Total Hutang</label>
                        <input type="text" name="Total_hutang" class="form-control" id="Total_hutang" readonly value="0">
                        <label for="Total_pembayaran">Total Dibayar</label>
                        <input type="text" name="Total_pembayaran" class="form-control" id="Total_pembayaran" readonly value="0">
                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 60px;">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                        <li class="active"><a href="#tab-hutang" data-toggle="tab">Data Hutang</a></li>
                        <li><a href="#tab-pembayaran" data-toggle="tab">Pembayaran</a></li>
                    </ul>
                </div>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="tab-hutang">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Hutang</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <table class="table table-bordered table-responsive" id="table-hutang" style="font-size: 12px; margin-top: 10px; width: 100%;">
                                <thead style="background-color: #16305d; color: white">
                                    <tr>
                                        <th>No</th>
                                        <th style="">Do</th>
                                        <th style="">No Invoice</th>
                                        <th style="">Tanggal Faktur</th>
                                        <th style="">Jatuh Tempo</th>
                                        <th style="">Total Faktur</th>
                                        <th style="">UM</th>
                                        <th style="">Retur</th>
                                        <th style="">Telah Dibayar</th>
                                        <th style="">Dibayar</th>
                                        <th style="">Saldo</th>
                                    </tr>
                                </thead>
                                <tbody id="list-hutang">
        
                                </tbody>
                            </table>
                            <div class="btn col-16">
                                <span> <a style="color: white; float: right;" href="{{ route('PembayaranHutang.index') }}">Kembali</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-pembayaran">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Pembayaran</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <div class="col-md-6">
                                <label>Jenis Pembayaran</label>
                                <select name="Jenis_pembayaran" id="Jenis_pembayaran" class="form-control">
                                    <option value="">-- Pilih Jenis Pembayaran --</option>
                                    <?php foreach ($cara_bayar as $cb) { ?>
										<option value="<?php echo strtolower($cb->Nama) ?>"><?php echo $cb->Nama ?></option>
									<?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nama COA</label>
                                <select name="IDCoa" id="IDCoa" class="form-control">
                                    <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nomor Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control" name="Nomor_giro" id="Nomor_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Tanggal Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control datepicker" name="Tanggal_giro" id="Tanggal_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Mata Uang</label>
                                <select data-placeholder="Cari Kurs" class="form-control select2" name="IDMataUang" id="IDMataUang" style="width: 100%;" >
                                    <?php foreach ($kurs as $ks) {
                                        echo "<option value='$ks->IDMataUang' data-kurs='$ks->Kurs'>$ks->Mata_uang</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Kurs</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="Kurs" id="Kurs" >
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Pembayaran</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="Nominal_pembayaran" id="Nominal_pembayaran" >
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary" type="button" id="Tambah_pembayaran" name="Tambah_pembayaran" style="float: center; margin-top: 10px;">Tambah</button>
                            </div>
                            <div class="col-md-12">
                                <table class="table table-bordered table-responsive" id="table-pembayaran" style="font-size: 12px; margin-top: 10px; width: 100%;">
                                    <thead style="background-color: #16305d; color: white">
                                        <tr>
                                            <th>No</th>
                                            <th style="">Jenis Pembayaran</th>
                                            <th style="">COA</th>
                                            <th style="">Tanggal Giro</th>
                                            <th style="">Nomor Giro</th>
                                            <th style="">Nominal</th>
                                            <th style="">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-pembayaran">
                                        
                                    </tbody>
                                    <tfoot style="background-color: #16305d; color: white">
                                        <tr>
                                            <th colspan="5" style="text-align:right">Total Nominal</th>
                                            <th colspan="2"></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <label>Total Pembayaran</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="Total_pembayaran_1" id="Total_pembayaran_1" readonly value="0">
                                <span class="text-danger notifikasi-pembayaran" style="display: none;">Nilai Pembayaran masih kurang dari Total Dibayar</span>
                            </div>
                            <div class="col-md-6">
                                <label>Kelebihan Bayar</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123" name="Kelebihan_bayar" id="Kelebihan_bayar" readonly value="0">
                            </div>
                            <div class="col-md-6">
                            </div>
                            <div class="col-md-6">
                                <label for="IDCoa_kelebihan_bayar"> COA </label>
                                <select name="IDCoa_kelebihan_bayar" id="IDCoa_kelebihan_bayar" class="form-control select2" style="width: 100%!important; padding: 3px 2px;border-color: #d8d8d8;color: #999999;">
                                    <option value="">-- Pilih COA Kelebihan Bayar --</option>
                                    <?php foreach($coa as $item) { ?>
                                        <option value="<?php echo $item->IDCoa ?>">
                                            <?php echo $item->Kode_COA ?> - <?php echo $item->Nama_COA ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-12 text-center">
                                <button class="btn btn-primary" type="submit" id="Simpan" name="Simpan" style="float: center; margin-top: 10px;">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
var urlIndex    = '{{ route("PembayaranHutang.index") }}';
var urlNumber   = '{{ route("PembayaranHutang.number") }}';
var urlInsert   = '{{ route("PembayaranHutang.store") }}';
var urlHutang  = '{{ route("PembayaranHutang.hutang") }}';
var urlCoa      = '{{ route("PembayaranHutang.get_coa") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/pembayaranhutang/pembayaranhutang_create.js') }}"></script>

@endsection