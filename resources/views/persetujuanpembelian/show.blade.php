<!-- ===========Create By Dedy 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('PersetujuanPesanan')}}">Data Persetujuan Pesanan Pembelian</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Persetujuan Pesanan Pembelian - <b><?php echo $po->Nomor ?></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="table cell-border" width="100%" style="font-size: 12px;">
                            <thead style="display: none;">
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo date("d/m/Y", strtotime( $po->Tanggal) )?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo $po->Nomor?></td>
                                </tr>
                                <!-- <tr>
                                    <td  style="background-color: #e5eff0; border: 1px solid;">Tanggal Selesai</td>
                                    <td  style="background-color: #e5eff0; border: 1px solid;"><?php echo date("d/m/Y", strtotime($po->Tanggal_selesai ))?></td>
                                </tr> -->
                                <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Supplier</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo $po->Nama ?></td>
                                </tr>

                                <!-- <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Jenis Pesanan</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo $po->Jenis_Pesanan?></td>
                                </tr> -->

                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo $po->Keterangan?></td>
                                </tr>
                                <!-- <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Status</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo $po->Batal?></td>
                                </tr> -->
                                <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Total Qty</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo number_format($totalqtypo->totqty) ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Total Harga</td>
                                    <?php if($coreset->Angkakoma!='0'){ ?>
                                        <td style="text-align: left;border: 1px solid;"><?php echo $po->Mata_uang ?>. <?php echo number_format($po->Grand_total, $coreset->Angkakoma, '.', ','); ?></td>
                                    <?php }else{ ?>
                                        <td style="text-align: left;border: 1px solid;"><?php echo $po->Mata_uang ?>. <?php echo number_format($po->Grand_total); ?></td>
                                    <?php } ?>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Kurs</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo $po->Mata_uang ?></td>
                                </tr>

                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Status PPN</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo strtoupper($po->pilihanppn) ?></td>
                                </tr>

                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <!-- <table class="display data_tbl">
                 --><table id="tblshowpo" class="table cell-border" width="100%" style="font-size: 12px;">
                  <thead style="background-color: #16305d; color: #fff">
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Harga</th>
                      <th>Satuan</th>
                      <th>PPN % </th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($podetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($podetail as $data2) {
                        $subtotal   = ($data2->Qty*$data2->Harga);
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Nama_Barang; ?></td>
                          <td class="text-center"><?php echo number_format($data2->Qty); ?></td>
                            <?php if($coreset->Angkakoma!='0'){ ?>
                                <td style="text-align: right;"><?php echo $po->Mata_uang ?>. <?php echo number_format($data2->Harga, $coreset->Angkakoma, '.', ','); ?></td>
                            <?php }else{ ?>
                                <td style="text-align: right;"><?php echo $po->Mata_uang ?>. <?php echo number_format($data2->Harga); ?></td>
                            <?php } ?>
                          <td><?php echo $data2->Satuan; ?></td>
                          <td style="text-align: right;"><?php echo $data2->PPN; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                            <?php if($po->approve=='Disetujui') { ?>
                                
                            <?php }else{ ?>
                                <?php if ($po->Batal == 'aktif') { ?>
                                    <div class="btn col-11 hvr-icon-float-away">
                                        <span> <a style="color: white;" href="{{url('PersetujuanPesanan')}}/approve/<?php echo $po->IDPO ?>" name="simpan">Setujui</a></span>
                                    </div>
                                    <div class="btn col-5 hvr-icon-spin">
                                    <span><a style="color: white;" href="{{url('PersetujuanPesanan')}}/reject/<?php echo $po->IDPO ?>" target="__blank">Tolak</a></span>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            </div>
                        </div>
                        <br>
                        <div class="widget_content py-4">
                            <div class="form_grid_12">
                                <div class="btn col-3 hvr-icon-back">
                                    <span> <a style="color: white;" href="{{url('PersetujuanPesanan')}}" name="simpan">Kembali</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <br><br><br>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/purchaseorder/purchaseorder_show.js') }}"></script>
@endsection
