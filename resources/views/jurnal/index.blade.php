<!-- ===========Create By Dedy 04-02-2020=============== -->
@extends('layouts.app')
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Jurnal')}}">Data Jurnal Transaksi</a>
        </h2>
    </div>
<!-- ======================= -->
<?php $hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)); ?>
    <br>
    <div class="banner container">
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                    <div class="form_input">
                        <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
                    </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
                </div>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select name="jenispencarian" id="jenispencarian" data-placeholder="Cari Data Berdasarkan" style="width: 100%!important" class="chosen-select" tabindex="13">
                    <option value=""></option>
                    <option value="Nomor">No Transaksi</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>
            </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="banner">
            <div class="widget_content">
                        <!-- <table class="display" id="action_tbl"> -->
                <table id="tbljurnal" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                    <thead style="color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nomor Transaksi</th>
                            <th>Kode COA</th>
                            <th>Nama COA</th>
                            <th>Debet</th>
                            <th>Kredit</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid;" id="previewdata">
                    <?php 
                    $i = 1;
                    foreach ($jurnal as $row) { ?>
                    <tr class="odd gradeA">
                        <td> <center><?php echo $i; ?></center></td>
                        <td> <center><?php echo date('d M Y', strtotime($row->Tanggal)); ?></center></td>
                        <td> <center><?php echo $row->Nomor; ?></center></td>
                        
                        <?php if($row->Status=='kepala'){ ?>
                            <td> <center><?php echo $row->kode_anak; ?></center></td>
                            <td> <?php echo $row->coa_anak; ?></td>
                        <?php }else{ ?>
                            <td> <center><?php echo $row->Kode_COA; ?></center></td>
                            <td> <?php echo $row->Nama_COA; ?></td>
                        <?php } ?>
                        
                        <td style="text-align:right;">Rp. <?php echo number_format($row->Debet, 0, ',', '.'); ?></right></td>
                        <td style="text-align:right;">Rp. <?php echo number_format($row->Kredit, 0, ',', '.'); ?></right></td>
                        <td><?php echo $row->Keterangan; ?></td>
                     </tr>
                                <?php
                                        $i++;
                                    }
                                
                                ?>  
                    </tbody>
                </table>
    </div>
</div>

<script src="{{ asset('js/jurnal/jurnal_index.js') }}"></script>
<script type="text/javascript">
    var token = "{{ csrf_token() }}";
    var urlCari = "{{url('Po/pencarian')}}";
</script>
@endsection
