@extends('layouts.app')   
@section('content')

<?php
use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('HargaJualBarang')}}">Data Harga Jual Barang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Data Harga Jual Barang - {{ $data_barang->Nama_Barang }}</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">     

      <span>List Harga Jual Barang {{ $data_barang->Nama_Barang }}</span>
    </div>
    <div class="bannerbody">
        <div class="container">
          <table class="table table-responsive">
              <tr>
                  <td style="width: 20%;">
                      Nama Barang
                  </td>
                  <td style="width: 5%;"> : </td>
                  <td>
                      {{ $data_barang->Nama_Barang }}
                  </td>
              </tr>
          </table>
          <div class="container" style="width: 100%">
              <br>
              <table class="table table-bordered" id="table-satuan" style="font-size: 12px;">
                  <thead style="background-color: #16305d; color: white">
                    <tr>
                        <th style="width: 5%;">No</th>
                        <th style="width: 20%;">Group Customer</th>
                        <th style="width: 15%">Satuan</th>
                        <th style="width: 15%;">Mata Uang</th>
                        <th style="width: 20%;">Modal</th>
                        <th style="width: 20%;">Jual</th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data_harga_jual_barang as $key => $item)
                        <tr>
                          <td> {{ ++$key }} 
                            <input type="hidden" class="id" name="IDHargaJual" value="{{ $item->IDHargaJual }}">
                            <input type="hidden" class="modal" name="Modal" value="{{ $item->Modal }}">
                            <input type="hidden" class="harga_jual" name="Harga_Jual" value="{{ $item->Harga_Jual }}">
                            <input type="hidden" class="Nama_Group_Customer" name="Nama_Group_Customer" value="{{ $item->Nama_Group_Customer }}">
                            <input type="hidden" class="Satuan" name="Satuan" value="{{ $item->Satuan }}">
                            <input type="hidden" class="Mata_uang" name="Mata_uang" value="{{ $item->Mata_uang }}">
                            <input type="hidden" class="IDMataUang" name="IDMataUang" value="{{ $item->IDMataUang }}">
                            <input type="hidden" class="IDGroupCustomer" name="IDGroupCustomer" value="{{ $item->IDGroupCustomer }}">
                            <input type="hidden" class="IDBarang" name="IDBarang" value="{{ $item->IDBarang }}">
                          </td>
                          <td> {{ $item->Nama_Group_Customer }} </td>
                          <td> {{ $item->Satuan }} </td>
                          <td> {{ $item->Mata_uang }} </td>
                          <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Modal, $angkakoma) }} </td>
                          <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga_Jual, $angkakoma) }} </td>
                          <td> <span class="action-icons edit"><i class="fa fa-pencil fa-lg"></i></span> &nbsp;
                            <span><a title="Hapus" onclick="deleteData('{{ $item->IDHargaJual }}', '{{ url('HargaJualBarang') . '/' . $item->IDHargaJual }}')"><i class="fa fa-times-circle fa-lg"  style="color: red"></i></a></span>
                          </td>
                        </tr>
                    @endforeach
                  </tbody>
              </table>
              <div class="btn col-16">
                  <span> <a style="color: white; float: right;" href="{{ route('HargaJualBarang.index') }}">Kembali</a></span>
              </div>
          </div>
        </div>
        <br><br><br>
    </div>
  </div>
</div>



<div id="modal-data" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form id="form-data">
      {{ csrf_field() }}
      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Edit Harga Jual</h4>
          </div>
          <div class="modal-body">
            <div class="row" style="padding: 10px;">
              <input type="hidden" name="IDHargaJual" id="IDHargaJual">
              <input type="hidden" name="IDBarang" id="IDBarang">
              <label class="field_title">Group Customer</label>
              <select name="IDGroupCustomer" id="IDGroupCustomer" class="form-control select2" style="width: 100%">
                  <option value="">- Pilih Group Customer -</option>
                  @foreach ($data_customer_group as $item)
                      <option value="{{ $item->IDGroupCustomer }}"> {{ $item->Nama_Group_Customer }} </option>
                  @endforeach
              </select>
              <label class="field_title">Satuan</label>
              <div class="form_input">
                <input type="text" name="Satuan" id="Satuan" class="form-control" readonly>
              </div>
              <label class="field_title">Mata Uang</label>
              <select name="IDMataUang" id="IDMataUang" class="form-control select2" style="width: 100%">
                  <option value="">- Pilih Mata Uang -</option>
                  @foreach ($data_mata_uang as $item)
                      <option value="{{ $item->IDMataUang }}"> {{ $item->Mata_uang }} </option>
                  @endforeach
              </select>
              <label class="field_title">Modal</label>
              <div class="form_input">
                <input type="text" name="Modal" id="Modal" class="form-control text-right price">
              </div>
              <label class="field_title">Harga Jual</label>
              <div class="form_input">
                <input type="text" name="Harga_Jual" id="Harga_Jual" class="form-control text-right price">
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn col-16" class="close" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success">Simpan</button>
          </div>
      </div>
    </form>
  </div>
</div>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/hargajual/update.js') }}"></script>
<script>
    var urlUpdate   = '{{ route("HargaJualBarang.update_data") }}';
    var url         = '{{ url("HargaJualBarang") }}';
    var IDBarang    = '{{ $data_barang->IDBarang }}';
</script>

@endsection
