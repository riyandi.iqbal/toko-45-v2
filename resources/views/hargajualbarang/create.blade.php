<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
        font-size: 14px;
    }
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('HargaJualBarang.index')}}">Data Harga Jual Barang</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Harga Jual Barang</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Harga Jual Barang</span>
    </div>

    <div class="bannerbody">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td style="width: 20%;">
                        Nama Barang
                    </td>
                    <td style="width: 5%;"> : </td>
                    <td>
                        <select name="IDBarang" id="IDBarang" class="form-control select2" style="width: 100%">
                            <option value="">- Pilih Barang -</option>
                            @foreach ($data_barang as $item)
                                <option value="{{ $item->IDBarang }}" data-idsatuan="{{ $item->IDSatuan }}" data-satuan="{{ $item->Satuan }}"> {{ $item->Nama_Barang }} </option>
                            @endforeach
                        </select>
                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 50px;">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                        <li class="active"><a href="#tab-barang" data-toggle="tab">Input Harga Jual Barang</a></li>
                    </ul>
                </div>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="tab-barang">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <table class="table table-bordered" id="table-satuan" style="font-size: 12px;">
                                <thead style="background-color: #16305d; color: white">
                                <tr>
                                    <th style="width: 20%;">Group Customer</th>
                                    <th style="width: 15%">Satuan</th>
                                    <th style="width: 20%;">Mata Uang</th>
                                    <th style="width: 20%;">Modal</th>
                                    <th style="width: 20%;">Jual</th>
                                    <th style="width: 5%;">Action</th>
                                </tr>
                                </thead>
                                <tbody id="list-satuan">

                                </tbody>
                            </table>
                            <a class="btn col-11" id="btn-add-satuan" ><i class="col-11 hvr-icon-float-away">Tambah</i></a>
                            <div class="btn col-16">
                                <span> <a style="color: white; float: right;" href="{{ route('HargaJualBarang.index') }}">Kembali</a></span>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-info" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex    = '{{ route("HargaJualBarang.index") }}';
    var urlInsert = '{{ route("HargaJualBarang.store") }}';
    var url     = '{{ url("HargaJualBarang") }}';
    var urlSatuan = '{{ route("HargaJualBarang.get_satuan") }}';
    var data_customer_group = <?php echo json_encode($data_customer_group, JSON_PRETTY_PRINT) ?>;
    var data_mata_uang = <?php echo json_encode($data_mata_uang, JSON_PRETTY_PRINT) ?>;
    var data_satuan = [];
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/hargajual/create.js') }}"></script>

@endsection