<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
      <title>TOKO 45</title>
      <link rel="icon" type="image/png" href="{{ url ('log/images/icons/favicon.ico') }}"/>
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
      <!-- bootstrap-css -->
      <link rel="stylesheet" href="{{ url ('newtemp/css/bootstrap413.css') }}">
      <link rel="stylesheet" href="{{ url ('newtemp/css/dataTables.bootstrap4.min.css') }}">
      <!-- //bootstrap-css -->
      <!-- Custom CSS -->
      <link href="{{ url ('newtemp/css/style.css?5') }}" rel='stylesheet' type='text/css' />
      <!-- font CSS -->
      <link href="{{ url ('newtemp/css/font-robot.css') }}" type='text/css'>
      <!-- font-awesome icons -->
      <link rel="stylesheet" href="{{ url ('newtemp/css/font.css') }}" type="text/css"/>
      <link href="{{ url ('newtemp/css/font-awesome.css') }}" rel="stylesheet">
      <link href="{{ url ('newtemp/css/component-chosen.css') }}" type='text/css'>
      <link href="{{ url ('newtemp/editable/jquery-editable-select.css') }}" rel="stylesheet" />
<style>
  #dialogoverlay{
      display: none;
      opacity: .8;
      position: fixed;
      top: 0px;
      left: 0px;
      background: #FFF;
      width: 100%;
      z-index: 10;
  }
  #dialogbox{
      display: none;
      position: fixed;
      background: #000;
      border-radius:7px;
      width:550px;
      z-index: 10;
  }
  #dialogbox > div{ background:#FFF; margin:8px; }
  #dialogbox > div > #dialogboxhead{ background: #666; font-size:19px; padding:10px; color:#CCC; }
  #dialogbox > div > #dialogboxbody{ background:#333; padding:20px; color:#FFF; }
  #dialogbox > div > #dialogboxfoot{ background: #666; padding:10px; text-align:right; }
</style>
<script>
  function CustomAlert(){
      this.render = function(dialog){
          var winW = window.innerWidth;
          var winH = window.innerHeight;
          var dialogoverlay = document.getElementById('dialogoverlay');
          var dialogbox = document.getElementById('dialogbox');
          dialogoverlay.style.display = "block";
          dialogoverlay.style.height = winH+"px";
          dialogbox.style.left = (winW/2) - (550 * .5)+"px";
          dialogbox.style.top = "100px";
          dialogbox.style.display = "block";
          document.getElementById('dialogboxhead').innerHTML = "Acknowledge This Message";
          document.getElementById('dialogboxbody').innerHTML = dialog;
          document.getElementById('dialogboxfoot').innerHTML = '<button onclick="Alert.ok()">OK</button>';
      }
      this.ok = function(){
          document.getElementById('dialogbox').style.display = "none";
          document.getElementById('dialogoverlay').style.display = "none";
      }
  }
  var Alert = new CustomAlert();
</script>

<div id="dialogoverlay"></div>
<div id="dialogbox">
  <div>
    <div id="dialogboxhead"></div>
    <div id="dialogboxbody"></div>
    <div id="dialogboxfoot"></div>
  </div>
</div>

<style type="text/css">

    * { box-sizing: border-box; }
body {
  font: 12px Arial;
}
.autocomplete {
  /*the container must be positioned relative:*/
  position: relative;
  display: inline-block;
}
.judul {
    font-size: <?php echo $coreset->FontSize ?>px;
    font-weight: <?php echo $coreset->FontStyle ?>;
    font-family: <?php echo $coreset->Font ?>;
    padding: 10px;
}
input {
  border: 1px solid transparent;
  background-color: #f1f1f1;
  padding: 10px;
  font-size: 16px;
}
input[type=text] {
  background-color: #f1f1f1;
  width: 100%;
    font-size: <?php echo $coreset->FontSize ?>px;
    font-weight: <?php echo $coreset->FontStyle ?>;
    font-family: <?php echo $coreset->Font ?>;
}
input[type=submit] {
  background-color: DodgerBlue;
  color: #fff;
}
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
  left: 0;
  right: 0;
}
.autocomplete-items div {
  padding: 10px;
  cursor: pointer;
  background-color: #fff;
  border-bottom: 1px solid #d4d4d4;
}
.autocomplete-items div:hover {
  /*when hovering an item:*/
  background-color: #e9e9e9;
}
.autocomplete-active {
  /*when navigating through the items using the arrow keys:*/
  background-color: DodgerBlue !important;
  color: #ffffff;
}

  .chosen-select {
  width: 100%;
}
.chosen-select-deselect {
  width: 100%;
}
.chosen-container {
  display: inline-block;
  font-size: 14px;
  position: relative;
  vertical-align: middle;
  width:100%;
}
.chosen-container .chosen-drop {
  background: #ffffff;
  border: 1px solid #cccccc;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  -webkit-box-shadow: 0 8px 8px rgba(0, 0, 0, .25);
  box-shadow: 0 8px 8px rgba(0, 0, 0, .25);
  margin-top: -1px;
  position: absolute;
  top: 100%;
  left: -9000px;
  z-index: 1060;
}
.chosen-container.chosen-with-drop .chosen-drop {
  left: 0;
  right: 0;
}
.chosen-container .chosen-results {
  color: #555555;
  margin: 0 4px 4px 0;
  max-height: 240px;
  padding: 0 0 0 4px;
  position: relative;
  overflow-x: hidden;
  overflow-y: auto;
  -webkit-overflow-scrolling: touch;
}
.chosen-container .chosen-results li {
  display: none;
  line-height: 1.42857143;
  list-style: none;
  margin: 0;
  padding: 5px 6px;
}
.chosen-container .chosen-results li em {
  background: #feffde;
  font-style: normal;
}
.chosen-container .chosen-results li.group-result {
  display: list-item;
  cursor: default;
  color: #999;
  font-weight: bold;
}
.chosen-container .chosen-results li.group-option {
  padding-left: 15px;
}
.chosen-container .chosen-results li.active-result {
  cursor: pointer;
  display: list-item;
}
.chosen-container .chosen-results li.highlighted {
  background-color: #428bca;
  background-image: none;
  color: white;
}
.chosen-container .chosen-results li.highlighted em {
  background: transparent;
}
.chosen-container .chosen-results li.disabled-result {
  display: list-item;
  color: #777777;
}
.chosen-container .chosen-results .no-results {
  background: #eeeeee;
  display: list-item;
}
.chosen-container .chosen-results-scroll {
  background: white;
  margin: 0 4px;
  position: absolute;
  text-align: center;
  /*width: 321px;*/
  z-index: 1;
}
.chosen-container .chosen-results-scroll span {
  display: inline-block;
  height: 1.42857143;
  text-indent: -5000px;
  width: 9px;
}
.chosen-container .chosen-results-scroll-down {
  bottom: 0;
}
.chosen-container .chosen-results-scroll-down span {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") no-repeat -4px -3px;
}
.chosen-container .chosen-results-scroll-up span {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") no-repeat -22px -3px;
}
.chosen-container-single .chosen-single {
  background-color: #ffffff;
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  border: 1px solid #cccccc;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  color: #555555;
  display: block;
  height: 34px;
  overflow: hidden;
  line-height: 34px;
  padding: 0 0 0 8px;
  position: relative;
  text-decoration: none;
  white-space: nowrap;
}
.chosen-container-single .chosen-single span {
  display: block;
  margin-right: 26px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.chosen-container-single .chosen-single abbr {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") right top no-repeat;
  display: block;
  font-size: 1px;
  height: 10px;
  position: absolute;
  right: 26px;
  top: 12px;
  width: 12px;
}
.chosen-container-single .chosen-single abbr:hover {
  background-position: right -11px;
}
.chosen-container-single .chosen-single.chosen-disabled .chosen-single abbr:hover {
  background-position: right 2px;
}
.chosen-container-single .chosen-single div {
  display: block;
  height: 100%;
  position: absolute;
  top: 0;
  right: 0;
  width: 18px;
}
.chosen-container-single .chosen-single div b {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") no-repeat 0 7px;
  display: block;
  height: 100%;
  width: 100%;
}
.chosen-container-single .chosen-default {
  color: #777777;
}
.chosen-container-single .chosen-search {
  margin: 0;
  padding: 3px 4px;
  position: relative;
  white-space: nowrap;
  z-index: 1000;
}
.chosen-container-single .chosen-search input[type="text"] {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") no-repeat 100% -20px, #ffffff;
  border: 1px solid #cccccc;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  margin: 1px 0;
  padding: 4px 20px 4px 4px;
  width: 100%;
}
.chosen-container-single .chosen-drop {
  margin-top: -1px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
}
.chosen-container-single-nosearch .chosen-search input {
  position: absolute;
  left: -9000px;
}
.chosen-container-multi .chosen-choices {
  background-color: #ffffff;
  border: 1px solid #cccccc;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  cursor: text;
  height: auto !important;
  height: 1%;
  margin: 0;
  overflow: hidden;
  padding: 0;
  position: relative;
}
.chosen-container-multi .chosen-choices li {
  float: left;
  list-style: none;
}
.chosen-container-multi .chosen-choices .search-field {
  margin: 0;
  padding: 0;
  white-space: nowrap;
}
.chosen-container-multi .chosen-choices .search-field input[type="text"] {
  background: transparent !important;
  border: 0 !important;
  -webkit-box-shadow: none;
  box-shadow: none;
  color: #555555;
  height: 32px;
  margin: 0;
  padding: 4px;
  outline: 0;
}
.chosen-container-multi .chosen-choices .search-field .default {
  color: #999;
}
.chosen-container-multi .chosen-choices .search-choice {
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  background-color: #eeeeee;
  border: 1px solid #cccccc;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  background-image: -webkit-linear-gradient(top, #ffffff 0%, #eeeeee 100%);
  background-image: -o-linear-gradient(top, #ffffff 0%, #eeeeee 100%);
  background-image: linear-gradient(to bottom, #ffffff 0%, #eeeeee 100%);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffeeeeee', GradientType=0);
  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
  color: #333333;
  cursor: default;
  line-height: 13px;
  margin: 6px 0 3px 5px;
  padding: 3px 20px 3px 5px;
  position: relative;
}
.chosen-container-multi .chosen-choices .search-choice .search-choice-close {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") right top no-repeat;
  display: block;
  font-size: 1px;
  height: 10px;
  position: absolute;
  right: 4px;
  top: 5px;
  width: 12px;
  cursor: pointer;
}
.chosen-container-multi .chosen-choices .search-choice .search-choice-close:hover {
  background-position: right -11px;
}
.chosen-container-multi .chosen-choices .search-choice-focus {
  background: #d4d4d4;
}
.chosen-container-multi .chosen-choices .search-choice-focus .search-choice-close {
  background-position: right -11px;
}
.chosen-container-multi .chosen-results {
  margin: 0 0 0 0;
  padding: 0;
}
.chosen-container-multi .chosen-drop .result-selected {
  display: none;
}
.chosen-container-active .chosen-single {
  border: 1px solid #66afe9;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  -webkit-transition: border linear .2s, box-shadow linear .2s;
  -o-transition: border linear .2s, box-shadow linear .2s;
  transition: border linear .2s, box-shadow linear .2s;
}
.chosen-container-active.chosen-with-drop .chosen-single {
  background-color: #ffffff;
  border: 1px solid #66afe9;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  -webkit-transition: border linear .2s, box-shadow linear .2s;
  -o-transition: border linear .2s, box-shadow linear .2s;
  transition: border linear .2s, box-shadow linear .2s;
}
.chosen-container-active.chosen-with-drop .chosen-single div {
  background: transparent;
  border-left: none;
}
.chosen-container-active.chosen-with-drop .chosen-single div b {
  background-position: -18px 7px;
}
.chosen-container-active .chosen-choices {
  border: 1px solid #66afe9;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  box-shadow: 0 1px 1px rgba(0, 0, 0, .075) inset, 0 0 8px rgba(82, 168, 236, .6);
  -webkit-transition: border linear .2s, box-shadow linear .2s;
  -o-transition: border linear .2s, box-shadow linear .2s;
  transition: border linear .2s, box-shadow linear .2s;
}
.chosen-container-active .chosen-choices .search-field input[type="text"] {
  color: #111 !important;
}
.chosen-container-active.chosen-with-drop .chosen-choices {
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.chosen-disabled {
  cursor: default;
  opacity: 0.5 !important;
}
.chosen-disabled .chosen-single {
  cursor: default;
}
.chosen-disabled .chosen-choices .search-choice .search-choice-close {
  cursor: default;
}
.chosen-rtl {
  text-align: right;
}
.chosen-rtl .chosen-single {
  padding: 0 8px 0 0;
  overflow: visible;
}
.chosen-rtl .chosen-single span {
  margin-left: 26px;
  margin-right: 0;
  direction: rtl;
}
.chosen-rtl .chosen-single div {
  left: 7px;
  right: auto;
}
.chosen-rtl .chosen-single abbr {
  left: 26px;
  right: auto;
}
.chosen-rtl .chosen-choices .search-field input[type="text"] {
  direction: rtl;
}
.chosen-rtl .chosen-choices li {
  float: right;
}
.chosen-rtl .chosen-choices .search-choice {
  margin: 6px 5px 3px 0;
  padding: 3px 5px 3px 19px;
}
.chosen-rtl .chosen-choices .search-choice .search-choice-close {
  background-position: right top;
  left: 4px;
  right: auto;
}
.chosen-rtl.chosen-container-single .chosen-results {
  margin: 0 0 4px 4px;
  padding: 0 4px 0 0;
}
.chosen-rtl .chosen-results .group-option {
  padding-left: 0;
  padding-right: 15px;
}
.chosen-rtl.chosen-container-active.chosen-with-drop .chosen-single div {
  border-right: none;
}
.chosen-rtl .chosen-search input[type="text"] {
  background: url("{{URL::to('/')}}/newtemp/images/chosen-sprite.png") no-repeat -28px -20px, #ffffff;
  direction: rtl;
  padding: 4px 5px 4px 20px;
}

@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-resolution: 144dpi) {
  .chosen-rtl .chosen-search input[type="text"],
  .chosen-container-single .chosen-single abbr,
  .chosen-container-single .chosen-single div b,
  .chosen-container-single .chosen-search input[type="text"],
  .chosen-container-multi .chosen-choices .search-choice .search-choice-close,
  .chosen-container .chosen-results-scroll-down span,
  .chosen-container .chosen-results-scroll-up span {
    background-image: url("chosen-sprite@2x.png") !important;
    background-size: 52px 37px !important;
    background-repeat: no-repeat !important;
  }
}
    @media (max-width: 479px) {
        .gambarnya {
            width: 7%;
        }
        .buttonlog {
            left: 0; bottom: 0; width: 100%; color: white; text-align: center;
        }
    }
    @media (min-width: 480px) {
        .gambarnya {
            width: 45%;
        }
        .buttonlog {
            position: fixed; left: 0; bottom: 0; width: 100%; color: white; text-align: center;
        }
    }

    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: #fff;
    }
    .preloader .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      font: 14px arial;
    }

    .custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid, .was-validated .form-control:invalid {
        border-color: #dc3545;
    }

    .is-invalid .select2-selection,
    .needs-validation ~ span > .select2-dropdown{
      border-color:#dc3545 !important;
    }

    /*=============================TREE VIEW */
    .tree, .tree ul {
        margin:0;
        padding:0;
        list-style:none
    }
    .tree ul {
        margin-left:1em;
        position:relative
    }
    .tree ul ul {
        margin-left:.5em
    }
    .tree ul:before {
        content:"";
        display:block;
        width:0;
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        border-left:1px solid
    }
    .tree li {
        margin:0;
        padding:0 1em;
        line-height:2em;
        color:#369;
        font-weight:700;
        position:relative;
        display:list-item;
    }
    .tree ul li:before {
        content:"";
        display:block;
        width:10px;
        height:0;
        border-top:1px solid;
        margin-top:-1px;
        position:absolute;
        top:1em;
        left:0
    }
    .tree ul li:last-child:before {
        background:#fff;
        height:auto;
        top:1em;
        bottom:0
    }
    .indicator {
        margin-right:5px;
    }
    .tree li a {
        text-decoration: none;
        color:#369;
    }
    .tree li button, .tree li button:active, .tree li button:focus {
        text-decoration: none;
        color:#369;
        border:none;
        background:transparent;
        margin:0px 0px 0px 0px;
        padding:0px 0px 0px 0px;
        outline: 0;
    }

/*========================pindahan dari style.css karena ga bisa pakai php */
.form-title {
  color: #ffffff;
	padding: 1em 2em;
    background-color: #254283;
    border-bottom: 3px solid #6894e0;
    font-size: <?php echo $coreset->FontSize ?>px;
    font-weight: <?php echo $coreset->FontStyle ?>;
    font-family: <?php echo $coreset->Font ?>;
}
.nav-tabs>li>a{
  border:0;border-radius:0;
  background-color:#6894e0;
  color:#fff;
  -webkit-transition:all .1s;
  transition:all .1s;
  font-size: <?php echo $coreset->FontSize ?>px;
  font-weight: <?php echo $coreset->FontStyle ?>;
  font-family: <?php echo $coreset->Font ?>;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: 700;
    text-align: center;
    vertical-align: middle;
    text-transform: uppercase;
    cursor: pointer;
    background-image: none;
    border: 0;
    border-color: rgba(0, 0, 0, 0.07) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.18);
    white-space: nowrap;
    padding: 8px 15px;
    border-radius: 3px;
    color: #fff;
    letter-spacing: 0.02em;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    font-family: <?php echo $coreset->Font ?>;
}
.banner {
    background: #fff;
    display: block;
    padding: 1em;
    border: 1px solid #ebeff6;
    border-color: #ebeff6;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
    margin-bottom : 5px;
    font-size: <?php echo $coreset->FontSize ?>px;
    font-weight: <?php echo $coreset->FontStyle ?>;
    font-family: <?php echo $coreset->Font ?>;
}
.bannerbody {
  background: #fff;
  display: block;
  padding: 1em;
  border: 1px solid #ebeff6;
  border-color: #ebeff6;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
  box-shadow: 0 1px 1px rgba(0,0,0,.05);
  margin-bottom : 75px;
  font-size: <?php echo $coreset->FontSize ?>px;
  font-weight: <?php echo $coreset->FontStyle ?>;
  font-family: <?php echo $coreset->Font ?>;
}
.text-center {
  font-family: <?php echo $coreset->Font ?>;
}
.form-group {
  font-family: <?php echo $coreset->Font ?>;
}
</style>
      <!-- //font-awesome icons -->
      <script src="{{ url ('newtemp/js/jquery2.0.3.min.js') }}"></script>
      <script src="{{ url ('newtemp/js/ajax.js') }}"></script>
      <script src="{{ url ('newtemp/js/modernizr.js') }}"></script>
      <script src="{{ url ('newtemp/js/jquery.cookie.js') }}"></script>
      <script src="{{ url ('newtemp/js/screenfull.js') }}"></script>
      <script src="{{ url ('newtemp/editable/jquery-editable-select.js')}}"></script>

          <script>

          $(function () {
            $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

            if (!screenfull.enabled) {
              return false;
            }



            $('#toggle').click(function () {
              screenfull.toggle($('#container')[0]);
            });
          });
          </script>
      <!-- charts -->
      <script src="{{ url ('newtemp/js/raphael-min.js') }}"></script>
      <script src="{{ url ('newtemp/js/morris.js') }}"></script>
      <link rel="stylesheet" href="{{ url ('newtemp/css/morris.css') }}">
      <!-- //charts -->
      <!--skycons-icons-->
      <script src="{{ url ('newtemp/js/skycons.js') }}"></script>
      <!--//skycons-icons-->
      <script src="{{ url ('newtemp/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ url ('newtemp/js/dataTables.bootstrap.min.js') }}"></script>
        <script type="text/javascript">

            var save_method; //for save method string
            var table;

            $(document).ready(function() {
                //datatables
                table = $('#table').DataTable({
                    //"ordering": false,
                    "info":     false,
                    "lengthChange": false
                });

            });
        </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js"></script>
      <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
      <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
     <style>
       #modal-loading {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            text-align: center;
            vertical-align: middle;
            padding-top: 20%;
            background: black;
            color: white;
            opacity: 0.8;
            display: none;
            z-index: 9999;
        }

        .form-white {
          background-color: #fff !important;
        }

        .error {
          color: #ff0101;
        }
     </style>
</head>
<body>
    <div id="app">
      <div id="modal-loading">
          <h4>
              Memproses
              <i class="fa fa-spinner fa-spin"></i>
          </h4>
      </div>


        <nav class="user-menu">
            <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
            </a>

        </nav>
<nav class="main-menu" data-spy="scroll" data-target="#myScrollspy" data-offset="20" >
        <ul class="">
            <li>
                <a href="{{url('home')}}">
                    <i class="icon-home nav-icon"></i>
                </a>
            </li>
            <!-- <li>
                <a href="{{url('Laporan')}}">
                    <i class="icon-book nav-icon" title="LAPORAN"></i>
                </a>
            </li> -->
        </ul>
        <ul>
                        <?php
$i = 1;
foreach ($aksesmenu as $menu) {
    ?>
                        <li class="has-subnav" >
                            <a href="javascript:;">
                                <i class="nav-icon"  aria-hidden="true"><img src="{{URL::to('/')}}/log/images/iconmenu/<?php echo $menu->icon; ?>" class="gambarnya"></img></i>
                            </a>
                            <ul style="height: 95%; overflow-y: auto; margin-top: 5px;">
                            <br>
                            <h4>
                              <span style="color: #fff;
                                            font-weight: bold;
                                            width: 100%;
                                            margin-left: 25%;
                                            font-family: <?php echo $coreset->Font ?>;"><?php echo $menu->Menu ?></span>
                            </h4>
                            <hr>
                            <?php foreach ($aksesmenudetail as $detail) {
        if ($menu->IDMenu == $detail->IDMenu) {?>

                                <li class="has-subnav">
                                    <a class="subnav-text" href="{{URL::to('/')}}/<?php echo $detail->Url ?>" style="font-family: <?php echo $coreset->Font ?>;">
                                        <span class="list-icon">&nbsp;</span><?php echo $detail->Menu_Detail ?>
                                    </a>
                                </li>

                                <?php if ($detail->Menu_Detail == 'Laporan Pembelian') {?>
                                   <li class="has-subnav">
                                        <a href="#">
                                            <span class="nav_icon computer_imac"></span> Dashboard
                                            <span class="alert_notify blue">4</span>
                                            <span class="up_down_arrow">&nbsp;</span>
                                        </a>
                                        <ul>
                                            <li class="has-subnav"><a href="{{URL::to('/')}}/home"><span class="list-icon">&nbsp;</span>Dashboard Main</a></li>
                                        </ul>
                                  </li>
                                <?php }}}?>
                            </ul>
                        </li>
                        <?php
$i++;
}
?>
        </ul>
        <ul class="buttonlog" style="">
            <li>
            <a href="{{url('logout')}}">
            <i class="icon-off nav-icon"></i>
            </a>
            </li>
        </ul>
    </nav>
        <section class="title-bar">
            <div id="header" style="background: white;">
             <center><div style="margin:auto;left:0;right: 0;" id="alert"></div></center>

                <section class="title-bar" style="background-color: white">
                    <div class="container" style="width: 95%;margin-left: 60px;">
                        <div class="logo" style="margin-top: 5px;">
                        <h1><a href="home"><img src="{{URL::to('/')}}/log/images/icons/logo.png" alt="" style="padding: 10px; width: 100%;"/></a></h1>
                    </div>
                    <div class="profile_details" style="margin-right: 25px;">
                        <ul>
                            <li class="dropdown profile_details_drop">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <div class="profile_img">
                                        <span class="prfil-img"><i class="fa fa-user fa-2x" aria-hidden="true"></i></span>
                                            <div class="clearfix"></div>
                                    </div>
                                </a>

                                <ul class="dropdown-menu drp-mnu">
                                    <li> <a href="#"><i class="fa fa-user"></i>&nbsp;<?php echo $namauser ?></a> </li>
                                    <li> <a href="logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    </div>
                </section>
            </div>
        </section>

        @yield('content')


        @yield('footer')
    </div>

    <!-- Scripts -->

<script src="{{ url ('newtemp/js/bootstrap.js') }}"></script>
<script src="{{ url ('newtemp/js/proton.js')}}"></script>
<script src="{{ url ('newtemp/js/sweetalert.min.js')}}"></script>
  <!-- CSS -->


<!-- JS -->


<script src="{{ url ('newtemp/js/chosen.jquery.min.js')}}"></script>
  <script>
    // When the user scrolls down 20px from the top of the document, slide down the navbar
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() {
      if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrolling").style.top = "0";
      } else {
        document.getElementById("scrolling").style.top = "-50px";
      }
    }
   </script>
<script>
function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("id", this.id + "autocomplete-list");
      a.setAttribute("class", "autocomplete-items");
      this.parentNode.appendChild(a);
      for (i = 0; i < arr.length; i++) {
        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          b = document.createElement("DIV");
          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
          b.innerHTML += arr[i].substr(val.length);
          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
          b.addEventListener("click", function(e) {
              inp.value = this.getElementsByTagName("input")[0].value;
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("div");
      if (e.keyCode == 40) {
        currentFocus++;
        addActive(x);
      } else if (e.keyCode == 38) {
        currentFocus--;
        addActive(x);
      } else if (e.keyCode == 13) {
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}

      $(function() {
        $('.chosen-select').chosen();
        $('.chosen-select-deselect').chosen({ allow_single_deselect: true });
      });
</script>
<script type="text/javascript">

    var tanpa_rupiah_debit = document.getElementById('harga-123');
    if(tanpa_rupiah_debit){
        tanpa_rupiah_debit.addEventListener('keyup', function(e)
        {
            tanpa_rupiah_debit.value = formatRupiah(this.value);
        });

        tanpa_rupiah_debit.addEventListener('keydown', function(event)
        {
            limitCharacter(event);
        });
    }

    var numberField = document.getElementsByClassName('harga-123');
    if(numberField){
        $.each(numberField, function(key, value){
            console.log(key);
            $(this).on('keyup', function(e)
            {
                $(this).val(formatRupiah($(this).val() ) );
            });
        });
    }



    var tanpa_rupiah_kredit = document.getElementById('tanpa-rupiah-kredit');
    if(tanpa_rupiah_kredit){
    tanpa_rupiah_kredit.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_kredit.value = formatRupiah(this.value);
    });

    tanpa_rupiah_kredit.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

var tanpa_rupiah_debit1 = document.getElementById('tanpa-rupiah-debit');
    if(tanpa_rupiah_debit1){
    tanpa_rupiah_debit1.addEventListener('keyup', function(e)
    {
        tanpa_rupiah_debit1.value = formatRupiah(this.value);
    });

    tanpa_rupiah_debit1.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

var uangsejumlah = document.getElementById('uangsejumlah');
    if(uangsejumlah){
    uangsejumlah.addEventListener('keyup', function(e)
    {
        uangsejumlah.value = formatRupiah(this.value);
    });

    uangsejumlah.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}

var KreditatauDebet = document.getElementById('KreditatauDebet');
    if(KreditatauDebet){
    KreditatauDebet.addEventListener('keyup', function(e)
    {
        KreditatauDebet.value = formatRupiah(this.value);
    });

    KreditatauDebet.addEventListener('keydown', function(event)
    {
        limitCharacter(event);
    });
}



    function formatRupiah(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
        split   = number_string.split(','),
        sisa    = split[0].length % 3,
        rupiah  = split[0].substr(0, sisa),
        ribuan  = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

</script>

<script>
   $(document).on('click', '#simtam', function () {
       tanggal = $('#Tanggal').val();
       no_bo = $('#no_bo').val();
       tanggal_selesai = $('#Tanggal_selesai').val();
       id_corak = $('#id_corak').val();
       qty = $('#qty').val();
       Keterangan = $('#Keterangan').val();

       if (tanggal != '' && no_bo != '' && tanggal_selesai != '' && qty != '') {
             //console.log(tanggal + ' ' + tanggal_selesai + ' ' + no_bo + ' ' + id_corak + ' ' + qty + ' ' + Keterangan);
             $.post('BookingOrder/store', {'tanggal' : tanggal, 'no_bo' : no_bo, 'tanggal_selesai' : tanggal_selesai, 'id_corak' : id_corak, 'qty' : qty, 'Keterangan' : Keterangan})
             .done(function(res) {
                     window.location.href = "BookingOrder/create";
                     document.getElementById("simtam").disabled = true;
                 });
         } else {
           $('#alert').html('<div class="pesan sukses">Isi Semua Data Dengan Lengkap</div>');
       }
   });

   function print_data() {
       bo = $('#no_bo').val();

       window.open("BookingOrder/print_data/" + bo);
   }
</script>
<script type="text/javascript">
    function rupiah(nStr) {
       nStr += '';
       x = nStr.split('.');
       x1 = x[0];
       x2 = x.length > 1 ? '.' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1))
       {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
  }

  function rupiahkoma(nStr) {
       nStr += '';
       x = nStr.split(',');
       x1 = x[0];
       x2 = x.length > 1 ? ',' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1))
       {
          x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      return x1 + x2;
  }

  function formatDate(date) {
      var d = new Date(date),
          //month = '' + (d.getMonth() + 1),
          day = '' + d.getDate(),
          year = d.getFullYear();

          var month = new Array();
              month[0] = "Jan";
              month[1] = "Feb";
              month[2] = "Mar";
              month[3] = "Apr";
              month[4] = "May";
              month[5] = "Jun";
              month[6] = "Jul";
              month[7] = "Aug";
              month[8] = "Sep";
              month[9] = "Oct";
              month[10] = "Nov";
              month[11] = "Dec";
              var n = month[d.getMonth()];

      // if (month.length < 2)
      //     month = '0' + month;
      if (day.length < 2)
          day = '0' + day;

      return [day, n, year].join(' ');
  }

</script>
@if (session('failed'))
  <script>
    alert('{{ session("failed") }}')
  </script>
@endif
@if (session('success'))
  <script>
    alertSuccess('{{ session("success") }}')
  </script>
@endif
</body>
</html>


