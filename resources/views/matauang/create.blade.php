<!-- create @2019-12-10
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<?php
if ($kode->curr_number == null) {
  $number = 1;
} else {
  $number = $kode->curr_number + 1;
}

$kodemax = str_pad($number, 2, "0", STR_PAD_LEFT);
$code = 'MU-'.$kodemax;
?>

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{ url ('home') }}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{ url ('Matauang') }}">Data Mata Uang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Mata Uang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Mata Uang</span>
    </div>
    <div class="banner">
    	<div class="container">
        <table width="100%">
          <tr>
            <td>Tanggal</td>
            <td>Kurs</td>
            <td>ID Mata Uang</td>
          </tr>
          <tr>
            <td>
              <input type="hidden" class="form-control" name="kodematauang" id="kodematauang" value="<?php echo $code ?>" readonly>
              <input type="date" class="form-control" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d'); ?>">
            </td>
            <td>
              <input type="number" class="form-control" name="kurs" id="kurs">
            </td>
            <td>
              <select data-placeholder="Cari Mata Uang" class="chosen-select" name="idmatauang" id="idmatauang" required oninvalid="this.setCustomValidity('Mata Uang Tidak Boleh Kosong')" onchange="gantilisting()">
                        <?php foreach ($idr as $row) {
                            echo "<option value='$row->Nama'>$row->Nama</option>";
                        }
                        ?>
              </select>
              <button class="btn btn-success" data-toggle="modal" data-target="#modalAdd">Add ID</button>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td style="text-align: right;"><a class="btn btn-success hvr-icon-float-away"><span style="color: white;" onclick="simpan()">Simpan</span></a></td>
          </tr>
        </table>
        <table id="tablekurs" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
          <thead style="color: #fff">
            <th>Tanggal</th>
            <th>Kurs</th>
            <th>ID Mata Uang</th>
            <th>Aksi</th>
          </thead>
          <tbody id="previewdata">
          </tbody>
        </table>

    	
    </div>
</div>

</div>
          <form method="post" action="{{ url ('Matauang/addidr') }}">

            <div id="modalAdd" class="modal fade" role="dialog">
              <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add ID Mata Uang!</h4>
                  </div>
                  <div class="modal-body">
                    {{ csrf_field() }}
                    <label>ID Mata Uang</label>
                    <input type="text" name="namaid" id="namaid" class="form-control" placeholder="USD/IDR/JPY/EUR">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>                   
                    <button type="submit" class="btn hvr-icon-shrink col-10">Simpan</button>
                  </div>
                </div>
              </div>
            </div>
          </form>

<script type="text/javascript">
  $(document).ready(function() {
        $('#tablekurs').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });
        tampildata();          
    })

	function field()
	{
		var data1 = {
          "Kodematauang"        	:$('#kodematauang').val(),
          "Tanggal"      			    :$('#tanggal').val(),
          "Idmatauang"     			  :$('#idmatauang').val(),
          "Kurs"					        :$('#kurs').val(),        
        }
        return data1;
	}

  function gantilisting()
  {
    tampildata();
  }

	var PM  = [];
	function tampildata()
	{
		var datacari = $('#idmatauang').val();
        $.ajax({
                  url : "{{ url ('Matauang/pencarian') }}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data:datacari},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        swal('Perhatian', 'Data Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];

                      if(PM[i].Aktif=='aktif'){
                         var $status  = "<a href='edit/"+PM[i].Kode+"' data-toggle='modal'><i class='fa fa-pencil fa-lg'></i></a>"
                      }else{
                         var $status  = "<a style='cursor: no-drop; color:grey;' href='edit/"+PM[i].Kode+"' data-toggle='modal'><i class='fa fa-pencil fa-lg'></i></a>"
                      }

                      var value =
                      "<tr>" +
                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Mata_uang+"</td>"+
                      "<td style='text-align: right'>"+PM[i].Kurs+"</td>"+
                      "<td style='text-align: center'>"+$status+"</td>"+
                      "</tr>";
                      $("#previewdata").append(value);
                      }
                      
                     }
                }
            });
	}

	function simpan()
	{
		var data = field();
		console.log($('#kurs').val());
		if($('#kurs').val()==''){
			swal('Perhatian', 'Kurs Masih Kosong', 'warning');
		}else if($('#idmatauang').val()==''){
			swal('Perhatian', 'ID Mata Uang Masih Kosong', 'warning');
		}else{
			console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{ url ('Matauang/simpandata') }}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}", data1:data},
                  dataType:'json',
                  beforeSend: function(data) {
                    $('#modal-loading').fadeIn('fast');
                  },
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    swal({
                        title: "Berhasil",
                        text: "Berhasil Simpan Data",
                        icon: "success"
                    }).then(function() {
                        window.location = "{{url ('Matauang') }}";
                    });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    swal('Gagal', 'Data Gagal Simpan / Data Sudah Ada', 'warning');
                 }
               });
		}		
	}
</script>
@endsection 