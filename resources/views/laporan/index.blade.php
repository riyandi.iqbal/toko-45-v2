<!-- /*=====Create DEDY @10/12/2019====*/ -->
@extends('layouts.app')   
@section('content')

<div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Laporan')}}">Data Laporan</a>
        </h2>
    </div>
    <br>
    <div class="bannerbody">
        <div id="content">
		    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
		        <li class="active"><a href="#tab-1" data-toggle="tab" style="border-radius: 5px;">Pembelian</a></li>
		        <li><a href="#tab-2" data-toggle="tab" style="border-radius: 5px;">Penjualan</a></li>
                <li><a href="#tab-3" data-toggle="tab" style="border-radius: 5px;">Keuangan</a></li>
                <li><a href="#tab-4" data-toggle="tab" style="border-radius: 5px;">Persediaan</a></li>
		    </ul>
            <br>
            <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active container" id="tab-1">
                        
                            <?php foreach ($laporanpembelian as $data1) { ?>
                            
                                <div class="col-md-3 container laporan">
                                    <a href="<?php echo $data1->Url ?>"><label class="judul2"><?php echo $data1->Menu_Detail ?></label></a>
                                    <br>
                                    <label class="isidata"><?php echo $data1->Keterangan ?></label>
                                    <br><br>
                                    <a href="<?php echo $data1->Url ?>"><button class="btn showdata" style="float: right; margin-bottom: 5px; color: #6894e0; border: 1px solid">SHOW MORE</button></a>
                                </div>
                            <?php } ?>
                    <br>    
		            </div>
		            <div class="tab-pane container" id="tab-2">
                    <?php foreach ($laporanpenjualan as $data2) { ?>
                            
                            <div class="col-md-3 container laporan">
                                <a href="<?php echo $data1->Url ?>"><label class="judul2"><?php echo $data2->Menu_Detail ?></label></a>
                                <br>
                                <label class="isidata"><?php echo $data2->Keterangan ?></label>
                                <br><br>
                                <a href="<?php echo $data2->Url ?>"><button class="btn showdata" style="float: right; margin-bottom: 5px; color: #6894e0; border: 1px solid">SHOW MORE</button></a>
                            </div>
                        <?php } ?>
                    <br>
		            </div>
                    <div class="tab-pane container" id="tab-3">
                    <?php foreach ($laporankeuangan as $data3) { ?>
                            
                            <div class="col-md-3 container laporan">
                                <a href="<?php echo $data1->Url ?>"><label class="judul2"><?php echo $data3->Menu_Detail ?></label></a>
                                <br>
                                <label class="isidata"><?php echo $data3->Keterangan ?></label>
                                <br><br>
                                <a href="<?php echo $data3->Url ?>"><button class="btn showdata" style="float: right; margin-bottom: 5px; color: #6894e0; border: 1px solid">SHOW MORE</button></a>
                            </div>
                        <?php } ?>
                    <br>
                    </div>
                    <div class="tab-pane container" id="tab-4">
                    <?php foreach ($laporanpersediaan as $data4) { ?>
                            
                            <div class="col-md-3 container laporan">
                                <a href="<?php echo $data1->Url ?>"><label class="judul2"><?php echo $data4->Menu_Detail ?></label></a>
                                <br>
                                <label class="isidata"><?php echo $data4->Keterangan ?></label>
                                <br><br>
                                <a href="<?php echo $data4->Url ?>"><button class="btn showdata" style="float: right; margin-bottom: 5px; color: #6894e0; border: 1px solid">SHOW MORE</button></a>
                            </div>
                        <?php } ?>
                    <br>
                    </div>
		    	</div>
        </div>
    </div>
</div>
@endsection