<!-- /*=====Create DEDY @10/12/2019====*/ -->
@extends('layouts.app')   
@section('content')

<div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Laporan')}}">Data Laporan</a>
        </h2>
    </div>
    <br>
    <div class="bannerbody">
        <div id="content">
		    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
		        
                <li><a href="#tab-4" data-toggle="tab" style="border-radius: 5px;">Persediaan</a></li>
		    </ul>
            <br>
            <div id="my-tab-content" class="tab-content">
		            
                    <div class="tab-pane active container" id="tab-4">
                    <?php foreach ($laporanpersediaan as $data4) { ?>
                            
                            <div class="col-md-3 container laporan">
                                <a href="{{ url($data4->Url) }}"><label class="judul2"><?php echo $data4->Menu_Detail ?></label></a>
                                <br>
                                <label class="isidata"><?php echo $data4->Keterangan ?></label>
                                <br><br>
                                <a href="{{ url($data4->Url) }}"><button class="btn showdata" style="float: right; margin-bottom: 5px; color: #6894e0; border: 1px solid">SHOW MORE</button></a>
                            </div>
                        <?php } ?>
                    <br>
                    </div>
		    	</div>
        </div>
    </div>
</div>
@endsection