<!-- create @2019-01-03
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Formula')}}">Data Formula</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Formula</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Formula</span>
    </div>
<form class="form-horizontal" action="{{url('Formula/simpandata')}}" method="post" onsubmit="$('#modal-loading').show();">
{{ csrf_field() }}
    <div class="banner">
        <div class="container">
        <br><br>
            <div class="col-md-3">
                <label class="judul">Tanggal</label>
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" name="tanggal" id="tanggal" readonly required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
            <br><br>
            </div>
            <div class="col-md-3">
                <label class="judul">Nama Formula</label>
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="nama" id="nama" required oninvalid="this.setCustomValidity('Nama Group Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>
        </div>
    <div class="banner">
        <table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;">
            <thead style="background-color: #16305d; color: white">
                <tr>
                    <th style="width: 25%">Nama Barang</th>
                    <th style="width: 10%">Qty</th>
                    <th style="width: 15%">Satuan</th>
                    <th style="width: 2%">Action</th>
                </tr>
            </thead>
            <tbody id="tabel-cart-po">
            </tbody>
        </table>
        <a class="btn col-11" id="buttonadd" onclick="addmore()" /><i class="col-11 hvr-icon-float-away">Tambah</i></a>
    </div>
    <br>
    <div>
        <div class="col-md-3">
            <label class="judul">Total</label>
        </div>
        <div class="col-md-3">
            <input type="tex" class="form-control" name="total" id="total" readonly>
        </div>
    </div>
    <br><br><br>
    <div class="text-center">
    <br><br>
        <div class="btn col-11 hvr-icon-back">
            <span> <a href="{{url('Formula')}}" style="color: white;" name="simpan">Kembali</a></span>
        </div>
        <button class="btn btn-success hvr-icon-float-away" type="submit">Simpan</button> 
    </div>
</div>
</form>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

			$('#Barang').editableSelect({
				effects: 'fade'
			});

			addmore();
			
    });

    //=================================ADDMORE ROW TABLE
    var i=1;
    function addmore()
	{
		i = i + 1;
		var htm_option_barang = "";
		var get_data_barang = JSON.parse(`<?php echo json_encode($barang); ?>`);
		for(var a in get_data_barang){
			var row_barang = get_data_barang[a];
			htm_option_barang+="<option value="+row_barang['IDBarang']+">"+row_barang['Nama_Barang']+"</option>";
		}	
		
		$('#tabelfsdfsf').append('<tr id="rowvar'+i+'" style="width:100%">'

            +'<td style="width:25%"><select  placeholder="Cari Barang" class="cari form-control" name="Barang_array[]" id="Barang'+i+'" onchange="gantisatuan(this.value, i)" style="width:100%;"><option value="" label="Pilih Barang"></option>'+htm_option_barang+'</select><input type="hidden" name="kode_baranga_array[]" class="form-control"></td>'

			+'<td style="width:15%"><input placeholder="Berat" type="tex" name="berat_array[]" id="berat_array'+i+'" onkeyup="hitung(this.value, i)" class="form-control"></td>'

			+'<td style="width:15%"><input type="text" id="Satuan_array'+i+'" name="Satuan_array[]" readonly class="form-control"><input type="hidden" id="kode_satuan_array'+i+'" name="kode_satuan_array[]" readonly class="form-control"></td><td style="width:2%"><a onclick="deleteitem('+i+')"><i class="fa fa-times-circle fa-lg" style="color:red; padding: 10px;"></i></a></td>'

			+'</tr>');

		$('.cari').select2(); 
	}

    function deleteitem(i)
	{
		var result = confirm("Apakah anda yakin?");
		if (result) {
			console.log('HITUNG ULANG TOTAL');
			a=0;
			a=$('#total').val()-$('#berat_array'+i).val();
            $('#total').val(a);

			$('#rowvar'+i).remove();
		}

	}

    var val_berat;
	var array_berat = new Array(100);
	for (var q = 0; q < array_berat.length; q++) {
		array_berat[q] = new Array(100);
	}

	function hitung(nilai, urutan)
	{
		$('#berat_array'+urutan).val(nilai.replace(/[^\d]/,''));

		total_berat = 0;
		array_berat[urutan][0]=urutan;
		array_berat[urutan][1]=parseInt(nilai);

		for (var y = 0; y < array_berat.length; y++) {
			if(array_berat[y][1]!=null){
				total_berat = parseInt(total_berat) + parseInt(array_berat[y][1]);
			}
		}

		var berat = $('#berat_array'+urutan).val();
		console.log('CEK BERAT');
		console.log(berat);

		$('#total').val(total_berat);
	}

    function simpan(){
		var ket = document.getElementById("nama").value;
		if (ket !="") {
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}

    function gantisatuan(nilai, urutan)
	{
		console.log('IDBARANG');
		console.log($('#Barang'+urutan).val());
		var idbrg 	= $('#Barang'+urutan).val();
		$.ajax({
				url : "{{url('Formula/getsatuan')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",data:idbrg},
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log(data[0].Satuan);
                    $('#Satuan_array'+urutan).val(data[0].Satuan);
                    $('#kode_satuan_array'+urutan).val(data[0].IDSatuan);
                  }
		});
		
	}
</script>
@endsection