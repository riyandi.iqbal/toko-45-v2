
<style type = "text/css">
</style>
<?php
use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
	<div class="banner">
		<div class="printkertas">
			<div class="headerprint">
				<button class="btn btn-primary" id="printgo" onclick="printgo()">Print</button>
				<a href="{{url('ReturPembelian')}}"><button class="btn btn-primary">Close</button></a>
			</div>			
			<div class="bodyprint">
				<br>
				<table width="90%">
					<tr>
						<td>
							<div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
							
						</td>
					</tr>
				</table>
				<hr>
				<br>
				<table width="90%">
					<tr>
						<td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="7">Return Pembelian</td>
					</tr>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td>Nomor</td>
						<td>:</td>
						<td><?php echo $data->Nomor; ?></td>
					</tr>
					<tr>
						<td>Tanggal</td>
						<td>:</td>
						<td><?php echo date("d M Y", strtotime($data->Tanggal)); ?></td>
					</tr>
					<tr>
						<td>Total Qty</td>
						<td>:</td>
						<td><?php echo $data->Total_qty_yard; ?></td>
					</tr>
				</table>
				<br>
				<table width="90%" class="table table-bordered">
					<thead>
						<th>No</th>
			            <th>Barang</th>
						<th>Qty</th>
						<th>Satuan</th>
			            <th>Harga Satuan</th>
			            <th>Total</th>
			     
			        </thead>
        <tbody>
          <?php if(empty($datadetail)){ ?>
          <?php }else{
            $i=1;
          foreach ($datadetail as $data2) { ?>
            <tr class="odd gradeA">
              <td class="text-center"><?php echo $i; ?></td>
              <td><?php echo $data2->Nama_Barang; ?></td>
              <td><?php echo $data2->Qty_yard; ?></td>
              <td><?php echo $data2->Satuan; ?></td>
              <td>{{ $data->Kode . ' ' . AppHelper::NumberFormat($data2->Harga, $angkakoma) }}</td>
              <td>{{ $data->Kode . ' ' . AppHelper::NumberFormat($data2->Subtotal, $angkakoma) }}</td>
           </tr>
          <?php
            $i++;
            }
          }
          ?>
        </tbody>
				</table>
				<br>
				<table width="90%">
					<tr>
						<td width="50%">Keterangan : </td>						
					</tr>
					<tr>
						<td colspan="2" width="50%"><?php echo $data->Keterangan; ?></td>
					</tr>
				</table>
				
			</div>
		</div>		
	</div>	
</div>

</div>
<script src="{{ url ('newtemp/js/jquery2.0.3.min.js') }}"></script>
<script type="text/javascript">

	function printgo()
	{
		$('.headerprint').hide();
		$('.main-menu').hide();
		$('.title-bar').hide();
		window.print();
		location.reload();
	}
</script>