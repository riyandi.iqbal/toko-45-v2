<!-- ===========Create By Tiar 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    .select2-selection__rendered {
        line-height: 31px !important;
    }
    .select2-container .select2-selection--single {
        height: 35px !important;
    }
    .select2-selection__arrow {
        height: 34px !important;
        font-size: 14px;
    }
</style>

<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('ReturPembelian.index')}}">Data Retur Pembelian</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Retur Pembelian</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Retur Pembelian</span>
    </div>

    <div class="bannerbody">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td style="width: 50%;">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control form-white datepicker" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date('d/m/Y') }}">
                    </td>
                    <td style="width: 50%;">
                      <label for="Tanggal_beli">Tanggal Pembelian</label>
                      <input type="text" class="form-control form-white datepicker" id="Tanggal_beli"  name="Tanggal_beli"  placeholder="Tanggal" required>
                    </td>
                </tr>
                <tr>
                    <td>
                      <label for="Nomor">Nomor Retur</label>
                      <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Retur" readonly>
                    </td>
                    <td style="vertical-align: middle;">
                      <label for="IDSupplier">Nama Supplier</label>
                      <input type="text" name="NamaSupplier" id="NamaSupplier" class="form-control" placeholder="Nama Supplier" readonly>
                      <input type="hidden" name="IDSupplier" id="IDSupplier" class="form-control" placeholder="No Retur" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                      <label>No Pembelian</label>
                      <select name="IDFB" id="IDFB" data-placeholder="Pilih No Invoice Pembelian" class="select2 form-control">
                          <option value="">Pilih No Invoice Pembelian</option>
                          <?php foreach($data_pembelian as $invs) { ?>
                          <option value="<?php echo $invs->IDFB ?>"><?php echo $invs->Nomor ?> - <?php echo $invs->Nama ?></option>
                          <?php } ?>
                      </select>
                    </td>
                    <td>
                      <label>Status PPN</label>
                      <input type="text" name="Status_ppn" id="Status_ppn" readonly>
                    </td>
                </tr>
            </table>
            <!--<br>-->

            <div class="container" style="width: 100%; margin-bottom: 20px;">
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                </div> 
                <div class="container" style="width: 100%">
                    <label style="color: red; font-weight: bold; font-style: italic;">*Kosongkan kolom Qty retur jika barang tidak dilakukan retur.</label>
                    <br>
                    <table class="table table-bordered" id="table-barang" style="font-size: 12px; margin-top: 10px; margin-bottom: 10px;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th style="width: 25%">Nama Barang</th>
                                <th style="width: 10%">Qty Jual</th>
                                <th style="width: 10%">Qty Retur</th>
                                <th style="width: 15%">Harga</th>
                                <th style="width: 15%">Satuan</th>
                                <th style="width: 5%">PPN</th>
                                <th style="width: 15%">Subtotal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="list-barang">

                        </tbody>
                    </table>
                    <table style="width: 100%; margin-top:10px;">
                        <tr>
                            <td style="width: 50%; vertical-align: top;">
                                <label for="Keterangan">Keterangan</label>
                                    <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"></textarea>
                            </td>
                            <td style="padding-left: 10px; vertical-align: top;">
                                <label style="float: left;" for="Total_qty">Total Qty</label>
                                <input type="text" style="width: 70%" id="Total_qty" name="Total_qty" class="form-control text-right" placeholder="Total Qty" readonly>
                                <br> <br> <br>
                                <div>
                                    <label style="float: left;" id="juduldpp">DPP</label>
                                    <input type="text" style="width: 70%" id="DPP" name="DPP" class="form-control text-right total-add" placeholder="DPP" readonly>
                                    <br><br><br>
                                    <?php ?>
                                    <label style="float: left;" id="judulppn">PPN</label>
                                    <input type="text" style="width: 70%" id="PPN" name="PPN" class="form-control text-right total-add" placeholder="PPN" readonly>
                                    <br><br><br>
                                </div>
                                <label style="float: left;" for="Grand_total">Total Retur</label>
                                <input type="text" style="width: 70%" id="Grand_total" name="Grand_total" class="form-control text-right" placeholder="Total Retur" readonly>
                            </td>
                        </tr>
                    </table>
                    
                    <button class="btn btn-primary" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                    <div class="btn col-16">
                        <span> <a style="color: white; float: right;" href="{{ route('ReturPembelian.index') }}">Kembali</a></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("ReturPembelian.index") }}';
    var urlInsert           = '{{ route("ReturPembelian.store") }}';
    var urlPembelian       = '{{ route("ReturPembelian.pembelian") }}';
    var urlPembelianDetail       = '{{ route("ReturPembelian.pembelian_detail") }}';
    var url                 = '{{ url("ReturPembelian") }}';
    
    function ambilnomor()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR RJ');
            $.ajax({
                    url : "{{url('ambilnomorrb')}}",
                    type: "GET",
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU RJ');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/returpembelian/returpembelian_create.js') }}"></script>
<script>
    
</script>
@endsection