<!-- ===========Create By Dedy 25-12-2019=============== -->
@extends('layouts.app')   
@section('content')

<?php
use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ReturPembelian')}}">Data Retur Pembelian</a>
                <i class="fa fa-angle-right"></i>
                <a href="">Detail Retur Pembelian</a>
        </h2>
    </div>
    <br>
    <div class="banner">
      <table class="table cell-border" width="100%">
        <thead style="display: none;">
        </thead>
        <tbody>
          <tr>
            <td style="background-color: #ffffff;">Tanggal</td>
            <td style="background-color: #ffffff;"><?php echo date('d M Y', strtotime($data->Tanggal)) ?></td>
          </tr>
          <tr>
            <td width="35%" style="background-color: #e5eff0;">Nomor</td>
            <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
          </tr>
          <tr>
            <td  style="background-color: #ffffff;">Nomor Invoice Pembelian</td>
            <td  style="background-color: #ffffff;"><?php echo $data->Nomor_inv ?></td>
          </tr>
          <tr>
            <td style="background-color: #e5eff0;">Supplier</td>
            <td style="background-color: #e5eff0;"><?php echo $data->Nama ?></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;">Tanggal Invoice Pembelian</td>
            <td style="background-color: #ffffff;"><?php echo date('d M Y', strtotime($data->Tanggal_fb)) ?></td>
          </tr>
          <tr>
            <td  style="background-color: #e5eff0;">Discount</td>
            <td  style="background-color: #e5eff0;"><?php echo $data->Discount ?></td>
          </tr>
          <tr>
            <td style="background-color: #ffffff;">Status PPN</td>
            <td style="background-color: #ffffff;"><?php echo $data->Status_ppn ?></td>
          </tr>
          <tr>
            <td  style="background-color: #e5eff0;">Keterangan</td>
            <td  style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
          </tr>
          <!-- <tr>
            <td style="background-color: #ffffff;">Status</td>
            <td style="background-color: #ffffff;"><?php echo $data->Batal ?></td>
          </tr> -->
          <tr>
            <td  style="background-color: #ffffff;">Total Qty</td>
            <td  style="background-color: #ffffff;"><?php echo $data->Total_qty_yard ?></td>
          </tr>
        </tbody>
        <tfoot></tfoot>
      </table>
    </div>
    <br><br><br>
    <div class="banner">
      <table id="table2" class="table cell-border" width="100%" style="font-size: 12px;">
        <thead style="background-color: #254283; color: white;">
          <tr>
              <th>No</th>
              <th>Nama Barang</th>
              <th>Qty</th>
              <th>Harga</th>
              <th>Satuan</th>
              <th>PPN % </th>
              <th>Total</th>
          </tr>
        </thead>
        <tbody style="border: 1px; border-collapse: collapse">
          @foreach ($datadetail as $key => $item)
              <tr>
                  <td> {{ ++$key }} </td>
                  <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                  <td style="text-align: center;"> {{ $item->Qty_yard }} </td>
                  <td style="text-align: right;"> {{ $data->Kode . ' ' . AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                  <td style="text-align: center;"> {{ $item->Satuan }} </td>
                  <td style="text-align: center;"> {{ ($data->Status_ppn == 'include') ? 0 : 10 }} </td>
                  <td style="text-align: right;"> {{ $data->Kode . ' ' . AppHelper::NumberFormat($item->Subtotal, $angkakoma) }} </td>
              </tr>
          @endforeach
      </tbody>
      </table>
    </div> 
    <div class="banner text-center">
      <div class="btn col-11 hvr-icon-back">
        <span> <a style="color: white;" href="{{url('ReturPembelian')}}" name="simpan">&nbsp;&nbsp;&nbsp;Kembali</a></span>
      </div>
      <div class="btn btn-primary hvr-icon-spin">              
        <span><a style="color: white;" href="{{url('ReturPembelian/print/')}}/<?php echo $data->IDRB ?>">Print Data&nbsp;&nbsp;&nbsp;</a></span>
      </div>
    </div>      
    <br><br><br>
</div>
<script type="text/javascript">
  $(document).ready(function() {
        $('#table2').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });            
    })
</script>
@endsection


