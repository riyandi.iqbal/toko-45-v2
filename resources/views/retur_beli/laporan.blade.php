<!-- ===========Create By Dedy 24-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('ReturPembelian')}}">Data Retur Pembelian</a>
        </h2>
    </div>
    <br>
<?php $hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)); ?>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Data Retur Pembelian</span>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                    <div class="form_input">
                        <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
                    </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
                </div>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select name="jenispencarian" id="jenispencarian" data-placeholder="No Retur Pembelian" style="width: 100%!important" class="form-control" tabindex="13">
                    <option value="Nomor">No RB</option>
                    <option value="Nama">Supplier</option>
                    <option value="IDFB">No Invoice</option>
                    <option value="Nama_Barang">Nama Barang</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
    <button onclick="exlin()" class="btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;EXCEL</button>
    <br><br>
        <table id="tabel2" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
                                <tr>
                                    <th>No</th>
                                    <th>No RB</th>
                                    <th>Tanggal</th>
                                    <th>Supplier</th>
                                    <th>Qty</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody id="previewdata">
                                <?php if (empty($retur)) {
                                    ?>
                                <?php
                                    } else {
                                        $i = 1;
                                        foreach ($retur as $data) {
                                            ?>
                                <tr class="odd gradeA">
                                    <td class="center">
                                        <center><?php echo $i; ?></center>
                                    </td>
                                    <td><?php echo $data->Nomor; ?></td>
                                    <td><?php echo date("d-m-Y", strtotime($data->Tanggal)); ?></td>
                                    <td><?php echo $data->Nama; ?></td>
                                    <td><?php echo $data->Total_qty_yard; ?></td>
                                    <td><?php echo $data->Keterangan; ?></td>
                                    <td>
                                        <center><?php echo $data->Batal; ?></center>
                                    </td>
                                    
                                </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                ?>

                            </tbody>
                        </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tabel2').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });
    })

    var PM = [];
    function caridata()
    {
        var jenis       = $('#jenispencarian').val();
        var keyword     = $('#keyword').val();
        var date_from   = $('#date_from').val();
        var date_until  = $('#date_until').val();
            $.ajax({
                  url : "{{url('ReturPembelian/pencarian')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td style='text-align:right;'>"+PM[i].Saldo_yard+"</td>"+
                      "<td>"+PM[i].Keterangan+"</td>"+
                      "<td>"+PM[i].Batal+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }

                     }
                }
            });
    }
    function exlin()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

       window.location.replace("{{url('LaporanReturBeli/toexcel/?date1=')}}"+date1+'&date2='+date2+'&jenis='+jenis+'&keyword='+keyword);
    }

</script>
@endsection