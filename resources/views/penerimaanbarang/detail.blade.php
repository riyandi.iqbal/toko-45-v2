@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('PenerimaanBarang')}}">Data Penerimaan Barang</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Penerimaan Barang - {{ $penerimaan_barang->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <thead style="display: none;">
                    </thead>
                    <tbody>
                        <tr>
                            <td style="background-color: #ffffff;">Tanggal</td>
                            <td style="background-color: #ffffff;"><?php echo date('d M Y', strtotime($penerimaan_barang->Tanggal)) ?></td>
                        </tr>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0;">Nomor Penerimaan Barang</td>
                            <td width="65%" style="background-color: #e5eff0;"><?php echo $penerimaan_barang->Nomor ?></td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff;">Nomor PO</td>
                            <td style="background-color: #ffffff;"><?php echo $penerimaan_barang->Nomor_po ?></td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0;">Supplier</td>
                            <td style="background-color: #e5eff0;"><?php echo $penerimaan_barang->Nama ?></td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff;">Total Qty</td>
                            <td style="background-color: #ffffff;"><?php echo $penerimaan_barang->Total_qty_yard ?></td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0;">Keterangan</td>
                            <td style="background-color: #e5eff0;"><?php echo $penerimaan_barang->Keterangan ?></td>
                        </tr>
                    </tbody>
                    <tfoot></tfoot>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($penerimaan_barang_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty_yard }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('PenerimaanBarang.index') }}" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('PenerimaanBarang.print', $penerimaan_barang->IDTBS) }}" target="__blank">Print Data</a></span>
                    </div>
                    <div class="btn col-1">
                      <span><a style="color: white;" href="{{ route('PenerimaanBarang.show', $penerimaan_barang->IDTBS) }}">Edit Data</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection