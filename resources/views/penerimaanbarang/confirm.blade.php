@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('suratjalan')}}">Data Surat Jalan</a>
              <i class="fa fa-angle-right"></i>
              <a>Konfirmasi Surat Jalan - {{ $surat_jalan->nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <input type="hidden" value="{{ $surat_jalan->idsjc }}" id="idsjc" name="idsjc">
    <div class="banner">
        <div class="widget_content">
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($surat_jalan_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->qty }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <h2 style="font-size: 15px;">Sisa Barang</h2>
            <hr>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Satuan</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($sales_order_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Saldo_qty }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content" style="text-align: center;">
                <input type="radio" name="Status_so" id="SOHangus" value="SOHangus"> <label for="SOHangus"> SO Hangus </label>
                <input type="radio" name="Status_so" id="SOLanjut" value="SOLanjut"> <label for="SOLanjut"> SO Lanjut </label>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <a style="color: white;" id="btn-save">
                        <div class="btn col-3">
                            <span>Simpan</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script type="text/javascript">
    var url                 = '{{ url("SuratJalan") }}';
    var urlSetSO           = '{{ route("SuratJalan.set_so") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/suratjalan/suratjalan_confirm.js') }}"></script>
@endsection