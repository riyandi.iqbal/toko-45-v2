@extends('layouts.app')
@section('content')
<?php 
    use App\Helpers\AppHelper;
?>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('PenerimaanBarang.index')}}">Data Penerimaan Barang</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Edit Penerimaan Barang</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Penerimaan Barang</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive">
                <tr>
                    <td style="width: 35%;">
                        <input type="hidden" value="{{ $penerimaan_barang->IDTBS }}" name="IDTBS" id="IDTBS">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control form-white datepicker" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date_format(date_create($penerimaan_barang->Tanggal), 'd/m/Y') }}">
                        <br>
                        <label for="Nomor">Nomor Penerimaan Barang</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Penerimaan Barang" readonly value="{{ $penerimaan_barang->Nomor }}">
                        <br>
                        <label for="IDPO">Nomor Pesanan Pembelian</label>
                        <select name="IDPO" id="IDPO" class="form-control select2" style="width: 100%;">
                            <option value="{{ $penerimaan_barang->IDPO }}"> {{ $penerimaan_barang->Nomor_po }} </option>
                        </select>
                        <input type="hidden" name="Total_harga" id="Total_harga">
                    </td>
                    <td style="width: 35%;">
                        <label for="IDSupplier">Supplier</label><br>
                        <input type="text" name="NamaSupplier" id="NamaSupplier" class="form-control" readonly value="{{ $penerimaan_barang->Nama }}">
                        <input type="hidden" name="IDSupplier" id="IDSupplier" value="{{ $penerimaan_barang->IDSupplier }}">
                        <br>
                        <label for="Total_qty">Total</label>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly value="{{ $penerimaan_barang->Total_qty_yard }}">
                        <br>
                        <label for="IDGudang">Gudang</label>
                        <select name="IDGudang" id="IDGudang" class="form-control select2">
                            <option value="">-- Pilih Gudang --</option>
                            @foreach ($gudang as $item)
                                <option value="{{ $item->IDGudang }}" {{ $penerimaan_barang->IDGudang == $item->IDGudang ? 'selected' : '' }} > {{ $item->Nama_Gudang }} </option>
                            @endforeach
                        </select>
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5">{{ $penerimaan_barang->keterangan }}</textarea>
                    </td>
                </tr>
            </table>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 20px;">
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                </div> 
                <div class="container" style="width: 100%">
                    <br>
                    <table class="table table-bordered" id="table-barang" style="font-size: 12px; margin-top: 10px;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th>No</th>
                                <th style="width: 45%">Nama Barang</th>
                                <th style="width: 10%">Qty Pesan</th>
                                <th style="width: 10%">Qty Terima</th>
                                <th style="width: 15%">Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($penerimaan_barang_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Nama_Barang }} </td>
                                    <td style="text-align: center;"> {{ $item->Qty }} </td>
                                    <td> 
                                        <input type="hidden" name="IDPODetail[]" value="{{ $item->IDPODetail }}">
                                        <input type="hidden" name="IDBarang[]" value="{{ $item->IDBarang }}">
                                        <input type="hidden" name="Qty[]" value="{{ $item->Qty }}">
                                        <input type="hidden" name="Harga[]" class="harga" value="{{ $item->Harga }}">
                                        <input type="hidden" name="Sub_total[]" class="subtotal" value="{{ $item->Saldo }}">
                                        <input type="hidden" name="IDSatuan[]" value="{{ $item->IDSatuan }}">
                                        
                                        <input type="text" name="Qty_kirim[]" value="{{ $item->penerimaan_detail ? $item->penerimaan_detail->Qty_yard : 0 }}" class="qty form-control"> <br>
                                        <span class="valid-qty"> </span>
                                    </td>
                                    <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    <button class="btn btn-primary" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;" disabled>Simpan</button>
                    <div class="btn col-16">
                        <span> <a style="color: white; float: right;" href="{{ route('PenerimaanBarang.index') }}">Kembali</a></span>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("PenerimaanBarang.index") }}';
    var urlInsert           = '{{ route("PenerimaanBarang.update_data") }}';
    var urlPurchaseOrder       = '{{ route("PenerimaanBarang.purchase_order") }}';
    var urlPurchaseOrderDetail       = '{{ route("PenerimaanBarang.purchase_order_detail") }}';
    var url                 = '{{ url("PenerimaanBarang") }}';
    var Status_pakai        = {{ $penerimaan_barang->Status_pakai == 1 ? false : true }};
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/penerimaanbarang/penerimaanbarang_update.js') }}"></script>
<script>
    
</script>
@endsection