<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Penerimaan Barang</title>
</head>
<style>
    address {
        font-style: normal;
    }
    h2, h3 {
        margin: 0px;
    }
    
    .table-print th {
        padding: .4rem;
        font-weight : bold;
        vertical-align: top;
        border-top: 1px solid #0a0a0a;
        border-bottom: 1px solid #0a0a0a;
        border-left: 1px solid #0a0a0a;
        border-right: 1px solid #0a0a0a;
    }

    .table-print td:first-child {
        border-left: 1px solid #0a0a0a;
    }

    .table-print td:last-child {
        border-right: 1px solid #0a0a0a;
    }

    .table-print tr:last-child {
        border-bottom: 1px solid #0a0a0a;
    }

    .table-print { padding: .4rem; border-collapse: collapse }
    .table-print td { padding: .4rem }
</style>
<?php 
    use App\Helpers\AppHelper;
?>
<body>
    <table style="width: 100%;">
        <tr>
            <td colspan="2"> 
                <table>
                    <tr>
                            <div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
                    </tr>                
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align: center;">
                <h2> Penerimaan Barang </h2>
            </td>
        </tr>
        <tr>
            <td style="width: 50%; vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 30%">Nomor</td>
                        <td style="width: 1%"> : </td>
                        <td> {{ $penerimaan_barang->Nomor }} </td>
                    </tr>
                    <tr>
                        <td style="width: 30%">Nomor Pesanan Pembelian</td>
                        <td style="width: 1%"> : </td>
                        <td> {{ $penerimaan_barang->Nomor_po }} </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td> : </td>
                        <td> {{ AppHelper::DateIndo($penerimaan_barang->tanggal) }} </td>
                    </tr>
                </table>
            </td>
            <td style="vertical-align: top;">
                <table style="width: 100%;">
                    <tr>
                        <td>Supplier</td>
                        <td> : </td>
                        <td> {{ $penerimaan_barang->Nama }} </td>
                    </tr>
                    <tr>
                        <td>Alamat</td>
                        <td> : </td>
                        <td> {{ $penerimaan_barang->Alamat }} </td>
                    </tr>
                    <tr>
                        <td>Telp</td>
                        <td> : </td>
                        <td> {{ $penerimaan_barang->No_Telpon }} </td>
                    </tr>
                </table>
            </td>
        </tr>
        <td>
            <tr>
                <td colspan="2">
                    <table style="width: 100%" class="table-print">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($penerimaan_barang_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Nama_Barang }} </td>
                                    <td style="text-align: center;"> {{ $item->Qty_meter }} </td>
                                    <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </td>
            </tr>
        </td>
    </table>
</body>
</html>

<script>
    window.onload = print();
</script>