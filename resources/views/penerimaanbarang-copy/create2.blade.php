<!-- ===========Create By Dedy 19-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php

// if ($last_number->curr_number == null) {
//   $number = 1;
// } else {
//   $number = $last_number->curr_number + 1;
// }

// $kodemax = str_pad($number, 4, "0", STR_PAD_LEFT);
    
// if ($last_number2->curr_number == null) {
//   $number2 = 1;
// } else {
//   $number2 = $last_number2->curr_number + 1;
// }

// $kodemax2 = str_pad($number2, 4, "0", STR_PAD_LEFT);
    
// $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");

// $code_booking = '0-'.$kodemax.'/'.'PB'.'/'.$array_bulan[date('n')].'/'.date('y');
    
// $code_booking2 = '1-'.$kodemax2.'/'.'PB'.'/'.$array_bulan[date('n')].'/'.date('y');
?>


<div class="main-grid">
	<div class="banner">
		<h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PenerimaanBarang')}}">Data Penerimaan Barang</a>>
                <i class="fa fa-angle-right"></i>
                Tambah Penerimaan Barang
        </h2>
	</div><!-- banner -->
<form class="form-horizontal"  action="{{url('PenerimaanBarang/storenew')}}" method="post">
  {{ csrf_field() }}
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Tambah Data Penerimaan Barang</span>
    </div>
    <div class="banner">
            <table class="table table-responsive">
            <tr>
                <td class="col-md-4">
                    <input type="hidden" name="no_pocari" id="no_pocari" value="<?php echo $urlid->Nomor ?>">
                    <label>Tanggal</label>
                        <input type="date" class="form-control input-date-padding date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    
                    <label>No PO</label>
                        <select name="no_po" id="no_po" onchange="pindahhalaman()" required style=" width:100%;" class="form-control" tabindex="13">
                            <option value="<?php echo $urlid->IDPO ?>"><?php echo $urlid->Nomor ?> - <?php echo $urlid->Nama ?></option>
                            <option value=""></option>
                            <?php foreach ($nopo as $po) : ?>
                            <option value="<?php echo $po->IDPO ?>">
                            <?php echo $po->Nomor ?> - <?php echo $po->Nama ?></option>
                            <?php endforeach ?>
                        </select>
                    
                    <label>Total Qty</label>
                    <input type="text" class="form-control" name="qtytotal" id="qtytotal" readonly>
                </td>
                <td class="col-md-4">
                    <label>Supplier</label>
                    <input type="text" name="namasupplier" class="form-control" id="namasupplier" readonly="">
                    <input type="hidden" name="id_supplier" id="id_supplier">
                    <input type="hidden" name="kurs" id="kurs">
                    
                    <label>Nomor PB</label>
                    <input type="text" class="form-control" name="no_bo" id="no_bo" readonly>
                    
                    <!-- <label>Grand Total</label> -->
                    <input type="hidden" class="form-control" name="grandtotal" id="grandtotal" readonly>                    
                    <label>Gudang Penerima</label>
                    <select data-placeholder="Gudang Penerima" class="chosen-select" id="gudang" name="gudang">
                      <?php foreach ($gudang as $gd) {
                            echo "<option value='$gd->IDGudang'>$gd->Nama_Gudang</option>";
                        }
                        ?>
                    </select>
                </td>
                <td class="col-md-4">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="keterangan" id="keterangan" rows="7"></textarea>
                </td>
            </tr>
        </table>
    </div>
    
    <div class="banner">
        <table id="tabelfsdfsf" class="display" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
            <tr>
                <th style="width: 5%">No</th>
                <th style="width: 50%">Barang</th>
                <th style="width: 10%">Qty PO</th>
                <th style="width: 10%">Qty Terima</th>
                <!-- <th style="width: 20%">Harga</th> -->
                <th style="width: 10%">Satuan</th>
                <!-- <th style="width: 15%">Subtotal</th> -->
            </tr>
            </thead>
            <tbody id="tabel_detail_tampungan">
            </tbody>
        </table>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content px-2 text-center">
            <div class="py-4 mx-2">
                <span><input type="button" class="btn btn-warning" style="cursor: no-drop;" id="print_" name="print_" onclick="print_cek()" value="Print Data"></span>
                <span><button onclick="val_submit()" type="submit"  class="btn btn-primary">Simpan Data</button></span>
            </div>
    </div>
    <br><br><br>
</form>
        
</div><!-- main grid -->



<script>
document.getElementById("print_").disabled = true;
</script>

<script type="text/javascript">
            $(document).ready(function() {
                //====================================datatables
                table = $('#tabelfsdfsf').DataTable({ 
                    "searching": false,
                    "lengthChange": false
                });

                var selectedOption  = $("#no_po").val();
                var karakter1       =  selectedOption.slice(1, 2);
                console.log(karakter1);
                if (karakter1=='0-'){
                    selectedOption = $("#no_bo").val('<?php echo $code_booking ?>');

                }else{
                    selectedOption = $("#no_bo").val('<?php echo $code_booking2 ?>');
                }

                caridatapo();
                // hitungulang();
                ambilnopb();
            });

function caridatapo()
{
    $("#modal-loading").fadeIn();
  console.log('MASUK SINI DULU');
  console.log('===========Mulai Cari Data============')
  ambilnopb();
                // var selectedOption  = $("#no_pocari").val();
                // var karakter1       =  selectedOption.slice(0, 2);
                // console.log(karakter1);
                // if (karakter1=='0-'){
                //     selectedOption = $("#no_bo").val('<?php echo $code_booking ?>');

                // }else{
                //     selectedOption = $("#no_bo").val('<?php echo $code_booking2 ?>');
                // }
  var no_po = $('#no_pocari').val();
  console.log(no_po);
    $.ajax({
            url: "{{url('PenerimaanBarang/tampildatapo')}}",
            data: {"_token": "{{ csrf_token() }}",data:no_po},
            type: "POST",
            dataType: "json",
            success: function (data) {
                $("#modal-loading").fadeOut();
              console.log('GET IT')
              console.log(data);


              $("#tabel_detail_tampungan").html("");
              
                i=0;
                var totalharga = 0;
                var totalqty = 0;


                $('.row_tampungan').remove();
                for(i = 0; i<data.length;i++) {
                  PM[i] = data[i];
                  console.log(PM[i].Nama);
                  $('#namasupplier').val(PM[i].Nama);
                  $('#id_supplier').val(PM[i].IDSupplier);
                  $('#kurs').val(PM[i].IDMataUang);
                  var subtotal = PM[i].Qty * PM[i].Harga;
                  var value =
                  "<tr class='row_tampungan' id='row_tampungan"+i+"'>"+
                  "<td>"+(i+1)+"</td>"+
                  "<td><input style='width:100%' type='text' class='form-control' name='Barang_array[]' value='"+PM[i].Nama_Barang+"' readonly></td>"+
                  "<td><input type='text' class='form-control' name='qty_array[]' value='"+PM[i].Qty+"' id='qty_array"+i+"' readonly></td>"+
                  "<td><input type='text' class='form-control qty' name='qty_terima_array[]' id='qty_terima_array"+i+"' value='"+PM[i].Qty_terima+"' onclick='hitungulang(this.value, "+i+")'  onkeyup='hitungulang(this.value, "+i+")' autocomplete='off'></td>"+
                  "<td><input type='text' class='form-control' name='satuan_array[]' id='satuan_array"+i+"' value='"+PM[i].Satuan+"' readonly> <input type='hidden' class='form-control' name='idsatuan_array[]' id='idsatuan_array"+i+"' value='"+PM[i].IDSatuan+"' readonly></td>"+
                  "<td><input type='hidden' class='form-control price' name='harga_array[]' id='harga_array"+i+"' value='"+PM[i].Harga+"' readonly></td>"+
                  
                  "<td><input type='hidden' class='form-control' name='total_array[]' id='total_array"+i+"' readonly></td>"
                  "</tr>";
                  $("#tabel_detail_tampungan").append(value);

                  totalharga += parseInt(subtotal);
                  totalqty += parseInt(PM[i].Qty_terima);

                  $('#grandtotal').val(totalharga);
                  $('#qtytotal').val(totalqty);

                }

            },
            error: function(data,msg,response,status){
              console.log("Failure");
              $("#modal-loading").fadeOut();
            }

    });
}


  var val_qty;
  var array_qty2 = new Array(100);
  for (var q = 0; q < array_qty2.length; q++) {
    array_qty2[q] = new Array(100);
  }

  var val_gt;
  var array_gt = new Array(100);
  for (var z = 0; z < array_gt.length; z++) {
    array_gt[z] = new Array(100);
  }

function hitungulang(nilai, urutan)
{
    $("#modal-loading").fadeIn();
    $('#qty_terima_array'+urutan).val(nilai.replace(/[^\d]/,''));
    $('#total_array'+urutan).val(nilai.replace(/[^\d]/,'')*$('#harga_array'+urutan).val());

    if($('#qty_terima_array'+urutan).val()<=0){
      alert('QTY Terima Tidak Boleh kurang dari 0');
      $('#qty_terima_array'+urutan).focus();
    }

    b = $('#harga_array'+urutan).val();
    console.log(b);
    total_qty = 0;
    a = 0;
    array_qty2[urutan][0]=urutan;
    array_qty2[urutan][1]=parseInt(nilai);

    console.log(urutan);

    // for (var y = 0; y < array_qty2.length; y++) {
    //   if(array_qty2[y][1]!=null){
    //     total_qty = parseInt(total_qty) + parseInt(array_qty2[y][1]);
    //     a = total_qty * parseInt(b);
    //   }   
    // }

    $('.qty').each(function() {
        var row = $(this).closest('tr');
        
        var thisQty = ( ! isNaN($(this).val())) ? parseFloat($(this).val()) : 0 ;
        var thisPrice    = ((row.find('.price').val().length > 0)) ? parseFloat(row.find('.price').val().replace(/\./g,'').replace(',', '.')) : 0;

        var thisTotal = thisQty * thisPrice;

        total_qty += thisQty;
        a += thisTotal;
    });

    $('#qtytotal').val(total_qty);
    $('#grandtotal').val(a);

    console.log('HITUNG SUBTOTAL');
    var r = $('#harga_array'+urutan).val();
    console.log(r);
    var s = $('#qty_terima_array'+urutan).val();
    console.log(s);
    var t = 0;
    t = s*r;
    console.log(t);

    $('#total_array'+urutan).val(t);
    $("#modal-loading").fadeOut();

}

  

  function hitungulang3(nilai, urutan)
  {
    $("#modal-loading").fadeIn();
    $('#total_array'+urutan).val(nilai.replace(/[^\d]/,''));
    console.log("CARI Grand Total");
    total_gt = 0;
    array_gt[urutan][0]=urutan;
    array_gt[urutan][1]=parseInt(nilai);

    for (var y = 0; y < array_gt.length; y++) {
      if(array_gt[y][1]!=null){
        total_gt = parseInt(total_gt) + parseInt(array_gt[y][1]);
      }
    }

    $('#grandtotal').val(total_gt);
    $("#modal-loading").fadeOut();
  
  }
    
function Penerimaan() {
     this.IDTBS;
     this.Barang;
     this.Qty;
     this.Harga;     
 }

 var PM = new Array();
 var Total = new Array();
 index = 0;  

function pindahhalaman()
{
	var id = $("#no_po").val();
	console.log(id);
	window.location.href = "{{url('PenerimaanBarang/check_PO')}}/id="+id+"";
}

//=============================from libjs
  var base_url = window.location.pathname.split('/');
 document.getElementById("print_").disabled = true;

 function Penerimaan() {
     this.IDTBS;
     this.Barcode;
     this.Corak;
     this.Warna;
     this.Merk;
     this.Qty_yard;
     this.Qty_meter;
     this.Grade;
     this.Satuan;
     this.NoPO;
     this.NoSO;
     this.Party;
     this.Indent;
     this.Lebar;
     this.Tanggal;
     this.NoPB;
     this.ID_supplier;
     this.NoSJ;
     this.Keterangan;
     this.Total_yard;
     this.Total_meter;
     this.Harga;
     this.Total_harg;
 }

 var PM = new Array();
 var Total = new Array();
 index = 0;

 function save() {
    $("#modal-loading").fadeIn();
     $("#no_sjc").css('border', '');

     if ($("#no_sjc").val() == "") {
         $('#alert').html('<div class="pesan sukses">Data No SJC Tidak Boleh Kosong</div>');
         $("#no_sjc").css('border', '1px #C33 solid').focus();
     } else {
         $.post(baseUrl + 'PenerimaanBarang/store', {
                 "data": PM,
                 "Total": Total,
                 'id_supplier': $('#id_supplier').val(),
                 Keterangan: $('#keterangan').val(),
                 total_harga: $('#total_harga').val()
             })
             .done(function(res) {
                 console.log(res);
                 $("#modal-loading").fadeOut();
                 swal({
                          title: "Berhasil!",
                          text: "Data Berhasil Disimpan!",
                          icon: "success",
                      }).then(function() {
                          window.location = "../../PenerimaanBarang";
                      });

             });
     }
 }

 var PM = [];
 var temp = [];

 function renderJSON(data) {
     if (data != null) {
         if (PM == null)
             PM = [];
         console.log(data);
         $("#tabel-cart-asset").html("");
         Total_yard = 0;
         Total_meter = 0;
         for (i = 0; i < data.length; i++) {
             PM[i] = data[i];
             var value =
                 "<tr>" +
                 "<td>" + (i + 1) + "</td>" +
                 // "<td>" + (i + 1) + "</td>" +
                 "<td>" + PM[i].Barcode + "</td>" +
                 "<td>" + PM[i].Corak + "</td>" +
                 "<td>" + PM[i].Warna + "</td>" +
                 "<td>" + PM[i].Merk + "</td>" +
                 "<td class='text-center'>" + PM[i].Qty_yard + "</td>" +
                 "<td class='text-center'>" + PM[i].Qty_meter + "</td>" +
                 "<td>" + PM[i].Grade + "</td>" +
                 "<td>" + PM[i].Satuan + "</td>" +
                 // "<td>"+rupiah(PM[i].Harga)+"</td>"+
                 "</tr>";
             $("#tabel-cart-asset").append(value);
             Total_yard = parseFloat(Total_yard) + parseFloat(PM[i].Qty_yard);
             Total_meter = parseFloat(Total_meter) + parseFloat(PM[i].Qty_meter);
             Total_meter2 = Total_meter.toFixed(2)
         }

         $('#total_meter').val(Total_meter2);
         $('#total_yard').val(Total_yard);
     }
 }

 function deleteItem(i) {

     if (PM[i].id != '') {
         temp.push(PM[i]);
     }

     PM.splice(i, 1);
     renderJSON(PM);
 }

 function field() {
     var data = {
         "IDTBS": $("#IDBTS").val(),
         "Tanggal": $("#Tanggal").val(),
         "IDSupplier": $("#id_supplier").val(),
         "Keterangan": $("#Keterangan").val(),
         "Kurs":$('#kurs').val(),
     }
     return data;
 }

 function saveChange() {
    $("#modal-loading").fadeIn();
     var data = field();
     $.ajax({

         url: "../update_",
         type: "POST",
         data: {
             "data1": data,
             "data2": temp
         },

         success: function(msg, status) {
            $("#modal-loading").fadeOut();
              swal({
                          title: "Berhasil!",
                          text: "Data Berhasil Dirubah!",
                          icon: "success",
                      }).then(function() {
                          window.location = "../../PenerimaanBarang";
                      });
         },
         error: function(msg, status, data) {
            $("#modal-loading").fadeOut();
             alert("Failure " + msg + status + data);
         }

     });
 }

 function remove_data() {
     var row = $(this).parent().parent().parent();
     row.remove();
 }

 function print_cek() {
     window.open(baseUrl + "PenerimaanBarang/print_data/");
 }

 function print_cek_edit() {
     sjc = $('#IDBTS').val();
     window.open(baseUrl + "PenerimaanBarang/print_data_edit/" + sjc);
 }

 function val_submit()
 {
    $("#modal-loading").fadeIn();
    return true;
 }

 function ambilnopb() {
    $("#modal-loading").fadeIn();
	console.log('AMBIL DATA NOMOR PB');
		$.ajax({
				url : "{{url('ambilnomorpb')}}",
                type: "GET",
                data:{nomopo:$('#no_po').val()},
                dataType:'json',
                  success: function(data)
                  { 
					console.log('KODE BARU PB');
                    console.log(data);
					$('#no_bo').val(data);
					$("#modal-loading").fadeOut();
                  }
		});
 }
</script>
@endsection
