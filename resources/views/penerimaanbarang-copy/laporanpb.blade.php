<!-- ===========Create By Dedy 18-12-2019=============== -->
@extends('layouts.app')
@section('content')
<?php
    $hari_ini       = date("Y-m-d");
    $tgl_terakhir   = date('Y-m-t', strtotime($hari_ini));
?>

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PenerimaanBarang')}}">Penerimaan Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
            <span>Data Penerimaan Barang</span>
            </div>
            <br>
    <div class="banner container">
            
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                    <div class="form_input">
                        <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
                    </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
                </div>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select name="jenispencarian" id="jenispencarian" data-placeholder="No PO" style="width: 100%!important" class="form-control" tabindex="13">
                    <option value="Nomor">No PB</option>
                    <option value="Nomor_sj">Nomor PO</option>
                    <option value="Nama">Supplier</option>
                    <option value="Nama_Barang">Nama Barang</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>
            </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="banner">
            <div class="widget_content">
            <button onclick="exlin()" class="btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;EXCEL</button>
            <br><br>
                <table id="tablepb" class="display" width="100%" style="background-color: #254283; font-size: 12px;">
                    <thead style="color: #fff">
                    <tr>
                                <th>No</th>
                                <th>Tanggal</th>
                                <th>Nomor PB</th>
                                <th>Nomor PO</th>
                                <th>Nama Barang</th>                                
                                <th>Supplier</th>
                                <th>Harga Satuan</th>
                                <th>Qty</th>
                                <th>Satuan</th>
                            </tr>
                        </thead>
                        <tbody id="previewdata">
                            <?php if (!empty($PenerimaanBarang)) {
                                $i = 1;
                                foreach ($PenerimaanBarang as $data) {
                                    ?>
                            <tr class="odd gradeA">
                                <td>
                                    <center><?php echo $i; ?></center>
                                </td>
                                <td><?php echo date('d M Y', strtotime($data->Tanggal)); ?></td>
                                <td><?php echo $data->Nomor; ?></td>
                                <td><?php echo $data->Nomor_sj; ?></td>
                                <td><?php echo $data->nama_barang; ?></td>
                                <td><?php echo $data->Nama; ?></td>
                                <td><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php echo $data->harga; ?></td>
                                <td><?php echo $data->qty; ?></td>
                                <td><?php echo $data->satuan; ?></td>
                            </tr>
                            <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
            <br><br><br>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tablepb').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });
    })

    var PM  = [];
    function caridata()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

        $.ajax({
                  url : "{{url('Laporan_penerimaan_barang/laporan_pb')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",date1:date1, date2:date2, jenis:jenis, keyword:keyword},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=7 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];


                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+PM[i].Tanggal+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nomor_sj+"</td>"+
                      "<td>"+PM[i].nama_barang+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td>"+PM[i].harga_satuan+"</td>"+
                      "<td>"+PM[i].qtymeter+"</td>"+
                      "<td>"+PM[i].satuan+"</td>"+
                      "</tr>";
                      $("#previewdata").append(value);
                      }
                      
                     }
                }
            });
    }

    function exlin()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

       window.location.replace("{{url('Laporan_penerimaan_barang/toexcel/?date1=')}}"+date1+'&date2='+date2+'&jenis='+jenis+'&keyword='+keyword);
    }

</script>

@endsection
