<!-- ===========Create By Dedy 19-12-2019=============== -->

<style type = "text/css">
</style>
<div class="main-grid">
	<div class="banner">
		<div class="printkertas">
			<div class="headerprint">
				<button class="btn btn-primary" id="printgo" onclick="printgo()">Print</button>
				<a href="{{url('PenerimaanBarang')}}"><button class="btn btn-primary">Close</button></a>
			</div>
			<div class="bodyprint">
				<br>
				<table width="90%">
					<tr>
						<td>
							<div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
						</td>
					</tr>
				</table>
				<hr>
				<br>
				<table width="90%">
					<tr>
						<td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="7">Penerimaan Barang</td>
					</tr>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td>Nomor</td>
						<td>:</td>
						<td><?php echo $data->Nomor; ?></td>
						<td>&nbsp;</td>
						<td>Grand Total</td>
						<td>:</td>
						<td style="text-align: right;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php echo number_format($data->Total_harga, $coreset->Angkakoma,',','.'); ?></td>
					</tr>
					<tr>
						<td>Tanggal</td>
						<td>:</td>
						<td><?php echo date("d M Y", strtotime($data->Tanggal)); ?></td>
						<td>&nbsp;</td>
						<td>Total Qty</td>
						<td>:</td>
						<td style="text-align: right;"><?php echo $data->Total_qty_yard; ?></td>
					</tr>
				</table>
				<br>
				<table width="90%" class="table table-bordered">
					<thead>
						<th>No</th>
						<th>Nama Barang</th>
						<th>Qty</th>
						<th>Harga</th>
						<th>Satuan</th>
						<th>Subtotal</th>
					</thead>
					<tbody>
						<?php if(empty($pbdetail)){
	                      ?>
	                      <?php
	                    }else{
	                      $i=1;
	                      foreach ($pbdetail as $data2) {
	                      		$subtotal = $data2->Qty_meter*$data2->Harga;
	                        ?>
	                        <tr class="odd gradeA">
	                          <td class="text-center"><?php echo $i; ?></td>
	                          <td><?php echo $data2->Nama_Barang; ?></td>
	                          <td class="text-center"><?php echo number_format($data2->Qty_meter,0,',','.'); ?></td>
	                          <td style="text-align: right;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php echo number_format($data2->Harga,$coreset->Angkakoma,',','.'); ?></td>
	                          <td><?php echo $data2->Satuan; ?></td>
                     		  <td style="text-align: right;"><?php echo $data->Mata_uang ?>&nbsp;&nbsp;&nbsp;<?php echo number_format($subtotal,$coreset->Angkakoma,',','.'); ?></td>
	                        </tr>
	                        <?php
	                        $i++;
	                      }
	                    }
	                    ?>
					</tbody>
				</table>
				<br>
				<table width="90%">
					<tr>
						<td>Keterangan : </td>
						<td>&nbsp;</td>
						<td><?php echo $data->Keterangan; ?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
</div>
<script src="{{ url ('newtemp/js/jquery2.0.3.min.js') }}"></script>
<script type="text/javascript">
	function printgo()
	{
		$('.headerprint').hide();
		$('.main-menu').hide();
		$('.title-bar').hide();
		window.print();
		location.reload();
	}
</script>
