<!-- ===========Create By Dedy 18-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>

<?php
    $hari_ini       = date("Y-m-d");
    $tgl_terakhir   = date('Y-m-t', strtotime($hari_ini));
?>

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PenerimaanBarang')}}">Penerimaan Barang</a>
        </h2>
    </div>
    <br>

    <div class="banner container">
            <div class="form_grid_3">
            <a href="{{url('PenerimaanBarang/create')}}">
                <div class="btn btn-primary hvr-icon-float-away">
                    <span style="color: white;">Tambah&nbsp;&nbsp;&nbsp;</span>
                </div>
            </a>
            </div>
            <br>
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                    <div class="form_input">
                        <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
                    </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">Cari Berdasarkan</label>
                    <select data-placeholder="Cari Customer" class="chosen-select" name="field" id="field" required style="width: 100%">
                        <option value="">- Pilih -</option>
                        <option value="tbl_terima_barang_supplier.Nomor">Nomor</option>
                        <option value="tbl_supplier.Nama">Customer</option>
                        <option value="tbl_terima_barang_supplier.Batal">Status</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">Keyword</label>
                    <div id="pilihan">
                        <input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>
            </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="banner">
            <div class="widget_content">
                <table id="table-data" class="display" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
                    <thead style="color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nomor PB</th>
                            <th>Nomor Pesanan</th>
                            <th>Supplier</th>
                            <th>Total Qty Terima</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                        
                    <tfoot style="color: #fff">
                        <tr>
                            <th colspan="5" style="text-align: right;">
                                Total
                            </th>
                            <th style="text-align: center;"></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <br><br><br>
    </div>
</div>
<?php
if (isset($PenerimaanBarang)){ foreach($PenerimaanBarang as $data){ ?>
            <div id="modalBatal<?php echo $data->IDTBS?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Peringatan!</h4>
                  </div>
                  <div class="modal-body">
                    <p>Yakin Akan Menghapus Data <?php echo $data->IDTBS ?> ?!</p>
                    <input type="hidden" name="idcari" value="<?php echo $data->IDTBS?>">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <div class="btn hvr-icon-shrink col-10">
                        <a style="color: white;" href="PenerimaanBarang/hapus_data/<?php echo $data->IDTBS ?>"><span>Hapus</span></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
    <?php } } ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tablepb').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false
        });
         @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif
    })
    var PM = [];
    function caridata()
    {
        var jenis       = $('#jenispencarian').val();
        var keyword     = $('#keyword').val();
        var date_from   = $('#date_from').val();
        var date_until  = $('#date_until').val();
            $.ajax({
                  url : "{{url('PenerimaanBarang/indexcari')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",jenis:jenis, keyword:keyword, date_from:date_from, date_until:date_until},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=8 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];


                      if(PM[i].Batal=='Aktif'){
                         var $status  = "<span><a href='PenerimaanBarang/show/"+PM[i].IDTBS+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='PenerimaanBarang/printnewpb/"+PM[i].IDTBS+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>&nbsp;<a href='PenerimaanBarang/cekallpostedit/"+PM[i].IDTBS+"'title='Edit Data'>&nbsp;<i class='fa fa-pencil fa-lg'></i></a></span>&nbsp;<span><a data-toggle='modal' data-target='#modalBatal"+PM[i].IDTBS+"' title='Batalkan.?'><i class='fa fa-times-circle fa-lg' style='color: red'></i></a></span>";
                      }else{
                         var $status  = "<span><a href='PenerimaanBarang/show/"+PM[i].IDTBS+"' title='Detail'><i class='fa fa-search fa-lg' style='color: green'></i></a></span>&nbsp;<span><a href='PenerimaanBarang/printnewpb/"+PM[i].IDTBS+"' title='Print'><i class='fa fa-print fa-lg' style='color: grey'></i></a></span>";
                      }

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+

                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nomor_sj+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td style='text-align:right;'>"+PM[i].Batal+"</td>"+
                      "<td style='text-align:center;'>"+$status+"</td>"
                      "</tr>";
                      $("#previewdata").append(value);
                      }

                     }
                }
            });
    }
</script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("PenerimaanBarang.datatable") }}';
    var url = '{{ url("PenerimaanBarang") }}';
    var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/penerimaanbarang/penerimaanbarang_index.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('alertakses'))
            swal("Perhatian", "{{ session('alertakses') }}", "warning");
        @endif
    })
</script>
@endsection
