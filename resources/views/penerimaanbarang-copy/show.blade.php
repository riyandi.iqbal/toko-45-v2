<!-- ===========Create By Dedy 18-12-2019=============== -->
@extends('layouts.app')
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PenerimaanBarang')}}">Penerimaan Barang</a>
                <i class="fa fa-angle-right"></i>
                <a>Detail Penerimaan Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner">
    <div class="widget_content">
                    <div class="form_container left_label">
    <table class="table cell-border" width="100%" style="font-size: 12px;">
            <thead style="display: none;">
            </thead>
            <tbody>
                <tr>
                    <td style="background-color: #ffffff;">Tanggal</td>
                    <td style="background-color: #ffffff;"><?php echo date('d M Y', strtotime($data->Tanggal)) ?></td>
                </tr>
                <tr>
                    <td width="35%" style="background-color: #e5eff0;">Nomor Penerimaan Barang</td>
                    <td width="65%" style="background-color: #e5eff0;"><?php echo $data->Nomor ?></td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff;">Nomor PO</td>
                    <td style="background-color: #ffffff;"><?php echo $data->Nomor_sj ?></td>
                </tr>
                <tr>
                    <td style="background-color: #e5eff0;">Supplier</td>
                    <td style="background-color: #e5eff0;"><?php echo $data->Nama ?></td>
                </tr>
                <tr>
                    <td style="background-color: #ffffff;">Total Qty</td>
                    <td style="background-color: #ffffff;"><?php echo $data->Total_qty_yard ?></td>
                </tr>
                <!-- <tr>
                    <td style="background-color: #e5eff0;">Status</td>
                    <td style="background-color: #e5eff0;"><?php echo $data->Batal ?></td>
                </tr> -->
                <tr>
                    <td style="background-color: #e5eff0;">Keterangan</td>
                    <td style="background-color: #e5eff0;"><?php echo $data->Keterangan ?></td>
                </tr>
            </tbody>
            <tfoot></tfoot>
        </table>
        </div></div>
    </div>
    <br>
    <div class="banner">
        <table id="tblshowpb" class="table cell-border" width="100%" style="font-size: 12px;">
            <thead style="background-color: #254283; color: white;">
                <tr>
                    <th class="center" style="width: 40px">No</th>
                    <th>Nama Barang</th>
                    <th>Quantity</th>
                    <th>Quantity Terima</th>
                    <th>Satuan</th>
                </tr>
            </thead>
            <tbody>
                <?php if (empty($pbdetail)) {
                ?>
                <?php
                } else {
                        $i = 1;
                        foreach ($pbdetail as $data2) {
                ?>
                <tr class="odd gradeA">
                    <td class="text-center"><?php echo $i; ?></td>
                    <td><?php echo $data2->Nama_Barang; ?></td>
                    <td><?php echo $data2->Qty_yard; ?></td>
                    <td><?php echo $data2->Qty_meter; ?></td>
                            
                    <td><?php echo $data2->Satuan; ?></td>
                            
                    
                </tr>
                <?php
                        $i++;
                }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="banner text-center">
        <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('PenerimaanBarang')}}" name="simpan">&nbsp;&nbsp;&nbsp;Kembali</a></span>
        </div>
        <div class="btn col-3 hvr-icon-spin">
          <span><a style="color: white;" href="../printnewpb/<?php echo $data->IDTBS ?>">Print Data&nbsp;&nbsp;&nbsp;</a></span>
        </div>
    </div>
    <br><br><br>
    <br><br><br>
</div>

    <script>
         $(document).ready(function() {
            $('#tblshowpb').DataTable({
                "searching": false,
                "info": false,
                "ordering": true,
                "lengthChange": false
            });
        })
    </script>

@endsection
