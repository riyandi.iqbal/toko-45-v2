<!-- ===========Create By Dedy 19-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php

if ($last_number->curr_number == null) {
  $number = 1;
} else {
  $number = $last_number->curr_number + 1;
}

$kodemax = str_pad($number, 4, "0", STR_PAD_LEFT);
    
if ($last_number2->curr_number == null) {
  $number2 = 1;
} else {
  $number2 = $last_number2->curr_number + 1;
}

$kodemax2 = str_pad($number2, 4, "0", STR_PAD_LEFT);
    
$array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");

$code_booking = '0-'.$kodemax.'/'.'PB'.'/'.$array_bulan[date('n')].'/'.date('y');
    
$code_booking2 = '1-'.$kodemax2.'/'.'PB'.'/'.$array_bulan[date('n')].'/'.date('y');
?>

<!-- style -->
<style type="text/css">
.chzn-container {
    width: 102.5% !important;
}
.chzn-container .chzn-drop {
    width: 198px !important;
}
.chzn-container-single .chzn-search input {
    width: 163px !important;
}
</style>

<div class="main-grid">
	<div class="banner">
		<h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PenerimaanBarang')}}">Data Penerimaan Barang</a>
                <i class="fa fa-angle-right"></i>
                Tambah Penerimaan Barang
        </h2>
	</div><!-- banner -->
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Tambah Data Penerimaan Barang</span>
    </div>
    <div class="banner">

            <table class="table table-responsive">
            <tr>
                <td class="col-md-4">
                    <label>Tanggal</label>
                        <input type="date" class="form-control input-date-padding date-picker hidden-sm-down" name="tanggal" id="tanggal" value="<?php echo date('Y-m-d'); ?>" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <!--<br>-->
                    <label>Nomor PO</label>
                        <select name="no_po" id="no_po" onchange="pindahhalaman()" required style=" width:100%;" class="chosen-select form-control" tabindex="13" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong dan lakukan search')" oninput="setCustomValidity('')">
                            <option value=""><-=Silahkan Pilih Nomor PO=-></option>
                            <?php foreach ($nopo as $po) : ?>
                            <option value="<?php echo $po->IDPO ?>">
                            <?php echo $po->Nomor ?> - <?php echo $po->Nama ?></option>
                            <?php endforeach ?>
                        </select>
                    <br>
                    <label>Total Qty</label>
                    <input type="text" class="form-control" name="qtytotal" id="qtytotal" readonly>
                </td>
                <td class="col-md-4">
                    <label>Supplier</label>
                    <input type="text" name="namasupplier" class="form-control" id="namasupplier" readonly="">
                    <br>
                    <label>Nomor PB</label>
                    <input type="text" class="form-control" name="no_bo" id="no_bo" readonly>
                    
                    <label>Gudang Penerima</label>
                    <select data-placeholder="Gudang Penerima" class="chosen-select" id="gudang" name="gudang">
                      <?php foreach ($gudang as $gd) {
                            echo "<option value='$gd->IDGudang'>$gd->Nama_Gudang</option>";
                        }
                        ?>
                    </select>
                </td>
                <td class="col-md-4">
                    <label>Keterangan</label>
                    <textarea class="form-control" name="keterangan" id="keterangan" rows="7"></textarea>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="banner">
        <table id="tabelfsdfsf" class="display" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
            <tr>
                <th style="width: 5%">No</th>
                <th style="width: 40%">Barang</th>
                <th style="width: 10%">Qty PO</th>
                <th style="width: 10%">Qty Terima</th>
                <!-- <th style="width: 20%">Harga</th>
                <th style="width: 15%">Subtotal</th> -->
            </tr>
            </thead>
            <tbody id="tabel_detail_tampungan">
            </tbody>
        </table>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content px-2 text-center">
            <div class="py-4 mx-2">
                <span><input type="button" class="btn btn-warning" style="cursor: no-drop;" id="print_" name="print_" onclick="print_cek()" value="Print Data"></span>
                <span><button onclick="save()" class="btn btn-primary">Simpan Data</button></span>
            </div>
    </div>
    <br><br><br>

        
</div><!-- main grid -->



<script>
document.getElementById("print_").disabled = true;

</script>
<script type="text/javascript">
            $(document).ready(function() {
                //====================================datatables
                table = $('#tabelfsdfsf').DataTable({ 
                    "searching": false,
                    "lengthChange": false
                });

                var selectedOption  = $("#no_po").val();
                var karakter1       =  selectedOption.slice(1, 2);
                console.log(karakter1);
                if (selectedOption=='0-'){
                    selectedOption = $("#no_bo").val('<?php echo $code_booking ?>');

                }else{
                    selectedOption = $("#no_bo").val('<?php echo $code_booking2 ?>');
                }

                

            });
    
function Penerimaan() {
     this.IDTBS;
     this.Barang;
     this.Qty;
     this.Harga;     
 }

 var PM = new Array();
 var Total = new Array();
 index = 0; 

function pindahhalaman()
{
	var id = $("#no_po").val();
	console.log(id);
	window.location.href = "{{url('PenerimaanBarang/check_PO')}}/id="+id+"";
}

function save() {
     $("#no_po").css('border', '');

     if ($("#no_po").val() == "") {
         swal("Perhatian!","Nomor Po Tidak Boleh Kosong", "warning" );
         $("#no_po").css('border', '1px #C33 solid').focus();
     } else {
         
         console.log('----------- Prosessimpan---------------')
            $.ajax({
                  url : "{{url('PenerimaanBarang/simpandata')}}",
                  type: "POST",
                  data:{"data"          : PM,
                         "Total"        : Total,
                         'id_supplier'  : $('#id_supplier').val(),
                         Keterangan     : $('#keterangan').val(),
                         total_harga    : $('#total_harga').val(),
                         nopo           : $('#no_po').val(),
                         tanggal        : $('#tanggal').val(),
                         nopb           : $('#no_bo').val(),
                         total_qty      : $('#qtytotal').val(),
                         grandtotal     : $('#grandtotal').val(),
                         qty_terima_array: $('#qty_terima_array').val(),
                       },
                  dataType:'json',
                  success: function(data)
                  { 
                    console.log('----------Ajax berhasil-----');
                    swal({
                          title: "Berhasil!",
                          text: "Data Berhasil Disimpan!",
                          icon: "success",
                      }).then(function() {
                          window.location = "../../PenerimaanBarang";
                      });
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                  swal("Perhatian!", "Isi Data Dengan Lengkap!", "warning")
                 }
               });
     }
 }
 //=============================from libjs
  var base_url = window.location.pathname.split('/');
 document.getElementById("print_").disabled = true;

 function Penerimaan() {
     this.IDTBS;
     this.Barcode;
     this.Corak;
     this.Warna;
     this.Merk;
     this.Qty_yard;
     this.Qty_meter;
     this.Grade;
     this.Satuan;
     this.NoPO;
     this.NoSO;
     this.Party;
     this.Indent;
     this.Lebar;
     this.Tanggal;
     this.NoPB;
     this.ID_supplier;
     this.NoSJ;
     this.Keterangan;
     this.Total_yard;
     this.Total_meter;
     this.Harga;
     this.Total_harg;
 }

 var PM = new Array();
 var Total = new Array();
 index = 0;

 function save() {
     $("#no_sjc").css('border', '');

     if ($("#no_sjc").val() == "") {
         $('#alert').html('<div class="pesan sukses">Data No SJC Tidak Boleh Kosong</div>');
         $("#no_sjc").css('border', '1px #C33 solid').focus();
     } else {
         $.post(baseUrl + 'PenerimaanBarang/store', {
                 "data": PM,
                 "Total": Total,
                 'id_supplier': $('#id_supplier').val(),
                 Keterangan: $('#keterangan').val(),
                 total_harga: $('#total_harga').val()
             })
             .done(function(res) {
                 console.log(res);
                 swal({
                          title: "Berhasil!",
                          text: "Data Berhasil Disimpan!",
                          icon: "success",
                      }).then(function() {
                          window.location = "../../PenerimaanBarang";
                      });

             });
     }
 }

 var PM = [];
 var temp = [];

 function renderJSON(data) {
     if (data != null) {
         if (PM == null)
             PM = [];
         console.log(data);
         $("#tabel-cart-asset").html("");
         Total_yard = 0;
         Total_meter = 0;
         for (i = 0; i < data.length; i++) {
             PM[i] = data[i];
             var value =
                 "<tr>" +
                 "<td>" + (i + 1) + "</td>" +
                 // "<td>" + (i + 1) + "</td>" +
                 "<td>" + PM[i].Barcode + "</td>" +
                 "<td>" + PM[i].Corak + "</td>" +
                 "<td>" + PM[i].Warna + "</td>" +
                 "<td>" + PM[i].Merk + "</td>" +
                 "<td class='text-center'>" + PM[i].Qty_yard + "</td>" +
                 "<td class='text-center'>" + PM[i].Qty_meter + "</td>" +
                 "<td>" + PM[i].Grade + "</td>" +
                 "<td>" + PM[i].Satuan + "</td>" +
                 // "<td>"+rupiah(PM[i].Harga)+"</td>"+
                 "</tr>";
             $("#tabel-cart-asset").append(value);
             Total_yard = parseFloat(Total_yard) + parseFloat(PM[i].Qty_yard);
             Total_meter = parseFloat(Total_meter) + parseFloat(PM[i].Qty_meter);
             Total_meter2 = Total_meter.toFixed(2)
         }

         $('#total_meter').val(Total_meter2);
         $('#total_yard').val(Total_yard);
     }
 }

 function deleteItem(i) {

     if (PM[i].id != '') {
         temp.push(PM[i]);
     }

     PM.splice(i, 1);
     renderJSON(PM);
 }

 function field() {
     var data = {
         "IDTBS": $("#IDBTS").val(),
         "Tanggal": $("#Tanggal").val(),
         "IDSupplier": $("#id_supplier").val(),
         "Keterangan": $("#Keterangan").val()
     }
     return data;
 }

 function saveChange() {
     var data = field();
     $.ajax({

         url: "../update_",
         type: "POST",
         data: {
             "data1": data,
             "data2": temp
         },

         success: function(msg, status) {
              swal({
                          title: "Berhasil!",
                          text: "Data Berhasil Dirubah!",
                          icon: "success",
                      }).then(function() {
                          window.location = "../../PenerimaanBarang";
                      });
         },
         error: function(msg, status, data) {
             alert("Failure " + msg + status + data);
         }

     });
 }

 function remove_data() {
     var row = $(this).parent().parent().parent();
     row.remove();
 }

 function print_cek() {
     window.open(baseUrl + "PenerimaanBarang/print_data/");
 }

 function print_cek_edit() {
     sjc = $('#IDBTS').val();
     window.open(baseUrl + "PenerimaanBarang/print_data_edit/" + sjc);
 }

 
</script>
@endsection
