<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('GroupBarang')}}">Data Group Barang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Group Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Group Barang</span>
    </div>
    <div class="banner">
        <form id="form-data">
            <div class="container">
            <br><br>
            <div class="col-md-3">
            <label class="judul">Kode Group Barang</label>
            </div>
            <div class="col-md-9">
            <input type="tex" class="form-control" name="kodegroup" id="kodegroup" value="<?php echo $edit->Kode_Group_Barang ?>"  required oninvalid="this.setCustomValidity('Kode Group Barang Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <input type="hidden" name="IDGroupBarang" id="IDGroupBarang" value="<?php echo $edit->IDGroupBarang; ?>">
            <br><br>
            </div>
                <div class="col-md-3">
                    <label class="judul">Nama Group Barang</label>
                </div>
                <div class="col-md-9">
                    <input type="tex" class="form-control" name="namagroup" id="namagroup"  value="<?php echo $edit->Group_Barang ?>" required oninvalid="this.setCustomValidity('Nama Group Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                </div>
                
            </div>
            <br><br>
            <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                <li class="active"><a href="#tab-1" data-toggle="tab">Data Barang</a></li>
                <li><a href="#tab-2" data-toggle="tab">Kode Tercetak</a></li>
                <li><a href="#tab-3" data-toggle="tab">Nama Tercetak</a></li>
            </ul>
            <div id="my-tab-content" class="tab-content">
                <!-- ================== TAB 1 ===-->
                <div class="tab-pane active" id="tab-1">
                <br><br><br><br>
                    <div class="form-title">
                        <span><i class="fa fa-th-list"></i>&nbsp;Data Barang</span>   
                    </div> 
                    <div class="container" style="width: 100%">
                    <br>
                        <div><input type="checkbox" id="kode" name="kode" value="kode"> Kode Group</div><br>
                        <div><input type="checkbox" id="nama" name="nama" value="nama"> Nama</div><br>
                        <div><input type="checkbox" id="tipe" name="tipe" value="tipe"> Tipe</div><br>
                        <div><input type="checkbox" id="ukuran" name="ukuran" value="ukuran"> Ukuran</div><br>
                        <div><input type="checkbox" id="kategori" name="kategori" value="kategori"> Kategori</div><br>
                    </div>
                </div> 

                <!-- ================== TAB 2 ===-->
                <div class="tab-pane" id="tab-2">
                <br><br><br><br>
                    <div class="form-title">
                        <span><i class="fa fa-th-list"></i>&nbsp;Kode Tercetak</span>   
                    </div> 
                    <div class="container" style="width: 100%">
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 30%; border: 1px solid #000; padding: 5px;">
                                <div class="kode2">
                                    <input type="checkbox" id="kode2" name="kode2" value="kode2"> 
                                    <select style="width: 75%; float: right;" class="form-control" id="kode2select" name="kode2select">
                                        <option value="kode2">Kode Group</option>
                                        <option value="nama2">Nama Group</option>
                                    </select> 
                                </div>   
                                <br>
                                
                                <div class="nama2"><input type="checkbox" id="nama2" name="nama2" value="nama2"> Nama</div><br>
                                <div class="tipe2"><input type="checkbox" id="tipe2" name="tipe2" value="tipe2"> Tipe</div><br>
                                <div class="ukuran2"><input type="checkbox" id="ukuran2" name="ukuran2" value="ukuran2"> Ukuran</div><br>
                                <div class="kategori2"><input type="checkbox" id="kategori2" name="kategori2" value="kategori2"> Kategori</div><br>
                            </td>
                            <td align="left" valign="top" style="width: 70%; border: 1px solid #000; padding: 5px;" >
                                <div>
                                    <label class="judul">Pemisah</label>&nbsp;
                                    <input tipe="text" class="col-md-3 form-control" id="pemisah" name="pemisah">&nbsp;<label style="font-style: italic;">Default : _(satu spasi)</label> 
                                </div>
                                <br>
                                <div>
                                    <label class="judul">Panjang Nomor Urut</label>&nbsp;
                                    <input tipe="text" class="col-md-3 form-control" id="panjang" name="panjang">
                                </div>
                                <div>
                                    <label class="judul">Kode Tercetak</label><br>
                                    <label id="kodelabel">[KODE]</label>
                                    <label id="pemisahlabel"></label>
                                    <label id="namalabel">[NAMA]</label>
                                    <label id="pemisahlabel2"></label>
                                    <label id="tipelabel">[TIPE]</label>
                                    <label id="pemisahlabel3"></label>
                                    <label id="ukuranlabel">[UKURAN]</label>
                                    <label id="pemisahlabel4"></label>
                                    <label id="kategorilabel">[KATEGORI]</label>
                                    <label id="pemisahlabel5"></label>
                                    <label id="panjanglabel"></label>
                                </div>
                                <br><br><br>
                            </td>
                        </tr>
                    </table>
                    <br>
                        
                    </div>
                </div> 

                <!-- ================== TAB 3 ===-->
                <div class="tab-pane" id="tab-3">
                <br><br><br><br>
                    <div class="form-title">
                        <span><i class="fa fa-th-list"></i>&nbsp;Nama Tercetak</span>   
                    </div> 
                    <div class="container" style="width: 100%">
                    <br>
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 30%; border: 1px solid #000; padding: 5px;">
                                <div class="kode3">
                                    <input type="checkbox" id="kode3" name="kode3" value="kode3"> 
                                    <select style="width: 75%; float: right;" class="form-control" id="kode3select" name="kode3select">
                                        <option value="kode3">Kode Group</option>
                                        <option value="nama3">Nama Group</option>
                                    </select> 
                                </div><br>
                                <div class="nama3"><input type="checkbox" id="nama3" name="nama3" value="nama3"> Nama</div><br>
                                <div class="tipe3"><input type="checkbox" id="tipe3" name="tipe3" value="tipe3"> Tipe</div><br>
                                <div class="ukuran3"><input type="checkbox" id="ukuran3" name="ukuran3" value="ukuran3"> Ukuran</div><br>
                                <div class="kategori3"><input type="checkbox" id="kategori3" name="kategori3" value="kategori3"> Kategori</div><br>
                            </td>
                            <td align="left" valign="top" style="width: 70%; border: 1px solid #000; padding: 5px;" >
                                <div>
                                    <label class="judul">Pemisah</label>&nbsp;
                                    <input tipe="text" class="col-md-3 form-control" id="pemisah2" name="pemisah2">&nbsp;<label style="font-style: italic;">Default : _(satu spasi)</label> 
                                </div>
                                <br>
                                <div>
                                    <label class="judul">Panjang Nomor Urut</label>&nbsp;
                                    <input tipe="text" class="col-md-3 form-control" id="panjang2" name="panjang2">
                                </div>
                                <div>
                                    <label class="judul">Nama Tercetak</label><br>
                                    <label id="2kodelabel">[KODE]</label>
                                    <label id="2pemisahlabel"></label>
                                    <label id="2namalabel">[NAMA]</label>
                                    <label id="2pemisahlabel2"></label>
                                    <label id="2tipelabel">[TIPE]</label>
                                    <label id="2pemisahlabel3"></label>
                                    <label id="2ukuranlabel">[UKURAN]</label>
                                    <label id="2pemisahlabel4"></label>
                                    <label id="2kategorilabel">[KATEGORI]</label>
                                    <label id="2pemisahlabel5"></label>
                                    <label id="2panjanglabel"></label>
                                </div>
                                <br><br><br>
                            </td>
                        </tr>
                    </table>
                    <br>
                    </div>
                </div>
                <br><br>
                <div class="text-center">
                    <br><br>
                    <div class="btn col-11 hvr-icon-back">
                        <span> <a href="{{url('GroupBarang')}}" style="color: white;" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn">
                        <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("GroupBarang") }}';
  var urlUpdate           = '{{ url("GroupBarang/simpandataedit") }}';
  var url                 = '{{ url("GroupBarang") }}';

  var Panjang_Kode  = '{{ $edit->Panjang_Kode }}';
  var Pemisah_Kode  = '{{ $edit->Pemisah_Kode }}';
  var Panjang_Nama  = '{{ $edit->Panjang_Nama }}';
  var Pemisah_Nama  = '{{ $edit->Pemisah_Nama }}';

  var Checked_Kode  = '{{ $array_kode[0] }}';
  var Checked_Nama  = '{{ $array_kode[1] }}';
  var Checked_Tipe  = '{{ $array_kode[2] }}';
  var Checked_Ukuran  = '{{ $array_kode[3] }}';
  var Checked_Kategori  = '{{ $array_kode[4] }}';
  var data_barang_exist = {{ ($data_barang) ? 'true' : 'false' }};
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/baranggroup/update.js') }}"></script>
<script>
    $(document).ready(function() {
        if (Checked_Kode == 'KODE') {
            $('#kode').prop('checked', true).trigger('change');
            $('#kode2').prop('checked', true).trigger('change');
            $('#kode3').prop('checked', true).trigger('change');
        }

        if (Checked_Nama == 'NAMA') {
            $('#nama').prop('checked', true).trigger('change');
            $('#nama2').prop('checked', true).trigger('change');
            $('#nama3').prop('checked', true).trigger('change');
        }

        if (Checked_Tipe == 'TIPE') {
            $('#tipe').prop('checked', true).trigger('change');
            $('#tipe2').prop('checked', true).trigger('change');
            $('#tipe3').prop('checked', true).trigger('change');
        }

        if (Checked_Ukuran == 'UKURAN') {
            $('#ukuran').prop('checked', true).trigger('change');
            $('#ukuran2').prop('checked', true).trigger('change');
            $('#ukuran3').prop('checked', true).trigger('change');
        }

        if (Checked_Kategori == 'KATEGORI') {
            $('#kategori').prop('checked', true).trigger('change');
            $('#kategori2').prop('checked', true).trigger('change');
            $('#kategori3').prop('checked', true).trigger('change');
        }

        if (data_barang_exist === true) {
            $('#my-tab-content input').prop('disabled', true);
            $('#my-tab-content select').prop('disabled', true);
        }
    })
</script>
@endsection