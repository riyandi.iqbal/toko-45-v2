<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('GroupBarang')}}">Data Group Barang</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Group Barang</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Group Barang</span>
    </div>
    <div class="banner">
        <label class="judul">Kode Group Barang</label>
        <input type="text" class="form-control" id="kodegroup" name="kodegroup" onkeyup="javascript:this.value=this.value.toUpperCase();">
        <br>
        <label class="judul">Nama Group Barang</label>
        <input type="text" class="form-control" id="namagroup" name="namagroup" onkeyup="javascript:this.value=this.value.toUpperCase();">
        <br><br>
        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
            <li class="active"><a href="#tab-1" data-toggle="tab">Data Barang</a></li>
            <li><a href="#tab-2" data-toggle="tab">Kode Tercetak</a></li>
            <li><a href="#tab-3" data-toggle="tab">Nama Tercetak</a></li>
        </ul>
        <div id="my-tab-content" class="tab-content">
            <!-- ================== TAB 1 ===-->
            <div class="tab-pane active" id="tab-1">
            <br><br><br><br>
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Data Barang</span>   
                </div> 
                <div class="container" style="width: 100%">
                <br>
                    <div><input type="checkbox" id="kode" name="kode" value="kode" onchange="kode()"> Kode Group</div><br>
                    <div><input type="checkbox" id="nama" name="nama" value="nama" onchange="nama()"> Nama</div><br>
                    <div><input type="checkbox" id="tipe" name="tipe" value="tipe" onchange="tipe()"> Tipe</div><br>
                    <div><input type="checkbox" id="ukuran" name="ukuran" value="ukuran" onchange="ukuran()"> Ukuran</div><br>
                    <div><input type="checkbox" id="kategori" name="kategori" value="kategori" onchange="kategori()"> Kategori</div><br>
                </div>
            </div> 

            <!-- ================== TAB 2 ===-->
            <div class="tab-pane" id="tab-2">
            <br><br><br><br>
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Kode Tercetak</span>   
                </div> 
                <div class="container" style="width: 100%">
                <br>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 30%; border: 1px solid #000; padding: 5px;">
                            <div class="kode2">
                                <input type="checkbox" id="kode2" name="kode2" value="kode2" onchange="kode2()"> 
                                <select style="width: 75%; float: right;" class="form-control" id="kode2select" name="kode2select" onchange="kode2()">
                                    <option value="kode2">Kode Group</option>
                                    <option value="nama2">Nama Group</option>
                                </select> 
                            </div>   
                            <br>
                            
                            <div class="nama2"><input type="checkbox" id="nama2" name="nama2" value="nama2" onchange="nama2()"> Nama</div><br>
                            <div class="tipe2"><input type="checkbox" id="tipe2" name="tipe2" value="tipe2" onchange="tipe2()"> Tipe</div><br>
                            <div class="ukuran2"><input type="checkbox" id="ukuran2" name="ukuran2" value="ukuran2" onchange="ukuran2()"> Ukuran</div><br>
                            <div class="kategori2"><input type="checkbox" id="kategori2" name="kategori2" value="kategori2" onchange="kategori2()"> Kategori</div><br>
                        </td>
                        <td align="left" valign="top" style="width: 70%; border: 1px solid #000; padding: 5px;" >
                            <div>
                                <label class="judul">Pemisah</label>&nbsp;
                                <input tipe="text" class="col-md-3 form-control" id="pemisah" name="pemisah" onkeyup="pemisah()">&nbsp;<label style="font-style: italic;">Default : _(satu spasi)</label> 
                            </div>
                            <br>
                            <div>
                                <label class="judul">Panjang Nomor Urut</label>&nbsp;
                                <input tipe="text" class="col-md-3 form-control" id="panjang" name="panjang" onkeyup="panjang()">
                            </div>
                            <div>
                                <label class="judul">Kode Tercetak</label><br>
                                <label id="kodelabel">[KODE]</label>
                                <label id="pemisahlabel"></label>
                                <label id="namalabel">[NAMA]</label>
                                <label id="pemisahlabel2"></label>
                                <label id="tipelabel">[TIPE]</label>
                                <label id="pemisahlabel3"></label>
                                <label id="ukuranlabel">[UKURAN]</label>
                                <label id="pemisahlabel4"></label>
                                <label id="kategorilabel">[KATEGORI]</label>
                                <label id="pemisahlabel5"></label>
                                <label id="panjanglabel"></label>
                            </div>
                            <br><br><br>
                        </td>
                    </tr>
                </table>
                <br>
                    
                </div>
            </div> 

            <!-- ================== TAB 3 ===-->
            <div class="tab-pane" id="tab-3">
            <br><br><br><br>
                <div class="form-title">
                    <span><i class="fa fa-th-list"></i>&nbsp;Nama Tercetak</span>   
                </div> 
                <div class="container" style="width: 100%">
                <br>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 30%; border: 1px solid #000; padding: 5px;">
                            <div class="kode3">
                                <input type="checkbox" id="kode3" name="kode3" value="kode3" onchange="kode3()"> 
                                <select style="width: 75%; float: right;" class="form-control" id="kode3select" name="kode3select" onchange="kode3()">
                                    <option value="kode3">Kode Group</option>
                                    <option value="nama3">Nama Group</option>
                                </select> 
                            </div><br>
                            <div class="nama3"><input type="checkbox" id="nama3" name="nama3" value="nama3" onchange="nama3()"> Nama</div><br>
                            <div class="tipe3"><input type="checkbox" id="tipe3" name="tipe3" value="tipe3" onchange="tipe3()"> Tipe</div><br>
                            <div class="ukuran3"><input type="checkbox" id="ukuran3" name="ukuran3" value="ukuran3" onchange="ukuran3()"> Ukuran</div><br>
                            <div class="kategori3"><input type="checkbox" id="kategori3" name="kategori3" value="kategori3" onchange="kategori3()"> Kategori</div><br>
                        </td>
                        <td align="left" valign="top" style="width: 70%; border: 1px solid #000; padding: 5px;" >
                            <div>
                                <label class="judul">Pemisah</label>&nbsp;
                                <input tipe="text" class="col-md-3 form-control" id="pemisah2" name="pemisah2" onkeyup="pemisah2()">&nbsp;<label style="font-style: italic;">Default : _(satu spasi)</label> 
                            </div>
                            <br>
                            <div>
                                <label class="judul">Panjang Nomor Urut</label>&nbsp;
                                <input tipe="text" class="col-md-3 form-control" id="panjang2" name="panjang2" onkeyup="panjang2()">
                            </div>
                            <div>
                                <label class="judul">Nama Tercetak</label><br>
                                <label id="2kodelabel">[KODE]</label>
                                <label id="2pemisahlabel"></label>
                                <label id="2namalabel">[NAMA]</label>
                                <label id="2pemisahlabel2"></label>
                                <label id="2tipelabel">[TIPE]</label>
                                <label id="2pemisahlabel3"></label>
                                <label id="2ukuranlabel">[UKURAN]</label>
                                <label id="2pemisahlabel4"></label>
                                <label id="2kategorilabel">[KATEGORI]</label>
                                <label id="2pemisahlabel5"></label>
                                <label id="2panjanglabel"></label>
                            </div>
                            <br><br><br>
                        </td>
                    </tr>
                </table>
                <br>
                </div>
            </div>
            <br><br>
        </div>
        <button onclick="simpandata()" class="btn btn-primary">Simpan</button>
    </div>
</div>

</div>
<script src="{{ asset('js/baranggroup/groupbarang_create.js') }}"></script>
<script>
    var token      = "{{ csrf_token() }}";
    var urlSimpan  = "{{url('GroupBarang/simpangroupbaru')}}";
    var BackGroupBarang = "{{url('GroupBarang')}}";
</script>
@endsection