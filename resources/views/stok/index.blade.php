<!-- create @2019-01-07
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')

<?php
$hari_ini = date("Y-m-01");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>

	<div class="main-grid">
		<div class="banner">
			<h2>
	            <span><i class="fa fa-home"></i><a href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	                <i class="fa fa-angle-right"></i>
	                <a href="{{url('stok')}}">Laporan Stok</a>
	        </h2>
		</div>
		<br>
		<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
	    	<span>Laporan Stok</span>
	    </div>
		<div class="banner">
			<div class="container">
				<div class="col-md-3">
					<label>Periode</label>
					<input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-01') ?>">
				</div>
				<div class="col-md-3">
					<label> S/D </label>
                    <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
				</div>
				<div class="col-md-3">
					<label>.</label>
                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Cari Nama Barang">
				</div>
				<div class="col-md-3">
					<label>.</label><br>
					<button onclick="caridata()" class="btn btn-primary">SEARCH</button>
				</div>
			</div>
		</div>
		<br>
		<div class="banner">
			<table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
					<th>No</th>
					<th>Gudang</th>
					<th>Nama Barang</th>
					<th>Satuan</th>
					<th>Qty</th>
				</thead>
			</table>
		</div>

	</div>

</div>

<script>
    var urlData = '{{ route("Stok.datatable") }}';
    var url = '{{ url("stok") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/stok/index.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('alertakses'))
            swal("Perhatian", "{{ session('alertakses') }}", "warning");
        @endif
    })
</script>
@endsection
