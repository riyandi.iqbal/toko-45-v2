<!-- create @2019-01-07
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
	<div class="main-grid">
		<div class="banner" id="hiden1">
			<h2>
	            <span><i class="fa fa-home"></i><a href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	                <i class="fa fa-angle-right"></i>
	                <a href="{{url('Laporan_stok')}}">Laporan Stok</a>
	                <i class="fa fa-angle-right"></i>
	                <a href="#">Laporan Kartu Stok &nbsp;<b><?php echo $headerstok->Nama_Barang ?></b></a>
	        </h2>
		</div>
		<br>
    <div class="banner" id="hidenheader">
      <h1>Laporan Kartu Stok Barang  &nbsp;<b><?php echo $headerstok->Nama_Barang ?></b></h1>
    </div>
    <br>
		<div class="banner">
			<div class="widget_content">
                    <div class="form_container left_label">
                        <table class="table cell-border" width="100%" style="font-size: 12px;">
                            <thead style="display: none;">
                            </thead>
                            <tbody> 
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo date("d M Y" )?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff; border: 1px solid;">Kode Barang</td>
                                    <td style="background-color: #ffffff; border: 1px solid; font-weight: bold;"><?php echo $headerstok->Kode_Barang ?></td>
                                </tr>  
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Nama Barang</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo $headerstok->Nama_Barang ?></td>
                                </tr>
                                <tr>         
                                    <td style="background-color: #ffffff; border: 1px solid;">Kategori</td>
                                    <td style="background-color: #ffffff; border: 1px solid; font-weight: bold;"><?php echo $headerstok->Nama_Kategori ?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tipe</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo $headerstok->Nama_Tipe ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff; border: 1px solid;">Ukuran</td>
                                    <td style="background-color: #ffffff; border: 1px solid; font-weight: bold;"><?php echo $headerstok->Nama_Ukuran ?></td>
                                </tr>
                                <tr>         
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Satuan</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo $headerstok->Satuan ?></td>
                                </tr>  
                                <tr>         
                                    <td style="background-color: #ffffff; border: 1px solid;">Gudang</td>
                                    <td style="background-color: #ffffff; border: 1px solid; font-weight: bold;"><?php echo $headerstok->Nama_Gudang ?></td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <!-- <table class="display data_tbl">
                 --><table id="tblshowstok" class="table cell-border" width="100%" style="font-size: 12px;">
                  <thead style="background-color: #16305d; color: #fff">
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Nomor Transaksi</th>
                      <th>Qty</th>
                      <th>Berat /Qty</th>
                      <th>Satuan</th>
                      <!-- <th>Total</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($kartustok)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($kartustok as $data2) {
                        if($data2->Masuk_pcs==''||$data2->Masuk_pcs==0){
                          $qtystok = '-'.$data2->Keluar_pcs;
                          $qtystok2 = $data2->Keluar_pcs;
                        }else{
                          $qtystok = $data2->Masuk_pcs;
                          $qtystok2 = $data2->Masuk_pcs;
                        }
                      ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo date('d M Y', strtotime($data2->Tanggal))?></td>
                          <td><?php echo $data2->Nomor_faktur; ?></td>
                          <td style="text-align: center;"><?php echo $qtystok; ?></td>
                          <td style="text-align: center;"><?php echo $data2->Berat; ?></td>
                          <td><?php echo $data2->Satuan; ?></td>
                          <!-- <td style="text-align: right;"><?php echo $qtystok2; ?></td> -->
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center" id="hiden2">
                            <div class="form_grid_12">
                                <div class="btn col-11 hvr-icon-back">
                                    <span> <a style="color: white;" href="{{url('Laporan_stok')}}" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn col-3 hvr-icon-spin">
                                  <span><a style="color: white;" onclick="printdata()"  >Print Data</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		</div>

	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblshowstok').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });
        $('#hidenheader').hide();            
    })
    function printdata()
    {

      $('#hiden1').hide();
      $('#hiden2').hide();
      $('#hidenheader').show(); 
      $('.headerprint').hide();
      $('.main-menu').hide();
      $('.title-bar').hide();
      $('#tblshowstok_paginate').hide();

      window.print();
      location.reload();
    }
</script>
@endsection