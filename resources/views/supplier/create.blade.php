<!-- create @2019-12-11
dedywinda@gmail.com -->
<?php
    if ($kode->curr_number == null) {
        $number = 1;
    } else {
        $number = $kode->curr_number + 1;
    }
    $code = 'SP-'.$number;
?>
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Supplier')}}">Data Supplier</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Supplier</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Tambah Data Supplier</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
        <div class="col-md-3">
          <label class="judul">Group Supplier</label>
        </div>
        <div class="col-md-9">
          <select data-placeholder="Pilih Group Supplier" style=" width:75%;" class="chosen-select" tabindex="13"  name="IDGroupSupplier" id="IDGroupSupplier" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <option value=""></option>
              <?php 
              foreach ($DataGroup as $key ) { 
              ?>
              <option value="<?= $key->IDGroupSupplier ?>"> <?= 'Kode Group : '.$key->Kode_Group_Supplier.' - Group '.$key->Group_Supplier ?></option> 
              <?php }
              ?>
            </select>

                <div class="flot-right btn btn-primary hvr-icon-float-away">
                	<a data-toggle="modal" data-target="#modalgroup"><span style="color: white;">Group Supplier</span></a>
                </div>

          <br><br>
        </div>
            <div class="col-md-3">
                <label class="judul">Kode Supplier</label>
            </div>
            <div class="col-md-9">

<input type="tex" class="form-control" name="Kode_Suplier" id="Kode_Suplier" placeholder="Kode_Suplier" value="<?php echo $code; ?>" required oninvalid="this.setCustomValidity('Kode Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')" readonly>

                <br><br>
            </div>

            <div class="col-md-3">
              <label class="judul">Nama Supplier</label>
            </div>
            <div class="col-md-9">
              <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama" id="Nama" placeholder="Nama" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Alamat</label>	
            </div>
            <div class="col-md-9">
            	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Alamat" id="Alamat" placeholder="Alamat" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            	<br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">No Telpon</label>                 
            </div>
            <div class="col-md-9">
                  <input type="tex" class="form-control" name="No_Telpon" id="No_Telpon" placeholder="0821xxxxxx" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Kota</label>
            </div>
            <div class="col-md-9">            	
                <select data-placeholder="Pilih Kota" style=" width:75%;" class="chosen-select" name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>                    
                    <?php 
                       foreach ($DataKota as $key ) { 
                    ?>
                    <option value="<?= $key->IDKota ?>"> <?= $key->Kota ?></option> 
                    <?php }
                    ?>
                </select>
              
                    <div class="flot-right btn btn-primary hvr-icon-float-away">
                    	<a data-toggle="modal" data-target="#modalkota"><span style="color: white;">Tambah Kota</span></a>
                    </div>
               
                <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Fax</label>               
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" name="Fax" id="Fax" placeholder=" Fax" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Email</label>
            </div>
            <div class="col-md-9">
            	<input type="email" class="form-control" name="Email" id="Email" placeholder=" Email" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">NPWP</label>
            </div>
            <div class="col-md-9">
            	<input type="tex" class="form-control" name="NPWP" id="NPWP" placeholder=" NPWP" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            	<br><br>
            </div>
            
            <div class="col-md-3">
            	<label class="judul">No KTP</label>
            </div> 
            <div class="col-md-9">
            	<input type="tex" class="form-control" name="No_KTP" id="No_KTP" placeholder=" No_KTP" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            </div>
            
        </div>
        <div class="text-center">
            <br><br>
            <div class="btn col-11 hvr-icon-back">
                <span> <a href="{{url('Supplier')}}" style="color: white;" name="simpan">Kembali</a></span>
            </div>
            <div class="btn">
                <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
            </div>
        </div>
      </form>
    </div>
</div>

</div>



			<div id="modalgroup" class="modal fade" role="dialog">
              <div class="modal-dialog">
              	<form method="post" action="{{url('Supplier/simpandatagroup')}}" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Data Group Supplier</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                    	<label class="judul">Kode Group Supplier</label>             
                    </div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Kode_Group_Supplier" id="Kode_Group_Supplier" placeholder="Kode Group Supplier" required oninvalid="this.setCustomValidity('Kode Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Group Supplier</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" name="Group_Supplier" onkeyup="javascript:this.value=this.value.toUpperCase();" id="Group_Supplier" class="form-control" placeholder="Nama Group Supplier" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            	</form>
              </div>
            </div>


            <div id="modalkota" class="modal fade" role="dialog">
              <div class="modal-dialog">
              	<form method="post" action="{{url('Supplier/simpandatakota')}}" enctype="multipart/form-data" class="form_container left_label">
                <!-- Modal content-->
                {{ csrf_field() }}
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kota</h4>
                  </div>
                  <div class="modal-body">
                    <div class="col-md-3">
                    	<label class="judul">Kode Kota</label>             
                    </div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kode Kota" name="kode" id="kode" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Provinsi</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Provinsi" name="provinsi" id="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                    <div class="col-md-3">
                    	<label class="judul">Kota</label>
            		</div>
                    <div class="col-md-9">
                    	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" placeholder="Kota" name="kota" id="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <br><br>
                    </div>

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                  </div>
                </div>
            	</form>
              </div>
            </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Supplier") }}';
  var urlInsert           = '{{ route("Supplier.store") }}';
  var url                 = '{{ url("Supplier") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/supplier/create.js') }}"></script>
@endsection 
