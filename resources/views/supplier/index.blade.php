<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="#">Data Supplier</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-12">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url('Supplier/create')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div>
            </div>
            <br><br><br>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                    <option value=""></option>
                    <option value="Kode_Suplier">Kode Supplier</option>
                    <option value="Nama">Nama</option>
                    <option value="Alamat">Alamat</option>
                    <option value="Kota">Kota</option>
                    <option value="Email">Email</option>
                    <option value="NPWP">NPWP</option>
                    <option value="No_KTP">No KTP</option>
                </select>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <input name="keyword" id="keyword" type="tex" placeholder="Cari Nama Kategori" class="form-control">
            </div>
            <div class="col-md-3">
                <label>&nbsp;.</label>
                <br>
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>
    </div>
    <br>
    <div class="banner">
        <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
            <thead style="color: #fff">
                <th>No</th>
                <th>Kode Supplier</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>No Telp</th>
                <th>Aksi</th>
            </thead>
        </table>
    </div>
</div>

</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/supplier/index.js') }}"></script>
<script>
    var urlData = '{{ route("Supplier.datatable") }}';
    var url                 = '{{ url("Supplier") }}';
</script>
@endsection