<!-- create @2019-12-11
dedywinda@gmail.com -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Supplier')}}">Data Supplier</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Supplier</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
        <span>Edit Data Supplier</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
        <div class="col-md-3">
          <label class="judul">Group Supplier</label>
        </div>
        <div class="col-md-9">
          <input type="hidden" name="IDSupplier" id="IDSupplier" value="<?php echo $Supplier->IDSupplier?>">
          <select data-placeholder="Pilih Group Supplier" style=" width:75%;" class="chosen-select" tabindex="13"  name="IDGroupSupplier" id="IDGroupSupplier" required oninvalid="this.setCustomValidity('Group Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <option value=""></option>
              <?php 
              foreach ($DataGroup as $key ) { 
                if ($key->IDGroupSupplier == $Supplier->IDGroupSupplier) { ?>
              <option value="<?= $key->IDGroupSupplier ?>" selected> <?= 'Kode Group : '.$key->Kode_Group_Supplier.' - Group : '.$key->Group_Supplier ?></option> 
              <!-- Bila Data Sudah isi sebelumnya -->
              <?php }
              ?>
              <option value="<?= $key->IDGroupSupplier ?>"> <?= 'Kode Group : '.$key->Kode_Group_Supplier.' - Group '.$key->Group_Supplier ?></option> 
              <?php }
              ?>
            </select>
          <br><br>
        </div>
            <div class="col-md-3">
                <label class="judul">Kode Supplier</label>
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" name="Kode_Suplier" id="Kode_Suplier" placeholder="Kode_Suplier" value="<?php echo $Supplier->Kode_Suplier; ?>" required oninvalid="this.setCustomValidity('Kode Supplier Tidak Boleh Kosong')" oninput="setCustomValidity('')">

                <br><br>
            </div>

            <div class="col-md-3">
              <label class="judul">Nama Supplier</label>
            </div>
            <div class="col-md-9">
              <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama" id="Nama" placeholder="Nama" value="<?php echo $Supplier->Nama; ?>" required oninvalid="this.setCustomValidity('Nama Tidak Boleh Kosong')" oninput="setCustomValidity('')">
              <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Alamat</label>	
            </div>
            <div class="col-md-9">
            	<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Alamat" id="Alamat" placeholder="Alamat" value="<?php echo $Supplier->Alamat; ?>" required oninvalid="this.setCustomValidity('Alamat Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            	<br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">No Telpon</label>                 
            </div>
            <div class="col-md-9">
                  <input type="tex" class="form-control" name="No_Telpon" id="No_Telpon" placeholder="0821xxxxxx" value="<?php echo $Supplier->No_Telpon; ?>" required oninvalid="this.setCustomValidity('No Telpon Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Kota</label>
            </div>
            <div class="col-md-9">            	
                <select data-placeholder="Pilih Kota" style=" width:75%;" class="chosen-select" name="IDKota" id="IDKota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
                    <option value=""></option>
                    <?php 
                       foreach ($DataKota as $key ) { 
                       if ($key->IDKota == $Supplier->IDKota) { ?>
                    <option value="<?= $key->IDKota ?>" selected> <?= $key->Kota ?></option> 
                    <?php }
                    ?>
                    <option value="<?= $key->IDKota ?>"> <?= $key->Kota ?></option> 
                    <?php }
                    ?>
                </select>
                <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Fax</label>               
            </div>
            <div class="col-md-9">
                <input type="tex" class="form-control" name="Fax" id="Fax" placeholder=" Fax" value="<?php echo $Supplier->Fax; ?>" required oninvalid="this.setCustomValidity('Fax Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">Email</label>
            </div>
            <div class="col-md-9">
            	<input type="email" class="form-control" name="Email" id="Email" placeholder=" Email" value="<?php echo $Supplier->Email; ?>" required oninvalid="this.setCustomValidity('Email Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
            </div>

            <div class="col-md-3">
            	<label class="judul">NPWP</label>
            </div>
            <div class="col-md-9">
            	<input type="tex" class="form-control" name="NPWP" id="NPWP" placeholder=" NPWP" value="<?php echo $Supplier->NPWP; ?>" required oninvalid="this.setCustomValidity('NPWP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            	<br><br>
            </div>
            
            <div class="col-md-3">
            	<label class="judul">No KTP</label>
            </div> 
            <div class="col-md-9">
            	<input type="tex" class="form-control" name="No_KTP" id="No_KTP" placeholder=" No_KTP" value="<?php echo $Supplier->No_KTP; ?>" required oninvalid="this.setCustomValidity('No KTP Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            </div>
            
        </div>
        <div class="text-center">
            <br><br>
            <div class="btn col-11 hvr-icon-back">
                <span> <a href="{{url('Supplier')}}" style="color: white;" name="simpan">Kembali</a></span>
            </div>
            <div class="btn">
                <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
            </div>
        </div>
      </form>
    </div>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Supplier") }}';
  var urlUpdate           = '{{ route("Supplier.update_data") }}';
  var url                 = '{{ url("Supplier") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/supplier/update.js') }}"></script>
@endsection 