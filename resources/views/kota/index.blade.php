<!-- /*=====Create DEDY @10/12/2019====*/ -->
@extends('layouts.app')   
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>

<div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Kota')}}">Data Kota</a>
        </h2>
    </div>
    <br>
    <div class="banner container">
            <div class="form_grid_3">
                <div class="btn btn-primary hvr-icon-float-away">
                    <a href="{{url('Kota/create')}}"><span style="color: white;">Tambah Data&nbsp;&nbsp;</span></a>
                </div>
            </div>
            <br>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label>.</label>
                    <select data-placeholder="Cari Berdasarkan" name="field" id="field" style="width: 100%!important" class="chosen-select" tabindex="13">
                        <option value=""></option>
                        <option value="Kode_Kota">Kode Kota</option>
                        <option value="Provinsi">Provinsi</option>
                        <option value="Kota">Kota</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Nama Kota" class="form-control">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                </div>                
            </div>
    </div>
    <br>
    <div class="banner">
        <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
            <thead style="color: #fff">
                <tr>
                    <th>No</th>
                    <th>Kode Kota</th>
                    <th>Provinsi</th>
                    <th>Kota</th>
                    <th>Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/kota/index.js') }}"></script>
<script>
    var urlData = '{{ route("Kota.datatable") }}';
    var url                 = '{{ url("Kota") }}';
</script>
@endsection