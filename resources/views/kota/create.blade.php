<!-- /*=====Create DEDY @10/12/2019====*/ -->
@extends('layouts.app')   
@section('content')

  <div class="main-grid">
    <div class="banner">
         <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Kota')}}">Data Kota</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Kota</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Tambah Data Kota</span>
    </div>
    <div class="banner">
      <form id="form-data">
        <div class="container">
        <br><br>
          <div class="col-md-3">
            <label class="judul">Kode Kota</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="kodekota" id="kodekota" required oninvalid="this.setCustomValidity('Kode Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Provinsi</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="provinsi" id="provinsi" required oninvalid="this.setCustomValidity('Provinsi Tidak Boleh Kosong')" oninput="setCustomValidity('')">
            <br><br>
          </div>
          <div class="col-md-3">
            <label class="judul">Kota</label>
          </div>
          <div class="col-md-9">
            <input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="kota" id="kota" required oninvalid="this.setCustomValidity('Kota Tidak Boleh Kosong')" oninput="setCustomValidity('')">
          </div>
        </div>
        <br><br><br>
        <div class="text-center">
          <div class="btn col-11 hvr-icon-back">
            <span> <a style="color: white;" href="{{url('home')}}" name="simpan">Kembali</a></span>
          </div>
          <div class="btn">
              <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Kota") }}';
  var urlInsert           = '{{ route("Kota.store") }}';
  var url                 = '{{ url("Kota") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/kota/create.js') }}"></script>
@endsection