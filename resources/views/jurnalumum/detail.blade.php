<!-- ===========Create By Tiar 18-02-2020 =============== -->
@extends('layouts.app')
@section('content')
    <!-- body data -->
    <?php use App\Helpers\AppHelper; ?>
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('JurnalUmum')}}">Jurnal Umum</a>
              <i class="fa fa-angle-right"></i>
              Detail Jurnal Umum - {{ $jurnal_umum->Nomor }}
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table table-bordered" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" >Tanggal</td>
                            <td width="65%"> {{ AppHelper::DateIndo($jurnal_umum->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td> {{ $jurnal_umum->Nomor }} </td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td> {{ ($jurnal_umum->Batal == 0) ? 'Aktif' : 'Dibatalkan' }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Kode Perkiraan</th>
                            <th>Nama Perkiraan</th>
                            <th>Keterangan</th>
                            <th>Debet</th>
                            <th>Kredit </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $total_debet = 0; $total_kredit=0; ?>
                        @foreach ($jurnal_umum_detail as $key => $item)
                        <?php $total_debet += $item->Debet; $total_kredit += $item->Kredit; ?>
                            <tr>
                                <td>{{ ++$key }}</td>
                                <td> {{ $item->Kode_COA }} </td>
                                <td> {{ $item->Nama_COA }} </td>
                                <td> {{ $item->Keterangan }} </td>
                                <td style="text-align: right;"> {{ $item->Kode . AppHelper::NumberFormat($item->Debet) }} </td>
                                <td style="text-align: right;"> {{ $item->Kode . AppHelper::NumberFormat($item->Kredit) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot style="background-color: #16305d; color: #fff">
                        <tr>
                            <th colspan="4">Total</th>
                            <th style="text-align: right;"> {{ AppHelper::NumberFormat($total_debet) }} </th>
                            <th style="text-align: right;"> {{ AppHelper::NumberFormat($total_kredit) }} </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <a style="color: white;" href="{{ route('JurnalUmum.index') }}">
                        <div class="btn col-11">
                            <span> Kembali </span>
                        </div>
                    </a>
                    {{-- <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('JurnalUmum.print', $jurnal_umum->IDJU) }}" target="__blank">Print Data</a></span>
                    </div> --}}
                    @if ($jurnal_umum->Batal == 0)
                        <a style="color: white;" href="{{ route('JurnalUmum.show', $jurnal_umum->IDJU) }}">
                            <div class="btn col-1">
                                <span>Edit Data</span>
                            </div>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection