<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    table tr td {
        padding: 5px;
    }

    hr {
        margin : 5px;
        border-top : 1px solid #8f8f8f;
    }

    .pagination {
        margin: unset !important;
    }

    
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('JurnalUmum.index')}}">Jurnal Umum</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Jurnal Umum</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Jurnal Umum</span>
    </div>

    <form id="form-data">
        <div class="banner">
            {{ csrf_field() }}
            <table class="table-responsive" style="width: 100%;">
                <tr>
                    <td style="width: 50%">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control input-date-padding datepicker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="<?php echo date('d/m/Y'); ?>">
                    </td>
                    <td style="width: 50%">
                        <label for="Nomor">Nomor</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="" readonly>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="KodePerkiraan">Kode Perkiraan</label>
                        <select name="KodePerkiraan" id="KodePerkiraan" class="form-control select2">
                            <option value="">- Pilih -</option>
                            @foreach ($COA as $item)
                                <optgroup label="{{ $item->Nama_Group }}">
                                    @foreach ($item->data as $items)
                                        <option value="{{ $items->IDCoa }}">{{ $items->Kode_COA . ' - ' .$items->Nama_COA }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </td>
                    <td style="width: 50%">
                        <label for="Posisi_header">Posisi</label>
                        <select name="Posisi_header" id="Posisi_header" class="form-control select2">
                            <option value="debet">Debit</option>
                            <option value="kredit" selected>Kredit</option>
                        </select>
                    </td>
                </tr>
            </table>
            <hr>
            <table class="table-responsive" style="width: 100%;">
                <tr>
                    <td style="width: 50%">
                        <label for="IDCOA">Kode Perkiraan</label>
                        <select name="IDCOA" id="IDCOA" class="form-control select2 form-detail">
                            <option value="">- Pilih -</option>
                            @foreach ($COA as $item)
                                <optgroup label="{{ $item->Nama_Group }}">
                                    @foreach ($item->data as $items)
                                        <option value="{{ $items->IDCoa }}" data-kode="{{ $items->Kode_COA }}" data-nama="{{ $items->Nama_COA }}">{{ $items->Kode_COA . ' - ' .$items->Nama_COA }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </td>
                    <td style="width: 50%">
                        <label for="Debet" class="debet">Nominal Debet</label>
                        <input type="text" name="Debet" id="Debet" class="form-control debet harga-123 form-detail" placeholder="Masukkan nominal debet">
                        <label for="Kredit" class="kredit">Nominal Kredit</label>
                        <input type="text" name="Kredit" id="Kredit" class="form-control kredit harga-123 form-detail" placeholder="Masukkan nominal kredit">
                        <input type="hidden" name="Posisi" id="Posisi" value="debet">
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="IDMataUang">Mata Uang</label><br>
                        <select data-placeholder="Cari Kurs" class="form-control select2 form-detail" style="width: 55%" name="IDMataUang" id="IDMataUang">
                            <option value="">-- Pilih Mata Uang --</option>
                            @foreach ($mata_uang as $item)
                                <option value="{{ $item->IDMataUang }}" data-kurs="{{ $item->Kurs }}" > {{ $item->Mata_uang }} </option>
                            @endforeach
                        </select>
                        <input type="text" name="Kurs" id="Kurs" style="width: 40%; float: right;" readonly class="form-control form-detail" placeholder="Kurs">
                    </td>
                    <td>
                        <label for="Keterangan">Keterangan</label>
                        <textarea name="Keterangan" id="Keterangan" class="form-control form-detail"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <button class="btn btn-primary pull-right" type="button" id="btn-add-detail" name="btn-add-detail" style="float: center; margin-top: 10px;">Tambah</button>
                    </td>
                </tr>
            </table>
        </div>
        <div class="banner">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-bordered table-responsive" id="table-detail" style="font-size: 12px; margin-top: 10px; width: 100%;">
                        <thead style="background-color: #16305d; color: white">
                            <tr>
                                <th>No</th>
                                <th>Kode Perkiraan</th>
                                <th>Keterangan</th>
                                <th>Debet</th>
                                <th>Kredit</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot style="background-color: #16305d; color: white">
                            <tr>
                                <th colspan="3" style="text-align:right">Total Nominal</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                {{-- <div class="col-lg-12">
                    <div class="form-group">
                        <label for="">Balance</label>
                        <input type="text" class="form-control harga-123" value="0">
                    </div>
                </div> --}}
                <div class="col-lg-12" style="margin-bottom: 40px;" >
                    <button class="btn btn-primary pull-right" type="submit" id="Simpan" name="Simpan" style="float: center; margin-top: 10px;">Simpan</button>
                </div>
            </div>
        </div>
    </form>
</div>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("JurnalUmum.index") }}';
    var urlNumberInvoice    = '{{ route("JurnalUmum.number") }}';
    var urlInsert           = '{{ route("JurnalUmum.store") }}';
    var url                 = '{{ url("JurnalUmum") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/jurnalumum/jurnalumum_create.js') }}"></script>

@endsection