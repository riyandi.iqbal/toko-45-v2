<!-- ===========Create By TIAR 17-02-2020=============== -->
@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{route('JurnalUmum.index')}}">Jurnal Umum</a>
        </h2>
    </div>
    <div class="banner container" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="form_grid_3">
            <a href="{{route('JurnalUmum.create')}}">
                <div class="btn btn-primary hvr-icon-float-away">
                    <span style="color: white;">Tambah Data&nbsp;&nbsp;</span>
                </div>
            </a>
        </div>
        <br>
        <div class="col-md-2">
            <label class="field_title mt-dot2">Periode</label>
                <div class="form_input">
                    <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
                </div>
        </div>
        <div class="col-md-2">
            <label class="field_title mt-dot2"> S/D </label>
            <div class="form_input">
                <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">Cari Berdasarkan</label>
                <select data-placeholder="Cari Customer" class="chosen-select" name="field" id="field" required style="width: 100%">
                    <option value="">- Pilih -</option>
                    <option value="Nomor">Nomor</option>
                    <option value="Batal">Status</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">Keyword</label>
                <div id="pilihan">
                    <input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form_grid_2">
                <label>&nbsp;.</label>
                <br>
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>
        </div>
    </div>
    <div class="banner">
        <div class="widget_content">
            <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor</th>
                        <th>Debet</th>
                        <th>Kredit</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlDataTable = '{{ route("JurnalUmum.datatable") }}';
    var url = '{{ url("JurnalUmum") }}';
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/jurnalumum/jurnalumum_index.js') }}"></script>

@endsection