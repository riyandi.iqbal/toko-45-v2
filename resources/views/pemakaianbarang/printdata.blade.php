
<style type = "text/css">
      .printkertas {
         size: 8.5in 11in;  /* width height */
      }
  
</style>
<div class="main-grid">
	<div class="banner">
		<div class="printkertas">
			<div class="headerprint">
				<button class="btn btn-primary" id="printgo" onclick="printgo()">Print</button>
				<a href="{{url('PemakaianBarang')}}"><button class="btn btn-primary">Close</button></a>
			</div>			
			<div class="bodyprint">
				<br>
				<table width="90%">
					<tr>
						<td>
							<div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
							
						</td>
					</tr>
				</table>
				<hr>
				<br>
				<table width="90%">
					<tr>
						<td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="7">PEMAKAIAN BARANG</td>
					</tr>
					<tr>
						<td colspan="7">&nbsp;</td>
					</tr>
					<tr>
						<td>Nomor</td>
						<td>:</td>
						<td><?php echo $pakaiheader->Nomor; ?></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Tanggal</td>
						<td>:</td>
						<td><?php echo date("d M Y", strtotime($pakaiheader->Tanggal)); ?></td>
						<td>&nbsp;</td>
					</tr>
				</table>
				<br>
				<table width="90%" class="table table-bordered">
					<thead>
						<th>No</th>
						<th>Nama Barang</th>
						<th>Qty</th>
						<th>Satuan</th>
					</thead>
					<tbody>
						<?php if(empty($pakaidetail)){
	                      ?>
	                      <?php
	                    }else{
	                      $i=1;
	                      foreach ($pakaidetail as $data2) {
	                        ?>
	                        <tr class="odd gradeA">
	                          <td class="text-center"><?php echo $i; ?></td>
	                          <td><?php echo $data2->Nama_Barang; ?></td>
	                          <td class="text-center"><?php echo $data2->Qty; ?></td>
							  <td><?php echo $data2->Satuan; ?></td>	                           
	                        </tr>
	                        <?php
	                        $i++;
	                      }
	                    }
	                    ?>
					</tbody>
				</table>
				<br>
				<table width="90%">
					<tr>
						<td width="50%">Keterangan : </td>						
					</tr>
					<tr>
						<td colspan="2" width="50%"><?php echo $pakaiheader->Keterangan; ?></td>
					</tr>
				</table>
				
			</div>
		</div>		
	</div>	
</div>

</div>
<script src="{{ url ('newtemp/js/jquery2.0.3.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
	  window.print();
	});
	function printgo()
	{
		$('.headerprint').hide();
		$('.main-menu').hide();
		$('.title-bar').hide();
		window.print();
		location.reload();
	}
</script>
