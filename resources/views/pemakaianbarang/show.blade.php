<!-- ===========Create By Dedy 11-02-2020=============== -->
@extends('layouts.app')
@section('content')

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('PemakaianBarang')}}">Data Pemakaian Barang</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Pemakaian Barang - <b><?php echo $pakaiheader->Nomor ?></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
                    <div class="form_container left_label">
                        <table class="table cell-border" width="100%" style="font-size: 12px;">
                            <thead style="display: none;">
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                                    <td width="65%" style="background-color: #e5eff0; border: 1px solid; "><?php echo date("d M Y", strtotime( $pakaiheader->Tanggal) )?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo $pakaiheader->Nomor?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #e5eff0; border: 1px solid;">Dibuat Oleh</td>
                                    <td style="background-color: #e5eff0; border: 1px solid;"><?php echo $pakaiheader->name ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                                    <td style="background-color: #ffffff; border: 1px solid;"><?php echo $pakaiheader->Keterangan?></td>
                                </tr>
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                        <br><br>
                                 <div class="widget_content">

                <!-- <table class="display data_tbl">
                 --><table id="tblshowpo" class="table cell-border" width="100%" style="font-size: 12px;">
                  <thead style="background-color: #16305d; color: #fff">
                    <tr>
                      <th>No</th>
                      <th>Nama Barang</th>
                      <th>Qty</th>
                      <th>Satuan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if(empty($pakaidetail)){
                      ?>
                      <?php
                    }else{
                      $i=1;
                      foreach ($pakaidetail as $data2) {
                        ?>
                        <tr class="odd gradeA">
                          <td class="text-center"><?php echo $i; ?></td>
                          <td><?php echo $data2->Nama_Barang; ?></td>
                          <td class="text-center"><?php echo number_format($data2->Qty); ?></td>
                          <td><?php echo $data2->Satuan; ?></td>
                        </tr>
                        <?php
                        $i++;
                      }
                    }
                    ?>

                  </tbody>
                </table>
              </div>
                        <div class="widget_content py-4 text-center">
                            <div class="form_grid_12">
                                <div class="btn col-11 hvr-icon-back">
                                    <span> <a style="color: white;" href="{{url('PemakaianBarang')}}" name="simpan">Kembali</a></span>
                                </div>
                                <div class="btn col-3 hvr-icon-spin">
                                  <span><a style="color: white;" href="{{url('PemakaianBarang')}}/printdata/<?php echo $pakaiheader->IDPakai ?>" target="__blank">Print Data</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblshowpo').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false
        });


    })
</script>
@endsection
