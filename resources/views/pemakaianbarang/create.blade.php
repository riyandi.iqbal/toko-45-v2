<!-- ===========Create By Dedy 31-01-2020=============== -->
@extends('layouts.app')   
@section('content')

<?php
if ($kode->curr_number == null) {
  $number = 1;
} else {
  $number = $kode->curr_number + 1;
}
$kodemax = str_pad($number, 4, "0", STR_PAD_LEFT);
$array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
$code = $kodemax.'/'.$array_bulan[date('n')].'/'.date('y');
?>

<style type="text/css">
	body {
	background: #f1f1f1;
}



@media screen and (max-width:768px) {
	.table-responsive {
		overflow: auto;
	}
}
</style>
<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{url('PemakaianBarang')}}">Data Pemakaian Barang</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Pemakaian Barang</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Pemakaian Barang</span>
    </div>
	<div class="banner">
		<form class="form-horizontal" action="{{url('PemakaianBarang/simpandata')}}" method="post" >
			{{ csrf_field() }}
			<table class="table table-responsive">
			<tr>
				<td>
					<label>Tanggal</label>
					<input type="date" class="form-control input-date-padding date-picker hidden-sm-down" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required oninvalid="this.setCustomValidity('Tanggal Tidak Boleh Kosong')" oninput="setCustomValidity('')" value="<?php echo date('Y-m-d'); ?>">
					<br>
					<label>Nomor Pemakaian Barang</label>
					<input type="text" name="Nomor" id="Nomor" class="form-control" readonly value="<?php echo $code ?>">
					<br>
					<label>Gudang</label>
					<select name="IDGudang" id="IDGudang" class="form-control select2" style="width: 100%;">
						<option value="">- Pilih Gudang -</option>
						@foreach ($gudang as $item)
							<option value="{{ $item->IDGudang }}"> {{ $item->Nama_Gudang }} </option>
						@endforeach
					</select>
                </td>
				<td>
                    <label>Dibuat Oleh</label>
		        	<input type="text" name="usernama" id="usernama" class="form-control" value="<?php echo $user['name'] ?>" readonly>
                    <input type="hidden" name="userid" id="userid" value="<?php echo $user['id'] ?>">
					</br>
					<label>Keterangan</label>
					<textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5" required oninvalid="this.setCustomValidity('Tidak Boleh Kosong')" oninput="setCustomValidity('')"></textarea>
				</td>
			</tr>
		</table>
		<!--<br>-->
		<div class="container" style="width: 100%">
			<div id="content">
		        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
		            <li class="active"><a href="#tab-1" data-toggle="tab">Input Data Pemakaian Barang</a></li>
		            <!-- <li><a href="#tab-2" data-toggle="tab">Pembayaran</a></li> -->
				
		        </ul>
				<br>
		        <div id="my-tab-content" class="tab-content">
		            <div class="tab-pane active" id="tab-1">
		            	
		            	<div class="container" style="width: 100%">
		            		<br>
								<table class="table table-bordered" id="tabelfsdfsf" style="font-size: 12px;">
		                        
			                        <thead style="background-color: #16305d; color: white">
			                          <tr>
			                            <th style="width: 25%">Nama Barang</th>
			                            <th style="width: 10%">Qty</th>
			                            <th style="width: 10%">Satuan</th>
			                            <th style="width: 2%">Action</th>
			                          </tr>
			                        </thead>
			                        <tbody id="tabel-cart-po">

			                        </tbody>
			                    </table>
			                    <a class="btn col-11" id="buttonadd" onclick="addmore()" /><i class="col-11 hvr-icon-float-away">Tambah</i></a>
		                    <br> 
		            	</div>
		            </div>
		    	</div>
				<button onclick="val_submit()" class="btn btn-info" id="simpan" name="simpan" style="float: right;">Simpan</button>
			</div>
		</div>
		</form>
	</div>
	</div>
    <br><br><br>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/pemakaianbarang/pemakaianbarang_create.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#Baranga').editableSelect({
			effects: 'fade'
		});
		addmore();
		
	})

	var databarang = JSON.parse(`<?php echo json_encode($barang); ?>`);
	var datasatuan = JSON.parse(`<?php echo json_encode($satuan); ?>`);
	var token = "{{ csrf_token() }}";
	var amdetail = "{{url('PenerimaanBarangJadi/ambildetail')}}";
	var gansatuan = "{{url('Po/getsatuan')}}";
	
	function val_submit(){
		
		var ket = document.getElementById("Keterangan").value;
		
		if (ket !="") {
			$("#modal-loading").fadeIn();
			return true;
		}else{
			alert('Anda harus mengisi data dengan lengkap !');
		}
	}

</script>
@endsection