
<!-- ===========Create By Dedy 31-01-2020=============== -->
@extends('layouts.app')
@section('content')

<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
  </style>
<?php
    $hari_ini       = date("Y-m-d");
    $tgl_terakhir   = date('Y-m-t', strtotime($hari_ini));
?>

<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('PemakaianBarang')}}">Pemakaian Barang</a>
        </h2>
    </div>
    <br>

    <div class="banner container">
            <div class="form_grid_3">
            <a href="{{url('PemakaianBarang/create')}}">
                <div class="btn btn-primary hvr-icon-float-away">
                    <span style="color: white;">Tambah Data&nbsp;&nbsp;&nbsp;</span>
                </div>
            </a>
            </div>
            <br>
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                <div class="form_input">
                    <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
                </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
                </div>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select name="field" id="field" data-placeholder="" style="width: 100%!important" class="form-control" tabindex="13">
                    <option value="">Cari Berdasarkan</option>
                    <option value="Nomor">Nomor</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>    
                </div>
            </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <table id="table-data" class="display" width="100%" style="background-color: #254283; font-size: 12px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nomor</th>
                        <th>Dibuat Oleh</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
        <br><br><br>
    </div>
</div>
<?php
if (isset($pakai)){ foreach($pakai as $data){ ?>
            <div id="modalBatal<?php echo $data->IDPakai?>" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Peringatan!</h4>
                  </div>
                  <div class="modal-body">
                    <p>Yakin Akan Menghapus Data ?!</p>
                    <input type="hidden" name="idcari" value="<?php echo $data->IDPakai?>">
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <div class="btn hvr-icon-shrink col-10">
                        <a style="color: white;" href="PemakaianBarang/hapus_data/<?php echo $data->IDPakai ?>"><span>Hapus</span></a>
                    </div>
                  </div>
                </div>

              </div>
            </div>
    <?php } } ?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlData = '{{ route("PemakaianBarang.datatable") }}';
    var url                 = '{{ url("PemakaianBarang") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/pemakaianbarang/index.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tablepb').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false
        });
         @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif

        @if (session('alert2'))
            swal("Berhasil", "{{ session('alert2') }}", "success");
        @endif
    })
</script>

@endsection
