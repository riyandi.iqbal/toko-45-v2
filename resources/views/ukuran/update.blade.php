<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Ukuran')}}">Data Ukuran</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Edit Data Ukuran</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Edit Data Ukuran <b><?php echo $ukuran->Nama_Ukuran; ?></b></span>
    </div>
    <div class="banner">
      <form id="form-data">
    	<div class="container">
    	<br><br>
        <div class="col-md-3">
          <label class="judul">Kode Ukuran</label>
        </div>
        <div class="col-md-9">
          <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Kode_Ukuran" id="Kode_Ukuran" value="<?php echo $ukuran->Kode_Ukuran ?>" readonly>
          <br><br>
        </div>
    		<div class="col-md-3">
    			<label class="judul">Nama Ukuran</label>
    		</div>
    		<div class="col-md-9">
          <input type="hidden" name="IDUkuran" id="IDUkuran" value="<?php echo $ukuran->IDUkuran; ?>">
    			<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama_Ukuran" id="Nama_Ukuran" value="<?php echo $ukuran->Nama_Ukuran; ?>">
    			<br><br>
    		</div>
    		<div class="col-md-3">
    			<label class="judul">Status</label>
    		</div>
    		<div class="col-md-9">
    			<input type="radio" name="Status" id="Status" value="aktif" {{ ($ukuran->Status == 'aktif') ? 'checked' : '' }}>Aktif &nbsp;
    			<input type="radio" name="Status" id="Status" value="nonaktif" {{ ($ukuran->Status == 'nonaktif') ? 'checked' : '' }}>Non Aktif
    		</div>
    	</div>
    	<div class="text-center">
        <div class="btn col-11 hvr-icon-back">
          <span> <a style="color: white;" href="{{url('Tipe')}}" name="simpan">Kembali</a></span>
        </div>
        <div class="btn">
            <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
        </div>
      </div>
    </form>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Ukuran") }}';
  var urlUpdate           = '{{ route("Ukuran.update_data") }}';
  var url                 = '{{ url("Ukuran") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/ukuran/update.js') }}"></script>
@endsection