<!-- ===========Create By Dedy 12-12-2019=============== -->
@extends('layouts.app')   
@section('content')
<?php
if ($kode->curr_number == null) {
  $number = 1;
} else {
  $number = $kode->curr_number + 1;
}

$kodemax = str_pad($number, 2, "0", STR_PAD_LEFT);
$code = 'UK-'.$kodemax;
?>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Ukuran')}}">Data Ukuran</a>
                <i class="fa fa-angle-right"></i>
                <a href="#">Tambah Data Ukuran</a>
        </h2>
    </div>
    <br>
    <div class="banner text-center" style="font-size: 14px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Ukuran</span>
    </div>
    <div class="banner">
      <form id="form-data">
    	<div class="container">
    	<br><br>
        <div class="col-md-3">
          <label class="judul">Kode Ukuran</label>
        </div>
        <div class="col-md-9">
          <input type="text" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Kode_Ukuran" id="Kode_Ukuran" value="<?php echo $code ?>" readonly>
          <br><br>
        </div>
    		<div class="col-md-3">
    			<label class="judul">Nama Ukuran</label>
    		</div>
    		<div class="col-md-9">
    			<input type="tex" class="form-control" onkeyup="javascript:this.value=this.value.toUpperCase();" name="Nama_Ukuran" id="Nama_Ukuran">
    			<br><br>
    		</div>
    		<div class="col-md-3">
    			<label class="judul">Status</label>
    		</div>
    		<div class="col-md-9">
    			<input type="radio" name="Status" id="Status" value="aktif" checked>Aktif &nbsp;
    			<input type="radio" name="Status" id="Status" value="nonaktif">Non Aktif
    		</div>
    	</div>
    	<div class="text-center">
    		<div class="btn col-11 hvr-icon-back">
          <span> <a style="color: white;" href="{{url('Ukuran')}}" name="simpan">Kembali</a></span>
        </div>
        <div class="btn">
            <button type="submit" class="btn btn-success hvr-icon-float-away">Simpan</button>
        </div>
    	</div>
    </div>
  </form>
</div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
  var urlIndex            = '{{ url("Ukuran") }}';
  var urlInsert           = '{{ route("Ukuran.store") }}';
  var url                 = '{{ url("Ukuran") }}';
</script>

<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/ukuran/create.js') }}"></script>
@endsection