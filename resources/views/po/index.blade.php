<!-- ===========Create By Dedy 13-12-2019=============== -->
@extends('layouts.app')
@section('content')
<style>
    span a .fa {
        cursor: pointer;
        padding: 5px;
    }
</style>
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Po')}}">Data Pesanan Pembelian</a>
        </h2>
    </div>
<!-- ======================= -->
<?php $hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)); ?>
    <br>
    <div class="banner container">
        <div class="form_grid_3">
        <!-- <a href="{{url('Po/create')}}"> -->
            <div class="btn btn-primary hvr-icon-float-away">
                <span style="color: white;" onclick="cekaksestambah()">Tambah Data&nbsp;&nbsp;&nbsp;</span>
            </div>
        <!-- </a> -->
        </div>
        <br>
        <div class="col-md-2">
            <label class="field_title mt-dot2">Periode</label>
                <div class="form_input">
                    <input type="text" class="form-control datepicker" name="date_from" id="date_from" value="">
                </div>
        </div>
        <div class="col-md-2">
            <label class="field_title mt-dot2"> S/D </label>
            <div class="form_input">
                <input type="text" class="form-control datepicker" name="date_until" id="date_until" value="{{ date('t/m/Y', strtotime(date('Y-m-d'))) }}">
            </div>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">Cari Berdasarkan</label>
                <select data-placeholder="Cari Customer" class="chosen-select" name="field" id="field" required style="width: 100%">
                    <option value="">- Pilih -</option>
                    <option value="tbl_purchase_order.Nomor">Nomor PO</option>
                    <option value="tbl_supplier.Nama">Customer</option>
                    <option value="tbl_purchase_order.Batal">Status</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form_grid_2">
                <label class="field_title mt-dot2">Keyword</label>
                <div id="pilihan">
                    <input name="keyword" id="keyword" type="text" placeholder="Masukkan Keyword" class="form-control">
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form_grid_2">
                <label>&nbsp;.</label>
                <br>
                <button id="cari-data" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
            </div>
        </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="bannerbody">
        <div class="widget_content">
            <table id="table-data" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px; margin-top: 5px;">
                <thead style="color: #fff">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th data-data="Nomor">Nomor Pesanan</th>
                        <th data-data="Nama">Nama Supplier</th>
                        <th>Total Qty</th>
                        <th>Total Harga</th>
                        <th>Status</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tfoot style="color: #fff">
                    <tr>
                        <th colspan="4" style="text-align: right;">
                            Total
                        </th>
                        <th style="text-align: right;"></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">

    function cekaksestambah()
    {
        $("#modal-loading").fadeIn();
        $.ajax({
                  url : "{{url('Permission/tambah')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}"},
                  dataType:'json',
                success: function(data){
                $("#modal-loading").fadeOut();
                        
                        if(data==null)
                        {
                            swal('Perhatian', 'Anda Tidak Memiliki Akses Tambah Data', 'warning');
                        }else if(data['Tambah']=='yes')
                        {
                            window.location = "{{url('Po/create')}}";
                        }else{
                            swal('Perhatian', 'Anda Tidak Memiliki Akses Tambah Data', 'warning');
                        }

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    $("#modal-loading").fadeOut();
                    swal('Perhatian', 'Anda Tidak Memiliki Akses Tambah Data', 'warning');
                 }
            });
    }
</script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>
<script>
    var urlData = '{{ route("Po.datatable") }}';
    var url = '{{ url("Po") }}';
    var angkakoma = <?php echo isset($coreset) ? $coreset->Angkakoma : 0 ?>;
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/po/po_index.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('alertakses'))
            swal("Perhatian", "{{ session('alertakses') }}", "warning");
        @endif

        @if (session('alert'))
            swal("Berhasil", "{{ session('alert') }}", "success");
        @endif
    })
</script>
@endsection
