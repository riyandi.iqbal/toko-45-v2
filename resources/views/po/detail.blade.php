<!-- ===========Create By Tiar 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
$angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('Po')}}">Data Pesanan Pembelian</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Pesanan Pembelian - {{ $purchase_order->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table cell-border" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" style="background-color: #e5eff0; border: 1px solid;">Tanggal</td>
                            <td width="65%" style="background-color: #e5eff0; border: 1px solid; "> {{ $purchase_order->Tanggal }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Nomor</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $purchase_order->Nomor }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Customer</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ $purchase_order->Nama }} </td>
                        </tr>

                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Keterangan</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $purchase_order->Keterangan }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Status</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($purchase_order->Batal == 0 || $purchase_order->Batal == null ) ? 'Aktif' : 'Batal'  }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Total Qty</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $purchase_order->Total_qty }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #e5eff0; border: 1px solid;">Total Harga</td>
                            <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($purchase_order->Grand_total, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td style="background-color: #ffffff; border: 1px solid;">Mata Uang</td>
                            <td style="background-color: #ffffff; border: 1px solid;"> {{ $purchase_order->Mata_uang }} </td>
                        </tr>
                        @if ($purchase_order->pilihanppn == 'exclude')
                            {{-- <tr>
                                <td style="background-color: #e5eff0; border: 1px solid;">Status PPN</td>
                                <td style="background-color: #e5eff0; border: 1px solid;"> {{ ($purchase_order->Status_ppn) }} </td>
                            </tr> --}}
                            <tr>
                                <td style="background-color: #e5eff0; border: 1px solid;">Nilai PPN</td>
                                <td style="background-color: #e5eff0; border: 1px solid;"> {{ AppHelper::NumberFormat($purchase_order->Grand_total - ($purchase_order->Grand_total / 1.1), $angkakoma) }} </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>PPN % </th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($purchase_order_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                <td style="text-align: center;"> {{ ($item->Jenis_ppn == '1') ? 0 : 10 }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Saldo, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot style="background-color: #16305d; color: #fff">
                        <tr>
                            <th colspan="6" style="text-align: right;">Jenis Pembayaran</th>
                            <th>{{ $um ?  strtoupper($um->Jenis_pembayaran ) : '-' }}</th>
                        </tr>
                        <tr>
                            <th colspan="6" style="text-align: right;">Pembayaran Via</th>
                            <th>{{ $um ?  strtoupper($um->Nama_COA ) : '-' }}</th>
                        </tr>
                        <tr>
                            <th colspan="6" style="text-align: right;">Nilai UM</th>
                            <th>{{ $um ? AppHelper::NumberFormat($um->Nominal) : '-' }}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('Po.index') }}" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('Po.print', $purchase_order->IDPO) }}" target="__blank">Print Data</a></span>
                    </div>
                    <div class="btn col-1">
                      <span><a style="color: white;" href="{{ route('Po.show', $purchase_order->IDPO) }}">Edit Data</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection