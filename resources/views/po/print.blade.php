
<style type = "text/css">
    .printkertas {
       size: 8.5in 11in;  /* width height */
    }

</style>
<?php
    use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>
<div class="main-grid">
    <div class="banner">
        <div class="headerprint">
            <button class="btn btn-primary" id="printgo" onclick="print_d('printkertas')">Print</button>
            <a href="{{route('Po.index')}}"><button class="btn btn-primary">Close</button></a>
        </div>
        <div class="printkertas" id="printkertas">
            <div class="bodyprint" id="bodyprint">
                <br>
                <table width="90%">
                    <tr>
                        <td>
                            <div style="width: 50%">
								<img src="{{URL::to('/')}}/newtemp/<?php echo $perusahaan->Logo_head ?>" style="width: 50%; margin-bottom: 10px">
								<h5><?php echo $perusahaan->Alamat ?><br>
								<?php echo $perusahaan->Nama_Kota ?>, <?php echo $perusahaan->Provinsi ?><br>
								Tlp. <?php echo $perusahaan->Telp ?></h5>
							</div>
                      </td>
                    </tr>
                </table>
                <hr>
                <br>
                <table width="90%">
                    <tr>
                        <td style="text-align: center; font-size: 20px; font-weight: bold;" colspan="7">SALES ORDER</td>
                    </tr>
                    <tr>
                        <td colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>:</td>
                        <td><?php echo $purchase_order->Nomor; ?></td>
                        <td>&nbsp;</td>
                        <td>Grand Total</td>
                        <td>:</td>
                        <td style="text-align: right;"><?php echo $kurs->Kode . AppHelper::NumberFormat($purchase_order->Grand_total, $angkakoma); ?></td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td><?php echo date("d M Y", strtotime($purchase_order->Tanggal)); ?></td>
                        <td>&nbsp;</td>
                        <td>Total Qty</td>
                        <td>:</td>
                        <td style="text-align: right;"><?php echo $purchase_order->Total_qty; ?></td>
                    </tr>
                    <tr>
                        <td>Mata Uang</td>
                        <td>:</td>
                        <td><?php echo $purchase_order->Mata_uang; ?></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br>
                <table width="90%" class="table table-bordered">
                        <thead>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>Subtotal</th>
                        </thead>
                        <tbody>
                            @foreach ($purchase_order_detail as $key => $item)
                                <tr>
                                    <td> {{ ++$key }} </td>
                                    <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                    <td style="text-align: center;"> {{ $item->Qty }} </td>
                                    <td style="text-align: right;"> {{ $kurs->Kode . AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                    <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                    <td style="text-align: right;"> {{ $kurs->Kode . AppHelper::NumberFormat($item->Saldo, $angkakoma) }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                </table>
                <table width="90%">
                    <tr>
                        <td width="50%">Keterangan : </td>						
                        <td>&nbsp;</td>
                        <td>Jenis Pembayaran</td>
                        <td>:</td>
                        <td>{{ $um ?  strtoupper($um->Jenis_pembayaran ) : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">{{ $purchase_order->Keterangan }}</td>
                        <td>Nama COA</td>
                        <td>:</td>
                        <td>{{ $um ? strtoupper($um->Nama_COA ) : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">&nbsp;</td>
                        <td>Jumlah Pembayaran</td>
                        <td>:</td>
                        <td>{{ $um ? $kurs->Kode . AppHelper::NumberFormat($um->Nominal, $angkakoma) : '' }}</td>
                    </tr>
                </table>
            </div>
        </div>		
    </div>	
</div>

<script>
    window.load = print_d('printkertas')
    function print_d(elem){
        var mywindow = window.open('', 'PRINT', 'height=600,width=800');

        mywindow.document.write('<html><head><title>' + document.title  + '</title>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(document.getElementById(elem).innerHTML);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10*/

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>