<!-- ===========Create By Dedy 01-01-2019=============== -->
@extends('layouts.app')
@section('content')

<style type="text/css">
/*  .table_top{
        display: none !important;
        }*/
</style>
<!-- tittle data -->
<div class="page_title">
    
    <span class="title_icon"><span class="blocks_images"></span></span>
   
    <div class="top_search">

    </div>
</div>
<!-- ======================= -->
<?php
$hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini));
?>
<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
            <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
                <i class="fa fa-angle-right"></i>
                <a href="{{url('Po')}}">Data Purchase Order</a>
        </h2>
    </div>
    <?php $hari_ini = date("Y-m-d");
$tgl_terakhir = date('Y-m-t', strtotime($hari_ini)); ?>
    <br>
    <div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
      <span>Data Purchase Order</span>
    </div>
    <br>
    <div class="banner container">
            <div class="col-md-2">
                <label class="field_title mt-dot2">Periode</label>
                    <div class="form_input">
                        <input type="date" class="form-control" name="date_from" id="date_from" value="<?php echo date('Y-m-d') ?>">
                    </div>
            </div>
            <div class="col-md-2">
                <label class="field_title mt-dot2"> S/D </label>
                <div class="form_input">
                    <input type="date" class="form-control" name="date_until" id="date_until" value="<?php echo $tgl_terakhir ?>">
                </div>
            </div>
            <div class="col-md-3">
                <label class="field_title mt-dot2">.</label>
                <select name="jenispencarian" id="jenispencarian" data-placeholder="No PO" style="width: 100%!important" class="form-control" tabindex="13">
                    <option value="">Cari Berdasarkan</option>
                    <option value="Nomor">No PO</option>
                    <option value="Nama">Supplier</option>
                    <option value="Nama_Barang">Barang</option>
                </select>
            </div>
            <div class="col-md-3">
                <div class="form_grid_2">
                    <label class="field_title mt-dot2">.</label>
                    <input name="keyword" id="keyword" type="tex" placeholder="Cari Berdasarkan" class="form-control">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form_grid_2">
                    <label>&nbsp;.</label>
                    <br>
                    <button onclick="caridata()" class="btn btn-primary"><i class="fa fa-search"></i>&nbsp;Search</button>
                    
                </div>
            </div>
        <!-- </form> -->
    </div>
    <br>
    <div class="banner">
            <div class="widget_content">
            <button onclick="exlin()" class="btn btn-success"><i class="fa fa-file-excel-o"></i>&nbsp;EXCEL</button>
            <br><br>
            <!-- <table class="display" id="action_tbl"> -->
                <table id="tblpo" class="table cell-border" width="100%" style="background-color: #254283; font-size: 12px;">
                    <thead style="color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Nomor PO</th>
                            <th>Nama Supplier</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px solid;" id="previewdata">
                        <?php if (empty($po)) { ?>
                        <?php } else {
                            $i = 1;
                                foreach ($po as $data) {
                        ?>
                        <tr class="odd gradeA">
                            <td> <center><?php echo $i; ?></center></td>
                            <td><?php echo date("d M Y", strtotime($data->Tanggal)); ?></td>
                            <td><?php echo $data->Nomor; ?></td>
                            <td><?php echo $data->Nama; ?></td>
                            <td><?php echo $data->Nama_Barang; ?></td>
                            <td style="text-align: right;"><?php echo number_format($data->jumlah_qty); ?></td>
                            <td style="text-align: right;"><?php echo $data->Mata_uang ?>. <?php echo number_format($data->harga_satuan, $coreset->Angkakoma, '.', ','); ?></td>
                        </tr>
                                <?php
                                        $i++;
                                    }
                                }
                                ?>
                    </tbody>
                </table>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#tblpo').DataTable({
            "searching": false,
            "info": false,
            "ordering": true,
            "lengthChange": false            
        });
        //caridata();
    })

    var PM  = [];
    function caridata()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

        $.ajax({
                  url : "{{url('Po/laporan_po2')}}",
                  type: "POST",
                  data:{"_token": "{{ csrf_token() }}",date1:date1, date2:date2, jenis:jenis, keyword:keyword},
                  dataType:'json',
                success: function(data){
                    var html = '';
                      if (data <= 0) {
                        console.log('-----------data kosong------------');
                        $("#previewdata").html("");
                        var value =
                          "<tr>" +
                          "<td colspan=7 class='text-center'>DATA KOSONG</td>"+
                          "</tr>";
                        $("#previewdata").append(value);
                        swal('Perhatian', 'Data Sesuai Pencarian Tidak Ada', 'warning');
                      } else {
                       console.log(data);
                     $("#previewdata").html("");
                      for(i = 0; i<data.length;i++){
                      PM[i] = data[i];

                      var value =
                      "<tr>" +
                      "<td>"+(i+1)+"</td>"+
                      "<td>"+formatDate(PM[i].Tanggal)+"</td>"+
                      "<td>"+PM[i].Nomor+"</td>"+
                      "<td>"+PM[i].Nama+"</td>"+
                      "<td>"+PM[i].Nama_Barang+"</td>"+
                      "<td>"+PM[i].jumlah_qty+"</td>"+
                      "<td>"+PM[i].harga_satuan+"</td>"+
                      "</tr>";
                      $("#previewdata").append(value);
                      }
                      
                     }
                }
            });
    }

    function exlin()
    {
        var date1 = $('#date_from').val();
        var date2 = $('#date_until').val();
        var jenis = $('#jenispencarian').val();
        var keyword = $('#keyword').val();
        console.log(date1 , date2, jenis, keyword);

       window.location.replace("{{url('Po/toexcel/?date1=')}}"+date1+'&date2='+date2+'&jenis='+jenis+'&keyword='+keyword);
    }

</script>
@endsection
