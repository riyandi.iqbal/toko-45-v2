<!-- ===========Create By Tiar 13-12-2019=============== -->
@extends('layouts.app')
@section('content')

<div class="main-grid">
	<div class="banner">
	    <h2>
	        <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
	            <i class="fa fa-angle-right"></i>
	            <a href="{{route('InvoicePenjualan.index')}}">Data Invoice Penjualan</a>
	            <i class="fa fa-angle-right"></i>
	        <span>Tambah Invoice Penjualan</span>
	    </h2>
	</div>
	<br>
	<div class="banner text-center" style="font-size: 20px; font-weight: bold;color: #fff;background-color: #254283;padding: 10px; height: 50px;">
    	<span>Tambah Data Invoice Penjualan</span>
    </div>

    <div class="banner">
        <form id="form-data">
            {{ csrf_field() }}
            <table class="table table-responsive" style="margin-bottom: 0px;">
                <tr>
                    <td style="width: 35%;">
                        <label for="tanggal">Tanggal</label>
                        <input type="text" class="form-control form-white datepicker" id="Tanggal"  name="Tanggal"  placeholder="Tanggal" required value="{{ date('d/m/Y') }}">
                        <br>
                        <label for="Nomor">Nomor Invoice</label>
                        <input type="text" name="Nomor" id="Nomor" class="form-control" placeholder="No Invoice Penjualan" readonly>
                        <br>
                        <label for="IDSJC">Nomor Surat Jalan</label>
                        <select name="IDSJC" id="IDSJC" class="form-control select2" style="width: 100%;">
                            <option value="">-- Pilih Nomor Surat Jalan --</option>
                            @foreach ($surat_jalan_customer as $item)
                                <option value="{{ $item->idsjc }}"> {{ $item->nomor . ' - ' . $item->Nama }} </option>
                            @endforeach
                        </select>
                    </td>
                    <td style="width: 35%;">
                        <div>
                            <label for="Jatuh_tempo">Jatuh Tempo</label><br>
                            <input type="text" name="Jatuh_tempo" id="Jatuh_tempo" class="form-control form-white" style="width: 40%;">
                        </div>
                        <br>
                        <label for="Tanggal_jatuh_tempo">Tanggal Jatuh Tempo</label><br>
                        <input type="text" name="Tanggal_jatuh_tempo" id="Tanggal_jatuh_tempo" class="form-control form-white">
                        <br>
                        <label for="Status_ppn">Status PPN</label><br>
                        <input type="text" name="Status_ppn" id="Status_ppn" class="form-control" readonly>
                        <br>
                    </td>
                    <td style="width: 30%;">
                        <label for="IDCustomer">Customer</label><br>
                        <input type="text" name="NamaCustomer" id="NamaCustomer" class="form-control" readonly>
                        <input type="hidden" name="IDCustomer" id="IDCustomer">
                        <br>
                        <label for="Nama_di_faktur">Nama Di Faktur</label><br>
                        <input type="text" name="Nama_di_faktur" id="Nama_di_faktur" class="form-control form-white">
                        <br>
                        <label for="Total_qty">Total Qty</label>
                        <input type="text" name="Total_qty" id="Total_qty" class="form-control" placeholder="Total Qty" readonly>
                        <br>   
                    </td>
                </tr>
            </table>
            <table style="width: 100%;">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="width: 20%">
                        Total Invoice <br>
                        <input type="text" class="form-control sisa-total-invoice" style="width: 100%; float:right;" readonly>
                    </td>
                </tr>
            </table>
            <br>
            <!--<br>-->
            <div class="container" style="width: 100%; margin-bottom: 50px;">
                <div id="content">
                    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs" style="float: right;margin-right: 15px;">
                        <li class="active"><a href="#tab-barang" data-toggle="tab">Input Barang</a></li>
                        <li><a href="#tab-pembayaran" data-toggle="tab">Pembayaran</a></li>
                    </ul>
                </div>
                <div id="my-tab-content" class="tab-content">
                    <div class="tab-pane active" id="tab-barang">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Barang</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <table class="table table-bordered table-responsive" id="table-barang" style="font-size: 12px; margin-top: 10px; width: 100%;">
                                <thead style="background-color: #16305d; color: white">
                                    <tr>
                                        <th>No</th>
                                        <th style="width: 45%">Nama Barang</th>
                                        <th style="width: 10%">Qty</th>
                                        <th style="width: 15%">Harga</th>
                                        <th style="width: 15%">Satuan</th>
                                        <th style="width: 15%">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody id="list-barang">
        
                                </tbody>
                            </table>
        
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width: 50%; vertical-align: top;">
                                        <label for="Keterangan">Keterangan</label>
                                            <textarea name="Keterangan" id="Keterangan"  rows="9" tabindex="5"></textarea>
                                    </td>
                                    <td style="padding-left: 10px;">
                                        <label style="float: left;" id="juduldpp">DPP</label>
                                        <input type="text" style="width: 70%" id="DPP" name="DPP" class="form-control text-right total-add" placeholder="DPP" readonly>
                                        <br class="ppn"><br class="ppn"><br class="ppn">
                                        <label style="float: left;" id="judulppn" class="ppn">PPN</label>
                                        <input type="text" style="width: 70%" id="PPN" name="PPN" class="form-control text-right ppn total-add" placeholder="PPN" readonly>
                                        <br><br><br>
                                        <label style="float: left;" for="total_invoice">Sub Total Invoice</label>
                                        <input type="text" style="width: 70%" id="total_invoice" name="total_invoice" class="form-control text-right" placeholder="Sub Total Invoice" readonly>
                                        <?php if($namauser=='Administrator'){ ?>
                                        <br><br><br>
                                            <label style="float: left;">Diskon %</label>
                                            <input type="text" style="width: 70%" id="diskonpersen" name="diskonpersen" class="form-control text-right form-white">
                                            <br><br><br>
                                            <label style="float: left;">Diskon Rupiah</label>
                                            <input type="text" style="width: 70%" id="diskonrupiah" name="diskonrupiah" class="form-control text-right form-white harga-123 total-min">
                                            <br><br><br>
                                        <?php } ?>
                                        <label style="float: left;" for="UMCustomer">DP</label>
                                        <input type="text" style="width: 70%" id="UMCustomer" name="UMCustomer" class="form-control text-right" placeholder="DP" readonly>
                                        <br><br><br>
                                        <label style="float: left;" for="total_invoice_diskon_view">Total Invoice</label>
                                        <input type="text" style="width: 70%" id="total_invoice_diskon_view" name="total_invoice_diskon_view" class="form-control text-right sisa-total-invoice" placeholder="Total Invoice" readonly>

                                        <input type="hidden" style="width: 70%" id="total_invoice_diskon" name="total_invoice_diskon" class="form-control text-right total-invoice" placeholder="Total Invoice" readonly>                                    </td>
                                </tr>
                            </table>

                            <div class="btn col-16">
                                <span> <a style="color: white; float: right;" href="{{ route('InvoicePenjualan.index') }}">Kembali</a></span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-pembayaran">
                        <div class="form-title">
                            <span><i class="fa fa-th-list"></i>&nbsp;Pembayaran</span>	
                        </div> 
                        <div class="container" style="width: 100%">
                            <br>
                            <div class="col-md-6">
                                <label>Jenis Pembayaran</label>
                                <select name="Jenis_Pembayaran" id="Jenis_Pembayaran" class="form-control">
                                    <option value="">-- Pilih Jenis Pembayaran --</option>
                                    <?php foreach ($cara_bayar as $cb) { ?>
										<option value="<?php echo strtolower($cb->Nama) ?>"><?php echo $cb->Nama ?></option>
									<?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nama COA</label>
                                <select name="IDCoa" id="IDCoa" class="form-control">
                                    <option value="">--Silahkan Pilih Jenis Pembayaran Dahulu--</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label>Nomor Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control" name="Nomor_giro" id="Nomor_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Tanggal Giro</label>
                                <input type="text" class="input-date-padding-3-2 form-control datepicker" name="Tanggal_giro" id="Tanggal_giro" disabled>
                            </div>
                            <div class="col-md-6">
                                <label>Jumlah Pembayaran</label>
                                <input type="text" class="input-date-padding-3-2 form-control harga-123 form-white" name="pembayaran" id="pembayaran" >
                            </div>
                            <div class="col-md-12">
                            <button class="btn btn-primary" type="submit" id="simpan" name="simpan" style="float: right; margin-top: 10px;">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"> </script>

<script>
    var urlIndex            = '{{ route("InvoicePenjualan.index") }}';
    var urlNumberInvoice    = '{{ route("InvoicePenjualan.number") }}';
    var urlInsert           = '{{ route("InvoicePenjualan.store") }}';
    var urlSuratJalan       = '{{ route("InvoicePenjualan.surat_jalan") }}';
    var urlSuratJalanDetail       = '{{ route("InvoicePenjualan.surat_jalan_detail") }}';
    var urlCoa = '{{ route("InvoicePenjualan.get_coa") }}';
    var url                 = '{{ url("InvoicePenjualan") }}';
    function ambilnomorfj()
    {
        $("#modal-loading").fadeIn();
        console.log('AMBIL DATA NOMOR FJ');
            $.ajax({
                    url : "{{url('ambilnomorfj')}}",
                    type: "GET",
                    data:{status:$('#Status_ppn').val()},
                    dataType:'json',
                    success: function(data)
                    { 
                        console.log('KODE BARU FJ');
                        console.log(data);
                        $('#Nomor').val(data);
                        $("#modal-loading").fadeOut();
                    }
            });
    }
</script>
<script src="{{ asset('js/global.js') }}"></script>
<script src="{{ asset('js/invoicepenjualan/invoicepenjualan_create.js') }}"></script>
<script>
    
</script>
@endsection