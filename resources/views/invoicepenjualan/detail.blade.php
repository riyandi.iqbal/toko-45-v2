<!-- ===========Create By Dedy 17-12-2019=============== -->
@extends('layouts.app')
@section('content')

<?php
use App\Helpers\AppHelper;
    $angkakoma = isset($coreset) ? $coreset->Angkakoma : 0;
?>

<!-- body data -->
<div class="main-grid">
    <div class="banner">
        <h2>
          <span><i class="fa fa-home"></i><a class="action-icons" href="{{url('home')}}" title="Dashboard" style="border: none;">Home</a></span>
              <i class="fa fa-angle-right"></i>
              <a href="{{url('InvoicePenjualan')}}">Data Invoice Penjualan</a>
              <i class="fa fa-angle-right"></i>
              <a>Detail Invoice Penjualan - {{ $penjualan->Nomor }} <b></b> </a>
        </h2>
    </div>
    <br>
    <div class="banner">
        <div class="widget_content">
            <div class="form_container left_label">
                <table class="table table-bordered" width="100%" style="font-size: 12px;">
                    <tbody>
                        <tr>
                            <td width="35%" >Tanggal</td>
                            <td width="65%"> {{ AppHelper::DateIndo($penjualan->Tanggal) }} </td>
                        </tr>
                        <tr>
                            <td>Nomor Penjualan</td>
                            <td> {{ $penjualan->Nomor }} </td>
                        </tr>
                        <tr>
                            <td>Nomor Surat Jalan</td>
                            <td> {{ $penjualan->Sjc_nomor }} </td>
                        </tr>
                        <tr>
                            <td>Customer</td>
                            <td> {{ $penjualan->Nama }} </td>
                        </tr>
                        <tr>
                            <td width="35%">Tanggal Jatuh Tempo</td>
                            <td width="65%"> {{ AppHelper::DateIndo($penjualan->Tanggal_jatuh_tempo) }} </td>
                        </tr>
                        <tr>
                            <td>Keterangan</td>
                            <td> {{ $penjualan->Keterangan }} </td>
                        </tr>
                        {{-- <tr>
                            <td>Status PPN</td>
                            <td> {{ ucfirst($penjualan->Status_ppn) }} </td>
                        </tr> --}}
                        <tr>
                            <td>Status</td>
                            <td> {{ ($penjualan->Batal == 0 || $penjualan->Batal == null ) ? 'Aktif' : 'Batal'  }} </td>
                        </tr>
                        <tr>
                            <td>Total Qty</td>
                            <td> {{ $penjualan->Total_qty }} </td>
                        </tr>
                        <tr>
                            <td>Sub Total Penjualan</td>
                            <td> {{ AppHelper::NumberFormat($penjualan->Grand_total + $penjualan->Discount, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td>Diskon</td>
                            <td> {{ AppHelper::NumberFormat($penjualan->Discount, $angkakoma) }} </td>
                        </tr>
                        <tr>
                            <td>Total Penjualan</td>
                            <td> {{ AppHelper::NumberFormat($penjualan->Grand_total, $angkakoma) }} </td>
                        </tr>
                        @if ($penjualan->Status_ppn == 'exclude')
                            <tr>
                                <td>PPN</td>
                                <td> {{ AppHelper::NumberFormat($penjualan->PPN, $angkakoma) }} </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="widget_content">
                <table class="table cell-border table-bordered" width="100%" style="font-size: 12px;">
                    <thead style="background-color: #16305d; color: #fff">
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Qty</th>
                            <th>Harga</th>
                            <th>Satuan</th>
                            <th>PPN % </th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px; border-collapse: collapse">
                        @foreach ($penjualan_detail as $key => $item)
                            <tr>
                                <td> {{ ++$key }} </td>
                                <td> {{ $item->Kode_Barang . ' - ' . $item->Nama_Barang }} </td>
                                <td style="text-align: center;"> {{ $item->Qty }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Harga, $angkakoma) }} </td>
                                <td style="text-align: center;"> {{ $item->Satuan }} </td>
                                <td style="text-align: center;"> {{ ($penjualan->Status_ppn == 'include') ? 0 : 10 }} </td>
                                <td style="text-align: right;"> {{ AppHelper::NumberFormat($item->Sub_total, $angkakoma) }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget_content py-4 text-center">
                <div class="form_grid_12">
                    <div class="btn col-11">
                        <span> <a style="color: white;" href="{{ route('InvoicePenjualan.index') }}" name="simpan">Kembali</a></span>
                    </div>
                    <div class="btn col-3">
                      <span><a style="color: white;" href="{{ route('InvoicePenjualan.print', $penjualan->IDFJ) }}" target="__blank">Print Data</a></span>
                    </div>
                    <div class="btn col-1">
                      <span><a style="color: white;" href="{{ route('InvoicePenjualan.show', $penjualan->IDFJ) }}">Edit Data</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
</div>

<script type="text/javascript">

</script>
@endsection